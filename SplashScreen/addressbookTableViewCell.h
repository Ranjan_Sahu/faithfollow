//
//  addressbookTableViewCell.h
//  wireFrameSplash
//
//  Created by Vikas on 18/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface addressbookTableViewCell : UITableViewCell

@property(weak,nonatomic)IBOutlet UILabel *ContactName;
@property(weak,nonatomic)IBOutlet UIButton *RightBTN;
@property(weak,nonatomic)IBOutlet UILabel *lastName;
@end
