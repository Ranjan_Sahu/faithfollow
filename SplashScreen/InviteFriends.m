//
//  InviteFriends.m
//  wireFrameSplash
//
//  Created by home on 4/1/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "InviteFriends.h"
#import "AppDelegate.h"
#import "YahooContactViewController.h"
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface InviteFriends ()<FBSDKAppInviteDialogDelegate>

@end

@implementation InviteFriends


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //    self.imageViewProfile.layer.cornerRadius =  self.imageViewProfile.frame.size.width / 2;
    //    self.imageViewProfile.clipsToBounds = YES;
    
    self.navigationController.navigationBar.topItem.title =@"";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationItem.title = @"Invite Friend";
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    // Do any additional setup after loading the view.
    
    _facebook_btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    //_contact_btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    _yahoo_btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    
    _contact_btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
}


-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //   [self.navigationItem setHidesBackButton:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)LoginWithFacebook:(id)sender{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logInWithReadPermissions:@[@"public_profile",@"email"] handler:^(FBSDKLoginManagerLoginResult *fbresult, NSError *error)
     {
         if (error)
         {
             NSLog(@"Process error");
             //             UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please Note" message:@"Process error." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             //             [message show];
         }
         else if (fbresult.isCancelled)
         {
             //             UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please Note" message:@"Cancelled ." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             //             [message show];
         }
         else
         {
             
             [self invite];
             
         }
     }];
    
}

-(void)invite{
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/872051476236961"];
    //optionally set previewImageURL
    content.appInvitePreviewImageURL = [NSURL URLWithString:@"https://www.mydomain.com/my_invite_image.jpg"];
    
    // present the dialog. Assumes self implements protocol `FBSDKAppInviteDialogDelegate`
    
    [FBSDKAppInviteDialog showFromViewController:self withContent:content delegate:self];
}

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog
       didFailWithError:(NSError *)error{
    
}
- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog
 didCompleteWithResults:	(NSDictionary *)results{
    
}




-(IBAction)LoginWithYahoo:(id)sender{
    // [self didReceiveAuthorization];
    AppDelegate*appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate createYahooSession];
    
    
}
-(IBAction)ContactList:(id)sender{
    ContactViewController *contactView=[self.storyboard instantiateViewControllerWithIdentifier:@"ContactViewController"];
    [self.navigationController pushViewController:contactView animated:YES];
    
}


-(IBAction)SelectContact:(id)sender{
    [self invite];
}






/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
