//
//  ContactViewController.m
//  wireFrameSplash
//
//  Created by home on 4/4/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "ContactViewController.h"

#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "addressbookTableViewCell.h"
#import "SearchResultsTableViewController.h"
@import Contacts;

@interface ContactViewController ()<ABPeoplePickerNavigationControllerDelegate>{
    NSMutableArray*  MutableArray__Contact;
}
@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBookController;
@property (nonatomic, strong) NSMutableArray *arrContactsData;
@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    

    UINavigationController *searchResultsController = [[self storyboard] instantiateViewControllerWithIdentifier:@"TableSearchResultsNavController"];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];
    self.searchController.searchResultsUpdater = self;
    
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x,
                                                       self.searchController.searchBar.frame.origin.y,
                                                       self.searchController.searchBar.frame.size.width, 44.0);
    
        _airlines=[[NSMutableArray alloc]initWithObjects:@"A",@"B",@"C",@"D",nil];
    

    // Do any additional setup after loading the view.
 
    NSString *version = [[UIDevice currentDevice] systemVersion];
    BOOL isVersion8 = [version hasPrefix:@"8."];
    BOOL isVersion7 = [version hasPrefix:@"7."];
    if(isVersion7 || isVersion8){
        //Use AddressBook
        [self addressBook];
    }
    else{
        //Use Contacts
        [self Contactsbook];
    }
    
}
-(void)Contactsbook{
    
    
   _contactArray=[[NSMutableArray alloc]init];
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            } else {
                NSString *phone;
                NSString *fullName;
                NSString *firstName;
                NSString *lastName;
                UIImage *profileImage;
                NSMutableArray *contactNumbersArray;
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    else{
                        fullName=[NSString stringWithFormat:@"%@  %@",firstName,lastName];
                    }
                    UIImage *image = [UIImage imageWithData:contact.imageData];
                    if (image != nil) {
                        profileImage = image;
                    }else{
                        profileImage = [UIImage imageNamed:@"person-icon.png"];
                    }
                    for (CNLabeledValue *label in contact.phoneNumbers) {
                        phone = [label.value stringValue];
                        if ([phone length] > 0) {
                            [contactNumbersArray addObject:phone];
                        }
                    }
                    NSDictionary* personDict = [[NSDictionary alloc] initWithObjectsAndKeys: fullName,@"fullName",profileImage,@"userImage",phone,@"PhoneNumbers", nil];
                    [_contactArray addObject:personDict];
                }
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                  // NSLog(@"%@",ar_Contact);
                                   [self.tableView reloadData];
                               });
            }
        }
    }];

}
-(void)addressBook{
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    
    if (status == kABAuthorizationStatusDenied || status == kABAuthorizationStatusRestricted) {
        
        [[[UIAlertView alloc] initWithTitle:nil message:@"This app requires access to your contacts to function properly. Please visit to the \"Privacy\" section in the iPhone Settings app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    
    if (!addressBook) {
        NSLog(@"ABAddressBookCreateWithOptions error: %@", CFBridgingRelease(error));
        return;
    }
    
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        if (error) {
            NSLog(@"ABAddressBookRequestAccessWithCompletion error: %@", CFBridgingRelease(error));
        }
        
        if (granted) {
            // if they gave you permission, then just carry on
            
            [self listPeopleInAddressBook:addressBook];
        } else {
            // however, if they didn't give you permission, handle it gracefully, for example...
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // BTW, this is not on the main thread, so dispatch UI updates back to the main queue
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"This app requires access to your contacts to function properly. Please visit to the \"Privacy\" section in the iPhone Settings app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            });
        }
        
        CFRelease(addressBook);
    });

}
- (void)listPeopleInAddressBook:(ABAddressBookRef)addressBook
{
    NSArray *allPeople = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
    NSInteger numberOfPeople = [allPeople count];
    _contactArray=[[NSMutableArray alloc]init];
    for (NSInteger i = 0; i < numberOfPeople; i++) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        
        ABRecordRef person = (__bridge ABRecordRef)allPeople[i];
        
        NSString *firstName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName  = CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
        
         NSString*  fullName=[NSString stringWithFormat:@"%@  %@",firstName,lastName];
        NSLog(@"Name:%@ %@", firstName, lastName);
        [dict setObject:fullName forKey:@"fullName"];

        
        
        //to get mobile number
        
        //        ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
        //
        //        CFIndex numberOfPhoneNumbers = ABMultiValueGetCount(phoneNumbers);
        //        for (CFIndex i = 0; i < numberOfPhoneNumbers; i++) {
        //            NSString *phoneNumber = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phoneNumbers, i));
        //            NSLog(@"  phone:%@", phoneNumber);
        //        }
        //        CFRelease(phoneNumbers);
        [self.contactArray addObject:dict];
        
        
        NSLog(@"=============================================");
    }
    
    
}

-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    return NO;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [_contactArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    addressbookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"address" forIndexPath:indexPath];
   cell.ContactName.text=[[_contactArray objectAtIndex:indexPath.row]valueForKey:@"fullName"];
    //cell.lastName.text=[[_contactArray objectAtIndex:indexPath.row]valueForKey:@"lastName"];
    [cell.RightBTN  addTarget:self action:@selector(TickClickBTN:) forControlEvents:UIControlEventTouchUpInside];
    //cell.ContactName.text=@"hello";
    
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footer=[[UIView alloc] initWithFrame:CGRectMake(0,0,self.tableView.frame.size.width,50.0)];
    footer.layer.borderWidth = 1;
    footer.layer.borderColor =[UIColor colorWithRed:189.0/255.0f green:189.0/255.0f blue:189.0/255.0f alpha:1.0].CGColor;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"Invites" forState:UIControlStateNormal];
    button.frame = CGRectMake(self.tableView.frame.size.width/2-50, 50,100,30);
    button.layer.cornerRadius = 5;
    button.layer.borderWidth = 1;
    button.backgroundColor=[UIColor redColor];
    button.layer.borderColor =[UIColor colorWithRed:189.0/255.0f green:189.0/255.0f blue:189.0/255.0f alpha:1.0].CGColor;
    [button  addTarget:self action:@selector(InvitesFriendMethod) forControlEvents:UIControlEventTouchUpInside];
    footer.backgroundColor=[UIColor whiteColor];
    [footer addSubview:button];
    return footer;
}


-(void)TickClickBTN:(UIButton*)sender {
    
    sender.selected=!sender.selected;
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [self.tableView convertPoint:buttonOrigin fromView:[sender superview]];
    NSIndexPath *rowIndexPath = [self.tableView indexPathForRowAtPoint:originInTableView];
    NSInteger section = [rowIndexPath section];
    NSLog(@"Facebook Button Clicked");
    NSLog(@"Tableview Section %ld",section);
    NSLog(@"Tableview Row %ld",rowIndexPath.row);
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 135.0f;
}
-(void)InvitesFriendMethod {
    NSLog(@"hello invited");
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
