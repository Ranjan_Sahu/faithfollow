//
//  HomeViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "FeedViewController.h"
#import "FeedTableViewCell.h"
#import "DeviceConstant.h"
#define SKY_BLUE_COLOR [UIColor colorWithRed:0.1137 green:0.4824 blue:0.7765 alpha:1.0]

@interface FeedViewController ()<UITabBarDelegate>{
    NSMutableArray *items,*_quoteArray;
    CGFloat heightofCell;
    NSInteger selectedRow;
    UIView *lineView ;
    

}
@property (strong, nonatomic) FeedTableViewCell *customCell;
@end

@implementation FeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    
    //_customtabbar.delegate=self;
  // [_customtabbar setSelectedItem:[_customtabbar.items objectAtIndex:0]];
    self.view.backgroundColor=[UIColor whiteColor];
    
    
    items=[[NSMutableArray alloc]initWithObjects:@"John",@"Josh",@"Butler",@"Warner",nil];
    if (IS_IPHONE6PLUS) {
          _leadingconstraints.constant=60;
    }

  

  //  [self.tableViewOutlet setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//   [self.tableViewOutlet setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    selectedRow=-1;
    
 //   [_tableViewOutlet setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
 //   [_tableViewOutlet setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];

    //cell.contentView.layer.borderColor=[UIColor clearColor].CGColor;
  
    _upperView.layer.borderWidth=0.5f;
    _upperView.layer.borderColor=[UIColor grayColor].CGColor;
    
    
    _quoteArray = [@[@"For the past 33 years, I have looked in the mirror every morning and asked myself: 'If today were the last day of my life, would I want to do what I am about to do today?' And whenever the answer has been 'No' for too many days in a row, I know I need to change something. -Steve Jobs",
                     @"Be a yardstick of quality. Some people aren't used to an environment where excellence is expected. - Steve Jobs",
                     @"Innovation distinguishes between a leader and a follower. -Steve Jobs",  @"Innovation distinguishes between a leader and a follower. -Steve Jobs"] mutableCopy];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return items.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* identifier=@"Cell";
    
    FeedTableViewCell *cell = [_tableViewOutlet dequeueReusableCellWithIdentifier:identifier];

    if (cell==nil) {
        
        cell =[[FeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    if (selectedRow==indexPath.row) {
          [cell.comments_btn setTitleColor:SKY_BLUE_COLOR forState:UIControlStateNormal];
    }else{
        [cell.comments_btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

    }
    
    cell.contentView.backgroundColor=[UIColor clearColor];
    cell.personName_lbl.text=[items objectAtIndex:indexPath.row];
    [cell.comments_btn addTarget:self action:@selector(comments_act:) forControlEvents:UIControlEventTouchUpInside];
    cell.comments_btn.tag=1000;
   // cell.person_imageview.image=[UIImage imageNamed:@"tom"];
    
    cell.com_lbl.text=[_quoteArray objectAtIndex:indexPath.row];
    UIFont *systemFont = [UIFont systemFontOfSize:17.0];
    cell.com_lbl.font=  systemFont;
    
    //NSLog(@"fomnts is%@",cell.com_lbl.font);
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(!self.customCell) {
        self.customCell = [_tableViewOutlet dequeueReusableCellWithIdentifier:@"Cell"];
    }
    
    // Configure the cell
    self.customCell.personName_lbl.text=[items objectAtIndex:indexPath.row];
    UIFont *systemFont = [UIFont systemFontOfSize:17.0];
    self.customCell.com_lbl.text=[_quoteArray objectAtIndex:indexPath.row];
    self.customCell.com_lbl.font=systemFont;
    self.customCell.personName_lbl.font=systemFont;
    [self.customCell layoutIfNeeded];
    
    // Get the height for the cell
    if(indexPath.row == selectedRow) {
        return self.customCell.com_lbl.frame.size.height +372;
    }
        heightofCell=195+self.customCell.com_lbl.frame.size.height;
    return heightofCell;
  }
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPat{
    return 140;
    }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)btn_act:(id)sender {
}
- (IBAction)status_act:(id)sender {
}

- (IBAction)prayer_act:(id)sender {
}

- (IBAction)photo_act:(id)sender {
}
- (IBAction)comments_act:(UIButton*)sender {
    
    if (sender.selected) {
        selectedRow=-1;
       [_tableViewOutlet reloadData];
        sender.selected=false;
        }else{
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
        NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
        if (indexPath != nil)
        {
            selectedRow=indexPath.row;
          [_tableViewOutlet reloadData];
           }
    sender.selected=true;
    }
    
}
@end
