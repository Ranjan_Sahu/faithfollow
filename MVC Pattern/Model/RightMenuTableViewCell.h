//
//  RightMenuTableViewCell.h
//  wireFrameSplash
//
//  Created by home on 9/1/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightMenuTableViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UIImageView *rightslideImage;
@property(nonatomic,weak)IBOutlet UILabel *rightSlideNameLbl;
@end
