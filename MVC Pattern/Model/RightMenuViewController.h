//
//  RightMenuViewController.h
//  SlideMenu
//
//  Created by Aryan Gh on 4/26/14.
//  Copyright (c) 2014 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "SlideNavigationContorllerAnimator.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"

@interface RightMenuViewController : UIViewController<UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@property (weak, nonatomic) IBOutlet UIImageView *mainViewImage;

- (IBAction)viewProfileMethodAction:(UIButton*)sender;

@property (weak, nonatomic) IBOutlet UIButton *ViewProfileMethod;

//

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageRightTrailing;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewProfileTailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inviteTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *RateTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoutTrailing;


@end
