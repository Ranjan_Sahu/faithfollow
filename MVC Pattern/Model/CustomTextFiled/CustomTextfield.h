//
//  CustomTextfield.h
//  wireFrameSplash
//
//  Created by home on 5/5/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextfield : UITextField
- (void)drawRect:(CGRect)rect;
- (void)drawPlaceholderInRect:(CGRect)rect;
-(void)setPlaceholder:(NSString *)placeholder;

@end
