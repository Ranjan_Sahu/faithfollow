//
//  WebServiceHelper.m
//  APiDemo
//
//  Created by SourceKode on 06/10/15.
//  Copyright © 2015 Ecsion. All rights reserved.
//

#import "WebServiceHelper.h"
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "Constant1.h"
#import "UIView+Toast.h"



@implementation WebServiceHelper

#pragma mark - SharedInstance

+ (WebServiceHelper *)sharedInstance {
    static dispatch_once_t pred;
    static WebServiceHelper *shared = nil;
    dispatch_once(&pred, ^{
        shared = [WebServiceHelper new];
    });
    return shared;
}
#pragma mark - Get API Calling
- (void)callGetDataWithMethod:(NSString *)strMethodName  withParameters:(NSDictionary *)requestDict  withHud:(BOOL)isHud success:(void(^)(id response))successBlock errorBlock:(void(^)(id error))errorBlock{
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate showhud];
        
    });
    NSMutableString *urlString = [NSMutableString new];
    [urlString appendString:baseURl];
//    [urlString appendString:@""];
    [urlString appendString:strMethodName];
    if (requestDict){
        NSArray *keysArray = [requestDict allKeys];
        for (int i= 0 ; i< keysArray.count; i++) {
            NSString *key = [keysArray objectAtIndex:i];
            if (i == 0) {
                [urlString appendString:[NSString stringWithFormat:@"?%@=%@",key,[requestDict valueForKey:key]]];
            }
            else {
                [urlString appendString:[NSString stringWithFormat:@"&%@=%@",key,[requestDict valueForKey:key]]];
            }
        }
    }
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        // Asynchronously Api is hit here
    NSURLSessionDataTask  *postDataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if (data != nil && !error) {
            
            NSError *parseJsonError = nil;
            id response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseJsonError];
            if (!parseJsonError){
                successBlock(response);
            }
            else{
                
                errorBlock(response);
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please try after some time." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            });
            
        }
        
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate hidehud];
        
    });
  
    [postDataTask resume];
    
}

#pragma mark - Post API Calling

- (void)callPostDataWithMethod:(NSString *)strMethodName withParameters:(NSDictionary *)requestDict  withHud:(BOOL)isHud success:(void(^)(id response))successBlock errorBlock:(void(^)(id error))errorBlock{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate showhud];
        
    });
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSString  *baseUrl=baseURl;
    NSString *encodedUrl = [[NSString stringWithFormat:@"%@%@", baseUrl, strMethodName]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL  *serverUrl = [NSURL URLWithString:encodedUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:serverUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    NSMutableString *paramStr = [NSMutableString new];
    for(NSString *key in requestDict){
        if(paramStr.length){
            [paramStr appendString:[NSString stringWithFormat:@"&%@=%@",key,requestDict[key]]];
        }
        else{
            [paramStr appendString:[NSString stringWithFormat:@"%@=%@",key,requestDict[key]]];
        }
        
    }
    
    NSData *postData = [paramStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *reqJSONStr = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[reqJSONStr dataUsingEncoding:NSUTF8StringEncoding] mutableCopy]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if (data != nil && !error)
        {
//          NSString *str  = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            NSLog(@"repsonse %@",str);
            
            NSError *parseJsonError = nil;
            id response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseJsonError];
            if (!parseJsonError){
                successBlock(response);
            }
            else{
                errorBlock(response);
            }

        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Seems like there is  a problem connecting to the server,please Try after some time." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            });


        }
 
        dispatch_async(dispatch_get_main_queue(), ^{
            AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            [appDelegate hidehud];
            
        });
    }];
  
    [postDataTask resume];
}

// getting notificaton service
#pragma mark - Post API Calling

- (void)callPostDataWithMethodnotification:(NSString *)strMethodName withParameters:(NSDictionary *)requestDict  withHud:(BOOL)isHud success:(void(^)(id response))successBlock errorBlock:(void(^)(id error))errorBlock{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSString  *baseUrl=baseURl;
    NSString *encodedUrl = [[NSString stringWithFormat:@"%@%@", baseUrl, strMethodName]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL  *serverUrl = [NSURL URLWithString:encodedUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:serverUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    NSMutableString *paramStr = [NSMutableString new];
    for(NSString *key in requestDict){
        if(paramStr.length){
            [paramStr appendString:[NSString stringWithFormat:@"&%@=%@",key,requestDict[key]]];
        }
        else{
            [paramStr appendString:[NSString stringWithFormat:@"%@=%@",key,requestDict[key]]];
        }
        
    }
    
    NSData *postData = [paramStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *reqJSONStr = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[reqJSONStr dataUsingEncoding:NSUTF8StringEncoding] mutableCopy]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if (data != nil && !error)
        {
            //          NSString *str  = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            //            NSLog(@"repsonse %@",str);
            
            NSError *parseJsonError = nil;
            id response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseJsonError];
            if (!parseJsonError){
                successBlock(response);
            }
            else{
                errorBlock(response);
            }
            
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Seems like there is  a problem connecting to the server,please Try after some time." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            });
            
            
        }
        
    }];
    
    [postDataTask resume];
}


//



- (void)call_notificationPostDataWithMethod:(NSString *)strMethodName withParameters:(NSDictionary *)requestDict  withHud:(BOOL)isHud success:(void(^)(id response))successBlock errorBlock:(void(^)(id error))errorBlock{
       dispatch_async(dispatch_get_main_queue(), ^{
        
    });
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSString  *baseUrl=baseURl;
    NSString *encodedUrl = [[NSString stringWithFormat:@"%@%@", baseUrl, strMethodName]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL  *serverUrl = [NSURL URLWithString:encodedUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:serverUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    NSMutableString *paramStr = [NSMutableString new];
    for(NSString *key in requestDict){
        if(paramStr.length){
            [paramStr appendString:[NSString stringWithFormat:@"&%@=%@",key,requestDict[key]]];
        }
        else{
            [paramStr appendString:[NSString stringWithFormat:@"%@=%@",key,requestDict[key]]];
        }
        
    }
    
    NSData *postData = [paramStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *reqJSONStr = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[reqJSONStr dataUsingEncoding:NSUTF8StringEncoding] mutableCopy]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
        if (data != nil && !error)
        {
//            commented for memory leak
//            NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSError *parseJsonError = nil;
            id response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseJsonError];
            if (!parseJsonError){
                successBlock(response);
            }
            else{
                errorBlock(response);
            }
        }
        else{
            errorBlock(@"Server is not responding. Please try after some time.");
        }
    }];
    
    [postDataTask resume];
}
- (void)callGetDataMethod:(NSString *)strMethodName withParameters:(NSDictionary *)requestDict  withHud:(BOOL)isHud success:(void(^)(id response))successBlock errorBlock:(void(^)(id error))errorBlock{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (isHud){
        //    [SVProgressHUD showWithStatus:@"Loading"];
        }
    });
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSString  *baseUrl=baseURl;
    NSString *encodedUrl = [[NSString stringWithFormat:@"%@%@", baseUrl, strMethodName]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL  *serverUrl = [NSURL URLWithString:encodedUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:serverUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    NSMutableString *paramStr = [NSMutableString new];
    for(NSString *key in requestDict){
        if(paramStr.length){
            [paramStr appendString:[NSString stringWithFormat:@"&%@=%@",key,requestDict[key]]];
        }
        else{
            [paramStr appendString:[NSString stringWithFormat:@"%@=%@",key,requestDict[key]]];
        }
        
    }
    
    NSData *postData = [paramStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *reqJSONStr = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    [request setHTTPMethod:@"GET"];
    [request setHTTPBody:[[reqJSONStr dataUsingEncoding:NSUTF8StringEncoding] mutableCopy]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isHud)
            {
                [SVProgressHUD dismiss];
            }
        });
        
        if (data != nil && !error)
        {
//            commnet  for memory leak
//            NSString *charlieSendString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSError *parseJsonError = nil;
            id response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseJsonError];
            if (!parseJsonError){
                successBlock(response);
            }
            else{
                errorBlock(response);
            }
        }
        else{
            errorBlock(@"Server is not responding. Please try after some time.");
        }
    }];
    
    [postDataTask resume];
}
@end
