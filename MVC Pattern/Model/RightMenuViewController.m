//
//  RightMenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/26/14.
//  Copyright (c) 2014 Aryan Ghassemi. All rights reserved.
//

#import "RightMenuViewController.h"
#import "InviteFriends.h"
#import "Profile_editViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "SignupViewController.h"
#import "iRate.h"
#import "DeviceConstant.h"
#import "Mixpanel.h"
#import "Constant1.h"
@implementation RightMenuViewController
{
    
}
#pragma mark - UIViewController Methods -

- (void)viewDidLoad
{
	[super viewDidLoad];
    self.slideOutAnimationEnabled = YES;
    
    _mainViewImage.image=[UIImage imageNamed:@"rightMenuimage.jpg"];

    if (IS_IPHONE5) {
        _imageRightTrailing.constant=45;
        _viewProfileTailing.constant=2;
        _inviteTrailing.constant=2;
        _RateTrailing.constant=2;
        _logoutTrailing.constant=2;
    }
    else if (IS_IPHONE6PLUS){
        _imageRightTrailing.constant=90;

    }
    else if(IS_IPHONE4){
        _imageRightTrailing.constant=45;
        _viewProfileTailing.constant=2;
        _inviteTrailing.constant=2;
        _RateTrailing.constant=2;
        _logoutTrailing.constant=2;
  
    }
 
	
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(userProfile)
//                                                 name:@"userprofile"
//                                               object:nil];
//
}

-(void)userProfile{
    
}

-(void)inviteFriendclassMethod{
    InviteFriends *invite=[self.storyboard instantiateViewControllerWithIdentifier:@"InviteFriends"];
//    NSLog(@"%@",self.navigationController);
    [self.navigationController pushViewController:invite animated:YES];
}

-(void)ViewProfileclassMethod{
  Profile_editViewController  *profileEdit = [self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
    [self.navigationController pushViewController:profileEdit animated:YES];
}

-(void)rateusclassMethod{
    
}


-(void)logoutMehtod{
    UIAlertView *alertViewaction=[[UIAlertView alloc] initWithTitle:appTitle message:@"Are you sure you want to logout?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alertViewaction show];
 
  }

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==1) {
        [self LogoutAlertAction];
    }
    
}

// logout services

-(void)LogoutAlertAction{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }
    else {
        
        if ([[FBSDKAccessToken currentAccessToken] isKindOfClass:[FBSDKAccessToken class]]) {
            
            AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me/permissions/" parameters:@{@"access_token":[FBSDKAccessToken currentAccessToken]} HTTPMethod:@"DELETE"];
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                  id result,
                                                  NSError *error){
                if (!error) {
//                    NSLog(@"logout successfully");
                }
                else {
//                    NSLog(@"error :: %@",error);
                }
            }];
            
            if (!app.fbManager) {
                app.fbManager = [[FBSDKLoginManager alloc] init];
            }
            
            [app.fbManager logOut];
            
            [FBSDKAccessToken setCurrentAccessToken:nil];
            [FBSDKProfile setCurrentProfile:nil];
            
        }
        
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"device_token":  [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"]?:@"",
                               @"device_type":@"IOS",
                               };
//        NSLog(@"dict value %@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"logout" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                NSDate *logoutDate=[NSDate date];
                                 [AppDelegate sharedAppDelegate].logdate=logoutDate;
                                NSTimeInterval distanceBetweenDates = [[AppDelegate sharedAppDelegate].logdate timeIntervalSinceDate:[AppDelegate sharedAppDelegate].logindate];
                                NSString *timeinterval=[self stringFromTimeInterval:distanceBetweenDates];
                                NSLog(@"time interval %@",timeinterval);
                                NSString *sessionTime=[Constant1 logoutsession:timeinterval];
                                NSLog(@"tsessionTime %@",sessionTime);
                                NSString *logouttime=[Constant1 createTimeStamp];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Last seen": logouttime}];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Last seen": logouttime}];
                                
                                
                                [[AppDelegate sharedAppDelegate].mxPanel track:@"Logout" properties:@{
                                             @"Last seen": logouttime,
                                             @"Session length":sessionTime
                                        }];
                                [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"Login"];
                                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"fbvalue"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                    SignupViewController *controler=[self.storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
                    [self.navigationController setViewControllers:@[controler]];
                        [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:controler
                                withSlideOutAnimation:nil  andCompletion:nil];
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                    [[AppDelegate sharedAppDelegate].mxPanel reset];
                                });
                        });
                        
                        }
                        else{
                            NSArray *messageArray=nil;
                            messageArray=[response valueForKey:@"message"];
                            if ([messageArray count]>0) {
                                NSString *mess=[messageArray objectAtIndex:0];
                                [self.view makeToast:mess duration:1.0 position:CSToastPositionCenter];
                            }
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
    }
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}
- (IBAction)viewProfileMethodAction:(UIButton*)sender {
    UIViewController *vc ;
    switch (sender.tag) {
        case 10:
            [TenjinSDK sendEventWithName:@"menu_nav_profile"];
            [FBSDKAppEvents logEvent:@"menu_nav_profile"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"menu_nav_profile"]];
        vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
            break;
            
        case 11:
            [TenjinSDK sendEventWithName:@"menu_nav_invite_friends"];
            [FBSDKAppEvents logEvent:@"menu_nav_invite_friends"];

            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"menu_nav_invite_friends"]];
        vc = [self.storyboard instantiateViewControllerWithIdentifier: @"InviteFriends"];
            break;
        case 12:
            [TenjinSDK sendEventWithName:@"menu_nav_rate_us"];
            [FBSDKAppEvents logEvent:@"menu_nav_rate_us"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"menu_nav_rate_us"]];
            [self rateUsPressed];
            break;
            
        case 13:
            [TenjinSDK sendEventWithName:@"menu_nav_logout"];
            [FBSDKAppEvents logEvent:@"menu_nav_logout"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"menu_nav_logout"]];
        [self logoutMehtod];
            break;
            
        default:
            break;
    }
    
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                  withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                          andCompletion:nil];
    
}


-(void)rateUsPressed
{
    [iRate sharedInstance].delegate=(id)self;
    [iRate sharedInstance].previewMode = YES;
    [[iRate sharedInstance] logEvent:NO];
}

@end
