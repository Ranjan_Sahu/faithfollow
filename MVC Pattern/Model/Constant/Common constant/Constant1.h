//
//  Constant.h
//  Demp_projeect
//
//  Created by home on 2/15/16.
//  Copyright (c) 2016 home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import "WebServiceHelper.h"

static NSString *const NoNetwork = @"The Internet connection appears to be offline";
static NSString *const EnterEmail = @"Please enter Email Address";
static NSString *const EnterPassword = @"Password number must be six or more than six digits";
static NSString *const InvalidEmail = @"Please enter valid Email";
static NSString *const FirstnameStr = @"First name  should not be Empty";
static NSString *const LastnameStr =@"Last name should not be Empty";
static NSString *const NewPasswordStr = @"New Password should not be Empty";
static NSString *const OldPasswordStr = @"Old Password should not be Empty";
static NSString *const ImageStr = @"Select your image";
static NSString *const FeedbackStr = @"Please enter Feed Back ";


static NSString *const phoneStr=@"Phone should not be Empty";
static NSString *const RetypePasswordStr1 =@"Retype  Password should not be Empty";
static NSString *const RetypePasswordStr =@"Please reenter confirm password";
static NSString *const CountryStr=@"Country should not be Empty";
static NSString *const ZipcodeStr =@"Zipcode should not be Empty";
static NSString *const ZipcodeDigit =@"Zipcode should  be  Equal six digit";
static NSString *const StateStr =@"State  should not be Empty";
static NSString *const CityStr =@"City should not be Empty";
static NSString *const CitylandStr =@"CityLand should not be Empty";
static NSString *const AddressStr =@"Address field  should not be Empty";
static NSString *const STDStr=@"DOB field should not be Empty";
static NSString *const CurrentPass=@"Curentpassword should not be Empty";
static NSString *const NewpassStr=@"Newpassword  should not be Empty";
static NSString *const termselectedStr=@"Please select Term and condition ! ";
static NSString *const GenderStr=@"Please select Gender ! ";
static NSString *const zipStr1=@"Zipcode number should be five digits";
//static NSString *const ImageapiUrl=@"http://10.0.11.134/socialmedia/public/";


// edit Setting
static NSString *const appName=@"Please enter your App Name";
static NSString *const myfriend=@"Please enter your Friend";
static NSString *const enterpassword=@"Please enter Password";
static NSString *const retypepass=@"Please enter Retype Password";
static NSString *const username = @"User name  should not be Empty";
static NSString *const MobileNumberEmpty=@"Phone number should be Empty";
static NSString *const birthdayStr=@"Select Birthday ";
static NSString *const genderStr=@"Select Gender ";

static NSString *const MobileNumber=@"Phone number should be ten digits";
static NSString *const zipNumber=@"Zip  number should be five digits";

static NSString *const denomination=@"Enter your  Denomination";

//Addition information

static NSString *const strjobEnter=@"Enter your Job Title";
static NSString *const strParishEnter=@"Enter your Parish";
static NSString *const strEmployeeEnter=@"Enter your Employee Title";
static NSString *const strHighEnter=@"Enter your high school";
static NSString *const strSiblingEnter=@"Enter your sibling";
static NSString *const strchildrenEnter=@"Enter your children";
static NSString *const strFavouriteEnter=@"Enter your favourite";
static NSString *const strInspariartionEnter=@"Enter your inspiration";
//application Title
static NSString *const appTitle=@"Faith Follow";
static NSString *const birthday=@"Enter your Birthday ";
static NSString *const birthday13=@"you must be 13 years old to sign up ";
static NSString *const selectgender=@"select your gender ";




//baseurl



@interface Constant1 : NSObject
+ (BOOL)validateEmail:(NSString *)strEmailId;
+ (BOOL)myMobileNumberValidate:(NSString*)number;
+ (BOOL)myZipNumberValidate:(NSString*)number;
+(NSString *)getStringObject:(id)strString;
+(NSString *)getNumberStringObject:(id)strString;
+(CGFloat)checkimagehieght;
+(UIImage *)compressImage:(UIImage *)image;
+(NSAttributedString *) customizeString :(NSString *) str range:(NSInteger)substrrange;
+(NSAttributedString *) customizeString :(NSString *) str range:(NSInteger)substrrange rangeOne:(NSInteger)substrrangeOne;
+(BOOL)validateArray:(NSMutableArray*)messageArray;

+(NSString *)getCurrentDateTime;
+(NSString *)getCurrentDate;
+(NSString *)isStrEmpty:(NSString*) str;
+(NSString *)createTimeStamp;
+(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate;
+(NSString *)getLoginCurrentDateTime;
+ (NSString*)createTimeStampBirth:(NSString*)dateStr;
+ (NSString*)logoutsession:(NSString*)dateStr;
+(NSString*)birthdate:(NSString*)dateStr;


@end
