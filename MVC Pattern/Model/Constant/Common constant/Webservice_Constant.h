//
//  Webservice_Constant.h
//  wireFrameSplash
//
//  Created by Vikas on 18/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#ifndef Webservice_Constant_h
#define Webservice_Constant_h
//
static NSString * const kStatusBarTappedNotification = @"statusBarTappedNotification";
//
// Staging webservice URL an beta 3 project of mixpanel

//static NSString *const currentVersion=@"4";
//static NSString *const mixPanelTokenLive=@"b74da0841852ed6b03a76fd39f2fb5e6";
//static NSString *const baseURl=@"http://mobile.faithfollow.com/Socialmedia/public/api/";
//static NSString *const ImageapiUrl=@"http://mobile.faithfollow.com/Socialmedia/public/";
//static NSString *const post_ImageapiUrl=@"http://mobile.faithfollow.com/Socialmedia/storage/";
//static NSString *const mywallpostStatusViewService=@"http://mobile.faithfollow.com/Socialmedia/public/api/wall?";
//static NSString *const wallStatusViewService=@"http://mobile.faithfollow.com/Socialmedia/public/api/groupPost";
//static NSString *const feedviewwallService=@"http://mobile.faithfollow.com/Socialmedia/public/api/wall/";
//


// invite facebook image
static NSString *const postLogo_invite=@"https://app.faithfollow.com/Socialmedia/public/assets/img/faithfollowInvite.png";
////
// privacy policy
static NSString *const byclickingtermcondition=@"https://faithfollow.com/PrivacyPolicyandTermsConditions";
static NSString *const urlPrivacy=@"https://faithfollow.com/PrivacyPolicyandTermsConditions#privacy";
static NSString *const urlTermCondiatiom=@"https://faithfollow.com/PrivacyPolicyandTermsConditions#service";
static NSString *const contactInviteText=@"Join me on the top Christian Social Network at www.faithfollow.com";

//  vesrion of WS
// Live Web service URL/// change wall statusview services

static NSString *const currentVersion=@"5";
static NSString *const mixPanelTokenLive=@"02c7aa898402a244c0a14124b53830a8";
static NSString *const wallStatusViewService=@"https://app.faithfollow.com/v5/Socialmedia/public/api/groupPost";
static NSString *const baseURl=@"https://app.faithfollow.com/v5/Socialmedia/public/api/";
static NSString *const ImageapiUrl=@"https://app.faithfollow.com/Socialmedia/public/";
static NSString *const post_ImageapiUrl=@"https://app.faithfollow.com/Socialmedia/storage/";
static NSString *const feedviewwallService=@"https://app.faithfollow.com/v5/Socialmedia/public/api/wall/";
static NSString *const mywallpostStatusViewService=@"https://app.faithfollow.com/v5/Socialmedia/public/api/wall?";

//
// single post
static NSString *const singleDetailedPost=@"singleDetailedPost";
//community
static NSString *const influencerList=@"influencerList";
static NSString *const Following=@"friendslist";
static NSString *const followers=@"followers";
static NSString *const unfollow=@"unfollow";
static NSString *const follow=@"follow";
static NSString *const searchfriend=@"searchfriend";
static NSString *const singleUserPost=@"singleUserPost";

//

static NSString *const wall_unlike=@"unlike";
//////friendrequest
static NSString *const recievedrequest=@"recievedrequest";
static NSString *const acceptrequest=@"acceptrequest";
static NSString *const cancelrequest=@"cancelrequest";
static NSString *const unfriend=@"unfriend";
static NSString *const rejectrequest=@"rejectrequest";
static NSString *const friendprofile=@"friendsprofile";
static NSString *const friendslist=@"friendslist";
// new popGroup webservice

static NSString *const updateProfile=@"updateProfile";

//////friendNoitification
static NSString *const notificationCount=@"getnotificationcount";
static NSString *const notificationList=@"getnotificationlist";
static NSString *const setnotificationasread=@"setnotificationasread";
static NSString *const wallview=@"wallview";
static NSString *const feedback=@"saveFeedback";
static NSString *const getnotificationcount=@"getnotificationcount";

//////community
static NSString *const communitylist=@"community/mylist";
//////getpostcount
static NSString *const getpostcount=@"getpostcount";
///////////////
static NSString *const createCommunity=@"community";
static NSString *const deleteCommunity=@"community-destroy";
static NSString *const communityDisplay=@"communitydisplay";
static NSString *const communityUpdate=@"community/update";
static NSString *const communityPosttype=@"community-post-type";
//////communityDetails
static NSString *const approve=@"approve";
static NSString *const join=@"communitydetails";
static NSString *const leave=@"community/leave";
static NSString *const reject=@"community/reject";
//////User

static NSString *const signup=@"user";
static NSString *const signupQuick=@"quickSignup";
static NSString *const login=@"login";
static NSString *const getimage=@"getimage";
// facebook login

static NSString *const signupfb=@"signupByfb";
static NSString *const incompleteFbData=@"fbIncompleteDataNfbIdExist";
//static NSString *const loginfaith=@"http://app.faithfollow.com/api/login";
//Wall
static NSString *const create=@"wall";
static NSString *const delete_wall=@"wall/delete";
static NSString *const wall_display=@"walldetails/display";
static NSString *const allwalllist =@"newWallFeed";
static NSString *const abuse=@"abuse";
//Walls like
static NSString *const wall_like=@"praised";
//Comments
static NSString *const addcomments=@"comment";
static NSString *const commentLists=@"commentlist";
static NSString *const communitytLists=@"communityList";

// community group notification
static NSString *const getnotificationlist=@"getnotificationlist";
static NSString *const communitydetails=@"communitydetails";
//favourite group
static NSString *const favourite=@"favourite";
static NSString *const remove_favourite=@"remove_favourite";
static NSString *const favourite_postlist=@"favourite_postlist";
static NSString *const groupsearch=@"groupsearch";
static NSString *const walldetails=@"communitydetails/walldetails";
static NSString *const postGroupType=@"post";
static NSString *const grouplistFeedPost=@"community/list";

/////admob

// /593395991/Test  duble clicking

//bottom ad unit: ca-app-pub-1832837484980178/9585789849
//#define kBannerAdUnitID @"/593395991/Test"
#define kBannerAdUnitID @"ca-app-pub-1832837484980178/9585789849"
#define knative_BannerAdUnitID @"ca-app-pub-1832837484980178/5011035848"
#define  semibold  [UIFont fontWithName:@"Avenir-Medium" size:16];
#define  SemoboldSmall  [UIFont fontWithName:@"Avenir-Medium" size:13];
#define  light  [UIFont fontWithName:@"Avenir-Roman" size:15];

#define  font_roman_14  [UIFont fontWithName:@"Avenir-Roman" size:14];
#define  font_roman_12  [UIFont fontWithName:@"Avenir-Roman" size:12];



#endif /* Webservice_Constant_h */
