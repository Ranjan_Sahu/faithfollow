//
//  Constant.m
//  Demp_projeect
//
//  Created by home on 2/15/16.
//  Copyright (c) 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "Constant1.h"

@implementation Constant1

+ (BOOL)validateEmail:(NSString *)strEmailId {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:strEmailId];
}

+ (BOOL)myMobileNumberValidate:(NSString*)number{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    return [numberTest evaluateWithObject:number];
}

+ (BOOL)myZipNumberValidate:(NSString*)number{
    NSString *numberRegEx = @"[0-9]{5}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    return [numberTest evaluateWithObject:number];
    
}

#pragma mark - Check Null Values

+ (NSArray *)checkForNullValuesInArray:(NSArray *)array {
    //---- to check whether json response contains null values ----
    
    NSMutableArray *arrFinal =[[NSMutableArray alloc] init];
    for (NSDictionary *dict in array){
        [arrFinal addObject:[self checkForNullValuesInDict:dict]];
    }
    return arrFinal;
}

+ (NSDictionary *)checkForNullValuesInDict:(NSDictionary *)aDictionary {
    //---- to check whether json response contains null values ----
    
    NSMutableDictionary *compactDictionary = [aDictionary mutableCopy];
    for (NSString *key in [aDictionary allKeys]){
        if ([aDictionary[key] isKindOfClass:[NSNull class]]){
            [compactDictionary setValue:@"" forKey:key];
        }
        else if ([aDictionary[key] isKindOfClass:[NSDictionary class]]){
            [compactDictionary setObject:[self checkForNullValuesInDict:aDictionary[key]] forKey:key];
        }
    }
    return compactDictionary;
}
+(NSString *)getStringObject:(id)strString{
    if([strString isKindOfClass:[NSNull class]]){
        return @"";
    }else if (!strString){
        return @"";
    }
    
    return [NSString stringWithFormat:@"%@",strString];
}

+(NSString *)getNumberStringObject:(id)strString{
    if([strString isKindOfClass:[NSNull class]]){
        return @"";
    }else if (!strString){
        return @"";
    }
    
    if([strString isKindOfClass:[NSNumber class]]){
        if([strString floatValue] == 0){
            return @"";
        }
    }
    
    return [NSString stringWithFormat:@"%@",strString];
}

+(CGFloat)checkimagehieght{
    CGFloat wightofView;
    if (IS_IPHONE4){
        wightofView=[UIScreen mainScreen].bounds.size.width-100;
    }
    else if (IS_IPHONE5)
    {
    wightofView=[UIScreen mainScreen].bounds.size.width-100;
    }
    else if (IS_IPHONE6)
    {
    wightofView=[UIScreen mainScreen].bounds.size.width-180;
    }
    else{
        wightofView=[UIScreen mainScreen].bounds.size.width-200;
    }
    return wightofView;
}

+(UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}

+(NSAttributedString *) customizeString :(NSString *) str range:(NSInteger)substrrange
{
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:str];
    UIColor *color=UIColorFromRGB(0x1AA8EC);
       [attString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(substrrange, [str length]-substrrange)];
    return attString;
}

+(NSAttributedString *) customizeString :(NSString *) str range:(NSInteger)substrrange rangeOne:(NSInteger)substrrangeOne
{
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:str];
    //UIColor *color= [UIColor colorWithRed:0.1412 green:0.3608 blue:0.6510 alpha:1.0];
    UIColor *color= UIColorFromRGB(0x00BAF5);
    NSDictionary *attrDict = @{
                               NSForegroundColorAttributeName : color,
                               NSFontAttributeName : [UIFont fontWithName:@"Avenir-Roman" size:14]
                               };
    [attString addAttributes:attrDict range:NSMakeRange(substrrange, substrrangeOne)];
    return attString;
}

+(BOOL)validateArray:(NSMutableArray*)messageArray{
    
    if ([messageArray isKindOfClass:[NSArray class]]) {
        if ([messageArray count]>0) {
            return YES;
        }
        else{
            return NO;
  
        }
    }
    else{
        return NO;
    }
    
}

+(NSString *)getCurrentDateTime:(NSString *)IN_strLocalTime
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss.SSS a"];
    NSDate  *objDate    = [dateFormatter dateFromString:IN_strLocalTime];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *CurrentTimestamp   = [dateFormatter stringFromDate:objDate];
    return CurrentTimestamp;
    
    //    NSString *CurrentTimestamp=[dateFormatter stringFromDate:[NSDate date]];
    //    return CurrentTimestamp;
}

+(NSString *)getCurrentDate:(NSString *)IN_strLocalTime
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate  *objDate    = [dateFormatter dateFromString:IN_strLocalTime];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *CurrentTimestamp   = [dateFormatter stringFromDate:objDate];
    return CurrentTimestamp;
    
    
    
}


+(NSString *)getCurrentDateTime
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss.SSS a"];
    NSString *CurrentTimestamp=[dateFormatter stringFromDate:[NSDate date]];
    return CurrentTimestamp;
}

+(NSString *)getLoginCurrentDateTime
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss"];
    NSString *CurrentTimestamp=[dateFormatter stringFromDate:[NSDate date]];
    return CurrentTimestamp;
}


+(NSString *)getCurrentDate
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *CurrentTimestamp=[dateFormatter stringFromDate:[NSDate date]];
    return CurrentTimestamp;
}

+ (NSString*)createTimeStamp {
    //    UTC Date
    NSDate *today = [NSDate dateWithTimeIntervalSinceNow:0];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *dataFormatStr=[NSString stringWithFormat:@"yyyy/MM/dd HH:mm:ss a"];
    [dateFormat setDateFormat:dataFormatStr];
    NSString *dateString = [dateFormat stringFromDate:today];
    NSString *timestamp = [NSString stringWithFormat:@"%@",dateString];
    return timestamp;
}
+ (NSString*)logoutsession:(NSString*)dateStr {
    //    UTC Date
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:@"HH:mm:ss"];
    NSDate *dateString = [formatter dateFromString:dateStr];
    NSString *stringFromDate = [formatter stringFromDate:dateString];
    NSString *timestamp = [NSString stringWithFormat:@"%@",stringFromDate];
    return timestamp;
}

+ (NSString*)birthdate:(NSString*)dateStr {
    //    UTC Date
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:@"yyyy/mm/dd"];
    NSDate *dateString = [formatter dateFromString:dateStr];
    NSString *stringFromDate = [formatter stringFromDate:dateString];
    NSString *timestamp = [NSString stringWithFormat:@"%@",stringFromDate];
    return timestamp;
}

+(NSString*)isStrEmpty:(NSString *) str {
    
    if([str isKindOfClass:[NSNumber class]])
    {
        return str;
    }
    if([str isKindOfClass:[NSNull class]] || str==nil)
    {
        return @"";
    }
    if([str length] == 0) {
        
        return @"";
    }
    if(![[str stringByTrimmingCharactersInSet:[NSCharacterSet
                                               whitespaceAndNewlineCharacterSet]] length]) {
        return @"";
    }
    if([str isEqualToString:@"(null)"])
    {
        return @"";
    }
    if([str isEqualToString:@"<null>"])
    {
        return @"";
    }
    return str;
}

+(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    NSDateComponents *components;
    NSDate *date;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: startDate toDate: endDate options: 0];
    date=[[NSCalendar currentCalendar] dateFromComponents:components];
    //    UTC Date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormat setDateFormat:@"hh:mm:ss"];
    NSString *dateString = [dateFormat stringFromDate:date];
    NSString *timestamp = [NSString stringWithFormat:@"%@",dateString];
    return timestamp;
    
}
-(id)isStrEmpty:(NSString *) str {
    
    if([str isKindOfClass:[NSNumber class]])
    {
        return str;
    }
    if([str isKindOfClass:[NSNull class]] || str==nil)
    {
        return @"";
    }
    if([str length] == 0) {
        
        return @"";
    }
    if(![[str stringByTrimmingCharactersInSet:[NSCharacterSet
                                               whitespaceAndNewlineCharacterSet]] length]) {
        return @"";
    }
    if([str isEqualToString:@"(null)"])
    {
        return @"";
    }
    if([str isEqualToString:@"<null>"])
    {
        return @"";
    }
    return str;
}

@end
