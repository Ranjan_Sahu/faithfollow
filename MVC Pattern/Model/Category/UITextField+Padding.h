
#import <UIKit/UIKit.h>

@interface UITextField (Padding)

- (void)setPadding:(float)width withImage:(UIImage*)image;
- (void)setOnlyPadding:(float)width withPlaceHolderColor:(UIColor*)color;

@end
