
#import "UITextField+Padding.h"

@implementation UITextField (Padding)

- (void)setPadding:(float)width withImage:(UIImage *)image{
    UIImageView *paddingView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, self.frame.size.height)];
    paddingView.image = image;
    paddingView.contentMode = UIViewContentModeCenter;
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
    self.tintColor = [UIColor whiteColor];
    [self setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)setOnlyPadding:(float)width withPlaceHolderColor:(UIColor *)color{
    UIImageView *paddingView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, self.frame.size.height)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
    self.tintColor = [UIColor blackColor];
    [self setValue:color forKeyPath:@"_placeholderLabel.textColor"];
}

@end
