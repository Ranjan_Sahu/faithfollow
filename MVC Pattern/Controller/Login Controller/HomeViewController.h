//
//  SkipViewController.h
//  wireFrameSplash
//
//  Created by home on 4/1/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController: UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *loginScrenImage;


@property (strong, nonatomic) IBOutlet UIButton *mainview_signbtn;
@property (strong, nonatomic) IBOutlet UIButton *mainview_loginbtn;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_bg;


@property (weak, nonatomic) IBOutlet UIView *animateloginView;


- (IBAction)loginAct:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomconstraint;
@property (weak, nonatomic) IBOutlet UITextField *emailTxtfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxtfield;

- (IBAction)tapAct:(id)sender;
- (IBAction)submit_Act:(id)sender;

- (IBAction)signup_act:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightofview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainviewHeght;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logintopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailtopconstraints;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordTopConstra;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnView_heightConstraints;
- (IBAction)btn_forget:(id)sender;



@end
