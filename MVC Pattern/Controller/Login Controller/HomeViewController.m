//
//  SkipViewController.m
//  wireFrameSplash
//
//  Created by home on 4/1/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "HomeViewController.h"
#import "UITextField+Padding.h"
#import "CustomPagerViewController.h"
#import "ForgotViewController.h"
#import "MHTabBarController.h"
#import "Mixpanel.h"
@interface HomeViewController ()<UITextFieldDelegate>{
    UIButton *showPasswordBtn;
    NSMutableArray *    arrFeeds;
}
@end
@implementation HomeViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView *userImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user-1"]];
    userImage.frame = CGRectMake(0, 0, userImage.image.size.width+20.0, userImage.image.size.height);
    userImage.contentMode = UIViewContentModeCenter;
    self.emailTxtfield.leftViewMode = UITextFieldViewModeAlways;
    self.emailTxtfield.leftView = userImage;
    UIImageView *passwordImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password"]];
    passwordImage.frame = CGRectMake(0, 0, passwordImage.image.size.width+20.0, passwordImage.image.size.height);
    passwordImage.contentMode = UIViewContentModeCenter;
    self.passwordTxtfield.leftViewMode = UITextFieldViewModeAlways;
    self.passwordTxtfield.leftView = passwordImage;
    _emailTxtfield.delegate=self;
    _passwordTxtfield.delegate=self;
    showPasswordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [showPasswordBtn addTarget:self
                        action:@selector(showPassword)
              forControlEvents:UIControlEventTouchUpInside];
    [showPasswordBtn setImage:[UIImage imageNamed:@"visible_icon"] forState:UIControlStateNormal];
    [showPasswordBtn setImage:[UIImage imageNamed:@"visible_icon"] forState:UIControlStateSelected];
    showPasswordBtn.contentMode = UIViewContentModeCenter;
    showPasswordBtn.frame =  CGRectMake(_passwordTxtfield.frame.size.width+20, 0, passwordImage.image.size.width+10.0, passwordImage.image.size.height+5.0);
    [showPasswordBtn addTarget:self action:@selector(showPasswordBTN) forControlEvents:UIControlEventTouchUpInside];
    self.passwordTxtfield.rightViewMode = UITextFieldViewModeAlways;
    self.passwordTxtfield.rightView = showPasswordBtn;
    showPasswordBtn.selected=false;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    //self.navigationController.navigationBar.hidden=YES;
    //  self.navigationController.navigationBarHidden=YES;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    if (IS_IPHONE6PLUS){
        _heightofview.constant=150;
        _mainviewHeght.constant=300;
        _emailtopconstraints.constant=30;
        _logintopConstraint.constant=25;
        _passwordTopConstra.constant=15;
    }
    else  if (IS_IPHONE4){
        //_heightofview.constant=150;
        _emailtopconstraints.constant=5;
        _mainviewHeght.constant=240;
        _btnView_heightConstraints.constant=57;
    }
    self.navigationController.navigationBarHidden=YES;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    _mainview_loginbtn.layer.borderColor=[UIColor whiteColor].CGColor;
    _mainview_loginbtn.layer.borderWidth=2.5;
    _mainview_signbtn.layer.borderColor=[UIColor whiteColor].CGColor;
    _mainview_signbtn.layer.borderWidth=2.5;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAct:)];
    [self.imageview_bg addGestureRecognizer:tapGestureRecognizer];
    _imageview_bg.userInteractionEnabled=YES;
    _imageview_bg.multipleTouchEnabled=YES;
    _emailTxtfield.layer.borderWidth=1.0f;
    _passwordTxtfield.layer.borderWidth=1.0f;
    _emailTxtfield .layer.borderColor=[UIColor blackColor].CGColor;
    _passwordTxtfield .layer.borderColor=[UIColor blackColor].CGColor;
    _emailTxtfield.keyboardType=UIKeyboardTypeEmailAddress;
    [self loginYes];
    
}
- (void) viewWillAppear:(BOOL)animated
{
    _emailTxtfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [super viewWillAppear:animated];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    _bottomconstraint.constant=-screenRect.size.height;
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}
-(void)showPasswordBTN{
    
    if (showPasswordBtn.selected) {
        self.passwordTxtfield.secureTextEntry = NO;
    }
    else
    {
        self.passwordTxtfield.secureTextEntry = YES;
    }
}
-(void)showPassword{
    if (showPasswordBtn.selected) {
        _passwordTxtfield.secureTextEntry=YES;
        showPasswordBtn.selected=false;
    }else{
        _passwordTxtfield.secureTextEntry=NO;
        showPasswordBtn.selected=true;
    }
}
-(void)loginYes {
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"Login"]isEqualToString:@"yes"]) {
        MHTabBarController *homescreen=[self.storyboard instantiateViewControllerWithIdentifier:@"MHTabBarController"];
        [self.navigationController pushViewController:homescreen animated:YES];
    }
}
- (void)keyboardWillHide:(NSNotification *)n
{_bottomconstraint.constant=0;
    [self.view layoutIfNeeded];
}
- (void)keyboardWillShow:(NSNotification *)n
{    NSDictionary* userInfo = [n userInfo];
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    _bottomconstraint.constant=keyboardSize.height-20;
    [self.view layoutIfNeeded];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    //Keyboard becomes visible
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    //keyboard will hide
}
#pragma mark - Validation
- (NSString *)validateFields {
    NSString *errorMessage ;
    NSString *strEmail = [self.emailTxtfield.text removeWhiteSpaces];
    NSString *strPassword = [self.passwordTxtfield.text removeWhiteSpaces];
    if ([strEmail length] == 0) {
        errorMessage =EnterEmail;
        return errorMessage;
    }
    BOOL isEmailvalid = [Constant1 validateEmail:strEmail];
    if (!isEmailvalid) {
        errorMessage =InvalidEmail;
        return errorMessage;
    }
    if ([strPassword length] == 0) {
        errorMessage = EnterPassword;
        return errorMessage;
    }
    return 0;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)loginAct:(id)sender {

    
    
    [self animateView:0];
}
- (IBAction)tapAct:(id)sender {
    [self.view endEditing:YES];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    [self animateView:-screenRect.size.height];
}

- (IBAction)submit_Act:(id)sender {
    
    NSString *deviceToken=    [[NSUserDefaults standardUserDefaults]  objectForKey:@"apnsToken"];
    if (deviceToken.length==0) {
        deviceToken=@"32e06ecb6890317a192ad121e17ea9635d6d7ee16a5d6a8e550736a9a3d89015";
         [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"apnsToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        NSString *errorMessage = [self validateFields];
        if (errorMessage) {
            [[[UIAlertView alloc] initWithTitle:appTitle message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
            return;
        }else{
            NSDictionary *dict = @{
                                   @"email" :self.emailTxtfield.text,
                                   @"password" : self.passwordTxtfield.text,
                                   @"device_token":deviceToken,
                                   @"device_type":@"IOS",
                                   };
//             NSLog(@"dict %@",dict);
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:login withParameters:dict withHud:YES success:^(id response){
//                NSLog(@"res %@",response);
                if ([response isKindOfClass:[NSDictionary class]]){
                    if (![response isKindOfClass:[NSNull class]]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if([response[@"status"]intValue ] ==1){
                                [[NSUserDefaults standardUserDefaults]setObject:response[@"api_token"] forKey:@"api_token"];
                                [[NSUserDefaults standardUserDefaults]setObject:response[@"id"] forKey:@"userid"];
                                AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                                [appDelegate notification_alert];
                                 [[NSUserDefaults standardUserDefaults]setObject:[response objectForKey:@"profile_picture"]forKey:@"profile_picture"];
                                [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                [appDelegate setRoots];
                                
                                MHTabBarController *homescreen=[self.storyboard instantiateViewControllerWithIdentifier:@"MHTabBarController"];
                                [self.navigationController pushViewController:homescreen animated:YES];
                            }else{
                                NSMutableArray *messageArray=nil;
                                messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            }
                        });
                    }
                }
            } errorBlock:^(id error)
             {
                [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//                 NSLog(@"error is %@",error);
             }];
        }
    }
}
- (IBAction)signup_act:(id)sender {
    CustomPagerViewController *signup=[self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
    [TenjinSDK sendEventWithName:@"SignUp_Tapped"];
     [FBSDKAppEvents logEvent:@"SignUp_Tapped"];
//     Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"SignUp_Tapped"];
    // Urban airship event fired
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"SignUp_Tapped"]];
    
    
    [self.navigationController pushViewController:signup animated:YES];
}
-(void)animateView:(CGFloat)constrantsConstant{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _bottomconstraint.constant=constrantsConstant;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                     }];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
- (IBAction)btn_forget:(id)sender {
    ForgotViewController *forget=[self.storyboard instantiateViewControllerWithIdentifier:@"ForgotViewController"];
    [self.navigationController pushViewController:forget animated:YES];
}
@end
