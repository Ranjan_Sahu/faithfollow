//
//  SignupViewController.h
//  wireFrameSplash
//
//  Created by home on 8/17/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *signup_profile_pic;
@property (weak, nonatomic) IBOutlet UILabel *FaithfollowLBL;
@property (weak, nonatomic) IBOutlet UIButton *signupBTN;
@property (weak, nonatomic) IBOutlet UIButton *facebookBTN;

@property (weak, nonatomic) IBOutlet UIButton *alreadyBTN;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fromtoprofile_pic;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logowidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logobetweenconnectHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *connectbetweensignupHeight;

// view controller

@property (weak, nonatomic) IBOutlet UIView *viewLogin;

@property (weak, nonatomic) IBOutlet UITextField *emialpop;

@property (weak, nonatomic) IBOutlet UITextField *passwordpop;

@property (weak, nonatomic) IBOutlet UIButton *loginpopMethod;

@property (weak, nonatomic) IBOutlet UIView *signUpview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonVerticalHieght;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *popViewHeigh;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *poviewWidth;







//Reset Textfield



@property (weak, nonatomic) IBOutlet UIButton *ClicktosignBTN;
- (IBAction)alreadyMethod:(id)sender;
- (IBAction)clicktosignmethod:(id)sender;
- (IBAction)facebookMethod:(id)sender;
- (IBAction)signupMethod:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *emialAddressTXT;

@property (weak, nonatomic) IBOutlet UITextField *passwordTXT;
- (IBAction)fogotpasswordMethod:(id)sender;
- (IBAction)loginmethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *closeloginMethod;
@property (weak, nonatomic) IBOutlet UITextView *attributedLinkTXt;
@property (weak, nonatomic) IBOutlet UILabel *lblAlready;

- (IBAction)closepopView:(id)sender;

- (IBAction)clicktosignaleardyMethod:(id)sender;

@end
