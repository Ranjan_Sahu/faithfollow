//
//  LoginViewController.h
//  wireFrameSplash
//
//  Created by home on 8/17/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTxtfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxtfield;
@property (weak, nonatomic) IBOutlet UIView *loginView;
@property(weak,nonatomic)IBOutlet UIImageView *imageProfilePop;
@property(strong,nonatomic)NSString *imagestrURl;

-(IBAction)loginMethod:(id)sender;
-(IBAction)forgotMethod:(id)sender;
@end
