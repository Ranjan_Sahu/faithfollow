//
//  SignupViewController.m
//  wireFrameSplash
//
//  Created by home on 8/17/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "SignupViewController.h"
#import "DeviceConstant.h"
#import "CustomPagerViewController.h"
#import "LoginViewController.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "CongratulationViewController.h"
#import "MHTabBarController.h"
#import "NeosPopup.h"
#import "ForgotViewController.h"
#import "Constant1.h"
#import "LoginViewController.h"
#import "KLCPopup.h"
#import "TutorialViewController.h"
#import "newSignUpViewController.h"
#import "Mixpanel.h"
@interface SignupViewController ()<UITextFieldDelegate>
{
    NeosPopup *nepopuphome;
    UIButton *showPasswordBtn;
    KLCPopup *popview;
}
@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deviceToken:)
                                                 name:@"deviceToken"
                                               object:nil];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    _viewLogin.layer.cornerRadius=8.0f;
    [_viewLogin.layer setMasksToBounds:YES];
    
    _emialAddressTXT.text=@"";
    _passwordTXT.text=@"";
    
    [[AppDelegate sharedAppDelegate] removeadd];
    
    self.signup_profile_pic.clipsToBounds = YES;
    self.signup_profile_pic.layer.cornerRadius=8.0f;
    
    
    if (IS_IPHONE4) {
        _connectbetweensignupHeight.constant=20;
        _logobetweenconnectHeight.constant=20;
        _fromtoprofile_pic.constant=50;
        _buttonVerticalHieght.constant=15;
    }
    if (IS_IPHONE5) {
        _logoheight.constant=80;
        _logowidth.constant=80;
        _fromtoprofile_pic.constant=100;
        _logobetweenconnectHeight.constant=10;
        _buttonVerticalHieght.constant=20;
        //[[self lblAlready] setFont:[UIFont boldSystemFontOfSize:18]];
        _connectbetweensignupHeight.constant=30;
    }
    if (IS_IPHONE6) {
        _logoheight.constant=90;
        _logowidth.constant=90;
        _fromtoprofile_pic.constant=110;
        _logobetweenconnectHeight.constant=30;
        _connectbetweensignupHeight.constant=30;
        _buttonVerticalHieght.constant=20;
        //[[self lblAlready] setFont:[UIFont boldSystemFontOfSize:19]];
    }
    
    if (IS_IPHONE6PLUS) {
        _logowidth.constant=120;
        _logoheight.constant=120;
        _fromtoprofile_pic.constant=140;
        _logobetweenconnectHeight.constant=20;
        _connectbetweensignupHeight.constant=30;
        _buttonVerticalHieght.constant=20;
        //[[self lblAlready] setFont:[UIFont boldSystemFontOfSize:19]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
//    NSAttributedString *attributedString=[Constant1 customizeString:@"Already Registered? Sign In"  range:18];
//    _lblAlready.attributedText=attributedString;
    
    _facebookBTN.layer.borderWidth = 1.0;
    _facebookBTN.layer.borderColor = [UIColor colorWithRed:214.0/255.0 green:221.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    _emialAddressTXT.layer.borderWidth = 1.0;
    _emialAddressTXT.layer.borderColor = [UIColor colorWithRed:214.0/255.0 green:221.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    _passwordTXT.layer.borderWidth = 1.0;
    _passwordTXT.layer.borderColor = [UIColor colorWithRed:214.0/255.0 green:221.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    
}

-(void)deviceToken:(NSNotification *) notification{
    
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *at=[[UIAlertView alloc]initWithTitle:@"device" message:[userInfo valueForKey:@"tokenDevice"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [at show];
        
    });
    
}



-(void)keyboardWillShow:(NSNotification*)notification
{
    CGRect frame = popview.frame;
    frame.origin.y = -110;
    [popview setFrame:frame];
}
-(void)keyboardWillHide:(NSNotification*)notification
{
    CGRect frame = popview.frame;
    frame.origin.y = 0;
    [popview setFrame:frame];
}


-(void)viewWillDisappear:(BOOL)animated
{    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (_emialAddressTXT==textField) {
        _emialAddressTXT.autocapitalizationType = UITextAutocapitalizationTypeNone;
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (_passwordTXT==textField) {
        [_passwordTXT resignFirstResponder];
        
    }
    return YES;
}




-(void)cancel_popup{
    [self.view endEditing:YES];
    [nepopuphome dismiss];
}

-(void)showPassword123{
    if (showPasswordBtn.selected) {
        _passwordTXT.secureTextEntry=YES;
        showPasswordBtn.selected=false;
    }else{
        _passwordTXT.secureTextEntry=NO;
        showPasswordBtn.selected=true;
    }
}

-(void)forgotMethodpopView{
    
}


#pragma mark - Validation
- (NSString *)validateFields {
    NSString *errorMessage ;
    NSString *strEmail = [self.emialAddressTXT.text removeWhiteSpaces];
    NSString *strPassword = [self.passwordTXT.text removeWhiteSpaces];
    if ([strEmail length] == 0) {
        errorMessage =EnterEmail;
        return errorMessage;
    }
    BOOL isEmailvalid = [Constant1 validateEmail:strEmail];
    if (!isEmailvalid) {
        errorMessage =InvalidEmail;
        return errorMessage;
    }
    if ([strPassword length] == 0) {
        errorMessage = EnterPassword;
        return errorMessage;
    }
    return 0;
}




- (IBAction)facebookMethod:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (!app.fbManager) {
            app.fbManager = [[FBSDKLoginManager alloc] init];
        }

        [app.fbManager logOut];
        [app.fbManager setLoginBehavior:FBSDKLoginBehaviorNative];
        [app.fbManager logInWithReadPermissions:@[@"public_profile", @"email",@"user_birthday"] fromViewController:self  handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error)
            {
                [[AppDelegate sharedAppDelegate] hidehud];
                [self.view makeToast:@"Seems there is some problem ,please try after some time "  duration:2.0 position:CSToastPositionCenter];
            }
            else if (result.isCancelled)
            {
                [[AppDelegate sharedAppDelegate] hidehud];
            }
            else
            {    [[AppDelegate sharedAppDelegate] showhud];
                if ([result.grantedPermissions containsObject:@"email"])
                {
                    [self retriveUserData];
                }
            }
        }];
    }
    
    //vikas
    //    TutorialViewController *tut=[[TutorialViewController alloc]initWithNibName:@"TutorialViewController" bundle:nil];
    //    tut.isFromFacebook = YES;
    //    [self.navigationController pushViewController:tut animated:YES];
}



-(void)retriveUserData
{
    NSMutableArray *SelectedArray = [[NSMutableArray alloc]init];
//    NSArray *array=[[NSArray alloc]initWithObjects:@"picture.width(960).height(1704)" ,@"email", @"first_name", @"last_name" ,@"gender",@"about",@"birthday", @"bio", @"verified",@"age_range", nil];
    NSArray *array=[[NSArray alloc]initWithObjects:@"picture.width(960).height(1704)" ,@"email", @"first_name", @"last_name" ,@"gender",@"about",@"birthday", @"verified",@"age_range", nil];
    [SelectedArray addObjectsFromArray:array];
    
    NSMutableString *strFinal=[[NSMutableString alloc]initWithString:@"id"];
    
    for (NSString *str in SelectedArray)
    {
        strFinal = [NSMutableString stringWithFormat:@"%@,%@",strFinal,str];
    }
    if ([FBSDKAccessToken currentAccessToken])
    {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": strFinal}]
         
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             if (!error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate sharedAppDelegate] hidehud];
                     [self getFacebookProfileInfo:result];
                 });
             }
             else
             {
                 [[AppDelegate sharedAppDelegate] hidehud];
                 [self.view makeToast:@"Seems there is some problem ,please try after some time "  duration:2.0 position:CSToastPositionCenter];
                 
                 //                 NSLog(@"error %@",error.description);
             }
         }];
    }
}

-(NSString*)base64String:(UIImage*)image{
    NSString *base64EncodeStr = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    base64EncodeStr = [base64EncodeStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    return base64EncodeStr;
}


-(void)getFacebookProfileInfo:(NSDictionary *)dict
{
    //
    NSString *deviceToken=    [[NSUserDefaults standardUserDefaults]  objectForKey:@"apnsToken"];
    if (deviceToken.length==0) {
        deviceToken=@"32e06ecb6890317a192ad121e17ea9635d6d7ee16a5d6a8e550736a9a3d89015";
        [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"apnsToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    //
    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"fbDictionay"];
    [[NSUserDefaults standardUserDefaults] setObject:@"fbvalue" forKey:@"fbvalue"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //    NSLog(@"dict %@",dict);
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *firstname=[dict valueForKey:@"first_name"];
        if ([firstname isEqualToString:@""]) {
            firstname=@"";
        }
        else{
            firstname=[dict valueForKey:@"first_name"];
        }
        
        NSString *last_name=[dict valueForKey:@"last_name"];
        if ([last_name isEqualToString:@""]) {
            last_name=@"";
        }
        else{
            last_name=[dict valueForKey:@"last_name"];
        }
        
      
        
        NSString *emailid;
        if ([dict objectForKey:@"email"] && ![[dict objectForKey:@"email"] isKindOfClass:[NSNull class]])
        {
            emailid=[dict valueForKey:@"email"];
            [[NSUserDefaults standardUserDefaults]setObject:emailid forKey:@"emailID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            emailid = @"";
            [[NSUserDefaults standardUserDefaults]setObject:emailid forKey:@"emailID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
       
        
        
        
        NSString *dob,*completeDOB;
        NSDate *dtPostDate;
        if ([dict objectForKey:@"birthday"] && ![[dict objectForKey:@"birthday"] isKindOfClass:[NSNull class]])
        {
            dob=[dict valueForKey:@"birthday"];
            NSString *yearValue = [dob componentsSeparatedByString:@"/"][2];
            NSString *monthValue = [dob componentsSeparatedByString:@"/"][0];
            NSString *dayValue = [dob componentsSeparatedByString:@"/"][1];
            completeDOB=[NSString stringWithFormat:@"%@/%@/%@",yearValue,monthValue,dayValue];
            NSDateFormatter *df1 = [[NSDateFormatter alloc] init] ;
            [df1 setDateFormat:@"yyyy/mm/dd"];
            dtPostDate = [df1 dateFromString:completeDOB];
            NSLog(@"complete date %@",completeDOB);
            
        }
        else
        {
            dob = @"";
        }
        
        NSString *gendercheck;
        NSString *gender;
        
        gender=[dict valueForKey:@"gender"];
        if ([gender isEqualToString:@""]) {
//            gender=@"";
//  memory leak
        }
        else{
            gender=[[dict valueForKey:@"gender"] capitalizedString];
            if ([gender isEqualToString:@"Male"]) {
                gendercheck=@"1";
            }
            else{
                gendercheck=@"0";
            }
            
            
        }
        
        NSString *facebookId=[dict valueForKey:@"id"];
        
        if ([facebookId isEqualToString:@""]) {
            facebookId=@"";
        }
        else{
            facebookId=[dict valueForKey:@"id"];
        }
        NSDictionary *profile_pic=[dict valueForKey:@"picture"];
        NSString *imageString;
        NSString  *imageprofileURL;
        UIImage *img;
        if([profile_pic isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dataDict=[profile_pic valueForKey:@"data"];
            if ([dataDict isKindOfClass:[NSDictionary class]]) {
                imageprofileURL=[dataDict valueForKey:@"url"];
                NSURL *tempURL = [NSURL URLWithString:imageprofileURL];
                NSData *tempData = [NSData dataWithContentsOfURL:tempURL];
                img =[[UIImage alloc] initWithData:tempData];
                if (img!=nil) {
                    img=[Constant1 compressImage:img];
                    imageString=[self base64String:img];
                }
                else{
                    imageString=@"";
                }
            }
        }
        
        
        if ([firstname isEqualToString:@""]||[last_name isEqualToString:@""]||[emailid isEqualToString:@""]) {
            NSDictionary *dictparameter = @{
                              @"first_name" :firstname,
                              @"last_name" :last_name,
                              @"email" :emailid,
                              @"date_of_birth" :dob,
                              @"gender":gendercheck,
                              @"image":imageString,
                              @"facebookId":facebookId,
                              @"password":@"",
                              @"device_type":@"IOS",
                              @"advertise_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"adID"],
                              @"device_token":deviceToken
                              };
            
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:incompleteFbData withParameters:dictparameter withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSString *status=[NSString stringWithFormat:@"%@",[response valueForKey:@"status"]];
                        if ([status isEqualToString:@"0"]) {
                            [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"fbDictionay"];
                            [[NSUserDefaults standardUserDefaults] setObject:@"fbvalue" forKey:@"fbvalue"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [TenjinSDK sendEventWithName:@"signup_with_facebook"];
                                [FBSDKAppEvents logEvent:@"signup_with_facebook"];
                                [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"signup_with_facebook"]];
                                newSignUpViewController *newsignupFb=[self.storyboard instantiateViewControllerWithIdentifier:@"newSignUpViewController"];
                                newsignupFb.emailFBSTR=emailid;
                                newsignupFb.fisrtnameFBSTR=firstname;
                                newsignupFb.secondnameFBSTR=last_name;
                                newsignupFb.imageFBSTR=imageprofileURL;
                                newsignupFb.isFromFacebooknew = YES;
                                [self.navigationController pushViewController:newsignupFb animated:YES];
                            });
                        }
                        else{
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                    NSString *accountCreate=[Constant1 createTimeStamp];
                    NSString *userid=[NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];

                                
                    if ([userid isEqualToString:@"(null)"] || [userid isEqualToString:@""] )
                        {
                            userid=@"";
                            }
                        else{
                            userid=[NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
                                }
                                
                    [[AppDelegate sharedAppDelegate].mxPanel identify:userid];
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    NSData *myEncodedObject = [prefs objectForKey:@"mixdeviceToken"];
                    if (myEncodedObject!=nil) {
                        NSLog(@"mix device token %@",myEncodedObject);
                        [[AppDelegate sharedAppDelegate].mxPanel.people addPushDeviceToken:myEncodedObject];
                        
                    }
                                
                                
                    NSDate *loginDate=[NSDate date];
                    [AppDelegate sharedAppDelegate].logindate=loginDate;
//                    [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Total logins":@""}];
                                
                     [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total logins" by:@1];
                                
                        [[AppDelegate sharedAppDelegate].mxPanel track:@"Login" properties:@{
                                                @"Login method":@"facebook",
                                                @"Last login":accountCreate,
                                        }];
                                
                                [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"api_token"] forKey:@"api_token"];
                                [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"id"] forKey:@"userid"];
                                [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                [TenjinSDK sendEventWithName:@"connect_with_facebook"];
                                [FBSDKAppEvents logEvent:@"connect_with_facebook"];
                                [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"connect_with_facebook"]];
                                [appDelegate setRoots];
                                
                            });
                            
                        }
                        
                    });
                    
                }
                else{
                    [self.view makeToast:@"Seems there is some problem,Please try after some time "  duration:1.0 position:CSToastPositionCenter];
                    
                }
            } errorBlock:^(id error)
             {
                 //             NSLog(@"service error");
             }];
            
        }
        else{
            // direct login found all data
            NSDictionary *dictparameter;
            dictparameter = @{
                              @"first_name" :firstname,
                              @"last_name" :last_name,
                              @"email" :emailid,
                              @"date_of_birth" :dob,
                              @"gender":gendercheck,
                              @"image":imageString,
                              @"facebookId":facebookId,
                              @"password":@"",
                              @"device_type":@"IOS",
                              @"advertise_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"adID"],
                              @"device_token":deviceToken
                              };
            
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:signupfb withParameters:dictparameter withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                    NSString *userid=[NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];

                        [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"api_token"] forKey:@"api_token"];
                        [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"id"] forKey:@"userid"];
                        [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        NSString *is_registersign=[NSString stringWithFormat:@"%@",[response valueForKey:@"is_register"]];
                        
                        if ([is_registersign isEqualToString:@"1"]) {
                            
                            // mix panel
                            NSString *genderFB;
                            if ([gendercheck isEqualToString:@"1"]) {
                                genderFB=@"M";
                            }
                            else{
                                genderFB=@"F";

                            }
                            
                            
                            if ([dob isEqualToString:@""]) {
                                NSString *accountCreate=[Constant1 createTimeStamp];
                                NSDate *loginDate=[NSDate date];
                                [AppDelegate sharedAppDelegate].logindate=loginDate;
                                [[AppDelegate sharedAppDelegate].mxPanel identify:userid];
                                
                                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                NSData *myEncodedObject = [prefs objectForKey:@"mixdeviceToken"];
                                if (myEncodedObject!=nil) {
                                    NSLog(@"mix device token %@",myEncodedObject);
                                    [[AppDelegate sharedAppDelegate].mxPanel.people addPushDeviceToken:myEncodedObject];
                                    
                                }
                                
                                
                                
                                
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total logins" by:@1];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Email":emailid,@"Registration method":@"facebook",@"Gender":genderFB,@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"$first_name":firstname,@"$last_name":last_name,@"Last Profile Update":accountCreate}];
                                
                                
                                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"$email":emailid,@"Registration method":@"facebook",@"gender":genderFB,@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"$first_name":firstname,@"$last_name":last_name,@"Last Profile Update":accountCreate}];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel track:@"Sign up"
                                properties:@{@"Email":emailid,@"Registration method":@"facebook",@"Gender":genderFB,@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"Last Profile Update":accountCreate}];
                            }
                            else{
                                NSString *accountCreate=[Constant1 createTimeStamp];
                                NSDate *loginDate=[NSDate date];
                                [AppDelegate sharedAppDelegate].logindate=loginDate;
                                [[AppDelegate sharedAppDelegate].mxPanel identify:userid];
                                
                                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                NSData *myEncodedObject = [prefs objectForKey:@"mixdeviceToken"];
                                if (myEncodedObject!=nil) {
                                    NSLog(@"mix device token %@",myEncodedObject);
                                    [[AppDelegate sharedAppDelegate].mxPanel.people addPushDeviceToken:myEncodedObject];
                                    
                                }

                                
                                
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total logins" by:@1];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Email":emailid,@"Registration method":@"facebook",@"Gender":genderFB,@"Birthdate":dtPostDate,@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"$first_name":firstname,@"$last_name":last_name,@"Last Profile Update":accountCreate}];
                                
                                
                                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"$email":emailid,@"Registration method":@"facebook",@"gender":genderFB,@"Birthdate":dtPostDate,@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"$first_name":firstname,@"$last_name":last_name,@"Last Profile Update":accountCreate}];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel track:@"Sign up"
                                                properties:@{@"Email":emailid,@"Registration method":@"facebook",@"Gender":genderFB,@"Birthdate":dtPostDate,@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"Last Profile Update":accountCreate}];
                            }
                            
                            [TenjinSDK sendEventWithName:@"signup_with_facebook"];
                            [FBSDKAppEvents logEvent:@"signup_with_facebook"];
                            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"signup_with_facebook"]];
                            [TenjinSDK sendEventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [FBSDKAppEvents logEvent:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            
                            UACustomEvent *event = [UACustomEvent eventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [[UAirship shared].analytics addEvent:event];
                            TutorialViewController *tut=[[TutorialViewController alloc]initWithNibName:@"TutorialViewController" bundle:nil];
                            tut.isFromFacebook = NO;
                            [self.navigationController pushViewController:tut animated:YES];
                            
                        }
                        else{
                            
                            NSString *accountCreate=[Constant1 createTimeStamp];
                            if ([userid isEqualToString:@"(null)"] || [userid isEqualToString:@""] )
                            {
                                userid=@"";
                            }
                            else{
                                userid=[NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
                            }
                            
                            NSString *loginCount=[NSString stringWithFormat:@"%@",[response objectForKey:@"login_count"]];
                            
                            [[AppDelegate sharedAppDelegate].mxPanel identify:userid];
                            
                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                            NSData *myEncodedObject = [prefs objectForKey:@"mixdeviceToken"];
                            if (myEncodedObject!=nil) {
                                NSLog(@"mix device token %@",myEncodedObject);
                                [[AppDelegate sharedAppDelegate].mxPanel.people addPushDeviceToken:myEncodedObject];
                                
                            }

                            
                            NSDate *loginDate=[NSDate date];
                            [AppDelegate sharedAppDelegate].logindate=loginDate;
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total logins" by:@1];
                            
                            //                        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Total logins":loginCount}];
                            [[AppDelegate sharedAppDelegate].mxPanel.people set:@{
                                                                                  @"Total logins":loginCount,
                                                                                  @"Last login":accountCreate}];
                            
                            [[AppDelegate sharedAppDelegate].mxPanel track:@"Login" properties:@{
                                                                                                 @"Login method":@"facebook",
                                                                                                 @"Last login":accountCreate,
                                                                                                 }];
                            [TenjinSDK sendEventWithName:@"connect_with_facebook"];
                            [FBSDKAppEvents logEvent:@"connect_with_facebook"];
                            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"connect_with_facebook"]];
                            [appDelegate setRoots];
                            
                        }
                        
                    });
                }
                else{
                    [self.view makeToast:@"Seems there is some problem,Please try after some time "  duration:1.0 position:CSToastPositionCenter];
                    
                }
            } errorBlock:^(NSError *error)
             {
                 NSLog(@"service error %@",error.description);
             }];
            
        }
        
    }
    
}

- (IBAction)signupMethod:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *current=[Constant1 createTimeStamp];
        newSignUpViewController *newsignup=[self.storyboard instantiateViewControllerWithIdentifier:@"newSignUpViewController"];
        [TenjinSDK sendEventWithName:@"SignUp_Tapped"];
        [FBSDKAppEvents logEvent:@"SignUp_Tapped"];
        
        
        [[AppDelegate sharedAppDelegate].mxPanel track:@"Sign Up clicked" properties:@{
                                                @"Signup Type ":@"Standard",
                                                @"Signup Clicked at":current,
                                                    }];
        [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"SignUp_Tapped"]];
        [self.navigationController pushViewController:newsignup animated:YES];
        
    }
    
}

- (IBAction)fogotpasswordMethod:(id)sender {
    
    
    [_emialAddressTXT resignFirstResponder];
    [_passwordTXT resignFirstResponder];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;    }
    else{
        [popview dismiss:YES];
        ForgotViewController *forget=[self.storyboard instantiateViewControllerWithIdentifier:@"ForgotViewController"];
        [self.navigationController pushViewController:forget animated:YES];
    }
    
    
}

- (IBAction)loginmethod:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *deviceToken=    [[NSUserDefaults standardUserDefaults]  objectForKey:@"apnsToken"];
        if (deviceToken.length==0) {
            deviceToken=@"32e06ecb6890317a192ad121e17ea9635d6d7ee16a5d6a8e550736a9a3d89015";
            [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"apnsToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        NSString *errorMessage = [self validateFields];
        if (errorMessage) {
            [[[UIAlertView alloc] initWithTitle:appTitle message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
            return;
        }else{
            NSDictionary *dict = @{
                                   @"email" :self.emialAddressTXT.text,
                                   @"password" : self.passwordTXT.text,
                                   @"device_token":deviceToken,
                                   @"device_type":@"IOS",
                                   @"advertise_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"adID"]
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:login withParameters:dict withHud:YES success:^(id response){
                //                NSLog(@"res %@",response);
                if ([response isKindOfClass:[NSDictionary class]]){
                    if (![response isKindOfClass:[NSNull class]]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if([response[@"status"]intValue ] ==1)  {
                                
                                 [[NSUserDefaults standardUserDefaults]setObject:self.emialAddressTXT.text forKey:@"emailID"];
                                [[NSUserDefaults standardUserDefaults]setObject:response[@"api_token"] forKey:@"api_token"];
                                [[NSUserDefaults standardUserDefaults]setObject:response[@"id"] forKey:@"userid"];
                                [[NSUserDefaults standardUserDefaults]setObject:[response objectForKey:@"profile_picture"]forKey:@"profile_picture"];
                                [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
                                NSString *totalvalueLogin;
                                NSString *checklogintotal=[NSString stringWithFormat:@"%@",[response objectForKey:@"login_count"]];
                                
                                if ([checklogintotal isEqualToString:@"(null)"] || [checklogintotal isEqualToString:@""] )
                                {
                                totalvalueLogin=@"0";
                                }
                            else{
                                totalvalueLogin=[NSString stringWithFormat:@"%@",[response objectForKey:@"login_count"]];
                                
                            }
                            [[NSUserDefaults standardUserDefaults] synchronize];
                                
                            NSString *accountCreate=[Constant1 createTimeStamp];
                            NSDate *loginDate=[NSDate date];
                                [AppDelegate sharedAppDelegate].logindate=loginDate;

                                NSString *userid=[NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
                            if ([userid isEqualToString:@"(null)"] || [userid isEqualToString:@""] )
                                {
                                    userid=@"";
                                }
                                else{
                                    userid=[NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
                                }
                                [[AppDelegate sharedAppDelegate].mxPanel identify:userid];
                                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                NSData *myEncodedObject = [prefs objectForKey:@"mixdeviceToken"];
                                if (myEncodedObject!=nil) {
                                    NSLog(@"mix device token %@",myEncodedObject);
                                    [[AppDelegate sharedAppDelegate].mxPanel.people addPushDeviceToken:myEncodedObject];
                                }
                                
            
                                
                    [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total logins" by:@1];
                    [[AppDelegate sharedAppDelegate].mxPanel track:@"Login" properties:@{
                                       @"Login method":@"standard",
                                       @"Last login":accountCreate,
                                       }];
                                
                                [appDelegate setRoots];
                                
                            }
                            else{
                                NSMutableArray *messageArray=nil;
                                messageArray=[response valueForKey:@"message"];
                                if ([messageArray count]>0) {
                                    UIAlertView *loginAlert=[[UIAlertView alloc]initWithTitle:appTitle message:[messageArray objectAtIndex:0] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] ;
                                    [loginAlert show];
                                }
                            }
                        });
                    }
                }
            } errorBlock:^(NSError *error)
             {
                 //                 NSLog(@"error is %@",error);
             }];
        }
        
    }
    
    
}
- (IBAction)closepopView:(id)sender {
    [_emialAddressTXT resignFirstResponder];
    [_passwordTXT resignFirstResponder];
    [popview dismiss:YES];
}

- (IBAction)clicktosignaleardyMethod:(id)sender {
    
    _emialAddressTXT.text=@"";
    _passwordTXT.text=@"";
    UIImageView *userImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user-1"]];
    userImage.frame = CGRectMake(0, 0, userImage.image.size.width+20.0, userImage.image.size.height);
    userImage.contentMode = UIViewContentModeCenter;
    self.emialAddressTXT.leftViewMode = UITextFieldViewModeAlways;
    self.emialAddressTXT.leftView = userImage;
    _emialAddressTXT.placeholder=@"Email Address";
    _emialAddressTXT.textAlignment=NSTextAlignmentLeft;
    
    UIImageView *passwordImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password"]];
    passwordImage.frame = CGRectMake(0, 0, passwordImage.image.size.width+20.0, passwordImage.image.size.height);
    passwordImage.contentMode = UIViewContentModeCenter;
    self.passwordTXT.leftViewMode = UITextFieldViewModeAlways;
    self.passwordTXT.leftView = passwordImage;
    _passwordTXT.placeholder=@" Password";
    _passwordTXT.textAlignment=NSTextAlignmentLeft;
    [_passwordTXT   setSecureTextEntry:YES];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    showPasswordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [showPasswordBtn setBackgroundImage:[UIImage imageNamed:@"visible_icon"] forState:UIControlStateNormal];
    [showPasswordBtn setImage:[UIImage imageNamed:@"visibleSelected"] forState:UIControlStateSelected];
    showPasswordBtn.contentMode = UIViewContentModeCenter;
    showPasswordBtn.frame =  CGRectMake(0, 5, 24, 20);
    [showPasswordBtn addTarget:self action:@selector(showPassword123) forControlEvents:UIControlEventTouchUpInside];
    showPasswordBtn.selected=false;
    [paddingView addSubview:showPasswordBtn];
    passwordImage.contentMode = UIViewContentModeCenter;
    self.passwordTXT.rightViewMode = UITextFieldViewModeAlways;
    self.passwordTXT.rightView = paddingView;
    [_emialAddressTXT resignFirstResponder];
    [_passwordTXT resignFirstResponder];
    _emialAddressTXT.delegate=self;
    _passwordTXT.delegate=self;
    
    [popview removeFromSuperview];
    popview = nil;
    popview=[KLCPopup popupWithContentView:self.viewLogin];
    popview.shouldDismissOnBackgroundTouch=NO;
    
    [self.view addSubview:popview];
    
    [popview show];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_emialAddressTXT becomeFirstResponder];
        
    });
    
    
}
@end
