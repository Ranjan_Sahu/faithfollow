//
//  LoginViewController.m
//  wireFrameSplash
//
//  Created by home on 8/17/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "LoginViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ForgotViewController.h"
#import "MHTabBarController.h"
#import "UIImageView+WebCache.h"

@interface LoginViewController ()<UITextFieldDelegate>
{
     UIButton *showPasswordBtn;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
      // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    
//    NSLog(@"imagestr %@",_imagestrURl);
    
    [super viewWillAppear:YES];
    self.view.userInteractionEnabled=YES;
UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived:)];
    [tapGestureRecognizer setDelegate:(id)self];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x009AD9);
    self.navigationController.navigationBar.translucent = NO;
    _imageProfilePop.clipsToBounds = YES;
    _imageProfilePop.layer.masksToBounds = YES;
    [self loadpopImage];
}

-(void)loadpopImage{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_imageProfilePop sd_setImageWithURL:[NSURL URLWithString:_imagestrURl] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
    });
}
-(void)tapReceived:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

-(void)leftButtonPressedLogin{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
