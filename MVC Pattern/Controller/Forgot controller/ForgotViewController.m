//
//  ForgotViewController.m
//  wireFrameSplash
//
//  Created by home on 5/2/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "ForgotViewController.h"
#import "AppDelegate.h"
#import "Constant1.h"
#import "NSString+StringValidation.h"
#import "WebServiceHelper.h"
@interface ForgotViewController ()

@end

@implementation ForgotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if (IS_IPHONE4) {
        _emailViewHeight.constant=30;
        _submitBtnHeight.constant=30;
    }else if (IS_IPHONE5) {
        _emailViewHeight.constant=50;
        _submitBtnHeight.constant=50;
    }
    
    self.emailView.layer.borderWidth = 1.0f;
    self.emailView.layer.borderColor = [UIColor colorWithRed:214.0/255.0 green:221.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    
}

-(IBAction)closeAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark - Validation

- (NSString *)validateFields {
    
    NSString *errorMessage ;
    NSString *strEmail = [self.emailTXT.text removeWhiteSpaces];
    
    if ([strEmail length] == 0) {
        errorMessage =EnterEmail;
        return errorMessage;
    }
    
    BOOL isEmailvalid = [Constant1 validateEmail:strEmail];
    if (!isEmailvalid) {
        errorMessage =InvalidEmail;
        return errorMessage;
    }
    
    
    return 0;
}

-(IBAction)submit:(id)sender{
    [self.emailTXT resignFirstResponder];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    } else {
        
        NSString *errorMessage = [self validateFields];
        
        if (errorMessage) {
            [[[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
            return;
            
        }
        else{
            
            NSDictionary *dict = @{
                                   @"email" :self.emailTXT.text,
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"forgotpassword" withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            NSMutableArray *messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            
                        }
                        else{
                            NSMutableArray *messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            
                            
                        }
                    });
                }
                }
            } errorBlock:^(id error)
             {
               [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
                 
             }];
        }
    }
  
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
