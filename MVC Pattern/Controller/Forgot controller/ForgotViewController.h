//
//  ForgotViewController.h
//  wireFrameSplash
//
//  Created by home on 5/2/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotViewController : UIViewController
-(IBAction)Back:(id)sender;
-(IBAction)submit:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *emailTXT;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitBtnHeight;


@end
