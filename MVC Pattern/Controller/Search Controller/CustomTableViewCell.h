//
//  CustomTableViewCell.h
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelOutlet;
@property (weak, nonatomic) IBOutlet UILabel *namelabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
- (IBAction)button:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *button_btn;
@property (weak, nonatomic) IBOutlet UIButton *btn_GroupJoin;
@property (weak, nonatomic) IBOutlet UIImageView *search_Groupimage;
@property (weak, nonatomic) IBOutlet UIImageView *Mygroup_Groupimage;
@end
