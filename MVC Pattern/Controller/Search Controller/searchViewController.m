//
//  secondViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "searchViewController.h"
#import "CustomTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "ViewMycommunityViewController.h"
#import "FriendsProfileViewController.h"
#import "SearchCommunityProfileView.h"
#import "ProfileFriendViewController.h"
#import "MycommunityTableViewCell.h"
#import "UIView+Toast.h"
#import "InfluencerProfileController.h"

@interface searchViewController ()
{
    NSMutableArray *searcharray,*request_dataArray,*webSearchArray,*messageArray;
    NSString *searchString;
    NSMutableDictionary *dictwebSearchArray;
    
}
@end

@implementation searchViewController
@synthesize isFiltered;


- (void)viewDidLoad {
    
     [self.tableView registerNib:[UINib nibWithNibName:@"MycommunityTableViewCell" bundle:nil] forCellReuseIdentifier:@"MycommunityTableViewCell"];
    
    [super viewDidLoad];
    _airlines=[[NSMutableArray alloc]initWithObjects:@"raj",@"Amit",@"Candan",@"ashok",@"depak",nil];
    searcharray=[[NSMutableArray alloc]init];
    webSearchArray=[[NSMutableArray alloc]init];
    messageArray=[[NSMutableArray alloc]init];
    
//    [self.view makeToast:@"please search " duration:1.0 position:CSToastPositionCenter];
    ///  [self send_Friendrequest];
    
    // Customize SearchBar
    UITextField *TF_Search = (UITextField *)[self.searchbar valueForKey:@"searchField"];
    TF_Search.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1];
    TF_Search.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:242.0/255.0 blue:243.0/255.0 alpha:1];
    self.searchbar.barTintColor = [UIColor whiteColor];
    self.searchbar.tintColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1];
    self.searchbar.placeholder = @"Search";
    
    UILabel *TF_Label = (UILabel *)[TF_Search valueForKey:@"placeholderLabel"];
    TF_Label.textColor = [UIColor colorWithRed:151.0/255.0 green:175.0/255.0 blue:191.0/255.0 alpha:1];
    
    UIImageView *glassImg = (UIImageView *)TF_Search.leftView;
    //glassImg.image = [UIImage imageNamed:@"searchIcon30"];
    glassImg.image = [glassImg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    glassImg.tintColor = [UIColor colorWithRed:151.0/255.0 green:175.0/255.0 blue:191.0/255.0 alpha:1];
    
    self.searchbar.backgroundImage = [UIImage new];

    // Do any additional setup after loading the view.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSUserDefaults standardUserDefaults] setObject:@"SearchCommunity" forKey:@"SearchCommunity"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"pageIndexZero"];
    
     self.tableView.tableFooterView = [UIView new];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [webSearchArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict=[webSearchArray objectAtIndex:indexPath.row];
    NSString *frind_satus,*checkis_influencer;
    
    if ([dict isKindOfClass:[NSDictionary class]]) {
        frind_satus=[dict valueForKey:@"friend_status"];
        checkis_influencer=[dict valueForKey:@"is_influencer"];
    }
    
   MycommunityTableViewCell *cell = (MycommunityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MycommunityTableViewCell" forIndexPath:indexPath];
    
    if ([checkis_influencer isEqualToString:@"0"]) {
        cell.influencerImage.hidden=YES;
    }
    [cell.btn_viewprofile addTarget:self action:@selector(searchFellow:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([frind_satus isEqualToString:@"Follow"]) {
        [cell.btn_viewprofile setTitle:@"Follow" forState:UIControlStateNormal];
        [cell.btn_viewprofile setBackgroundColor:[UIColor whiteColor]];
        [cell.btn_viewprofile setTitleColor:[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        cell.btn_viewprofile.layer.borderWidth = 1.0;
        cell.btn_viewprofile.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0].CGColor;
    }else{
//        [cell.btn_viewprofile setTitle:@"UnFollow" forState:UIControlStateNormal];
//        [cell.btn_viewprofile setBackgroundColor:[UIColor orangeColor]];
//        [cell.btn_viewprofile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        cell.btn_viewprofile.layer.borderWidth = 1.0;
//        cell.btn_viewprofile.layer.borderColor = [UIColor orangeColor].CGColor;
        [cell.btn_viewprofile setTitle:@"Followed" forState:UIControlStateNormal];
        [cell.btn_viewprofile setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0]];
        [cell.btn_viewprofile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cell.btn_viewprofile.layer.borderWidth = 1.0;
        cell.btn_viewprofile.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0].CGColor;

    }
    
    cell.btn_viewprofile.tag=indexPath.row;
    [cell.btn_viewprofile addTarget:self action:@selector(searchFellow:) forControlEvents:UIControlEventTouchUpInside];
//    cell.lbl_name.numberOfLines = 1;
//    cell.lbl_name.minimumScaleFactor = 0.5;
//    cell.lbl_name.adjustsFontSizeToFitWidth = YES;
    cell.lbl_name.text=[dict valueForKey:@"first_name"];
    NSDictionary *userProfile=[dict valueForKey:@"usersprofile"];
    NSString *userId=[NSString stringWithFormat:@"%@",userProfile[@"user_id"]];
    
    if ([userId isEqualToString:@"1" ]|| [userId isEqualToString:@"2"]) {
        cell.btn_viewprofile.hidden=YES;
    }else{
      cell.btn_viewprofile.hidden=NO;
    }

    NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,userProfile[@"profile_picture"]];
    [cell.imageView_profile  sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
    
    // add tap geture to show profile of user
    cell.imageView_profile.tag=indexPath.row;
    cell.imageView_profile.userInteractionEnabled=YES;
    UITapGestureRecognizer *imagegProfile = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageviewProfileTapped:)];
    [cell.imageView_profile addGestureRecognizer:imagegProfile];
    
    //add tapgesture to show profile
    cell.lbl_name.tag = indexPath.row;
    cell.lbl_name.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *profilename = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilenameTaped:)];
    [cell.lbl_name addGestureRecognizer:profilename];
    
    
    return cell;
    
}

-(void)searchFellow:(UIButton*)sender{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        //[self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *frind_id;
        CGPoint center= sender.center;
        CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableView];
        NSIndexPath *indexPathvalue = [self.tableView indexPathForRowAtPoint:rootViewPoint];
        NSMutableDictionary *arrayDict=[webSearchArray objectAtIndex:indexPathvalue.row];
        if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
            NSMutableDictionary *usersprofile=arrayDict[@"usersprofile"];
            frind_id=[usersprofile valueForKey:@"user_id"];
        }
        NSDictionary *dict=nil;
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 @"friend_id" : frind_id,
                 
                 };
        NSString *methodname=nil;
//        NSLog(@"The button title is %@",sender.titleLabel.text);
        UIButton *btn = (UIButton*)sender;
        if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
            methodname=follow;
        }
        else{
            methodname=unfollow;
            
        }
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodname withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                             messageArray=[response valueForKey:@"message"];
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
                                NSMutableDictionary *arrayDict=[webSearchArray objectAtIndex:indexPathvalue.row];
                            if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
                                [arrayDict setObject:@"Unfollow" forKey:@"friend_status"];
                                    
                                }
                                
                            }
                            else{
                                NSMutableDictionary *arrayDict=[webSearchArray objectAtIndex:indexPathvalue.row];
                                if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
                                    [arrayDict setObject:@"Follow" forKey:@"friend_status"];
                                    
                                }
                            }
                            
                            [self reloadCellSearch:indexPathvalue.row];
                        });
                        
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
        
    }
    
    
    
}


-(void)reloadCellSearch:(NSUInteger)cell_index{
    
    [self.tableView beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cell_index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
    
}

- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

// tapgeture method

// taped image and profile name to  show profile of user

- (void)imageviewProfileTapped:(UITapGestureRecognizer*)sender {
    NSMutableDictionary *arrayProfileDict=[webSearchArray objectAtIndex:sender.view.tag];
    NSString *frindID;
    if ([arrayProfileDict isKindOfClass:[NSMutableDictionary class]]) {
        frindID=[arrayProfileDict valueForKey:@"id"];
        NSString *indexpathID=[NSString stringWithFormat:@"%ld",sender.view.tag];
        [ self  viewFriendProfile :nil FriendId:frindID indexpath :indexpathID];
    }
}

- (void)profilenameTaped:(UITapGestureRecognizer*)sender {
    NSMutableDictionary *arrayProfileDict=[webSearchArray objectAtIndex:sender.view.tag];
    NSString *frindID;
    if ([arrayProfileDict isKindOfClass:[NSMutableDictionary class]]) {
        frindID=[arrayProfileDict valueForKey:@"id"];
        NSString *indexpathID=[NSString stringWithFormat:@"%ld",sender.view.tag];
        [ self  viewFriendProfile :nil FriendId:frindID indexpath :indexpathID];
    }
}





#pragma Identify method friend or community



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    dictwebSearchArray=[webSearchArray objectAtIndex:indexPath.row];
        if ([[dictwebSearchArray valueForKey:@"object_type"]isEqualToString:@"user"]) {
        NSString *idcheck=[NSString stringWithFormat:@"%@",dictwebSearchArray[@"object_id"]];
        NSString *loginId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
        if ([idcheck isEqualToString:loginId]) {
            [self.navigationController removeFromParentViewController];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"profileshow" object:nil userInfo:nil];
        }
        else{
//            [self viewFriendProfile:dictwebSearchArray[@"object_id"] FriendId:dictwebSearchArray[@"id"]];
        }
        
        
    }
    
    
    if ([[dictwebSearchArray valueForKey:@"object_type"]isEqualToString:@"community"]) {
        //        [self viewCommunityProfile:dictwebSearchArray[@"object_id"] communityId:dictwebSearchArray[@"id"]];
    }
    
}



#pragma viewFriendProfile

-(void)viewFriendProfile :(NSString*)friend_object FriendId:(NSString*)Id indexpath:(NSString*)indexpath{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id":Id
                           };
    
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    NSMutableDictionary *FriendDict=response[@"data"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                    NSDictionary *profileviewDict=[FriendDict valueForKey:@"usersprofile"];
                        NSString *profile_picture=[profileviewDict valueForKey:@"profile_picture"];
                        NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",FriendDict[@"first_name"],FriendDict[@"last_name"]]]];
                        [[NSUserDefaults standardUserDefaults] setObject:nameTxt forKey:@"name"];
                        [[NSUserDefaults standardUserDefaults] setObject:profile_picture forKey:@"profile_picture12"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        NSMutableDictionary *arrayProfileDict=[webSearchArray objectAtIndex:[indexpath intValue]];
                        
                        NSString *checkFluencer;
                        if ([arrayProfileDict isKindOfClass:[NSMutableDictionary class]]) {
                            checkFluencer=[arrayProfileDict valueForKey:@"is_influencer"];
                            if ([checkFluencer isEqualToString:@"0"]) {
                            FriendsProfileViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                                allprofileView.friendProfileDict=FriendDict;
                                allprofileView.postFriendProfileDict=FriendDict;
//                                [self presentViewController:allprofileView animated:YES completion:nil];
                                [self.navigationController pushViewController:allprofileView animated:YES];
                            }
                            else{
                                InfluencerProfileController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                                allprofileView.influencerDict=FriendDict;
//                                [self presentViewController:allprofileView animated:YES completion:nil];
                                [self.navigationController pushViewController:allprofileView animated:YES];

                            }
                            
                        }

                        
                        
                    });
                }
            }
        }
        else{
            
          messageArray=[response valueForKey:@"message"];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([messageArray count]>0) {
                  [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                }
               
            });
        }
        
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
    
}

#pragma viewCommunityProfile




#pragma mark - UISearchControllerDelegate & UISearchResultsDelegate


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length == 0)
    {
    }
    else{
        searchString=searchText;
        
    }
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    // called when keyboard search button pressed
    [self serachTextString:searchString];
    
    
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    dispatch_async(dispatch_get_main_queue(), ^{
        [searchBar resignFirstResponder];
            });
    
    
}



-(void)serachTextString:(NSString*)searchText {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"searchterm" :searchText,
                               
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:searchfriend withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            [_searchbar resignFirstResponder];
                            webSearchArray=[response valueForKey:@"data"];
                            [self.tableView reloadData];
                            
                        }
                        else{
                            messageArray=[response valueForKey:@"message"];
                            if ([messageArray count]>0) {
                              [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            }
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
    }
}
-(void)sendRequestforViewprofile:(NSString*)methodName anddict:(NSDictionary*)dict{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
//            NSLog(@"responmse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        NSMutableDictionary*    friendsdataDict=[response valueForKey:@"data"];
                        if (friendsdataDict) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if ([methodName isEqualToString:communityDisplay]) {
                                    ViewMycommunityViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewMycommunityViewController"];
                                    allprofileView.friendProfileDict=friendsdataDict;
                                    [self.navigationController pushViewController:allprofileView animated:YES];
                                }
                                
                            });
                        }else{
                            messageArray=[response valueForKey:@"message"];
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            }
                            
                        }
                        
                    }
                }
            }
            
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
    }
    
}



@end
