//
//  SearchResultsTableViewController.h
//  DemotableviewSection
//
//  Created by home on 3/10/16.
//  Copyright (c) 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsTableViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray *searchResults;

@end
