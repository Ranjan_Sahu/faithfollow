//
//  secondViewController.h
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface searchViewController : UIViewController<UISearchResultsUpdating>
@property(strong,nonatomic)IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *airlines;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property(nonatomic,weak)IBOutlet  UISearchBar *searchbar;
@property (nonatomic, assign) bool isFiltered;

@end
