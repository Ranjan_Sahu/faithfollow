//
//  AdditionalDisplayView.m
//  wireFrameSplash
//
//  Created by home on 6/15/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "AdditionalDisplayView.h"

@interface AdditionalDisplayView ()
{
    NSMutableArray *webResponseData;
    
}
@end

@implementation AdditionalDisplayView

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    NSLog(@"dic t %@",_additionDispaly);
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }
    else {
    
      if ([_additionDispaly isKindOfClass:[NSMutableDictionary class]]) {
        
    NSMutableDictionary *countryName=[[NSMutableDictionary alloc]init];
    countryName=[_additionDispaly valueForKey:@"get_county_details"];
    
    if ([countryName isKindOfClass:[NSMutableDictionary class]])
    {
        NSString *countryValue=[countryName valueForKey:@"name"];
        
        if ([countryValue isEqualToString:@""]) {
            _countryLBL.text=@"-";
            
        }
        else{
            _countryLBL.text=countryValue;
            
        }
        
    }
    else{
        _countryLBL.text=@"-";
  
    }
    
    
    
    NSString *zip=[_additionDispaly valueForKey:@"zip"];
    if ([zip isEqualToString:@""]) {
        _zipcodeLBL.text=@"-";
        
    }
    else{
        _zipcodeLBL.text=zip;
        
    }
    
    NSString *city=[_additionDispaly valueForKey:@"city"];
    if ([city isEqualToString:@""]) {
        _cityLBL.text=@"-";
        
    }
    else{
        _cityLBL.text=city;
        
    }
    
    NSString *address=[_additionDispaly valueForKey:@"address"];
    if ([address isEqualToString:@""]) {
        _addressLBL.text=@"-";
        
    }
    else{
        _addressLBL.text=address;
        
    }
    
    NSString *inspirationa_saying=[_additionDispaly valueForKey:@"inspirationa_saying"];
    if ([inspirationa_saying isEqualToString:@""]) {
        _inspirationLBL.text=@"-";
        
    }
    else{
        _inspirationLBL.text=inspirationa_saying;
        
    }
    
    
    NSString *favourite_prayer=[_additionDispaly valueForKey:@"favourite_prayer"];
    if ([favourite_prayer isEqualToString:@""]) {
        _favouriteLBL.text=@"-";
        
    }
    else{
        _favouriteLBL.text=favourite_prayer;
        
    }
    
    
    NSString *children=[_additionDispaly valueForKey:@"children"];
    if ([children isEqualToString:@""]) {
        _childrenLBL.text=@"-";
        
    }
    else{
        _childrenLBL.text=children;
        
    }
    
    NSString *siblings=[_additionDispaly valueForKey:@"siblings"];
    if ([siblings isEqualToString:@""]) {
        _siblingLBL.text=@"-";
        
    }
    else{
        _siblingLBL.text=siblings;
        
    }
    
    NSString *jobtitle=[_additionDispaly valueForKey:@"job_title"];
    if ([jobtitle isEqualToString:@""]) {
        _jobLBL.text=@"-";
        
    }
    else{
        _jobLBL.text=jobtitle;
        
    }
    
    NSString *parish=[_additionDispaly valueForKey:@"parish"];
    if ([jobtitle isEqualToString:@""]) {
        _parishLBL.text=@"-";
        
    }
    else{
        _parishLBL.text=parish;
        
    }
    
    NSString *employer=[_additionDispaly valueForKey:@"employer"];
    if ([jobtitle isEqualToString:@""]) {
        _employeeLBL.text=@"-";
        
    }
    else{
        _employeeLBL.text=employer;
        
    }
    
    NSString *high_school=[_additionDispaly valueForKey:@"high_school"];
    if ([high_school isEqualToString:@""]) {
        _highSchoolLBL.text=@"-";
        
    }
    else{
        _highSchoolLBL.text=employer;
        
    }
    
    
    NSString *state=[_additionDispaly valueForKey:@"state"];
    if ([state isEqualToString:@""]) {
        _stateLBL.text=@"-";
        
    }
    else{
        _stateLBL.text=state;
        
    }
    
    NSString *relationship_status=[_additionDispaly valueForKey:@"relationship_status"];
    if ([relationship_status isEqualToString:@""]) {
        _relationLBL.text=@"-";
        
    }
    else{
        _relationLBL.text=relationship_status;
        
    }
    
    }
    }
    //    else{
    //        [self webresponse];
    //    }
    
    
    // Do any additional setup after loading the view.
}

//-(void)webresponse{
//
//    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
//    if (!appDelegate.isReachable) {
//        UIAlertView *message = [[UIAlertView alloc] initWithTitle:appTitle message:NoNetwork  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [message show];
//
//    }
//    else{
//        NSDictionary *dict = @{
//                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
//                               @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
//                               };
//        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"profile" withParameters:dict withHud:YES success:^(id response){
//            if ([response isKindOfClass:[NSDictionary class]]){
//                if (![response isKindOfClass:[NSNull class]]) {
//                    if([response[@"status"]intValue ] ==1){
////                        webResponseData=[response valueForKey:@"data"];
////                        [usersprofileDict addEntriesFromDictionary:[webResponseData valueForKey:@"usersprofile"]];
////                        NSLog(@"userdict %@",_additionDispaly);
//                        dispatch_async(dispatch_get_main_queue(), ^{
//
////
//                        });
//                    }
//                }
//                else{
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        NSMutableDictionary *emailDict = [[NSMutableDictionary alloc]init];
//                        NSMutableArray *email = [[NSMutableArray alloc]init];
//                        emailDict=[response valueForKey:@"errors"];
//                        email=[emailDict valueForKey:@"email"];
//                        [self.view makeToast:[email objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
//
//
//                    });
//                }
//            }
//        } errorBlock:^(id error)
//         {
//             NSLog(@"error");
//
//         }];
//
//    }
//
//}
//





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)backDisplayView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
