//
//  FriendListController.m
//  wireFrameSplash
//
//  Created by home on 5/13/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "FriendListController.h"
#import "ProfileTableViewCell.h"
#import "WebServiceHelper.h"
#import "AppDelegate.h"
#import "Constant1.h"
#import "UIImageView+WebCache.h"
#import "ProfileFriendViewController.h"
#import "MNMBottomPullToRefreshManager.h"
#import "Profile_editViewController.h"
#import "InfluencerProfileController.h"

@interface FriendListController ()<MNMBottomPullToRefreshManagerClient>
{
    NSMutableArray *webresponseArray;
    int current_page;
    NSString *urlString;
    NSString *comingStr;
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
    int pagenumber,total_pagenumber,checkbackgournd;
    
    
}
@end

@implementation FriendListController

- (void)viewDidLoad {
    
pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:60.0f tableView:_tableView withClient:self];
    
    
    isPageRefresing=NO;
    [super viewDidLoad];
    webresponseArray=[[NSMutableArray alloc]init];
    current_page=1;
    if ([_checkProfile isEqualToString:@"checkFrendList"]) {
        //profileView controller
        [self frinendlistResponseProfile];
        
    }
    else{
        //friend view controller
        [self frinendlistResponse];
        
    }
    // Do any additional setup after loading the view.
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (getVar.checkDisapper) {
        [[AppDelegate sharedAppDelegate] addsView];
//        getVar.checkDisapper=NO;
    }
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)appEnterInForegroundfrindlist{
    checkbackgournd=checkbackgournd+1;
    if (checkbackgournd%3==0) {
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"Frindlist"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        checkbackgournd=0;
    }
}
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideaddView];
 [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterInForegroundfrindlist) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //    if ([_checkProfile isEqualToString:@"checkFrendList"]) {
    //        [self frinendlistResponseProfile];
    //
    //    }
    //    else{
    //        [self frinendlistResponseProfile];
    //
    //    }
    
    
}
#pragma mark frinendlistResponseProfileMethod

-(void)frinendlistResponseProfile{
    comingStr=@"frinendlistResponseProfile";
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else {
        
        NSDictionary *dict = @{
                               @"profile_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"profile_id"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsoffriend" withParameters:dict withHud:YES success:^(id response){
            
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            NSMutableDictionary *mainData=[[NSMutableDictionary alloc]init];
                            mainData=[response valueForKey:@"data"];
                            urlString=[mainData valueForKey:@"next_page_url"];
                            NSString *current_page1=[mainData valueForKey:@"current_page"];
                            current_page=[current_page1 intValue];
                            NSString *totalPage=[NSString stringWithFormat:@"%@",[mainData valueForKey:@"last_page"]];
                            total_pagenumber=[totalPage intValue];
                            webresponseArray = [[NSMutableArray alloc] initWithArray:[mainData valueForKey:@"data"]];
                            
                            NSString *totalrecord=[NSString stringWithFormat:@"%@",[mainData valueForKey:@"total"]];
                            if ([totalrecord intValue]==0) {
                                [self.view makeToast:@"No Friend record" duration:1.0 position:CSToastPositionCenter];
                                
                            }
                            [self.tableView reloadData];
                        }
                        else{
                        NSMutableDictionary *mainData=[[NSMutableDictionary alloc]init];
                        NSMutableArray *messageAray=mainData[@"message"];
                            if ([messageAray count]>0) {
                              [self.view makeToast:[messageAray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            }
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
}

#pragma mark frinendlistResponseMethod

-(void)frinendlistResponse{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }
    else {
        NSDictionary *dict = @{
                               @"profile_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"friendfriendlistId"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsoffriend" withParameters:dict withHud:YES success:^(id response){
            
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            NSMutableDictionary *mainData=[[NSMutableDictionary alloc]init];
                            mainData=[response valueForKey:@"data"];
                            urlString=[mainData valueForKey:@"next_page_url"];
                            NSString *current_page1=[mainData valueForKey:@"current_page"];
                            current_page=[current_page1 intValue];
                            NSString *totalPage=[NSString stringWithFormat:@"%@",[mainData valueForKey:@"last_page"]];
                            total_pagenumber=[totalPage intValue];
                            webresponseArray = [[NSMutableArray alloc] initWithArray:[mainData valueForKey:@"data"]];
                            NSString *totalrecord=[NSString stringWithFormat:@"%@",[mainData valueForKey:@"total"]];
                            if ([totalrecord intValue]==0) {
                                [self.view makeToast:@"No Friend record" duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            [self.tableView reloadData];
                            
                        }
                        else{
                        NSMutableDictionary *mainData=[[NSMutableDictionary alloc]init];
                        NSMutableArray *messageAray=mainData[@"message"];
                            if ([messageAray count]>0) {
                             [self.view makeToast:[messageAray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            }
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
}
#pragma mark cancelBTNMethod

-(IBAction)cancelBTN:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark tableview delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [webresponseArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendlist"];
    NSMutableDictionary *dict=[webresponseArray objectAtIndex:indexPath.row];
    NSMutableArray *userdict=[dict valueForKey:@"get_friend_details"];
    NSMutableArray *userdictuser=[dict valueForKey:@"get_user_details"];
    
    if ([userdict count]!=0) {
        NSDictionary *userProfile=[userdict objectAtIndex:0];
        cell.nameLabel.numberOfLines = 1;
        cell.nameLabel.minimumScaleFactor = 0.5;
        cell.nameLabel.adjustsFontSizeToFitWidth = YES;
        cell.nameLabel.text=[NSString stringWithFormat:@"%@ %@",[userProfile valueForKey:@"first_name"],[userProfile valueForKey:@"last_name"]];
        NSDictionary *userProfileUnderProfile=[userProfile valueForKey:@"usersprofile"];
        NSString *useridFriend=[NSString stringWithFormat:@"%@",[userProfileUnderProfile valueForKey:@"user_id"]];
        NSString *influncerimage=[NSString stringWithFormat:@"%@",[userProfileUnderProfile valueForKey:@"user_type"]];
        NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[userProfileUnderProfile valueForKey:@"profile_picture"]];
        NSURL *imageURL = [NSURL URLWithString:str];
        if ([influncerimage isEqualToString:@"2"]) {
            cell.influencerImage.hidden=NO;
        [cell.imageViewNotification sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
        }
        else{
            cell.influencerImage.hidden=YES;
            [cell.imageViewNotification sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
            
        }
        
    }
    else if ([userdictuser count]!=0) {
        NSDictionary *userProfile=[userdictuser objectAtIndex:0];
        cell.nameLabel.numberOfLines = 1;
        cell.nameLabel.minimumScaleFactor = 0.5;
        cell.nameLabel.adjustsFontSizeToFitWidth = YES;
        cell.nameLabel.text=[NSString stringWithFormat:@"%@ %@",[userProfile valueForKey:@"first_name"],[userProfile valueForKey:@"last_name"]];
        NSDictionary *userProfileUnderProfile=[userProfile valueForKey:@"usersprofile"];
        
        
        NSString *useridFriend=[NSString stringWithFormat:@"%@",[userProfileUnderProfile valueForKey:@"user_id"]];
//        NSLog(@"useridFriend %@",useridFriend);
        
        
        NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[userProfileUnderProfile valueForKey:@"profile_picture"]];
        NSURL *imageURL = [NSURL URLWithString:str];
        [cell.imageViewNotification sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
        
    }
   
    return cell;
    
}
- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *dict=[webresponseArray objectAtIndex:indexPath.row];
    NSMutableArray *userdict=[dict valueForKey:@"get_friend_details"];
    NSMutableArray *userdictUser=[dict valueForKey:@"get_user_details"];
    
    if ([userdict count]!=0) {
        NSMutableDictionary *userProfile=[userdict objectAtIndex:0];
        NSDictionary *userProfileUnderProfile=[userProfile valueForKey:@"usersprofile"];
        NSString *useridFriend=[NSString stringWithFormat:@"%@",[userProfileUnderProfile valueForKey:@"user_id"]];
        NSString *loginId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
         NSString *checkinfluencer=[NSString stringWithFormat:@"%@",[userProfile valueForKey:@"user_type"]];
        if ([checkinfluencer isEqualToString:@"2"]) {
            if ([useridFriend isEqualToString:loginId]) {
                InfluencerProfileController *incluncerView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                incluncerView.influencerDict=userProfile ;
//                [self presentViewController:incluncerView animated:YES completion:nil];
                [self.navigationController pushViewController:incluncerView animated:YES];
                
            }
            else{
             [self viewFrndProfile:useridFriend];
            }
            

        }
        else{
            if ([useridFriend isEqualToString:loginId]){
            Profile_editViewController *profileView=[self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
            CGRect    screenRect = [[UIScreen mainScreen] bounds];
            UIView *viewProfile=[[UIView alloc]initWithFrame:CGRectMake(0, 0,screenRect.size.width,60)];
            viewProfile.backgroundColor=UIColorFromRGB(0x0088CF);
            [[profileView view] addSubview:viewProfile];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button addTarget:self
                       action:@selector(aMethod)
             forControlEvents:UIControlEventTouchUpInside];
            [button setImage:[UIImage imageNamed:@"leftArrow"] forState:UIControlStateNormal];
            button.frame = CGRectMake(5, 25,30, 30);
            [viewProfile addSubview:button];
            
            UILabel *lbl1 = [[UILabel alloc] init];
            [lbl1 setFrame:CGRectMake(screenRect.size.width/2-20,15,100,50)];
            lbl1.font = [UIFont fontWithName:@"Avenir-Medium" size:16];
            
            lbl1.backgroundColor=[UIColor clearColor];
            lbl1.textColor=[UIColor whiteColor];
            lbl1.text= @"Profile";
            [viewProfile addSubview:lbl1];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"checkprofile"];
            [[NSUserDefaults standardUserDefaults] synchronize];
//            [self presentViewController:profileView animated:YES completion:nil];
            [self.navigationController pushViewController:profileView animated:YES];

        }
        else{
               [self viewFrndProfile:useridFriend];
            }
        }
        
        
    }
    else if ([userdictUser count]!=0) {
        
        NSMutableDictionary *userProfile=[userdictUser objectAtIndex:0];
        NSDictionary *userProfileUnderProfile=[userProfile valueForKey:@"usersprofile"];
        NSString *useridFriend=[NSString stringWithFormat:@"%@",[userProfileUnderProfile valueForKey:@"user_id"]];
        NSString *loginId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
        NSString *checkinfluencer=[NSString stringWithFormat:@"%@",[userProfile valueForKey:@"user_type"]];
        
        if ([checkinfluencer isEqualToString:@"2"]) {
             if ([useridFriend isEqualToString:loginId]) {
            InfluencerProfileController *incluncerView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                 incluncerView.influencerDict=userProfile ;
//            [self presentViewController:incluncerView animated:YES completion:nil];
                 [self.navigationController pushViewController:incluncerView animated:YES];

             }
             else{
                [self viewFrndProfile:useridFriend];
             }
        }
        else{
        if ([useridFriend isEqualToString:loginId]) {
         Profile_editViewController *profileView=[self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
            CGRect    screenRect = [[UIScreen mainScreen] bounds];
            UIView *viewProfile=[[UIView alloc]initWithFrame:CGRectMake(0, 0,screenRect.size.width,60)];
            viewProfile.backgroundColor=UIColorFromRGB(0x0088CF);
            [[profileView view] addSubview:viewProfile];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button addTarget:self
                       action:@selector(aMethod)
             forControlEvents:UIControlEventTouchUpInside];
            [button setImage:[UIImage imageNamed:@"leftArrow"] forState:UIControlStateNormal];
            button.frame = CGRectMake(20, 25,30, 30);
            [viewProfile addSubview:button];
            
            UILabel *lbl1 = [[UILabel alloc] init];
            [lbl1 setFrame:CGRectMake(screenRect.size.width/2-20,15,100,50)];
            lbl1.font = [UIFont fontWithName:@"Avenir-Medium" size:16];
            
            lbl1.backgroundColor=[UIColor clearColor];
            lbl1.textColor=[UIColor whiteColor];
            lbl1.text= @"Profile";
            [viewProfile addSubview:lbl1];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"checkprofile"];
            [[NSUserDefaults standardUserDefaults] synchronize];
//            [self presentViewController:profileView animated:YES completion:nil];
            [self.navigationController pushViewController:profileView animated:YES];

        }
        else{
          [self viewFrndProfile:useridFriend];
        }
        
        }
    
        
    }
    
}


-(void)aMethod{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate: (BOOL)decelerate
{
    [pullToRefreshManager_ tableViewReleased];
}


- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager
{
    
//    NSLog(@"last page %d",total_pagenumber);
//    NSLog(@"current page %d",current_page);
    
    if (current_page==total_pagenumber) {
//        NSLog(@"end");
        
    }else{
        [self getRecordsScroll:current_page+1];
    }
    
    
}

#pragma mark GetRecordsScrollMethod

-(void)getRecordsScroll:(NSInteger)pageNumber{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else {
        NSDictionary *dict;
        if ([comingStr  isEqualToString:@"frinendlistResponseProfile"]) {
            dict = @{
                     @"profile_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"profile_id"],
                     @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                     @"page":[NSString stringWithFormat:@"%ld",pageNumber],
                     
                     };
        }
        else{
            dict =@{
                    @"profile_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"friendfriendlistId"],
                    @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                    @"page":[NSString stringWithFormat:@"%ld",pageNumber],
                    
                    };
            
        }
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsoffriend" withParameters:dict withHud:YES success:^(id response){
            
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            isPageRefresing = NO;
                            NSMutableDictionary *mainData=[[NSMutableDictionary alloc]init];
                            mainData=[response valueForKey:@"data"];
                            NSString *current_page1=[mainData valueForKey:@"current_page"];
                            current_page=[current_page1 intValue];
                            NSString *totalPage=[NSString stringWithFormat:@"%@",[mainData valueForKey:@"last_page"]];
                            total_pagenumber=[totalPage intValue];
                            NSMutableArray *localarray=[mainData objectForKey:@"data"];
                            [webresponseArray addObjectsFromArray:localarray];
                            NSString *totalrecord=[NSString stringWithFormat:@"%@",[mainData valueForKey:@"total"]];
                            if ([totalrecord intValue]==0) {
                                [self.view makeToast:@"No Friend " duration:1.0 position:CSToastPositionCenter];
                            }
                            [self.tableView reloadData];
                            [pullToRefreshManager_ tableViewReloadFinished];
                            
                        }
                        else{
                            NSMutableDictionary *mainData=[[NSMutableDictionary alloc]init];
                            NSMutableArray *messageAray=mainData[@"message"];
                            if ([messageAray count]>0) {
                              [self.view makeToast:[messageAray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            }
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  60.0f ;
    
}


#pragma mark ViewFrndProfileMethod

-(void)viewFrndProfile :(NSString*)friend_id{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : friend_id,
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if([response[@"status"]intValue ] ==1){
                
                friendsdataDict=[response valueForKey:@"data"];
                if (friendsdataDict) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSUserDefaults standardUserDefaults]setObject:@"frndlistSearch" forKey:@"frndlistSearch"];
                        ProfileFriendViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileFriendViewController"];
                        allprofileView.friendProfileDictProfile=friendsdataDict;
                        allprofileView.postFriendProfileDict=friendsdataDict;
//                        [self presentViewController:allprofileView animated:YES completion:nil];
                        [self.navigationController pushViewController:allprofileView animated:YES];

                    });
                    
                }
                
            }else{
                NSMutableArray *messageArray=[[NSMutableArray alloc]init];
                messageArray=[response valueForKey:@"message"];
                if ([messageArray count]>0) {
                  [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                }
                
            }
            
        }
    } errorBlock:^(id error)
     {
    [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//         NSLog(@"error");
         
     }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
