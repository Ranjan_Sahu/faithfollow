//
//  AdditionInfoViewController.m
//  wireFrameSplash
//
//  Created by home on 4/14/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "AdditionInfoViewController.h"
#import "ProfileTableViewCell.h"
#import "DeviceConstant.h"
#import "Constant1.h"
#import "NSString+StringValidation.h"
#import "AppDelegate.h"
#import "WebServiceHelper.h"


@interface AdditionInfoViewController ()<UITextFieldDelegate>
{
    NSMutableArray *countryArrayname,*countryArrayid,*relationlist;
}

@end

@implementation AdditionInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else {
        
        if ([_additionDispaly isKindOfClass:[NSMutableDictionary class]]) {
        relationlist=[[NSMutableArray alloc]init];
        self.zipcodeTXT.delegate=self;
        countryArrayname=[[NSMutableArray alloc]init];
        countryArrayid=[[NSMutableArray alloc]init];
        [[_selectView layer] setBorderWidth:1.0f];
        [[_selectView layer] setBorderColor:[UIColor grayColor].CGColor];
        [[_relationView layer] setBorderWidth:1.0f];
        [[_relationView layer] setBorderColor:[UIColor grayColor].CGColor];
        
        [self countryDroplist];
        [self relationshipStatusMethod];
        
        NSMutableDictionary *countryName=[[NSMutableDictionary alloc]init];
        countryName=[_additionDispaly valueForKey:@"get_county_details"];
        
        if ([countryName isKindOfClass:[NSMutableDictionary class]])
        {
            NSString *countryValue=[countryName valueForKey:@"name"];
            
            if ([countryValue isEqualToString:@""]) {
                [_countyDropdownBTN setTitle:@"-" forState:UIControlStateNormal];
                
            }
            else{
                [_countyDropdownBTN setTitle:countryValue forState:UIControlStateNormal];
                
            }
            
        }
        else{
            [_countyDropdownBTN setTitle:@"Select " forState:UIControlStateNormal];
            
        }
        
        NSString *zip=[_additionDispaly valueForKey:@"zip"];
        if ([zip isEqualToString:@""]) {
            _zipcodeTXT.text=@"-";
            
        }
        else{
            _zipcodeTXT.text=zip;
            
        }
        
        NSString *city=[_additionDispaly valueForKey:@"city"];
        if ([city isEqualToString:@""]) {
            _cityTXT.text=@"-";
            
        }
        else{
            _cityTXT.text=city;
            
        }
        
        NSString *address=[_additionDispaly valueForKey:@"address"];
        if ([address isEqualToString:@""]) {
            _addressText.text=@"-";
            
        }
        else{
            _addressText.text=address;
            
        }
        
        NSString *inspirationa_saying=[_additionDispaly valueForKey:@"inspirationa_saying"];
        if ([inspirationa_saying isEqualToString:@""]) {
            _inspirationTXT.text=@"-";
            
        }
        else{
            _inspirationTXT.text=inspirationa_saying;
            
        }
        
        
        NSString *favourite_prayer=[_additionDispaly valueForKey:@"favourite_prayer"];
        if ([favourite_prayer isEqualToString:@""]) {
            _fovorituteTXT.text=@"-";
            
        }
        else{
            _fovorituteTXT.text=favourite_prayer;
            
        }
        
        
        NSString *children=[_additionDispaly valueForKey:@"children"];
        if ([children isEqualToString:@""]) {
            _childrenTXT.text=@"-";
            
        }
        else{
            _childrenTXT.text=children;
            
        }
        
        NSString *siblings=[_additionDispaly valueForKey:@"siblings"];
        if ([siblings isEqualToString:@""]) {
            _siblingTXT.text=@"-";
            
        }
        else{
            _siblingTXT.text=siblings;
            
        }
        
        NSString *jobtitle=[_additionDispaly valueForKey:@"job_title"];
        if ([jobtitle isEqualToString:@""]) {
            _jodtitleTXT.text=@"-";
            
        }
        else{
            _jodtitleTXT.text=jobtitle;
            
        }
        
        NSString *parish=[_additionDispaly valueForKey:@"parish"];
        if ([jobtitle isEqualToString:@""]) {
            _parishTXT.text=@"-";
        }
        else{
            _parishTXT.text=parish;
        }
        
        NSString *employer=[_additionDispaly valueForKey:@"employer"];
        if ([jobtitle isEqualToString:@""]) {
            _employeeTXT.text=@"-";
            
        }
        else{
            _employeeTXT.text=employer;
            
        }
        
        NSString *high_school=[_additionDispaly valueForKey:@"high_school"];
        if ([high_school isEqualToString:@""]) {
            _highschoollTXT.text=@"-";
            
        }
        else{
            _highschoollTXT.text=employer;
            
        }
        
        
        NSString *state=[_additionDispaly valueForKey:@"state"];
        if ([state isEqualToString:@""]) {
            _stateTXT.text=@"-";
            
        }
        else{
            _stateTXT.text=state;
            
        }
        
        
        
        
        NSString *relationship_status=[_additionDispaly valueForKey:@"relationship_status"];
        if ([relationship_status isEqualToString:@""]) {
            [_selectRelationBTN setTitle:@"Select" forState:UIControlStateNormal];
            
        }
        else{
            [_selectRelationBTN setTitle:relationship_status forState:UIControlStateNormal];
            
        }
        
    }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillLayoutSubviews{
    if (IS_IPHONE4||IS_IPHONE5) {
        _srcollView.contentSize=CGSizeMake(_srcollView.frame.size.width, 600);
        
    }
    
}
#pragma mark RelationshipStatusMethod

-(void)relationshipStatusMethod{
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"relationshipstatus" withParameters:nil withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]])  {
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        relationlist=[response valueForKey:@"data"];
                    }
                }
                else{
                    
                }
            }
        } errorBlock:^(id error)
         {
            [self.view makeToast:error  duration:1.0 position:CSToastPositionCenter];
             
         }];
        
    }
    
}
#pragma mark CountryDroplistMethod

-(void)countryDroplist{
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"getcountries" withParameters:nil withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]])  {
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        NSMutableArray *countrylist=nil;
                        countrylist=[response valueForKey:@"data"];
                        
                        countryArrayname=[countrylist valueForKey:@"name"];
                        countryArrayid=[countrylist valueForKey:@"id"];
                        
                        
                    }
                }
                else{
                    //                    dispatch_async(dispatch_get_main_queue(), ^{
                    //                        UIImage *placeholderImage = [UIImage imageNamed:@"profile_image"];
                    //                        self.profile_Picture.image=placeholderImage;
                    //                    });
                    //
                }
            }
        } errorBlock:^(id error)
         {
            [self.view makeToast:error  duration:1.0 position:CSToastPositionCenter];
   
         }];
        
    }
    
}

#pragma mark - Validation

- (NSString *)validateFields {
    NSString *errorMessage ;
    NSString *strJob = [self.jodtitleTXT.text removeWhiteSpaces];
    NSString *strParish = [self.parishTXT.text removeWhiteSpaces];
    NSString *strEmployee = [self.employeeTXT.text removeWhiteSpaces];
    NSString *strHigh = [self.highschoollTXT.text removeWhiteSpaces];
    NSString *strSibling = [self.siblingTXT.text removeWhiteSpaces];
    NSString *strchildren = [self.childrenTXT.text removeWhiteSpaces];
    NSString *strFavourite = [self.fovorituteTXT.text removeWhiteSpaces];
    NSString *strInspariartion = [self.inspirationTXT.text removeWhiteSpaces];
    
    if ([strJob length] == 0) {
        errorMessage = strjobEnter;
        return errorMessage;
    }
    if ([strParish length] == 0) {
        errorMessage = strParishEnter;
        return errorMessage;
    }
    
    if ([strEmployee length] == 0) {
        errorMessage =strEmployeeEnter;
        return errorMessage;
    }
    if ([strHigh length] == 0) {
        errorMessage = strHighEnter;
        return errorMessage;
    }
    if ([strSibling length] == 0) {
        errorMessage =strSiblingEnter;
        return errorMessage;
    }
    if ([strchildren length] == 0) {
        errorMessage = strchildrenEnter;
        return errorMessage;
    }
    if ([strFavourite length] == 0) {
        errorMessage =strFavouriteEnter;
        return errorMessage;
    }
    if ([strInspariartion length] == 0) {
        errorMessage = strInspariartionEnter;
        return errorMessage;
    }
    return 0;
}


#pragma mark SelectRelationMethod
- (IBAction)selectRelationMethod:(id)sender {
    [self.view endEditing:YES];
    [[NSUserDefaults standardUserDefaults]setObject:@"selectRelationMethod123" forKey:@"selectRelationMethod123"];
    if(dropDown == nil) {
        CGFloat f = 200;
        [[[sender superview] superview] setTag:143];
        [[sender superview]  setTag:147];
        dropDown = [[NIDropDown alloc]showDropDown:[[sender superview]superview] :&f :relationlist :nil :@"down" button:sender ];
        dropDown.tag=222;
        
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
    
}

#pragma mark SelectCountryMethod
- (IBAction)selectCountryMethod:(id)sender {
    [self.view endEditing:YES];
    [[NSUserDefaults standardUserDefaults]setObject:@"Dropdowncountry" forKey:@"Dropdowncountry"];
    if(dropDown == nil) {
        if ([countryArrayname count]!=0) {
            CGFloat f = 200;
            [[[sender superview] superview] setTag:119];
            [[sender superview]  setTag:120];
            dropDown = [[NIDropDown alloc]showDropDown:[[sender superview]superview] :&f :countryArrayname :nil :@"down" button:sender ];
            dropDown.tag=333;
            dropDown.delegate = self;
            
        }
        else{
            [dropDown hideDropDown:sender];
            [self rel];
            
        }
        
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
    
}


#pragma  NIDropdown delagte
- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [sender removeFromSuperview];
    sender = nil;
    [self rel];
}

-(void)rel{
    dropDown = nil;
}

- (IBAction)back_act:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSString *strMobile=self.zipcodeTXT.text;
    
    if (textField==self.zipcodeTXT) {
        
        BOOL isMobile = [Constant1 myZipNumberValidate:strMobile];
        if (!isMobile) {
            [self.view makeToast:zipNumber duration:1.0 position:CSToastPositionCenter];
        }
        
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSString *strMobile=self.zipcodeTXT.text;
    if ([textField.text isEqualToString:@"-"]) {
        textField.text=@"";
    }
    if (textField==self.zipcodeTXT) {
        BOOL isMobile = [Constant1 myZipNumberValidate:strMobile];
        if (!isMobile) {
            [self.view makeToast:zipNumber duration:1.0 position:CSToastPositionCenter];
        }
        
    }
    
    textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
}

#pragma mark SaveadditioninfoView

-(IBAction)saveadditioninfoView:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        NSString *idcountry;
        NSInteger valuecopuntryid =[[NSUserDefaults standardUserDefaults] integerForKey:@"indexpathcountry"];
        for (int i=0; i<[countryArrayid count]; i++) {
            if (valuecopuntryid==i) {
                idcountry=[countryArrayid objectAtIndex:i];
//                NSLog(@"%@",[countryArrayid objectAtIndex:i]);
//                NSLog(@"country is is %@",idcountry);
                
                break;
            }
            
        }

    
    NSString *strMobile=self.zipcodeTXT.text;;
    BOOL isMobile = [Constant1 myZipNumberValidate:strMobile];
    if (!isMobile) {
        [self.view makeToast:zipNumber duration:1.0 position:CSToastPositionCenter];
    }
    else{
        
        NSString *imageValue12=[[NSUserDefaults standardUserDefaults]objectForKey:@"imageValue"];
        NSDictionary *dict;
        if (imageValue12==nil) {
            dict = @{
                     @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                     @"token" : [[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                     @"first_name" :[[NSUserDefaults standardUserDefaults]objectForKey:@"nameupdate"],
                     @"last_name" :[[NSUserDefaults standardUserDefaults]objectForKey:@"lastnameupdate"],
                     @"date_of_birth" :[[NSUserDefaults standardUserDefaults]objectForKey:@"dobupdate"],
                     @"phone" :[[NSUserDefaults standardUserDefaults] objectForKey:@"phone"],
                     @"job_title" :self.jodtitleTXT.text,
                     @"parish" : self.parishTXT.text,
                     @"employer" :self.employeeTXT.text,
                     @"high_school" : self.highschoollTXT.text,
                     @"relationship_status" :self.selectRelationBTN.titleLabel.text,
                     @"children" : self.childrenTXT.text,
                     @"favourite_prayer" :self.fovorituteTXT.text,
                     @"inspirationa_saying" : self.inspirationTXT.text,
                     @"city" : self.cityTXT.text,
                     @"zip" : self.zipcodeTXT.text,
                     @"country" :idcountry,
                     @"state" :self.stateTXT.text,
                     @"siblings" :self.siblingTXT.text,
                     @"denomination" :[[NSUserDefaults standardUserDefaults] objectForKey:@"denominationValue"],
//                     @"image":imageValue12,
                     };
        }
        else{
            dict = @{
                     @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                     @"token" : [[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                     @"first_name" :[[NSUserDefaults standardUserDefaults]objectForKey:@"nameupdate"],
                     @"last_name" :[[NSUserDefaults standardUserDefaults]objectForKey:@"lastnameupdate"],
                     @"date_of_birth" :[[NSUserDefaults standardUserDefaults]objectForKey:@"dobupdate"],
                     @"phone" :[[NSUserDefaults standardUserDefaults] objectForKey:@"phone"],
                     @"job_title" :self.jodtitleTXT.text,
                     @"parish" : self.parishTXT.text,
                     @"employer" :self.employeeTXT.text,
                     @"high_school" : self.highschoollTXT.text,
                     @"relationship_status" :self.selectRelationBTN.titleLabel.text,
                     @"children" : self.childrenTXT.text,
                     @"favourite_prayer" :self.fovorituteTXT.text,
                     @"inspirationa_saying" : self.inspirationTXT.text,
                     @"city" : self.cityTXT.text,
                     @"zip" : self.zipcodeTXT.text,
                     @"country" :idcountry,
                     @"state" :self.stateTXT.text,
                     @"siblings" :self.siblingTXT.text,
                     @"denomination" :[[NSUserDefaults standardUserDefaults] objectForKey:@"denominationValue"],
                     @"image":imageValue12,
                     
                     };
            
        }
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"update" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            NSMutableArray *messageArray = nil;
                            messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            
                        }
                        
                        else{
                            NSMutableArray *messageArray = nil;
                            messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
    }
    
}


@end
