//
//  AdditionalDisplayView.h
//  wireFrameSplash
//
//  Created by home on 6/15/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdditionalDisplayView : UIViewController
-(IBAction)backDisplayView:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *jobLBL;

@property (weak, nonatomic) IBOutlet UILabel *parishLBL;
@property (weak, nonatomic) IBOutlet UILabel *employeeLBL;

@property (weak, nonatomic) IBOutlet UILabel *highSchoolLBL;

@property (weak, nonatomic) IBOutlet UILabel *relationLBL;
@property (weak, nonatomic) IBOutlet UILabel *siblingLBL;

@property (weak, nonatomic) IBOutlet UILabel *childrenLBL;

@property (weak, nonatomic) IBOutlet UILabel *favouriteLBL;

@property (weak, nonatomic) IBOutlet UILabel *inspirationLBL;
@property (weak, nonatomic) IBOutlet UILabel *addressLBL;
@property (weak, nonatomic) IBOutlet UILabel *cityLBL;

@property (weak, nonatomic) IBOutlet UILabel *zipcodeLBL;

@property (weak, nonatomic) IBOutlet UILabel *stateLBL;

@property (weak, nonatomic) IBOutlet UILabel *countryLBL;
@property (strong, nonatomic) NSMutableDictionary *additionDispaly;

@property (strong, nonatomic) NSString *checkdict;


@end
