//
//  YahooContactViewController.m
//  wireFrameSplash
//
//  Created by home on 4/1/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "YahooContactViewController.h"
#import "SearchResultsTableViewController.h"
#import "addressbookTableViewCell.h"
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
@interface YahooContactViewController ()<MFMailComposeViewControllerDelegate>{
    NSMutableDictionary *details;
    NSMutableArray *objects;
    NSString *emailData;
    NSMutableArray *emailArray;
    UIButton *buttonInvite;
    NSMutableArray *yahooArray;
}

@end

@implementation YahooContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Invite Friend";
    
    UINavigationController *searchResultsController = [[self storyboard] instantiateViewControllerWithIdentifier:@"TableSearchResultsNavController"];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];
    self.searchController.searchResultsUpdater = self;
    
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x,
                                                       self.searchController.searchBar.frame.origin.y,
                                                       self.searchController.searchBar.frame.size.width, 44.0);
    self.tableView.tableHeaderView = self.searchController.searchBar;
    _airlines=[[NSMutableArray alloc]initWithObjects:@"A",@"B",@"C",@"D",nil];
    self.navigationController.navigationBar.topItem.title =@"";
    yahooArray=[[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //  [self.navigationItem setHidesBackButton:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [objects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    addressbookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"address" forIndexPath:indexPath];
    
    
    
    NSString *fullname=[[[objects objectAtIndex:indexPath.row]valueForKey:@"lastname"]  stringByAppendingString:[NSString stringWithFormat:@"  %@",[[objects objectAtIndex:indexPath.row]valueForKey:@"firstname"] ]];
    cell.ContactName.text=fullname;
    
    [cell.RightBTN  addTarget:self action:@selector(TickClickBTN:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footer=[[UIView alloc] initWithFrame:CGRectMake(0,0,self.tableView.frame.size.width,50.0)];
    footer.layer.borderWidth = 1;
    footer.layer.borderColor =[UIColor colorWithRed:189.0/255.0f green:189.0/255.0f blue:189.0/255.0f alpha:1.0].CGColor;
    buttonInvite = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonInvite setTitle:@"Invites" forState:UIControlStateNormal];
    buttonInvite.frame = CGRectMake(self.tableView.frame.size.width/2-50, 50,100,30);
    buttonInvite.layer.cornerRadius = 5;
    buttonInvite.layer.borderWidth = 1;
    buttonInvite.backgroundColor=[UIColor redColor];
    buttonInvite.layer.borderColor =[UIColor colorWithRed:189.0/255.0f green:189.0/255.0f blue:189.0/255.0f alpha:1.0].CGColor;
    [buttonInvite  addTarget:self action:@selector(InvitesFriendMethod:) forControlEvents:UIControlEventTouchUpInside];
    [footer addSubview:buttonInvite];
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 135.0f;
}


-(void)TickClickBTN:(UIButton*)sender {
    sender.selected=!sender.selected;
    addressbookTableViewCell *cell1 = (addressbookTableViewCell *)sender.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell1];
    if (cell1.RightBTN.selected==YES) {
        NSMutableDictionary *dictData;
        dictData=[objects objectAtIndex:indexPath.row];
        emailData=[dictData  valueForKey:@"email"];
        emailArray =[[self  convertToArrayFromCommaSeparated:emailData] mutableCopy];
        [yahooArray addObject:emailArray];
    }
 
    
}

-(NSArray *)convertToArrayFromCommaSeparated:(NSString*)string{
    return [string componentsSeparatedByString:@","];
}


-(void)InvitesFriendMethod:(UIButton*)sender {
    
    if (emailArray.count==0) {
        UIAlertView *at=[[UIAlertView alloc]initWithTitle:@"Please Select !" message:@"Please tick " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [at show];
    }
    else
    {
        NSString *emailTitle = @"Hello";
        NSString *messageBody = @"Hey, check this out!";
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:yahooArray];
        yahooArray=nil;
        [self presentViewController:mc animated:YES completion:NULL];
        
        
    }
    
}

#pragma mail delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{   UIAlertView *at;
    switch (result)
    {
        case MFMailComposeResultCancelled:
//            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
//            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            at=[[UIAlertView alloc]initWithTitle:@"Please Check !" message:@"Mail sent " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [at show];
            break;
        case MFMailComposeResultFailed:
//            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}




#pragma mark - UISearchControllerDelegate & UISearchResultsDelegate

// Called when the search bar becomes first responder
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchString = self.searchController.searchBar.text;
    [self updateFilteredContentForAirlineName:searchString];
    if (self.searchController.searchResultsController) {
        UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
        SearchResultsTableViewController *vc = (SearchResultsTableViewController *)navController.topViewController;
        vc.searchResults = self.searchResults;
        [vc.tableView reloadData];
    }
}

- (void)updateFilteredContentForAirlineName:(NSString *)searchString {
    if ([searchString length]>0)
    {
        if (searchString == nil) {
            //            self.searchResults = [flatArray mutableCopy];
        } else {
            NSMutableArray *searchResults =nil;
            NSPredicate *resultPredicate=nil;
//          resultPredicate = [NSPredicate predicateWithFormat:@"name beginswith[c] %@", searchString];
            //            NSArray *flatArray = [[dict allValues] valueForKeyPath: @"@unionOfArrays.@self"];
            //            searchResults = [[[flatArray filteredArrayUsingPredicate:resultPredicate]valueForKey:@"name"] mutableCopy];
            self.searchResults = searchResults;
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)loadContactList:(NSArray *)contactList{
    
    
    objects=[[NSMutableArray alloc]init];
    details=[[NSMutableDictionary alloc]init];
    
    for (NSDictionary *prod in contactList) {
        
        [details setObject:[[prod objectForKey:@"yahooid"] objectForKey:@"value"] forKey:@"username"];
        [details setObject:[[prod objectForKey:@"name"] objectForKey:@"familyName"] forKey:@"firstname"];
        [details setObject:[[prod objectForKey:@"name"] objectForKey:@"givenName"] forKey:@"lastname"];
        [details setObject:[[prod objectForKey:@"email"] objectForKey:@"value"] forKey:@"email"];
        
    }
    
    if (details==nil) {
        
    }else {
        
        
        [objects addObject:details];
        
        [_tableView reloadData];
    }
}

@end
