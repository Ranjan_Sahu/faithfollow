//
//  YahooContactViewController.h
//  wireFrameSplash
//
//  Created by home on 4/1/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YahooContactViewController : UIViewController<UISearchResultsUpdating>
@property(strong,nonatomic)IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *airlines;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;

- (void)loadContactList:(NSArray *)contactList;


@end
