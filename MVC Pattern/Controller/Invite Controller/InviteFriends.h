//
//  InviteFriends.h
//  wireFrameSplash
//
//  Created by home on 4/1/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InviteFriends.h"

#import "YahooContactViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "ContactViewController.h"
@interface InviteFriends : UIViewController
-(IBAction)LoginWithFacebook:(id)sender;
-(IBAction)LoginWithYahoo:(id)sender;
-(IBAction)ContactList:(id)sender;
-(IBAction)SelectContact:(id)sender;
@property(weak,nonatomic)IBOutlet UIImageView *imageViewProfile;
@property (strong, nonatomic) IBOutlet UIButton *facebook_btn;
@property (strong, nonatomic) IBOutlet UIButton *contact_btn;
@property (strong, nonatomic) IBOutlet UIButton *yahoo_btn;
@property(weak,nonatomic)IBOutlet UIScrollView *scrollView;

@end
