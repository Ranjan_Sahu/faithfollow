//
//  ContactViewController.h
//  wireFrameSplash
//
//  Created by home on 4/4/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate>
@property(weak,nonatomic)IBOutlet UITableView *tableView;
@property(strong,nonatomic)NSMutableArray *contactArray;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) NSMutableArray *airlines;
@property(nonatomic,weak)IBOutlet  UISearchBar *searchbar;
@property (nonatomic, assign) bool isFiltered;



@end
