//
//  ContactViewController.m
//  wireFrameSplash
//
//  Created by home on 4/4/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "ContactViewController.h"

#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "addressbookTableViewCell.h"
#import <MessageUI/MessageUI.h>
#import "UIImageView+WebCache.h"
#import "Constant1.h"
#import "InviteFriends.h"
@import Contacts;

@interface ContactViewController ()<ABPeoplePickerNavigationControllerDelegate,MFMessageComposeViewControllerDelegate,UINavigationControllerDelegate>{
    NSMutableArray*  MutableArray__Contact;
    NSString *emailData;
    NSMutableArray *emailArray;
    NSMutableArray *objects;
    NSMutableArray *phoneNumberArray;
   }
@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBookController;
@property (nonatomic, strong) NSMutableArray *arrContactsData;
@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    emailArray=[[NSMutableArray alloc]init];
    _searchResults=[[NSMutableArray alloc]init];
    
    //navigation left button
    UIImage *listImage = [UIImage imageNamed:@"leftArrow.png"];
    UIButton *listButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [listButton setFrame:CGRectMake(5, 4, 30, 30)];
    [listButton setImage:listImage forState:UIControlStateNormal];
    [listButton  addTarget:self action:@selector(Back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *jobsButton = [[UIBarButtonItem alloc] initWithCustomView:listButton];
    self.navigationItem.leftBarButtonItem = jobsButton;
     //naviagtion item
//    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]} ];
//    self.navigationController.navigationBar.translucent = NO;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-50,20, 100, 44)];
    label.center = CGPointMake(self.view.frame.size.width/2, label.center.y);
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Avenir-Medium" size:20];
    label.textColor =[UIColor whiteColor];
    label.text=@"Invite Friends";
    self.navigationItem.titleView = label;
    
    // Customize SearchBar
    UITextField *TF_Search = (UITextField *)[self.searchbar valueForKey:@"searchField"];
    TF_Search.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1];
    TF_Search.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:242.0/255.0 blue:243.0/255.0 alpha:1];
    self.searchbar.barTintColor = [UIColor whiteColor];
    self.searchbar.tintColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1];
    self.searchbar.placeholder = @"Search";
    
    UILabel *TF_Label = (UILabel *)[TF_Search valueForKey:@"placeholderLabel"];
    TF_Label.textColor = [UIColor colorWithRed:151.0/255.0 green:175.0/255.0 blue:191.0/255.0 alpha:1];
    
    UIImageView *glassImg = (UIImageView *)TF_Search.leftView;
    //glassImg.image = [UIImage imageNamed:@"searchIcon30"];
    glassImg.image = [glassImg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    glassImg.tintColor = [UIColor colorWithRed:151.0/255.0 green:175.0/255.0 blue:191.0/255.0 alpha:1];
    
    self.searchbar.backgroundImage = [UIImage new];

    
    NSString *version = [[UIDevice currentDevice] systemVersion];
    BOOL isVersion8 = [version hasPrefix:@"8."];
    BOOL isVersion7 = [version hasPrefix:@"7."];
    if(isVersion7 || isVersion8){
        //Use AddressBook
        [self addressBook];
    }
    else{
        //Use Contacts
        [self Contactsbook];
    }
    self.tableView.tableFooterView=[UIView new];
    
}
- (void)viewDidUnload
{
    //    [self setSearchBar:nil];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)Back{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)Contactsbook{
    
    _contactArray=[[NSMutableArray alloc]init];
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
//            NSLog(@"main contact %@",cnContacts);
            if (error) {
//                NSLog(@"error fetching contacts %@", error);
            } else {
                NSString *phone;
                NSString *fullName;
                NSString *firstName;
                NSString *lastName;
                UIImage *profileImage;
                NSMutableArray *contactNumbersArray=[[NSMutableArray alloc] init];
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    else{
                        fullName=[NSString stringWithFormat:@"%@  %@",firstName,lastName];
                    }
                    UIImage *image = [UIImage imageWithData:contact.imageData];
                    if (image != nil) {
                        profileImage = image;
                    }else{
                        profileImage = [UIImage imageNamed:@"person-icon.png"];
                    }
                    for (CNLabeledValue *label in contact.phoneNumbers) {
                        phone = [label.value stringValue];
                        if ([phone length] > 0) {
                            [contactNumbersArray addObject:phone];
                        }
                    }
//                    NSLog(@"phone number is %@",contactNumbersArray);
                    if (profileImage==nil) {
                        UIImage *placeholderImage = [UIImage imageNamed:@"profile_image"];
                        profileImage=placeholderImage;
                    }
                    
                    NSDictionary* personDict = [[NSDictionary alloc] initWithObjectsAndKeys: fullName,@"fullName",profileImage,@"userImage",phone,@"PhoneNumbers", nil];
                    [_contactArray addObject:personDict];
                    
                }
                
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   // NSLog(@"%@",ar_Contact);
                                   [self.tableView reloadData];
                               });
            }
        }
    }];
    
//    NSLog(@"contact array is %@",_contactArray);
    
}
-(void)addressBook{
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    
    if (status == kABAuthorizationStatusDenied || status == kABAuthorizationStatusRestricted) {
        
        [[[UIAlertView alloc] initWithTitle:nil message:@"This app requires access to your contacts to function properly. Please visit to the \"Privacy\" section in the iPhone Settings app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    
    if (!addressBook) {
//        NSLog(@"ABAddressBookCreateWithOptions error: %@", CFBridgingRelease(error));
        return;
    }
    
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        if (error) {
//            NSLog(@"ABAddressBookRequestAccessWithCompletion error: %@", CFBridgingRelease(error));
        }
        
        if (granted) {
            // if they gave you permission, then just carry on
            
            [self listPeopleInAddressBook:addressBook];
        } else {
            // however, if they didn't give you permission, handle it gracefully, for example...
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // BTW, this is not on the main thread, so dispatch UI updates back to the main queue
                
                [[[UIAlertView alloc] initWithTitle:nil message:@"This app requires access to your contacts to function properly. Please visit to the \"Privacy\" section in the iPhone Settings app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            });
        }
        
        CFRelease(addressBook);
    });
    
}
- (void)listPeopleInAddressBook:(ABAddressBookRef)addressBook
{
    NSArray *allPeople = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
    NSInteger numberOfPeople = [allPeople count];
    _contactArray=[[NSMutableArray alloc]init];
    for (NSInteger i = 0; i < numberOfPeople; i++) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        ABRecordRef person = (__bridge ABRecordRef)allPeople[i];
        NSString *firstName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName  = CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
        NSString*  fullName=[NSString stringWithFormat:@"%@  %@",firstName,lastName];
//        NSLog(@"Name:%@ %@", firstName, lastName);
        [dict setObject:fullName forKey:@"fullName"];
        //        to get mobile number
        ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
        CFIndex numberOfPhoneNumbers = ABMultiValueGetCount(phoneNumbers);
        for (CFIndex i = 0; i < numberOfPhoneNumbers; i++) {
            NSString *phoneNumber=nil;
             phoneNumber = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phoneNumbers, i));
        }
        CFRelease(phoneNumbers);
        [self.contactArray addObject:dict];
    }
}

-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    return NO;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    NSInteger rowCount;
    
    if(self.isFiltered)
        
        rowCount = _searchResults.count;
    
    else
        
        rowCount = _contactArray.count;
    
    return rowCount;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    addressbookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"address" forIndexPath:indexPath];
    NSDictionary *dictData;
    if(_isFiltered){
        dictData=[_searchResults objectAtIndex:indexPath.row];
         cell.ContactName.text=[[_searchResults objectAtIndex:indexPath.row]valueForKey:@"fullName"];
    }else{
        dictData=[_contactArray objectAtIndex:indexPath.row];
      cell.ContactName.text=[[_contactArray objectAtIndex:indexPath.row]valueForKey:@"fullName"];
    }
   
    UIImage *placeholderImage = [UIImage imageNamed:@"user"];    
    NSString *phoneNum=[dictData  valueForKey:@"PhoneNumbers"];

    cell.RightBTN.selected=NO;
    if([emailArray containsObject:phoneNum]){
        cell.RightBTN.selected=YES;
    }
    
    cell.imageViewContact.image=placeholderImage;
    [cell.RightBTN  addTarget:self action:@selector(TickClickBTN1:) forControlEvents:UIControlEventTouchUpInside];
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - UISearchControllerDelegate & UISearchResultsDelegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length == 0)
    {
        _isFiltered = FALSE;
    }
    else{
        _isFiltered = true;
            NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"fullName beginswith[c]%@", searchText];        
        _searchResults = [[_contactArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    [self.tableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    _isFiltered = false;
    [_searchResults removeAllObjects];
    [_searchbar resignFirstResponder];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(void)TickClickBTN1:(UIButton*)sender {
    
    [_searchbar resignFirstResponder];
   
    addressbookTableViewCell *cell1 = (addressbookTableViewCell *)sender.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell1];
    
        NSMutableDictionary *dictData;
    
        if (self.isFiltered) {
        dictData=[_searchResults objectAtIndex:indexPath.row];
        }
        else{
            dictData=[_contactArray objectAtIndex:indexPath.row];
        }
        
        emailData=[dictData  valueForKey:@"PhoneNumbers"];
    
        if(![emailArray containsObject:emailData])
        {
            [emailArray addObject:emailData];
        }
        else
        {
            [emailArray removeObject:emailData];
        }
        [self.tableView reloadData];
}


-(NSArray *)convertToArrayFromCommaSeparated:(NSString*)string{
    return [string componentsSeparatedByString:@","];
}

#pragma mail delegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
        case MessageComposeResultFailed:
            
            break;
        case MessageComposeResultSent:
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(IBAction)InvitesFriendMethod:(id)sender {
    if (emailArray.count==0) {
        UIAlertView *at=[[UIAlertView alloc]initWithTitle:appTitle message:@"Please select " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [at show];
    }
    else
    {
        if([MFMessageComposeViewController canSendText])
        {
            MFMessageComposeViewController  *messegeController = [[MFMessageComposeViewController alloc] init];
            messegeController.body = contactInviteText;
            messegeController.recipients = emailArray;
            messegeController.messageComposeDelegate = self;
            [self presentViewController:messegeController animated:YES completion:NULL];
        }
        else{
            UIAlertView *at=[[UIAlertView alloc]initWithTitle:appTitle message:@"Device doesn't support this feature." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [at show];

        }
        
    }
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
