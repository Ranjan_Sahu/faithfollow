//
//  InviteFriends.m
//  wireFrameSplash
//
//  Created by home on 4/1/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "InviteFriends.h"
#import "AppDelegate.h"
#import "Mixpanel.h"
#import "YahooContactViewController.h"
#import <FBSDKShareKit/FBSDKShareKit.h>

#import "DeviceConstant.h"

@interface InviteFriends ()<FBSDKAppInviteDialogDelegate>
{
}
@end

@implementation InviteFriends

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.topItem.title =@"";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationItem.title = @"Invite Friend";
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    UIImage *listImage = [UIImage imageNamed:@"leftArrow"];
    UIButton *listButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [listButton setFrame:CGRectMake(0, 0, 30, 30)];
    [listButton setImage:listImage forState:UIControlStateNormal];
    //[listButton setBackgroundImage:listImage forState:UIControlStateNormal];
    [listButton  addTarget:self action:@selector(cancelBTN) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *infoButtonImage = [[UIBarButtonItem alloc]initWithCustomView:listButton ];
    self.navigationItem.leftBarButtonItem =infoButtonImage;

    _facebook_btn.layer.borderWidth = 1.0f;
    _facebook_btn.layer.borderColor = [UIColor colorWithRed:238.0/255.0 green:242.0/255.0  blue:243.0/255.0  alpha:1.0].CGColor;
    
}

-(void)cancelBTN{
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)viewWillLayoutSubviews{
    if (IS_IPHONE4) {
        _scrollView.contentSize=CGSizeMake(_scrollView.frame.size.width, 550);
    }
}



-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.topItem.title =@"";
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"Invite Friend";
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [[AppDelegate sharedAppDelegate] removeadd];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)LoginWithFacebook:(id)sender{
    
    [TenjinSDK sendEventWithName:@"click_invite_friends_from_facebook"];
    [FBSDKAppEvents logEvent:@"click_invite_friends_from_facebook"];
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"click_invite_friends_from_facebook"]];
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"click_invite_friends_from_facebook"];

    
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (!app.fbManager) {
        app.fbManager = [[FBSDKLoginManager alloc] init];
    }
    [app.fbManager logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *fbresult, NSError *error)
     {
         if (error)
         {
//             [self.view makeToast:(NSString*)error duration:1.0 position:CSToastPositionCenter];
//             NSLog(@"Process error");
             //             UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please Note" message:@"Process error." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             //             [message show];
         }
         else if (fbresult.isCancelled)
         {
             //             UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please Note" message:@"Cancelled ." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             //             [message show];
         }
         else
         {
             
             [self invite];
             
         }
     }];
    
}

-(void)invite{
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
    // From plist file 1619269094960491
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/1649794328574634"];
     //content.appLinkURL = [NSURL URLWithString:@"https://fb.me/1619269094960491"];
    content.appInvitePreviewImageURL = [NSURL URLWithString:postLogo_invite];
    [FBSDKAppInviteDialog showFromViewController:self
                                     withContent:content
                                        delegate:self];
//    [FBSDKAppInviteDialog showFromViewController:self withContent:content delegate:self];
}

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog
       didFailWithError:(NSError *)error{
//    NSLog(@"facebook %@",error.localizedDescription);
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appTitle message:@"There may be some issue , Please try after some time" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alert show];
}
- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog
 didCompleteWithResults:(NSDictionary *)results{
    
    if (results) {
        if(![results objectForKey:@"completionGesture"])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appTitle message:@"Invited successfully!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
        }
    }
    
    
}

-(IBAction)LoginWithYahoo:(id)sender{
//    // [self didReceiveAuthorization];
//    AppDelegate*appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    [appDelegate createYahooSession];
}
-(IBAction)ContactList:(id)sender{
    [TenjinSDK sendEventWithName:@"click_invite_friends_from_contacts"];
    [FBSDKAppEvents logEvent:@"click_invite_friends_from_contacts"];
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"click_invite_friends_from_contacts"]];
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"click_invite_friends_from_contacts"];
    ContactViewController *contactView=[self.storyboard instantiateViewControllerWithIdentifier:@"ContactViewController"];
    [self.navigationController pushViewController:contactView animated:YES];
}

-(IBAction)SelectContact:(id)sender{
    [self invite];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
