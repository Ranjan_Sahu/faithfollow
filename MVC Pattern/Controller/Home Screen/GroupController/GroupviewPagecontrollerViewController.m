//
//  GroupviewPagecontrollerViewController.m
//  wireFrameSplash
//
//  Created by home on 6/24/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "GroupviewPagecontrollerViewController.h"
#import "GroupSearchViewController.h"
#import "GroupDiscussionViewController.h"
#import "MyGroupViewController.h"
#import "AppDelegate.h"
#import "DetailMygroupViewController1.h"

@interface GroupviewPagecontrollerViewController ()
{
    NSMutableArray *grouplistArray,*webSearchArraypage;
}
@end

@implementation GroupviewPagecontrollerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //    NSLog(@"che3ckaddsub view %@",_checkaddsubview);
    
    //    [_mycommunity_btn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    //    [_search_btn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    //    [_friendrequest_btn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    
    // Create the data model
    _pageTitles = @[@"Over 200 Tips and Tricks", @"Discover Hidden Features", @"Bookmark Favorite Tip"];
    _pageImages = @[@"page1.png", @"page2.png", @"page3.png"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = (id)self;
    MyGroupViewController *group=[self.storyboard instantiateViewControllerWithIdentifier:@"MyGroupViewController"];
    NSArray *viewControllers = @[group];
    
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageViewController.view.frame = CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height );
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    MyGroupViewController *pageContentViewController1=[self.storyboard instantiateViewControllerWithIdentifier:@"MyGroupViewController"];
    return pageContentViewController1;
}

#pragma mark - Page View Controller Data Source

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    //     [_friendrequest_btn setTitleColor:[UIColor colorWithRed:134.0/255.0f green:134.0/255.0f  blue:134.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
    //    [_mycommunity_btn setTitleColor:[UIColor colorWithRed:134.0/255.0f green:134.0/255.0f  blue:134.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
    //    [_search_btn setTitleColor:[UIColor colorWithRed:134.0/255.0f green:134.0/255.0f  blue:134.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
    //
    //    [_friendrequest_btn setTitleColor:[UIColor colorWithRed:0/255.0f green:173.0/255.0f  blue:235.0/255.0f  alpha:1.0f]  forState:UIControlStateSelected];
    //    [_mycommunity_btn setTitleColor:[UIColor colorWithRed:0/255.0f green:173.0/255.0f  blue:235.0/255.0f  alpha:1.0f]  forState:UIControlStateSelected];
    //
    //    [_search_btn setTitleColor:[UIColor colorWithRed:0/255.0f green:173.0/255.0f  blue:235.0/255.0f  alpha:1.0f]  forState:UIControlStateSelected];
    
    
    if (IS_IPHONE4){
        _topconstraint.constant=0;
    }
    
    NSString *seachGroupStr=[[NSUserDefaults standardUserDefaults] valueForKey:@"SearchGroup"];
    
    if ([seachGroupStr isEqualToString:@"groupSearch"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SearchGroup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self open_groupSearchViewController];
    }else{
        NSString *checkcondiction=[[NSUserDefaults standardUserDefaults] objectForKey:@"backcheckYes"];
        NSString *checkcondictionDis=[[NSUserDefaults standardUserDefaults] objectForKey:@"backDiscuss"];
        NSString *checkdetailgroup=[[NSUserDefaults standardUserDefaults] objectForKey:@"detailgroup"];
        
        if ([checkdetailgroup isEqualToString:@"detail"]) {
            _search_btn.selected=true;
            _mycommunity_btn.selected=false;
            _friendrequest_btn.selected=false;
            _bckview3.hidden=NO;
            _bckView1.hidden=YES;
            _bckVIew2.hidden=YES;
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"detailgroup"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self open_groupSearchViewController];
            return;
        }
        
        
        if ([checkcondictionDis isEqualToString:@"backDiscuss"]) {
            _search_btn.selected=false;
            _mycommunity_btn.selected=true;
            _friendrequest_btn.selected=false;
            _bckview3.hidden=YES;
            _bckView1.hidden=YES;
            _bckVIew2.hidden=NO;
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"backDiscuss"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self mycommunity_act:nil];
        }
        else{
            _search_btn.selected=false;
            _mycommunity_btn.selected=false;
            _friendrequest_btn.selected=true;
            _bckview3.hidden=YES;
            _bckView1.hidden=NO;
            _bckVIew2.hidden=YES;
            if ([checkcondiction isEqualToString:@"backcheck"]) {
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"backcheckYes"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else{
                [self mygroupListMethodpagecontroller];
            }
        }
    }
}

#pragma mark mygroupListMethodpagecontrollerMethod

-(void)mygroupListMethodpagecontroller{
   
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
       // [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    [[AppDelegate sharedAppDelegate] showhud];
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           };
//    NSLog(@"disct is%@",dict);
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:communitylist withParameters:dict withHud:YES success:^(id response){
//        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[AppDelegate sharedAppDelegate] hidehud];
                        grouplistArray=[response valueForKey:@"data"];
                        if([grouplistArray isKindOfClass:[NSArray class]])
                        {
                            if([grouplistArray count] > 0)
                            {
                                [self freiend_act:nil];
                            }
                            else
                            {
                                [self open_groupSearchViewController];
                            }
                        }
                        else
                        {
                            [self open_groupSearchViewController];
                        }
                        
                    });
                }
            }
        }
        else{
            
            NSMutableArray  *messageArray=[response valueForKey:@"message"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[AppDelegate sharedAppDelegate] hidehud];
                if ([messageArray count]>0) {
                   [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                }
               
                [self open_groupSearchViewController];

            });
        }
        
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
     }];
        
    }
    
}

-(void)checkpagecontroller:(NSNotification *) notification{
//    NSDictionary *dict = [notification userInfo];
//    NSLog(@"dict %@",dict);
//    [self freiend_act:nil];
//    NSDictionary *dict=[webSearchArraypage objectAtIndex:indexPath.row];
//    NSDictionary *getcommuntiyDict=[dict valueForKey:@"get_community"];
//    DetailMygroupViewController1 *detailView=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
//    detailView.navigationTitle=[getcommuntiyDict valueForKey:@"name"];
//    detailView.idUser=[dict valueForKey:@"community_id"];
//    NSString *commid=[NSString stringWithFormat:@"%@",[dict valueForKey:@"community_id"]];
//    [[NSUserDefaults standardUserDefaults] setObject:commid forKey:@"comid"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    [self.navigationController pushViewController:detailView animated:YES];
    
    
//        DetailMygroupViewController1 *detailpush=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
//        [self.navigationController pushViewController:detailpush animated:YES];
}


- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (IBAction)search_act:(id)sender {
    [self open_groupSearchViewController];
}

#pragma mark open_groupSearchViewControllerMethod

-(void)open_groupSearchViewController{
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
//        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    GroupSearchViewController *thirdView=[self.storyboard instantiateViewControllerWithIdentifier:@"GroupSearchViewController"];
    NSArray *viewControllers = @[thirdView];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
   _mycommunity_btn.selected=false;
    _search_btn.selected=true;
    _friendrequest_btn.selected=false;
    _bckview3.hidden=NO;
    _bckView1.hidden=YES;
    _bckVIew2.hidden=YES;
    }
}

#pragma mark Mycommunity_actMethod

- (IBAction)mycommunity_act:(id)sender {
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
       // [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    GroupDiscussionViewController *second=[self.storyboard instantiateViewControllerWithIdentifier:@"GroupDiscussionViewController"];
    NSArray *viewControllers = @[second];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    _bckview3.hidden=YES;
    _bckView1.hidden=YES;
    _bckVIew2.hidden=NO;
    _mycommunity_btn.selected=true;
    _search_btn.selected=false;
    _friendrequest_btn.selected=false;
        
    }
}

#pragma mark freiend_actMethod

- (IBAction)freiend_act:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    MyGroupViewController *first=[self.storyboard instantiateViewControllerWithIdentifier:@"MyGroupViewController"];
    NSArray *viewControllers = @[first];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    _mycommunity_btn.selected=false;
    _search_btn.selected=false;
    _friendrequest_btn.selected=true;
    _bckview3.hidden=YES;
    _bckView1.hidden=NO;
    _bckVIew2.hidden=YES;
        
    }
    
}

-(void)setbackViewColor:(UIColor*)backcolor andotherbackViewColor:(UIColor*)commonbackcolor andViewname1:(UIView*)backview1  andViewname2:(UIView*)backview2 andViewname3:(UIView*)backview3{
    backview1.backgroundColor=[UIColor colorWithRed:116.0/255.0f green:194.0/255.0f  blue:228.0/255.0f  alpha:1.0f];
    backview2.backgroundColor=commonbackcolor;
    backview3.backgroundColor=commonbackcolor;
}

//-(void)mycommunity{
////    PageContentViewController *startingViewController = [self viewControllerAtIndex:1];
////    startingViewController.pageIndex=1;
////    NSArray *viewControllers = @[startingViewController];
////    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
////    [self setbackViewColor:nil andotherbackViewColor:[UIColor clearColor] andViewname1:_bckview3 andViewname2:_bckView1 andViewname3:_bckVIew2];
////    _mycommunity_btn.selected=true;
////    _search_btn.selected=false;
////    _friendrequest_btn.selected=false;
//
//}
//


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
