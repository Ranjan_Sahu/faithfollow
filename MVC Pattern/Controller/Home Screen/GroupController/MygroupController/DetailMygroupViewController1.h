//
//  DetailMygroupViewController1.h
//  wireFrameSplash
//
//  Created by home on 6/23/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLKMenuPopover.h"
@protocol MyFirstControllerDelegate<NSObject>
-(void) FunctionOne: (NSString*) dataOne andArray:(NSMutableArray*)arraycontent;
-(void) btn_pressed: (NSString*) buttonName andtotal_comments:(NSMutableArray*)totalComments  andpost:(NSDictionary*)post  andCellindex:(NSUInteger)cell_index;
-(void)openViewProfile: (NSString*) dataOne andArray:(NSMutableDictionary*)dictcontent;

@end

@interface DetailMygroupViewController1 : UIViewController

@property(weak,nonatomic)IBOutlet UITableView *tableview;
@property(strong,nonatomic)NSString *navigationTitle;
@property (weak, nonatomic) IBOutlet UILabel *addDiscussionLBL;
@property (weak, nonatomic) IBOutlet UIView *backview1;
@property (weak, nonatomic) IBOutlet UILabel *wallLBL;
@property (weak, nonatomic) IBOutlet UIButton *wallbtn;
@property (weak, nonatomic) IBOutlet UIView *backView2;
- (IBAction)addPrayermethod:(id)sender;
- (IBAction)AddDiscussionMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *adddiscussionimage;
@property (weak, nonatomic) IBOutlet UIImageView *prayerImge;
@property(strong,nonatomic)NSString *idUser;
@property (weak, nonatomic) IBOutlet UIView *PrayerContentView;

@property (weak, nonatomic) IBOutlet UIButton *addPrayerBTN;
@property (weak, nonatomic) IBOutlet UIButton *addDiscussionBTN;
@property (weak, nonatomic) IBOutlet UIView *Redview;
@property(nonatomic,strong) NSArray *menuItems;
@property(nonatomic,strong) MLKMenuPopover *menuPopover3;
-(void)handleSingleTapGestureNameComment:(UITapGestureRecognizer *)tapgesture;
@property (nonatomic, weak) id <MyFirstControllerDelegate>delegate1;
@property(strong,nonatomic) UIRefreshControl *refreshControl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *redviewTopConstrainstHeigth;



@end





