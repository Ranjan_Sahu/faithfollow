//
//  DetailMygruopViewController2.m
//  wireFrameSplash
//
//  Created by home on 6/23/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]
#import "DetailMygruopViewController2.h"
#import "DiscusscustomTableViewCell.h"
#import "GroupDiscussionViewController.h"
#import "UIImageView+WebCache.h"
#import "NeosPopup.h"
#import "GroupCommentTableViewCell.h"
#import "DiscussheaderTableViewCell.h"
#import "MWPhotoBrowser.h"
#import "DetailMygroupViewController1.h"
#import "InfluencerProfileController.h"
#import "PraiseTableViewCell.h"

@interface DetailMygruopViewController2 ()<MWPhotoBrowserDelegate,PPLabelDelegate>
{
    NSString *string1,*textstring;
    CGFloat label_personViewHeight;
    CGFloat heightOfLBL,lineoftext;
    NSMutableArray *webgroupArray;
    NSString *content;
    NeosPopup *nepopup;
    UITextView *enterOldPawwordTXT;
    NSString *objectType,*commnetNumber,*dateStr;
    NSMutableArray *addcommentArray;
    NSString *isFavorite,*postId;
    NSString *checkafterServicefavorite,*postType2;
    MWPhotoBrowser *browser;
    NSMutableArray *photos;
    int group2Background;
    NSDataDetector *detector;
    NSArray *matches;
    NSString *imageHeightValueGroup,*imageWidthValueGroup;

}
@end

@implementation DetailMygruopViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableview.sectionHeaderHeight = UITableViewAutomaticDimension;
    self.tableview.estimatedSectionHeaderHeight = 100;
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    self.tableview.tableFooterView = footerView;
   }

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [AppDelegate sharedAppDelegate].groupMorenotnull=NO;
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (getVar.checkDisapper) {
        [[AppDelegate sharedAppDelegate] addsView];
        //getVar.checkDisapper=NO;
    }
    
}

//group2Background
#pragma mark appEnterInForegroundGroup2Method

-(void)appEnterInForegroundGroup2{
    group2Background=group2Background+1;
    if (group2Background%3==0) {
    [[NSUserDefaults standardUserDefaults]setObject:@"Yes" forKey:@"GroupDetail2"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    group2Background=0;
    }

    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]} ];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2,22, 120, 44)];
    label.center = CGPointMake(self.view.frame.size.width/2, 22);
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Avenir-Medium" size:20];
    label.textColor =[UIColor whiteColor];
    label.text=_navigationTitil2;
    self.navigationItem.titleView = label;
    
    UIImage *listImage = [UIImage imageNamed:@"leftArrow"];
    UIButton *listButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [listButton setFrame:CGRectMake(10, 4, 30, 30)];
    [listButton setImage:listImage forState:UIControlStateNormal];
    [listButton  addTarget:self action:@selector(leftButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *jobsButton = [[UIBarButtonItem alloc] initWithCustomView:listButton];
    self.navigationItem.leftBarButtonItem = jobsButton;

    [[AppDelegate sharedAppDelegate] hideaddView];
    self.tableview.showsVerticalScrollIndicator=NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterInForegroundGroup2) name:UIApplicationDidBecomeActiveNotification object:nil];
    addcommentArray=[[NSMutableArray alloc]init];
    postType2=[NSString stringWithFormat:@"%@",[_maindict valueForKey:@"post_type"]];
    isFavorite=[NSString stringWithFormat:@"%@",[_maindict valueForKey:@"is_favourite"]];
    
    if ([_commentnumberCheck isEqualToString:@"discuss"]) {
        commnetNumber=[NSString stringWithFormat:@"%@",_commnetNumbervalue];;
        _commentnumberCheck=@"nodiscuss";
    }else{
        commnetNumber=[NSString stringWithFormat:@"%@",[_maindict valueForKey:@"id"]];
    }
    
    _discissionLBL.text=_contentStr;
    [self commentLismethod];
    
}


#pragma mark LeftButtonPressedMethod

-(void)leftButtonPressed{
    // updated current cell value
//    yesDiscusion
    if ([self.favoritediscussioncheck isEqualToString:@"yesDiscusion"]) {
        self.favoritediscussioncheck=@"";
      [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        if (addcommentArray.count==0) {
          [self.navigationController popViewControllerAnimated:YES];
        }else{
            
            [self.navigationController popViewControllerAnimated:YES];
            NSMutableArray *reversecomment=nil;
        // Memory Leak commented array
//            reversecomment=[[NSMutableArray alloc]init];
            NSString *coutofcomment=[NSString stringWithFormat:@"%ld",[addcommentArray count]];
            reversecomment=[[[addcommentArray reverseObjectEnumerator] allObjects] mutableCopy];
            NSDictionary *dict = @{
                                   @"commenst_Arr" :reversecomment,
                                   @"cellindex" :_cellidGRoup,
                                   @"countofcomments":coutofcomment,
                                   };
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadcellgroup" object:nil userInfo:dict];
            });
        }
    }
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [addcommentArray count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PraiseTableViewCell  *praisecell ;
    
    if (indexPath.row==0) {
        praisecell =(PraiseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PraiseTableViewCell"];
        
        if ([addcommentArray count]>0) {
            praisecell.commentLBL.text=[NSString stringWithFormat:@"%lu %@",(unsigned long)[addcommentArray count],@"Comments"];
        }else{
            praisecell.commentLBL.text=[NSString stringWithFormat:@"%lu %@",(unsigned long)[addcommentArray count],@"Comment"];
        }
        
        if ([postType2 isEqualToString:@"1"]) {
            NSDictionary *totalpraiseDict=[_maindict objectForKey:@"get_total_praised"];
            
            if (![totalpraiseDict isKindOfClass:[NSNull class]]) {
                NSString *countpraise=[NSString stringWithFormat:@"%@",[totalpraiseDict objectForKey:@"count"]];
                if ([countpraise isEqualToString:@"0"]) {
                    praisecell.praiseLBL.text=@"No Praise yet";
                }else{
                    praisecell.praiseLBL.text=[NSString stringWithFormat:@"%@ %@",countpraise,@"praised This"];
                }
            }else{
                praisecell.praiseLBL.text=@"No Praise yet";
            }
        }
        else{
            
            NSDictionary *totalpraiseDict=[_maindict objectForKey:@"get_total_praised"];
            
            if (![totalpraiseDict isKindOfClass:[NSNull class]]) {
                NSString *countpraise=[NSString stringWithFormat:@"%@",[totalpraiseDict objectForKey:@"count"]];
                if ([countpraise isEqualToString:@"0"]) {
                    praisecell.praiseLBL.text=@"No Prayers yet";
                }else{
                    praisecell.praiseLBL.text=[NSString stringWithFormat:@"%@ %@",countpraise,@"Prayed This "];
                }
            }
            else{
                praisecell.praiseLBL.text=@"No Prayers yet";
            }
        }
        
        return praisecell;
    }
    else{
        
        NSMutableDictionary *latestCommentDict=[addcommentArray objectAtIndex:indexPath.row-1];
        NSMutableDictionary *userDetailDict=[latestCommentDict valueForKey:@"get_object_user_details"];
        NSMutableDictionary *userProfileDict=[userDetailDict valueForKey:@"usersprofile"];
        GroupCommentTableViewCell  *Groupcell ;
        
        if (Groupcell == nil) {
            Groupcell =(GroupCommentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"GroupCommentTableViewCell"];
            
            NSString *body=[latestCommentDict valueForKey:@"body"];
            Groupcell.pplableComment.text=body;
            
//            NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:12]};
//            CGRect rect = [body boundingRectWithSize:CGSizeMake(self.view.frame.size.height, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes                                                                                                        context:nil];
//            heightOfLBL=rect.size.height;
//            if (heightOfLBL>21.0) {
//                Groupcell.groupHeight.constant=heightOfLBL+10;
//                Groupcell.pplableComment.text=body;
//            }
//            else{
//                Groupcell.groupHeight.constant=21;
//                Groupcell.pplableComment.text=body;
//            }
            
            matches = [detector matchesInString:Groupcell.pplableComment.text options:0 range:NSMakeRange(0, Groupcell.pplableComment.text.length)];
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            
            [Groupcell.pplableComment addGestureRecognizer:singleTap];
            [self highlightLinksWithIndex:NSNotFound custum:Groupcell.pplableComment];
            
            if ([userDetailDict isKindOfClass:[NSDictionary class]]) {
                NSString *fullName1 = [NSString stringWithFormat: @"%@  %@",[userDetailDict valueForKey:@"first_name"],[userDetailDict valueForKey:@"last_name"]];
                Groupcell.namelabel.text=fullName1;
            }
            NSURL *imageURL1;
            if ([userProfileDict isKindOfClass:[NSDictionary class]]) {
                NSString *imageStr=[userProfileDict objectForKey:@"profile_picture"];
                NSString *imageurl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,imageStr];
                imageURL1 = [NSURL URLWithString:imageurl];
            }
            
            Groupcell.commentImage.tag=indexPath.row;
            Groupcell.namelabel.tag=indexPath.row;
            Groupcell.commentImage.userInteractionEnabled=YES;
            Groupcell.namelabel.userInteractionEnabled=YES;
            UITapGestureRecognizer *commentImage_gesturelike1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup2:)];
            [Groupcell.commentImage addGestureRecognizer:commentImage_gesturelike1];
            UITapGestureRecognizer *commentpersongesturelike1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup2:)];
            [Groupcell.namelabel addGestureRecognizer:commentpersongesturelike1];
            
            NSString *incluencercheck=[NSString stringWithFormat:@"%@",[userDetailDict valueForKey:@"user_type"]];
            if ([incluencercheck isEqualToString:@"2"]) {
                Groupcell.influencerimage.hidden=NO;
                [Groupcell.commentImage   sd_setImageWithURL:imageURL1 placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
            }else{
                Groupcell.influencerimage.hidden=YES;
                [Groupcell.commentImage   sd_setImageWithURL:imageURL1 placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
            }
            Groupcell.timelable.text=[latestCommentDict valueForKey:@"created_at"];
        }
        
        return Groupcell;
        
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    if (indexPath.row==0) {
//        return 38;
//    }
//    else{
//        NSMutableDictionary *latestCommentDict=[addcommentArray objectAtIndex:indexPath.row-1];
//        NSString *body=[latestCommentDict valueForKey:@"body"];
//        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:12]};
//        CGRect rect = [body boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes                                                                                                        context:nil];
//        heightOfLBL=rect.size.height;
//        static GroupCommentTableViewCell *cell = nil;
//        static dispatch_once_t onceToken;
//        dispatch_once(&onceToken, ^{
//            cell = [self.tableview dequeueReusableCellWithIdentifier:@"GroupCommentTableViewCell"];
//        });
//        if (heightOfLBL>21.0) {
//            CGFloat actualhight=heightOfLBL-21.0;
//            [cell setNeedsUpdateConstraints];
//            [cell updateConstraintsIfNeeded];
//            [cell layoutIfNeeded];
//            return 95+actualhight;
//        }
//        else{
//            [cell setNeedsUpdateConstraints];
//            [cell updateConstraintsIfNeeded];
//            [cell layoutIfNeeded];
//            return 85;
//        }
//    }
//
//}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSMutableArray *attachArray=[_maindict valueForKey:@"get_attachments"];
    isFavorite=[NSString stringWithFormat:@"%@",[_maindict valueForKey:@"is_favourite"]];
    
    if ([attachArray count]>0) {
        NSDictionary *attachDict=[attachArray objectAtIndex:0];
        DiscusscustomTableViewCell  *cell1 ;
        if (cell1 == nil) {
            cell1 =(DiscusscustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DiscussCell"];
            cell1.attachementImage.hidden=YES;
            NSURL *imageURL = [NSURL URLWithString:_imageURl];
            NSString *incluencerimagechewck;
            NSDictionary *getsubjectDict=[_maindict valueForKey:@"get_subject_user_details"];
            NSString *formattedDate=[NSString stringWithFormat:@"%@",[_maindict objectForKey:@"formatted_date"]];
            cell1.timeLBL.text=formattedDate;
            
            if ([getsubjectDict isKindOfClass:[NSDictionary class]]) {
                
                NSString *subjectname =[NSString stringWithFormat: @"%@ %@",getsubjectDict[@"first_name"], getsubjectDict[@"last_name"]];
                
                //                NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Aileron-SemiBold" size:14]};
                //                CGRect rect1 = [subjectname boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes                                                                                                        context:nil];
                //                cell1.nameLBLHeight.constant=40;
                //                if (rect1.size.height<20) {
                //                    cell1.nameLBLHeight.constant=40;
                //                }
                //                else{
                //
                //                }
                cell1.nameLBL.text=subjectname;
                incluencerimagechewck=[NSString stringWithFormat:@"%@",[getsubjectDict valueForKey:@"user_type"]];
                
                if ([incluencerimagechewck isEqualToString:@"2"]) {
                    cell1.influencerimage.hidden=NO;
                    [cell1.imageview   sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
                }else{
                    cell1.influencerimage.hidden=YES;
                    [cell1.imageview   sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
                }
            }
            
            NSString *imageStr=[attachDict objectForKey:@"file"];
            NSString *imageurl=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,imageStr];
            NSURL *imageURL1 = [NSURL URLWithString:imageurl];
            
            // dynamic image
            CGFloat widthView1,widthView;
            [cell1.attachementImage sd_setImageWithURL:imageURL1 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL, NSString *imageTag) {
                NSString *key = [[SDWebImageManager sharedManager] cacheKeyForURL:imageURL1];
                UIImage *lastPreviousCachedImage1 = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:key];
                if (image != nil) {
                    if(lastPreviousCachedImage1!=nil && [AppDelegate sharedAppDelegate].groupMorenotnull==NO)
                    {
                        [AppDelegate sharedAppDelegate].groupMorenotnull=YES;
                        if (attachDict!= nil && [attachDict count]>0) {
                            //                      NSString *imgAbsoluteUrl= imageURL.absoluteString;
                            imageHeightValueGroup=[NSString stringWithFormat:@"%f",image.size.height];
                            imageWidthValueGroup=[NSString stringWithFormat:@"%f",image.size.width];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.tableview reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:NO];
                                //                                [self.tableview reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
                            });
                        }
                        else{
                            //    skiped image already added.
                            
                        }
                    }
                }
                
            }];
            
            NSString *key = [[SDWebImageManager sharedManager] cacheKeyForURL:imageURL1];
            UIImage *lastPreviousCachedImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:key];
            if(lastPreviousCachedImage && ![attachDict isKindOfClass:[NSNull class]]){
                cell1.attachementImage.hidden=NO;
                widthView1 = [self calculateImageHeight:self.tableview.frame.size.width imageHeight:lastPreviousCachedImage.size.height imageWidth:lastPreviousCachedImage.size.width];
            }
            else{
                cell1.attachementImage.hidden=NO;
                widthView=[Constant1 checkimagehieght];
                widthView1=285;
            }
            
//            // end dynamic image code
//            NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Aileron-Regular" size:16]};
//            CGRect rect = [_contentStr boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes                                                                                                        context:nil];
//            heightOfLBL=rect.size.height;
//            if (heightOfLBL>71) {
//                cell1.text2heightheader.constant=heightOfLBL;
//                // cell1.textDiscussLBL.text=_contentStr;
//                cell1.labelPP.text=_contentStr;
//
//            }
//            else{
//                cell1.text2heightheader.constant=40;
//                //                cell1.textDiscussLBL.text=_contentStr;
//                cell1.labelPP.text=_contentStr;
//            }
            
            // pplabel implementation
            cell1.labelPP.delegate=self;
            NSError *error = nil;
            detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
            matches = [detector matchesInString:cell1.labelPP.text options:0 range:NSMakeRange(0, cell1.labelPP.text.length)];
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            [cell1.labelPP addGestureRecognizer:singleTap];
            [self highlightLinksWithIndex:NSNotFound custum:cell1.labelPP];
//            cell1.labelPP.font=[UIFont  fontWithName:@"Aileron-Regular" size:14.0];
            //            cell1.textDiscussLBL.backgroundColor=[UIColor clearColor];
            //            cell1.labelPP.textColor=UIColorFromRGB(0x626262);
            
            cell1.btn_moreImages.tag=section;
            [cell1.btn_moreImages addTarget:self action:@selector(btn_moreImages:) forControlEvents:UIControlEventTouchUpInside];
            cell1.attachementImage.tag=section;
            cell1.attachementImage.userInteractionEnabled=YES;
            
            UITapGestureRecognizer *imagegallery_open = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moreImages:)];
            [cell1.attachementImage addGestureRecognizer:imagegallery_open];
            
            cell1.view_btnmoareimage.layer.borderWidth=0.6f;
            cell1.view_btnmoareimage.layer.cornerRadius= cell1.view_btnmoareimage.frame.size.width/2;
            cell1.view_btnmoareimage.layer.borderColor=[UIColor blackColor].CGColor;
            
            if ([attachArray count]==1) {
                cell1.view_btnmoareimage.hidden=YES;
            }else{
                NSString *countofimage=[NSString stringWithFormat:@"+%lu",[attachArray count]-1];
                [cell1.btn_moreImages setTitle:countofimage forState:UIControlStateNormal];
                cell1.view_btnmoareimage.hidden=NO;
            }
            
            if ([postType2 isEqualToString:@"1"]) {
                if ([isFavorite isEqualToString:@"0"]) {
                    UIImage *btnImage = [UIImage imageNamed:@"Unselectheart"];
                    [cell1.heartBTN setImage:btnImage forState:UIControlStateNormal];
                    [cell1.heartBTN addTarget:self action:@selector(heartmethodAdd:) forControlEvents:UIControlEventTouchUpInside];
                }else{
                    UIImage *btnImage = [UIImage imageNamed:@"heart_25"];
                    [cell1.heartBTN setImage:btnImage forState:UIControlStateNormal];
                    [cell1.heartBTN addTarget:self action:@selector(heartmethodremove:) forControlEvents:UIControlEventTouchUpInside];
                }
                
            }
            return cell1;
        }
        
    }
    else{
        DiscussheaderTableViewCell  *cell2 ;
        if (cell2 == nil) {
            cell2 =(DiscussheaderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Discusscustom"];
            [cell2 layoutIfNeeded];
            NSURL *imageURL = [NSURL URLWithString:_imageURl];
            NSDictionary *getsubjectDict=[_maindict valueForKey:@"get_subject_user_details"];
            NSString *incluencerimagechewck;
            NSString *formattedDate=[NSString stringWithFormat:@"%@",[_maindict objectForKey:@"formatted_date"]];
            cell2.timeLBL.text=formattedDate;
            
            if ([getsubjectDict isKindOfClass:[NSDictionary class]]) {
                incluencerimagechewck=[NSString stringWithFormat:@"%@",[getsubjectDict valueForKey:@"user_type"]];
                NSString *subjectname = [NSString stringWithFormat: @"%@ %@",getsubjectDict[@"first_name"], getsubjectDict[@"last_name"]];
                cell2.nameLBL.text=subjectname;
                
                if ([incluencerimagechewck isEqualToString:@"2"]) {
                    cell2.influencerimage.hidden=NO;
                    [cell2.headerImageView   sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
                }else{
                    cell2.influencerimage.hidden=YES;
                    [cell2.headerImageView   sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
                }
            }
            
            cell2.labelPP.text=_contentStr;
//            NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Aileron-Regular" size:17]};
//            CGRect rect = [_contentStr boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-70, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes                                                                                                        context:nil];
//            heightOfLBL=rect.size.height;
//            if (heightOfLBL>21) {
//                if ([postType2 isEqualToString:@"1"])
//                {
//                    cell2.disheaderheight.constant=heightOfLBL+21;
//                }
//                else
//                {
//                    cell2.disheaderheight.constant=heightOfLBL+30;
//                    cell2.heartimageWidthConstant.constant=0;
//                    cell2.lblTextconstant.constant=rect.size.width+70;
//                }
//                //                cell2.textDiscussLBL.text=_contentStr;
//                cell2.labelPP.text=_contentStr;
//
//            }
//            else{
//                cell2.disheaderheight.constant=31;
//                //                cell2.textDiscussLBL.text=_contentStr;
//                cell2.labelPP.text=_contentStr;
//            }
            
            cell2.labelPP.delegate=self;
            NSError *error = nil;
            detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
            matches = [detector matchesInString:cell2.labelPP.text options:0 range:NSMakeRange(0, cell2.labelPP.text.length)];
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            [cell2.labelPP addGestureRecognizer:singleTap];
            [self highlightLinksWithIndex:NSNotFound custum:cell2.labelPP];
            
//            cell2.labelPP.font=[UIFont  fontWithName:@"Aileron-Regular" size:14.0];
            //            cell2.textDiscussLBL.backgroundColor=[UIColor clearColor];
            //            cell2.labelPP.textColor=UIColorFromRGB(0x626262);
            
            if ([postType2 isEqualToString:@"1"]) {
                if ([isFavorite isEqualToString:@"0"]) {
                    UIImage *btnImage = [UIImage imageNamed:@"Unselectheart"];
                    [cell2.heartBTN setImage:btnImage forState:UIControlStateNormal];
                    [cell2.heartBTN addTarget:self action:@selector(heartmethodAdd:) forControlEvents:UIControlEventTouchUpInside];
                }else{
                    UIImage *btnImage = [UIImage imageNamed:@"heart_25"];
                    [cell2.heartBTN setImage:btnImage forState:UIControlStateNormal];
                    [cell2.heartBTN addTarget:self action:@selector(heartmethodremove:) forControlEvents:UIControlEventTouchUpInside];
                }
            }
            return cell2;
        }
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return UITableViewAutomaticDimension;
    
    NSMutableArray *attachArray=[_maindict valueForKey:@"get_attachments"];
    if ([attachArray count]>0) {
        
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Avenir-Roman" size:16]};
        CGRect rect = [_contentStr boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes                                                                                                        context:nil];
        static DiscusscustomTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell = [self.tableview dequeueReusableCellWithIdentifier:@"DiscussCell"];
        });
        // dynamic imahe height calculating
        float imgHeight=0;
        //        CGFloat widthView1=0;
        if ([imageHeightValueGroup floatValue] >0 && [imageWidthValueGroup floatValue] >0) {
            imgHeight=[self calculateImageHeight:tableView.frame.size.width imageHeight:[imageHeightValueGroup floatValue] imageWidth:[imageWidthValueGroup floatValue]]+10;
        }
        else{
            if (IS_IPHONE5) {
                imgHeight=150;
            }
            else if (IS_IPHONE6){
                imgHeight=200;
                
            }else if (IS_IPHONE6PLUS){
                imgHeight=200;
 
            }
            else{
                imgHeight=150;
  
            }
            
        }
        //end dynamic image
        heightOfLBL=rect.size.height;
               if (heightOfLBL>21.0) {
            CGFloat actualHieght=heightOfLBL;
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            [cell layoutIfNeeded];
            return imgHeight+60+actualHieght;
        }
        else{
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            [cell layoutIfNeeded];
            return 160+imgHeight;
        }
    }
    else{
        static DiscussheaderTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell = [self.tableview dequeueReusableCellWithIdentifier:@"Discusscustom"];
        });
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Avenir-Roman" size:15]};
        CGRect rect = [_contentStr boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes                                                                                                        context:nil];
        heightOfLBL=rect.size.height;
        if (heightOfLBL>21.0) {
            CGFloat actualHieght=heightOfLBL;
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            [cell layoutIfNeeded];
            return 110+actualHieght;
        }
        else{
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            [cell layoutIfNeeded];
            return 110;
        }
    }
}


-(float)calculateImageHeight:(float)boxWidth imageHeight:(float)imageHeight1 imageWidth:(float)imagewidth1{
    CGFloat screenHeight=[UIScreen mainScreen].bounds.size.height;
    CGFloat preferImageHeight=screenHeight-300;
    //    CGFloat actualImageHeight=imageHeight1;
    CGFloat widthView1,widthView;
    if (imagewidth1>imageHeight1) {
        widthView1=[Constant1 checkimagehieght];
        widthView=widthView1+30;
    }
    else{
        widthView=preferImageHeight;
        
    }
    return widthView;
}


#pragma mark subject_touch_commentGroup2Method

- (void)subject_touch_commentGroup2:(UITapGestureRecognizer*)sender {
    NSDictionary *dict=[addcommentArray objectAtIndex:sender.view.tag];
    if ([dict isKindOfClass:[NSDictionary class]]) {
        NSDictionary *getobjectDict=[dict valueForKey:@"get_object_user_details"];
        if ([getobjectDict isKindOfClass:[NSDictionary class]]) {
            NSString *influencercheck=[NSString stringWithFormat:@"%@",[getobjectDict valueForKey:@"user_type"]];
            NSString *userid=[NSString stringWithFormat:@"%@",[getobjectDict valueForKey:@"id"]];
            [self profileView:friendprofile anfriend_ID:userid influencerData:influencercheck];
        }
        
    }
    
}


#pragma mark ProfileViewMethod

-(void)profileView :(NSString*)methodName anfriend_ID:(NSString*)friendID influencerData:(NSString*)influencerData{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : friendID,
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                NSMutableArray *messageArray;
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if ([influencerData isEqualToString:@"2"]) {
                            NSMutableDictionary *seemoreDict=[response valueForKey:@"data"];
                            InfluencerProfileController *frndSeemore=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                            frndSeemore.influencerDict=seemoreDict;
                            [self.navigationController pushViewController:frndSeemore animated:YES];
                        }
                        else{
                            NSMutableDictionary *seemoreDict=[response valueForKey:@"data"];
                            FriendsProfileViewController *frndSeemore=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                            frndSeemore.friendProfileDict=seemoreDict;
                            frndSeemore.postFriendProfileDict=seemoreDict;
                            //                        [self presentViewController:frndSeemore animated:YES completion:nil];
                            [self.navigationController pushViewController:frndSeemore animated:YES];
                        }
                        
                    });
                    
                }else{
                    messageArray=[response valueForKey:@"message"];
                    if ([Constant1 validateArray:messageArray]) {
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    }
                    
                    
                }
                
            }
        }
        
    } errorBlock:^(NSError *error)
     {
//         NSLog(@"error");
         
     }];
}


- (void)moreImages:(UITapGestureRecognizer*)sender {
//    UIView *view = sender.view;
//    NSLog(@"%ld", (long)view.tag);//By tag, you can find out where you had tapped.
    //NSDictionary *dict=[_maindict valueForKey:@"get_attachments"][[view tag]];
    NSMutableArray *get_attachments=_maindict[@"get_attachments"];
    NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
//    NSLog(@"get_attachments is%@",get_attachments);
    for (NSDictionary* image_dict in get_attachments){
        [post_imageArray addObject:image_dict[@"file"]];
    }
    [self openphoto:post_imageArray];
}

#pragma mark Btn_moreImagesMethod

-(void)btn_moreImages:(UIButton*)sender{
    
    NSMutableArray *get_attachments=_maindict[@"get_attachments"];
    NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
//    NSLog(@"get_attachments is%@",get_attachments);
    for (NSDictionary* image_dict in get_attachments){
        [post_imageArray addObject:image_dict[@"file"]];
    }
    [self openphoto:post_imageArray];
}
-(void)openphoto:(NSMutableArray*)imagearra{
    NSMutableArray *imagearr=[[NSMutableArray alloc]init];
    
    for (int i=0; i<imagearra.count; i++) {
        NSString *str=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,[imagearra objectAtIndex:i]];
        MWPhoto *photo=[MWPhoto photoWithURL:[NSURL URLWithString:str]];
//        photo.caption = @"Biblefaithfollow.SocialApp";
        [imagearr addObject:photo];
    }
    photos=[[[imagearr reverseObjectEnumerator] allObjects] mutableCopy];
    browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    BOOL displayActionButton = YES;
    BOOL displaySelectionButtons = NO;
    BOOL enableGrid = YES;
    BOOL startOnGrid = NO;
    browser.displayActionButton = displayActionButton;
    //  browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = NO;
    browser.zoomPhotosToFill = YES;
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = NO;
    //browser.autoPlayOnAppear = autoPlayOnAppear;
    [browser setCurrentPhotoIndex:0];
    
//    enableGrid = NO;
    
    [self.navigationController pushViewController:browser animated:YES];
}
#pragma mark MWPhotoBrowser delegate

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
    
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
//    NSLog(@"Did finish modal presentation");
    // [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark ScrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight;
    NSMutableArray *attachArray=[_maindict valueForKey:@"get_attachments"];
    if ([attachArray count]>0) {
        sectionHeaderHeight = 700;
    }
    else{
        sectionHeaderHeight=150;
    }
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}



#pragma Addcommentmethod

-(IBAction)addcommentMethod:(id)sender{
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        CGFloat resetViewWidth = 300;
        CGFloat resetViewHeight = 340;
        CGFloat closeBtnHeight = 30;
        CGFloat margin = 10;
        CGFloat txtViewHeight = 170;
        CGFloat sendBtnHeight = 50;
        
        // create popup view
        UIView  *reset_view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, resetViewWidth, resetViewHeight)];
        reset_view.backgroundColor=[UIColor whiteColor];
        reset_view.layer.cornerRadius = 8;
        reset_view.layer.masksToBounds = YES;
        reset_view.layer.shadowRadius = 10;
        reset_view.layer.borderColor= [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0].CGColor;
        
        // create close btn
        UIImageView *cancel_pop=[[UIImageView alloc]initWithFrame:CGRectMake(reset_view.frame.size.width-(closeBtnHeight + (2*margin)), 20, closeBtnHeight,closeBtnHeight)];
        cancel_pop.image=[UIImage imageNamed:@"ic_cancel"];
        cancel_pop.contentMode = UIViewContentModeCenter;
        [reset_view addSubview:cancel_pop];
        cancel_pop.userInteractionEnabled=YES;
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancel_popup)];
        [cancel_pop addGestureRecognizer:tapGestureRecognizer ];
        
        // create text entry UI
        enterOldPawwordTXT=[[UITextView alloc]init];
        [enterOldPawwordTXT becomeFirstResponder];
        enterOldPawwordTXT.frame = CGRectMake((2*margin), ((2*margin)+closeBtnHeight+(2*margin)), (resetViewWidth-(4*margin)), txtViewHeight);
        enterOldPawwordTXT.textColor=[UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0];
        enterOldPawwordTXT.font = semibold;
        [enterOldPawwordTXT.layer setBorderColor:[[UIColor colorWithRed:238.0/255.0 green:242.0/255.0 blue:243.0/255.0 alpha:1.0] CGColor]];
        [enterOldPawwordTXT.layer setBorderWidth:1.0];
        enterOldPawwordTXT.layer.cornerRadius = 8;
        enterOldPawwordTXT.clipsToBounds = YES;
        enterOldPawwordTXT.backgroundColor=[UIColor whiteColor];
        enterOldPawwordTXT.textAlignment=NSTextAlignmentLeft;
        [reset_view addSubview:enterOldPawwordTXT];
        
        // create send btn
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(submitCommentMethod)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Send" forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"Avenir-Roman" size:18]];
        button.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0];
        button.layer.cornerRadius=10.0f;
        button.frame = CGRectMake((2*margin), ((2*margin)+closeBtnHeight+(2*margin)+txtViewHeight+(3*margin)), (resetViewWidth-(4*margin)), sendBtnHeight);
        [reset_view addSubview:button];
        
        nepopup=[[NeosPopup  alloc]initWithFrame:CGRectZero];
        nepopup.taponContentView=YES;
        nepopup.backcontentView=reset_view;
        [nepopup show];
    }
    
}

#pragma mark SubmitCommentMethod

-(void)submitCommentMethod{
    NSInteger lengthOfString = [[enterOldPawwordTXT.text stringByReplacingOccurrencesOfString:@" " withString:@""] length];
        if ([enterOldPawwordTXT.text isEqualToString:@""]) {
        UIAlertView *at=[[UIAlertView alloc]initWithTitle:appTitle message:@"Enter Your Comment" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [at show];
        return;
    }
    else if (!lengthOfString){
        UIAlertView *at=[[UIAlertView alloc]initWithTitle:appTitle message:@"Enter Your Comment" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [at show];
        return;
    }
    else
    {
    [nepopup dismiss];
    [enterOldPawwordTXT resignFirstResponder];
    NSDictionary *dict = @{
                           @"ref_type":postGroupType,
                           @"ref_id":commnetNumber,
                           @"body" :enterOldPawwordTXT.text,
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           };
        
//        NSLog(@"comment parameter %@",dict);
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else
    {
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:addcomments withParameters:dict withHud:YES success:^(id response){
            NSMutableArray *messageArray;
//            NSLog(@"response is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        // Comment
                        
                         [[NSNotificationCenter defaultCenter] postNotificationName: @"reloadtable" object:nil userInfo:nil];
                        
                        NSString *commentCount;
                        NSString *currentDate=[Constant1 createTimeStamp];
                        NSString *emailID;
                        
                        NSDictionary *subjectNameDict=[_maindict objectForKey:@"get_subject_user_details"];
                        if ([subjectNameDict isKindOfClass:[NSDictionary class]]) {
                            emailID=[NSString stringWithFormat:@"%@ %@",[subjectNameDict objectForKey:@"first_name"],[subjectNameDict objectForKey:@"last_name"]];
                        }
                        else{
                           emailID=@"";
                        }
                        
                        NSMutableArray *countArray=[response objectForKey:@"count"];
                        if ([countArray isKindOfClass:[NSArray class]]) {
                            NSDictionary *countDict=[countArray objectAtIndex:0];
                            commentCount=[NSString stringWithFormat:@"%@",[countDict objectForKey:@"count"]];
                        }
                        else{
                            commentCount=@"";
                        }

                        NSString *contentType;
                        if ([postType2 isEqualToString:@"1"]|| [postType2 isEqualToString:@"3"]) {
                            contentType=@"Update";
                            
                        }
                        else{
                            contentType=@"Prayer Request";
                        }
                        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last comment":currentDate}];
                        
                        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Date of last comment":currentDate,@"Total times commented":commentCount}];
                        
                        [[AppDelegate sharedAppDelegate].mxPanel track:@"Comment"
                            properties:@{@"Content ID":commnetNumber,@"Posted by":emailID,@"Content type":contentType,@"Date of last like":currentDate}
                         ];
                        
                    [AppDelegate sharedAppDelegate].isCommentSelected = YES;
                        [self commentLismethod];
                        
                    }
                    else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        });
                    }
                }
            }
        }errorBlock:^(id error)
         {
//             NSLog(@"error is %@",error);
         }];
    }
    }
}

#pragma mark CommentLismethod

-(void)commentLismethod{
    [nepopup dismiss];
    AppDelegate *appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    
    else{
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"ref_type":postGroupType,
                           @"ref_id":commnetNumber,
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           };
//    NSLog(@"favorite comment%@",dict);
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:commentLists withParameters:dict withHud:YES success:^(id response){
            NSMutableArray *messageArray;
//            NSLog(@"response is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSArray* reversedArray=[response valueForKey:@"data"];
                            addcommentArray = [[[reversedArray reverseObjectEnumerator] allObjects] mutableCopy];
                            [_tableview reloadData];
                        });
                        
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        });
                    }
                }
            }
        }errorBlock:^(id error)
         {
//             NSLog(@"error is %@",error);
         }];
    }
    }
}

-(void)cancel_popup{
    [nepopup dismiss];
}


-(void)praisedBtnmethod{
    
}

#pragma mark HeartmethodAdd

-(void)heartmethodAdd:(UIButton*)sender{
    
    UIView *parentCell = sender.superview.superview.superview;
    if ([parentCell isKindOfClass:[DiscusscustomTableViewCell class]]) {
//        DiscusscustomTableViewCell *cell1 = (DiscusscustomTableViewCell*)parentCell;
        AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
        if (!appDelegate.isReachable) {
            [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
            return;
        }
        else{
            NSDictionary *dict = @{
                                   @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                                   @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                                   @"post_id" :_postIdStr,
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:favourite withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]])  {
                    if (![response isKindOfClass:[NSNull class]]) {
                        if([response[@"status"]intValue ] ==1){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if ([isFavorite isEqualToString:@"0"]) {                                                                      [_maindict  setObject:@"1" forKey:@"is_favourite"];
                                    [self.tableview reloadData];
                                    
                                }
                                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            });
                            
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            });
                            
                        }
                    }
                }
            } errorBlock:^(id error)
             {
//                 NSLog(@"error");
                 
             }];
            
        }
        
    }
    // another celll
    if ([parentCell isKindOfClass:[DiscussheaderTableViewCell class]]) {
//        DiscussheaderTableViewCell *cell2 = (DiscussheaderTableViewCell*)parentCell;
        AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
        if (!appDelegate.isReachable) {
            [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
            return;
        }
        else{
            NSDictionary *dict = @{
                                   @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                                   @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                                   @"post_id" :_postIdStr,
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:favourite withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]])  {
                    if (![response isKindOfClass:[NSNull class]]) {
                        if([response[@"status"]intValue ] ==1){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                isFavorite=@"0";
                                if ([isFavorite isEqualToString:@"0"]) {                                                                      [_maindict  setObject:@"1" forKey:@"is_favourite"];
                                    [self.tableview reloadData];
                                    
                                }
                                
                                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            });
                            
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                                
                            });
                        }
                    }
                }
            } errorBlock:^(id error)
             {
//                 NSLog(@"error");
                 
             }];
            
        }
        
    }
    
}

#pragma mark Heartmethodremove

-(void)heartmethodremove:(UIButton*)sender{
    UIView *parentCell = sender.superview.superview.superview;
    if ([parentCell isKindOfClass:[DiscusscustomTableViewCell class]]) {
//        DiscusscustomTableViewCell *cell = (DiscusscustomTableViewCell *)parentCell;
        AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (!appDelegate.isReachable) {
            [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
            return;
        }
        else{
            NSDictionary *dict = @{
                                   @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                                   @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                                   @"post_id" :_postIdStr,
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:remove_favourite withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]])  {
                    if (![response isKindOfClass:[NSNull class]]) {
                        if([response[@"status"]intValue ] ==1){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                isFavorite=@"1";
                                if ([isFavorite isEqualToString:@"1"]) {                                                                      [_maindict  setObject:@"0" forKey:@"is_favourite"];
                                    [self.tableview reloadData];
                                }
                                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            });
                            
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            });
                        }
                    }
                }
            } errorBlock:^(id error)
             {
//                 NSLog(@"error");
                 
             }];
            
        }
        
    }
    // another celll
    if ([parentCell isKindOfClass:[DiscussheaderTableViewCell class]]) {
//        DiscussheaderTableViewCell *cell2 = (DiscussheaderTableViewCell *)parentCell;
        AppDelegate *appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
        if (!appDelegate.isReachable) {
            [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
            return;
        }
        else{
            NSDictionary *dict = @{
                                   @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                                   @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                                   @"post_id" :_postIdStr,
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:remove_favourite withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]])  {
                    if (![response isKindOfClass:[NSNull class]]) {
                        if([response[@"status"]intValue ] ==1){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                isFavorite=@"1";
                                if ([isFavorite isEqualToString:@"1"]) {                                                                      [_maindict  setObject:@"0" forKey:@"is_favourite"];
                                    [self.tableview reloadData];
                                }
                                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            });
                            
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                                
                            });
                        }
                    }
                }
            } errorBlock:^(id error)
             {
//                 NSLog(@"error");
                 
             }];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

#pragma pplabel delegate
#pragma mark -

- (void)label:(PPLabel *)label didBeginTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:charIndex custum:label];
 
}

- (void)label:(PPLabel *)label didMoveTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:charIndex custum:label];
}

- (void)label:(PPLabel *)label didEndTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:NSNotFound custum:label];
    //
    //    for (NSTextCheckingResult *match in self.matches) {
    //
    //        if ([match resultType] == NSTextCheckingTypeLink) {
    //
    //            NSRange matchRange = [match range];
    //
    //            if ([self isIndex:charIndex inRange:matchRange]) {
    //
    //                [[UIApplication sharedApplication] openURL:match.URL];
    //                break;
    //            }
    //        }
    //    }
    
}

- (void)label:(PPLabel *)label didCancelTouch:(UITouch *)touch {
    
    //    [self highlightLinksWithIndex:NSNotFound custum:label];
}

#pragma mark -

- (BOOL)isIndex:(CFIndex)index inRange:(NSRange)range {
    return index > range.location && index < range.location+range.length;
}

- (void)highlightLinksWithIndex:(CFIndex)index custum:(PPLabel*)label {
    
    NSMutableAttributedString* attributedString = [label.attributedText mutableCopy];
    
    for (NSTextCheckingResult *match in matches) {
        
        if ([match resultType] == NSTextCheckingTypeLink) {
            
            NSRange matchRange = [match range];
            
            if ([self isIndex:index inRange:matchRange]) {
                [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:matchRange];
                
            }
            else {
                [attributedString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x00ADEB) range:matchRange];
            }
            
            [attributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:matchRange];
        }
    }
    
    label.attributedText = attributedString;
}

- (void)handleTap:(UITapGestureRecognizer *)tapRecognizer
{
    PPLabel *textLabel = (PPLabel *)tapRecognizer.view;
    CGPoint tapLocation = [tapRecognizer locationInView:textLabel];
    NSString *testURL=[NSString stringWithFormat:@"%@",textLabel.text];
    
    // init text storage
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:textLabel.attributedText];
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    [textStorage addLayoutManager:layoutManager];
    
    // init text container
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(textLabel.frame.size.width, textLabel.frame.size.height+100) ];
    textContainer.lineFragmentPadding  = 0;
    textContainer.maximumNumberOfLines = textLabel.numberOfLines;
    textContainer.lineBreakMode        = textLabel.lineBreakMode;
    
    [layoutManager addTextContainer:textContainer];
    
    NSUInteger characterIndex = [layoutManager characterIndexForPoint:tapLocation
                                                      inTextContainer:textContainer
                             fractionOfDistanceBetweenInsertionPoints:NULL];
    
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:testURL options:0 range:NSMakeRange(0, [testURL length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSRange matchRange = [match range];
            if ([self isIndex:characterIndex inRange:matchRange]) {
                [[UIApplication sharedApplication] openURL:match.URL];
                break;
            }
        }
    }
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
