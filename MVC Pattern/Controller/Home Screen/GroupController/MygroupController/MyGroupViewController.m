//
//  MyGroupViewController.m
//  wireFrameSplash
//
//  Created by home on 6/22/16.
//  Copyright © 2016 home. All rights reserved.
//


#import "MyGroupViewController.h"
#import "CustomTableViewCell.h"
#import "DetailMygroupViewController1.h"
#import "MNMBottomPullToRefreshManager.h"
#import "UIImageView+WebCache.h"
#import "dummyTableViewCell.h"

@interface MyGroupViewController ()<MNMBottomPullToRefreshManagerClient>
{
    NSMutableArray *searcharray,*request_dataArray,*webSearchArray,*simplearray;
    NSString *searchString;
    NSMutableDictionary *dictwebSearchArray;
    int current_page;
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
    int pagenumber,total_pagenumber;
    NSInteger countadd;
    
}
@end

@implementation MyGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:0.0f tableView:_tableView withClient:self];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self mygroupListMethod];
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = footerView;
}

#pragma mark MygroupListMethod

-(void)mygroupListMethod{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:communitylist withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        webSearchArray=[response valueForKey:@"data"];
                        NSInteger countcheck;
                         countcheck =[webSearchArray count];
                        countadd=countcheck+1;
                        [self.tableView reloadData];
                    });
                }
            }
        }
        else{
            NSMutableArray  *messageArray=[response valueForKey:@"message"];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([messageArray count]>0) {
                   [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter]; 
                }
            });
        }
        
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
     }];
    
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (countadd>0) {
//        return countadd;
//    } else{
//        return 0;
//    }
    return webSearchArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *cell;
    
    NSDictionary *dict=[webSearchArray objectAtIndex:indexPath.row];
    NSDictionary *getcommuntiyDict=[dict valueForKey:@"get_community"];
    if (cell==nil) {
        cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    }
    cell.namelabel.text=[getcommuntiyDict valueForKey:@"name"];
    NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[getcommuntiyDict valueForKey:@"image_file"]];
    if ([[getcommuntiyDict valueForKey:@"image_file"] isEqualToString:@""]) {
        [cell.Mygroup_Groupimage sd_setImageWithURL:nil placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
    }else{
        [cell.Mygroup_Groupimage sd_setImageWithURL:[NSURL URLWithString:str]  placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
    }
    NSInteger index=[webSearchArray indexOfObject:[webSearchArray lastObject]];
    return cell;
    
}

- (UIImage *)imageFromColor:(UIColor *)color {
    
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dict=[webSearchArray objectAtIndex:indexPath.row];
    
    if (![dict isKindOfClass:[NSNull class]]) {
        NSDictionary *getcommuntiyDict=[dict valueForKey:@"get_community"];
        DetailMygroupViewController1 *detailView=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
        detailView.navigationTitle=[getcommuntiyDict valueForKey:@"name"];
        detailView.idUser=[dict valueForKey:@"community_id"];
        NSString *commid=[NSString stringWithFormat:@"%@",[dict valueForKey:@"community_id"]];
        [[NSUserDefaults standardUserDefaults] setObject:commid forKey:@"comid"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController pushViewController:detailView animated:YES];
    }
    
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

@end
