//
//  DetailMygroupViewController1.m
//  wireFrameSplash
//
//  Created by home on 6/23/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#define  kCellName1 @"FeedwithImagePostTableViewCell"
//See_CommentsTableViewCell //FeedwithImagePostTableViewCell
#import "DetailMygroupViewController1.h"
#import "DiscusscustomTableViewCell.h"
#import "PrayerCustomTableViewCell.h"
#import "DetailMygruopViewController2.h"
#import "StatusViewController.h"
#import "UIImageView+WebCache.h"
#import "MNMBottomPullToRefreshManager.h"
#import "SearchCommunityProfileView.h"
#import "WallView.h"
#import "FeedwithImagePostTableViewCell.h"
#import "Constant1.h"
#import "UIImageView+WebCache.h"
#import "MWPhotoBrowser.h"
#import "FriendsProfileViewController.h"
#import "SuggestedGroupTableViewCell.h"
#import "Profile_editViewController.h"
#import "InfluencerProfileController.h"
#import "Mixpanel.h"
#import "AppDelegate.h"


@interface DetailMygroupViewController1 ()<MNMBottomPullToRefreshManagerClient,MWPhotoBrowserDelegate,MLKMenuPopoverDelegate>
{
    NSMutableArray *photos;
    MWPhotoBrowser *browser;
    NSMutableArray *webgroupArray;
    UIFont *postfont;
    NSString *string1,*textstring,*postIdStrPraised,*islike;
    CGFloat label_personViewHeight;
    CGFloat heightOfLBL,lineoftext;
    int current_page;
    NSString *userIDGroup;
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
    int pagenumber,total_pagenumber;
    int countLine;
    NSString *post_type,*strresponse;
    NSMutableDictionary *dataResponse,*searchDict;
    NSMutableArray *getaatachArray,*postimageArray;
    CGFloat imageHeight;
    CGFloat imageWidth;
    CGFloat heightofView;
    CGFloat heightofcell;
    NSDataDetector *detector;
    NSArray *matches;
}
@end

@implementation DetailMygroupViewController1

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppDelegate sharedAppDelegate].groupimageArray=[[NSMutableArray alloc]init];
    [AppDelegate sharedAppDelegate].postimagecheckNotnull=NO;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]} ];
    self.navigationController.navigationBar.translucent = NO;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,20, 100, 44)];
    label.font = [UIFont fontWithName:@"Avenir-Medium" size:20];
    label.textColor =[UIColor whiteColor];
    label.textAlignment=NSTextAlignmentCenter;
    label.text=_navigationTitle;
    self.navigationItem.titleView = label;
    
    UIImage *listImage = [UIImage imageNamed:@"leftArrow"];
    UIButton *listButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [listButton setFrame:CGRectMake(0, 0, 30, 30)];
    [listButton setImage:listImage forState:UIControlStateNormal];
    listButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [listButton  addTarget:self action:@selector(leftButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *infoButtonImage = [[UIBarButtonItem alloc]initWithCustomView:listButton ];
    self.navigationItem.leftBarButtonItem =infoButtonImage;
    
    UIImage *listImage1 = [UIImage imageNamed:@"info"];
    UIButton *listButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [listButton1 setFrame:CGRectMake(0, 0, 30, 30)];
    [listButton1 setImage:listImage1 forState:UIControlStateNormal];
    listButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [listButton1  addTarget:self action:@selector(ProfileGroupmember) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *infoButton = [[UIBarButtonItem alloc]initWithCustomView:listButton1 ];
    self.navigationItem.rightBarButtonItem =infoButton;
    
    self.navigationItem.hidesBackButton = YES;
    
    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:0.0f tableView:_tableview withClient:self];
    [self.tableview   registerNib:[UINib nibWithNibName:kCellName1 bundle:nil] forCellReuseIdentifier:kCellName1];
    [self getDetailCommunity];
    [self profileGroupwervices];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getGroupData)
                                                 name:@"reloadtablegroup"
                                               object:nil];
    self.tableview.dataSource = nil;
    self.tableview.delegate = nil;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"backcheck" forKey:@"backcheckYes"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]} ];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    current_page=1;
    
    if ([AppDelegate sharedAppDelegate].isCommentSelected==YES) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reloadcellogcommetMethod:)
                                                     name:@"reloadcellgroup"
                                                   object:nil];
    }
}



-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    if ([AppDelegate sharedAppDelegate].isCommentSelected==YES) {
        [AppDelegate sharedAppDelegate].isCommentSelected = NO;
        [[NSNotificationCenter defaultCenter]removeObserver:self];
    }
    //    else{
    //    [[NSNotificationCenter defaultCenter]removeObserver:self];
    //
    //    }
    
}

#pragma reload Comment method

- (void) reloadcellogcommetMethod:(NSNotification *) notification{
    
    NSDictionary   *comment_dict = notification.userInfo;
    NSMutableArray*comment_arr=comment_dict[@"commenst_Arr"];
    NSString* cellnumber=comment_dict[@"cellindex"];
    [webgroupArray objectAtIndex:[cellnumber integerValue] ][@"latest_comments"]=comment_arr;
    
    if (  [[webgroupArray objectAtIndex:[cellnumber integerValue]] [@"get_total_commnets"] isKindOfClass:[NSNull class]]) {
        NSDictionary *dict=@{@"count":[NSString stringWithFormat:@"%@",comment_dict[@"countofcomments"]]
                             };
        [[webgroupArray objectAtIndex:[cellnumber integerValue]] setObject:dict forKey:@"get_total_commnets"];
    }else{
        NSDictionary *dict=@{@"count":[NSString stringWithFormat:@"%@",comment_dict[@"countofcomments"]]
                             };
        [[webgroupArray objectAtIndex:[cellnumber integerValue]] setObject:dict forKey:@"get_total_commnets"];
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[cellnumber integerValue] inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableview reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
}


-(void)refreshViewGroup:(UIRefreshControl *)refresh {
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (!appDelegate.isReachable) {
        [self.refreshControl endRefreshing];
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
    }else{
        pagenumber=1;
        
        [self  pullUptorefreshGroup];
    }
}
-(void)getGroupData
{
    pagenumber=1;
    [self  pullUptorefreshGroup];
}


#pragma pullUpFreshMethod

-(void)pullUptorefreshGroup{
    
    
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        
        
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"comm_id":_idUser,
                               @"page" :@"1",
                               };
        //    NSLog(@"disct is%@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:walldetails withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (webgroupArray) {
                    [webgroupArray removeAllObjects];
                }
                 if ([AppDelegate sharedAppDelegate].groupimageArray) {
                    [[AppDelegate sharedAppDelegate].groupimageArray removeAllObjects];
                }

                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            dataResponse=[response objectForKey:@"data"];
                            NSString *current_page1=[dataResponse valueForKey:@"current_page"];
                            current_page=[current_page1 intValue];
                            NSString *totalPage=[NSString stringWithFormat:@"%@",[dataResponse valueForKey:@"last_page"]];
                            total_pagenumber=[totalPage intValue];
                            webgroupArray=[dataResponse objectForKey:@"data"];
                            
                            for (int i=0; i<[webgroupArray count]; i++) {
                                [[AppDelegate sharedAppDelegate].groupimageArray addObject:[NSNull null]];
                                    }
                            
                            [self.tableview reloadData];
                            [self.tableview layoutIfNeeded];
                            [_refreshControl endRefreshing];
                            [pullToRefreshManager_ tableViewReloadFinished];
                            
                        });
                    }
                }
            }
            else{
                
                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                });
            }
        } errorBlock:^(id error)
         {
             [self.view makeToast:error  duration:1.0 position:CSToastPositionCenter];
             //         NSLog(@"error");
             
         }];
    }
}




-(void)ProfileGroupmember{
    SearchCommunityProfileView *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchCommunityProfileView"];
    allprofileView.friendProfileDict=searchDict;
    [self.navigationController pushViewController:allprofileView animated:YES];
}

-(void)profileGroupwervices{
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict = @{
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"id":_idUser
                               };
        //    NSLog(@"disct is%@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:communityDisplay withParameters:dict withHud:YES success:^(id response){
            //        NSLog(@"resposnse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            searchDict=[response objectForKey:@"data"];
                        });
                    }
                }
            }
            else{
                
                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                });
            }
            
        } errorBlock:^(id error)
         {
             [self.view makeToast:error  duration:1.0 position:CSToastPositionCenter];
             //         NSLog(@"error");
             
         }];
    }
}


-(void)getDetailCommunity{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if([AppDelegate sharedAppDelegate].groupimageArray)
    {
        [[AppDelegate sharedAppDelegate].groupimageArray removeAllObjects];
    }
    if (webgroupArray) {
        [webgroupArray removeAllObjects];
    }
    
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"comm_id":_idUser,
                               @"page" :[NSString stringWithFormat:@"%d",current_page],
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:walldetails withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            dataResponse=[response objectForKey:@"data"];
                            NSString *current_page1=[dataResponse valueForKey:@"current_page"];
                            current_page=[current_page1 intValue];
                            NSString *totalPage=[NSString stringWithFormat:@"%@",[dataResponse valueForKey:@"last_page"]];
                            total_pagenumber=[totalPage intValue];
                            webgroupArray=[dataResponse objectForKey:@"data"];
                            for (int i=0; i<[webgroupArray count]; i++) {
                            [[AppDelegate sharedAppDelegate].groupimageArray addObject:[NSNull null]];
                                    
                            }
                            self.tableview.dataSource = (id)self;
                            self.tableview.delegate = (id)self;
                            [self.tableview reloadData];
                        });
                    }
                }
            }
            else{
                
                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                });
            }
        } errorBlock:^(id error)
         {
             
             [self.view makeToast:error  duration:1.0 position:CSToastPositionCenter];
             //         NSLog(@"error");
             
         }];
    }
}

-(void)leftButtonPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(!webgroupArray)
        return 0;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [webgroupArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeedwithImagePostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName1 forIndexPath:indexPath];
    NSLog(@"indepath %ld",indexPath.row);
//    cell.backgroundColor=[UIColor whiteColor];
//    cell.backgroundColor=[UIColor colorWithRed:239/255.0f green:239/255.0f blue:239/255.0f alpha:1.0];
    cell.lbl_postStatus.hidden=YES;
    cell.lbl_date.hidden=YES;
    cell.labelPP.delegate=(id)self;
    cell.pplableComment1.delegate=(id)self;
    cell.pplableComment2.delegate=(id)self;
    cell.pplableComment3.delegate=(id)self;
    [self commonsetUpCell:cell atIndexPath:indexPath];
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailMygruopViewController2 *detailViewGroup=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygruopViewController2"];
    NSDictionary *dict=[webgroupArray objectAtIndex:indexPath.row];
    
    if (![dict isKindOfClass:[NSNull class]]) {
        
        detailViewGroup.postIdStr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
        detailViewGroup.navigationTitil2=_navigationTitle;
        detailViewGroup.detail2Dict=[dict valueForKey:@"get_attachments"];
        NSString *contentType=[dict valueForKey:@"content"];
        NSMutableDictionary *communityDict=[dict valueForKey:@"get_subject_user_details"];
        NSMutableDictionary *userDict=[communityDict valueForKey:@"usersprofile"];
        NSString *imageStr=[userDict objectForKey:@"profile_picture"];
        NSString *imageurl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,imageStr];
        detailViewGroup.imageURl=imageurl;
        detailViewGroup.contentStr=contentType;
        NSString *fullName1 = [NSString stringWithFormat: @"%@  %@",[communityDict valueForKey:@"first_name"],[communityDict valueForKey:@"last_name"]];
        detailViewGroup.nameLabel=fullName1;
        detailViewGroup.maindict=[webgroupArray objectAtIndex:indexPath.row];
        NSMutableArray *fullArray=[[NSMutableArray alloc]init];
        [fullArray addObject:[webgroupArray objectAtIndex:indexPath.row]];
        detailViewGroup.Fullmaindict=fullArray;
        [self.navigationController pushViewController:detailViewGroup animated:YES];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[webgroupArray objectAtIndex:indexPath.row];
    float contentTextHeight=0;
    CGFloat see_morecomment=0;
    CGFloat heightofpostimage=0;
    
    NSString *content_string=dict[@"content"];
    UIFont *font = semibold;
    CGRect rect1 = [content_string boundingRectWithSize:CGSizeMake(self.view.frame.size.width, MAXFLOAT)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName : font}
                                        context:nil];
    
    heightOfLBL=rect1.size.height;
    
    if (rect1.size.height<120) {
        contentTextHeight=rect1.size.height;
    }else{
        contentTextHeight=120;
    }
    
    if ([dict[@"get_total_commnets"] isKindOfClass:[NSNull class]]){
        heightofView=0;
    }else{
        // get_total_commnet=dict[@"get_total_commnets"];
        int totalcount=[dict[@"get_total_commnets"][@"count"] intValue];
        if(totalcount==1)  {
            heightofView=61;
        }else if(totalcount==2)  {
            heightofView=61*2;
        }else if(totalcount==3)  {
            heightofView=61*3;
        }else if(totalcount>3)  {
            heightofView=61*3;
            see_morecomment=32;
        }
    }

    CGFloat  widthView1;
    postimageArray=[[NSMutableArray alloc]init];
    postimageArray=dict[@"get_attachments"];
    if (postimageArray.count==0){
        heightofpostimage=0;
    }
    else{
        // change for image height
        if ([[AppDelegate sharedAppDelegate].groupimageArray count]>0) {
            NSDictionary *dict=[[AppDelegate sharedAppDelegate].groupimageArray objectAtIndex:indexPath.row];
            if (![dict isKindOfClass:[NSNull class]])
            {
                NSString *imageHeight1=[NSString stringWithFormat:@"%@",[dict objectForKey:@"imgHeight"]];
                NSString *imagewidth1=[NSString stringWithFormat:@"%@",[dict objectForKey:@"imgWidth"]];
                heightofpostimage=[self calculateImageHeight:tableView.frame.size.width imageHeight:[imageHeight1 floatValue] imageWidth:[imagewidth1 floatValue]];
            }
            else{
                widthView1=[Constant1 checkimagehieght];
                heightofpostimage=178;
            }
        }
        else{
            widthView1=[Constant1 checkimagehieght];
            heightofpostimage=widthView1+30;
        }
    }

    heightofcell = 4+74+heightofpostimage+10+contentTextHeight+30+44+44+heightofView+see_morecomment+10;
//      heightofcell=80+contentTextHeight+70+heightofView+see_morecomment+heightofpostimage+15;

    return  heightofcell;
}


#pragma mark Clicked on Image Post method
- (void)handleSingleTapGestureimageposted:(UITapGestureRecognizer *)tapgesture {
    NSMutableDictionary *imageDict=[webgroupArray objectAtIndex:tapgesture.view.tag];
    NSMutableArray *get_attachments=imageDict[@"get_attachments"];
    NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
    //    NSLog(@"get_attachments is%@",get_attachments);
    for (NSDictionary* imageDict in get_attachments){
        [post_imageArray addObject:imageDict[@"file"]];
    }
    [self openphoto:post_imageArray];
    
}

#pragma mark moreButtonImageCount method
-(void)btnMoreImages:(UIButton*)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableview];
    NSIndexPath *indexPath = [self.tableview indexPathForRowAtPoint:buttonPosition];
    NSMutableDictionary *imageDict=[webgroupArray objectAtIndex:indexPath.row];
    NSMutableArray *get_attachments=imageDict[@"get_attachments"];
    NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
    //    NSLog(@"get_attachments is%@",get_attachments);
    for (NSDictionary* imageDict in get_attachments){
        [post_imageArray addObject:imageDict[@"file"]];
    }
    [self openphoto:post_imageArray];
}
-(void)openphoto:(NSMutableArray*)imagearra{
    
    NSMutableArray *imagearr=[[NSMutableArray alloc]init];
    for (int i=0; i<imagearra.count; i++) {
        NSString *str=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,[imagearra objectAtIndex:i]];
        MWPhoto *photo=[MWPhoto photoWithURL:[NSURL URLWithString:str]];
        //        photo.caption = @"Biblefaithfollow.SocialApp";
        [imagearr addObject:photo];
    }
    photos=[[[imagearr reverseObjectEnumerator] allObjects] mutableCopy];
    browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    BOOL displayActionButton = YES;
    BOOL displaySelectionButtons = NO;
    BOOL enableGrid = YES;
    BOOL startOnGrid = NO;
    browser.displayActionButton = displayActionButton;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = NO;
    browser.zoomPhotosToFill = YES;
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = NO;
    [browser setCurrentPhotoIndex:0];
    enableGrid = NO;
    (void)enableGrid;
    [self.navigationController pushViewController:browser animated:YES];
}



-(void)btncommentsmore_act_group:(UIButton*)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableview];
    NSIndexPath *indexPath = [self.tableview indexPathForRowAtPoint:buttonPosition];
    DetailMygruopViewController2 *detailViewGroup=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygruopViewController2"];
    // stay where u click any comment
    detailViewGroup.cellidGRoup=[NSString stringWithFormat:@"%ld",indexPath.row];
    
    NSDictionary *dict=[webgroupArray objectAtIndex:indexPath.row];
    detailViewGroup.postIdStr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
    detailViewGroup.navigationTitil2=_navigationTitle;
    detailViewGroup.detail2Dict=[dict valueForKey:@"get_attachments"];
    NSString *contentType=[dict valueForKey:@"content"];
    NSMutableDictionary *communityDict=[dict valueForKey:@"get_subject_user_details"];
    
    //chnages communtiy dict
    
    if ([communityDict isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *userDict=[communityDict valueForKey:@"usersprofile"];
        NSString *imageStr=[userDict objectForKey:@"profile_picture"];
        NSString *imageurl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,imageStr];
        detailViewGroup.imageURl=imageurl;
        detailViewGroup.contentStr=contentType;
        NSString *fullName1 = [NSString stringWithFormat: @"%@  %@",[communityDict valueForKey:@"first_name"],[communityDict valueForKey:@"last_name"]];
        detailViewGroup.nameLabel=fullName1;
    }
    
    
    detailViewGroup.maindict=[webgroupArray objectAtIndex:indexPath.row];
    NSMutableArray *fullArray=[[NSMutableArray alloc]init];
    [fullArray addObject:[webgroupArray objectAtIndex:indexPath.row]];
    detailViewGroup.Fullmaindict=fullArray;
    [self.navigationController pushViewController:detailViewGroup animated:YES];
    
}

#pragma mark Clicked on comment to open next Controller

- (void)handleSingleTapGestureNameComment:(UITapGestureRecognizer *)tapgesture {
    
    DetailMygruopViewController2 *detailViewGroup=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygruopViewController2"];
    NSDictionary *dict=[webgroupArray objectAtIndex:tapgesture.view.tag];
    //    cell index pass to next controller
    detailViewGroup.cellidGRoup=[NSString stringWithFormat:@"%ld",tapgesture.view.tag];
    //
    detailViewGroup.postIdStr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
    detailViewGroup.navigationTitil2=_navigationTitle;
    detailViewGroup.detail2Dict=[dict valueForKey:@"get_attachments"];
    NSString *contentType=[dict valueForKey:@"content"];
    NSMutableDictionary *communityDict=[dict valueForKey:@"get_subject_user_details"];
    
    // changes community
    
    if ([communityDict isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *userDict=[communityDict valueForKey:@"usersprofile"];
        NSString *imageStr=[userDict objectForKey:@"profile_picture"];
        NSString *imageurl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,imageStr];
        detailViewGroup.imageURl=imageurl;
        detailViewGroup.contentStr=contentType;
        NSString *fullName1 = [NSString stringWithFormat: @"%@  %@",[communityDict valueForKey:@"first_name"],[communityDict valueForKey:@"last_name"]];
        detailViewGroup.nameLabel=fullName1;
        
    }
    detailViewGroup.maindict=[webgroupArray objectAtIndex:tapgesture.view.tag];
    NSMutableArray *fullArray=[[NSMutableArray alloc]init];
    [fullArray addObject:[webgroupArray objectAtIndex:tapgesture.view.tag]];
    detailViewGroup.Fullmaindict=fullArray;
    [self.navigationController pushViewController:detailViewGroup animated:YES];
    
}



#pragma mark Subject Profile method and Subject Name

- (void)handleSingleTapGestureNameProfile:(UITapGestureRecognizer *)tapgesture {
    
    NSDictionary *dict=[webgroupArray objectAtIndex:tapgesture.view.tag];
    NSMutableDictionary *subject_dict=[dict valueForKey:@"get_subject_user_details"];
    // changes
    
    if ([subject_dict isKindOfClass:[NSDictionary class]]) {
        NSDictionary *userprofileDict=[subject_dict valueForKey:@"usersprofile"];
        NSString *idcheck=[NSString stringWithFormat:@"%@",userprofileDict[@"user_id"]];
        NSString *loginId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
        NSString *checkInfluencerProfile=[NSString stringWithFormat:@"%@",subject_dict[@"user_type"]];
        
        
        if ([[dict valueForKey:@"object_type"]isEqualToString:@"community"]) {
            
            if ([checkInfluencerProfile isEqualToString:@"2"] ) {
                InfluencerProfileController *influcerview=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                influcerview.influencerDict=subject_dict;
                //            [self presentViewController:influcerview animated:YES completion:nil];
                [self.navigationController pushViewController:influcerview animated:YES];
                
            }
            else{
                if ([idcheck isEqualToString:loginId]){
                    Profile_editViewController *profileView=[self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
                    //            CGRect    screenRect = [[UIScreen mainScreen] bounds];
                    //            UIView *viewProfile=[[UIView alloc]initWithFrame:CGRectMake(0, 0,screenRect.size.width,60)];
                    //            viewProfile.backgroundColor=UIColorFromRGB(0x0088CF);
                    //            [[profileView view] addSubview:viewProfile];
                    //            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    //            [button addTarget:self
                    //                       action:@selector(backgroup)
                    //             forControlEvents:UIControlEventTouchUpInside];
                    //            [button setImage:[UIImage imageNamed:@"leftArrow"] forState:UIControlStateNormal];
                    //            button.frame = CGRectMake(5, 25,30, 30);
                    //            [viewProfile addSubview:button];
                    //
                    //            UILabel *lbl1 = [[UILabel alloc] init];
                    //            [lbl1 setFrame:CGRectMake(screenRect.size.width/2-20,15,100,50)];
                    //            lbl1.font = [UIFont fontWithName:@"Avenir-Medium" size:16];
                    //
                    //            lbl1.backgroundColor=[UIColor clearColor];
                    //            lbl1.textColor=[UIColor whiteColor];
                    //            lbl1.text= @"Profile";
                    //            [viewProfile addSubview:lbl1];
                    //
                    //            [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"checkprofile"];
                    //            [[NSUserDefaults standardUserDefaults] synchronize];
                    ////            [self presentViewController:profileView animated:YES completion:nil];
                    [self.navigationController pushViewController:profileView animated:YES];
                    
                }
                else{
                    [self viewFriendProfile:idcheck];
                }
                
                
            }
            
        }
        
    }
    
}

-(void)backgroup{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma viewFriendProfile

-(void)viewFriendProfile :(NSString*)userID {
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict = @{
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"friend_id":userID
                               };
        
//        NSLog(@"disct is%@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
            //        NSLog(@"resposnse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        NSMutableDictionary *FriendDict=response[@"data"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // commented for memory leak
                            //                      NSString *friebdStatus=[FriendDict valueForKey:@"friend_status"];
                            
                            NSDictionary *profileviewDict=[FriendDict valueForKey:@"usersprofile"];
                            NSString *profile_picture=[profileviewDict valueForKey:@"profile_picture"];
                            NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",FriendDict[@"first_name"],FriendDict[@"last_name"]]]];
                            [[NSUserDefaults standardUserDefaults] setObject:nameTxt forKey:@"name"];
                            [[NSUserDefaults standardUserDefaults] setObject:profile_picture forKey:@"profile_picture12"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            FriendsProfileViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                            allprofileView.friendProfileDict=FriendDict;
                            allprofileView.postFriendProfileDict=FriendDict;
                            //                        [self presentViewController:allprofileView animated:YES completion:nil];
                            [self.navigationController pushViewController:allprofileView animated:YES];
                            
                        });
                    }
                }
            }
            else{
                
                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                });
            }
            
        } errorBlock:^(id error)
         {
             //         NSLog(@"error");
             
         }];
    }
    
}


#pragma  Mark CommonSetUp Method

-(void)commonsetUpCell:(FeedwithImagePostTableViewCell *)postimagecell  atIndexPath:(NSIndexPath *)indexPath {
    [self.menuPopover3 dismissMenuPopover];
    NSLog(@"index path is %ld",(long)indexPath.row);
    // commented below live
    postimagecell.widthofnamelbl.constant=200;
    if ([[webgroupArray objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
        //comment view
        //Dropdown
        postimagecell.dropdownbutton.tag=indexPath.row;
        [postimagecell.dropdownbutton addTarget:self action:@selector(dropdownbtnact:) forControlEvents:UIControlEventTouchUpInside];
        postimagecell.btn_seemorecomments.hidden=YES;
        UITapGestureRecognizer   *singleTapGestureRecognizerName3 =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureNameComment:)];
        [postimagecell.btn_Comments  addGestureRecognizer:singleTapGestureRecognizerName3];
        postimagecell.btn_Comments.tag=indexPath.row;
        postimagecell.btn_Comments.userInteractionEnabled = YES;
        //postimagecell.backgroundColor = [self colorFromHexString:@"#d8d6d6"];
        
        postimagecell.btn_readmorepost .tag=indexPath.row;
        [postimagecell.btn_readmorepost addTarget:self action:@selector(btncommentsmore_act_group:) forControlEvents:UIControlEventTouchUpInside];
        
        // hide top post title
        
        postimagecell.toppostImagelblHeight.constant=0;
        postimagecell.seeMoreGroupRequestbtn.hidden=YES;
        postimagecell.toppostbetviewinImage.constant=0;
        postimagecell.commentViewSeemoretopHeight.constant=-20;
        postimagecell.topgroupHeight.constant=-1.5;
        
        //arvind
        postimagecell.heightofBottomButtonView.constant=0.0;
        
        NSDictionary *get_object_user_details;
        NSString *objectName=nil;
        NSDictionary *dict=[webgroupArray objectAtIndex:indexPath.row];
        if ( [dict[@"get_total_commnets"] isKindOfClass:[NSNull class]]) {
            [postimagecell.btn_commentsCount setTitle:@"0 Comment" forState:UIControlStateNormal];
        }
        else{
            NSMutableDictionary *countDict=[dict valueForKey:@"get_total_commnets"];
            NSString *commnetcount=[NSString stringWithFormat:@"%@ %@",[countDict valueForKey:@"count"],@"Comment"];
            [postimagecell.btn_commentsCount setTitle:commnetcount forState:UIControlStateNormal];
        }
        
        islike=[NSString stringWithFormat:@"%@",[dict objectForKey:@"is_like"]];
        NSString *dateFormated=[dict valueForKey:@"formatted_date"];
        if ([dateFormated isKindOfClass:[NSNull class]]) {
            postimagecell.lbl_objectname.text=@"-";
        }
        else{
            postimagecell.lbl_objectname.text=dateFormated;
        }
        NSString *contentStr=[dict valueForKey:@"content"];
        if ([contentStr isKindOfClass:[NSNull class]]) {
            postimagecell.labelPP.text=@"-";
        }
        else{
            postimagecell.labelPP.text=contentStr;
            UIFont *font = semibold;
            CGRect rect1 = [contentStr boundingRectWithSize:CGSizeMake(self.view.frame.size.width, MAXFLOAT)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:@{NSFontAttributeName : font}
                                                        context:nil];
            
           countLine=[self lineCountForText:contentStr];
            if (countLine>=6) {
                postimagecell.btn_readmorepost.hidden=NO;
                label_personViewHeight=120;
            }
            else{
                postimagecell.btn_readmorepost.hidden=YES;
                label_personViewHeight=rect1.size.height;
            }

            NSError *error = nil;
            detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
            matches = [detector matchesInString:postimagecell.labelPP.text options:0 range:NSMakeRange(0, postimagecell.labelPP.text.length)];
            
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            [postimagecell.labelPP addGestureRecognizer:singleTap];
            [self highlightLinksWithIndex:NSNotFound custum:postimagecell.labelPP];
            
        }
        
        if ( [dict[@"get_object_community_details"] isKindOfClass:[NSNull class]]) {
            objectName=@"";
        }else{
            get_object_user_details=dict[@"get_subject_user_details"];
            //                objectName=[NSString stringWithFormat:@"%@",get_object_user_details[@"name"]];
        }
        NSString *subjectName;
        if ( [dict[@"get_subject_user_details"] isKindOfClass:[NSNull class]]) {
            subjectName=@"";
        }
        else{
            NSDictionary *get_subject_user_details=dict[@"get_subject_user_details"];
            NSDictionary *subject_user_details=get_subject_user_details[@"usersprofile"];
            subjectName=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
            
            NSString *myid=[NSString stringWithFormat:@"%@",get_subject_user_details[@"id"]];
            NSString *loginIdcheck=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
            NSString *influencerCheckimage=[NSString stringWithFormat:@"%@",get_subject_user_details[@"user_type"]];
            if ([myid isEqualToString:loginIdcheck]) {
                postimagecell.lbl_personName.text=@"You";
            }
            else{
                postimagecell.lbl_personName.text=subjectName;
            }
            
            UITapGestureRecognizer   *singleTapGestureRecognizerName3 =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureNameProfile:)];
            [postimagecell.lbl_personName  addGestureRecognizer:singleTapGestureRecognizerName3];
            postimagecell.lbl_personName.tag=indexPath.row;
            postimagecell.lbl_personName.userInteractionEnabled = YES;
            
            UITapGestureRecognizer  *singleTapGestureRecognizerNameimagepost =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureimageposted:)];
            [postimagecell.imageview_post  addGestureRecognizer:singleTapGestureRecognizerNameimagepost];
            postimagecell.imageview_post.tag=indexPath.row;
            postimagecell.imageview_post.userInteractionEnabled = YES;
            
            UITapGestureRecognizer  *singleTapGestureRecognizerNameimagepostimage =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureNameProfile:)];
            [postimagecell.imageview_person  addGestureRecognizer:singleTapGestureRecognizerNameimagepostimage];
            postimagecell.imageview_person.tag=indexPath.row;
            postimagecell.imageview_person.userInteractionEnabled = YES;
            
            
            NSString *profile_picture=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:subject_user_details[@"profile_picture"]]];
            NSString *umageUrl;
            umageUrl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,profile_picture];
            NSURL *imageURL = [NSURL URLWithString:umageUrl];
            
            if ([influencerCheckimage isEqualToString:@"2"]) {
                postimagecell.main_profile_image.hidden=NO;
                [postimagecell.imageview_person   sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
            }
            else{
                postimagecell.main_profile_image.hidden=YES;
                [postimagecell.imageview_person   sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
            }
            
        }
        
        [postimagecell.btn_moreImages addTarget:self action:@selector(btnMoreImages:) forControlEvents:UIControlEventTouchUpInside];
        postimageArray=[[NSMutableArray alloc]init];
        
        postimageArray=dict[@"get_attachments"];
        CGFloat widthView,widthView1;
//        postimageArray=dict[@"get_attachments"];
        
        if (postimageArray.count==0){
            postimagecell.heightofimageview.constant=0;
        }
        else{
            if ([postimageArray count]>=2) {
                NSString *countimage=[NSString stringWithFormat:@"+%lu",postimageArray.count-1];
                [postimagecell.btn_moreImages setTitle:countimage  forState:UIControlStateNormal];
                postimagecell.view_btnmoareimage.hidden=NO;
            }
            else{
                postimagecell.view_btnmoareimage.hidden=YES;
            }
            NSMutableDictionary *DictAttachimage=[postimageArray objectAtIndex:0];
            NSString *imageStr=[DictAttachimage objectForKey:@"file"];
            NSString *imageurl=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,imageStr];
            [postimagecell.imageview_post sd_setImageWithURL:[NSURL URLWithString:imageurl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL, NSString *imageTag) {
                NSString *key = [[SDWebImageManager sharedManager] cacheKeyForURL:imageURL];
                UIImage *lastPreviousCachedImage1 = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:key];
                    if (image != nil) {
                    id arrObj=[[AppDelegate sharedAppDelegate].groupimageArray objectAtIndex:[imageTag intValue]];
                    
                    if(![arrObj isKindOfClass:[NSNull class]] && lastPreviousCachedImage1!=nil)
                    {
                        NSDictionary *dataDict;
                        if ([AppDelegate sharedAppDelegate].postimagecheckNotnull==YES && [imageTag intValue]==0) {
                            dataDict=[[AppDelegate sharedAppDelegate].groupimageArray objectAtIndex:0];
                            if (dataDict!= nil && [dataDict count]>0) {
                                NSDictionary *dictimage;
                                NSString *imgAbsoluteUrl= imageURL.absoluteString;
                                NSString *imgHeight=[NSString stringWithFormat:@"%f",image.size.height];
                                NSString *imgWidth=[NSString stringWithFormat:@"%f",image.size.width];
                                NSString *tag=[NSString stringWithFormat:@"%d",[imageTag intValue]];
                                dictimage= @{
                                             @"imgAbsoluteUrl" :imgAbsoluteUrl ,
                                             @"imgHeight" :imgHeight,
                                             @"imgWidth":imgWidth,
                                             @"imageTag":tag,
                                             };
                                [[AppDelegate sharedAppDelegate].groupimageArray replaceObjectAtIndex:0 withObject:dictimage];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [AppDelegate sharedAppDelegate].postimagecheckNotnull=NO;
                                    [self.tableview reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:NO];
                                });
                                
                            }
                            
                        }
                        else{
                            //         skiped image already added.
//                            NSLog(@"skip in group");
                        }
                    }
                    else
                    {
                        NSDictionary *dictimage;
                        NSString *imgAbsoluteUrl= imageURL.absoluteString;
                        NSString *imgHeight=[NSString stringWithFormat:@"%f",image.size.height];
                        NSString *imgWidth=[NSString stringWithFormat:@"%f",image.size.width];
                        NSString *tag=[NSString stringWithFormat:@"%d",[imageTag intValue]];
                        dictimage= @{
                                     @"imgAbsoluteUrl" :imgAbsoluteUrl ,
                                     @"imgHeight" :imgHeight,
                                     @"imgWidth":imgWidth,
                                     @"imageTag":tag,
                                     };
                         [[AppDelegate sharedAppDelegate].groupimageArray replaceObjectAtIndex:[imageTag intValue] withObject:dictimage];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableview reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:[imageTag intValue] inSection:0], nil] withRowAnimation:NO];
                            
                        });
                    }
                }
                
            }];
            
            NSString *key = [[SDWebImageManager sharedManager] cacheKeyForURL:[NSURL URLWithString:imageurl]];
            UIImage *lastPreviousCachedImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:key];
            id imageDict=[[AppDelegate sharedAppDelegate].groupimageArray objectAtIndex:indexPath.row];
            if(lastPreviousCachedImage && ![imageDict isKindOfClass:[NSNull class]]){
                widthView1 = [self calculateImageHeight:self.tableview.frame.size.width imageHeight:lastPreviousCachedImage.size.height imageWidth:lastPreviousCachedImage.size.width];
            }
            else{
                widthView=[Constant1 checkimagehieght];
                widthView1=164;
            }
            postimagecell.heightofimageview.constant=widthView1;
        }
        if ([dict[@"get_total_commnets"] isKindOfClass:[NSNull class]]){
            postimagecell.mainview_height.constant=0;
        }
        postimagecell.imageview_post.hidden=NO;
        if (postimageArray.count==0){
            postimagecell.imageview_post.hidden=YES;
            postimagecell.view_btnmoareimage.hidden=YES;
        }
        
        
        postimagecell.widthofimageview.constant=[UIScreen mainScreen].bounds.size.width-16;
        postimagecell.heightofLabelconstraint.constant=label_personViewHeight;
        float width=[UIScreen mainScreen].bounds.size.width-(20);
        postimagecell.lbl_post.preferredMaxLayoutWidth = width; // This is necessary to allow the label
        postimagecell.bottomofcommentView.constant=16;
        
        //changes
        [postimagecell layoutIfNeeded];
        NSString *postType1=dict[@"post_type"];
        int post_val=[postType1 intValue];
        NSDictionary* get_total_praised=dict[@"get_total_praised"];
        postimagecell.btn_praised.tag=indexPath.row;
        
        if ( [dict[@"get_total_praised"] isKindOfClass:[NSNull class]]) {
            [postimagecell.btn_praisedCount setTitle:@"No Praises yet" forState:UIControlStateNormal];
            [postimagecell.btn_praised addTarget:self action:@selector(btnpraised_actgroup:) forControlEvents:UIControlEventTouchUpInside];
            if (post_val==1) {
                postimagecell.imageview_praised.text=@"Praise";
                [postimagecell.btn_praised setTitle:@"Praise" forState:UIControlStateNormal];
                [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                [postimagecell.btn_praisedCount setTitle:@"No Praises yet" forState:UIControlStateNormal];
//                [postimagecell.btn_praised setTitle:@"Praise" forState:UIControlStateNormal];
//                [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            }else{
                postimagecell.imageview_praised.text=@"Pray For This";
                [postimagecell.btn_praised setTitle:@"Pray For This" forState:UIControlStateNormal];
                [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                [postimagecell.btn_praisedCount setTitle:@"No Prayers yet" forState:UIControlStateNormal];
//                [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            }
            postimagecell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_unselected"];
            postimagecell.imageview_praised.textColor=[UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1.0];
            
        }else{
            get_total_praised=dict[@"get_total_praised"];
            NSString *like=[NSString stringWithFormat:@"%@",dict[@"is_like"]];
            int like_count = [like intValue];
            if (like_count==1) {
                NSString *like1=[NSString stringWithFormat:@"%@",get_total_praised[@"count"]];
                int main_like= [like1 intValue];
                NSString *total_count;
                if (post_val==1) {
                    if (main_like>=3) {
                        total_count=[NSString stringWithFormat:@"You and %d others Praised This",main_like-1];
                    }
                    else{
                        total_count=[NSString stringWithFormat:@"You and %d other Praised This",main_like-1];
                        
                    }
                    
                    if (main_like==1) {
                        total_count=[NSString stringWithFormat:@"You Praised This"];
                        
                    }
                    postimagecell.imageview_praised.text=@"You Praised This";
                    
                    [postimagecell.btn_praised setTitle:@"You Praised This" forState:UIControlStateNormal];
                    postimagecell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_selected"];
                    postimagecell.imageview_praised.textColor=[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
                    [postimagecell.btn_praised addTarget:self action:@selector(btnpraised_actgroup:) forControlEvents:UIControlEventTouchUpInside];
                    [postimagecell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                    [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                    
                }else{
                    
                    if (main_like>=3) {
                        total_count=[NSString stringWithFormat:@"You and %d others Prayed for This",main_like-1];
                    }
                    else{
                        total_count=[NSString stringWithFormat:@"You and %d other Prayed for This",main_like-1];
                        
                    }
                    if (main_like==1) {
                        total_count=[NSString stringWithFormat:@"You Prayed for This"];
                    }
                    postimagecell.imageview_praised.text=@"You Prayed";
                    
                    [postimagecell.btn_praised setTitle:@"You Prayed" forState:UIControlStateNormal];
                    postimagecell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_selected"];
                    postimagecell.imageview_praised.textColor=[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
                    [postimagecell.btn_praised addTarget:self action:@selector(btnpraised_actgroup:) forControlEvents:UIControlEventTouchUpInside];
                    [postimagecell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                    [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                }
            }else{
                if (post_val==1) {
                    postimagecell.imageview_praised.text=@"Praise";
                    [postimagecell.btn_praised setTitle:@"Praise" forState:UIControlStateNormal];
                    [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                    NSString *total_count=[NSString stringWithFormat:@"%@ Praised This",[Constant1 getStringObject:get_total_praised[@"count"]]];
                    [postimagecell.btn_praised addTarget:self action:@selector(btnpraised_actgroup:) forControlEvents:UIControlEventTouchUpInside];
                    [postimagecell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                }else{
                    postimagecell.imageview_praised.text=@"Pray For This";
                    [postimagecell.btn_praised setTitle:@"Pray For This" forState:UIControlStateNormal];
                    [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                    NSString *total_count=[NSString stringWithFormat:@"%@ Prayed for This",[Constant1 getStringObject:get_total_praised[@"count"]]];
                    [postimagecell.btn_praised addTarget:self action:@selector(btnpraised_actgroup:) forControlEvents:UIControlEventTouchUpInside];
                    [postimagecell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                }
                postimagecell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_unselected"];
                postimagecell.imageview_praised.textColor=[UIColor colorWithRed:209.0/255.0f green:209.0/255.0f blue:209.0/255.0f alpha:1.0];
            }
        }
        
        NSDictionary *get_total_commnet= dict[@"get_total_commnets"];
        if ([get_total_commnet isKindOfClass:[NSNull class]]){
            postimagecell.mainview_height.constant=0;
            postimagecell.heightof_seemorecomments.constant=0;
            postimagecell.heightofLabelconstraint.constant=label_personViewHeight;
            [self.view layoutIfNeeded];
        }else{
            NSMutableArray *get_latest=dict[@"latest_comments"];
            NSString *like=[NSString stringWithFormat:@"%@",get_total_commnet[@"count"]];
            int like_count = [like intValue];
            //  set comment here
            
            int indexpathOfRow=10*indexPath.row;
            if (like_count==1) {
                postimagecell.view_comment1.hidden=NO;
                postimagecell.view_comment2.hidden=YES;
                postimagecell.view_comment3.hidden=YES;
                postimagecell.mainview_height.constant=61;
                postimagecell.heightof_seemorecomments.constant=0;
                postimagecell.heightofLabelconstraint.constant=label_personViewHeight;
                NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                if ([getobjcet_details isKindOfClass:[NSNull class]]){
                    postimagecell.imageview_comment1.image=[self imageFromColorDefault:[UIColor whiteColor]];
                }else{
                    
                    postimagecell.imageview_comment1.tag=indexpathOfRow;
                    postimagecell.lbl_commentname1.tag=indexpathOfRow;
                    postimagecell.imageview_comment1.userInteractionEnabled=YES;
                    postimagecell.lbl_commentname1.userInteractionEnabled=YES;
                    
                    UITapGestureRecognizer *commentImage_gesturelike1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.imageview_comment1 addGestureRecognizer:commentImage_gesturelike1];
                    
                    UITapGestureRecognizer *commentpersongesturelike1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.lbl_commentname1 addGestureRecognizer:commentpersongesturelike1];
                    
                    NSString *influcerimage=[NSString stringWithFormat:@"%@",[getobjcet_details valueForKey:@"user_type"]];
                    if ([influcerimage isEqualToString:@"2"]) {
                        postimagecell.commentimage1.hidden=NO;
                        [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    else{
                        postimagecell.commentimage1.hidden=YES;
                        [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    
                    postimagecell.lbl_commentname1.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:0][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:0][@"get_object_user_details"][@"last_name"]];
                }
                postimagecell.pplableComment1.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:0][@"body"]];
                NSError *error = nil;
                detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                matches = [detector matchesInString:postimagecell.pplableComment1.text options:0 range:NSMakeRange(0, postimagecell.pplableComment1.text.length)];
                
                UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                [postimagecell.pplableComment1 addGestureRecognizer:singleTap];
                [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment1];
                
            }else  if (like_count==2) {
                postimagecell.view_comment1.hidden=NO;
                postimagecell.view_comment2.hidden=NO;
                postimagecell.view_comment3.hidden=YES;
                
                postimagecell.mainview_height.constant=122;
                postimagecell.heightof_seemorecomments.constant=0;
                
                postimagecell.heightofLabelconstraint.constant=label_personViewHeight;
                //                NSLog(@"dict is%@",get_latest);
                NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                NSDictionary *getobjcet_details2=[get_latest objectAtIndex:1][@"get_object_user_details"];
                if ([getobjcet_details isKindOfClass:[NSNull class]]){
                    postimagecell.imageview_comment1.image=[self imageFromColorDefault:[UIColor whiteColor]];
                }else{
                    
                    postimagecell.imageview_comment1.tag=indexpathOfRow;
                    postimagecell.lbl_commentname1.tag=indexpathOfRow;
                    
                    postimagecell.imageview_comment1.userInteractionEnabled=YES;
                    postimagecell.lbl_commentname1.userInteractionEnabled=YES;
                    
                    UITapGestureRecognizer *commentImage_gesturelike2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.imageview_comment1 addGestureRecognizer:commentImage_gesturelike2];
                    
                    UITapGestureRecognizer *commentpersongesturelike2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.lbl_commentname1 addGestureRecognizer:commentpersongesturelike2];
                    NSString *influcerimage2=[NSString stringWithFormat:@"%@",[getobjcet_details valueForKey:@"user_type"]];
                    if ([influcerimage2 isEqualToString:@"2"]) {
                        postimagecell.commentimage1.hidden=NO;
                        [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    else{
                        postimagecell.commentimage1.hidden=YES;
                        [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    postimagecell.lbl_commentname1.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:0][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:0][@"get_object_user_details"][@"last_name"]];
                }
                if ([getobjcet_details2 isKindOfClass:[NSNull class]]){
                    postimagecell.imageview_comment2.image=[self imageFromColorDefault:[UIColor whiteColor]];
                }else{
                    
                    postimagecell.imageview_comment2.tag=indexpathOfRow+1;
                    postimagecell.lbl_commentname2.tag=indexpathOfRow+1;
                    
                    postimagecell.imageview_comment2.userInteractionEnabled=YES;
                    postimagecell.lbl_commentname2.userInteractionEnabled=YES;
                    
                    UITapGestureRecognizer *commentImage_gesturelike2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.imageview_comment2 addGestureRecognizer:commentImage_gesturelike2];
                    
                    UITapGestureRecognizer *commentpersongesturelike2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.lbl_commentname2 addGestureRecognizer:commentpersongesturelike2];
                    
                    NSString *influcerimage2=[NSString stringWithFormat:@"%@",[getobjcet_details valueForKey:@"user_type"]];
                    if ([influcerimage2 isEqualToString:@"2"]) {
                        postimagecell.commentimage2.hidden=NO;
                        [postimagecell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    else{
                        postimagecell.commentimage2.hidden=YES;
                        [postimagecell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    
                    
                    
                    postimagecell.lbl_commentname2.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:1][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:1][@"get_object_user_details"][@"last_name"]];
                }
                postimagecell.pplableComment3.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:0][@"body"]];
                NSError *error = nil;
                detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                matches = [detector matchesInString:postimagecell.pplableComment3.text options:0 range:NSMakeRange(0, postimagecell.pplableComment3.text.length)];
                
                UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                [postimagecell.pplableComment3 addGestureRecognizer:singleTap];
                [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment3];
                
                postimagecell.pplableComment2.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:1][@"body"]];
                //                NSError *error = nil;
                detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                matches = [detector matchesInString:postimagecell.pplableComment2.text options:0 range:NSMakeRange(0, postimagecell.pplableComment2.text.length)];
                
                UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                [postimagecell.pplableComment2 addGestureRecognizer:singleTap1];
                [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment2];
                
                
            }else  if (like_count>=3)
            {
                NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                NSDictionary *getobjcet_details2=[get_latest objectAtIndex:1][@"get_object_user_details"];
                NSDictionary *getobjcet_details3=[get_latest objectAtIndex:1][@"get_object_user_details"];
                postimagecell.view_comment1.hidden=NO;
                postimagecell.view_comment2.hidden=NO;
                postimagecell.view_comment3.hidden=NO;
                if ([getobjcet_details isKindOfClass:[NSNull class]]){
                    postimagecell.imageview_comment1.image=[self imageFromColorDefault:[UIColor whiteColor]];
                }else{
                    
                    postimagecell.imageview_comment1.tag=indexpathOfRow;
                    postimagecell.lbl_commentname1.tag=indexpathOfRow;
                    
                    postimagecell.imageview_comment1.userInteractionEnabled=YES;
                    postimagecell.lbl_commentname1.userInteractionEnabled=YES;
                    
                    UITapGestureRecognizer *commentImage_gesturelike2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.imageview_comment1 addGestureRecognizer:commentImage_gesturelike2];
                    
                    UITapGestureRecognizer *commentpersongesturelike2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.lbl_commentname1 addGestureRecognizer:commentpersongesturelike2];
                    NSString *influcerimage31=[NSString stringWithFormat:@"%@",[getobjcet_details valueForKey:@"user_type"]];
                    if ([influcerimage31 isEqualToString:@"2"]) {
                        postimagecell.commentimage1.hidden=NO;
                        [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    else{
                        postimagecell.commentimage1.hidden=YES;
                        [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    
                    
                    
                    postimagecell.lbl_commentname1.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:0][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:0][@"get_object_user_details"][@"last_name"]];
                }
                if ([getobjcet_details2 isKindOfClass:[NSNull class]]){
                    postimagecell.imageview_comment2.image=[self imageFromColorDefault:[UIColor whiteColor]];
                }else{
                    
                    postimagecell.imageview_comment2.tag=indexpathOfRow+1;
                    postimagecell.lbl_commentname2.tag=indexpathOfRow+1;
                    postimagecell.imageview_comment2.userInteractionEnabled=YES;
                    postimagecell.lbl_commentname2.userInteractionEnabled=YES;
                    
                    UITapGestureRecognizer *commentImage_gesturelike2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.imageview_comment2 addGestureRecognizer:commentImage_gesturelike2];
                    
                    UITapGestureRecognizer *commentpersongesturelike2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.lbl_commentname2 addGestureRecognizer:commentpersongesturelike2];
                    
                    NSString *influcerimage32=[NSString stringWithFormat:@"%@",[getobjcet_details2 valueForKey:@"user_type"]];
                    if ([influcerimage32 isEqualToString:@"2"]) {
                        postimagecell.commentimage2.hidden=NO;
                        [postimagecell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    else{
                        postimagecell.commentimage2.hidden=YES;
                        [postimagecell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    
                    
                    
                    
                    postimagecell.lbl_commentname2.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:1][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:1][@"get_object_user_details"][@"last_name"]];
                }
                if ([getobjcet_details3 isKindOfClass:[NSNull class]]){
                    postimagecell.imageview_comment3.image=[self imageFromColorDefault:[UIColor whiteColor]];
                }else{
                    postimagecell.imageview_comment3.tag=indexpathOfRow+2;
                    postimagecell.lbl_commentname3.tag=indexpathOfRow+2;
                    postimagecell.imageview_comment3.userInteractionEnabled=YES;
                    postimagecell.lbl_commentname3.userInteractionEnabled=YES;
                    
                    UITapGestureRecognizer *commentImage_gesturelike3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.imageview_comment3 addGestureRecognizer:commentImage_gesturelike3];
                    
                    UITapGestureRecognizer *commentpersongesturelike3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_commentGroup:)];
                    [postimagecell.lbl_commentname3 addGestureRecognizer:commentpersongesturelike3];
                    NSString *influcerimage33=[NSString stringWithFormat:@"%@",[getobjcet_details2 valueForKey:@"user_type"]];
                    if ([influcerimage33 isEqualToString:@"2"]) {
                        postimagecell.commentimage3.hidden=NO;
                        [postimagecell.imageview_comment3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:2][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    else{
                        postimagecell.commentimage3.hidden=YES;
                        [postimagecell.imageview_comment3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:2][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                    }
                    
                    postimagecell.lbl_commentname3.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:2][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:2][@"get_object_user_details"][@"last_name"]];
                }
                postimagecell.pplableComment1.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:0][@"body"]];
                NSError *error = nil;
                detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                matches = [detector matchesInString:postimagecell.pplableComment1.text options:0 range:NSMakeRange(0, postimagecell.pplableComment1.text.length)];
                
                UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                [postimagecell.pplableComment1 addGestureRecognizer:singleTap];
                [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment1];
                
                
                postimagecell.pplableComment2.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:1][@"body"]];
                //                NSError *error = nil;
                detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                matches = [detector matchesInString:postimagecell.pplableComment2.text options:0 range:NSMakeRange(0, postimagecell.pplableComment2.text.length)];
                
                UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                [postimagecell.pplableComment2 addGestureRecognizer:singleTap1];
                [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment2];
                postimagecell.pplableComment3.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:2][@"body"]];
                //                NSError *error = nil;
                detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                matches = [detector matchesInString:postimagecell.pplableComment3.text options:0 range:NSMakeRange(0, postimagecell.pplableComment3.text.length)];
                
                UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                [postimagecell.pplableComment3 addGestureRecognizer:singleTap2];
                [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment3];
                
                postimagecell.heightof_seemorecomments.constant=0;
                postimagecell.mainview_height.constant=183;
                postimagecell.bottomofcommentView.constant=8;
                int totalcount=[get_total_commnet[@"count"] intValue];
                if (totalcount>3) {
                    postimagecell.bottomofcommentView.constant=25;
                    postimagecell.heightof_seemorecomments.constant=32;
                    postimagecell.btn_seemorecomments.hidden=NO;
                    postimagecell.btn_seemorecomments.tag=indexPath.row;
                    [postimagecell.btn_seemorecomments addTarget:self action:@selector(seemorecommentMethod:) forControlEvents:UIControlEventTouchUpInside];
                }
                postimagecell.heightofLabelconstraint.constant=label_personViewHeight;
            }
            
            [self.view layoutIfNeeded];
            
        }
    }
}




- (int)lineCountForText:(NSString *) text
{
    UIFont *font = light;
    CGRect rect = [text boundingRectWithSize:CGSizeMake(self.view.frame.size.width, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName : font}
                                     context:nil];
    return ceil(rect.size.height / font.lineHeight);
}

-(void)seemorecommentMethod:(UIButton*)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableview];
    NSIndexPath *indexPath = [self.tableview indexPathForRowAtPoint:buttonPosition];
    DetailMygruopViewController2 *detailViewGroup=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygruopViewController2"];
    //    cell index pass to next controller
    detailViewGroup.cellidGRoup=[NSString stringWithFormat:@"%ld",indexPath.row];
    //
    NSDictionary *dict=[webgroupArray objectAtIndex:indexPath.row];
    detailViewGroup.postIdStr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
    detailViewGroup.navigationTitil2=_navigationTitle;
    detailViewGroup.detail2Dict=[dict valueForKey:@"get_attachments"];
    NSString *contentType=[dict valueForKey:@"content"];
    NSMutableDictionary *communityDict=[dict valueForKey:@"get_subject_user_details"];
    // cnahges dict
    if ([communityDict isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *userDict=[communityDict valueForKey:@"usersprofile"];
        NSString *imageStr=[userDict objectForKey:@"profile_picture"];
        NSString *imageurl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,imageStr];
        detailViewGroup.imageURl=imageurl;
        detailViewGroup.contentStr=contentType;
        NSString *fullName1 = [NSString stringWithFormat: @"%@  %@",[communityDict valueForKey:@"first_name"],[communityDict valueForKey:@"last_name"]];
        detailViewGroup.nameLabel=fullName1;
        
    }
    
    detailViewGroup.maindict=[webgroupArray objectAtIndex:indexPath.row];
    NSMutableArray *fullArray=[[NSMutableArray alloc]init];
    [fullArray addObject:[webgroupArray objectAtIndex:indexPath.row]];
    detailViewGroup.Fullmaindict=fullArray;
    [self.navigationController pushViewController:detailViewGroup animated:YES];
    
}

// latest comment of group

- (void)subject_touch_commentGroup:(UITapGestureRecognizer*)sender {
    int index=(int)(sender.view.tag/10);
    int subIndex=(int)(sender.view.tag%10);
    NSDictionary *dict=[webgroupArray objectAtIndex:index];
    NSArray *latestComment=[dict valueForKey:@"latest_comments"];
    if ([latestComment isKindOfClass:[NSArray class]]) {
        NSMutableDictionary *userData=[latestComment objectAtIndex:subIndex];
        if ([userData isKindOfClass:[NSMutableDictionary class]]) {
            NSString *userid=[NSString stringWithFormat:@"%@",[userData valueForKey:@"user_id"]];
            NSString *loginId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
            NSMutableDictionary *getobjectDict=[userData valueForKey:@"get_object_user_details"];
            NSString *influencericheck;
            if ([getobjectDict isKindOfClass:[NSMutableDictionary class]]) {
                influencericheck=[NSString stringWithFormat:@"%@",[getobjectDict valueForKey:@"user_type"]];
            }
            
            
            if ([influencericheck isEqualToString:@"2"]) {
                InfluencerProfileController *profileView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                profileView.influencerDict=getobjectDict;
                [self.navigationController pushViewController:profileView animated:YES];
                
            }
            else{
                if ([userid isEqualToString:loginId]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        Profile_editViewController *profileView=[self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
                        [self.navigationController pushViewController:profileView animated:YES];
                    });
                    
//                    CGRect    screenRect = [[UIScreen mainScreen] bounds];
//                    UIView *viewProfile=[[UIView alloc]initWithFrame:CGRectMake(0, 0,screenRect.size.width,60)];
//                    viewProfile.backgroundColor=UIColorFromRGB(0x0088CF);
//                    [[profileView view] addSubview:viewProfile];
//                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//                    [button addTarget:self
//                               action:@selector(backgroup)
//                     forControlEvents:UIControlEventTouchUpInside];
//                    [button setImage:[UIImage imageNamed:@"leftArrow"] forState:UIControlStateNormal];
//                    button.frame = CGRectMake(5, 25,30, 30);
//                    [viewProfile addSubview:button];
//                    
//                    UILabel *lbl1 = [[UILabel alloc] init];
//                    [lbl1 setFrame:CGRectMake(screenRect.size.width/2-20,15,100,50)];
//                    lbl1.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
//                    
//                    lbl1.backgroundColor=[UIColor clearColor];
//                    lbl1.textColor=[UIColor whiteColor];
//                    lbl1.text= @"Profile";
//                    [viewProfile addSubview:lbl1];
//                    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"checkprofile"];
//                    [[NSUserDefaults standardUserDefaults] synchronize];
//                    [self.navigationController pushViewController:profileView animated:YES];
                    
                }
                else{
                    [self profileView:friendprofile anfriend_ID:userid];
                    
                }
                
                
            }
            
        }
        
        
    }
    
}


-(void)profileView :(NSString*)methodName anfriend_ID:(NSString*)friendID{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : friendID,
                           };
    
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                NSMutableArray *messageArray;
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableDictionary *seemoreDict=[response valueForKey:@"data"];
                        FriendsProfileViewController *frndSeemore=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                        frndSeemore.friendProfileDict=seemoreDict;
                        frndSeemore.postFriendProfileDict=seemoreDict;
                        //                        [self presentViewController:frndSeemore animated:YES completion:nil];
                        [self.navigationController pushViewController:frndSeemore animated:YES];
                    });
                    
                }else{
                    messageArray=[response valueForKey:@"message"];
                    if ([Constant1 validateArray:messageArray]) {
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    }
                    
                    
                }
                
            }
        }
        
    } errorBlock:^(NSError *error)
     {
         //         NSLog(@"error");
         
     }];
}


- (void)handleSingleTapGestureName34:(UITapGestureRecognizer *)tapgesture {
    
    DetailMygruopViewController2 *detailViewGroup=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygruopViewController2"];
    NSDictionary *dict=[webgroupArray objectAtIndex:tapgesture.view.tag];
    detailViewGroup.postIdStr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
    detailViewGroup.navigationTitil2=_navigationTitle;
    detailViewGroup.detail2Dict=[dict valueForKey:@"get_attachments"];
    NSString *contentType=[dict valueForKey:@"content"];
    NSMutableDictionary *communityDict=[dict valueForKey:@"get_subject_user_details"];
    NSMutableDictionary *userDict=[communityDict valueForKey:@"usersprofile"];
    NSString *imageStr=[userDict objectForKey:@"profile_picture"];
    NSString *imageurl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,imageStr];
    detailViewGroup.imageURl=imageurl;
    detailViewGroup.contentStr=contentType;
    NSString *fullName1 = [NSString stringWithFormat: @"%@  %@",[communityDict valueForKey:@"first_name"],[communityDict valueForKey:@"last_name"]];
    detailViewGroup.nameLabel=fullName1;
    detailViewGroup.maindict=[webgroupArray objectAtIndex:tapgesture.view.tag];
    NSMutableArray *fullArray=[[NSMutableArray alloc]init];
    [fullArray addObject:[webgroupArray objectAtIndex:tapgesture.view.tag]];
    detailViewGroup.Fullmaindict=fullArray;
    [self.navigationController pushViewController:detailViewGroup animated:YES];
}

#pragma mark Praise and Prayer method

-(void)btnpraised_actgroup:(UIButton*)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableview];
    NSIndexPath *indexPath = [self.tableview indexPathForRowAtPoint:buttonPosition];
    
    NSDictionary *object_dict=[webgroupArray objectAtIndex:sender.tag];
    NSString *posttype=[NSString stringWithFormat:@"%@",[object_dict objectForKey:@"post_type"]];
    NSDictionary *getsubjectDict=[object_dict objectForKey:@"get_subject_user_details"];
    NSString *subjectName;
    if ([getsubjectDict isKindOfClass:[NSDictionary class]]) {
        subjectName=[NSString stringWithFormat:@"%@ %@",[getsubjectDict objectForKey:@"first_name"],[getsubjectDict objectForKey:@"last_name"]];
    }
    else{
        subjectName=@"";
    }
    
    
    if ([sender.titleLabel.text isEqualToString:@"Praise"]||[sender.titleLabel.text isEqualToString:@"Pray For This"]){
        if ([sender.titleLabel.text isEqualToString:@"Praise"]){
            
            [TenjinSDK sendEventWithName:@"Praise_Button_Groups_Tapped"];
            [FBSDKAppEvents logEvent:@"Praise_Button_Groups_Tapped"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Praise_Button_Groups_Tapped"]];
            
        }else if ([sender.titleLabel.text isEqualToString:@"Pray For This"]){
            [TenjinSDK sendEventWithName:@"PrayForThis_Button_Groups_Tapped"];
            [FBSDKAppEvents logEvent:@"PrayForThis_Button_Groups_Tapped"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"PrayForThis_Button_Groups_Tapped"]];
        }
        
        
        
        NSDictionary *dict = @{
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                               @"object_id":object_dict[@"id"],
                               @"object_type":@"post",
                               };
        FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath ];
        //  feedImage_cell=cell;
        [self liked1:dict andmethodName:wall_like andcell_index:[sender tag]  andfeedimage_cell:cell post_type:posttype checklike:@"wall_like" subjectNamepost:subjectName];
        
        // [self liked]
    }else{
        //        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableview];
        //        NSIndexPath *indexPath = [self.tableview indexPathForRowAtPoint:buttonPosition];
        NSDictionary *object_dict=[webgroupArray objectAtIndex:sender.tag];
        NSDictionary *dict = @{
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                               @"object_id":object_dict[@"id"],
                               @"object_type":@"post",
                               
                               };
        //        NSLog(@"dict is%@",dict);
        
        FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
        
        [self liked1:dict andmethodName:wall_unlike andcell_index:[sender tag]  andfeedimage_cell:cell post_type:posttype checklike:@"wall_unlike" subjectNamepost:subjectName];
        
    }
    
    
}
-(void)reloadCell:(NSUInteger)cell_index{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cell_index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableview reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
}
-(void)liked1:(NSDictionary*)dict andmethodName:(NSString*)methodName andcell_index:(NSUInteger)cell_index  andfeedimage_cell:(FeedwithImagePostTableViewCell*)feedimagecell post_type:posttype checklike:(NSString *)checklike subjectNamepost:(NSString*)subjectName{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        //        NSLog(@"dict is%@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
            NSMutableArray *messageArray;
            //            NSLog(@"response is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    messageArray=[response valueForKey:@"message"];
                    NSString *checkContentType;
                    // implementation for mixpanel for like nad unlike
                    //                    int i=0,j=0,k=0;
                    
                    int i,j,k;
                    NSString *dataLast=[Constant1 createTimeStamp];
                    NSString *contenID=[NSString stringWithFormat:@"%@",[dict objectForKey:@"object_id"]];
                    
                    //                    NSString *checkContentType,*totalPraises,*totalPrayers;
                    if ([checklike isEqualToString:@"wall_like"]) {
                        if ([posttype isEqualToString:@"2"]) {
                            checkContentType=@"Prayer Request";
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Prayers" by:@1];
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@1];
                            
                            
                        }
                        if ([posttype isEqualToString:@"3"]) {
                            checkContentType=@"Update";
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@1];
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@1];
                        }
                        if ([posttype isEqualToString:@"1"]) {
                            checkContentType=@"Update";
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@1];
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@1];
                        }
                        
                        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last like": dataLast}];
                        
                        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Content ID":contenID,@"Posted by":subjectName,@"Content type":checkContentType,@"Date of last like":dataLast}];
                        
                        [[AppDelegate sharedAppDelegate].mxPanel track:@"Like"
                                                            properties:@{@"Content ID":contenID,@"Posted by":subjectName,@"Content type":checkContentType,@"Date of last like":dataLast}
                         ];
                        
                    }
                    else{
                        if ([posttype isEqualToString:@"2"]) {
                            checkContentType=@"Prayer Request";
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Prayers" by:@-1];
                            
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@-1];
                        }
                        if ([posttype isEqualToString:@"3"]) {
                            checkContentType=@"Update";
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@-1];
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@-1];
                        }
                        if ([posttype isEqualToString:@"1"]) {
                            checkContentType=@"Update";
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@-1];
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@-1];
                        }
                        
                        
                        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Content ID":contenID,@"Posted by":subjectName,@"Content type":checkContentType,@"Date of last like":dataLast}];
                        
                    }
                    
                    ////// mix panel end //////
                    
                    strresponse=[messageArray objectAtIndex:0];
                    if([response[@"status"]intValue ] ==1){
                        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                        
                        NSMutableArray *dict_arr=[response valueForKey:@"like_count"];
                        NSString *count;
                        for (NSDictionary* dict in dict_arr) {
                            count=[NSString stringWithFormat:@"%@",[dict valueForKey:@"count"]];
                        }
                        int count_val=[count intValue];
                        
                        [dict setObject:count forKey:@"count"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([strresponse  isEqualToString:@"Like Successfully added"]) {
                                if (count_val<=1) {
                                    
                                    [[webgroupArray objectAtIndex:cell_index]setObject:dict forKey:@"get_total_praised"];
                                    [[webgroupArray objectAtIndex:cell_index]setObject:@"1" forKey:@"is_like"];
                                    [self reloadCell:cell_index];
                                }else{
                                    
                                    [webgroupArray objectAtIndex:cell_index][@"get_total_praised"][@"count"]=count;
                                    [[webgroupArray objectAtIndex:cell_index]setObject:@"1" forKey:@"is_like"];
                                    [self reloadCell:cell_index];
                                }
                            }
                            else{
                                if (count_val==0) {
                                    [[webgroupArray objectAtIndex:cell_index]setObject:[NSNull null] forKey:@"get_total_praised"];
                                    [[webgroupArray objectAtIndex:cell_index]setObject:@"0" forKey:@"is_like"];
                                    [self reloadCell:cell_index];
                                }else
                                {
                                    [webgroupArray objectAtIndex:cell_index][@"get_total_praised"][@"count"]=count;
                                    [[webgroupArray objectAtIndex:cell_index]setObject:@"0" forKey:@"is_like"];
                                    [self reloadCell:cell_index];
                                }
                            }
                            //    [pullToRefreshManager_ tableViewReloadFinished];
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        });
                    }
                }
            }
        } errorBlock:^(id error)
         {
             //             NSLog(@"error is %@",error);
         }];
    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)AddDiscussionMethod:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    StatusViewController *statusView=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
    statusView.isGroupPost = YES;
    statusView.status_para=@"1";
    statusView.addDiscusssionflag=@"GroupAddDiscussion";
    [self.navigationController pushViewController:statusView animated:YES];
    //[self presentViewController:statusView animated:YES completion:nil];
}
- (IBAction)addPrayermethod:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    StatusViewController *statusView=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
    statusView.status_para=@"2";
    statusView.isGroupPost = YES;
    statusView.addDiscusssionflag=@"GroupAddDiscussion";
    [self.navigationController pushViewController:statusView animated:YES];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [pullToRefreshManager_ tableViewScrolled];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate: (BOOL)decelerate
{
    [pullToRefreshManager_ tableViewReleased];
}

- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager {
    if (current_page==total_pagenumber) {
    }
    else
    {
        [self getDetailCommunity:current_page+1];
    }
}

-(void)getDetailCommunity:(NSInteger)pageNumber{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }
    else {
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"comm_id":_idUser,
                               @"page" :[NSString stringWithFormat:@"%ld",(long)pageNumber],                   };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:walldetails withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            NSMutableDictionary *responseDict=[response objectForKey:@"data"];
                            NSString *current_page1=[responseDict valueForKey:@"current_page"];
                            current_page=[current_page1 intValue];
                            NSString *totalPage=[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"last_page"]];
                            total_pagenumber=[totalPage intValue];
                            NSMutableArray *localarray=[responseDict objectForKey:@"data"];
                            [webgroupArray addObjectsFromArray:localarray];
                            
                            for (int i=(int)[[AppDelegate sharedAppDelegate].groupimageArray count]; i<[webgroupArray count]; i++) {
                            [[AppDelegate sharedAppDelegate].groupimageArray addObject:[NSNull null]];
                                    }
                            [self.tableview reloadData];
                            [pullToRefreshManager_ tableViewReloadFinished];
                            
                        }
                        else{
                            NSMutableArray  *messageArray=[response valueForKey:@"message"];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            });
                        }
                    });
                }
            }
        }
                                                       errorBlock:^(id error)
         {
         }];
        
    }
    
}

-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    //    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
    
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    //    NSLog(@"Did finish modal presentation");
    // [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dropdownbtnact:(UIButton*)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableview];
        NSIndexPath *indexPath = [self.tableview indexPathForRowAtPoint:buttonPosition];
        
        NSDictionary *dict=[webgroupArray objectAtIndex:indexPath.row];
        if (dict[@"get_subject_user_details"][@"id"] ==[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]){
            self.menuItems = [NSArray arrayWithObjects:@"Delete",nil];
        }else{
            self.menuItems = [NSArray arrayWithObjects:@"Report abuse",nil];
        }
        UIView *parentCell = sender.superview.superview.superview.superview;
        NSString *postid=[NSString stringWithFormat:@"%@",[webgroupArray objectAtIndex:sender.tag][@"id"]];
        FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableview cellForRowAtIndexPath:indexPath];
        [self.menuPopover3 dismissMenuPopover];
        CGRect dropdownViewframe =CGRectMake(cell.dropdownbutton.frame.origin.x-32, cell.dropdownbutton.frame.origin.y+17, 70, 35);
        if (self.menuPopover3 ) {
            self.menuPopover3=nil;
        }
        
        self.menuPopover3=[[MLKMenuPopover alloc]initWithFrame:dropdownViewframe menuItems:self.menuItems imagemenuItems:nil isSearch:NO];
        
        self.menuPopover3.postid=postid;
        self.menuPopover3.cellindex=sender.tag;
        self.menuPopover3.menuPopoverDelegate = self;
        [self.menuPopover3 showInView:parentCell];
        
    });
    
}


#pragma  Mark Delete POP view method

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex andid:(NSString *)postid andcellindex:(int)cellindexpath
{
    [self.menuPopover3 dismissMenuPopover];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSString *methodname;
    NSDictionary *dict ;
    NSDictionary *dictmain=[webgroupArray objectAtIndex:cellindexpath];
    
    if (dictmain[@"get_subject_user_details"][@"id"] ==[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]){
        //        NSLog(@"delete  is%@",postid);
        
        methodname=delete_wall;
        
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 @"id" : postid,
                 };
    }
    else
    {
        methodname=abuse;
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 @"post_id" : postid,
                 };
    }
    //    NSLog(@"arr is%@",webgroupArray);
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodname withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                NSMutableArray *messageArray;
                if([response[@"status"]intValue ] ==1){
                    messageArray=[response valueForKey:@"message"];
                    NSString *str=[messageArray objectAtIndex:0];
                    if ( [str isEqualToString:@"You have successfully left the Post!"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.menuPopover3 dismissMenuPopover];
                            if ([methodname isEqualToString:delete_wall]) {
                                [webgroupArray removeObjectAtIndex:cellindexpath];
                        [[AppDelegate sharedAppDelegate].groupimageArray removeObjectAtIndex:cellindexpath];
                                [self.tableview reloadData];
                            }
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        });
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.menuPopover3 dismissMenuPopover];
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        });
                    }
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.menuPopover3 dismissMenuPopover];
                    });
                    messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    });
                }
            }
        }
    } errorBlock:^(id error)
     {
         [self.menuPopover3 dismissMenuPopover];
         //         NSLog(@"error");
         
     }];
    
}

- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

#pragma pplabel delegate
#pragma mark -

- (void)label:(PPLabel *)label didBeginTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    NSString *testURL=[NSString stringWithFormat:@"%@",label.text];
    //    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    //    NSArray *matches = [linkDetector matchesInString:testURL options:0 range:NSMakeRange(0, [testURL length])];
    //    for (NSTextCheckingResult *match in matches) {
    //        if ([match resultType] == NSTextCheckingTypeLink) {
    //            NSURL *url = [match URL];
    //            if ([[UIApplication sharedApplication] canOpenURL:url]) {
    //                [[UIApplication sharedApplication] openURL:url];
    //            }
    //        }
    //    }
    
}

- (void)label:(PPLabel *)label didMoveTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:charIndex custum:label];
}

- (void)label:(PPLabel *)label didEndTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:NSNotFound custum:label];
    //
    //    for (NSTextCheckingResult *match in self.matches) {
    //
    //        if ([match resultType] == NSTextCheckingTypeLink) {
    //
    //            NSRange matchRange = [match range];
    //
    //            if ([self isIndex:charIndex inRange:matchRange]) {
    //
    //                [[UIApplication sharedApplication] openURL:match.URL];
    //                break;
    //            }
    //        }
    //    }
    
}

- (void)label:(PPLabel *)label didCancelTouch:(UITouch *)touch {
    
    //    [self highlightLinksWithIndex:NSNotFound custum:label];
}

#pragma mark -

- (BOOL)isIndex:(CFIndex)index inRange:(NSRange)range {
    return index > range.location && index < range.location+range.length;
}

- (void)highlightLinksWithIndex:(CFIndex)index custum:(PPLabel*)label {
    
    NSMutableAttributedString* attributedString = [label.attributedText mutableCopy];
    
    for (NSTextCheckingResult *match in matches) {
        
        if ([match resultType] == NSTextCheckingTypeLink) {
            
            NSRange matchRange = [match range];
            
            if ([self isIndex:index inRange:matchRange]) {
                [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:matchRange];
                
            }
            else {
                [attributedString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x00ADEB) range:matchRange];
            }
            
            [attributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:matchRange];
        }
    }
    
    label.attributedText = attributedString;
}

- (void)handleTap:(UITapGestureRecognizer *)tapRecognizer
{
    PPLabel *textLabel = (PPLabel *)tapRecognizer.view;
    CGPoint tapLocation = [tapRecognizer locationInView:textLabel];
    NSString *testURL=[NSString stringWithFormat:@"%@",textLabel.text];
    
    // init text storage
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:textLabel.attributedText];
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    [textStorage addLayoutManager:layoutManager];
    
    // init text container
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(textLabel.frame.size.width, textLabel.frame.size.height+100) ];
    textContainer.lineFragmentPadding  = 0;
    textContainer.maximumNumberOfLines = textLabel.numberOfLines;
    textContainer.lineBreakMode        = textLabel.lineBreakMode;
    
    [layoutManager addTextContainer:textContainer];
    
    NSUInteger characterIndex = [layoutManager characterIndexForPoint:tapLocation
                                                      inTextContainer:textContainer
                             fractionOfDistanceBetweenInsertionPoints:NULL];
    
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:testURL options:0 range:NSMakeRange(0, [testURL length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSRange matchRange = [match range];
            if ([self isIndex:characterIndex inRange:matchRange]) {
                [[UIApplication sharedApplication] openURL:match.URL];
                break;
            }
        }
    }
    
    
}

-(float)calculateImageHeight:(float)boxWidth imageHeight:(float)imageHeight1 imageWidth:(float)imagewidth1{
    CGFloat screenHeight=[UIScreen mainScreen].bounds.size.height;
    CGFloat preferImageHeight=screenHeight-300;
    //    CGFloat actualImageHeight=imageHeight1;
    CGFloat widthView1,widthView;
    if (imagewidth1>imageHeight1) {
        widthView1=[Constant1 checkimagehieght];
        widthView=widthView1+30;
    }
    else{
        widthView=preferImageHeight;
        
    }
    return widthView;
}


@end
