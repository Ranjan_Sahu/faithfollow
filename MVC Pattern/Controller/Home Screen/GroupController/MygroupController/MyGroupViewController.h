//
//  MyGroupViewController.h
//  wireFrameSplash
//
//  Created by home on 6/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGroupViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic)IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIPageViewController *pageViewController;

@end
