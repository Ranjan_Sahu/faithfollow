//
//  GroupDiscussionViewController.m
//  wireFrameSplash
//
//  Created by home on 6/22/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "GroupDiscussionViewController.h"
#import "DiscusscustomTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "DetailMygruopViewController2.h"

@interface GroupDiscussionViewController ()<PPLabelDelegate>
{
    NSMutableArray *groupDiscussionArray;
    CGFloat heightOfLBL;
    NSDataDetector *detector;
    NSArray *matches;
    
}
@end

@implementation GroupDiscussionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    groupDiscussionArray=[[NSMutableArray alloc]init];
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableview.tableFooterView = footerView;
    [self fovoritelistMethod];
    
    
    // Do any additional setup after loading the view.
}

-(void)fovoritelistMethod{
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           };
//    NSLog(@"disct is%@",dict);
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:favourite_postlist withParameters:dict withHud:YES success:^(id response){
//        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([[response objectForKey:@"post"] isKindOfClass:[NSDictionary class]])
                        {
                           NSMutableDictionary *postDict=[response objectForKey:@"post"];
                           groupDiscussionArray=[postDict valueForKey:@"post"];
                          [self.tableview reloadData];
                        }
                        else if ([[response objectForKey:@"post"] isKindOfClass:[NSArray class]])
                        {
                            groupDiscussionArray=[response objectForKey:@"post"];
                            [self.tableview reloadData];

                        }
                    });
                }
            }
        }
        else{
            NSMutableArray  *messageArray=[response valueForKey:@"message"];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([messageArray count]>0) {
                 [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                }
                
            });
        }
        
    } errorBlock:^(id error)
     {
        [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//         NSLog(@"error");
     }];
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    DetailMygruopViewController2 *detailViewGroup=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygruopViewController2"];
    
    NSDictionary *dict1=[groupDiscussionArray objectAtIndex:indexPath.row];
    detailViewGroup.favoritediscussioncheck=@"yesDiscusion";
    if (![dict1 isKindOfClass:[NSNull class]]) {
    NSDictionary *dict=[dict1 valueForKey:@"post"];
    detailViewGroup.commentnumberCheck=@"discuss";
    detailViewGroup.commnetNumbervalue=[dict valueForKey:@"id"];
    if (![dict isKindOfClass:[NSNull class]]) {
        
        NSDictionary *gettitleDict=[dict valueForKey:@"get_object_community_details"];
        NSString *navigationTitle;
        if ([gettitleDict isKindOfClass:[NSNull class]]) {
        }
        else{
            navigationTitle=[gettitleDict valueForKey:@"name"];
        }
        detailViewGroup.createDate=@"yes";
        detailViewGroup.postIdStr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
        detailViewGroup.detail2Dict=[dict valueForKey:@"get_attachments"];
        NSString *contentType=[dict valueForKey:@"content"];
        NSMutableDictionary *communityDict=[dict valueForKey:@"get_subject_user_details"];
        NSMutableDictionary *userDict=[communityDict valueForKey:@"usersprofile"];
        NSString *imageStr=[userDict objectForKey:@"profile_picture"];
        NSString *imageurl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,imageStr];
        detailViewGroup.imageURl=imageurl;
        detailViewGroup.contentStr=contentType;
        NSString *fullName1 = [NSString stringWithFormat: @"%@  %@",[communityDict valueForKey:@"first_name"],[communityDict valueForKey:@"last_name"]];
        detailViewGroup.nameLabel=fullName1;
        detailViewGroup.navigationTitil2=navigationTitle;
        detailViewGroup.maindict=[dict1 valueForKey:@"post"];
        NSMutableArray *fullArray=[[NSMutableArray alloc]init];
        [fullArray addObject:[groupDiscussionArray objectAtIndex:indexPath.row]];
        detailViewGroup.Fullmaindict=fullArray;
        [[NSUserDefaults standardUserDefaults]setObject:@"backDiscuss" forKey:@"backDiscuss"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController pushViewController:detailViewGroup animated:YES];
    }
   
    }

}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [groupDiscussionArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableDictionary *dict=[groupDiscussionArray objectAtIndex:indexPath.row];
    NSDictionary *dictfavorite=[dict valueForKey:@"post"];
    DiscusscustomTableViewCell  *cell1 ;
    if (cell1 == nil) {
        cell1 =(DiscusscustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DiscussCell"];
    }
    
    if (![dictfavorite isKindOfClass:[NSNull class]]) {
        NSString *content=[dictfavorite valueForKey:@"content"];
        NSMutableDictionary *getSubjectDict=[dictfavorite valueForKey:@"get_subject_user_details"];
        NSString *fullName = [NSString stringWithFormat: @"%@  %@",[getSubjectDict valueForKey:@"first_name"],[getSubjectDict valueForKey:@"last_name"]];
        NSMutableDictionary *userDict=[getSubjectDict valueForKey:@"usersprofile"];
        NSString *imageStr=[userDict objectForKey:@"profile_picture"];
        NSString *imageurl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,imageStr];
        NSURL *imageURL = [NSURL URLWithString:imageurl];
        NSString *incluecercheck=[NSString stringWithFormat:@"%@",[getSubjectDict valueForKey:@"user_type"]];
        
        if ([incluecercheck isEqualToString:@"2"]) {
            cell1.influencerimage.hidden=NO;
            [cell1.gruopfavoriteImage   sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
        }else{
            cell1.influencerimage.hidden=YES;
            [cell1.gruopfavoriteImage   sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
        }
        
        cell1.discussionLBL.text=fullName;
        cell1.singleMotherLBL.hidden=YES;
        cell1.labelPP.text=content;
        cell1.labelPP.delegate=(id)self;
        NSError *error = nil;
        detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
        matches = [detector matchesInString:cell1.labelPP.text options:0 range:NSMakeRange(0, cell1.labelPP.text.length)];
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [cell1.labelPP addGestureRecognizer:singleTap];
        [self highlightLinksWithIndex:NSNotFound custum:cell1.labelPP];
        
    }
    return cell1;
}

- (UIImage *)imageFromColor:(UIColor *)color {
    
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    NSMutableDictionary *dict=[groupDiscussionArray objectAtIndex:indexPath.row];
//    NSMutableDictionary *dictfavorite=[dict valueForKey:@"post"];
//    if (![dictfavorite isKindOfClass:[NSNull class]]) {
//        NSString *content=[dictfavorite valueForKey:@"content"];
//        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Avenir-Medium" size:13]};
//        CGRect rect = [content boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-70, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes                                                                                                        context:nil];
//        heightOfLBL=rect.size.height;
//        static DiscusscustomTableViewCell *cell = nil;
//        static dispatch_once_t onceToken;
//        dispatch_once(&onceToken, ^{
//            cell = [self.tableview dequeueReusableCellWithIdentifier:@"DiscussCell"];
//        });
//
//        if (heightOfLBL>47.0) {
//            CGFloat actualhight=heightOfLBL;
//            [cell setNeedsUpdateConstraints];
//            [cell updateConstraintsIfNeeded];
//            [cell layoutIfNeeded];
//            return 55+actualhight;
//        }
//        else{
//            [cell setNeedsUpdateConstraints];
//            [cell updateConstraintsIfNeeded];
//            [cell layoutIfNeeded];
//            return 91;
//        }
//
//
//    }
//
//    return 0;
//
//}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma pplabel delegate
#pragma mark -

- (void)label:(PPLabel *)label didBeginTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:charIndex custum:label];
    
}

- (void)label:(PPLabel *)label didMoveTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:charIndex custum:label];
}

- (void)label:(PPLabel *)label didEndTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:NSNotFound custum:label];
    //
    //    for (NSTextCheckingResult *match in self.matches) {
    //
    //        if ([match resultType] == NSTextCheckingTypeLink) {
    //
    //            NSRange matchRange = [match range];
    //
    //            if ([self isIndex:charIndex inRange:matchRange]) {
    //
    //                [[UIApplication sharedApplication] openURL:match.URL];
    //                break;
    //            }
    //        }
    //    }
    
}

- (void)label:(PPLabel *)label didCancelTouch:(UITouch *)touch {
    
    //    [self highlightLinksWithIndex:NSNotFound custum:label];
}

#pragma mark -

- (BOOL)isIndex:(CFIndex)index inRange:(NSRange)range {
    return index > range.location && index < range.location+range.length;
}

- (void)highlightLinksWithIndex:(CFIndex)index custum:(PPLabel*)label {
    
    NSMutableAttributedString* attributedString = [label.attributedText mutableCopy];
    
    for (NSTextCheckingResult *match in matches) {
        
        if ([match resultType] == NSTextCheckingTypeLink) {
            
            NSRange matchRange = [match range];
            
            if ([self isIndex:index inRange:matchRange]) {
                [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:matchRange];
                
            }
            else {
                [attributedString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x00ADEB) range:matchRange];
            }
            
            [attributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:matchRange];
        }
    }
    
    label.attributedText = attributedString;
}

- (void)handleTap:(UITapGestureRecognizer *)tapRecognizer
{
    PPLabel *textLabel = (PPLabel *)tapRecognizer.view;
    CGPoint tapLocation = [tapRecognizer locationInView:textLabel];
    NSString *testURL=[NSString stringWithFormat:@"%@",textLabel.text];
    
    // init text storage
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:textLabel.attributedText];
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    [textStorage addLayoutManager:layoutManager];
    
    // init text container
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(textLabel.frame.size.width, textLabel.frame.size.height+100) ];
    textContainer.lineFragmentPadding  = 0;
    textContainer.maximumNumberOfLines = textLabel.numberOfLines;
    textContainer.lineBreakMode        = textLabel.lineBreakMode;
    
    [layoutManager addTextContainer:textContainer];
    
    NSUInteger characterIndex = [layoutManager characterIndexForPoint:tapLocation
                                                      inTextContainer:textContainer
                             fractionOfDistanceBetweenInsertionPoints:NULL];
    
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:testURL options:0 range:NSMakeRange(0, [testURL length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSRange matchRange = [match range];
            if ([self isIndex:characterIndex inRange:matchRange]) {
                [[UIApplication sharedApplication] openURL:match.URL];
                break;
            }
        }
    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
