//
//  GroupSearchViewController.h
//  wireFrameSplash
//
//  Created by home on 6/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface GroupSearchViewController : UIViewController
@property(strong,nonatomic)IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *airlines;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property(nonatomic,weak)IBOutlet  UISearchBar *searchbar;
@property (nonatomic, assign) bool isFiltered;
@property (strong, nonatomic) UIPageViewController *pageViewController;
- (IBAction)mygroupMethod:(id)sender;
// pop view for gender
@property (weak, nonatomic) IBOutlet UIView *popviewGender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *popviewGenderTopheight;
@property (weak, nonatomic) IBOutlet UIView *monthView;
@property (weak, nonatomic) IBOutlet UIView *dayView;
@property (weak, nonatomic) IBOutlet UIView *yearView;

@property (weak, nonatomic) IBOutlet UIButton *monthBTN;
@property (weak, nonatomic) IBOutlet UIButton *dayBTN;
@property (weak, nonatomic) IBOutlet UIButton *yearBTN;
@property (weak, nonatomic) IBOutlet UIButton *maleBTN;
@property (weak, nonatomic) IBOutlet UIButton *femaleBTN;
-(void)popUpSubmitClicked:(NSString *)dob gender:(NSString*)gender;
- (IBAction)yearDropdown:(id)sender;
- (IBAction)femaleMethod:(id)sender;
- (IBAction)maleMEthod:(id)sender;
- (IBAction)monthDropdown:(id)sender;
- (IBAction)dayDropdown:(id)sender;
- (IBAction)submitGenderBMethod:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *submitGenderBirthMethod;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomHeightCondstraint;

@end
