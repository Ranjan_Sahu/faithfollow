//
//  GroupSearchViewController.m
//  wireFrameSplash
//
//  Created by home on 6/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "GroupSearchViewController.h"
#import "CustomTableViewCell.h"
#import "FriendsProfileViewController.h"
#import "MyGroupViewController.h"
#import "SearchCommunityProfileView.h"
#import "MNMBottomPullToRefreshManager.h"
#import "UIImageView+WebCache.h"
#import "MyGroupViewController.h"
#import "DetailMygroupViewController1.h"
#import "KLCPopup.h"
#import "NeosPopup.h"
#import "popGroupViewController.h"
#import "Mixpanel.h"
@interface GroupSearchViewController ()<MNMBottomPullToRefreshManagerClient,UIAlertViewDelegate>
{
    NSMutableArray *searcharray,*request_dataArray,*webSearchArray;
    NSString *searchString;
    NSMutableDictionary *dictwebSearchArray;
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
    int pagenumber,last_pagenumberInt;
    int current_pageInt;
    KLCPopup *popviewSearchGroup;
     NeosPopup *nepopup;
    popGroupViewController *login;
    
}
@end

@implementation GroupSearchViewController
@synthesize isFiltered;

- (void)viewDidLoad {
    [super viewDidLoad];
    current_pageInt=1;
    _airlines=[[NSMutableArray alloc]initWithObjects:@"raj",@"Amit",@"Candan",@"ashok",@"depak",nil];
    searcharray=[[NSMutableArray alloc]init];
    webSearchArray=[[NSMutableArray alloc]init];
    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:0.0f tableView:_tableView withClient:self];
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    self.tableView.tableFooterView = footerView;

    [self.view makeToast:@"please search " duration:1.0 position:CSToastPositionCenter];
    
    // Customize SearchBar
    UITextField *TF_Search = (UITextField *)[self.searchbar valueForKey:@"searchField"];
    TF_Search.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1];
    TF_Search.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:242.0/255.0 blue:243.0/255.0 alpha:1];
    self.searchbar.barTintColor = [UIColor whiteColor];
    self.searchbar.tintColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1];
    self.searchbar.placeholder = @"Search";
    
    UILabel *TF_Label = (UILabel *)[TF_Search valueForKey:@"placeholderLabel"];
    TF_Label.textColor = [UIColor colorWithRed:151.0/255.0 green:175.0/255.0 blue:191.0/255.0 alpha:1];

    UIImageView *glassImg = (UIImageView *)TF_Search.leftView;
    //glassImg.image = [UIImage imageNamed:@"searchIcon30"];
    glassImg.image = [glassImg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    glassImg.tintColor = [UIColor colorWithRed:151.0/255.0 green:175.0/255.0 blue:191.0/255.0 alpha:1];
    
    self.searchbar.backgroundImage = [UIImage new];
    
//    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{
//                                                                                                 NSFontAttributeName: [UIFont fontWithName:@"Avenir-Roman" size:20],
//                                                                                                 }];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
//    _popviewGender.hidden=YES;
    [self allcommunityList];
}

-(void)allcommunityList{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"page" :@"1"
                           };
 
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:communitytLists withParameters:dict withHud:YES success:^(id response){
//        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        webSearchArray=[response valueForKey:@"data"];
                        NSString *currentPageValue=[response valueForKey:@"current_page"];
                        NSString *lastpageValue=[response valueForKey:@"last_page"];
                        current_pageInt=[currentPageValue intValue];
                        last_pagenumberInt=[lastpageValue intValue];
                        [self.tableView reloadData];
                    });
                }
            }
        }
        else{
            
            NSMutableArray  *messageArray=[response valueForKey:@"message"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
            });
        }
        
    } errorBlock:^(id error)
     {
        [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];

//         NSLog(@"error");
         
     }];
    }

}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate: (BOOL)decelerate
{
    [pullToRefreshManager_ tableViewReleased];
}


- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager
{

//NSLog(@"last page %d",last_pagenumberInt);
//    NSLog(@"current page %d",current_pageInt);
    if (current_pageInt==last_pagenumberInt) {
//        NSLog(@"end");
    }else{
        [self getDetailCommunity:current_pageInt+1];
    }


}

-(void)getDetailCommunity:(NSInteger)pageNumber{
    
     [_searchbar resignFirstResponder];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict = @{
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"page" :[NSString stringWithFormat:@"%ld",pageNumber],
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:communitytLists withParameters:dict withHud:YES success:^(id response){
//            NSLog(@"responmse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            NSMutableArray *localarray=[response objectForKey:@"data"];
                            NSString *currentPageValue=[response valueForKey:@"current_page"];
                            NSString *lastpageValue=[response valueForKey:@"last_page"];
                            current_pageInt=[currentPageValue intValue];
                            last_pagenumberInt=[lastpageValue intValue];
                            [webSearchArray addObjectsFromArray:localarray];
                            [self.tableView reloadData];
                            [pullToRefreshManager_ tableViewReloadFinished];
                        }

                        else{
                            NSMutableArray *messageArray=[[NSMutableArray alloc]init];
                            messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
        [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//             NSLog(@"error");

         }];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rowCount;
    if(self.isFiltered)
        rowCount = _searchResults.count;
    else
        rowCount = webSearchArray.count;
    
    return rowCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict=[webSearchArray objectAtIndex:indexPath.row];
    
    CustomTableViewCell *cell;
    if (cell==nil) {
        cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    }

    if(isFiltered){
        [cell.btn_GroupJoin  addTarget:self action:@selector(joincommunityGroup:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_GroupJoin setTag:indexPath.row];
        
        NSDictionary *filterdict=[_searchResults objectAtIndex:indexPath.row];
        cell.namelabel.text=[filterdict valueForKey:@"name"];
        NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[filterdict valueForKey:@"image"]];
        
        if ([[filterdict valueForKey:@"image"] isEqualToString:@""]) {
            [cell.search_Groupimage sd_setImageWithURL:nil placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
        }else{
            [cell.search_Groupimage sd_setImageWithURL:[NSURL URLWithString:str]  placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
        }
        
        if ([[filterdict valueForKey:@"add_status"] isEqualToString:@"Send Request"]) {
            [cell.btn_GroupJoin setTitle:@"Join" forState:UIControlStateNormal];
        }else{
            [cell.btn_GroupJoin setTitle:[filterdict valueForKey:@"add_status"] forState:UIControlStateNormal];
        }
        
    }
    else{
        NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[dict valueForKey:@"image"]];
        if ([[dict valueForKey:@"image"] isEqualToString:@""]) {
            [cell.search_Groupimage sd_setImageWithURL:nil placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
        }else{
            [cell.search_Groupimage sd_setImageWithURL:[NSURL URLWithString:str]  placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
        }
        
        cell.namelabel.text=[dict valueForKey:@"name"];
        [cell.btn_GroupJoin  addTarget:self action:@selector(joincommunityGroup:) forControlEvents:UIControlEventTouchUpInside];
        if ([[dict valueForKey:@"add_status"] isEqualToString:@"Send Request"]) {
            [cell.btn_GroupJoin setTitle:@"Join" forState:UIControlStateNormal];
        }else{
            [cell.btn_GroupJoin setTitle:[dict valueForKey:@"add_status"] forState:UIControlStateNormal];
        }
        [cell.btn_GroupJoin setTag:indexPath.row];
    }
    
    return cell;
 
}

- (UIImage *)imageFromColor:(UIColor *)color {
    
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)handleSingleTapGestureName34:(UITapGestureRecognizer *)tapgesture {
    
    dictwebSearchArray=[webSearchArray objectAtIndex:tapgesture.view.tag];
    if ([[dictwebSearchArray valueForKey:@"object_type"]isEqualToString:@"community"]) {
        SearchCommunityProfileView *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchCommunityProfileView"];
        allprofileView.friendProfileDict=dictwebSearchArray;
        [self.navigationController pushViewController:allprofileView animated:YES];
    }
}


#pragma Identify method friend or community
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma Join community details

-(void)joincommunityGroup :(UIButton *)sender{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    NSDictionary *dictGrupResctricatedDict=[webSearchArray objectAtIndex:sender.tag];
        NSString *check_restricted;
        if ([dictGrupResctricatedDict isKindOfClass:[NSDictionary class]]) {
           check_restricted=[NSString stringWithFormat:@"%@",[dictGrupResctricatedDict valueForKey:@"is_restricted"]];
        }
    if(isFiltered){
        NSString *objectGroupId;
        NSDictionary *dictGrup=[_searchResults objectAtIndex:indexPath.row];
        NSString *check_restrictedFilter;
        if ([dictGrupResctricatedDict isKindOfClass:[NSDictionary class]]) {
            check_restrictedFilter=[NSString stringWithFormat:@"%@",[dictGrup valueForKey:@"is_restricted"]];
        }
        NSString *filterGroupname=[dictGrup valueForKey:@"name"];
        objectGroupId=[dictGrup valueForKey:@"object_id"];
        
        UIButton *btn = (UIButton*)sender;
        if ([btn.titleLabel.text isEqualToString:@"Join"])
        {
        if ([check_restrictedFilter isEqualToString:@"1"]) {
                login=[self.storyboard instantiateViewControllerWithIdentifier:@"popGroupViewController"];
                login.owner = self;
                self.definesPresentationContext = YES;
                login.modalPresentationStyle = UIModalPresentationOverFullScreen;
                login.view.backgroundColor=[UIColor clearColor];
                [self presentViewController:login animated:YES completion:^{
                    login.view.backgroundColor =[UIColor colorWithRed:(0.0/255.0f) green:(0.0/255.0f) blue:(0.0/255.0f) alpha:0.5];
                }];
                [[NSUserDefaults standardUserDefaults] setObject:objectGroupId forKey:@"objectid"];
                [[NSUserDefaults standardUserDefaults] setObject:filterGroupname forKey:@"name"];
                [[NSUserDefaults standardUserDefaults] synchronize];
  
            }
            else{
            [self viewCommunityProfile:objectGroupId buttonsender:sender  Groupname:filterGroupname ];
            }
        
        }
        else{
            [self viewCommunityProfileleave:objectGroupId  buttonsender:(id)sender];
        }
    }
    else{
        
        
        dictwebSearchArray=[webSearchArray objectAtIndex:indexPath.row];
        UIButton *btn = (UIButton*)sender;
        if ([btn.titleLabel.text isEqualToString:@"Join"])
        {
           // we have to create pop box for  gender and birthday
            
            if ([check_restricted isEqualToString:@"1"]) {
                login=[self.storyboard instantiateViewControllerWithIdentifier:@"popGroupViewController"];
                login.owner = self;
                self.definesPresentationContext = YES;
                login.modalPresentationStyle = UIModalPresentationOverFullScreen;
                login.view.backgroundColor=[UIColor clearColor];
                [self presentViewController:login animated:YES completion:^{
                    login.view.backgroundColor =[UIColor colorWithRed:(0.0/255.0f) green:(0.0/255.0f) blue:(0.0/255.0f) alpha:0.5];
                    
                }];
                
                [[NSUserDefaults standardUserDefaults] setObject:dictwebSearchArray[@"object_id"] forKey:@"objectid"];
                [[NSUserDefaults standardUserDefaults] setObject:[dictwebSearchArray valueForKey:@"name"] forKey:@"name"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else{
            [self viewCommunityProfile:dictwebSearchArray[@"object_id"]  buttonsender:(id)sender  Groupname:[dictwebSearchArray valueForKey:@"name"]];
            }
 
        }
        else{
            [self viewCommunityProfileleave:dictwebSearchArray[@"object_id"]  buttonsender:(id)sender];
        }
    }
    }
}


-(void)popUpSubmitClicked:(NSString *)dob gender:(NSString*)gender;
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
       
        NSDictionary *dict = @{
                            @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                            @"token" :[[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                            @"date_of_birth":dob,
                            @"gender":gender,
                            };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:updateProfile withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    if (![response isKindOfClass:[NSNull class]]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if([response[@"status"]intValue ] ==1)
                            {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     if (login) {
                                         [self dismissViewControllerAnimated:login completion:nil];
                                     }
                                        [self viewCommunityProfile:[[NSUserDefaults standardUserDefaults] valueForKey:@"objectid"]  buttonsender:[[NSUserDefaults standardUserDefaults] valueForKey:@"buttonsender'"]  Groupname:[[NSUserDefaults standardUserDefaults] valueForKey:@"name"]];
                                                            });
                                
                            }
                            else{
                                NSMutableArray *messageArray=nil;
                                messageArray=[response valueForKey:@"message"];
                                if ([messageArray count]>0) {
                                    UIAlertView *loginAlert=[[UIAlertView alloc]initWithTitle:appTitle message:[messageArray objectAtIndex:0] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] ;
                                    [loginAlert show];
                                }
                            }
                        });
                    }
                }
            } errorBlock:^(NSError *error)
             {
                [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//                 NSLog(@"error is %@",error);
             }];
            
    }
}

#pragma viewFriendProfile

-(void)viewFriendProfile :(NSString*)friend_object FriendId:(NSString*)Id{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id":friend_object
                           };
//    NSLog(@"disct is%@",dict);
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
//        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    NSMutableDictionary *FriendDict=response[@"data"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSDictionary *profileviewDict=[FriendDict valueForKey:@"usersprofile"];
                        NSString *profile_picture=[profileviewDict valueForKey:@"profile_picture"];
                        NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",FriendDict[@"first_name"],FriendDict[@"last_name"]]]];
                        [[NSUserDefaults standardUserDefaults] setObject:nameTxt forKey:@"name"];
                        [[NSUserDefaults standardUserDefaults] setObject:profile_picture forKey:@"profile_picture12"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        FriendsProfileViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                        allprofileView.friendProfileDict=FriendDict;
                        allprofileView.postFriendProfileDict=FriendDict;
                        [self.navigationController pushViewController:allprofileView animated:YES];
//                        [self presentViewController:allprofileView animated:YES completion:nil];
                        
                        // USE PRESEENTED VIEW
                        
                    });
                }
            }
        }
        else{
            
            NSMutableArray  *messageArray=[response valueForKey:@"message"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
            });
        }
        
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
    }
    
}


#pragma viewCommunityJOIN

-(void)viewCommunityProfile :(NSString*)community_object buttonsender:(id)sender Groupname:(NSString *)groupname {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    NSDictionary *dict = @{
                        @"user_id" : [[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                        @"com_id" : community_object,
                           };
    
//    NSLog(@"disct is%@",dict);
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:communitydetails withParameters:dict withHud:YES success:^(id response){
//        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    NSMutableArray  *messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (isFiltered) {
                            UIButton *btn = (UIButton*)sender;
                            [btn setTitle:@"Leave" forState:UIControlStateNormal];
                            [self allcommunityList];
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];

                            
                            NSString *commid=[NSString stringWithFormat:@"%@",community_object];
                            [[NSUserDefaults standardUserDefaults] setObject:commid forKey:@"comid"];
                            [[NSUserDefaults standardUserDefaults]setObject:@"detail" forKey:@"detailgroup"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
    
                           
                    DetailMygroupViewController1 *detailViewsearch=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
                                detailViewsearch.navigationTitle=groupname;
                                detailViewsearch.idUser=[NSString stringWithFormat:@"%@",community_object];
                            // need to send Group name.
                            NSString *groupnameStr=[NSString stringWithFormat:@"%s%@","Group_Joined_",groupname];
                                [TenjinSDK sendEventWithName:groupnameStr];
                                [FBSDKAppEvents logEvent:groupnameStr];
                             [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:groupnameStr]];
                            [self.navigationController pushViewController:detailViewsearch animated:YES];
                        }
                        else{
                            
                            [self allcommunityList];
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            
                            DetailMygroupViewController1 *detailViewsearch=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
                            detailViewsearch.navigationTitle=groupname;
                            detailViewsearch.idUser=[NSString stringWithFormat:@"%@",community_object];
                            NSString *commid=[NSString stringWithFormat:@"%@",community_object];
                            [[NSUserDefaults standardUserDefaults] setObject:commid forKey:@"comid"];
                            [[NSUserDefaults standardUserDefaults]setObject:@"detail" forKey:@"detailgroup"];
                            [[NSUserDefaults standardUserDefaults] synchronize];

                            //need to change Group name
                             NSString *groupnameStr=[NSString stringWithFormat:@"%s%@","Group_Joined_",groupname];
                            [TenjinSDK sendEventWithName:groupnameStr];
                            [FBSDKAppEvents logEvent:groupnameStr];
                        [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:groupnameStr]];
                            [self.navigationController pushViewController:detailViewsearch animated:YES];
                        }
                    });
                }
                else{
                    NSMutableArray  *messageArray=[response valueForKey:@"message"];
                   dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *message = [NSString stringWithFormat:@"%@",[messageArray objectAtIndex:0]];
                       UIAlertView *alt=[[UIAlertView alloc]initWithTitle:appTitle message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                       [alt show];
                  });
                }
 
            }
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
    }
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==0) {
        [self allcommunityList]  ;
    }
}


#pragma leave group
-(void)viewCommunityProfileleave :(NSString*)community_object buttonsender:(id)sender   {

    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict = @{
                               @"user_id" : [[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"com_id" : community_object,
                               };
//        NSLog(@"disct is%@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:leave withParameters:dict withHud:YES success:^(id response){
//            NSLog(@"resposnse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        NSMutableArray  *messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if (isFiltered) {
//                                change in group search problem done
                                UIButton *btn = (UIButton*)sender;
                                [btn setTitle:@"Join" forState:UIControlStateNormal];
//                                [self allcommunityList];
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                                
                            }
                            else{
//                            change in group search problem done
                             UIButton *btn = (UIButton*)sender;
                             [btn setTitle:@"Join" forState:UIControlStateNormal];
//                            [self allcommunityList];
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }

                        });
                    }
                }
            }
            else{
                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                });
            }
            
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
         }];
    }
}



-(void)InvitesFriendMethod {
//    NSLog(@"hello invited");
}

-(void)TickClickBTN:(UIButton*)sender {
    
    sender.selected=!sender.selected;
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [self.tableView convertPoint:buttonOrigin fromView:[sender superview]];
    NSIndexPath *rowIndexPath = [self.tableView indexPathForRowAtPoint:originInTableView];
}


#pragma mark - UISearchControllerDelegate & UISearchResultsDelegate


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
  
    if(searchText.length == 0)
    {
        isFiltered = FALSE;
    }
    else{
        
        isFiltered = true;
//    mo
        
//        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name beginswith[c]%@", searchText];
//        _searchResults = [[[webSearchArray filteredArrayUsingPredicate:resultPredicate]valueForKey:@"name"] mutableCopy];
    }
//    [self.tableView reloadData];
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self serachTextString:searchBar.text];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    dispatch_async(dispatch_get_main_queue(), ^{
        [searchBar resignFirstResponder];
        searchBar.text=@"";
        isFiltered=false;
        [self  allcommunityList];
        
    });
    
    
}


-(void)serachTextString:(NSString*)searchText {
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
         }
    else{
        NSDictionary *dict = @{
                            @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                            @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                            @"searchterm" :searchText,
                               };
//        NSLog(@"dict %@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:groupsearch withParameters:dict withHud:YES success:^(id response){
//            NSLog(@"responmse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            isFiltered=YES;
                            [_searchbar resignFirstResponder];
                            _searchResults=[response valueForKey:@"data"];
                            [self.tableView reloadData];
                            
                        }
                  
                        else{
                            NSMutableArray *messageArray=[[NSMutableArray alloc]init];
                            messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
    }
}


-(void)sendRequestforViewprofile:(NSString*)methodName anddict:(NSDictionary*)dict{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }
    else{
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
//            NSLog(@"responmse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        NSMutableDictionary*    friendsdataDict=[response valueForKey:@"data"];
                        if (friendsdataDict) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if ([methodName isEqualToString:communityDisplay]) {
                                    //                                    ViewMycommunityViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewMycommunityViewController"];
                                    //                                    allprofileView.friendProfileDict=friendsdataDict;
                                    //                                    [self.navigationController pushViewController:allprofileView animated:YES];
                                }
                                
                            });
                        }else{
                            NSMutableArray *messageArray=[[NSMutableArray alloc]init];
                            messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            
                        }
                        
                    }
                }
            }
            
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
    }
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */





- (IBAction)mygroupMethod:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    MyGroupViewController *grouplistView=[self.storyboard instantiateViewControllerWithIdentifier:@"MyGroupViewController"];
    [self.navigationController pushViewController:grouplistView animated:YES];
    }
    
    
}

- (IBAction)submitGenderBMethod:(id)sender {
    
}


@end
