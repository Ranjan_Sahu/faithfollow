//
//  GroupviewPagecontrollerViewController.h
//  wireFrameSplash
//
//  Created by home on 6/24/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface GroupviewPagecontrollerViewController : UIViewController
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

@property (nonatomic, assign) NSUInteger subnotification;
- (IBAction)search_act:(id)sender;

- (IBAction)mycommunity_act:(id)sender;
- (IBAction)freiend_act:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *mycommunity_btn;
@property (weak, nonatomic) IBOutlet UIButton *search_btn;
@property (weak, nonatomic) IBOutlet UIButton *friendrequest_btn;




@property (weak, nonatomic) IBOutlet UIView *bckView1;
@property (weak, nonatomic) IBOutlet UIView *bckVIew2;
@property (weak, nonatomic) IBOutlet UIView *bckview3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topconstraint;
@property(strong,nonatomic)NSString *checkaddsubview;
@property(strong,nonatomic)NSString *selectedPageNO;

// new pop box

@property (weak, nonatomic) IBOutlet UIView *popgenderView;

@end
