//
//  NotificationViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "NotificationViewController.h"
#import "ProfileTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "MHTabBarController.h"
#import "mycommunityViewcontroller.h"
#import "ProfileFriendViewController.h"
#import "Constant1.h"
#import "SeeMoreCommentsViewController.h"
#import "MyGroupViewController.h"
#import "InfluencerProfileController.h"
#import "MNMBottomPullToRefreshManager.h"
#import "StatusViewController.h"


@interface NotificationViewController ()<UITableViewDelegate,UITableViewDataSource,MNMBottomPullToRefreshManagerClient>
{
    NSMutableArray *request_dataArray,*inluencerListArray,*messageArray;
    NSMutableDictionary *friendsdataDict;
    NSDictionary *commment_info;
    BOOL notification_isread;
    int current_page;
    int pagenumber,total_pagenumber;
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
}

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableViewOutlet.dataSource=self;
    _tableViewOutlet.delegate=self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self viewnotificationlist];
    [self influencerMethod];
}


#pragma mark InfluencerMethod

-(void)influencerMethod{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        NSDictionary *dict=nil;
        
        dict = @{
                 @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:influencerList withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            inluencerListArray=[response valueForKey:@"data"];
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
             [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
             
         }];
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return request_dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellidSetting1" forIndexPath:indexPath];
    NSDictionary *dict=[request_dataArray objectAtIndex:indexPath.row];
    if ([dict isKindOfClass:[NSDictionary class]]) {
        NSString *checkInfluencer;
        checkInfluencer=[NSString stringWithFormat:@"%@",[dict valueForKey:@"is_influencer"]];
        
        if ([checkInfluencer isEqualToString:@"0"]) {
            cell.influencerImage.hidden=YES;
        }else{
            cell.influencerImage.hidden=NO;
        }
        
        if ([dict[@"object_type"] isEqualToString:@"joinGroupDays"]) {
            cell.hourLabel.text=@"See Groups!";
            cell.hourLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0];
        }else if ([dict[@"object_type"] isEqualToString:@"postDays"]){
            cell.hourLabel.text = @"Post now!";
            cell.hourLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0];
        }else if ([dict[@"object_type"] isEqualToString:@"praiedDays"]){
            cell.hourLabel.text = @"Learn to Pray!";
            cell.hourLabel.textColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0];
        }else{
            cell.hourLabel.text=dict[@"created_at"];
            cell.hourLabel.textColor = [UIColor colorWithRed:181.0/255.0 green:181.0/255.0 blue:181.0/255.0 alpha:1.0];
        }
        
        NSString *notificationComment;
        notificationComment=[NSString stringWithFormat:@"%@",dict[@"body"]];
//        cell.postedLabel.numberOfLines = 1;
//        cell.postedLabel.minimumScaleFactor = 0.5;
//        cell.postedLabel.adjustsFontSizeToFitWidth = YES;
        cell.postedLabel.text=[Constant1 isStrEmpty:notificationComment];
        
        NSDictionary *user=dict[@"usersprofile"];
        NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,user[@"profile_picture"]];
        [cell.imageViewNotification sd_setImageWithURL:[NSURL URLWithString:str]  placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
        
//        cell.imageViewNotification.layer.borderColor=[UIColor colorWithRed:59.0/255.0f green:147.0/255.0f  blue:192.0/255.0f  alpha:1.0f].CGColor;
//        cell.imageViewNotification.layer.cornerRadius=5.0f;
//        cell.imageViewNotification.layer.borderWidth=1.0f;
//        cell.imageViewNotification.clipsToBounds=YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}



- (void)viewDidLayoutSubviews {
    [pullToRefreshManager_ relocatePullToRefreshView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    dispatch_async(dispatch_get_main_queue(), ^{
      pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:40.0f tableView:_tableViewOutlet withClient:self];
    });
    [pullToRefreshManager_ tableViewScrolled];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate: (BOOL)decelerate
{
    [pullToRefreshManager_ tableViewReleased];
}

- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager {
   
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        if (current_page==total_pagenumber) {
        }
        else
        {
            [self viewnotificationlist:current_page+1];
        }
    }
    
    }


- (UIImage *)imageFromColor:(UIColor *)color {
    
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dict=[request_dataArray objectAtIndex:indexPath.row];
    
    if ([dict[@"subject_type"] isEqualToString:@"post"] ){
        [TenjinSDK sendEventWithName:@"notification_clicked_post"];
        NSString *postID=[NSString stringWithFormat:@"%@",[dict valueForKey:@"post_id"]];
        if ([postID isEqualToString:@"<null>"]||[postID isEqualToString:@""]) {
            
        NSDictionary *dict=@{
                            @"feedScreen":@"0"
                              };
        [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationtoFeed" object:nil userInfo:dict];
        }
        else{
            [self postUpdatedMethod:postID  andislike:@"1"];
            
        }
    }
   
    // Learn to post
    
    if ([dict[@"subject_type"] isEqualToString:@"postDays"] ){
        [TenjinSDK sendEventWithName:@"notification_clicked_post"];
//        [FBSDKAppEvents logEvent:@"notification_clicked_post"];
//        [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"notification_clicked_post"]];
        NSString *postID=[NSString stringWithFormat:@"%@",[dict valueForKey:@"post_id"]];
        if ([postID isEqualToString:@"<null>"]||[postID isEqualToString:@""]) {
//            Status
            NSString *checkInfluencer;
            NSString *loggedinUser=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
            for (int i=0; i<[inluencerListArray count]; i++) {
                NSDictionary *dictinflue=[inluencerListArray objectAtIndex:i];
                if ([dictinflue isKindOfClass:[NSDictionary class]]) {
                    NSString *inlfuencerID=[NSString stringWithFormat:@"%@",[dictinflue objectForKey:@"id"]];
                    if ([loggedinUser isEqualToString:inlfuencerID]) {
                        checkInfluencer=@"1";
                    }
                    else{
                        checkInfluencer=@"0";
                        
                    }
                }
                else{
                  checkInfluencer=@"0";
                }
                
            }
            
            StatusViewController *statusNotification=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
            NSDictionary *dictNoti=@{@"objectID":[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                                     @"subjectID":[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                                     @"influcencerimagae":checkInfluencer,
                                     };
            statusNotification.dictofPost=dictNoti;
            [self.navigationController pushViewController:statusNotification animated:YES];
        }
        else{
            [self postUpdatedMethod:postID  andislike:@"1"];
        }
        
    }
    
     // Learn to Pray
    
    if ([dict[@"subject_type"] isEqualToString:@"praiedDays"] ){
        
        [TenjinSDK sendEventWithName:@"notification_clicked_pray"];
//        [FBSDKAppEvents logEvent:@"notification_clicked_pray"];
//        [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"notification_clicked_pray"]];
        
        NSString *postID=[NSString stringWithFormat:@"%@",[dict valueForKey:@"post_id"]];
        if ([postID isEqualToString:@"<null>"]||[postID isEqualToString:@""]) {
            [[AppDelegate sharedAppDelegate] showInstructionOverlay:@"Pray"];
        }
        else{
            [self postUpdatedMethod:postID  andislike:@"1"];
        }
    }
    
    // See Group
    
    if ([dict[@"subject_type"] isEqualToString:@"joinGroupDays"] ){
        
        [TenjinSDK sendEventWithName:@"notification_clicked_groups"];
//        [FBSDKAppEvents logEvent:@"notification_clicked_groups"];
//        [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"notification_clicked_groups"]];
        
        NSString *post_idGroup=[NSString stringWithFormat:@"%@",[dict valueForKey:@"post_id"]];
        if ([post_idGroup isEqualToString:@"<null>"]||[post_idGroup isEqualToString:@""]) {
            NSDictionary *dict=@{
                                 @"feedScreen":@"3"
                                 };
            [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationtoFeed" object:nil userInfo:dict];
        }
        else{
            [self groupopenmethod:post_idGroup];
        }
        
    }
    if ([dict[@"subject_type"] isEqualToString:@"Follow"] ){
        NSString *postid=[NSString stringWithFormat:@"%@",[dict valueForKey:@"subject_id"]];
        NSString *check_influencer=[NSString stringWithFormat:@"%@",[dict valueForKey:@"is_influencer"]];
        if ([check_influencer isEqualToString:@"0"]) {
            [self  viewFrndProfile :postid];
        }
        else{
            [self followinfluencer:postid influencer:check_influencer];
        }
    }
    if ([dict[@"subject_type"] isEqualToString:@"Unfollow"] ){
        NSString *postid=[NSString stringWithFormat:@"%@",[dict valueForKey:@"subject_id"]];
        NSString *check_influencer=[NSString stringWithFormat:@"%@",[dict valueForKey:@"is_influencer"]];
        if ([check_influencer isEqualToString:@"0"]) {
            [self  viewFrndProfile :postid];
        }
        else
        {
            [self followinfluencer:postid influencer:check_influencer];
        }

    }
    
}

- (void)handleSingleTapGestureName1:(UITapGestureRecognizer *)tapgesture {
 
    NSDictionary *dict=[request_dataArray objectAtIndex:tapgesture.view.tag];
    if ([dict[@"subject_type"] isEqualToString:@"Friend Request"] ) {
        NSString *friendID=[NSString stringWithFormat:@"%@",[dict valueForKey:@"subject_id"]];
        [self viewFrndProfile:friendID];
    }
    if ([dict[@"subject_type"] isEqualToString:@"post"] ){
    }else if ([dict[@"subject_type"] isEqualToString:@"Profile Picture"] ){
        NSString *friendID=[NSString stringWithFormat:@"%@",[dict valueForKey:@"subject_id"]];
        [self viewFrndProfile:friendID];
    }
    
    
}

-(void)followinfluencer:(NSString*)postid_influencer influencer:(NSString*)check_influencer{
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (!appDelegate.isReachable) {
            [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
            return;
        }
        else{
            NSDictionary *dict = @{
                            @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                            @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                            @"friend_id" : postid_influencer,
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    if([response[@"status"]intValue ] ==1){
                   NSMutableDictionary    *influencerdataDict=[response valueForKey:@"data"];
                        if ([influencerdataDict isKindOfClass:[NSMutableDictionary class]]) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                    InfluencerProfileController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                    allprofileView.influencerDict=influencerdataDict;
                    [self.navigationController pushViewController:allprofileView animated:YES];
                            });
                        }
                    }
                    else
                    {
                   NSMutableArray   *messageArray=[response valueForKey:@"message"];
                        if ([messageArray count]>0) {
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            return;
                            
                        }
                        
                    }
                    
                }
            } errorBlock:^(id error)
             {
                 [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
             }];
        }
  
}


-(void) groupopenmethod:(NSString*)postid_Group  {
    MyGroupViewController *detailgroupFeed=[self.storyboard instantiateViewControllerWithIdentifier:@"MyGroupViewController"];
    [self.navigationController pushViewController:detailgroupFeed animated:YES];
}


-(void)viewFrndProfile :(NSString*)friend_id{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : friend_id,
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if([response[@"status"]intValue ] ==1){
                
                friendsdataDict=[response valueForKey:@"data"];
                if (friendsdataDict) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSUserDefaults standardUserDefaults] setObject:@"directProfile" forKey:@"directProfile"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",friendsdataDict[@"first_name"],friendsdataDict[@"last_name"]]]];
                        
                        NSDictionary *dictUser=[friendsdataDict valueForKey:@"usersprofile"];
                        NSString *profile_picture=[dictUser valueForKey:@"profile_picture"];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:nameTxt forKey:@"mainName"];
                        [[NSUserDefaults standardUserDefaults]setObject:profile_picture forKey:@"profile_pictureStr"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        ProfileFriendViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileFriendViewController"];
                        allprofileView.friendProfileDictProfile=friendsdataDict;
                        allprofileView.postFriendProfileDict=friendsdataDict;
                        [self.navigationController pushViewController:allprofileView animated:YES];
                        
                    });
                }
                
            }else{
                NSMutableArray *messageArray=nil;
                messageArray=[response valueForKey:@"message"];
                UIAlertView *at=[[UIAlertView alloc]initWithTitle:@"Pls Notes!" message:[messageArray objectAtIndex:0] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [at show];
            }
            
        }
    } errorBlock:^(id error)
     {
      [self.view makeToast:NoNetwork duration:1.0 position:CSToastPositionCenter];  
     }];
    
}
-(void)postUpdatedMethod:(NSString *)postid andislike:(NSString*)islike_post{
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork duration:1.0 position:CSToastPositionCenter];
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate showhud];
        
    });
    
    NSDictionary *dict = @{
                           @"post_id" :postid,
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethodnotification:wallview withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    NSDictionary *dataofpost=response[@"data"];
                    if (![dataofpost isKindOfClass:[NSNull class]]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableArray *get_attachments=dataofpost[@"get_attachments"];
                        NSString *subjectName=[NSString stringWithFormat:@"%@ %@",dataofpost[@"get_subject_user_details"][@"first_name"],dataofpost[@"get_subject_user_details"][@"last_name"]];
                            
                        NSString *profile_picture=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:dataofpost[@"get_subject_user_details"][@"usersprofile"][@"profile_picture"]]];
                        NSDictionary *objectDict=[dataofpost valueForKey:@"get_object_user_details"];
                        NSString *objectId;
                        if ([objectDict isKindOfClass:[NSDictionary class]]) {
                            objectId=[NSString stringWithFormat:@"%@",[objectDict valueForKey:@"id"]];
                        }
                        else{
                         objectId=@"";
                        }
                        
                        profile_picture=[NSString stringWithFormat:@"%@%@",ImageapiUrl,profile_picture];
                        NSData *person_data=[NSData dataWithContentsOfURL:[NSURL URLWithString:profile_picture]];
                        UIImage *person_image=[UIImage imageWithData:person_data];
                                              
                        NSString *strlike,*checkInfluencer;
                        if ( [dataofpost[@"is_like"] isKindOfClass:[NSNull class]]) {
                            strlike=@"0";
                        }else{
                            strlike=[NSString stringWithFormat:@"%@",dataofpost[@"is_like"]];
                        }
                       
                    if ([dataofpost[@"is_influencer"] isKindOfClass:[NSNull class]]) {
                        checkInfluencer=@"0";
                      }else{
                      checkInfluencer=[NSString stringWithFormat:@"%@",dataofpost[@"is_influencer"]];
                      }
                        if (get_attachments.count==0) {
                            commment_info=@{
                                            @"post_body" :dataofpost[@"content"],
                                            @"post_picture" :person_image,
                                            @"post_personName" :subjectName,
                                            @"post_time" :dataofpost[@"formatted_date"],
                                            @"ref_id" :dataofpost[@"id"],
                                            @"ref_type" :@"post",
                                            @"postimage_array":get_attachments,
                                            @"get_total_praised":dataofpost[@"get_total_praised"],
                                            @"post_type":dataofpost[@"post_type"],
                                            @"is_like":strlike,
                                            @"objectID":objectId,
                                            @"subjectName":subjectName,
                                            @"Influencercheck":checkInfluencer
                                            };
                        }else{
                            NSString *post_image;
                            NSMutableArray*   postimageArray=[[NSMutableArray alloc]init];
                            for (NSDictionary* image_dict in get_attachments){
                                post_image=image_dict[@"file"];
                                [postimageArray addObject:image_dict[@"file"]];
                            }
                            NSString *post_picture=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:post_image]];
                            post_picture=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,post_picture];
                            NSData *dat=[NSData dataWithContentsOfURL:[NSURL URLWithString:post_picture]];
                            UIImage *ima=[UIImage imageWithData:dat];
                            
                            if(ima==nil){
                                ima=[self imageFromColor:[UIColor whiteColor]];
                            }
                            commment_info=@{
                                            @"post_body" :dataofpost[@"content"],
                                            @"post_picture" :person_image,
                                            @"post_personName" :subjectName,
                                            @"post_time" :dataofpost[@"formatted_date"],
                                            @"ref_id" :dataofpost[@"id"],
                                            @"ref_type" :@"post",
                                            @"postimage_array":get_attachments,
                                            @"post_Image":ima,
                                            @"get_total_praised":dataofpost[@"get_total_praised"],
                                            @"post_type":dataofpost[@"post_type"],
                                            @"is_like":strlike,
                                            @"objectID":objectId,
                                            @"subjectName":subjectName,
                                            @"Influencercheck":checkInfluencer
                                            };
                                }
                    [self getallcomments:dataofpost andpostInfo:commment_info];
                            });
                    }
                }
            }
            else{
                messageArray=[response valueForKey:@"message"];
                if ([messageArray count]>0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                        
                    });
                }
               
            }
            
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
    
}



-(void)viewnotificationlist{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"page":@"1"
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:getnotificationlist withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSDictionary *responseDict=[response valueForKey:@"data"];
                        NSString *current_page1=[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"current_page"]];
                        current_page=[current_page1 intValue];
                        NSString *totalPage=[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"last_page"]];
                        total_pagenumber=[totalPage intValue];
                        request_dataArray=[responseDict valueForKey:@"data"];
                        NSLog(@"request_dataArray %ld",[request_dataArray count]);
                        [self.tableViewOutlet reloadData];
                    });
                }
                
            }
            else{
                NSMutableArray *messageArray=nil;
                messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([messageArray count]>0) {
                        [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                    }
                    
                    
                });
            }
            
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
    
}


#pragma loadmore data 

-(void)viewnotificationlist:(NSInteger)pageNumber{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"page":[NSString stringWithFormat:@"%ld",(long)pageNumber]
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:getnotificationlist withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSDictionary *responseDict=[response valueForKey:@"data"];
                        NSString *current_page1=[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"current_page"]];
                        current_page=[current_page1 intValue];
                        NSString *totalPage=[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"last_page"]];
                        total_pagenumber=[totalPage intValue];
                        NSMutableArray *localarray=[responseDict objectForKey:@"data"];
                        [request_dataArray addObjectsFromArray:localarray];
                        NSLog(@"request_dataArray %ld",[request_dataArray count]);
                        [self.tableViewOutlet reloadData];
                        [pullToRefreshManager_ tableViewReloadFinished];

                    });
                }
                
            }
            else{
                NSMutableArray *messageArray=nil;
                messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([messageArray count]>0) {
                        [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                    }
                    
                    
                });
            }
            
        }
    } errorBlock:^(id error)
     {
         //         NSLog(@"error");
         
     }];
    
}


-(void)getallcomments:(NSDictionary*)dict  andpostInfo:(NSDictionary*)postinfo{
    AppDelegate *appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork duration:1.0 position:CSToastPositionCenter];
        return;
    }
   
    NSDictionary *para_dict = @{
                            @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                            @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                            @"ref_id":dict[@"id"],
                            @"ref_type":@"post",
                            };
    
    [[WebServiceHelper sharedInstance] callPostDataWithMethodnotification:commentLists withParameters:para_dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                            [appDelegate hidehud];
                            NSMutableArray *commentsArray=[response valueForKey:@"data"];
                            SeeMoreCommentsViewController *seemoreScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"SeeMoreCommentsViewController"];
                            seemoreScreen.totalComments=commentsArray;
                            seemoreScreen.post_dict=postinfo;
                            seemoreScreen.backViewController=@"NotificationViewController";
                            [self.navigationController pushViewController:seemoreScreen animated:YES];
                        });
                    
                    
                }else{
                    NSMutableArray *messageArray=nil;
                    messageArray=[response valueForKey:@"message"];
                    if ([messageArray count]>0) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            
                        });
  
                    }
                }
            }
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error is %@",error);
     }];
}

-(void)notitficationRead{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSDictionary *para_dict = @{
                        @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                        @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                        @"noti_id":@"yes"
                        };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:setnotificationasread withParameters:para_dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                    });
                    
                }
            }
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error is %@",error);
     }];
    
}
- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}


@end
