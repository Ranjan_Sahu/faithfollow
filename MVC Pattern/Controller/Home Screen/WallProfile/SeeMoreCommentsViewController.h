#import <UIKit/UIKit.h>

@protocol MyFirstControllerDelegate<NSObject>
-(void) FunctionOne: (NSString*) dataOne andArray:(NSMutableArray*)arraycontent;
-(void) btn_pressed: (NSString*) buttonName andtotal_comments:(NSMutableArray*)totalComments  andpost:(NSDictionary*)post  andCellindex:(NSUInteger)cell_index;
-(void)openViewProfile: (NSString*) dataOne andArray:(NSMutableDictionary*)dictcontent;

@end

@interface SeeMoreCommentsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView_outlet;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightofcommentsImageview;
@property(strong,nonatomic)NSMutableArray *totalComments;
@property (weak, nonatomic) IBOutlet UITextField *txtField_comments;
@property(strong,nonatomic)NSDictionary *post_dict;
- (IBAction)btn_cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *view_textfield;
-(IBAction)send:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *Constraint_bottomtxtView;
@property(assign,nonatomic)NSUInteger cellindex;
@property(strong,nonatomic)NSString *countofcomments;
@property(weak,nonatomic)NSString *backViewController;
@property (weak, nonatomic) IBOutlet UILabel *lbltopbar;
@property (nonatomic, weak) id <MyFirstControllerDelegate>delegate1;

@end
