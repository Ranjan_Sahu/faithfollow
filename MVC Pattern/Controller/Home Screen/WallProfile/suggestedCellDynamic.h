//
//  suggestedCellDynamic.h
//  wireFrameSplash
//
//  Created by webwerks on 12/13/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface suggestedCellDynamic : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UITextView *userNameTXT;
@property (weak, nonatomic) IBOutlet UIButton *followBTN;
@property (weak, nonatomic) IBOutlet UIView *sepreterLine;

@end
