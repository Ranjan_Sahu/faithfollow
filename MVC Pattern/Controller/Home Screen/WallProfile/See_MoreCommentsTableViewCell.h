//
//  See_MoreCommentsTableViewCell.h
//  wireFrameSplash
//
//  Created by Vikas on 27/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLabel.h"

@interface See_MoreCommentsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_comments;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_commentperson;
@property (weak, nonatomic) IBOutlet UILabel *lbl_commentPersonName;
@property (weak, nonatomic) IBOutlet UIImageView *influencerimage;
@property(weak,nonatomic)IBOutlet PPLabel *seePplable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentheightConstriantsLBL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topheightConstriantsLBL;



@end
