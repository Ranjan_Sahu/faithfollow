
//
//  popRatefeedViewController.h
//  wireFrameSplash
//
//  Created by home on 9/27/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface popRatefeedViewController : UIViewController
-(IBAction)closeBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *rateView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rateViewHight;
@property (weak, nonatomic) IBOutlet UIButton *couldbtn;
@property (weak, nonatomic) IBOutlet UIButton *loveitbtn;
@property (weak, nonatomic) IBOutlet UILabel *textmessageLBL;
@property (weak, nonatomic) IBOutlet UILabel *howcanLBL;
@property (weak, nonatomic) IBOutlet UIImageView *starImage;

- (IBAction)lovebtnMethod:(id)sender;

- (IBAction)couldbetterbtnMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *commenTXT;
@property (weak, nonatomic) IBOutlet UIButton *submitbnt;

- (IBAction)sumbitMethod:(id)sender;
@end
