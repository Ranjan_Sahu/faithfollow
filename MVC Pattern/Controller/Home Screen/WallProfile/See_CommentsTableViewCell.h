//
//  See_CommentsTableViewCell.h
//  wireFrameSplash
//
//  Created by Vikas on 26/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLabel.h"
@interface See_CommentsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_personName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_postStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_objectname;




@property (weak, nonatomic) IBOutlet UIImageView *imageview_person;
@property (weak, nonatomic) IBOutlet UILabel *lbl_post;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_post;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commetsImage_heightConstraint;
@property (weak, nonatomic) IBOutlet UIView *view_btnmoareimage;
@property (weak, nonatomic) IBOutlet UIButton *btn_moreImages;
@property (weak, nonatomic) IBOutlet UIButton *prasied_count;
@property (weak, nonatomic) IBOutlet UIButton *lblcomment_count;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthofnamelbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topofdatelabel;


@property (weak, nonatomic) IBOutlet UIImageView *influencerImage;

@property (weak, nonatomic) IBOutlet UIButton *praisedAndPray;

@property (weak, nonatomic) IBOutlet UILabel *praisedLBL;

@property (weak, nonatomic) IBOutlet UIImageView *praised_unselected_img;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *praisetextWidthConstraits;
@property(weak,nonatomic)IBOutlet PPLabel *pplable;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewheightConstriants;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHieght;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topPplableHeight;


@end
