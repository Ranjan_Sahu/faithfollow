//
//  See_CommentsTableViewCell.m
//  wireFrameSplash
//
//  Created by Vikas on 26/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "See_CommentsTableViewCell.h"

@implementation See_CommentsTableViewCell

- (void)awakeFromNib {
    // Initialization code
//    _imageview_person.layer.borderColor=[self colorFromHexString:@"009AD9"].CGColor;
//    _imageview_person.layer.borderWidth=1.2f;
//    _imageview_person.layer.cornerRadius=5.0;
//    _imageview_person.clipsToBounds=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
