//
//  lblClass.m
//  wireFrameSplash
//
//  Created by Vikas on 11/06/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "lblClass.h"

@implementation lblClass

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, -15, 0, 0};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}
@end
