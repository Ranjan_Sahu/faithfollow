//
//  FeedBackRateViewController.h
//  wireFrameSplash
//
//  Created by home on 9/28/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedBackRateViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *feedView;
@property (weak, nonatomic) IBOutlet UITextView *testViewtxt;
@property (weak, nonatomic) IBOutlet UIButton *submitbtn;
- (IBAction)sumbitMethod:(id)sender;
- (IBAction)backMethod:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeightConstraints;

@end
