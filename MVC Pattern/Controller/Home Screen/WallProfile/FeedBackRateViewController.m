//
//  FeedBackRateViewController.m
//  wireFrameSplash
//
//  Created by home on 9/28/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]


#import "FeedBackRateViewController.h"

@interface FeedBackRateViewController ()

@end
UITextView *testViewtxt;
@implementation FeedBackRateViewController
@synthesize feedView,submitbtn,testViewtxt;
- (void)viewDidLoad {
    [super viewDidLoad];
    feedView.layer.borderColor = [UIColor grayColor].CGColor;
    feedView.layer.borderWidth = 1.0f;
    [feedView.layer setMasksToBounds:YES];
    
    self.submitbtn.layer.borderWidth = 1.0f;
    self.submitbtn.layer.borderWidth = 1.0f;
    self.submitbtn.layer.borderColor = UIColorFromRGB(0x95AECC).CGColor;
    self.submitbtn.layer.borderColor = UIColorFromRGB(0x95AECC).CGColor;
    
    self.submitbtn.layer.cornerRadius = 3.0f;
    self.submitbtn.layer.cornerRadius = 3.0f;
    
    
    [[self.testViewtxt layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[self.testViewtxt layer] setBorderWidth:1.0];
    

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{    [super viewWillDisappear:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Validation
- (NSString *)validateFields {
    NSString *errorMessage ;
    NSString *strTextview = [self.testViewtxt.text removeWhiteSpaces];
   
    if ([strTextview length] == 0) {
        errorMessage =FeedbackStr;
        return errorMessage;
    }
       return 0;
}


- (IBAction)sumbitMethod:(id)sender {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
       
        NSString *errorMessage = [self validateFields];
        if (errorMessage) {
            [[[UIAlertView alloc] initWithTitle:appTitle message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
            return;
        }else{
            NSDictionary *dict = @{
//                                   @"email" :self.emialAddressTXT.text,
//                                   @"password" : self.passwordTXT.text,
//                                   @"device_token":deviceToken,
//                                   @"device_type":@"IOS",
//                                   @"advertise_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"adID"]
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"feedback" withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    if (![response isKindOfClass:[NSNull class]]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if([response[@"status"]intValue ] ==1)
                            {

                                
                                
                            }
                            else{
                                NSMutableArray *messageArray=nil;
                                messageArray=[response valueForKey:@"message"];
                                if ([messageArray count]>0) {
                                    UIAlertView *loginAlert=[[UIAlertView alloc]initWithTitle:appTitle message:[messageArray objectAtIndex:0] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] ;
                                    [loginAlert show];
                                }
                            }
                        });
                    }
                }
            } errorBlock:^(NSError *error)
             {
                 NSLog(@"error is %@",error);
             }];
        }
        
    }
    



}

- (IBAction)backMethod:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
