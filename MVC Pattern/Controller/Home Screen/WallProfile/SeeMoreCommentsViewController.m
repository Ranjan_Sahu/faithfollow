//
//  SeeMoreCommentsViewController.m
//  wireFrameSplash
//
//  Created by Vikas on 26/05/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "SeeMoreCommentsViewController.h"
#import "UIImageView+WebCache.h"
#import "See_CommentsTableViewCell.h"
#import "See_MoreCommentsTableViewCell.h"
#import "AppDelegate.h"
#import "MWPhotoBrowser.h"
#import "WebServiceHelper.h"
#import "Webservice_Constant.h"
#import "UIView+Toast.h"
#import "FeedViewController.h"
#import "FeedTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CustomCommentView.h"
#import "FriendsProfileViewController.h"
#import "InfluencerProfileController.h"


@interface SeeMoreCommentsViewController ()<UITextFieldDelegate,MWPhotoBrowserDelegate>{
    NSMutableArray *arrWallFeedArray;
    CGFloat height_header;
    UITextField *toolbar_txtfiled;
    BOOL keyboard;
    NSString *text_filedString;
    NSMutableArray *comments_arr;
    UIBarButtonItem *barButtonItem,*barButtonItem1 ;
    UIButton *barButton,*barButton1;
    UIImage *post_image;
    NSString *count;
    int count_val;
    NSMutableArray *get_latest_commnets_arr;
    NSMutableDictionary *getotaldict,*imageHeightDict;
    MWPhotoBrowser *browser;
    NSMutableArray *photos;
    int seemorebackgournd;
    NSDataDetector *detector;
    NSArray *matches;
    NSString *imageHeightValue,*imageWidthValue;
    
}

@end

@implementation SeeMoreCommentsViewController
#define  kCellName @"See_CommentsTableViewCell"
#define  kCellName2 @"See_MoreCommentsTableViewCell"

- (void)viewDidLoad {
    [super viewDidLoad];
   

    imageHeightValue = nil;
    imageWidthValue = nil;
    // Do any additional setup after loading the view.
    [self.tableView_outlet   registerNib:[UINib nibWithNibName:kCellName bundle:nil] forCellReuseIdentifier:kCellName];
    [self.tableView_outlet   registerNib:[UINib nibWithNibName:kCellName2 bundle:nil] forCellReuseIdentifier:kCellName2];
     comments_arr=[[NSMutableArray alloc]init];
    [AppDelegate sharedAppDelegate].seeMorecommnetNotnull=NO;
    if([AppDelegate sharedAppDelegate].isDeeplinkPost)
    {
        [self commentsViewlink];
        _lbltopbar.text=@"Comments";
        _txtField_comments.delegate=self;
        [AppDelegate sharedAppDelegate].isDeeplinkPost =NO;
    }
    else{
        arrWallFeedArray= [@[@"For the past 33 years, I have looked in the mirror every morning and asked myself: 'If today were the last day of my life, would I want to do what I am about to do today?' And whenever the answer has been 'No' for too many days in a row, I know I need to change something. -Steve Jobs,For the past 33 years, I have looked in the mirror every morning and asked myself: 'If today were the last day of my life, would I want to do what I am about to do today?' And whenever the answer has been 'No' for too many days in a row, I know I need to change something. -Steve Jobs,For the past 33 years, I have looked in the mirror every morning and asked myself: 'If today were the last day of my life, would I want to do what I am about to do today?' And whenever the answer has been 'No' for too many days in a row, I know I need to change something. -Steve Jobs",
                         @"Be a yardstick of quality. Some people aren't used to an environment where excellence is expected. - Steve Jobs",
                         @"Innovation distinguishes between a leader and a follower. -Steve Jobs"] mutableCopy];
        
       
        [comments_arr addObject:_post_dict];
        
        NSMutableDictionary *getDictValue=[_post_dict objectForKey:@"get_total_praised"];
        if ([getDictValue isKindOfClass:[NSNull class]]) {
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"count"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else{
            NSString *countcheck=[getDictValue objectForKey:@"count"];
            [[NSUserDefaults standardUserDefaults] setObject:countcheck forKey:@"count"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
        NSString *likebackup=[NSString stringWithFormat:@"%@",[_post_dict objectForKey:@"is_like"]];
        [[NSUserDefaults standardUserDefaults] setObject:likebackup forKey:@"likecheck"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [comments_arr addObjectsFromArray:_totalComments];
        if (_post_dict[@"post_Image"]!=nil) {
            post_image=_post_dict[@"post_Image"];
        }
        _lbltopbar.text=@"Comments";
        _txtField_comments.delegate=self;

          }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (getVar.checkDisapper) {
        [[AppDelegate sharedAppDelegate] addsView];
//        getVar.checkDisapper=NO;
    }

}
#pragma mark AppEnterInForegroundSeemoreMethod

-(void)appEnterInForegroundSeemore{
    seemorebackgournd=seemorebackgournd+1;
    if (seemorebackgournd%3==0) {
    [[NSUserDefaults standardUserDefaults]setObject:@"Yes" forKey:@"SeeMoreComment"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    seemorebackgournd=0;
    }
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterInForegroundSeemore) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myNotificationMethod:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myNotificationMethod2:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    [[AppDelegate sharedAppDelegate] hideaddView];

    
    
    
}
#pragma mark CommentsViewlinkMethod

-(void)commentsViewlink{
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"post_id":[AppDelegate sharedAppDelegate].ref_idData,
                           };
    
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:singleDetailedPost withParameters:dict withHud:YES success:^(id response){
        NSMutableArray *messageArray;
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    NSMutableDictionary *responseData=[response objectForKey:@"data"];
                    NSMutableArray *arry=[responseData objectForKey:@"data"];
                    NSMutableDictionary *DictResponse=[arry objectAtIndex:0];
                    NSMutableDictionary *get_object_user_details=[DictResponse objectForKey:@"get_object_user_details"];
                     NSMutableDictionary *get_subject_user_details=[DictResponse objectForKey:@"get_subject_user_details"];
                    NSString *namePerson;
                    UIImage *imageProfile ;
                    if ([get_object_user_details isKindOfClass:[NSMutableDictionary class]] || [get_subject_user_details isKindOfClass:[NSMutableDictionary class]] ) {
                        namePerson=[get_subject_user_details objectForKey:@"first_name"];
                        NSString *content=[DictResponse valueForKey:@"content"];
                        NSString *formatdata=[DictResponse valueForKey:@"formatted_date"];
                        NSString *object_id=[NSString stringWithFormat:@"%@",[DictResponse valueForKey:@"object_id"]];
                        NSString *stringPath=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[get_subject_user_details valueForKeyPath:@"usersprofile.profile_picture"]];
                        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:stringPath]];
                        imageProfile = [[UIImage alloc] initWithData:data] ;
                        NSArray *getImageArray=[DictResponse objectForKey:@"get_attachments"];
                        if ([getImageArray  isKindOfClass:[NSArray class]]) {
                            if ([getImageArray count]>0) {
                                NSDictionary *post_imgData=[getImageArray objectAtIndex:0];
                                NSString *stringPath=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,[post_imgData valueForKeyPath:@"file"]];
                                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:stringPath]];
                                if (data.length>0) {
                                    imageProfile = [[UIImage alloc] initWithData:data] ;
                                    post_image=imageProfile;
                                }
                                else{
                                    imageProfile=[[UIImage alloc]init];
                                }
                            }
                            else{
                                post_image=nil;
                               getImageArray=@[];
                            }
                        }
                        
                        if (!imageProfile) {
                            imageProfile = [[UIImage alloc]init];
                        }
                        
            dispatch_async(dispatch_get_main_queue(), ^{
            NSString *refid=[NSString stringWithFormat:@"%@",[AppDelegate sharedAppDelegate].ref_idData];
                
            NSString *namePerson1=[self isStrEmpty:namePerson];
            NSString *content1=[self isStrEmpty:content];
            NSString *formatdata1=[self isStrEmpty:formatdata];
            NSString *object_id1=[self isStrEmpty:object_id];
            NSString *refid1=[self isStrEmpty:refid];
                           NSDictionary *post_dictData = @{
                                                            @"objectname" : namePerson1,
                                                            @"post_personName":namePerson1,
                                                            @"post_body" :content1,
                                                            @"post_time" :formatdata1 ,
                                                            @"post_Image" :@"1",
                                                            @"objectID" :object_id1,
                                                            @"post_picture" :imageProfile,
                                                            @"postimage_array":getImageArray,
                                                            @"ref_type":@"post",
                                                            @"ref_id":refid1,
//                                                            @"Influencercheck":[AppDelegate sharedAppDelegate].ref_idData,
                                                            
                                                            };
                            NSMutableArray *latestcomment=[DictResponse objectForKey:@"latest_comments"];
                            [comments_arr addObject:post_dictData];
                            _post_dict=post_dictData;
                            [comments_arr addObjectsFromArray:latestcomment];
                            [self.tableView_outlet reloadData];
                        });
                    }
                    
                }else{
                    messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([Constant1 validateArray:messageArray]) {
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        }
                        
                    });
                }
                
            }
            
        }
        
    } errorBlock:^(id error)
     {
         //         NSLog(@"error is %@",error);
     }];
    
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}


#pragma mark - To Check String is Empty
-(NSString *)isStrEmpty:(NSString *) str {
    
    if([str isKindOfClass:[NSNumber class]])
    {
        return str;
    }
    if([str isKindOfClass:[NSNull class]] || str==nil)
    {
        return @"";
    }
    if([str length] == 0) {
        
        return @"";
    }
    if(![[str stringByTrimmingCharactersInSet:[NSCharacterSet
                                               whitespaceAndNewlineCharacterSet]] length]) {
        return @"";
    }
    if([str isEqualToString:@"(null)"])
    {
        return @"";
    }
    if([str isEqualToString:@"<null>"])
    {
        return @"";
    }
    return str;
}




#pragma mark tableview delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [comments_arr count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row==0) {
        See_CommentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName forIndexPath:indexPath];
        NSMutableDictionary *dict=[comments_arr objectAtIndex:indexPath.row];
        NSString *postlike=dict[@"is_like"];
        NSString *postType1=dict[@"post_type"];
        int post_val=[postType1 intValue];
        int postlike1=[postlike intValue];
        cell.praisedLBL.tag=indexPath.row+100;
        cell.pplable.delegate=(id)self;
        
        if ( ![dict[@"get_total_praised"] isKindOfClass:[NSNull class]]) {
            if (post_val==1 || post_val==3) {
                if (postlike1==1) {
                    cell.praisedLBL.text=@"You Praised This";
                    cell.praised_unselected_img.image=[UIImage imageNamed:@"praised_selected"];
                    cell.praisedLBL.textColor=[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f blue:245.0/255.0f alpha:1.0];
                }else{
                    cell.praisedLBL.text=@"Praise This";
                    cell.praised_unselected_img.image=[UIImage imageNamed:@"praised_unselected"];
                    cell.praisedLBL.textColor=[UIColor colorWithRed:209.0/255.0f green:209.0/255.0f blue:209.0/255.0f alpha:1.0];
                }
            }else{
                if (postlike1==1) {
                    cell.praisedLBL.text=@"You Prayed";
                    cell.praised_unselected_img.image=[UIImage imageNamed:@"praised_selected"];
                    cell.praisedLBL.textColor=[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f blue:245.0/255.0f alpha:1.0];
                }else{
                    cell.praisedLBL.text=@"Pray for This";
                    cell.praised_unselected_img.image=[UIImage imageNamed:@"praised_unselected"];
                    cell.praisedLBL.textColor=[UIColor colorWithRed:209.0/255.0f green:209.0/255.0f blue:209.0/255.0f alpha:1.0];
                }
            }
        }
        else{
            if (post_val==1 || post_val==3) {
                if (postlike1==1) {
                    cell.praisedLBL.text=@"You Praised This";
                }else{
                    cell.praisedLBL.text=@"Praise This";
                }
            }
            else{
                if (postlike1==1) {
                    cell.praisedLBL.text=@"You Prayed";
                }else{
                    cell.praisedLBL.text=@"Pray for This";
                }
            }
        }
        
        cell.praisedAndPray.tag = indexPath.row;
        [cell.praisedAndPray addTarget:self action:@selector(praiseAndpray:) forControlEvents:UIControlEventTouchUpInside];
        [self setUpCell:cell atIndexPath:indexPath];
        return cell;
    }else{
        See_MoreCommentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName2 forIndexPath:indexPath];
        [self setUpCell2:cell atIndexPath:indexPath];
        return cell;
    }
    
}

-(float)calculateImageHeight:(float)boxWidth imageHeight:(float)imageHeight1 imageWidth:(float)imagewidth1{
    CGFloat screenHeight=[UIScreen mainScreen].bounds.size.height;
    CGFloat preferImageHeight=screenHeight-250;
    CGFloat widthView1,widthView;
    if (imagewidth1>imageHeight1) {
        // landscape
        widthView1=[Constant1 checkimagehieght];
        widthView=widthView1+30;
    }
    else{
        // portraits
        widthView=preferImageHeight;
    }
    return widthView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewAutomaticDimension;
//    if (indexPath.row==0) {
//        NSDictionary *dict=[comments_arr objectAtIndex:indexPath.row];
//        NSString *postString=dict[@"post_body"];
//        UIFont *font = semibold;
//        CGRect rect = [postString boundingRectWithSize:CGSizeMake(self.view.frame.size.width, MAXFLOAT)
//                                         options:NSStringDrawingUsesLineFragmentOrigin
//                                      attributes:@{NSFontAttributeName : font}
//                                         context:nil];
//        float imgHeight=0;
////        CGFloat widthView1=0;
//        if (post_image!=nil ){
//            if ([imageHeightValue floatValue] >0 && [imageWidthValue floatValue] >0) {
//
////                float temp = (float)[imageHeightValue floatValue] / (float)[imageWidthValue floatValue];
////                imgHeight = (float)[imageHeightValue floatValue] * temp;
//                imgHeight=[self calculateImageHeight:tableView.frame.size.width imageHeight:[imageHeightValue floatValue] imageWidth:[imageWidthValue floatValue]]+10;
//            }
//            }
//        return imgHeight+rect.size.height+183;
//    }else{
        
//        NSString *text=[NSString stringWithFormat:@"%@",[comments_arr objectAtIndex:indexPath.row][@"body"]];
//        UIFont *font = font_roman_14;
//        CGRect rect = [text boundingRectWithSize:CGSizeMake(self.view.frame.size.width-70, MAXFLOAT)
//                                         options:NSStringDrawingUsesLineFragmentOrigin
//                                      attributes:@{NSFontAttributeName : font}
//                                         context:nil];
//
//        NSString *text1=[NSString stringWithFormat:@"%@",[comments_arr objectAtIndex:indexPath.row][@"created_at"]];
//        UIFont *font1 = font_roman_12;
//        CGRect rect1 = [text1 boundingRectWithSize:CGSizeMake(self.view.frame.size.width-70, MAXFLOAT)
//                                         options:NSStringDrawingUsesLineFragmentOrigin
//                                      attributes:@{NSFontAttributeName : font1}
//                                         context:nil];
//
//        NSString *text2=[NSString stringWithFormat:@"%@ %@",[comments_arr objectAtIndex:indexPath.row][@"get_object_user_details"][@"first_name"],[comments_arr objectAtIndex:indexPath.row][@"get_object_user_details"][@"last_name"]];
//        UIFont *font2 = font_roman_12;
//        CGRect rect2 = [text2 boundingRectWithSize:CGSizeMake(self.view.frame.size.width-70, MAXFLOAT)
//                                         options:NSStringDrawingUsesLineFragmentOrigin
//                                      attributes:@{NSFontAttributeName : font2}
//                                         context:nil];

//           if(rect.size.height >20)
//          {
//           return rect.size.height+40;
//          }
//        else
//        {
//            return rect.size.height+60;
//        }
//        return rect.size.height + rect1.size.height + rect2.size.height+25;
//    }
    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsUpdateConstraints];
    [sizingCell updateConstraintsIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}
- (void)setUpCell:(See_CommentsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict=[comments_arr objectAtIndex:indexPath.row];
    NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,dict[@"post_picture"]];
    NSURL *imageURL = [NSURL URLWithString:str];
   [cell.imageview_person sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
    
    // influcenser checking for image
        NSString *influncerImage=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Influencercheck"]];
        if ([influncerImage isEqualToString:@"2"]) {
            cell.influencerImage.hidden=NO;
        }else{
            cell.influencerImage.hidden=YES;
        }
    NSString *userid=[NSString stringWithFormat:@"%@",dict[@"objectID"]];
    NSString *loguserId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
    if ([userid isEqualToString:loguserId]) {
        cell.lbl_personName.text=@"You";
    }else{
        cell.lbl_personName.text=dict[@"post_personName"];
    }
    
    cell.lbl_date.text=dict[@"post_time"];
    cell.lbl_date.numberOfLines=0;
    
    // pplabel implementation here
    NSError *error = nil;
    cell.pplable.text=dict[@"post_body"];
    
    detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
    matches = [detector matchesInString:cell.pplable.text options:0 range:NSMakeRange(0, cell.pplable.text.length)];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [cell.pplable addGestureRecognizer:singleTap];
    [self highlightLinksWithIndex:NSNotFound custum:cell.pplable];
    
    cell.view_btnmoareimage.layer.borderWidth=0.6f;
    cell.view_btnmoareimage.layer.cornerRadius= cell.view_btnmoareimage.frame.size.width/2;
    cell.view_btnmoareimage.layer.borderColor=[UIColor blackColor].CGColor;
    cell.btn_moreImages.tag=indexPath.row;
    [cell.btn_moreImages addTarget:self action:@selector(btn_moreImages:) forControlEvents:UIControlEventTouchUpInside];
    cell.imageview_post.tag=indexPath.row;
    
    // Dynamic image
    CGFloat widthView1,widthView;
    // see more Group Comment and height for image manage
    NSMutableArray *get_attachmentsArray=dict[@"postimage_array"];
    NSString  *post_picture;
     NSURL *imageURLPost;
    if ([get_attachmentsArray count]>0) {
        NSDictionary *getUrlDict =[get_attachmentsArray objectAtIndex:0];
        post_picture=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,getUrlDict[@"file"]];
         imageURLPost= [NSURL URLWithString:post_picture];
    }
    
    [cell.imageview_post sd_setImageWithURL:imageURLPost completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL, NSString *imageTag) {
        NSString *key = [[SDWebImageManager sharedManager] cacheKeyForURL:imageURLPost];
        UIImage *lastPreviousCachedImage1 = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:key];
        if (image != nil) {
            if(lastPreviousCachedImage1!=nil && [AppDelegate sharedAppDelegate].seeMorecommnetNotnull==NO)
            {
                [AppDelegate sharedAppDelegate].seeMorecommnetNotnull=YES;
                if (dict!= nil && [dict count]>0) {
                    NSLog(@"imageUrl %@",imageURLPost);
                    imageHeightValue=[NSString stringWithFormat:@"%f",image.size.height];
                    imageWidthValue=[NSString stringWithFormat:@"%f",image.size.width];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView_outlet reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:NO];
                    });
                }
                else{
                    //    skiped image already added.
                }
            }
        }
        
    }];
    
    NSString *key = [[SDWebImageManager sharedManager] cacheKeyForURL:imageURLPost];
    UIImage *lastPreviousCachedImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:key];
    if(lastPreviousCachedImage && ![dict isKindOfClass:[NSNull class]]){
        widthView1 = [self calculateImageHeight:self.tableView_outlet.frame.size.width imageHeight:lastPreviousCachedImage.size.height imageWidth:lastPreviousCachedImage.size.width];
    }else{
        widthView=[Constant1 checkimagehieght];
        widthView1=285;
    }
    
    // end dynamic
    cell.imageview_post.userInteractionEnabled=YES;
    UITapGestureRecognizer *imagegallery_open = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moreImages:)];
    [cell.imageview_post addGestureRecognizer:imagegallery_open];
    NSMutableArray *get_attachments=dict[@"postimage_array"];
    if ([get_attachments count]<=1) {
        cell.view_btnmoareimage.hidden=YES;
    }else  if ([get_attachments count]>1){
        NSString *countofimage=[NSString stringWithFormat:@"+%lu",[get_attachments count]-1];
        [cell.btn_moreImages setTitle:countofimage forState:UIControlStateNormal];
        cell.view_btnmoareimage.hidden=NO;
    }
    
    if(comments_arr.count==1){
        [cell.lblcomment_count setTitle:@"0 Comment" forState:UIControlStateNormal];
    }else{
        NSString *comment_count=[NSString stringWithFormat:@"%lu Comment",[comments_arr count]-1];
        [cell.lblcomment_count setTitle:comment_count  forState:UIControlStateNormal];
    }
    NSString *postType1=dict[@"post_type"];
    int post_val=[postType1 intValue];
    if (post_val==1) {
        cell.lbl_postStatus.text=@"";
        cell.lbl_postStatus.text=@"wrote on ";
    }else{
    NSString *defaultinspiration=[NSString stringWithFormat:@"%@",[dict valueForKey:@"objectID"]];
        if ([defaultinspiration isEqualToString:@"1"]) {
           cell.lbl_postStatus.text=@"";
        }else{
             cell.lbl_postStatus.text=@"Requested A Prayer";
        }
    }
    
    if ([dict[@"objectname"]isEqualToString:@""]) {
        cell.lbl_objectname.text=@"";
    }else{
        NSString *userid=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
            NSString *objectID=[NSString stringWithFormat:@"%@",dict[@"objectID"]];
        
        if ([objectID isEqualToString:userid]) {
            cell.lbl_objectname.text=@"";
        }else{
            cell.lbl_objectname.text=dict[@"objectname"];
        }
    }
    
    if ( [dict[@"get_total_praised"] isKindOfClass:[NSNull class]]) {
        [cell.prasied_count setTitle:@"No Praises yet" forState:UIControlStateNormal];
        if (post_val==1 || post_val==3) {
            [cell.prasied_count setTitle:@"No Praises yet" forState:UIControlStateNormal];
        }else{
            [cell.prasied_count setTitle:@"No Prayers yet" forState:UIControlStateNormal];
        }
    }else{
        NSDictionary*  get_total_praised=dict[@"get_total_praised"];
        NSString *like=[NSString stringWithFormat:@"%@",dict[@"is_like"]];
        int like_count = [like intValue];
        if (like_count==1) {
            NSString *like1=[NSString stringWithFormat:@"%@",get_total_praised[@"count"]];
            int main_like= [like1 intValue];
            NSString *total_count;
            if (post_val==1 || post_val==3 ) {
                
                if (main_like>=3) {
                 total_count=[NSString stringWithFormat:@"You and %d others Praised This",main_like-1];
                }else{
                 total_count=[NSString stringWithFormat:@"You and %d other Praised This",main_like-1];
                }
                if (main_like==1) {
                    total_count=[NSString stringWithFormat:@"You Praised This"];
                }
                [cell.prasied_count setTitle:total_count forState:UIControlStateNormal];
                
            }else{
               
                if (main_like>=3) {
                    total_count=[NSString stringWithFormat:@"You and %d others Prayed for This",main_like-1];
                }else{
                     total_count=[NSString stringWithFormat:@"You and %d other Prayed for This",main_like-1];
                }
                if (main_like==1) {
                    total_count=[NSString stringWithFormat:@"You Prayed for This"];
                }
                [cell.prasied_count setTitle:total_count forState:UIControlStateNormal];
            }
        }else{
            if (post_val==1 || post_val==3) {
                NSString *total_count=[NSString stringWithFormat:@"%@ Praised This",[Constant1 getStringObject:get_total_praised[@"count"]]];
                [cell.prasied_count setTitle:total_count forState:UIControlStateNormal];
            }else{
                NSString *total_count=[NSString stringWithFormat:@"%@ Prayed for This",[Constant1 getStringObject:get_total_praised[@"count"]]];
                [cell.prasied_count setTitle:total_count forState:UIControlStateNormal];
            }
            
        }
    }
    
    if (post_image!=nil) {
        cell.imageview_post.image=post_image;
        cell.commetsImage_heightConstraint.constant=widthView1;
    }
    [self.view layoutIfNeeded];
    [self.view setNeedsUpdateConstraints];

}


#pragma mark praiseAndprayMethod

-(void)praiseAndpray:(UIButton*)sender
{
    NSMutableDictionary *dictData=[[comments_arr objectAtIndex:sender.tag]mutableCopy];
    NSString *subjectName;
    if ([subjectName isEqualToString:@"You"]) {
      subjectName=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"subjectName"]];
        
    }
    else{
        subjectName=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"post_personName"]];
    }
    UILabel *LBl=(UILabel*)[[sender superview] viewWithTag:sender.tag+100];
    if (LBl) {
    if ([LBl.text isEqualToString:@"Praise This"]||[LBl.text isEqualToString:@"Pray for This"]){
        NSDictionary *dict = @{
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                               @"object_id":[dictData valueForKey:@"ref_id"],
                               @"object_type":@"post",
                               };
        [self liked1:dict andmethodName:wall_like andcell_index:[sender tag] like:@"wall_like" subjecNamecheck:subjectName ];
            }
    else{
        
        NSDictionary *dict = @{
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                               @"object_id":[dictData valueForKey:@"ref_id"],
                               @"object_type":@"post",
                               };
        
        [self liked1:dict andmethodName:wall_unlike andcell_index:[sender tag] like:@"wall_unlike" subjecNamecheck:subjectName];
        
    }
    }

}

#pragma liked1Method

-(void)liked1:(NSDictionary*)dict andmethodName:(NSString*)methodName andcell_index:(NSUInteger)cell_index like:(NSString *)wallcheck subjecNamecheck:(NSString *)subjectName{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
        NSMutableDictionary *DictValue=[[comments_arr objectAtIndex:cell_index] mutableCopy];
        // start mix panel
        
        NSString  *postcheck=[NSString stringWithFormat:@"%@",[DictValue objectForKey:@"post_type"]];
        int i,j,k;
      NSString *checkContentType,*contentID;
        contentID=[NSString stringWithFormat:@"%@",[dict objectForKey:@"object_id"]];
        NSString *emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
        
        if ([emailID isEqualToString:@""]||[emailID isEqualToString:@"(null)"]) {
            emailID=@"";
        }
        else{
            emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
        }
        if ([wallcheck isEqualToString:@"wall_like"]) {
            
            if ([postcheck isEqualToString:@"2"]) {
                checkContentType=@"Prayer Request";
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Prayers" by:@1];
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@1];
                
            }
            if ([postcheck isEqualToString:@"3"]) {
                checkContentType=@"Update";
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@1];
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@1];
                
                
                    }
            if ([postcheck isEqualToString:@"1"]) {
                checkContentType=@"Update";
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@1];
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@1];
            }
            NSString *dataLast=[Constant1 createTimeStamp];
            NSString *contenID=[NSString stringWithFormat:@"%@",[dict objectForKey:@"object_id"]];
            
          
             [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last like": dataLast}];
            [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Content ID":contenID,@"Posted by":emailID,@"Content type":checkContentType,@"Date of last like":dataLast}];
            
            [[AppDelegate sharedAppDelegate].mxPanel track:@"Like"
                                                properties:@{@"Content ID":contenID,@"Posted by":subjectName,@"Content type":checkContentType,@"Date of last like":dataLast}
             ];
            
        }
        else{
            NSString *dataLast=[Constant1 createTimeStamp];
            NSString *contenID=[NSString stringWithFormat:@"%@",[dict objectForKey:@"object_id"]];
            
            if ([postcheck isEqualToString:@"2"]) {
                checkContentType=@"Prayer Request";
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Prayers" by:@-1];
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@-1];
                
            }
            if ([postcheck isEqualToString:@"3"]) {
                checkContentType=@"Update";
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@-1];
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@-1];
            }
            if ([postcheck isEqualToString:@"1"]) {
                checkContentType=@"Update";
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@-1];
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@-1];
            }
           
            [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Content ID":contenID,@"Posted by":subjectName,@"Content type":checkContentType,@"Date of last like":dataLast}];
            
        }
        
        
//    end mix panel
        
        NSString *status=[NSString stringWithFormat:@"%@",[response objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([status intValue]==1) {
                if ([DictValue isKindOfClass:[NSDictionary class]]) {
                    NSString *postlike=[DictValue objectForKey:@"is_like"];
                    int postlikecheck=[postlike intValue];
                    
                    NSMutableArray *Arraylikecount=[response objectForKey:@"like_count"];
                    NSMutableDictionary *coountDict=[Arraylikecount objectAtIndex:0];
                    NSString *countValue=[NSString stringWithFormat:@"%@",[coountDict objectForKey:@"count"]];

                    if (postlikecheck==1) {
                        NSMutableDictionary *getDict=[DictValue objectForKey:@"get_total_praised"];
                        if ([getDict isKindOfClass:[NSNull class]]) {
                            getDict=[[NSMutableDictionary alloc]init];
                            [getDict setObject:countValue forKey:@"count"];
                            [DictValue setObject:getDict forKey:@"get_total_praised"];
                        }
                        else{
                            [getDict setObject:countValue forKey:@"count"];
                            [DictValue setObject:getDict forKey:@"get_total_praised"];
                        }
                       
                        [DictValue setObject:@"0" forKey:@"is_like"];
                        NSString *likebackup=[NSString stringWithFormat:@"%@",[DictValue objectForKey:@"is_like"]];
                        [[NSUserDefaults standardUserDefaults] setObject:likebackup forKey:@"likecheck"];
                         [[NSUserDefaults standardUserDefaults] setObject:countValue forKey:@"countValue"];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
  
                    }
                    else{
                                               
                    NSMutableDictionary *getDict=[DictValue objectForKey:@"get_total_praised"];
                        if ([getDict isKindOfClass:[NSNull class]]) {
                            getDict=[[NSMutableDictionary alloc]init];
                            [getDict setObject:countValue forKey:@"count"];
                            [DictValue setObject:getDict forKey:@"get_total_praised"];
                            
                        }
                        else{
                            [getDict setObject:countValue forKey:@"count"];
                            [DictValue setObject:getDict forKey:@"get_total_praised"];
                        }
                        
                       
                        [DictValue setObject:@"1" forKey:@"is_like"];
                        NSString *likebackup=[NSString stringWithFormat:@"%@",[DictValue objectForKey:@"is_like"]];
                        [[NSUserDefaults standardUserDefaults] setObject:likebackup forKey:@"likecheck"];
                         [[NSUserDefaults standardUserDefaults] setObject:countValue forKey:@"countValue"];
                        [[NSUserDefaults standardUserDefaults] synchronize];

                    }
                    
                }
                [comments_arr replaceObjectAtIndex:cell_index withObject:DictValue] ;
                [self reloadCellseemore:cell_index];
            }
        
        });

        
    } errorBlock:^(id error)
     {
         //         NSLog(@"error is %@",error);
     }];
    
}
#pragma mark ReloadCellseemoreMethod

-(void)reloadCellseemore:(NSUInteger)cell_index{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cell_index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableView_outlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
}


- (void)setUpCell2:(See_MoreCommentsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    float width=[UIScreen mainScreen].bounds.size.width-(20);
    
    NSDictionary *dictSeemoreDict=[comments_arr objectAtIndex:indexPath.row];
    cell.seePplable.text=[comments_arr objectAtIndex:indexPath.row][@"body"];
    
    // pplabel implementation here
    NSError *error = nil;
    detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
    matches = [detector matchesInString:cell.seePplable.text options:0 range:NSMakeRange(0, cell.seePplable.text.length)];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [cell.seePplable addGestureRecognizer:singleTap];
    [self highlightLinksWithIndex:NSNotFound custum:cell.seePplable];
    cell.lbl_date.text=[comments_arr objectAtIndex:indexPath.row][@"created_at"];
    
    NSString *objectName;
    NSDictionary *getobjectuserdetailDict=[dictSeemoreDict valueForKey:@"get_object_user_details"];
    
    if ([getobjectuserdetailDict isKindOfClass:[NSDictionary class]]) {
        NSString *str1=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:[comments_arr objectAtIndex:indexPath.row][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]];
        str1=[NSString stringWithFormat:@"%@%@",ImageapiUrl,str1];
        
//        influencerimage
        NSString *checkinfluencerimage=[NSString stringWithFormat:@"%@",[getobjectuserdetailDict valueForKey:@"user_type"]];
        if ([checkinfluencerimage isEqualToString:@"2"]) {
            cell.influencerimage.hidden=NO;
           [cell.imageview_commentperson sd_setImageWithURL:[NSURL URLWithString:str1] placeholderImage:[UIImage imageNamed:@"person"]];
        }else{
            cell.influencerimage.hidden=YES;
           [cell.imageview_commentperson sd_setImageWithURL:[NSURL URLWithString:str1] placeholderImage:[UIImage imageNamed:@"person"]];
        }
        
        objectName=[NSString stringWithFormat:@"%@ %@",[comments_arr objectAtIndex:indexPath.row][@"get_object_user_details"][@"first_name"],[comments_arr objectAtIndex:indexPath.row][@"get_object_user_details"][@"last_name"]];
    }

    cell.imageview_commentperson.tag=indexPath.row;
    cell.imageview_commentperson.userInteractionEnabled=YES;
    cell.lbl_commentPersonName.tag=indexPath.row;
    cell.lbl_commentPersonName.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *commentImage_gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_Seecomment:)];
    [cell.imageview_commentperson addGestureRecognizer:commentImage_gesture3];
    
    UITapGestureRecognizer *commentpersongesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_Seecomment:)];
    [cell.lbl_commentPersonName addGestureRecognizer:commentpersongesture3];
    cell.lbl_commentPersonName.text=objectName;
    
}

#pragma mark subject_SeecommentMethod

- (void)subject_Seecomment:(UITapGestureRecognizer*)sender {
    NSDictionary *dictComment=[comments_arr objectAtIndex:sender.view.tag];
    NSDictionary *getobjectDict=[dictComment valueForKey:@"get_object_user_details"];
    if ([getobjectDict isKindOfClass:[NSDictionary class]]) {
    NSString *userid=[NSString stringWithFormat:@"%@",[getobjectDict valueForKey:@"id"]];
    NSString *influcerdata=[NSString stringWithFormat:@"%@",[getobjectDict valueForKey:@"user_type"]];
        [self profileView:friendprofile anfriend_ID:userid influncerdata:influcerdata];
    }
}

#pragma mark profileViewMethod

-(void)profileView :(NSString*)methodName anfriend_ID:(NSString*)friendID influncerdata:(NSString*)influncerdata{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : friendID,
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                NSMutableArray *messageArray;
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableDictionary *seemoreDict=[response valueForKey:@"data"];
                        
                        if ([influncerdata isEqualToString:@"2"]) {
                            InfluencerProfileController *influencer=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                            influencer.influencerDict=seemoreDict;
//                          [self presentViewController:influencer animated:YES completion:nil];
                            [self.navigationController pushViewController:influencer animated:YES];
  
                        }
                        else{
                            FriendsProfileViewController *frndSeemore=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                            frndSeemore.friendProfileDict=seemoreDict;
//                            [self presentViewController:frndSeemore animated:YES completion:nil];
                            [self.navigationController pushViewController:frndSeemore animated:YES];

                        }
                        
                    });
                    
                }else{
                    messageArray=[response valueForKey:@"message"];
                    if ([Constant1 validateArray:messageArray]) {
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    }
                    
                    
                }
                
            }
        }
        
    } errorBlock:^(NSError *error)
     {
//         NSLog(@"error");
         
     }];
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 85;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark btn_moreImagesMethod

-(void)btn_moreImages:(UIButton*)sender{
    NSDictionary *dict=[comments_arr objectAtIndex:0];
    NSMutableArray *get_attachments=dict[@"postimage_array"];
    NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
//    NSLog(@"get_attachments is%@",get_attachments);
    for (NSDictionary* image_dict in get_attachments){
        
        [post_imageArray addObject:image_dict[@"file"]];
    }
    [self openphoto:post_imageArray];
    
}
- (void)moreImages:(UITapGestureRecognizer*)sender {
    NSDictionary *dict=[comments_arr objectAtIndex:0];
    NSMutableArray *get_attachments=dict[@"postimage_array"];
    NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
//    NSLog(@"get_attachments is%@",get_attachments);
    for (NSDictionary* image_dict in get_attachments){
        
        [post_imageArray addObject:image_dict[@"file"]];
    }
    
    [self openphoto:post_imageArray];
    
}
-(void)openphoto:(NSMutableArray*)imagearra{
    NSMutableArray *imagearr=[[NSMutableArray alloc]init];
    
    //  photos=[[NSMutableArray alloc]init];
    for (int i=0; i<imagearra.count; i++) {
        NSString *str=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,[imagearra objectAtIndex:i]];
        MWPhoto *photo=[MWPhoto photoWithURL:[NSURL URLWithString:str]];
//        photo.caption = @"Biblefaithfollow.SocialApp";
        
        [imagearr addObject:photo];
    }
    photos=[[[imagearr reverseObjectEnumerator] allObjects] mutableCopy];
    browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    BOOL displayActionButton = YES;
    BOOL displaySelectionButtons = NO;
    BOOL displayNavArrows = NO;
    BOOL enableGrid = YES;
    BOOL startOnGrid = NO;
    BOOL autoPlayOnAppear = NO;
    browser.displayActionButton = displayActionButton;
    //  browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = NO;
    browser.zoomPhotosToFill = YES;
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = NO;
    //browser.autoPlayOnAppear = autoPlayOnAppear;
    [browser setCurrentPhotoIndex:0];
    
    enableGrid = NO;
    
    [self.navigationController pushViewController:browser animated:YES];
}
- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
    
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
//    NSLog(@"Did finish modal presentation");
    // [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Btn_cancelMethod

- (IBAction)btn_cancel:(id)sender {
    keyboard=YES;
    [toolbar_txtfiled resignFirstResponder];
    [_txtField_comments resignFirstResponder];
    
    if ([_backViewController isEqualToString:@"NotificationViewController"]) {
        [self.navigationController popViewControllerAnimated:YES];
        
        return;
    }
    
    if (get_latest_commnets_arr.count==0) {
        NSLog(@"hell0");
        
    }else{
        NSString *cellnumber=[NSString stringWithFormat:@"%lu",(unsigned long)_cellindex];
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:get_latest_commnets_arr,@"commenst_Arr",cellnumber,@"cellindex",_countofcomments,@"countofcomments",nil];
        NSDictionary *userInfo =[dict mutableCopy];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"commentsNoti" object:nil userInfo:userInfo];
    }
   NSString *cellnumber=[NSString stringWithFormat:@"%lu",(unsigned long)_cellindex];
    NSString *checklikeparameter=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"likecheck"]];
    NSString *countcheck=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countValue"]];
    NSMutableDictionary *dictdata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:cellnumber,@"cellindex",checklikeparameter,@"likefromseemore",countcheck,@"countcheck",nil];
    NSDictionary *userInfo =[dictdata mutableCopy];
         [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadcellwall" object:nil userInfo:userInfo];
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)send:(id)sender{
    keyboard=YES;
    [toolbar_txtfiled resignFirstResponder];
    [_txtField_comments resignFirstResponder];
    
//NSString *contentType=[ ]
    if (text_filedString.length==0) {
        text_filedString=@"";
        keyboard=YES;
        [toolbar_txtfiled resignFirstResponder];
        [_txtField_comments resignFirstResponder];
        get_latest_commnets_arr=nil;
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:appTitle message:@"Please enter comment"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message show];
        return ;
    }
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"ref_type":_post_dict[@"ref_type"],
                           @"ref_id":_post_dict[@"ref_id"],
                           @"body" :text_filedString,
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           };
   
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        //  NSLog(@"dict is%@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:addcomments withParameters:dict withHud:YES success:^(id response){
            NSMutableArray *messageArray;
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    
                    if([response[@"status"]intValue ] ==1){
                                                
                        messageArray=[response valueForKey:@"message"];
                        NSString *str=[messageArray objectAtIndex:0];
                        NSMutableArray *count_arr=[response valueForKey:@"post_details"];
                        get_latest_commnets_arr=[[NSMutableArray alloc]init];
                        for (NSDictionary *comment in count_arr) {
                            get_latest_commnets_arr=comment[@"get_latest_commnets"];
                        }
                        
                        for (NSDictionary *commede in count_arr) {
                            _countofcomments=commede[@"get_total_commnets"][@"count"];
                        }
                        // Comment for mixpanle
                        NSString *contentType;
                        NSString *postTypeSee=[NSString stringWithFormat:@"%@",[_post_dict objectForKey:@"post_type"]];
                        if ([postTypeSee isEqualToString:@"2"]) {
                            contentType=@"Prayer Request";
                        }
                        if ([postTypeSee isEqualToString:@"3"]) {
                            contentType=@"Update";
                        }
                        if ([postTypeSee isEqualToString:@"1"]) {
                            contentType=@"Update";
                        }
                       
                        NSString *emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
                        
                        if ([emailID isEqualToString:@""]||[emailID isEqualToString:@"(null)"]) {
                            emailID=@"";
                        }
                        else{
                            emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
                        }

                        
                        NSString *currentDate=[Constant1 createTimeStamp];
                        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last comment":currentDate}];
                        
                        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Date of last comment":currentDate,@"Total times commented":_countofcomments}];
                        
                        
                        [[AppDelegate sharedAppDelegate].mxPanel track:@"Comment"
                                properties:@{@"Content ID":_post_dict[@"ref_id"],@"Posted by":_post_dict[@"subjectName"],@"Content type":contentType,@"Date of last like":currentDate}
                         ];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                    if ([Constant1 validateArray:messageArray]) {
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        }
                    [self commentsView];
                    [_tableView_outlet reloadData];
                            
                        });
                        
                    }else{
                messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                  
                    if ([Constant1 validateArray:messageArray]) {
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    }
                        });

                    }
                }
            }
        }errorBlock:^(id error)
         {
//             NSLog(@"error is %@",error);
         }];
    }
}
- (IBAction)beginEditing: (id)sender {
    CGFloat _width = self.view.frame.size.width;
    CGFloat _height = 70.0;
    
    UIView *tfView = [[UIView alloc] initWithFrame:CGRectMake(24, 10, (_width-24-16-30-24), _height-20)];
    tfView.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    tfView.layer.cornerRadius = 8.0f;
    tfView.layer.masksToBounds = YES;
    
    toolbar_txtfiled = [[UITextField  alloc] initWithFrame: CGRectMake(10, 0, tfView.frame.size.width-10, _height-20)];
    // This sets the border style of the text field
    toolbar_txtfiled.borderStyle = UITextBorderStyleNone;
    toolbar_txtfiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [toolbar_txtfiled setFont:[UIFont fontWithName:@"Avenir-Roman" size:14]];
    toolbar_txtfiled.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0];
    toolbar_txtfiled.backgroundColor = [UIColor clearColor];
    toolbar_txtfiled.delegate=self;
    //Placeholder text is displayed when no text is typed
    toolbar_txtfiled.placeholder = @"Write a Comment";
    [tfView addSubview:toolbar_txtfiled];
    
    barButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [barButton setFrame:CGRectMake((24+tfView.frame.size.width+16), 20, 30, 30)];
    [barButton addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
    [barButton setImage:[UIImage imageNamed:@"send_unselected"] forState:UIControlStateNormal];
    
    barButtonItem=  [[UIBarButtonItem alloc] initWithCustomView:barButton];
    UIBarButtonItem *textFieldItem = [[UIBarButtonItem alloc] initWithCustomView:tfView];
    
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, _width, _height)];
    toolbar.barTintColor=[UIColor whiteColor];
    [toolbar setItems:[[NSArray alloc] initWithObjects:textFieldItem,barButtonItem, nil]] ;
    
    if ([sender isEqual:_txtField_comments]) {
        _txtField_comments.inputAccessoryView = toolbar;
    }
    [toolbar_txtfiled becomeFirstResponder];

//    toolbar_txtfiled = [[UITextField  alloc] initWithFrame: CGRectMake(20, 50, 280, _height)];
//    // This sets the border style of the text field
//    toolbar_txtfiled.borderStyle = UITextBorderStyleRoundedRect;
//    toolbar_txtfiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//    [toolbar_txtfiled setFont:[UIFont fontWithName:@"Avenir-Roman" size:14]];
//    toolbar_txtfiled.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0];
//    //Placeholder text is displayed when no text is typed
//    toolbar_txtfiled.placeholder = @"Write a Comment";
//    toolbar_txtfiled.delegate=self;
//    toolbar_txtfiled.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
//
//    barButton=[UIButton buttonWithType:UIButtonTypeCustom];
//    [barButton setFrame:CGRectMake(16, 0, 30, 30)];
//    [barButton addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
//    [barButton setImage:[UIImage imageNamed:@"send_unselected"] forState:UIControlStateNormal];
//    barButtonItem=  [[UIBarButtonItem alloc] initWithCustomView:barButton];
//    UIBarButtonItem *textFieldItem = [[UIBarButtonItem alloc] initWithCustomView:toolbar_txtfiled];
//
//    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//
//    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, -_height, _width, _height)];
//    toolbar.barTintColor=[UIColor whiteColor];
//    [toolbar setItems:[[NSArray alloc] initWithObjects:textFieldItem,flexibleSpace,barButtonItem, nil]] ;
//
//    if ([sender isEqual:_txtField_comments]) {
//        _txtField_comments.inputAccessoryView = toolbar;
//    }
//    [toolbar_txtfiled becomeFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (keyboard==YES) {
        
    }else{
        if ([textField isEqual:_txtField_comments]) {
            [self beginEditing:_txtField_comments];
            [toolbar_txtfiled becomeFirstResponder];
        }
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField==toolbar_txtfiled) {
        keyboard=YES;
        [toolbar_txtfiled resignFirstResponder];
        [_txtField_comments resignFirstResponder];
    }
    
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==_txtField_comments){
        
    }else
    {
        text_filedString=toolbar_txtfiled.text;
        toolbar_txtfiled.text=@"";
        [_txtField_comments resignFirstResponder];
    }
    
    
}
- (void)myNotificationMethod:(NSNotification*)notification
{
    if (keyboard==NO) {
        [barButton setImage:[UIImage imageNamed:@"send_selected"] forState:UIControlStateNormal];
        [toolbar_txtfiled becomeFirstResponder];
        return;
    }
    [barButton setImage:[UIImage imageNamed:@"send_unselected"] forState:UIControlStateNormal];
    
}
- (void)myNotificationMethod2:(NSNotification*)notification
{
    keyboard=NO;
}
#pragma mark commentsViewMethod

-(void)commentsView {
    
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"ref_type":_post_dict[@"ref_type"],
                           @"ref_id":_post_dict[@"ref_id"],
                           };
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:commentLists withParameters:dict withHud:YES success:^(id response){
            NSMutableArray *messageArray;
            //  NSLog(@"response is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        
                        _totalComments=[response valueForKey:@"data"];
                        [comments_arr removeAllObjects];
                        [comments_arr addObject:_post_dict];
                        [comments_arr addObjectsFromArray:_totalComments];
//                        comments_arr = [[[_totalComments reverseObjectEnumerator] allObjects] mutableCopy];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_tableView_outlet reloadData];
                            _txtField_comments.text=@"";
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                    if ([Constant1 validateArray:messageArray]) {
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                        });
                    }
                    
                }
                
            }
            
        } errorBlock:^(id error)
         {
//             NSLog(@"error is %@",error);
         }];
    }
}
-(void)animateView:(CGFloat)constrantsConstant andduration:(double)duration{
    //    [UIView animateWithDuration:duration
    //                          delay:0.0
    //                        options: UIViewAnimationOptionCurveEaseIn
    //                     animations:^{
    //                         _Constraint_bottomtxtView.constant=constrantsConstant;
    //                         [self.view layoutIfNeeded];
    //                     }
    //                     completion:^(BOOL finished){
    //                     }];
    _Constraint_bottomtxtView.constant=260;
    [self.view layoutIfNeeded];
}

#pragma mark -

- (void)label:(PPLabel *)label didBeginTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:charIndex custum:label];

}

- (void)label:(PPLabel *)label didMoveTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:charIndex custum:label];
}

- (void)label:(PPLabel *)label didEndTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //    [self highlightLinksWithIndex:NSNotFound custum:label];
    //
    //    for (NSTextCheckingResult *match in self.matches) {
    //
    //        if ([match resultType] == NSTextCheckingTypeLink) {
    //
    //            NSRange matchRange = [match range];
    //
    //            if ([self isIndex:charIndex inRange:matchRange]) {
    //
    //                [[UIApplication sharedApplication] openURL:match.URL];
    //                break;
    //            }
    //        }
    //    }
    
}

- (void)label:(PPLabel *)label didCancelTouch:(UITouch *)touch {
    
    //    [self highlightLinksWithIndex:NSNotFound custum:label];
}

#pragma mark -

- (BOOL)isIndex:(CFIndex)index inRange:(NSRange)range {
    return index > range.location && index < range.location+range.length;
}

- (void)highlightLinksWithIndex:(CFIndex)index custum:(PPLabel*)label {
    
    NSMutableAttributedString* attributedString = [label.attributedText mutableCopy];
    
    for (NSTextCheckingResult *match in matches) {
        
        if ([match resultType] == NSTextCheckingTypeLink) {
            
            NSRange matchRange = [match range];
            
            if ([self isIndex:index inRange:matchRange]) {
                [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:matchRange];
                
            }
            else {
                [attributedString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x00ADEB) range:matchRange];
            }
            
            [attributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:matchRange];
        }
    }
    
    label.attributedText = attributedString;
}

- (void)handleTap:(UITapGestureRecognizer *)tapRecognizer
{
    PPLabel *textLabel = (PPLabel *)tapRecognizer.view;
    CGPoint tapLocation = [tapRecognizer locationInView:textLabel];
    NSString *testURL=[NSString stringWithFormat:@"%@",textLabel.text];
    
    // init text storage
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:textLabel.attributedText];
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    [textStorage addLayoutManager:layoutManager];
    
    // init text container
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(textLabel.frame.size.width, textLabel.frame.size.height+100) ];
    textContainer.lineFragmentPadding  = 0;
    textContainer.maximumNumberOfLines = textLabel.numberOfLines;
    textContainer.lineBreakMode        = textLabel.lineBreakMode;
    
    [layoutManager addTextContainer:textContainer];
    
    NSUInteger characterIndex = [layoutManager characterIndexForPoint:tapLocation
                                                      inTextContainer:textContainer
                             fractionOfDistanceBetweenInsertionPoints:NULL];
    
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:testURL options:0 range:NSMakeRange(0, [testURL length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSRange matchRange = [match range];
            if ([self isIndex:characterIndex inRange:matchRange]) {
                [[UIApplication sharedApplication] openURL:match.URL];
                break;
            }
        }
    }
    
    
}


@end
