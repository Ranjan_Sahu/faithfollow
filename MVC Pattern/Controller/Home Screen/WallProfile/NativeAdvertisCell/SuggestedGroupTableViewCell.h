//
//  SuggestedGroupTableViewCell.h
//  wireFrameSplash
//
//  Created by home on 9/6/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WallView.h"

@interface SuggestedGroupTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeight;
@property (weak, nonatomic) IBOutlet UIView *main_view;

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic,strong)NSArray *influencelistArray;
-(void)setData:(NSArray *)array;







@end
