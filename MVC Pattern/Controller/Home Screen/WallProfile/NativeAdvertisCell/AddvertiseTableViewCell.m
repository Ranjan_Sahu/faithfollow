//
//  AddvertiseTableViewCell.m
//  wireFrameSplash
//
//  Created by Vikas on 14/06/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "AddvertiseTableViewCell.h"

@implementation AddvertiseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.main_view.layer.borderWidth=0.6f;
    self.main_view.layer.cornerRadius=8.0;
    
    //  self.formalView.backgroundColor=[UIColor blackColor];
    
    
    self.main_view.layer.borderColor=[UIColor grayColor].CGColor;

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
