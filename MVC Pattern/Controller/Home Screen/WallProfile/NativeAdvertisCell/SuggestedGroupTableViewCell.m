//
//  SuggestedGroupTableViewCell.m
//  wireFrameSplash
//
//  Created by home on 9/6/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "SuggestedGroupTableViewCell.h"
#import "suggestedCellDynamic.h"
#import "UIImageView+WebCache.h"


@implementation SuggestedGroupTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.tableview registerNib:[UINib nibWithNibName:@"suggestedCellDynamic" bundle:nil] forCellReuseIdentifier:@"suggestedCellDynamic"];
//    self.main_view.layer.borderWidth=0.6f;
//    self.main_view.layer.cornerRadius=8.0;
//    self.main_view.clipsToBounds=YES;

    self.tableview.tableFooterView = [UIView new];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)setData:(NSArray *)array{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.influencelistArray=array;
        [self.tableview reloadData];
    });
}

#pragma mark TableView Data Source methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.influencelistArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    suggestedCellDynamic *cell = (suggestedCellDynamic *)[self.tableview dequeueReusableCellWithIdentifier:@"suggestedCellDynamic"                                                                    forIndexPath:indexPath];
    
    NSDictionary *dict=[_influencelistArray objectAtIndex:indexPath.row];
    if ([dict isKindOfClass:[NSDictionary class]]) {
        
        NSString *Profilename=[NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"first_name"],[dict valueForKey:@"last_name"]];
        cell.userNameTXT.text=Profilename;
        cell.userNameTXT.editable = YES;
        cell.userNameTXT.font = semibold;
        cell.userNameTXT.editable = NO;
    NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[dict valueForKey:@"profile_picture"]];
        NSURL *imageURL = [NSURL URLWithString:str];
    [cell.userImageView sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
        
        // tap gesture to open Profile subject
            cell.userNameTXT.userInteractionEnabled=YES;
            cell.userImageView.userInteractionEnabled=YES;
            UITapGestureRecognizer *suggestedImage_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(suggetsedImage:)];
            [cell.userImageView addGestureRecognizer:suggestedImage_gesture];
        
            UITapGestureRecognizer *group1namename_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(suggetsedName:)];
            [cell.userNameTXT addGestureRecognizer:group1namename_gesture];
            cell.userNameTXT.tag=indexPath.row;
            cell.userImageView.tag=indexPath.row;
            cell.followBTN.tag=indexPath.row;
            [cell.followBTN addTarget:self action:@selector(fellowbtnMethod:) forControlEvents:UIControlEventTouchUpInside];
       
        if (indexPath.row==[_influencelistArray count]-1) {
            cell.sepreterLine.hidden=YES;
        }else{
            cell.sepreterLine.hidden=NO;
          }
        
    }
    
     return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)suggetsedImage:(UITapGestureRecognizer*)sender {
    NSDictionary *getDict=[_influencelistArray objectAtIndex:sender.view.tag];
    NSDictionary *userInfogroup = @{
                                    @"groupData" :getDict,
                                    };
    [[NSNotificationCenter defaultCenter] postNotificationName: @"DynamicImage" object:nil userInfo:userInfogroup];
}

- (void)suggetsedName:(UITapGestureRecognizer*)sender {
    
    NSDictionary *getDict=[_influencelistArray objectAtIndex:sender.view.tag];
        NSDictionary *userInfogroup = @{
                                        @"groupData" :getDict,
                                        };
    [[NSNotificationCenter defaultCenter] postNotificationName: @"suggestUsername" object:nil userInfo:userInfogroup];
}

-(void)fellowbtnMethod:(UIButton *)sender{
    CGPoint center= sender.center;
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableview];
    NSIndexPath *indexPathvalue = [self.tableview indexPathForRowAtPoint:rootViewPoint];
    NSDictionary *getDict=[_influencelistArray objectAtIndex:indexPathvalue.row];
    NSDictionary *userInfogroup = @{
                                    @"groupData" :getDict,
                                    };
    [[NSNotificationCenter defaultCenter] postNotificationName: @"fellowAction" object:nil userInfo:userInfogroup];
}


@end
