//
//  AddvertiseTableViewCell.h
//  wireFrameSplash
//
//  Created by Vikas on 14/06/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddvertiseTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_personName;
@property (weak, nonatomic) IBOutlet UIView *main_view;
@end
