//
//  popRatefeedViewController.m
//  wireFrameSplash
//
//  Created by home on 9/27/16.
//  Copyright © 2016 home. All rights reserved.
//

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]
#import "popRatefeedViewController.h"
#import "DeviceConstant.h"
#import "FeedBackRateViewController.h"
#import "iRate.h"



@interface popRatefeedViewController ()<UIAlertViewDelegate>

@end

@implementation popRatefeedViewController
@synthesize rateView;
- (void)viewDidLoad {
    [super viewDidLoad];
    _rateViewHight.constant=40;
    self.couldbtn.layer.borderWidth = 1.0f;
    self.couldbtn.layer.borderColor = UIColorFromRGB(0x95AECC).CGColor;
    
    self.loveitbtn.layer.borderWidth = 1.0f;
    self.loveitbtn.layer.borderColor = UIColorFromRGB(0x95AECC).CGColor;
    self.loveitbtn.layer.cornerRadius = 3.0f;
    self.couldbtn.layer.cornerRadius = 3.0f;
    self.howcanLBL.hidden=YES;
    self.commenTXT.layer.borderWidth = 0.4f;
    self.commenTXT.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
 
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[AppDelegate sharedAppDelegate] hideaddView];
    self.commenTXT.hidden=YES;
    self.submitbnt.hidden=YES;
    self.textmessageLBL.text=@"It looks like you are making use of Faith Follow. We would love to hear your feedback.";
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [getVar hideaddView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}





-(void)keyboardWillShow:(NSNotification*)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *info  = notification.userInfo;
//        NSLog(@"info %@",[info objectForKey:@"UIKeyboardFrameEndUserInfoKey"]);
        CGSize keyboardSize=[[info objectForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue].size;
        _rateViewHight.constant=keyboardSize.height+40.0;
        [self.view layoutIfNeeded];
    });
}
-(void)keyboardWillHide:(NSNotification*)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _rateViewHight.constant=40;
        [self.view layoutIfNeeded];
    });
}


-(void)viewWillDisappear:(BOOL)animated
{    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (getVar.checkDisapper) {
        [[AppDelegate sharedAppDelegate] addsView];
    }

    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(IBAction)closeBTN:(id)sender{
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"feedback"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    rateView.hidden=YES;
    [self dismissViewControllerAnimated:NO completion:^{
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [getVar unhideView];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (IBAction)lovebtnMethod:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^{
        AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [getVar unhideView];
        [iRate sharedInstance].delegate=(id)self;
        [iRate sharedInstance].previewMode = YES;
        [iRate sharedInstance].message=@"We are happy to hear you Love our app. Please rate us to help spread the word.";
        [[iRate sharedInstance] logEvent:NO];
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"feedback"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
   
}

- (IBAction)couldbetterbtnMethod:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"feedback"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.commenTXT.hidden=NO;
    self.submitbnt.hidden=NO;
    self.howcanLBL.hidden=NO;
    self.howcanLBL.text=@"How can we improve our app?";
    self.couldbtn.hidden=YES;
    self.loveitbtn.hidden=YES;
    self.textmessageLBL.hidden=YES;
 //    FeedBackRateViewController *feedBackView=[self.storyboard instantiateViewControllerWithIdentifier:@"FeedBackRateViewController"];
//    [self presentViewController:feedBackView animated:YES completion:nil];
    
}

#pragma mark - Validation
- (NSString *)validateFields {
    NSString *errorMessage ;
    NSString *strTextview = [self.commenTXT.text removeWhiteSpaces];
    
    if ([strTextview length] == 0) {
        errorMessage =FeedbackStr;
        return errorMessage;
    }
    return 0;
}

- (IBAction)sumbitMethod:(id)sender {
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *errorMessage = [self validateFields];
        if (errorMessage) {
            [[[UIAlertView alloc] initWithTitle:appTitle message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
            return;
        }else{
            NSDictionary *dict = @{
                                  @"user_id" :[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"],
                                   @"message" : self.commenTXT.text,
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:feedback withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    if (![response isKindOfClass:[NSNull class]]) {
                            if([response[@"status"]intValue ] ==1)
                            {
                                    NSString *message;
                                    message=[response valueForKey:@"message"];
                                    if ([message isKindOfClass:[NSString class]]) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            UIAlertView *popRateView=[[UIAlertView alloc]initWithTitle:appTitle message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] ;
                                            [popRateView show];
                                            
                                        });
                                }
                                
                            }
                            else{
                                NSString *message;
                                message=[response valueForKey:@"message"];
                                if ([message isKindOfClass:[NSString class]]) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        UIAlertView *popRateView=[[UIAlertView alloc]initWithTitle:appTitle message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] ;
                                        [popRateView show];
                                        
                                    });
                                    
                                }
                            }
                    }
                }
            } errorBlock:^(NSError *error)
             {
//                 NSLog(@"error is %@",error);
             }];
        }
        
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:NO completion:^{
        AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [getVar unhideView];
    }];
  });

}
    
}



@end
