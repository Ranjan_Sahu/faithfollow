//
//  FeedwithImagePostTableViewCell.h
//  wireFrameSplash
//
//  Created by Vikas on 30/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLabel.h"


@interface FeedwithImagePostTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_personName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_person;
@property (weak, nonatomic) IBOutlet UILabel *lbl_post;
@property (weak, nonatomic) IBOutlet UIButton *btn_praisedCount;
@property (weak, nonatomic) IBOutlet UIButton *btn_commentsCount;
@property (weak, nonatomic) IBOutlet UIButton *btn_praised;
@property (weak, nonatomic) IBOutlet UIButton *btn_Comments;
@property (weak, nonatomic) IBOutlet UIView *main_view;
@property (weak, nonatomic) IBOutlet UILabel *imageview_praised;

@property (weak, nonatomic) IBOutlet UIImageView *imageview_post;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightofLabelconstraint;



@property (weak, nonatomic) IBOutlet UIButton *btn_readmorepost;
@property (weak, nonatomic) IBOutlet UIButton *btn_moreImages;
@property (weak, nonatomic) IBOutlet UIView *view_btnmoareimage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainview_height;
@property (weak, nonatomic) IBOutlet UIButton *btn_commentsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightof_seemorecomments;
@property (weak, nonatomic) IBOutlet UIButton *btn_seemorecomments;
@property (weak, nonatomic) IBOutlet UILabel *lbl_postStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_objectname;

//////////////////////////
@property (weak, nonatomic) IBOutlet UIImageView *imageview_comment1;

@property (weak, nonatomic) IBOutlet UIImageView *imageview_comment2;

@property (weak, nonatomic) IBOutlet UIImageView *imageview_comment3;


@property (weak, nonatomic) IBOutlet UILabel *lbl_commentname1;

@property (weak, nonatomic) IBOutlet UILabel *lbl_commentname2;


@property (weak, nonatomic) IBOutlet UILabel *lbl_commentname3;


@property (weak, nonatomic) IBOutlet UILabel *lbl_commenttext1;

@property (weak, nonatomic) IBOutlet UILabel *lbl_commenttext2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_commenttext3;
@property (weak, nonatomic) IBOutlet UILabel *lbl_commentdate1;

@property (weak, nonatomic) IBOutlet UILabel *lbl_commentdate2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_commentdate3;

////////////////////////////////

@property (weak, nonatomic) IBOutlet UIView *view_comment1;
@property (weak, nonatomic) IBOutlet UIView *view_comment2;
@property (weak, nonatomic) IBOutlet UIView *view_comment3;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomofcommentView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightofobjectname;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthofnamelbl;
@property (weak, nonatomic) IBOutlet UIButton *dropdownbutton;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topofdatelabel;
@property (weak, nonatomic) IBOutlet UIImageView *praisedbtn_imageview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightofimageview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthofimageview;

@property (weak, nonatomic) IBOutlet UILabel *toppostImagelbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toppostImagelblHeight;

@property (weak, nonatomic) IBOutlet UIButton *seeMoreGroupRequestbtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *seeMoreGroupRequestHeght;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toppostbetviewinImage;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentViewSeemoretopHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *transparenttopHeight;


/////

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightofBottomButtonView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topgroupHeight;

@property (weak, nonatomic) IBOutlet PPLabel *labelPP;
@property (weak, nonatomic) IBOutlet PPLabel *pplableComment1;
@property (weak, nonatomic) IBOutlet PPLabel *pplableComment2;
@property (weak, nonatomic) IBOutlet PPLabel *pplableComment3;

// influencerimage 
@property (weak, nonatomic) IBOutlet UIImageView *main_profile_image;
@property (weak, nonatomic) IBOutlet UIImageView *commentimage1;
@property (weak, nonatomic) IBOutlet UIImageView *commentimage2;
@property (weak, nonatomic) IBOutlet UIImageView *commentimage3;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commetHeightfromcontentLBL;



@end
