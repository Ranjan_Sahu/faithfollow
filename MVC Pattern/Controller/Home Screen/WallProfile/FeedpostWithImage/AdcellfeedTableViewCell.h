//
//  AdcellfeedTableViewCell.h
//  wireFrameSplash
//
//  Created by webwerks on 2/6/17.
//  Copyright © 2017 home. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DFPBannerView;
@interface AdcellfeedTableViewCell : UITableViewCell
@property(strong,nonatomic)IBOutlet UIView *main_View;
@property(strong,nonatomic)IBOutlet UIImageView *imageview_person;
@property (weak, nonatomic) IBOutlet UILabel *adName;
@property (weak, nonatomic) IBOutlet UILabel *headline;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLbl;

@property (weak, nonatomic) IBOutlet UIButton *visitBTN;

@property (weak, nonatomic) IBOutlet UIImageView *AdImage;

@property(nonatomic, strong) IBOutlet DFPBannerView *bannerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerTopHieghtConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainView_Widthconstraits;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainView_Heightconstraits;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerView_Heightconstraits;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerView_Widthconstraits;





@end
