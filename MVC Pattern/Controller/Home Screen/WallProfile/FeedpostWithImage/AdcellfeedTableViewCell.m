//
//  AdcellfeedTableViewCell.m
//  wireFrameSplash
//
//  Created by webwerks on 2/6/17.
//  Copyright © 2017 home. All rights reserved.
//

#import "AdcellfeedTableViewCell.h"
@implementation AdcellfeedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
      
//    self.main_View.layer.borderWidth=0.6f;
//    self.main_View.layer.cornerRadius=8.0;
//    self.main_View.layer.masksToBounds = YES;
    
//    self.imageview_person.layer.borderColor=[UIColor colorWithRed:0 green:154/255 blue:217/255  alpha:1.0f].CGColor;
//    _imageview_person.layer.borderColor=[self colorFromHexString:@"009AD9"].CGColor;
//    self.imageview_person.layer.borderWidth=0.8f;
//    self.imageview_person.layer.cornerRadius=5.0;
//    self.imageview_person.clipsToBounds=YES;
    // Initialization code
}
-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
