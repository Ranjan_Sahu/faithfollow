//
//  FeedwithImagePostTableViewCell.m
//  wireFrameSplash
//
//  Created by Vikas on 30/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "FeedwithImagePostTableViewCell.h"

@implementation FeedwithImagePostTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.main_view.layer.borderWidth=0.6f;
//    self.main_view.layer.cornerRadius=8.0;
    
    //  self.formalView.backgroundColor=[UIColor blackColor];
    
    _imageview_post.clipsToBounds=YES;
//    _imageview_post.backgroundColor=[UIColor greenColor];
//    self.main_view.layer.borderColor=[UIColor grayColor].CGColor;
    self.view_btnmoareimage.layer.borderWidth=0.6f;
    self.view_btnmoareimage.layer.cornerRadius=_view_btnmoareimage.frame.size.width/2;
//    self.view_btnmoareimage.layer.borderColor=[UIColor blackColor].CGColor;
    //  self.formalView.backgroundColor=[UIColor blackColor];
    
//    _imageview_person.layer.borderColor=[UIColor colorWithRed:0 green:154/255 blue:217/255  alpha:1.0f].CGColor;
//    _imageview_person.layer.borderColor=[self colorFromHexString:@"009AD9"].CGColor;
//    _imageview_person.layer.borderWidth=0.8f;
//    _imageview_person.layer.cornerRadius=5.0;
    
//    self.imageview_comment1.layer.cornerRadius=  5.0;
//    _imageview_comment1.layer.borderColor=[self colorFromHexString:@"009AD9"].CGColor;
//    _imageview_comment1.layer.borderWidth=1.2f;
//    self.imageview_comment1.clipsToBounds=YES;
//    self.imageview_comment2.layer.cornerRadius=  5.0;
//    _imageview_comment2.layer.borderColor=[self colorFromHexString:@"009AD9"].CGColor;
//    _imageview_comment2.layer.borderWidth=1.2f;
//    //self.imageview_comment2.clipsToBounds=YES;
//    self.imageview_comment3.layer.cornerRadius=  5.0;
//    _imageview_comment3.layer.borderColor=[self colorFromHexString:@"009AD9"].CGColor;
//    _imageview_comment3.layer.borderWidth=1.2f;
//    self.imageview_comment3.clipsToBounds=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
