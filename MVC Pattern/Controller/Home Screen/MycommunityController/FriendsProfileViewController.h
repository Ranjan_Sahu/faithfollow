//
//  FriendsProfileViewController.h
//  wireFrameSplash
//
//  Created by Vikas on 09/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WallView.h"
@interface FriendsProfileViewController : UIViewController<MyFirstControllerDelegate>

@property(nonatomic,strong)NSMutableDictionary *postFriendProfileDict;;
@property(nonatomic,strong)NSMutableDictionary *friendProfileDict;;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(strong,nonatomic) NSString *checkListViewProfile;

@property (weak, nonatomic) IBOutlet UITableView *tableView_outlet;


@property (weak, nonatomic) IBOutlet UILabel *labeloutlet;

@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_email;

@property (weak, nonatomic) IBOutlet UILabel *lbl_mobile;

@property (weak, nonatomic) IBOutlet UILabel *lbl_birthday;


@property (weak, nonatomic) IBOutlet UILabel *lbl_gender;


@property (weak, nonatomic) IBOutlet UILabel *lbl_denomation;

@property (weak, nonatomic) IBOutlet UILabel *lbl_jobtitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_parish;
@property (weak, nonatomic) IBOutlet UILabel *lbl_employer;

@property (weak, nonatomic) IBOutlet UILabel *lbl_highschool;
@property (weak, nonatomic) IBOutlet UILabel *lbl_relationship;

@property (weak, nonatomic) IBOutlet UILabel *lbl_siblings;
@property (weak, nonatomic) IBOutlet UILabel *lbl_children;
@property (weak, nonatomic) IBOutlet UILabel *lbl_favourateplayer;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Commentscount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Likescount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_inspirational;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_frnds;
@property (weak, nonatomic) IBOutlet UIButton *btn_addFrind;
- (IBAction)btn_addFrind:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_btnwall;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_wall;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_btnprofile;
- (IBAction)btn_profile:(id)sender;

- (IBAction)btn_wall:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btn_profile;
@property (weak, nonatomic) IBOutlet UIButton *btn_wall;

@property (weak, nonatomic) IBOutlet UIView *bckView1;

@property (weak, nonatomic) IBOutlet UIView *bckview3;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Mainname;

@property (weak, nonatomic) IBOutlet UIView *cancelnavigationBtn;

-(IBAction)cancelBTN:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *wallView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
- (IBAction)ProfileViewBTN:(id)sender;

- (IBAction)wallViewMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *ProfileBTN;
@property (weak, nonatomic) IBOutlet UITextView *textViewTXT;

@property (weak, nonatomic) IBOutlet UIView *profileFndView;
@property (weak, nonatomic) IBOutlet UIView *wallFndView;
@property (weak, nonatomic) IBOutlet UIView *myFrinendView;
- (IBAction)myFriendListBTN:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightofwallView;

@property (weak, nonatomic) IBOutlet UILabel *friendbasicInfolbl;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *frindviewHeight;

@end
