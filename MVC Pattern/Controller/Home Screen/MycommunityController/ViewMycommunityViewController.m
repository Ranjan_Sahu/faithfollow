//
//  FriendsProfileViewController.m
//  wireFrameSplash
//
//  Created by Vikas on 09/05/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#define  kCellName @"FeedTableViewCell"
#import "UIImageView+WebCache.h"
#import "ViewMycommunityViewController.h"
#import "WebServiceHelper.h"
#import "FeedTableViewCell.h"
#import "StatusViewController.h"
#import "FeedViewController.h"

@interface ViewMycommunityViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>{
    NSMutableArray *arrFeeds,*_quoteArray;
    NSMutableDictionary *userProfile;
    NSString *user_id;
    CGFloat heightofCell;
    NSInteger selectedRow;
    UIView *lineView ;
    UIView *navigationView;
    
    
}

@end
@implementation ViewMycommunityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView_outlet   registerNib:[UINib nibWithNibName:kCellName bundle:nil] forCellReuseIdentifier:kCellName];
    selectedRow=-1;
    
    arrFeeds=[[NSMutableArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Feed" ofType:@"plist"]];
    
//    NSLog(@"friendDict %@",_friendProfileDict);
    _wallView.hidden=YES;
    _secondView.hidden=NO;
    
    
    _bckView1.hidden=NO;
  _bckview3.backgroundColor=UIColorFromRGB(0xAAAAAA);

    
    
    _lbl_btnwall.textColor=[UIColor colorWithRed:103/255.0f green:103/255.0f blue:103/255.0f alpha:1.0f];
    _lbl_btnprofile.textColor=[UIColor colorWithRed:27/255.0f green:153/255.0f blue:217/255.0f alpha:1.0f];
    _imageview_wall.image= [UIImage imageNamed:@"edit_unselected"];
    _imageview_profile.image= [UIImage imageNamed:@"profile_selected-1"];
    userProfile=_friendProfileDict[@"get_community_owner"];
    _lbl_ownername.text= [NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",userProfile[@"Owner_first_name"],userProfile[@"Owner_last_name"]]]];
    _lbl_lbldescrip.text= [NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"description"]]];
    
    _lbl_Mainname.text= [NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"community"]]];
    
    _lbl_name.text= [NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"community"]]];
    _imageView_frnds.layer.cornerRadius=_imageView_frnds.frame.size.width/2;
    _imageView_frnds.layer.borderWidth=0.5;
    _imageView_frnds.layer.borderColor=[UIColor blackColor].CGColor;
    _imageView_frnds.clipsToBounds=YES;
    _imageView_frnds.image=[self imageFromColorDefault:[UIColor whiteColor]];
    _imageView_frnds.layer.cornerRadius=_imageView_frnds.frame.size.width/2;
    _imageView_frnds.layer.borderWidth=0.5;
    _imageView_frnds.layer.borderColor=[UIColor blackColor].CGColor;
    _imageView_frnds.clipsToBounds=YES;
    
    [_imageView_frnds sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
    //    NSString *str1=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"profile_picture"]]];
    //    NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,str1];
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        [_imageView_frnds sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil];
    //    });
//    NSLog(@"userid is%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]);
    if (_friendProfileDict[@"created_by"]==[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]) {
        //        [_btn_addFrind setTitle:@"Left" forState:UIControlStateNormal];
    }else{
        //        [_btn_addFrind setTitle:@"Join" forState:UIControlStateNormal];
    }
    
    _lbl_createddate.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"created_at"]]];
    NSString *addStatus=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"add_status"]]];
    
    [_btn_addFrind setTitle:addStatus forState:UIControlStateNormal];
    
    [self imageLoad];
}
-(void)imageLoad{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_imageView_frnds sd_setImageWithURL:[NSURL URLWithString:_friendProfileDict[@"image_file"]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
        
        
    });
    
}

-(void)cancelBTN{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btn_addFrind:(id)sender {
    if ([_btn_addFrind.titleLabel.text isEqualToString:@"Join"]) {
//        NSLog(@"add");
        //   [self sendrequest :@"communitydetails"];
        
    }else{
        //   [self sendrequest :@"community/leave"];
        
//        NSLog(@"unadd");
    }
    //[self sendrequest :@"sendrequest"];
}
- (IBAction)btn_profile:(id)sender {
    _bckView1.hidden=NO;
    _imageview_wall.image= [UIImage imageNamed:@"edit_unselected"];
    _imageview_profile.image= [UIImage imageNamed:@"profile_selected-1"];
    _lbl_btnwall.textColor=[UIColor colorWithRed:103/255.0f green:103/255.0f blue:103/255.0f alpha:1.0f];
    _lbl_btnprofile.textColor=[UIColor colorWithRed:27/255.0f green:153/255.0f blue:217/255.0f alpha:1.0f];
    _wallView.hidden=YES;
    _secondView.hidden=NO;
    _bckview3.backgroundColor=UIColorFromRGB(0xAAAAAA);
    _bckView1.backgroundColor=UIColorFromRGB(0x009AD9);

    
    
}



- (IBAction)btn_wall:(id)sender {
    
    _bckview3.hidden=NO;
    _imageview_wall.image= [UIImage imageNamed:@"edit_selected"];
    _imageview_profile.image= [UIImage imageNamed:@"profile_unselected-1"];
    _lbl_btnprofile.textColor=[UIColor colorWithRed:103/255.0f green:103/255.0f blue:103/255.0f alpha:1.0f];
    _lbl_btnwall.textColor=[UIColor colorWithRed:27/255.0f green:153/255.0f blue:217/255.0f alpha:1.0f];
    _wallView.hidden=NO;
    _secondView.hidden=YES;
    FeedViewController *feed=[self.storyboard instantiateViewControllerWithIdentifier:@"FeedViewController"];
    //_wallView=feed.view;
    [_wallView addSubview:feed.view];
    _bckView1.backgroundColor=UIColorFromRGB(0xAAAAAA);
    _bckview3.backgroundColor=UIColorFromRGB(0x009AD9);

    
}
-(void)sendrequest :(NSString*)methodName{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"com_id" : user_id,
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
//        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                NSMutableArray *messageArray;
                if([response[@"status"]intValue ] ==1){
                    
                    messageArray=[response valueForKey:@"message"];
                    
                }else{
                    
                    messageArray=[response valueForKey:@"message"];
                    
                    
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([messageArray count]>0) {
                       [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                    }
                    
                });
            }
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
}

#pragma mark TableView Data Source methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrFeeds count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName forIndexPath:indexPath];
    [self setUpCell:cell atIndexPath:indexPath];
    
    if(indexPath.row == selectedRow)
    {
        NSArray *arr=[arrFeeds objectAtIndex:indexPath.row][@"comments"];
        [cell.commentsView refereshData:arr];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static FeedTableViewCell *cell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cell = [self.tableView_outlet dequeueReusableCellWithIdentifier:kCellName];
    });
    [self setUpCell:cell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:cell];
}
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsUpdateConstraints];
    [sizingCell updateConstraintsIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}
- (void)setUpCell:(FeedTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    float width=[UIScreen mainScreen].bounds.size.width-(20);
    cell.com_lbl.preferredMaxLayoutWidth = width; // This is necessary to allow the label
    NSDictionary *dict=[arrFeeds objectAtIndex:indexPath.row];
    cell.com_lbl.text=dict[@"desc"];
    [cell.comments_btn addTarget:self action:@selector(comments_act:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(indexPath.row == selectedRow)
    {
        NSArray *arr=dict[@"comments"];
        float  height=([arr count]*60);
        cell.commentsView.hidden=NO;
        cell.heightConsatraint.constant=height+72;
        cell.commentsView.tblViewHeight.constant=height;
        cell.mainView.layer.borderWidth=0.0f;
        
        //        cell.mainView.layer.borderWidth=0.5f;
        //        cell.mainView.layer.cornerRadius=8.0;
        //        cell.mainView.layer.borderColor=[UIColor redColor].CGColor;
        //
        //        cell.commentsView.layer.borderWidth=0.5f;
        //        cell.commentsView.layer.cornerRadius=8.0;
        //        cell.commentsView.layer.borderColor=[UIColor redColor].CGColor;
    }
    else{
        cell.heightConsatraint.constant=0;
        cell.commentsView.hidden=YES;
        cell.commentsView.tblViewHeight.constant=0;
        //        cell.mainView.layer.borderWidth=0.5f;
        //        cell.mainView.layer.cornerRadius=8.0;
        //        cell.mainView.layer.borderColor=[UIColor redColor].CGColor;
        
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 84.0;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"SectionHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    _textViewTXT=(UITextView*)[headerView viewWithTag:900];
    _textViewTXT.delegate=self;
    
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 84.0;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [_textViewTXT resignFirstResponder];
    StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
    [self presentViewController:statusScreen animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [_tableView_outlet reloadData];
    [_textViewTXT resignFirstResponder];
}

-(IBAction)cancelpresentViewBTN:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

@end
