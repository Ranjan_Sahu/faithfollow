//
//  SearchCommunityProfileView.h
//  wireFrameSplash
//
//  Created by home on 5/27/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WallView.h"
@interface SearchCommunityProfileView : UIViewController<MyFirstControllerDelegate>
-(IBAction)cancelSearchBtn:(id)sender;
@property(nonatomic,strong)NSMutableDictionary *friendProfileDict;;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *lbl_name;



@property (weak, nonatomic) IBOutlet UILabel *lbl_lbldescrip;

@property (weak, nonatomic) IBOutlet UILabel *lbl_createddate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ownername;


@property (weak, nonatomic) IBOutlet UIImageView *imageView_frnds;
@property (weak, nonatomic) IBOutlet UIButton *btn_addFrind;
- (IBAction)btn_addFrind:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_btnwall;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_wall;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_btnprofile;
- (IBAction)btn_profile:(id)sender;
-(IBAction)cancelpresentViewBTN:(id)sender;
- (IBAction)btn_wall:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btn_profile;
@property (weak, nonatomic) IBOutlet UIButton *btn_wall;

@property (weak, nonatomic) IBOutlet UIView *bckView1;

@property (weak, nonatomic) IBOutlet UIView *bckview3;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Mainname;

@property (weak, nonatomic) IBOutlet UIView *wallView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property(weak,nonatomic)IBOutlet UITableView *tableView_outlet;
@property(weak,nonatomic)IBOutlet UITextView  *textViewTXT;


@property (weak, nonatomic) IBOutlet UIView *profileFndView;
@property (weak, nonatomic) IBOutlet UIView *wallFndView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightofwallView;

@end
