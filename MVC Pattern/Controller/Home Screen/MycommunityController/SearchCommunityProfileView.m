//
//  SearchCommunityProfileView.m
//  wireFrameSplash
//
//  Created by home on 5/27/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#define  kCellName @"FeedTableViewCell"
#import "UIImageView+WebCache.h"
#import "ViewMycommunityViewController.h"
#import "WebServiceHelper.h"
#import "FeedTableViewCell.h"
#import "StatusViewController.h"
#import "FeedViewController.h"
#import "DeviceConstant.h"
#import "FriendListController.h"
#import "MWPhotoBrowser.h"
#import "SeeMoreCommentsViewController.h"
#import "FriendsProfileViewController.h"
#import "KLCPopup.h"
#import "SearchCommunityProfileView.h"

@interface SearchCommunityProfileView ()<MWPhotoBrowserDelegate>
{
    NSMutableArray *arrFeeds,*_quoteArray;
    NSMutableDictionary *userProfile;
    NSString *user_id;
    CGFloat heightofCell;
    NSInteger selectedRow;
    UIView *lineView ;
    UIView *navigationView;
    NSString *strimage ;
    KLCPopup *popPostUpdate;
    CGRect screenRect;
    WallView *wallvie;
    NSMutableArray *photos;
    MWPhotoBrowser *browser;
    NSString *commnityId;
}
@end

@implementation SearchCommunityProfileView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        screenRect = [[UIScreen mainScreen] bounds];
//        _heightofwallView.constant=screenRect.size.height/1.74;
        [self.view layoutIfNeeded];
        
        UIImage *listImage = [UIImage imageNamed:@"leftArrow"];
        UIButton *listButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [listButton setFrame:CGRectMake(10, 0, 30, 30)];
        [listButton setImage:listImage forState:UIControlStateNormal];
        [listButton  addTarget:self action:@selector(cancelBTN) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *infoButtonImage = [[UIBarButtonItem alloc]initWithCustomView:listButton ];
        self.navigationItem.leftBarButtonItem =infoButtonImage;
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [UIImage new];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0];
        self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
        self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2,20, 120, 44)];
        label.center = CGPointMake(self.view.frame.size.width/2, 22);
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"Avenir-Medium" size:20];
        label.textColor =[UIColor whiteColor];
        label.text=@"Profile";
        self.navigationItem.titleView =label;
        
        [self.tableView_outlet   registerNib:[UINib nibWithNibName:kCellName bundle:nil] forCellReuseIdentifier:kCellName];
        selectedRow=-1;
        arrFeeds=[[NSMutableArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Feed" ofType:@"plist"]];
        
//        _wallView.hidden=YES;
//        _bckView1.hidden=NO;
//        _bckview3.hidden=YES;
//        _profileFndView.backgroundColor=UIColorFromRGB(0xFFFFFF);
//        _lbl_btnwall.textColor=[UIColor colorWithRed:103/255.0f green:103/255.0f blue:103/255.0f alpha:1.0f];
//        _lbl_btnprofile.textColor=[UIColor colorWithRed:27/255.0f green:153/255.0f blue:217/255.0f alpha:1.0f];
//        _imageview_wall.image= [UIImage imageNamed:@"edit_unselected"];
//        _imageview_profile.image= [UIImage imageNamed:@"profile_selected-1"];
        
        if ( [_friendProfileDict  isKindOfClass:[NSNull class]]) {
        }else{
            
            userProfile=_friendProfileDict[@"get_community_owner"];
            NSString *ownerName=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",userProfile[@"Owner_first_name"],userProfile[@"Owner_last_name"]]]];
            
            if ([ownerName isEqualToString:@"(null) (null)"]) {
                _lbl_ownername.text=@"-";
            }else{
                _lbl_ownername.text=ownerName;
            }
            
            commnityId=[NSString stringWithFormat:@"%@",[_friendProfileDict valueForKey:@"id"]];
            NSString *desciption=_friendProfileDict[@"description"];
            if ([desciption isEqualToString:@""]) {
                _lbl_lbldescrip.text=@"-";
            }else{
                _lbl_lbldescrip.text=desciption;
            }
            
            _lbl_Mainname.text= [NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"community"]]];
            _lbl_name.text= [NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"community"]]];
            
//            _imageView_frnds.layer.cornerRadius=_imageView_frnds.frame.size.width/2;
//            _imageView_frnds.layer.borderWidth=0.5;
//            _imageView_frnds.layer.borderColor=[UIColor blackColor].CGColor;
//            _imageView_frnds.clipsToBounds=YES;
//            _imageView_frnds.image=[UIImage imageNamed:@"profile_image"];
//            _imageView_frnds.layer.cornerRadius=_imageView_frnds.frame.size.width/2;
//            _imageView_frnds.layer.borderWidth=0.5;
//            _imageView_frnds.layer.borderColor=[UIColor blackColor].CGColor;
//            _imageView_frnds.clipsToBounds=YES;
            
            strimage = [NSString stringWithFormat:@"%@%@", ImageapiUrl,_friendProfileDict[@"image_file"]];
            _lbl_createddate.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"created_at"]]];
            
            NSString *addStatus=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"add_status"]]];
            [_btn_addFrind setTitle:addStatus forState:UIControlStateNormal];
            
            if ([addStatus isEqualToString:@"Leave"]) {
                [_btn_addFrind setTitle:@"Leave Group" forState:UIControlStateNormal];
                [_btn_addFrind addTarget:self action:@selector(LeaveGruop) forControlEvents:UIControlEventTouchUpInside];
            }
            
            if ([addStatus isEqualToString:@"Send Request"]) {
                [_btn_addFrind setTitle:@"Send Request" forState:UIControlStateNormal];
                [_btn_addFrind addTarget:self action:@selector(SendRequestCommunity) forControlEvents:UIControlEventTouchUpInside];
            }
            
            [self imageLoad];
        }
    }
    
}

-(void)imageLoad{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_imageView_frnds sd_setImageWithURL:[NSURL URLWithString:strimage] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
    });
}

- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


-(void)viewWillLayoutSubviews{
    if (IS_IPHONE4||IS_IPHONE5||IS_IPHONE6||IS_IPHONE6PLUS) {
        //_scrollView.contentSize=CGSizeMake(_scrollView.frame.size.width, 600);
    }
}



-(void)SendRequestCommunity{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict = @{
                               @"user_id" : [[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"com_id" : commnityId,
                               };
//        NSLog(@"disct is%@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:join withParameters:dict withHud:YES success:^(id response){
//            NSLog(@"resposnse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==0){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.navigationController popViewControllerAnimated:YES];
                        });
                    }
                }
            }
            else{
                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                });
            }
            
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
         }];
    }
}









-(void)LeaveGruop{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
          NSDictionary *dict = @{
                               @"user_id" : [[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"com_id" : commnityId,
                               };
//        NSLog(@"disct is%@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:leave withParameters:dict withHud:YES success:^(id response){
//            NSLog(@"resposnse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.navigationController popToRootViewControllerAnimated:YES];
//                            [self.navigationController popViewControllerAnimated:YES];
                        });
                    }
                
            else{
                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                });
            }
                }
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
         }];
  }
}

-(void)cancelBTN{
 [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btn_addFrind:(id)sender {
    if ([_btn_addFrind.titleLabel.text isEqualToString:@"Join"]) {
//        NSLog(@"add");
        //   [self sendrequest :@"communitydetails"];
    }else{
        //   [self sendrequest :@"community/leave"];
//        NSLog(@"unadd");
    }
    //[self sendrequest :@"sendrequest"];
}

- (IBAction)btn_profile:(id)sender {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    _bckView1.hidden=NO;
    _bckview3.hidden=YES;
    _imageview_wall.image= [UIImage imageNamed:@"edit_unselected"];
    _imageview_profile.image= [UIImage imageNamed:@"profile_selected-1"];
    _lbl_btnwall.textColor=[UIColor colorWithRed:103/255.0f green:103/255.0f blue:103/255.0f alpha:1.0f];
    _lbl_btnprofile.textColor=[UIColor colorWithRed:27/255.0f green:153/255.0f blue:217/255.0f alpha:1.0f];
    _wallView.hidden=YES;
    _secondView.hidden=NO;
    _bckView1.backgroundColor=UIColorFromRGB(0x009AD9);
    //    _profileFndView.layer.borderColor = [UIColor clearColor].CGColor;
    //    _wallFndView.layer.borderColor = [UIColor grayColor].CGColor;
    //    _wallFndView.layer.borderWidth = 1.0f;
    //
    //    _profileFndView.backgroundColor=UIColorFromRGB(0xFFFFFF);
    //    _wallFndView.backgroundColor=UIColorFromRGB(0xF6F6F6);
    _profileFndView.backgroundColor=UIColorFromRGB(0xFFFFFF);
    _wallFndView.backgroundColor=UIColorFromRGB(0xF6F6F6);
    }
    
    
}



- (IBAction)btn_wall:(id)sender {
    
    //    NSDictionary *eventLocation = @{@"wallfor": @"myself",@"userid":_friendProfileDict[@"id"],@"friendWall":@"confirmedfriend",@"post_option":_friendProfileDict[@"usersprofile"][@"post_option"]};
    //    //NSDictionary *eventLocation = @{@"wallfor": @"myself"};
    //    wallvie=[[WallView alloc]initWithframe:CGRectMake(0, 0, 50, 50) andUserInfo:eventLocation];
    //    wallvie.frame= CGRectMake(0, 0,_wallView.frame.size.width,_wallView.frame.size.height);
    //    wallvie.delegate1=self;
    //    [wallvie webservice_call];
    //    //  [wallvie webservice_call];
    //    [_wallView addSubview:wallvie];
    //
    //
    //    _bckView1.hidden=YES;
    //    _bckview3.hidden=NO;
    //    _imageview_wall.image= [UIImage imageNamed:@"edit_selected"];
    //    _imageview_profile.image= [UIImage imageNamed:@"profile_unselected-1"];
    //    _lbl_btnprofile.textColor=[UIColor colorWithRed:103/255.0f green:103/255.0f blue:103/255.0f alpha:1.0f];
    //    _lbl_btnwall.textColor=[UIColor colorWithRed:27/255.0f green:153/255.0f blue:217/255.0f alpha:1.0f];
    //    _wallView.hidden=NO;
    //    _secondView.hidden=YES;
    //    FeedViewController *feed=[self.storyboard instantiateViewControllerWithIdentifier:@"FeedViewController"];
    //    //_wallView=feed.view;
    //    [_wallView addSubview:feed.view];
    //    _bckview3.backgroundColor=UIColorFromRGB(0x009AD9);
    //
    //    _wallFndView.backgroundColor=UIColorFromRGB(0xFFFFFF);
    //    _profileFndView.backgroundColor=UIColorFromRGB(0xF6F6F6);
    //
    //
    ////    _profileFndView.layer.borderColor = [UIColor grayColor].CGColor;
    ////    _profileFndView.layer.borderWidth = 1.0f;
    ////    _wallFndView.layer.borderColor = [UIColor clearColor].CGColor;
    ////    _wallFndView.layer.borderWidth = 1.0f;
    ////
    //
    ////    _profileFndView.backgroundColor=UIColorFromRGB(0xF6F6F6);
    ////    _wallFndView.backgroundColor=UIColorFromRGB(0xFFFFFF);
    
    
}
-(void)sendrequest :(NSString*)methodName{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"com_id" : user_id,
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
//        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                NSMutableArray *messageArray;
                if([response[@"status"]intValue ] ==1){
                    
                    messageArray=[response valueForKey:@"message"];
                    
                }else{
                    
                    messageArray=[response valueForKey:@"message"];
                    
                    
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                });
            }
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
    }
}

#pragma mark TableView Data Source methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrFeeds count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName forIndexPath:indexPath];
    [self setUpCell:cell atIndexPath:indexPath];
    
    if(indexPath.row == selectedRow)
    {
        NSArray *arr=[arrFeeds objectAtIndex:indexPath.row][@"comments"];
        [cell.commentsView refereshData:arr];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static FeedTableViewCell *cell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cell = [self.tableView_outlet dequeueReusableCellWithIdentifier:kCellName];
    });
    [self setUpCell:cell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:cell];
}
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsUpdateConstraints];
    [sizingCell updateConstraintsIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}
- (void)setUpCell:(FeedTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    float width=[UIScreen mainScreen].bounds.size.width-(20);
    cell.com_lbl.preferredMaxLayoutWidth = width; // This is necessary to allow the label
    NSDictionary *dict=[arrFeeds objectAtIndex:indexPath.row];
    cell.com_lbl.text=dict[@"desc"];
    [cell.comments_btn addTarget:self action:@selector(comments_act:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(indexPath.row == selectedRow)
    {
        NSArray *arr=dict[@"comments"];
        float  height=([arr count]*60);
        cell.commentsView.hidden=NO;
        cell.heightConsatraint.constant=height+72;
        cell.commentsView.tblViewHeight.constant=height;
        cell.mainView.layer.borderWidth=0.0f;
        
        //        cell.mainView.layer.borderWidth=0.5f;
        //        cell.mainView.layer.cornerRadius=8.0;
        //        cell.mainView.layer.borderColor=[UIColor redColor].CGColor;
        //
        //        cell.commentsView.layer.borderWidth=0.5f;
        //        cell.commentsView.layer.cornerRadius=8.0;
        //        cell.commentsView.layer.borderColor=[UIColor redColor].CGColor;
    }
    else{
        cell.heightConsatraint.constant=0;
        cell.commentsView.hidden=YES;
        cell.commentsView.tblViewHeight.constant=0;
        //        cell.mainView.layer.borderWidth=0.5f;
        //        cell.mainView.layer.cornerRadius=8.0;
        //        cell.mainView.layer.borderColor=[UIColor redColor].CGColor;
        
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 84.0;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"SectionHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    _textViewTXT=(UITextView*)[headerView viewWithTag:900];
    _textViewTXT.delegate=self;
    
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 84.0;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [_textViewTXT resignFirstResponder];
    StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
    [self presentViewController:statusScreen animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [_tableView_outlet reloadData];
    [_textViewTXT resignFirstResponder];
}

-(IBAction)cancelpresentViewBTN:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}
@end
