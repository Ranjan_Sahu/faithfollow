//
//  PageContentViewController.m
//  PageViewDemo
//
//  Created by Simon on 24/11/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "PageContentViewController.h"
#import "CustomTableViewCell.h"
#import "FriendsProfileViewController.h"
#import "friendProfilrTableViewCell.h"
#import "MycommunityTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "ViewMycommunityViewController.h"
#import "CreateCommunityViewController.h"
#import "FriendListController.h"
#import "ProfileFriendViewController.h"
#import "FriendsProfileViewController.h"
#import "myinfluencerCell.h"
#import "InfluencerProfileController.h"
#import "Mixpanel.h"



@interface PageContentViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *request_dataArray;
    NSMutableDictionary *friendsdataDict;
    NSString *kCellName;
    NSMutableArray *messageArray;
    NSUInteger _dataCount;
    NSArray *dataArray;
    NSString *imageProfile;
    
}

@end

@implementation PageContentViewController
@synthesize callService;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _greenView.hidden=YES;
    
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    self.tableviewoutlet.tableFooterView = footerView;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   
        if (_pageIndex==2) {
            [[NSUserDefaults standardUserDefaults] setObject:@"pageIndex" forKey:@"pageIndexZero"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            _button_bottomConstraint.constant=-50;
            [self.view layoutIfNeeded];
            //        [self sendRequest:recievedrequest andfriend_id:nil];
            [self followerMethod];
            [self.tableviewoutlet registerNib:[UINib nibWithNibName:@"MycommunityTableViewCell" bundle:nil] forCellReuseIdentifier:@"MycommunityTableViewCell"];
            
        }else if (_pageIndex==1){
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"pageIndexZero"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            _button_bottomConstraint.constant=0;
            [self.view layoutIfNeeded];
            [self influencerMethod];
            //        [self sendRequest:friendslist andfriend_id:nil];
            [self.tableviewoutlet registerNib:[UINib nibWithNibName:@"MycommunityTableViewCell" bundle:nil] forCellReuseIdentifier:@"MycommunityTableViewCell"];
            
        }
    
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
        [[NSNotificationCenter defaultCenter]removeObserver:self];
//    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    if (getVar.checkDisapper) {
//        [[AppDelegate sharedAppDelegate] addsView];
//        //        getVar.checkDisapper=NO;
//    }
    
}





-(void)followerMethod{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict=nil;
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:followers withParameters:dict withHud:YES success:^(id response){
            
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            request_dataArray=[response valueForKey:@"result"];
                            [_tableviewoutlet reloadData];
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_tableviewoutlet reloadData];
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
        
    }
  
}

-(void)influencerMethod{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        NSDictionary *dict=nil;
       
            dict = @{
                    @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                    @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                     };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:Following withParameters:dict withHud:YES success:^(id response){
            
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            request_dataArray=[response valueForKey:@"result"];
                            [_tableviewoutlet reloadData];
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_tableviewoutlet reloadData];
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
        
    }
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark TableView Data Source methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [request_dataArray count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MycommunityTableViewCell *cell = (MycommunityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MycommunityTableViewCell" forIndexPath:indexPath];
    NSMutableDictionary *arrayDict=[request_dataArray objectAtIndex:indexPath.row];
    
    if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
        //        NSArray *getobjectAarry=[arrayDict valueForKey:@"get_friend_details"];
        //        NSDictionary *getobjectDict=[getobjectAarry objectAtIndex:0];
        NSArray *getFrindArray=[[NSArray alloc]init];
        NSArray *getuserArray=[[NSArray alloc]init];
        getFrindArray=[arrayDict valueForKey:@"get_friend_details"];
        getuserArray=[arrayDict valueForKey:@"get_user_details"];
        
        if ([getFrindArray isKindOfClass:[NSArray class]]) {
            if ([getFrindArray count]!=0) {
                NSDictionary *getFriendDict=[getFrindArray objectAtIndex:0];
                
                if ([getFriendDict isKindOfClass:[NSDictionary class]]) {
                    
                    cell.lbl_name.text=[NSString stringWithFormat:@"%@ %@",[getFriendDict valueForKey:@"first_name"],[getFriendDict valueForKey:@"last_name"]];
                    NSMutableDictionary *usersprofile=getFriendDict[@"usersprofile"];
                    NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[usersprofile valueForKey:@"profile_picture"]];
                    NSURL *imageURL = [NSURL URLWithString:str];
                    [cell.imageView_profile sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
                    NSString *checkUserId=[NSString stringWithFormat:@"%@",[getFriendDict valueForKey:@"id"]];
                    
                    if ([checkUserId isEqualToString:@"1"]|| [checkUserId isEqualToString:@"2"]) {
                        cell.btn_viewprofile.hidden=YES;
                    }else{
                        cell.btn_viewprofile.hidden=NO;
                    }
                }
            }
            
        }
        
        if ([getuserArray isKindOfClass:[NSArray class]]) {
            if ([getuserArray count]!=0) {
                NSDictionary *getuser_detail=[getuserArray objectAtIndex:0];
                
                if ([getuser_detail isKindOfClass:[NSDictionary class]]) {
                    
                    cell.lbl_name.text=[NSString stringWithFormat:@"%@ %@",[getuser_detail valueForKey:@"first_name"],[getuser_detail valueForKey:@"last_name"]];
                    NSMutableDictionary *usersprofile=getuser_detail[@"usersprofile"];
                    NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[usersprofile valueForKey:@"profile_picture"]];
                    NSURL *imageURL = [NSURL URLWithString:str];
                    [cell.imageView_profile sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
                    
                    NSString *checkUserId=[NSString stringWithFormat:@"%@",[getuser_detail valueForKey:@"id"]];
                    if ([checkUserId isEqualToString:@"1"]|| [checkUserId isEqualToString:@"2"]) {
                        cell.btn_viewprofile.hidden=YES;
                    }else{
                        cell.btn_viewprofile.hidden=NO;
                    }
                }
            }
            
        }
        
        cell.btn_viewprofile.tag=indexPath.row;
        NSString *frindStatus;
        frindStatus=[arrayDict valueForKey:@"friend_status"];
        
        if ([frindStatus isEqualToString:@"Follow"]) {
            [cell.btn_viewprofile setTitle:@"Follow" forState:UIControlStateNormal];
            [cell.btn_viewprofile setBackgroundColor:[UIColor whiteColor]];
            [cell.btn_viewprofile setTitleColor:[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            cell.btn_viewprofile.layer.borderWidth = 1.0;
            cell.btn_viewprofile.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0].CGColor;
        }
        else{
//            [cell.btn_viewprofile setTitle:@"UnFollow" forState:UIControlStateNormal];
//            [cell.btn_viewprofile setBackgroundColor:[UIColor orangeColor]];
//            [cell.btn_viewprofile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            cell.btn_viewprofile.layer.borderWidth = 1.0;
//            cell.btn_viewprofile.layer.borderColor = [UIColor orangeColor].CGColor;
            [cell.btn_viewprofile setTitle:@"Followed" forState:UIControlStateNormal];
            [cell.btn_viewprofile setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0]];
            [cell.btn_viewprofile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            cell.btn_viewprofile.layer.borderWidth = 1.0;
            cell.btn_viewprofile.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0].CGColor;
        }
        
        NSString *checkis_influencer=[arrayDict valueForKey:@"is_influencer"];
        if ([checkis_influencer isEqualToString:@"0"]) {
            cell.influencerImage.hidden=YES;
        }else{
            cell.influencerImage.hidden=NO;
        }
        
        //            cell.lbl_name.numberOfLines = 1;
        //            cell.lbl_name.minimumScaleFactor = 0.5;
        //            cell.lbl_name.adjustsFontSizeToFitWidth = YES;
        
        // add tap geture to show profile of user
        cell.imageView_profile.tag=indexPath.row;
        cell.imageView_profile.userInteractionEnabled=YES;
        UITapGestureRecognizer *imagegallery_open = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageviewProfileAction:)];
        [cell.imageView_profile addGestureRecognizer:imagegallery_open];
        
        //add tapgesture to show profile
        cell.lbl_name.tag = indexPath.row;
        cell.lbl_name.userInteractionEnabled=YES;
        UITapGestureRecognizer *profilename = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilenameTaped:)];
        [cell.lbl_name addGestureRecognizer:profilename];
        [cell.btn_viewprofile addTarget:self action:@selector(fellowMethod:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return cell;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

// taped image and profile name to  show profile of user

- (void)imageviewProfileAction:(UITapGestureRecognizer*)sender {
    NSMutableDictionary *arrayProfileDict=[request_dataArray objectAtIndex:sender.view.tag];
    NSString *frindID;
    NSArray *getFrindArray=[[NSArray alloc]init];
    NSArray *getuserArray=[[NSArray alloc]init];
    getFrindArray=[arrayProfileDict valueForKey:@"get_friend_details"];
    getuserArray=[arrayProfileDict valueForKey:@"get_user_details"];
    
    if ([getFrindArray isKindOfClass:[NSArray class]]) {
        if ([getFrindArray count]!=0) {
            NSDictionary *getFriendDict=[getFrindArray objectAtIndex:0];
            if ([getFriendDict isKindOfClass:[NSDictionary class]]) {
                NSDictionary *userprofileDict=[getFriendDict valueForKey:@"usersprofile"];
                if ([userprofileDict isKindOfClass:[NSDictionary class]]) {
                    frindID=[userprofileDict valueForKey:@"user_id"];
                    NSString  *influencerchek=[getFriendDict valueForKey:@"user_type"];
                    NSString *indexPath=[NSString stringWithFormat:@"%ld",sender.view.tag];
              [self viewFrndProfile:frindID indexpath:indexPath influencerchek:influencerchek];                }
                
            }
        }
        
    }
    
    if ([getuserArray isKindOfClass:[NSArray class]]) {
        if ([getuserArray count]!=0) {
            NSDictionary *getuser_detail=[getuserArray objectAtIndex:0];
            if ([getuser_detail isKindOfClass:[NSDictionary class]]) {
                NSDictionary *userprofileDict=[getuser_detail valueForKey:@"usersprofile"];
                if ([userprofileDict isKindOfClass:[NSDictionary class]]) {
                    frindID=[userprofileDict valueForKey:@"user_id"];
                    NSString  *influencerchek=[getuser_detail valueForKey:@"user_type"];
                    NSString *indexPath=[NSString stringWithFormat:@"%ld",sender.view.tag];
            [self viewFrndProfile:frindID indexpath:indexPath influencerchek:influencerchek];                }
                
                
            }
        }
        
    }

}

- (void)profilenameTaped:(UITapGestureRecognizer*)sender {
    NSMutableDictionary *arrayProfileDict=[request_dataArray objectAtIndex:sender.view.tag];
    NSString *frindID;
        NSArray *getFrindArray=[[NSArray alloc]init];
        NSArray *getuserArray=[[NSArray alloc]init];
        getFrindArray=[arrayProfileDict valueForKey:@"get_friend_details"];
        getuserArray=[arrayProfileDict valueForKey:@"get_user_details"];
    
        if ([getFrindArray isKindOfClass:[NSArray class]]) {
            if ([getFrindArray count]!=0) {
                NSDictionary *getFriendDict=[getFrindArray objectAtIndex:0];
                if ([getFriendDict isKindOfClass:[NSDictionary class]]) {
                NSDictionary *userprofileDict=[getFriendDict valueForKey:@"usersprofile"];
                    if ([userprofileDict isKindOfClass:[NSDictionary class]]) {
                        frindID=[userprofileDict valueForKey:@"user_id"];
                     NSString  *influencerchek=[getFriendDict valueForKey:@"user_type"];

                NSString *indexPath=[NSString stringWithFormat:@"%ld",sender.view.tag];
                        [self viewFrndProfile:frindID indexpath:indexPath influencerchek:influencerchek];
                        
                        
                    }
                   
                }
            }
    
        }
    
        if ([getuserArray isKindOfClass:[NSArray class]]) {
            if ([getuserArray count]!=0) {
                NSDictionary *getuser_detail=[getuserArray objectAtIndex:0];
                if ([getuser_detail isKindOfClass:[NSDictionary class]]) {
                NSDictionary *userprofileDict=[getuser_detail valueForKey:@"usersprofile"];
                    if ([userprofileDict isKindOfClass:[NSDictionary class]]) {
                        frindID=[userprofileDict valueForKey:@"user_id"];
                        NSString  *influencerchek=[getuser_detail valueForKey:@"user_type"];
                        NSString *indexPath=[NSString stringWithFormat:@"%ld",sender.view.tag];
                        [self viewFrndProfile:frindID indexpath:indexPath influencerchek:influencerchek];
                    }
               

                }
            }
            
        }
   
}





-(void)fellowMethod:(UIButton*)sender{
    
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        CGPoint center= sender.center;
        CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableviewoutlet];
        NSIndexPath *indexPathvalue = [self.tableviewoutlet indexPathForRowAtPoint:rootViewPoint];
        NSMutableDictionary *arrayDict=[request_dataArray objectAtIndex:indexPathvalue.row];
              
        NSString *frindID,*follower_Name;
        NSArray *getFrindArray=[[NSArray alloc]init];
        NSArray *getuserArray=[[NSArray alloc]init];
        getFrindArray=[arrayDict valueForKey:@"get_friend_details"];
        getuserArray=[arrayDict valueForKey:@"get_user_details"];
        
        if ([getFrindArray isKindOfClass:[NSArray class]]) {
            if ([getFrindArray count]!=0) {
                NSDictionary *getFriendDict=[getFrindArray objectAtIndex:0];
                if ([getFriendDict isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *userprofileDict=[getFriendDict valueForKey:@"usersprofile"];
                    if ([userprofileDict isKindOfClass:[NSDictionary class]]) {
                        frindID=[userprofileDict valueForKey:@"user_id"];
                        follower_Name=[NSString stringWithFormat:@"%@ %@",[userprofileDict valueForKey:@"first_name"],[userprofileDict valueForKey:@"last_name"]];
                    }
                    
                }
            }
            
        }
        
        if ([getuserArray isKindOfClass:[NSArray class]]) {
            if ([getuserArray count]!=0) {
                NSDictionary *getuser_detail=[getuserArray objectAtIndex:0];
                if ([getuser_detail isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *userprofileDict=[getuser_detail valueForKey:@"usersprofile"];
                    if ([userprofileDict isKindOfClass:[NSDictionary class]]) {
                        frindID=[userprofileDict valueForKey:@"user_id"];
                         follower_Name=[NSString stringWithFormat:@"%@ %@",[userprofileDict valueForKey:@"first_name"],[userprofileDict valueForKey:@"last_name"]];
                    }
                    
                    
                }
            }
            
        }
        NSDictionary *dict=nil;
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 @"friend_id" : frindID,

                 };
        NSString *methodname=nil;
//        NSLog(@"The button title is %@",sender.titleLabel.text);
         UIButton *btn = (UIButton*)sender;
        if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
            methodname=follow;
        }
        else{
            methodname=unfollow;
  
        }
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodname withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
//                         [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
                                // Follow mixpanel
                            NSString *currentDate=[Constant1 createTimeStamp];
      [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Number of follows" by:@1];
                                [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last follow":currentDate}];
                                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Date of last follow":currentDate}];
                                
                                [ [AppDelegate sharedAppDelegate].mxPanel track:@"Follow"
                                                                     properties:@{@"Influencer?":[NSNumber numberWithBool:false],@"Date of last follow":currentDate,@"Following":follower_Name}];
                                
                                NSMutableDictionary *arrayDict=[request_dataArray objectAtIndex:indexPathvalue.row];
                                if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
                                [arrayDict setObject:@"Unfollow" forKey:@"friend_status"];

                                }
                                
                            }
                            else{
                                 [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Number of follows" by:@-1];
                                NSMutableDictionary *arrayDict=[request_dataArray objectAtIndex:indexPathvalue.row];
                                if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
                                [arrayDict setObject:@"Follow" forKey:@"friend_status"];

                                }
                            }

                            [self reloadCellCom:indexPathvalue.row];
                        });
                        
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_tableviewoutlet reloadData];
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
        
    }
    
}

-(void)reloadCellCom:(NSUInteger)cell_index{
    
    [self.tableviewoutlet beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cell_index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableviewoutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [self.tableviewoutlet endUpdates];
    
}
- (void)handleSingleTapGestureName:(UITapGestureRecognizer *)tapgesture {
    
    NSString *friendID = [NSString stringWithFormat:@"%ld",tapgesture.view.tag];
    
}

- (void)handleSingleTapGestureName1:(UITapGestureRecognizer *)tapgesture {
    NSString *friendID = [NSString stringWithFormat:@"%ld",tapgesture.view.tag];
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *arrayDict=[request_dataArray objectAtIndex:indexPath.row];
    NSMutableArray *dictArray=[arrayDict valueForKey:@"get_friend_details"];
    NSMutableArray *dictArray1=[arrayDict valueForKey:@"get_user_details"];
    if ([dictArray count]!=0) {
        NSDictionary *dict=[dictArray objectAtIndex:0];
        NSMutableDictionary *usersprofile=dict[@"usersprofile"];
//        [self viewFrndProfile:usersprofile[@"user_id"]];
    }
    
    if ([dictArray1 count]!=0) {
        NSDictionary *dict1=[dictArray1 objectAtIndex:0];
        NSMutableDictionary *usersprofile=dict1[@"usersprofile"];
//        [self viewFrndProfile:usersprofile[@"user_id"]];
    }
}

-(void)viewFrndProfile :(NSString*)friend_id indexpath:(NSString*)indexPath influencerchek:(NSString *)influencerData{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : friend_id,
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if([response[@"status"]intValue ] ==1){
                friendsdataDict=[response valueForKey:@"data"];
                if ([friendsdataDict isKindOfClass:[NSMutableDictionary class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSUserDefaults standardUserDefaults] setObject:@"directProfile" forKey:@"directProfile"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",friendsdataDict[@"first_name"],friendsdataDict[@"last_name"]]]];
                        
                        NSDictionary *dictUser=[friendsdataDict valueForKey:@"usersprofile"];
                        NSString *profile_picture=[dictUser valueForKey:@"profile_picture"];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:nameTxt forKey:@"mainName"];
                        [[NSUserDefaults standardUserDefaults]setObject:profile_picture forKey:@"profile_pictureStr"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                   NSMutableDictionary *arrayProfileDict=[request_dataArray objectAtIndex:[indexPath intValue]];
                     
//                        NSString *checkFluencer;
                        if ([arrayProfileDict isKindOfClass:[NSMutableDictionary class]]) {
//                            checkFluencer=[arrayProfileDict valueForKey:@"is_influencer"];
                            if ([influencerData isEqualToString:@"0"]) {
                            ProfileFriendViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileFriendViewController"];
                                allprofileView.friendProfileDictProfile=friendsdataDict;
                                allprofileView.postFriendProfileDict=friendsdataDict;
                                [self.navigationController pushViewController:allprofileView animated:YES];
                            }
                            else{
                            InfluencerProfileController *InfluencerProfileControllerView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                                InfluencerProfileControllerView.influencerDict=friendsdataDict;
                        [self.navigationController pushViewController:InfluencerProfileControllerView animated:YES];

                            
                            }
                            
                        }
                        
//                        InfluencerProfileController *allprofileViewpage=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
//                        allprofileViewpage.influencerDict=friendsdataDict;
////                        [self presentViewController:allprofileViewpage animated:YES completion:nil];
//                         [self.navigationController pushViewController:allprofileViewpage animated:NO];
                        
                        
                    });
                    
                    
                }
                
                
            }
            else
            {
                messageArray=[response valueForKey:@"message"];
                if ([messageArray count]>0) {
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    return;
                    
                }
            }
            
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
    }
    
}




- (IBAction)button_act:(id)sender {
}
#pragma mark add And Cancel Friend methods
#pragma mark add And Cancel Friend methods
-(void)acceptFriend:(UIButton*)sender{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
     [TenjinSDK sendEventWithName:@"AccceptFriendRequest_Tapped"];
    [FBSDKAppEvents logEvent:@"AccceptFriendRequest_Tapped"];
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"AccceptFriendRequest_Tapped"]];
    NSDictionary *dict=[request_dataArray objectAtIndex:sender.tag];
    NSMutableArray *dictArray=[dict valueForKey:@"get_friend_details"];
    NSMutableArray *dictArray1=[dict valueForKey:@"get_user_details"];
    
    
    if ([dictArray count]!=0) {
        NSDictionary *dict=[dictArray objectAtIndex:0];
        NSDictionary *userDict=[dict valueForKey:@"usersprofile"];
        [self sendAcceptRequest:acceptrequest anduserID:[userDict valueForKey:@"user_id"]];
        
        
    }
    else if ([dictArray1 count]!=0) {
        NSDictionary *dict=[dictArray1 objectAtIndex:0];
        NSDictionary *userDict=[dict valueForKey:@"usersprofile"];
        [self sendAcceptRequest:acceptrequest anduserID:[userDict valueForKey:@"user_id"]];
        
    }
    }
 
    
}
-(void)cancelFriend:(UIButton*)sender{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    NSDictionary *dict=[request_dataArray objectAtIndex:sender.tag];
    NSMutableArray *dictArray=[dict valueForKey:@"get_friend_details"];
    NSMutableArray *dictArray1=[dict valueForKey:@"get_user_details"];
    if ([dictArray count]!=0) {
        NSDictionary *dict=[dictArray objectAtIndex:0];
        NSDictionary *userDict=[dict valueForKey:@"usersprofile"];
        [self sendCancealRequest:rejectrequest anduserID:[NSString stringWithFormat:@"%@",[userDict valueForKey:@"user_id"]]];
        
    }
    else if ([dictArray1 count]!=0) {
        NSDictionary *dict=[dictArray1 objectAtIndex:0];
        NSDictionary *userDict=[dict valueForKey:@"usersprofile"];
        [self sendCancealRequest:rejectrequest anduserID:[NSString stringWithFormat:@"%@",[userDict valueForKey:@"user_id"]]];
        
    }
   
    }
    
    
}

-(void)sendCancealRequest:(NSString*)methodName   anduserID:(NSString*)user_Id{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id":user_Id,
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    messageArray=[response valueForKey:@"message"];
                }else{
                    messageArray=[response valueForKey:@"message"];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([messageArray count]>0) {
                     [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    }
                    [_tableviewoutlet reloadData];
                    
                });
            }
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
    }
}


-(void)sendAcceptRequest:(NSString*)methodName   anduserID:(NSString*)user_Id{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id":user_Id,
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
//        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    messageArray=[response valueForKey:@"message"];
                }else{
                    messageArray=[response valueForKey:@"message"];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([messageArray count]>0) {
                       [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    }
                    [self sendRequest:recievedrequest andfriend_id:nil];
                    
                });
            }
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
        
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{if (buttonIndex == 0&&alertView.tag==12)
{
    
}
}


//-(void)viewProfile:(UIButton*)sender{
//    NSDictionary *dict=[request_dataArray objectAtIndex:sender.tag];
//    [self viewCommunityProfile:dict[@"id"]];
//}


#pragma mark add And Cancel Friend methods
-(void)sendRequest :(NSString*)methodName andfriend_id:(NSString*)friend_id{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        //[self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    NSDictionary *dict=nil;
    if (friend_id) {
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 @"friend_id" : friend_id,
                 };
    }else{
        dict = @{
                 @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 };
    }
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:influencerList withParameters:dict withHud:YES success:^(id response){
//        NSLog(@"resposnse is%@",response);
        
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    if ([methodName isEqualToString:@"recievedrequest"]) {
                        request_dataArray=[response valueForKey:@"data"];
                        
                    }else{
                        request_dataArray=[response valueForKey:@"result"];;
                        if (request_dataArray.count==0) {
                            messageArray=[response valueForKey:@"message"];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [_tableviewoutlet reloadData];
                                if ([messageArray count]>0) {
                                  [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                                }
                                
                            });
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_tableviewoutlet reloadData];
                    });
                }else{
                    [request_dataArray removeAllObjects];
                    messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_tableviewoutlet reloadData];
                        if ([messageArray count]>0) {
                             [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        }
                       
                        
                    });
                }
                
            }
            
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
        
    }
}


-(void)viewCommunityProfile :(NSString*)community_id{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    FriendListController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendListController"];
    allprofileView.friendsdataDict=friendsdataDict;
    [self addChildViewController:allprofileView];
    [self.view addSubview:allprofileView.view];
    
    
    
    
    //    NSDictionary *dict = @{
    //                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
    //                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
    //                           @"id":community_id
    //                           };
    //
    //    NSLog(@"disct is%@",dict);
    //    [[WebServiceHelper sharedInstance] callPostDataWithMethod:communityDisplay withParameters:dict withHud:YES success:^(id response){
    //        NSLog(@"resposnse is%@",response);
    //        if ([response isKindOfClass:[NSDictionary class]]){
    //            if (![response isKindOfClass:[NSNull class]]) {
    //                if([response[@"status"]intValue ] ==1){
    //                    request_dataArray=[[NSMutableArray alloc]init];
    //                    messageArray=[[NSMutableArray alloc]init];
    //                    friendsdataDict=[response valueForKey:@"data"];
    //                    if (friendsdataDict) {
    //                        dispatch_async(dispatch_get_main_queue(), ^{
    //
    ////                            ViewMycommunityViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewMycommunityViewController"];
    ////                            allprofileView.friendProfileDict=friendsdataDict;
    ////                            [self addChildViewController:allprofileView];
    ////                            [self.view addSubview:allprofileView.view];
    //
    //                FriendListController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendListController"];
    //                            allprofileView.friendsdataDict=friendsdataDict;
    //                            [self addChildViewController:allprofileView];
    //                            [self.view addSubview:allprofileView.view];
    //
    //
    //                        });
    //                    }
    //                }
    //            }else{
    //
    //                messageArray=[response valueForKey:@"message"];
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
    //                });
    //            }
    //        }
    //    } errorBlock:^(id error)
    //     {
    //         NSLog(@"error");
    //
    //     }];
    //
    }
    
}
-(void)createCommunity{
    
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"name":@"fine Community",
                           @"description":@"this community id health",
                           
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:createCommunity withParameters:dict withHud:YES success:^(id response){
        messageArray=nil;
//        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            
            if([response[@"status"]intValue ] ==1){
                if ([[response valueForKey:@"message"] isKindOfClass:[NSArray class]]) {
                    messageArray=[response valueForKey:@"message"];
                    
                    //                    [_textViewTXT resignFirstResponder];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        CreateCommunityViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"CreateCommunityViewController"];
                        [self presentViewController:statusScreen animated:YES completion:nil];
                    });
                }else{
                    NSString *str=[response valueForKey:@"message"];
                    [messageArray addObject:str];
                }
            }else{
                if ([[response valueForKey:@"message"] isKindOfClass:[NSArray class]]) {
                    messageArray=[response valueForKey:@"message"];
                }else{
                    NSString *str=[response valueForKey:@"message"];
                    [messageArray addObject:str];
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([messageArray count]>0) {
                   [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                }
               
                
            });
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
    
    
}
-(void)deleteCommunity:(NSString*)communityId{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"id":@"164",
                           
                           };
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate sendCommunityRequset:@"community-destroy" andParadict
                                     :dict];
}
-(void)communityDisplay:(NSString*)communityId{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"id":@"164",
                           
                           };
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate sendCommunityRequset:@"community-display" andParadict
                                     :dict];
    
}
-(void)communityposttype{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate sendCommunityRequset:@"community-post-type" andParadict
                                     :nil];
    
}


-(void)joinCommunity:(NSString*)communityId{
    NSDictionary *dict = @{
                           @"com_id":@"164",
                           @"user_id" :@"246",
                           };
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate sendCommunityRequset:@"communitydetails"  andParadict
                                     :dict];
}
-(void)communityleave:(NSString*)communityId{
    NSDictionary *dict = @{
                           @"com_id":@"164",
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           };
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate sendCommunityRequset:@"community/leave"  andParadict
                                     :dict];
}
-(void)communityreject:(NSString*)communityId{
    NSDictionary *dict = @{
                           @"com_id":@"164",
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           };
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate sendCommunityRequset:@"community/reject"  andParadict
                                     :dict];
}
-(void)communitydetailsapprove
:(NSString*)communityId{
    NSDictionary *dict = @{
                           @"com_id":@"166",
                           @"user_id" :@"246",
                           };
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate sendCommunityRequset:@"community/approve"  andParadict
                                     :dict];
}
-(void)displaycommunitywall:(NSString*)communityId{
    NSDictionary *dict = @{
                           @"com_id":@"164",
                           
                           };
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate sendCommunityRequset:@"communitydetails/reject"  andParadict
                                     :dict];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    return pageViewController;
    
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    return pageViewController;
    
}


- (IBAction)btn_addCommunity:(id)sender {
    [self createCommunity];
    
    
}
- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}


@end
