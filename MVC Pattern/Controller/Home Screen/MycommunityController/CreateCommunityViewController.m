//
//  CreateCommunityViewController.m
//  wireFrameSplash
//
//  Created by Vikas on 20/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "CreateCommunityViewController.h"
#import "KLCPopup.h"

@interface CreateCommunityViewController (){
    KLCPopup *popUpView;
    UIImage* chosenImage;
    
}
@end

@implementation CreateCommunityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    
    
    //    _txtview_descrip.layer.borderWidth=0.5f;
    //    _txtview_descrip.layer.borderColor=[UIColor lightGrayColor].CGColor;
    //      _txtview_descrip.clipsToBounds=YES;
    
    
    
    
    
    
    _txtview_descrip.textColor = [UIColor blackColor];
    _txtview_descrip.text = @"Enter comments";
    _view_popup.hidden=YES;
    
    
    _imageview_chosenIMage.layer.cornerRadius=(_imageview_chosenIMage.frame.size.width/2);
    _imageview_chosenIMage.clipsToBounds=YES;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    _lbl_name.text=@"Name";
    _lbl_description.text=@"Description";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)bnt_cancel:(id)sender {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btn_photo:(id)sender {
    
    
    
    _view_popup.hidden=NO;
    popUpView=[KLCPopup popupWithContentView:_view_popup];
    [popUpView show];
}

-(NSString*)base64String:(UIImage*)image{
    NSString *base64EncodeStr = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    base64EncodeStr = [base64EncodeStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    return base64EncodeStr;
}

- (IBAction)btn_submit:(id)sender {
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }
    else {
        NSString *imagestr1=[self base64String:chosenImage];
        if (imagestr1==nil) {
            imagestr1=@"";
        }
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"name" :self.txtfield_name.text,
                               @"description" :self.txtview_descrip.text,
                               @"image_file" :imagestr1,
                               
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"community" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([response[@"message"] isKindOfClass:[NSString class]])
                            {
                                NSString *message=response[@"message"];
                                [self.view makeToast:message duration:1.0 position:CSToastPositionCenter];
                                
                            }
                            else
                            {
                                NSMutableArray *messageArray=response[@"message"];
                                [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                                
                            }
                        });
                        
                    }
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
    
    
}

- (IBAction)tap:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)btn_popViewcancel:(id)sender {
    _view_popup.hidden=YES;
}

- (IBAction)bnt_camera:(id)sender {
    [self camera];
}

- (IBAction)btn_gallery:(id)sender {
    [self gallery];
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    textView.text = @"";
    textView.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(textView.text.length == 0){
        textView.textColor = [UIColor blackColor];
        textView.text = @"Enter comments";
        [textView resignFirstResponder];
    }
}
#pragma mark - camera and gallery Methods.
-(void)gallery{
//    NSLog(@"gellery");
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    popUpView.hidden=YES;
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)camera{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    popUpView.hidden=YES;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - imagePickerController Delegate methods.

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    
    _imageview_chosenIMage.image=chosenImage;
    [picker dismissViewControllerAnimated:NO completion:NULL];
    
    //  [self forwa]
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
@end
