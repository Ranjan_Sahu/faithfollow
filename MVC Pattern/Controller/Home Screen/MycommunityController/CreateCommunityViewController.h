//
//  CreateCommunityViewController.h
//  wireFrameSplash
//
//  Created by Vikas on 20/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateCommunityViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtfield_name;
@property (weak, nonatomic) IBOutlet UITextView *txtview_descrip;
- (IBAction)bnt_cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_description;
@property (weak, nonatomic) IBOutlet UIView *view_popup;
@property (weak, nonatomic) IBOutlet UIImageView *imageview_chosenIMage;
- (IBAction)btn_photo:(id)sender;
- (IBAction)btn_submit:(id)sender;
- (IBAction)tap:(id)sender;
- (IBAction)btn_popViewcancel:(id)sender;
- (IBAction)bnt_camera:(id)sender;

- (IBAction)btn_gallery:(id)sender;

@end
