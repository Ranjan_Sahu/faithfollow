//
//  PageContentViewController.h
//  PageViewDemo
//
//  Created by Simon on 24/11/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController

@property NSUInteger pageIndex;


@property (weak, nonatomic) IBOutlet UITableView *tableviewoutlet;
@property (strong, nonatomic) IBOutlet UIView *greenView;
@property(nonatomic,assign) BOOL callService;

@property(nonatomic,strong)NSMutableArray *accept_dataArray;

- (IBAction)button_act:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button_bottomConstraint;

@property (weak, nonatomic) IBOutlet UIButton *btn_AddCommunity;
- (IBAction)btn_addCommunity:(id)sender;


@property(strong,nonatomic)NSString *indexPathImage;
@end
