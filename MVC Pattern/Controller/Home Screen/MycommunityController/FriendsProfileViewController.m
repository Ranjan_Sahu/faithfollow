#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "FriendsProfileViewController.h"
#import "FeedViewController.h"
#import "UIImageView+WebCache.h"
#import "Constant1.h"
#import "WebServiceHelper.h"
#import "FeedTableViewCell.h"
#import "StatusViewController.h"
#import"FriendListController.h"
#define  kCellName @"FeedTableViewCell"
#import "KLCPopup.h"
#import "FriendListController.h"
#import "MWPhotoBrowser.h"
#import "SeeMoreCommentsViewController.h"
#import "LoginViewController.h"
#import "DeviceConstant.h"
#import "FriendsProfileViewController.h"
#import "InfluencerProfileController.h"
#import "Mixpanel.h"
#import "Profile_editViewController.h"

@interface FriendsProfileViewController ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIActionSheetDelegate,MWPhotoBrowserDelegate>{
    NSMutableArray *arrFeeds,*_quoteArray,*denominationwebArray,*messageArray;
    NSMutableDictionary *userProfile;
    NSString *user_id;
    CGFloat heightofCell;
    NSInteger selectedRow;
    UIView *lineView ;
    NSString *strImage,*checkListViewProfile1;
    WallView *wallvie;
    NSMutableArray *photos;
    MWPhotoBrowser *browser;
    CGRect screenRect;
    NSString *strImage1;
    int checkBackgroudnumber;
}

@end

@implementation FriendsProfileViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
    self.navigationController.navigationBarHidden=YES;
    [self.tableView_outlet   registerNib:[UINib nibWithNibName:kCellName bundle:nil] forCellReuseIdentifier:kCellName];
    selectedRow=-1;
    
    arrFeeds=[[NSMutableArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Feed" ofType:@"plist"]];
    
    _wallView.hidden=YES;
    _bckView1.hidden=NO;
    _bckview3.hidden=YES;
    
    screenRect = [[UIScreen mainScreen] bounds];
//    NSLog(@"screeenrect is%f",screenRect.size.height/1.74);
    _heightofwallView.constant=screenRect.size.height/1.74;
    [self.view layoutIfNeeded];
    // friday iphone
    _postFriendProfileDict=_friendProfileDict;
//    NSLog(@"array is%@",_friendProfileDict);
    _labeloutlet.text=[_quoteArray firstObject];
    _labeloutlet.text=[_quoteArray firstObject];
    _lbl_Mainname.text=@"";
    NSDictionary *userDictProfile=[_friendProfileDict valueForKey:@"usersprofile"];
    NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",_friendProfileDict[@"first_name"],_friendProfileDict[@"last_name"]]]];
    _lbl_Mainname.text=nameTxt;
    NSString *imageStr=[userDictProfile valueForKey:@"profile_picture"];
    strImage=[NSString stringWithFormat:@"%@%@",ImageapiUrl,imageStr];
    
    [self loadImage];
    NSString *profileview_option=[NSString stringWithFormat:@"%@",[userDictProfile valueForKey:@"profileview_option"]];
    
//    NSString *friendStatus=[_friendProfileDict valueForKey:@"friend_status"];
        //follwer and following condition for profile view
        
//    if ([friendStatus isEqualToString:@"Unfollow"]) {
        if ([profileview_option isEqualToString:@"2"]) {
            userProfile=[_friendProfileDict valueForKey:@"usersprofile"];
            NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",_friendProfileDict[@"first_name"],_friendProfileDict[@"last_name"]]]];
            
            if ( [nameTxt isEqualToString:@""]) {
                _lbl_name.text=@"-";
                
            }
            else{
                _lbl_name.text=nameTxt;
            }
            
            NSString *maleFemale=[userProfile valueForKey:@"gender"];
            
            if ([maleFemale intValue]==0) {
                _lbl_gender.text=@"Female";
            }
            if ([maleFemale intValue]==1) {
                _lbl_gender.text=@"Male";
            }
            
            if ([maleFemale intValue]==2) {
                _lbl_gender.text=@"-";
            }
            
            
        }
        
        
        
        
//    }
//    //Add Friend
//    if ([friendStatus isEqualToString:@"Unfollow"]) {
//        if ([profileview_option isEqualToString:@"2"]) {
//            _friendProfileDict=nil;
//            _friendbasicInfolbl.hidden=YES;
//            _myFrinendView.hidden=YES;
//            _btn_addFrind.hidden=YES;
//            NSString *nameTxt123=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@%@",_friendProfileDict[@"first_name"],_friendProfileDict[@"last_name"]]]];
//            if ( [nameTxt123 isEqualToString:@"(null)(null)"]) {
//                _lbl_name.text=@"-";
//            }
//            
//            // changed for gender
//            NSString *maleFemaleStr=[userProfile valueForKey:@"gender"];
//            if (maleFemaleStr ==nil) {
//                _lbl_gender.text=@"-";
//            }
//            
//            if ([maleFemaleStr intValue]==0) {
//                _lbl_gender.text=@"Female";
//            }
//            if ([maleFemaleStr intValue]==1) {
//                _lbl_gender.text=@"Male";
//            }
//            
//            if ([maleFemaleStr intValue]==2) {
//                _lbl_gender.text=@"-";
//            }
//
//            
//            
//            
//            
//            checkListViewProfile1=@"profileAddFrined";
//            [self.view makeToast:@"No one can view profile " duration:3.0 position:CSToastPositionCenter];
//        }
//    }
    
    if ([profileview_option isEqualToString:@"3"])
    {
        _friendProfileDict=nil;
        _friendbasicInfolbl.hidden=YES;
        _myFrinendView.hidden=YES;
        _btn_addFrind.hidden=YES;
        NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",_friendProfileDict[@"first_name"],_friendProfileDict[@"last_name"]]]];
        if ( [nameTxt isEqualToString:@"(null) (null)"]) {
            _lbl_name.text=@"-";
        }
        NSString *maleFemale=[userProfile valueForKey:@"gender"];
        if (maleFemale ==nil) {
            _lbl_gender.text=@"-";
        }
        
        // change for no one view my profile setting here
        if ([maleFemale intValue]==0) {
            _lbl_gender.text=@"-";
        }
        if ([maleFemale intValue]==1) {
            _lbl_gender.text=@"-";
        }
        
        if ([maleFemale intValue]==2) {
            _lbl_gender.text=@"-";
        }
        
        
        
        _checkListViewProfile=@"profileviewYes";
        [self.view makeToast:@"No one can view profile " duration:3.0 position:CSToastPositionCenter];
        
    }
    
    if ([profileview_option isEqualToString:@"1"]){
        userProfile=[_friendProfileDict valueForKey:@"usersprofile"];
        NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",_friendProfileDict[@"first_name"],_friendProfileDict[@"last_name"]]]];
        
        if ( [nameTxt isEqualToString:@""]) {
            _lbl_name.text=@"-";
            
        }
        else{
            _lbl_name.text=nameTxt;
        }
        
        NSString *maleFemale=[userProfile valueForKey:@"gender"];
        
        if ([maleFemale intValue]==0) {
            _lbl_gender.text=@"Female";
        }
        if ([maleFemale intValue]==1) {
            _lbl_gender.text=@"Male";
        }
        
        if ([maleFemale intValue]==2) {
            _lbl_gender.text=@"-";
        }
        
        
    }
        
        if ([profileview_option isEqualToString:@"0"]) {
            // changed here for profileview_option=0
            userProfile=[_friendProfileDict valueForKey:@"usersprofile"];
            NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",_friendProfileDict[@"first_name"],_friendProfileDict[@"last_name"]]]];
            
            if ( [nameTxt isEqualToString:@""] || [nameTxt isEqualToString:@"(null) (null)"]) {
                _lbl_name.text=@"-";
            }
            else{
                _lbl_name.text=nameTxt;
            }
            NSString *maleFemale=[userProfile valueForKey:@"gender"];
            if ([maleFemale intValue]==0) {
                _lbl_gender.text=@"Female";
            }
            if ([maleFemale intValue]==1) {
                _lbl_gender.text=@"Male";
            }
            if ([maleFemale intValue]==2) {
                _lbl_gender.text=@"-";
            }
   
        }
//   else
//   {
//   // changed here for profileview_option=0
//       
//       userProfile=[_friendProfileDict valueForKey:@"usersprofile"];
//       NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",_friendProfileDict[@"first_name"],_friendProfileDict[@"last_name"]]]];
//       
//       if ( [nameTxt isEqualToString:@""] || [nameTxt isEqualToString:@"(null) (null)"]) {
//           _lbl_name.text=@"-";
//       }
//       else{
//           _lbl_name.text=nameTxt;
//       }
//       NSString *maleFemale=[userProfile valueForKey:@"gender"];
//       if ([maleFemale intValue]==0) {
//           _lbl_gender.text=@"Female";
//       }
//       if ([maleFemale intValue]==1) {
//           _lbl_gender.text=@"Male";
//       }
//       if ([maleFemale intValue]==2) {
//           _lbl_gender.text=@"-";
//       }
//   
//   }
//    _profileFndView.backgroundColor=UIColorFromRGB(0xFFFFFF);
//    _wallFndView.backgroundColor=UIColorFromRGB(0xF6F6F6);
//    _lbl_btnwall.textColor=[UIColor colorWithRed:103/255.0f green:103/255.0f blue:103/255.0f alpha:1.0f];
//    _lbl_btnprofile.textColor=[UIColor colorWithRed:27/255.0f green:153/255.0f blue:217/255.0f alpha:1.0f];
    _imageview_wall.image= [UIImage imageNamed:@"view_1x"];
    _imageview_profile.image= [UIImage imageNamed:@"profile_selected-1"];
       _lbl_email.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"email"]]];
    if ([_lbl_email.text isEqualToString:@""]) {
        _lbl_email.text=@"-";
    }
    
    _lbl_mobile.text=[NSString stringWithFormat:@"%@",[Constant1 getNumberStringObject:userProfile[@"phone"]]];
    if ([_lbl_mobile.text isEqualToString:@""]) {
        _lbl_mobile.text=@"-";
    }
    
    _lbl_birthday.text=[NSString stringWithFormat:@"%@",[Constant1 getNumberStringObject:userProfile[@"date_of_birth"]]];
if ([_lbl_birthday.text isEqualToString:@""] || [_lbl_birthday.text isEqualToString:@"0000-00-00"] ) {
        _lbl_birthday.text=@"-";
    }
    
    NSString *friendfriendlistId=[NSString stringWithFormat:@"%@",[userProfile valueForKey:@"user_id"]];
    [[NSUserDefaults standardUserDefaults] setObject:friendfriendlistId forKey:@"friendfriendlistId"];
        
    _lbl_jobtitle.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"job_title"]]];
    
    if ([_lbl_jobtitle.text isEqualToString:@""]) {
        _lbl_jobtitle.text=@"-";
    }
    
    _lbl_parish.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"parish"]]];
    if ([_lbl_parish.text isEqualToString:@""]) {
        _lbl_parish.text=@"-";
    }
    
    _lbl_employer.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"employer"]]];
    
    if ([_lbl_employer.text isEqualToString:@""]) {
        _lbl_employer.text=@"-";
    }
    
    _lbl_highschool.text=userProfile[@"high_school"];
    
    if ([_lbl_highschool.text isEqualToString:@""]) {
        _lbl_highschool.text=@"-";
    }
    
    _lbl_relationship.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"relationship_status"]]];
    
    if ([_lbl_relationship.text isEqualToString:@""]) {
        _lbl_relationship.text=@"-";
    }
    
    _lbl_siblings.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"siblings"]]];
    
    if ([_lbl_siblings.text isEqualToString:@""]) {
        _lbl_siblings.text=@"-";
    }
      
    _lbl_children.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"children"]]];
    if ([   _lbl_children.text isEqualToString:@""]) {
        _lbl_children.text=@"-";
    }
    
    
    _lbl_favourateplayer.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"favourite_prayer"]]];
    
    if ([_lbl_favourateplayer.text isEqualToString:@""]) {
        _lbl_favourateplayer.text=@"-";
    }
    _lbl_Commentscount.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"phone"]]];
    
    if ([ _lbl_Commentscount.text isEqualToString:@""]) {
        _lbl_Commentscount.text=@"";
    }
    
    _lbl_Likescount.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:_friendProfileDict[@"phone"]]];
    
    if ([_lbl_Likescount.text isEqualToString:@""]) {
        _lbl_Likescount.text=@"";
    }
    
    _lbl_inspirational.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"inspirationa_saying"]]];
    if ([_lbl_inspirational.text isEqualToString:@""]) {
        _lbl_inspirational.text=@"-";
    }
    
    user_id=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"user_id"]]];
    [[NSUserDefaults standardUserDefaults] setObject:user_id forKey:@"FriendUserID"];
//    [user_id isEqualToString:@"1499"]
    if ([user_id isEqualToString:@"1"] ) {
        _btn_addFrind.hidden=YES;
    }
    else{
    
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"frndlist"]isEqualToString:@"frndlist"]) {
        [_btn_addFrind setTitle:@"Unfriend" forState:UIControlStateNormal];
        
        
    }
    
    if ([_friendProfileDict[@"friend_status"]isEqualToString:@"Unfollow"]) {
        [_btn_addFrind setTitle:@"Unfollow" forState:UIControlStateNormal];
        [_btn_addFrind setBackgroundColor:[UIColor orangeColor]];
        _btn_addFrind.layer.borderWidth  = 1.0f;
        _btn_addFrind.layer.borderColor = [UIColor orangeColor].CGColor;
        [_btn_addFrind setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btn_addFrind addTarget:self action:@selector(friendProfileFollowunFollowMEthod:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    if ([_friendProfileDict[@"friend_status"]isEqualToString:@"Follow"]) {
        [_btn_addFrind setTitle:@"Follow" forState:UIControlStateNormal];
        [_btn_addFrind setBackgroundColor:[UIColor whiteColor]];
        _btn_addFrind.layer.borderWidth  = 1.0f;
        _btn_addFrind.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f].CGColor;
        [_btn_addFrind setTitleColor:[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
        [_btn_addFrind addTarget:self action:@selector(friendProfileFollowunFollowMEthod:) forControlEvents:UIControlEventTouchUpInside];
    }
    if ([_friendProfileDict[@"friend_status"]isEqualToString:@"Unfriend"]) {
        [_btn_addFrind setTitle:@"Unfriend" forState:UIControlStateNormal];
    }
    if ([_friendProfileDict[@"friend_status"]isEqualToString:@"Pending Request"]) {
        [_btn_addFrind setTitle:@"Pending Request" forState:UIControlStateNormal];
    }
    }
    
//    _imageView_frnds.layer.cornerRadius=_imageView_frnds.frame.size.width/2;
//    _imageView_frnds.layer.borderWidth=0.5;
//    _imageView_frnds.layer.borderColor=[UIColor blackColor].CGColor;
    _imageView_frnds.clipsToBounds=YES;
    _scrollView.scrollEnabled=YES;
    [self getdenomination];
    }
    
}

// follow and unfollow

-(void)friendProfileFollowunFollowMEthod:(UIButton*)sender{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
    [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *frindID;
        if ([_friendProfileDict isKindOfClass:[NSMutableDictionary class]]) {
            NSDictionary *userprofile=[_friendProfileDict valueForKey:@"usersprofile"];
            frindID=[userprofile valueForKey:@"user_id"];
        }
        
        NSDictionary *dict=nil;
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 @"friend_id" : frindID,
                 };
        NSString *methodname=nil;
//        NSLog(@"The button title is %@",sender.titleLabel.text);
        UIButton *btn = (UIButton*)sender;
        if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
            methodname=follow;
        }
        else{
            methodname=unfollow;
            
        }
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodname withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                        messageArray=[response valueForKey:@"message"];
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
                                
                                userProfile=[_friendProfileDict valueForKey:@"usersprofile"];
                                NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",_friendProfileDict[@"first_name"],_friendProfileDict[@"last_name"]]]];
                                NSString *currentDate=[Constant1 createTimeStamp];
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Number of follows" by:@1];
                                [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last follow":currentDate}];
                                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Date of last follow":currentDate}];
                                [ [AppDelegate sharedAppDelegate].mxPanel track:@"Follow"
                                                                     properties:@{@"Influencer?":[NSNumber numberWithBool:false],@"Date of last follow":currentDate,@"Following":nameTxt}];
                                
                                [_btn_addFrind setTitle:@"UnFollow" forState:UIControlStateNormal];
                                [_btn_addFrind setBackgroundColor:[UIColor orangeColor]];
                                [_btn_addFrind setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                _btn_addFrind.layer.borderWidth  = 1.0f;
                                _btn_addFrind.layer.borderColor = [UIColor orangeColor].CGColor;
                            }
                            else{
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Number of follows" by:@-1];
                                [_btn_addFrind setTitle:@"Follow" forState:UIControlStateNormal];
                                [_btn_addFrind setBackgroundColor:[UIColor whiteColor]];
                                _btn_addFrind.layer.borderWidth  = 1.0f;
                                _btn_addFrind.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f].CGColor;
                                [_btn_addFrind setTitleColor:[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
                            }
                            
                        });
                        
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
        [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//              NSLog(@"error");
             
         }];
        
    }
    
}

-(void)feedgroupMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        DetailMygroupViewController1 *detailgroupFeed=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
        detailgroupFeed.navigationTitle=[userInfo valueForKey:@"groupname"];
        detailgroupFeed.idUser=[userInfo valueForKey:@"groupis"];
        [self.navigationController pushViewController:detailgroupFeed animated:YES];
    });
    
}

-(void)friendProfileView:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        FriendsProfileViewController *friend=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
        NSMutableDictionary *dictcontent=[userInfo valueForKey:@"allData"];
        friend.friendProfileDict=dictcontent;
        friend.postFriendProfileDict=dictcontent;
        [self.navigationController pushViewController:friend animated:YES];
        
    });
    
}




-(void)influcencerProfileViewMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        InfluencerProfileController *InfluencerProfileControllerView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
        NSMutableDictionary *influencerDictData=[userInfo valueForKey:@"allData"];
        InfluencerProfileControllerView.influencerDict=influencerDictData;
        [self.navigationController pushViewController:InfluencerProfileControllerView animated:YES];
    });
    
}

-(void)userProfileViewfriendProfile:(NSNotification *) notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        Profile_editViewController *friendprofile=[self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
        [self.navigationController pushViewController:friendprofile animated:YES];
    });
}

-(void)updateCommentfriend :(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    [wallvie updateCommentfromProfile:userInfo];
}

-(void)reloadcellwallviewProfilefriend :(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    [wallvie reloadcellwallview:userInfo];
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateCommentfriend:)
                                                 name:@"commentsNoti"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadcellwallviewProfilefriend:)
                                                 name:@"reloadcellwall"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(feedgroupMethod:)
                                                 name:@"feedGroupsuggestion"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(friendProfileView:)
                                                 name:@"friendProfileView"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(influcencerProfileViewMethod:)
                                                 name:@"influcencerProfileView"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userProfileViewfriendProfile:)
                                                 name:@"userProfileView"
                                               object:nil];



    
    [[AppDelegate sharedAppDelegate] hideaddView];

    AppDelegate *appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView_outlet.frame.size.width,100)];
    footerView.backgroundColor=[UIColor whiteColor];
    self.tableView_outlet.tableFooterView = footerView;
    }
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterInForegroundFrindProfile) name:UIApplicationDidBecomeActiveNotification object:nil];
    
}

-(void)appEnterInForegroundFrindProfile{
    checkBackgroudnumber=checkBackgroudnumber+1;
    if (checkBackgroudnumber%3==0) {
    [[NSUserDefaults standardUserDefaults]setObject:@"Yes" forKey:@"freindProfile"];
    [[NSUserDefaults standardUserDefaults] synchronize];
        checkBackgroudnumber=0;
    }
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"feedGroupsuggestion" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"friendProfileView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"influcencerProfileView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"userProfileView" object:nil];
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (getVar.checkDisapper) {
        [[AppDelegate sharedAppDelegate] addsView];
//        getVar.checkDisapper=NO;
    }

}

-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE4||IS_IPHONE5) {
        _scrollView.contentSize=CGSizeMake(_scrollView.frame.size.width, 780);
    }
    if (IS_IPHONE6||IS_IPHONE6PLUS) {
        _scrollView.contentSize=CGSizeMake(_scrollView.frame.size.width, 890);

    }
    
}

-(void)getdenomination{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }
    else {
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"getdenomination" withParameters:nil withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            denominationwebArray=response[@"data"];
                            if ([_checkListViewProfile isEqualToString:@"profileviewYes"]) {
                                _lbl_denomation.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"denomination"]]];
                                if ([_lbl_denomation.text isEqualToString:@""]) {
                                    _lbl_denomation.text=@"-";
                                }
                                _checkListViewProfile=@"profileviewNo";
                            }
                            else{
                                
                                NSString *idStr1=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:userProfile[@"denomination"]]];
                                if ([idStr1 isEqualToString:@""]) {
                                    _lbl_denomation.text=@"-";
                                }
                                else{
                                
                                
                                for (int i=0; i<[denominationwebArray count]; i++) {
                                    NSDictionary *dict=[denominationwebArray  objectAtIndex:i];
                                    NSString *idStr=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
                                    if ([idStr isEqualToString:idStr1]) {
                                        if ([_lbl_denomation.text isEqualToString:@""]) {
                                            _lbl_denomation.text=@"-";
                                        }
                                        else{
                                            _lbl_denomation.text=[dict valueForKey:@"name"];
                                        }
                                        
                                        
                                    }
                                }
                                
                                
                                
                                
                            }
                            }
                        });
                        
                    }
                }
                else{
                    //                    UIAlertView *at=[[UIAlertView alloc]initWithTitle:appTitle message:@"server Failed " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    //                    [at show];
                    //                   [self.view makeToast:response[@"message"] duration:1.0 position:CSToastPositionCenter];
                    
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
    
    
}

-(void)loadImage {
    UITapGestureRecognizer   *singleTapGestureRecognizerclick =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureclick:)];
    _imageView_frnds.userInteractionEnabled=YES;
    _imageView_frnds.multipleTouchEnabled=YES;
    [_imageView_frnds addGestureRecognizer:singleTapGestureRecognizerclick];
    
    [_imageView_frnds sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
}


- (void)handleSingleTapGestureclick:(UITapGestureRecognizer *)tapGestureRecognizer{
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{

        NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
        [post_imageArray addObject:strImage];
        [self openphoto:post_imageArray influencerPhoto:@"no"];

        
    }
    
}

-(void)openphoto:(NSMutableArray*)imagearra influencerPhoto:(NSString *)checkInfluencer{
    NSMutableArray *imagearr=[[NSMutableArray alloc]init];
    
    for (int i=0; i<imagearra.count; i++) {
        NSString *strUrl=[NSString stringWithFormat:@"%@",[imagearra objectAtIndex:i]];
        MWPhoto *photo=[MWPhoto photoWithURL:[NSURL URLWithString:strUrl]];
        [imagearr addObject:photo];
    }
    photos=[[[imagearr reverseObjectEnumerator] allObjects] mutableCopy];
    browser = [[MWPhotoBrowser alloc] initWithDelegate:(id)self];
    BOOL displayActionButton = YES;
    BOOL displaySelectionButtons = NO;
    BOOL enableGrid = YES;
    BOOL startOnGrid = NO;
    browser.displayActionButton = displayActionButton;
    //  browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = NO;
    browser.zoomPhotosToFill = YES;
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = NO;
    //browser.autoPlayOnAppear = autoPlayOnAppear;
    [browser setCurrentPhotoIndex:0];
    enableGrid = NO;
    [self.navigationController pushViewController:browser animated:YES];
}

-(IBAction)cancelBTN:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController popToRootViewControllerAnimated:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btn_addFrind:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    if ([_btn_addFrind.titleLabel.text isEqualToString:@"Add Friend"]) {
        [TenjinSDK sendEventWithName:@"AddFriend_Tapped"];
        [FBSDKAppEvents logEvent:@"AddFriend_Tapped"];
        [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"AddFriend_Tapped"]];
        UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:appTitle delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Send Friend Request",nil];
        actionSheet.tag=123;
        actionSheet.delegate=self;
        [actionSheet showInView:self.view];
        
        
    }
    if ([_btn_addFrind.titleLabel.text isEqualToString:@"Unfriend"]) {
        
        [self sendrequestUnfriend];
        
    }
    if ([_btn_addFrind.titleLabel.text isEqualToString:@"Pending Request"]) {
        UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:appTitle delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Reject Request",nil];
        actionSheet.tag=12312;
        actionSheet.delegate=self;
        [actionSheet showInView:self.view];
        
        
    }
    if ([_btn_addFrind.titleLabel.text isEqualToString:@"Cancel Request"]) {
        [self sendrequestCancelFriend] ;
        
        
    }
        
    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(actionSheet.tag==123){
        if (buttonIndex==0) {
            [self sendrequestAddFriend] ;
        }
    }
    if(actionSheet.tag==1231){
        if (buttonIndex==0) {
            [self sendrequestUnfriend] ;
        }
    }
    if(actionSheet.tag==12312){
        if (buttonIndex==0) {
            [self sendrequestCancelFriendrejectrequest] ;
            
        }
    }
    
    
}

-(void) sendrequestCancelFriendrejectrequest{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : [[NSUserDefaults standardUserDefaults]objectForKey:@"FriendUserID"],
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"rejectrequest" withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_btn_addFrind setTitle:@"Add Friend" forState:UIControlStateNormal];
                        NSMutableArray *messageArray;
                        messageArray=[response valueForKey:@"message"];
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        
                    });
                    
                    
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableArray *messageArray;
                        messageArray=[response valueForKey:@"message"];
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        
                    });
                    
                    
                    
                }
                
            }
        }
        
    } errorBlock:^(id error)
     {
         [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
 
//         NSLog(@"error");
         
     }];
    }
}

#pragma  pending request

-(void)sendrequestPendingFriend{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{

    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"unfriend" withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_btn_addFrind setTitle:@"Add Friend" forState:UIControlStateNormal];
                        NSMutableArray *messageArray;
                        messageArray=[response valueForKey:@"message"];
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        
                    });
                    
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableArray *messageArray;
                        messageArray=[response valueForKey:@"message"];
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        
                    });
                    
                    
                    
                }
                
            }
        }
        
    } errorBlock:^(id error)
     {
    [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//         NSLog(@"error");
         
     }];
    }
    
}

#pragma  sendrequestUnfriend

-(void) sendrequestUnfriend{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : [[NSUserDefaults standardUserDefaults]objectForKey:@"FriendUserID"],
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"unfriend" withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableArray *messageArray;
                        messageArray=[response valueForKey:@"message"];
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        
                    });
                    
                    
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableArray *messageArray;
                        messageArray=[response valueForKey:@"message"];
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        
                    });
                    
                    
                    
                }
                
            }
        }
        
    } errorBlock:^(id error)
     {
         [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//         NSLog(@"error");
         
     }];
    }
}




#pragma  cancelFriendrequest

-(void) sendrequestCancelFriend{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : [[NSUserDefaults standardUserDefaults]objectForKey:@"FriendUserID"],
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"cancelrequest" withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_btn_addFrind setTitle:@"Add Friend" forState:UIControlStateNormal];
                        NSMutableArray *messageArray;
                        messageArray=[response valueForKey:@"message"];
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    });
                    
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableArray *messageArray;
                        messageArray=[response valueForKey:@"message"];
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        
                    });
                    
                    
                    
                }
                
            }
        }
        
    } errorBlock:^(id error)
     {
        [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];

//         NSLog(@"error");
         
     }];
    }
}




-(void) sendrequestAddFriend{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : [[NSUserDefaults standardUserDefaults]objectForKey:@"FriendUserID"],
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"sendrequest" withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_btn_addFrind setTitle:@"Cancel Request" forState:UIControlStateNormal];
                        NSMutableArray *messageArray;
                        messageArray=[response valueForKey:@"message"];
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        
                    });
                    
                    
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableArray *messageArray;
                        messageArray=[response valueForKey:@"message"];
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        
                    });
                    
                    
                    
                }
                
            }
        }
        
    } errorBlock:^(id error)
     {
        [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//         NSLog(@"error");
         
     }];
    }
}



- (IBAction)btn_profile:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    _scrollView.scrollEnabled=YES;
    _bckView1.hidden=NO;
        if (IS_IPHONE4) {
            _scrollView.contentSize=CGSizeMake(_scrollView.frame.size.width,_scrollView.frame.size.height+600.0);
        }
        else{
            _scrollView.contentSize=CGSizeMake(_scrollView.frame.size.width,890);
        }
    _imageview_wall.image= [UIImage imageNamed:@"edit_unselected"];
    _imageview_profile.image= [UIImage imageNamed:@"profile_selected-1"];
//    _lbl_btnwall.textColor=[UIColor colorWithRed:103/255.0f green:103/255.0f blue:103/255.0f alpha:1.0f];
//    _lbl_btnprofile.textColor=[UIColor colorWithRed:27/255.0f green:153/255.0f blue:217/255.0f alpha:1.0f];
//    _bckView1.backgroundColor=UIColorFromRGB(0x009AD9);
//    _profileFndView.backgroundColor=UIColorFromRGB(0xFFFFFF);
//    _wallFndView.backgroundColor=UIColorFromRGB(0xF6F6F6);
    if ([_checkListViewProfile isEqualToString:@"profileviewYes"]) {
        _myFrinendView.hidden=YES;
    }
    else{
        _myFrinendView.hidden=YES;
        _frindviewHeight.constant=50;
//        _myFrinendView.clipsToBounds = YES;

    }
    
    if ([checkListViewProfile1 isEqualToString:@"profileAddFrined"]) {
        _myFrinendView.hidden=YES;
        checkListViewProfile1=@"profileAddFrinedNo";
        
    }
    }
    
}



- (IBAction)btn_wall:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{

    if (_postFriendProfileDict) {
        _frindviewHeight.constant=0;
        _scrollView.contentOffset=CGPointZero;
        _scrollView.scrollEnabled=NO;
        _scrollView.contentSize=CGSizeMake(_scrollView.frame.size.width,0);
        NSDictionary *eventLocation = @{@"wallfor": @"Mywall",@"userid":_postFriendProfileDict[@"id"],@"friendWall":@"commonFriend",@"post_option":_postFriendProfileDict[@"usersprofile"][@"post_option"],@"friend_status":_postFriendProfileDict[@"friend_status"],@"profile_picture":_postFriendProfileDict[@"usersprofile"][@"profile_picture"],
             @"Hideheader":@"yes"
            };
        
        wallvie=[[WallView alloc]initWithframe:CGRectMake(0, 0, 50, 50) andUserInfo:eventLocation];
        wallvie.frame= CGRectMake(0, 0,_wallView.frame.size.width,_wallView.frame.size.height);
        wallvie.hideheader=YES;
        wallvie.delegate1=self;
        [wallvie webservice_call];
        [_wallView addSubview:wallvie];
    }
    
    _bckview3.hidden=NO;
    _imageview_wall.image= [UIImage imageNamed:@"view_blue_1x"];
    _imageview_profile.image= [UIImage imageNamed:@"profile_unselected-1"];
//    _lbl_btnprofile.textColor=[UIColor colorWithRed:103/255.0f green:103/255.0f blue:103/255.0f alpha:1.0f];
//    _lbl_btnwall.textColor=[UIColor colorWithRed:27/255.0f green:153/255.0f blue:217/255.0f alpha:1.0f];
//    _bckview3.backgroundColor=UIColorFromRGB(0x009AD9);
//    _profileFndView.backgroundColor=UIColorFromRGB(0xF6F6F6);
//    _wallFndView.backgroundColor=UIColorFromRGB(0xFFFFFF);
    _myFrinendView.hidden=YES;
    
    }
    
    
}

- (IBAction)ProfileViewBTN:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    _wallView.hidden=YES;
    _secondView.hidden=NO;
    _bckview3.hidden=YES;
    //    _myFrinendView.hidden=YES;
    }
    
    
    
}

- (IBAction)wallViewMethod:(id)sender {
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    _secondView.hidden=YES;
    _wallView.hidden=NO;
    _bckView1.hidden=YES;
    }
    
}

#pragma mark TableView Data Source methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrFeeds count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName forIndexPath:indexPath];
    [self setUpCell:cell atIndexPath:indexPath];
    
    if(indexPath.row == selectedRow)
    {
        NSArray *arr=[arrFeeds objectAtIndex:indexPath.row][@"comments"];
        [cell.commentsView refereshData:arr];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static FeedTableViewCell *cell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cell = [self.tableView_outlet dequeueReusableCellWithIdentifier:kCellName];
    });
    [self setUpCell:cell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:cell];
}
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsUpdateConstraints];
    [sizingCell updateConstraintsIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}
- (void)setUpCell:(FeedTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    float width=[UIScreen mainScreen].bounds.size.width-(20);
    cell.com_lbl.preferredMaxLayoutWidth = width; // This is necessary to allow the label
    NSDictionary *dict=[arrFeeds objectAtIndex:indexPath.row];
    cell.com_lbl.text=dict[@"desc"];
    [cell.comments_btn addTarget:self action:@selector(comments_act:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(indexPath.row == selectedRow)
    {
        NSArray *arr=dict[@"comments"];
        float  height=([arr count]*60);
        cell.commentsView.hidden=NO;
        cell.heightConsatraint.constant=height+72;
        cell.commentsView.tblViewHeight.constant=height;
        cell.mainView.layer.borderWidth=0.0f;
        
        //        cell.mainView.layer.borderWidth=0.5f;
        //        cell.mainView.layer.cornerRadius=8.0;
        //        cell.mainView.layer.borderColor=[UIColor redColor].CGColor;
        //
        //        cell.commentsView.layer.borderWidth=0.5f;
        //        cell.commentsView.layer.cornerRadius=8.0;
        //        cell.commentsView.layer.borderColor=[UIColor redColor].CGColor;
    }
    else{
        cell.heightConsatraint.constant=0;
        cell.commentsView.hidden=YES;
        cell.commentsView.tblViewHeight.constant=0;
        //        cell.mainView.layer.borderWidth=0.5f;
        //        cell.mainView.layer.cornerRadius=8.0;
        //        cell.mainView.layer.borderColor=[UIColor redColor].CGColor;
        
    }
}


-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [_textViewTXT resignFirstResponder];
    StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
//    [self presentViewController:statusScreen animated:YES completion:nil];
    [self.navigationController pushViewController:statusScreen animated:YES];

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 84.0;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"SectionHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    _textViewTXT=(UITextView*)[headerView viewWithTag:1000];
    _textViewTXT.delegate=self;
    
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 84.0;
}




- (IBAction)myFriendListBTN:(id)sender {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;    }
    else{
    FriendListController *friendlist=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendListController"];
//        [self presentViewController:friendlist animated:YES completion:nil];
    [self.navigationController pushViewController:friendlist animated:YES];
    }
    
}

- (void) FunctionOne: (NSString*) dataOne andArray:(NSMutableArray*)arraycontent
{
    if([dataOne isEqualToString:@"open statusView" ]){
        StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
        statusScreen.status_para=@"1";
        statusScreen.dictofPost=[arraycontent objectAtIndex:0];
//        [self presentViewController:statusScreen animated:YES completion:nil];
        [self.navigationController pushViewController:statusScreen animated:YES];

    }else if ([dataOne isEqualToString:@"open prayer" ]){
        StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
        statusScreen.status_para=@"2";
        // change for prayer request
        statusScreen.dictofPost=[arraycontent objectAtIndex:0];
        [self.navigationController pushViewController:statusScreen animated:YES];

//        [self presentViewController:statusScreen animated:YES completion:nil];
    }else if ([dataOne isEqualToString:@"open seemoreImage" ]){
        
        NSMutableArray *imagearr=[[NSMutableArray alloc]init];
        for (int i=0; i<arraycontent.count; i++) {
            NSString *str=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,[arraycontent objectAtIndex:i]];
            MWPhoto *photo=[MWPhoto photoWithURL:[NSURL URLWithString:str]];
//            photo.caption = @"Biblefaithfollow.SocialApp";
            
            [imagearr addObject:photo];
        }
        
        photos=[[[imagearr reverseObjectEnumerator] allObjects] mutableCopy];
        browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        BOOL displayActionButton = YES;
        BOOL displaySelectionButtons = NO;
//        BOOL displayNavArrows = NO;
        BOOL enableGrid = YES;
        BOOL startOnGrid = NO;
//        BOOL autoPlayOnAppear = NO;
        browser.displayActionButton = displayActionButton;
        //  browser.displayNavArrows = displayNavArrows;
        browser.displaySelectionButtons = displaySelectionButtons;
        browser.alwaysShowControls = NO;
        browser.zoomPhotosToFill = YES;
        browser.enableGrid = enableGrid;
        browser.startOnGrid = startOnGrid;
        browser.enableSwipeToDismiss = NO;
        //browser.autoPlayOnAppear = autoPlayOnAppear;
        [browser setCurrentPhotoIndex:0];
        enableGrid = NO;
       (void) enableGrid;
      
        NSArray *viewcontrollerArray=self.navigationController.viewControllers;
        if(viewcontrollerArray.count==0){
            UIView *topView=[[UIView alloc]initWithFrame:CGRectMake(0, 20,[UIScreen mainScreen].bounds.size.width, 50)];
            topView.backgroundColor=UIColorFromRGB(0x101010);
            
            UIButton *backbButton = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage *buttonImage = [UIImage imageNamed:@"webmbback"];
            [backbButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
            [backbButton addTarget:self
                            action:@selector(backBrowserMethod:)
                  forControlEvents:UIControlEventTouchUpInside];
            backbButton.frame = CGRectMake(20.0,10.0, 70, 30.0);
            [topView addSubview:backbButton];
            //
            NSString *imagecount=[NSString stringWithFormat:@"1 of %ld",[imagearr count]];
            UILabel *countLabel = [[UILabel alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/2)-40, 20, 100, 20)];
            [countLabel setTextColor:[UIColor whiteColor]];
            if ([imagearr count]==1) {
                countLabel.text=@"";
            }
            else{
                countLabel.text=imagecount;
            }
            countLabel.textAlignment=NSTextAlignmentCenter;
            [countLabel setBackgroundColor:[UIColor clearColor]];
            [topView addSubview:countLabel];
            
            //
            [browser.view addSubview:topView];
              [self presentViewController:browser animated:YES completion:nil];
        }else{
            [self.navigationController pushViewController:browser animated:YES];
        } 
    }
}
-(void)backBrowserMethod:(UIButton*)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}




-(void) btn_pressed: (NSString*) buttonName andtotal_comments:(NSMutableArray*)totalComments andpost:(NSDictionary*)post andCellindex:(NSUInteger)cell_index{
    SeeMoreCommentsViewController *seemoreScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"SeeMoreCommentsViewController"];
    seemoreScreen.totalComments=totalComments;
    seemoreScreen.post_dict=post;
    seemoreScreen.cellindex=cell_index;
    [self.navigationController pushViewController:seemoreScreen animated:YES];

//    [self presentViewController:seemoreScreen animated:YES completion:nil];
}
-(void)openViewProfile: (NSString*) dataOne andArray:(NSMutableDictionary*)dictcontent{
    
    if([dataOne isEqualToString:@"open friend" ]){
        
        NSString *idofUser=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
        int val=[idofUser intValue];
        int val2=[dictcontent[@"id"] intValue];
        dispatch_async(dispatch_get_main_queue(), ^{
//            NSLog(@"id is%@",dictcontent);
            if (val==val2) {
                [self.navigationController popToRootViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName: @"profileshow" object:nil userInfo:nil];
            }else{
                
                FriendsProfileViewController *friend=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                friend.friendProfileDict=dictcontent;
                friend.postFriendProfileDict=dictcontent;
                    [self.navigationController pushViewController:friend animated:YES];
//                [self presentViewController:friend animated:YES completion:nil];
                
            }
            
            
            
            
        });
    }
    
}
- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
    
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
//    NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
    {
        color=[UIColor whiteColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}






@end
