//
//  ViewController.m
//  PageViewDemo
//
//  Created by Simon on 24/11/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "mycommunityViewcontroller.h"
#import "searchViewController.h"
#import "InfluencerProfileController.h"
@interface mycommunityViewcontroller (){
    
}

@end

@implementation mycommunityViewcontroller

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Create the data model
    _pageTitles = @[@"Over 200 Tips and Tricks", @"Discover Hidden Features", @"Bookmark Favorite Tip"];
    _pageImages = @[@"page1.png", @"page2.png", @"page3.png"];
    
    // Create page view controller
    self.pageViewControllercom = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewControllercom.dataSource = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewControllercom setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewControllercom.view.frame = CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height );
    
    [self addChildViewController:_pageViewControllercom];
    [self.view addSubview:_pageViewControllercom.view];
    [self.pageViewControllercom didMoveToParentViewController:self];
    
    _search_btn.selected=false;
    _mycommunity_btn.selected=false;
    _friendrequest_btn.selected=true;
    
//    [_friendrequest_btn setTitleColor:[UIColor colorWithRed:134.0/255.0f green:134.0/255.0f  blue:134.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
//    [_mycommunity_btn setTitleColor:[UIColor colorWithRed:134.0/255.0f green:134.0/255.0f  blue:134.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
//    [_search_btn setTitleColor:[UIColor colorWithRed:134.0/255.0f green:134.0/255.0f  blue:134.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
//
//    [_friendrequest_btn setTitleColor:[UIColor colorWithRed:116.0/255.0f green:194.0/255.0f  blue:228.0/255.0f  alpha:1.0f]  forState:UIControlStateSelected];
//    [_mycommunity_btn setTitleColor:[UIColor colorWithRed:116.0/255.0f green:194.0/255.0f  blue:228.0/255.0f  alpha:1.0f]  forState:UIControlStateSelected];
//    [_search_btn setTitleColor:[UIColor colorWithRed:116.0/255.0f green:194.0/255.0f  blue:228.0/255.0f  alpha:1.0f]  forState:UIControlStateSelected];
//    _bckView1.backgroundColor=[UIColor colorWithRed:116.0/255.0f green:194.0/255.0f  blue:228.0/255.0f  alpha:1.0f];
    
    if (IS_IPHONE4){
        //_heightofview.constant=150;
        _topconstraint.constant=0;
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(feedgroupMethod:)
                                                 name:@"feedGroupsuggestion"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(friendProfileView:)
                                                 name:@"friendProfileView"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(influcencerProfileViewMethod:)
                                                 name:@"influcencerProfileView"
                                               object:nil];
    
    NSString *communitySearch=[[NSUserDefaults standardUserDefaults] valueForKey:@"SearchCommunity"];
    NSString *checkCancel=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pageIndexZero"]];
    if ([checkCancel isEqualToString:@"pageIndex"]) {
        [self search_act:nil];
        checkCancel=@"";
       
            }
    else{
    if (_subnotification==3) {
        [self mycommunity];
    }
    else if ([communitySearch isEqualToString:@"SearchCommunity"]) {
        [self freiend_act:nil];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SearchCommunity"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else{
        [self mycommunity];
    }
    }
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
}



-(void)feedgroupMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        DetailMygroupViewController1 *detailgroupFeed=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
        detailgroupFeed.navigationTitle=[userInfo valueForKey:@"groupname"];
        detailgroupFeed.idUser=[userInfo valueForKey:@"groupis"];
        [self.navigationController pushViewController:detailgroupFeed animated:YES];
    });
    
}


-(void)influcencerProfileViewMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        InfluencerProfileController *InfluencerProfileControllerView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
        NSMutableDictionary *influencerDictData=[userInfo valueForKey:@"allData"];
        InfluencerProfileControllerView.influencerDict=influencerDictData;
        [self.navigationController pushViewController:InfluencerProfileControllerView animated:NO];
          });
    
}


-(void)friendProfileView:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        FriendsProfileViewController *friend=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
        NSMutableDictionary *dictcontent=[userInfo valueForKey:@"allData"];
        friend.friendProfileDict=dictcontent;
        friend.postFriendProfileDict=dictcontent;
        [self.navigationController pushViewController:friend animated:NO];
        
    });
    
}







- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source



- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

// follower method
- (IBAction)search_act:(id)sender {
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        PageContentViewController *startingViewController = [self viewControllerAtIndex:1];
        startingViewController.pageIndex=2;
        NSArray *viewControllers = @[startingViewController];
        [self.pageViewControllercom setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        [self setbackViewColor:nil andotherbackViewColor:[UIColor clearColor] andViewname1:_bckVIew2 andViewname2:_bckView1 andViewname3:_bckview3];
        _mycommunity_btn.selected=false;
        _search_btn.selected=true;
        _friendrequest_btn.selected=false;
        
   
    }
    
    
}
- (IBAction)mycommunity_act:(id)sender {
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        PageContentViewController *startingViewController = [self viewControllerAtIndex:1];
        startingViewController.pageIndex=1;
        NSArray *viewControllers = @[startingViewController];
        [self.pageViewControllercom setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        [self setbackViewColor:nil andotherbackViewColor:[UIColor clearColor] andViewname1:_bckview3 andViewname2:_bckView1 andViewname3:_bckVIew2];
        _mycommunity_btn.selected=true;
        _search_btn.selected=false;
        _friendrequest_btn.selected=false;
    }
    
}

//search method
- (IBAction)freiend_act:(id)sender {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        searchViewController *second=[self.storyboard instantiateViewControllerWithIdentifier:@"searchViewController"];
        NSArray *viewControllers = @[second];
        [self.pageViewControllercom setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        [self setbackViewColor:nil andotherbackViewColor:[UIColor clearColor] andViewname1:_bckView1 andViewname2:_bckview3 andViewname3:_bckVIew2];
        
        _mycommunity_btn.selected=false;
        _search_btn.selected=false;
        _friendrequest_btn.selected=true;
    }
}

-(void)setbackViewColor:(UIColor*)backcolor andotherbackViewColor:(UIColor*)commonbackcolor andViewname1:(UIView*)backview1  andViewname2:(UIView*)backview2 andViewname3:(UIView*)backview3{
//    backview1.backgroundColor=[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f];
//    backview2.backgroundColor=commonbackcolor;
//    backview3.backgroundColor=commonbackcolor;
    backview1.hidden = NO;
    backview2.hidden = YES;
    backview3.hidden = YES;
}

-(void)mycommunity{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else
    {
    PageContentViewController *startingViewController = [self viewControllerAtIndex:1];
    startingViewController.pageIndex=1;
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewControllercom setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self setbackViewColor:nil andotherbackViewColor:[UIColor clearColor] andViewname1:_bckview3 andViewname2:_bckView1 andViewname3:_bckVIew2];
    _mycommunity_btn.selected=true;
    _search_btn.selected=false;
    _friendrequest_btn.selected=false;
    }
    
    
}
@end
