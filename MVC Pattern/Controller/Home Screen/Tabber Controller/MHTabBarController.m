#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "MHTabBarController.h"
#import "FeedViewController.h"
#import "NotificationViewController.h"
#import "mycommunityViewcontroller.h"
#import "MLKMenuPopover.h"
#import "Profile_editViewController.h"
#import "WebServiceHelper.h"
#import "Constant1.h"
#import "DeviceConstant.h"
#import "HomeViewController.h"
#import "GroupviewPagecontrollerViewController.h"
#import "RightMenuViewController.h"
#import "SlideNavigationController.h"
#import "InfluencerClassViewController.h"
#import "Mixpanel.h"


static const NSInteger TagOffset = 1000;
static const NSInteger fromTopHightContainer=60;
static const NSInteger searchViewHeight = 44;

@interface MHTabBarController ()<MLKMenuPopoverDelegate,UIScrollViewDelegate,SlideNavigationControllerDelegate>{
    CGRect MENU_POPOVER_FRAME;
    UIButton *feedFilter;
    UIView *vi,*mainVi;
    UIView *feedsubVi,*feedMainVi;
    UIButton *searchButton,*menuButton;
    int count;
    UILabel *notifylbl,*post_notifylbl;
    UIView *tabButtonsContainerView,*notificationView;
    UIView *contentContainerView;
    UIImageView *indicatorImageView,*logoImge;
    Profile_editViewController* listViewController5;
    UIButton *redviewbtn;
    UIView *popUpView;
    NSUInteger pagenumber;
    
    NSArray *imageArray;
    NSTimer *_timer;
    
    UIScrollView *topButtonscrollview;
    CGFloat widthofbutton;
    BOOL swiped,scrolled,swipedLeft,swipedright,buttonClikced,buttonProcessDone;
}
@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property(nonatomic,strong) NSArray *menuItems;
@end
@implementation MHTabBarController
@synthesize isserachGroupCommunitty,notificationLBL,searchView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Refresh myView and/or main view
    [self.safeView layoutIfNeeded];
    
    CGRect    screenRect = [self.safeView bounds];
    
    [SlideNavigationController sharedInstance].enableSwipeGesture = FALSE;
    
    //The event handling method
    searchView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, screenRect.size.width, 44)];

    //search image
    searchButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton addTarget:self action:@selector(searchbuttonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [searchButton setImage:[UIImage imageNamed:@"searchIcon30"] forState:UIControlStateNormal];
    searchButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    searchButton.frame = CGRectMake(10,7,30.0, 30.0);
    [searchView addSubview:searchButton];
    
    // logo image
    logoImge=[[UIImageView alloc]initWithFrame:CGRectMake(screenRect.size.width/2-15,7,30, 30)];
    logoImge.image=[UIImage imageNamed:@"indicator_logo1"];
    logoImge.contentMode =  UIViewContentModeScaleAspectFit;
    [searchView addSubview:logoImge];
    
    // slider menu
    menuButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setImage:[UIImage imageNamed:@"menu30"] forState:UIControlStateNormal];
    menuButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    menuButton.frame = CGRectMake(screenRect.size.width-40.0,7,30.0, 30.0);
    [menuButton addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
    
    [searchView addSubview:menuButton];
    [self.safeView addSubview:searchView];
    
    topButtonscrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, searchViewHeight, screenRect.size.width,[self tabBarHeight])];
    topButtonscrollview.delegate=self;
    // set scrollview scrolling enabeled NO.
    topButtonscrollview.scrollEnabled=NO;
    // contentContainerView main view Controller here we r adding all sub class in contentContainerView.
     topButtonscrollview.scrollEnabled=NO;
    topButtonscrollview.scrollEnabled=YES;//arvind
    topButtonscrollview.showsHorizontalScrollIndicator=NO;
    topButtonscrollview.backgroundColor = [UIColor redColor];
    [self.safeView addSubview:topButtonscrollview];
    
    FeedViewController *listViewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedViewController"];
    InfluencerClassViewController *listViewController2= [self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerClassViewController"];
    NotificationViewController *listViewController3=[self.storyboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
   GroupviewPagecontrollerViewController *listViewController4= [self.storyboard instantiateViewControllerWithIdentifier:@"GroupviewPagecontrollerViewController"];
     listViewController5 = [self.storyboard instantiateViewControllerWithIdentifier:@"mycommunityViewcontroller"];
    
    listViewController1.title = @"Feed";
    listViewController2.title=@"Influencers";
    listViewController3.title = @"Notifications";
    listViewController4.title = @"Group";
    listViewController5.title = @"Community";
    
    listViewController1.tabBarItem.image = [UIImage imageNamed:@"feed_unsel60"];
    listViewController2.tabBarItem.image = [UIImage imageNamed:@"influencer_unsel60"];
    listViewController3.tabBarItem.image = [UIImage imageNamed:@"not_unsel60"];
    listViewController4.tabBarItem.image = [UIImage imageNamed:@"groups_unsel60"];
    listViewController5.tabBarItem.image = [UIImage imageNamed:@"community_unsel60"];

    listViewController1.tabBarItem.selectedImage = [UIImage imageNamed:@"feed_sel40"];
    listViewController2.tabBarItem.selectedImage = [UIImage imageNamed:@"influencer_sel60"];
    listViewController3.tabBarItem.selectedImage = [UIImage imageNamed:@"not_sel60"];
    listViewController4.tabBarItem.selectedImage = [UIImage imageNamed:@"groups_sel60"];
    listViewController5.tabBarItem.selectedImage = [UIImage imageNamed:@"community_sel.60png"];
    
     NSArray *viewControllers = @[listViewController1,listViewController2,listViewController3,listViewController4,listViewController5];
    
    self.viewControllers = viewControllers;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    CGRect rect2 = CGRectMake(0.0f, searchViewHeight, self.view.bounds.size.width, self.tabBarHeight);
    topButtonscrollview.frame=rect2;
    
    CGRect rect = CGRectMake(0, 0, self.view.bounds.size.width, self.tabBarHeight);
    tabButtonsContainerView = [[UIView alloc] initWithFrame:rect];
    tabButtonsContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    rect.origin.y = self.tabBarHeight+searchViewHeight;
    rect.size.height = self.view.bounds.size.height -(self.tabBarHeight+searchViewHeight);
    contentContainerView = [[UIView alloc] initWithFrame:rect];
    contentContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.safeView addSubview:contentContainerView];
    
    indicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MHTabBarIndicator"]];
    [self.safeView addSubview:indicatorImageView];
    
    feedFilter= [UIButton buttonWithType:UIButtonTypeCustom];
    [feedFilter addTarget:self action:@selector(popOverMenu) forControlEvents:UIControlEventTouchUpInside];
    [feedFilter setImage:[UIImage imageNamed:@"dot"] forState:UIControlStateNormal];
    feedFilter.frame =CGRectMake(screenRect.size.width-25,(self.tabBarHeight/2)-6, 30, self.tabBarHeight);
    if (IS_IPHONE6PLUS || IS_IPHONE_X || IS_IPHONE_XS_Max) {
        feedFilter.frame =CGRectMake(screenRect.size.width-30, (self.tabBarHeight/2)-12, 30,  self.tabBarHeight);
    }
    [self.safeView addSubview:feedFilter];
    
// filtration indicator hidden as per requirment
    feedFilter.hidden=YES;
    [UIApplication sharedApplication].statusBarHidden=NO;
    contentContainerView.userInteractionEnabled=YES;
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionLeft;
    [contentContainerView addGestureRecognizer:swiperight];
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionRight;
    [contentContainerView addGestureRecognizer:swipeleft];
    [self reloadTabButtons];
    buttonProcessDone=YES;
    pagenumber=0;
    
}

-(void)profileshow:(NSNotification *) notification;{

}
-(void)getMatchListWS{
    if (count<8) {
        
        [UIView transitionWithView:vi
                          duration:1.0f
                           options:UIViewAnimationOptionAutoreverse  | UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            
                        } completion:NULL];
        
        [UIView transitionWithView:feedsubVi
                          duration:1.0f
                           options:UIViewAnimationOptionAutoreverse  | UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            
                        } completion:NULL];
        
        count++;
        return;
    }
    [_timer invalidate];
    _timer = nil;
    return;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    topButtonscrollview.multipleTouchEnabled=NO;
    topButtonscrollview.exclusiveTouch=YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                    selector:@selector(notificationtoFeedMethod:)
                                    name:@"notificationtoFeed"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tabdeepFeedMethod:)
                                                 name:@"tabdeepLinking"
                                               object:nil];
    
    self.navigationController.navigationBarHidden=YES;
    count=0;
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"tabdeepLinking" object:nil];
}






- (void)notificationtoFeedMethod:(NSNotification *) notification{
    NSDictionary   *userInfo = notification.userInfo;
    NSString *pageScreen=[userInfo valueForKey:@"feedScreen"];
    if ([pageScreen isEqualToString:@"0"]) {
        [self setSelectedIndex:0 animated:YES];
//        pagenumber=1;
    }
    else{
        [self setSelectedIndex:3 animated:YES];
//        pagenumber=2;
    }
    
}

- (void)tabdeepFeedMethod:(NSNotification *) notification{
    NSDictionary   *userInfo = notification.userInfo;
    NSString *pageScreen=[userInfo valueForKey:@"tabScreen"];
    if(![pageScreen isEqualToString:@"(null)"] ){
        if ([pageScreen intValue]==0) {
            AppDelegate *getdeepData = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            getdeepData.tabFeedInfluenceDeep=@"feedclick";
        }
        [self setSelectedIndex:[pageScreen intValue] animated:YES];
    }
}


- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return YES;
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    // Removed scrollview in TopscrollView

    [self.view endEditing:YES];
//     CGPoint locapoint=[gestureRecognizer locationInView:self.view];
//    if(locapoint.y>100){
       buttonClikced=NO;
    swipedLeft=NO;
    swipedright=NO;
    
    if (pagenumber!=0) {
        pagenumber--;
        swipedLeft=YES;
        
        [self setSelectedIndex:pagenumber animated:YES];
    }
    if (pagenumber==3) {
        mainVi.hidden=YES;
    }
    
   // Scroll enabled No as per requirment
    scrolled=YES;
        
    
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    
    scrolled=YES;
    [self.view endEditing:YES];
    buttonClikced=NO;
    swipedright=NO;
    // as per requirmnent for scrolling  .. WITHOUT Pofile view
    if (pagenumber!=4) {
        pagenumber++;
        if (pagenumber== 1) {
            swipedright=YES;
        }
        [self setSelectedIndex:pagenumber animated:YES];
    }
    if (pagenumber==3) {
        mainVi.hidden=YES;
    }
//    NSLog(@"left");
    
    
    //   [self backPage];
}

-(void)popOverMenu{
    
    MENU_POPOVER_FRAME=CGRectMake(10,43, 170,80);
    
    self.menuItems = [NSArray arrayWithObjects:[UIImage imageNamed:@"prayer_selected"], [UIImage imageNamed:@"mystatus_selected"],[UIImage imageNamed:@"viewall_selected"],[UIImage imageNamed:@"viewall_selected"],nil];
    imageArray=[NSArray arrayWithObjects:[UIImage imageNamed:@"prayer_unselected"], [UIImage imageNamed:@"mystatus_unselected"],[UIImage imageNamed:@"viewall_unselected"],[UIImage imageNamed:@"viewall_selected"],nil];
    if (self.menuPopover) {
        [self.menuPopover dismissMenuPopover];
        self.menuPopover=nil;

    }
    
    self.menuPopover=[[MLKMenuPopover alloc]initWithFrame:MENU_POPOVER_FRAME menuItems:self.menuItems imagemenuItems:imageArray isSearch:NO];
    
    self.menuPopover.menuPopoverDelegate = self;
    [self.menuPopover showInView:self.view];
    
    
}


-(void)searchbuttonMethod:(UIButton*)searchBtn{
    
    MENU_POPOVER_FRAME=CGRectMake(10,43, 170,80);
    self.menuItems = [NSArray arrayWithObjects:@"",@"",nil];
    if (self.menuPopover) {
        [self.menuPopover dismissMenuPopover];
        self.menuPopover.menuPopoverDelegate=nil;
        self.menuPopover=nil;
    }
    [TenjinSDK sendEventWithName:@"menu_search_clicked"];
    [FBSDKAppEvents logEvent:@"menu_search_clicked"];
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"menu_search_clicked"]];
    self.menuPopover=[[MLKMenuPopover alloc]initWithFrame:MENU_POPOVER_FRAME menuItems:self.menuItems imagemenuItems:nil isSearch:YES];
    self.menuPopover.menuPopoverDelegate = self;
    [self.menuPopover showInView:self.view];
}

#pragma mark -
#pragma mark MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex
{
    [self.menuPopover dismissMenuPopover];
    
    if (menuPopover.isSearchPopview) {
        if (selectedIndex==0) {
            [TenjinSDK sendEventWithName:@"menu_search_friends"];
            [FBSDKAppEvents logEvent:@"menu_search_friends"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"menu_search_friends"]];

            
            [[NSUserDefaults standardUserDefaults] setObject:@"SearchCommunity" forKey:@"SearchCommunity"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"pageIndexZero"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self setSelectedIndex:4 animated:YES];
            pagenumber=4;
            
        }
        else{
            [TenjinSDK sendEventWithName:@"menu_search_groups"];
            [FBSDKAppEvents logEvent:@"menu_search_groups"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"menu_search_groups"]];
            [[NSUserDefaults standardUserDefaults] setObject:@"SearchCommunity" forKey:@"SearchCommunity"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"pageIndexZero"];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"groupSearch" forKey:@"SearchGroup"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self setSelectedIndex:3 animated:YES];
            pagenumber=3;
        }
        
    
    }
    else{
        
        if (selectedIndex==0) {
            NSDictionary *dict=@{@"filtertype":@"1"
                                 
                                 };
            
            [[NSNotificationCenter defaultCenter] postNotificationName: @"filter" object:nil userInfo:dict];
        }else  if (selectedIndex==1){
            NSDictionary *dict=@{@"filtertype":@"2"
                                 
                                 };
            
            [[NSNotificationCenter defaultCenter] postNotificationName: @"filter" object:nil userInfo:dict];
            
        }else  if (selectedIndex==2){
            NSDictionary *dict=@{@"filtertype":@"3"
                                 
                                 };
            
            [[NSNotificationCenter defaultCenter] postNotificationName: @"filter" object:nil userInfo:dict];
            
        }

    }
    
    
}



- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self layoutTabButtons];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Only rotate if all child view controllers agree on the new orientation.
    for (UIViewController *viewController in self.viewControllers)
    {
        if (![viewController shouldAutorotateToInterfaceOrientation:interfaceOrientation])
            return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    if ([self isViewLoaded] && self.view.window == nil)
    {
        self.view = nil;
        tabButtonsContainerView = nil;
        contentContainerView = nil;
        indicatorImageView = nil;
    }
}

- (void)reloadTabButtons
{
    [self removeTabButtons];
    [self addTabButtons];
    
    // Force redraw of the previously active tab.
    NSUInteger lastIndex = _selectedIndex;
    _selectedIndex = NSNotFound;
    self.selectedIndex = lastIndex;
}

- (void)addTabButtons
{
    NSUInteger index = 0;

    for (UIViewController *viewController in self.viewControllers)
    {
        
        UIView  *view=[[UIView alloc]init];
        view.tag=TagOffset + index;
        
        UIImageView *imagetab =[[UIImageView alloc] init];
        imagetab.image=viewController.tabBarItem.image;
        if (index==0) {
            imagetab.image=viewController.tabBarItem.selectedImage;
        }
        [view addSubview:imagetab];
        
        
        UILabel *titletab=[[UILabel alloc]init];
        if (index==0) {
            //titletab.textColor=UIColorFromRGB(0x278ddc);
            titletab.textColor=UIColorFromRGB(0x00BAF5);
        }
        else{
            //titletab.textColor=[UIColor darkGrayColor];
            titletab.textColor=UIColorFromRGB(0xC6CCCE);
        }
        titletab.text=viewController.tabBarItem.title;
        [view addSubview:titletab];

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundColor:[UIColor clearColor]];
        AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
            if (index==3) {
                appDelegate.notificationView=[[UIView alloc]initWithFrame:CGRectMake(view.frame.size.width/2-32,6,20,20)];
                appDelegate.notificationView.layer.cornerRadius = appDelegate.notificationView.bounds.size.width/2;
                appDelegate.notificationView.backgroundColor=[UIColor yellowColor];
                appDelegate.notificationLBL = [[UILabel alloc]initWithFrame:CGRectMake(1,1,18,18)];
                appDelegate.notificationLBL.backgroundColor=[UIColor redColor];
                appDelegate.notificationLBL.adjustsFontSizeToFitWidth = YES;
                appDelegate.notificationLBL.textAlignment = NSTextAlignmentCenter;
                appDelegate.notificationLBL.textColor=[UIColor whiteColor];
                appDelegate.notificationLBL.layer.masksToBounds = YES;
                //[appDelegate.notificationLBL setFont:[UIFont fontWithName:@"Aileron-SemiBold" size:8]];
                [appDelegate.notificationLBL setFont:[UIFont fontWithName:@"Avenir-Roman" size:8]];
                appDelegate.notificationLBL.layer.cornerRadius = appDelegate.notificationLBL.bounds.size.width/2;
                appDelegate.notificationView.layer.masksToBounds = YES;
                [appDelegate.notificationView addSubview:appDelegate.notificationLBL];
                appDelegate.notificationView.hidden=YES;
                appDelegate.notificationLBL.hidden=YES;
                [view addSubview:appDelegate.notificationView];
            }
        
        button.tag = TagOffset + index;
        if (IS_IPHONE_X || IS_IPHONE_XS_Max) {
            [titletab setFont:[UIFont fontWithName:@"Avenir-Roman" size:13.0]];
        }else if (IS_IPHONE6PLUS || IS_IPHONE6) {
            //[titletab setFont:[UIFont fontWithName:@"Aileron-SemiBold" size:12.0]];
            [titletab setFont:[UIFont fontWithName:@"Avenir-Roman" size:12.0]];
        }else{
            //[titletab setFont:[UIFont fontWithName:@"Aileron-SemiBold" size:10.5]];
            [titletab setFont:[UIFont fontWithName:@"Avenir-Roman" size:10.0]];
        }
        
 [button addTarget:self action:@selector(tabButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:button];
       
//        if ([viewController isKindOfClass:[FeedViewController class]]) {
//            
//        }else   if ([viewController isKindOfClass:[NotificationViewController class]]) {
//            [button addSubview:mainVi];
//        }
        [self deselectTabButton:button subview:nil];
        [tabButtonsContainerView addSubview:view];
        
        ++index;
    }
    NSLog(@"view controller count %ld",[self.viewControllers count]);
}

- (void)removeTabButtons
{
    while ([tabButtonsContainerView.subviews count] > 0)
    {
        [[tabButtonsContainerView.subviews lastObject] removeFromSuperview];
    }
}


#pragma divide tabber button size 

- (void)layoutTabButtons
{
    NSUInteger index = 0;
    NSUInteger counts = [self.viewControllers count];
    widthofbutton=self.view.bounds.size.width/5;
    CGRect rect = CGRectMake(0.0f, 0.0f,widthofbutton, self.tabBarHeight+3);
    indicatorImageView.hidden = YES;
    NSArray *viewarray = [tabButtonsContainerView subviews];
    for (UIView *view in viewarray)
    {
        if (index == counts - 1)
            //rect.size.width = self.view.bounds.size.width - rect.origin.x;
            NSLog(@"rect is%f", rect.size.width );
        view.frame = rect;
        rect.origin.x += widthofbutton;
        UIButton *button;
        UILabel *label;
        UIImageView *imgView;
        for(id object in view.subviews)
        {
            if([object isKindOfClass:[UILabel class]]){
                label=(UILabel *)object;
                if (IS_IPHONE6PLUS || IS_IPHONE_X || IS_IPHONE_XS_Max) {
                    label.frame= CGRectMake(0,48,view.frame.size.width,20);
                }else if (IS_IPHONE6) {
                    label.frame= CGRectMake(0,38,view.frame.size.width,20);
                } else
                if (IS_IPHONE5 || IS_IPHONE4) {
                    label.frame= CGRectMake(0,38,view.frame.size.width,20);
                }else {
                    label.frame= CGRectMake(0,48,view.frame.size.width,20);

                }
                
                label.textAlignment=NSTextAlignmentCenter;
                
            }
            else if([object isKindOfClass:[UIButton class]]){
                button=(UIButton *)object;
                button.frame= CGRectMake(0.0f, 0.0f,view.frame.size.width, view.frame.size.height);
                
            }else if([object isKindOfClass:[UIImageView class]]){
                imgView=(UIImageView *)object;
                if (IS_IPHONE6PLUS || IS_IPHONE_X || IS_IPHONE_XS_Max) {
                    imgView.frame =CGRectMake(view.frame.size.width/2-16, 10, 35,35);
                }else
                if (IS_IPHONE6) {
                    imgView.frame =CGRectMake(view.frame.size.width/2-14, 7, 30, 30);
                }else
                if (IS_IPHONE5 || IS_IPHONE4) {
                    imgView.frame =CGRectMake(view.frame.size.width/2-15, 8, 30, 30);
                }else{
                    imgView.frame =CGRectMake(view.frame.size.width/2-16, 10, 35,35);

                }
            }
            if (index == self.selectedIndex){
                if ([object isKindOfClass:[UIButton class]]) {
                    button=(UIButton *)object;
                    [self centerIndicatorOnButton:button];
                }
            }
        }
        
        ++index;
    }
    
    // add tabButtonContainerView  Y cordinate
    
    tabButtonsContainerView.frame=CGRectMake(0,0,widthofbutton*counts, self.tabBarHeight);
    topButtonscrollview.contentSize = CGSizeMake(widthofbutton*counts,0);
    // scrollView container from top as per requirment
    topButtonscrollview.frame= CGRectMake(0.0f, searchViewHeight, self.view.bounds.size.width, self.tabBarHeight);
    
    [self.view layoutIfNeeded];
    [topButtonscrollview setShowsVerticalScrollIndicator:NO];
    [topButtonscrollview setShowsHorizontalScrollIndicator:NO];
    [topButtonscrollview addSubview:tabButtonsContainerView];
    
}

- (void)centerIndicatorOnButton:(UIButton *)button
{
    if(pagenumber==4){
        UIView *view;
        UIButton *btn;
        view=[tabButtonsContainerView viewWithTag:1004];
        for(id object in view.subviews)
        {
        if([object isKindOfClass:[UIButton class]]){
            btn=(UIButton*)object;
            CGRect rect = indicatorImageView.frame;
            if (IS_IPHONE5||IS_IPHONE4) {
                rect.origin.x = view.frame.origin.x +floorf(indicatorImageView.frame.size.width+15);
            }else
            if (IS_IPHONE6) {
                rect.origin.x = view.frame.origin.x +floorf(indicatorImageView.frame.size.width+19);
            }else
            if (IS_IPHONE6PLUS) {
                rect.origin.x = view.frame.origin.x +floorf(indicatorImageView.frame.size.width+22);
            }else
            if (IS_IPHONE_X || IS_IPHONE_XS_Max) {
                rect.origin.x = view.frame.origin.x +floorf(indicatorImageView.frame.size.width+25);
            }else{
                rect.origin.x = view.frame.origin.x +floorf(indicatorImageView.frame.size.width+15);
            }
            rect.origin.y = (self.tabBarHeight+searchViewHeight) - indicatorImageView.frame.size.height;
            indicatorImageView.frame = rect;
            indicatorImageView.hidden = NO;
            }
        }
        return;
    }
    
    
    if(pagenumber==1){
        NSLog(@"not swiped");
        UIButton *btn;
        btn=[tabButtonsContainerView viewWithTag:1001];
        CGRect rect = indicatorImageView.frame;
        rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        rect.origin.y = (self.tabBarHeight+searchViewHeight) - indicatorImageView.frame.size.height;
        indicatorImageView.frame = rect;
        indicatorImageView.hidden = NO;
        return;
        
    }
    if(pagenumber==2){
        NSLog(@"not swiped");
             UIButton *btn=[tabButtonsContainerView viewWithTag:1002];
        CGRect rect = indicatorImageView.frame;
        rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        rect.origin.y = (self.tabBarHeight+searchViewHeight) - indicatorImageView.frame.size.height;
        indicatorImageView.frame = rect;
        indicatorImageView.hidden = NO;
        return;
        
    }  if(pagenumber==3){
        
        UIButton *btn=[tabButtonsContainerView viewWithTag:1003];
        CGRect rect = indicatorImageView.frame;
        rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        rect.origin.y = (self.tabBarHeight+searchViewHeight) - indicatorImageView.frame.size.height;
        indicatorImageView.frame = rect;
        indicatorImageView.hidden = NO;
        return;
        
    }
    CGRect rect = indicatorImageView.frame;
    rect.origin.x = button.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
    rect.origin.y = (self.tabBarHeight+searchViewHeight) - indicatorImageView.frame.size.height;
    indicatorImageView.frame = rect;
    indicatorImageView.hidden = NO;
}

- (void)setViewControllers:(NSArray *)newViewControllers
{
    NSAssert([newViewControllers count] >= 2, @"MHTabBarController requires at least two view controllers");
    
    UIViewController *oldSelectedViewController = self.selectedViewController;
    
    // Remove the old child view controllers.
    for (UIViewController *viewController in _viewControllers)
    {
        [viewController willMoveToParentViewController:nil];
        [viewController removeFromParentViewController];
    }
    _viewControllers = [newViewControllers copy];
      NSUInteger newIndex = [_viewControllers indexOfObject:oldSelectedViewController];
    if (newIndex != NSNotFound)
        _selectedIndex = newIndex;
    else if (newIndex < [_viewControllers count])
        _selectedIndex = newIndex;
    else
        _selectedIndex = 0;
    
    // Add the new child view controllers.
    for (UIViewController *viewController in _viewControllers)
    {
        [self addChildViewController:viewController];
        [viewController didMoveToParentViewController:self];
    }
    
    if ([self isViewLoaded])
        [self reloadTabButtons];
}

- (void)setSelectedIndex:(NSUInteger)newSelectedIndex
{
    [self setSelectedIndex:newSelectedIndex animated:NO];
}

- (void)setSelectedIndex:(NSUInteger)newSelectedIndex animated:(BOOL)animated
{
    if (newSelectedIndex!=0) {
        [AppDelegate sharedAppDelegate].selectedTabBool=YES;
    }
    
    NSAssert(newSelectedIndex < [self.viewControllers count], @"View controller index out of bounds");
    
    if ([self.delegate respondsToSelector:@selector(mh_tabBarController:shouldSelectViewController:atIndex:)])
    {
        UIViewController *toViewController = (self.viewControllers)[newSelectedIndex];
        if (![self.delegate mh_tabBarController:self shouldSelectViewController:toViewController atIndex:newSelectedIndex])
            return;
    }
   
    
    if (![self isViewLoaded])
    {
        _selectedIndex = newSelectedIndex;
    }
    else if (_selectedIndex != newSelectedIndex)
    {
        UIViewController *fromViewController;
        UIViewController *toViewController;
        UIView *tabView;
        UIButton *tabBtn;
        
        if (_selectedIndex != NSNotFound)
        {
            tabView = (UIView *)[tabButtonsContainerView viewWithTag:TagOffset + _selectedIndex];
            for (id object in tabView.subviews)
            {
                if ([object isKindOfClass:[UIButton class]]) {
                    tabBtn= (UIButton *)[object viewWithTag:TagOffset + _selectedIndex];
                    [self deselectTabButton:tabBtn subview:tabView];
                }
            }
            
            fromViewController = self.selectedViewController;
        }
        
        NSUInteger oldSelectedIndex = _selectedIndex;
        _selectedIndex = newSelectedIndex;
        
        
        if (_selectedIndex != NSNotFound)
        {
            tabView = (UIView *)[tabButtonsContainerView viewWithTag:TagOffset + _selectedIndex];
            for (id object in tabView.subviews)
                {
                if ([object isKindOfClass:[UIButton class]]) {
                    tabBtn= (UIButton *)[object viewWithTag:TagOffset + _selectedIndex];
                    [self selectTabButton:tabBtn subview:tabView];
                }

            }
            
            
            toViewController = self.selectedViewController;
        }
        
        if (toViewController == nil)  // don't animate
        {
            [fromViewController.view removeFromSuperview];
        }
        else if (fromViewController == nil)  // don't animate
        {
            toViewController.view.frame = contentContainerView.bounds;
            [contentContainerView addSubview:toViewController.view];
            
            tabView = (UIView *)[tabButtonsContainerView viewWithTag:TagOffset + _selectedIndex];
            for (id object in tabView.subviews)
            {
                if ([object isKindOfClass:[UIButton class]]) {
                    tabBtn= (UIButton *)[object viewWithTag:TagOffset + _selectedIndex];
                    [self centerIndicatorOnButton:tabBtn];
                }
            }
            
            if ([self.delegate respondsToSelector:@selector(mh_tabBarController:didSelectViewController:atIndex:)])
                [self.delegate mh_tabBarController:self didSelectViewController:toViewController atIndex:newSelectedIndex];
//            buttonProcessDone=YES;

        }
        else if (animated)
        {
            CGRect rect = contentContainerView.bounds;
            if (oldSelectedIndex < newSelectedIndex)
                rect.origin.x = rect.size.width;
            else
                rect.origin.x = -rect.size.width;
            
            toViewController.view.frame = rect;
            tabButtonsContainerView.userInteractionEnabled = NO;
            
            [self transitionFromViewController:fromViewController
                              toViewController:toViewController
                                      duration:0.001f
                                       options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionCurveEaseOut
                                    animations:^
             {
                 CGRect rect = fromViewController.view.frame;
                 if (oldSelectedIndex < newSelectedIndex)
                     rect.origin.x = -rect.size.width;
                 else
                     rect.origin.x = rect.size.width;
                 
                 fromViewController.view.frame = rect;
                 toViewController.view.frame = contentContainerView.bounds;
                 UIButton *tabBtn;
                 for (id object in tabView.subviews)
                 {
                     if ([object isKindOfClass:[UIButton class]]) {
                         tabBtn= (UIButton *)[object viewWithTag:TagOffset + _selectedIndex];
                         [self centerIndicatorOnButton:tabBtn];
                     }
                 }
                 
//                 [self centerIndicatorOnButton:tabBtn];
             }
                                    completion:^(BOOL finished)
             {
                 tabButtonsContainerView.userInteractionEnabled = YES;
                 
                 if ([self.delegate respondsToSelector:@selector(mh_tabBarController:didSelectViewController:atIndex:)])
                     [self.delegate mh_tabBarController:self didSelectViewController:toViewController atIndex:newSelectedIndex];
//                 buttonProcessDone=YES;

                 
             }];
        }
        else  // not animated
        {
            [fromViewController.view removeFromSuperview];
            toViewController.view.frame = contentContainerView.bounds;
            [contentContainerView addSubview:toViewController.view];
            UIButton *tabBtn;
            for (id object in tabView.subviews)
            {
                if ([object isKindOfClass:[UIButton class]]) {
                    tabBtn= (UIButton *)[object viewWithTag:TagOffset + _selectedIndex];
                    [self centerIndicatorOnButton:tabBtn];
                }
            }
            
//            [self centerIndicatorOnButton:tabBtn];
            
            if ([self.delegate respondsToSelector:@selector(mh_tabBarController:didSelectViewController:atIndex:)])
                [self.delegate mh_tabBarController:self didSelectViewController:toViewController atIndex:newSelectedIndex];
//            buttonProcessDone=YES;

            
        }
    }
    
// ScrollView Offset Remove as per Requirment
    
    //[topButtonscrollview setContentOffset:CGPointMake(0, -20) animated:YES];
    if (pagenumber==2) {
//        [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
        
        
    }
    if (pagenumber==3) {
//        [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
        
        
    }
    
    if (pagenumber==1) {
        if (swipedright==YES) {
//            [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
        }else{
 
//            [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
        }
    }
    if (pagenumber==0) {
        if (swipedLeft==YES) {
 
//            [topButtonscrollview setContentOffset:CGPointMake(0, -20) animated:YES];
            
        }
    }
    
// commented code as per requirment that is Profile View
    
//    if (pagenumber==4) {
//        //[topButtonscrollview setContentOffset:CGPointMake(400, 0)];
//        NSLog(@"is%f",widthofbutton*4);
//        [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
//    }
    
    
    scrolled=NO;
}

- (UIViewController *)selectedViewController
{
    if (self.selectedIndex != NSNotFound)
        return (self.viewControllers)[self.selectedIndex];
    else
        return nil;
}

- (void)setSelectedViewController:(UIViewController *)newSelectedViewController
{
    [self setSelectedViewController:newSelectedViewController animated:NO];
}

- (void)setSelectedViewController:(UIViewController *)newSelectedViewController animated:(BOOL)animated
{
    NSUInteger index = [self.viewControllers indexOfObject:newSelectedViewController];
    if (index != NSNotFound)
        [self setSelectedIndex:index animated:animated];
}

- (void)tabButtonPressed:(UIButton *)sender
{
    if (buttonProcessDone) {
        buttonProcessDone=YES;
        [self rotatelogoImage];
        NSLog(@"sender.tag is%ld",(long)sender.tag-TagOffset);
        [self setSelectedIndex:sender.tag - TagOffset animated:YES];
        pagenumber=sender.tag-TagOffset;
        if (pagenumber==2) {
            AppDelegate *appDelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
            appDelegate.notificationView.hidden=YES;
            appDelegate.notificationLBL.hidden=YES;
        }
        else{
            [self notificationcount];
            [self rotateNotificationview];
            
        }
        
    }
   }

#pragma mark -notificationcount

-(void)notificationcount{
    
    AppDelegate *appDelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        return;
    }
    else {
            NSDictionary *dict = @{
                                   @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                                   @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                                   };
            [[WebServiceHelper sharedInstance] call_notificationPostDataWithMethod:getnotificationcount withParameters:dict withHud:NO success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSString *countnoti=[NSString stringWithFormat:@"%@",[response valueForKey:@"data"]];
                            if ([countnoti  intValue]>0) {
                                if ([countnoti  intValue]>99) {
                                    appDelegate.notificationLBL.text=@"99+";
                                }
                                appDelegate.notificationLBL.text=[NSString stringWithFormat:@"%@",[response valueForKey:@"data"]];
                                appDelegate.notificationView.hidden=NO;
                                appDelegate.notificationLBL.hidden=NO;
                            }
                            else{
                                appDelegate.notificationView.hidden=YES;
                                appDelegate.notificationLBL.hidden=YES;
                            }
                        });
                    }
                }
            } errorBlock:^(id error)
             {
                 
             }];
    }
    
}

#pragma mark RotateNotificationView

-(void)rotatelogoImage
{
    [logoImge.layer removeAnimationForKey:@"rotationAnimation"];
    [logoImge removeFromSuperview];
    CGRect    screenRect = [[UIScreen mainScreen] bounds];
    logoImge=[[UIImageView alloc]initWithFrame:CGRectMake(screenRect.size.width/2-15,4,30, 30)];
    logoImge.image=[UIImage imageNamed:@"indicator_logo1"];
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    [rotationAnimation setToValue:[NSNumber numberWithFloat:((2*M_PI))]];
    rotationAnimation.duration = 2.0;
    rotationAnimation.cumulative = NO;
    rotationAnimation.speed = 3.0f;
    rotationAnimation.repeatCount =2 ;
    [logoImge.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    [searchView addSubview:logoImge];
    
}

#pragma mark RotateImage
-(void)rotateNotificationview
{
    // flip Rotation
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    [rotationAnimation setToValue:[NSNumber numberWithFloat:((2*M_PI))]];
    rotationAnimation.duration = 2.0;
    rotationAnimation.cumulative = YES;
    rotationAnimation.speed = 1.5f;
    rotationAnimation.repeatCount =2 ;
    [appDelegate.notificationView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}



#pragma mark - Change these methods to customize the look of the buttons

- (void)selectTabButton:(UIButton *)button subview:(UIView*)subview
{
    UILabel *label;
    UIImageView *imgView;
    
    for(id object in subview.subviews)
    {
        if([object isKindOfClass:[UILabel class]]){
            label=(UILabel *)object;
            label.textAlignment=NSTextAlignmentCenter;
            //label.textColor=UIColorFromRGB(0x278ddc);
            label.textColor=UIColorFromRGB(0x00BAF5);
        }
        else if([object isKindOfClass:[UIImageView class]]){
            imgView=(UIImageView *)object;
            
            if (button.tag==1000) {
                imgView.image = [UIImage imageNamed:@"feed_sel40"];
            }
            if (button.tag==1001) {
                imgView.image = [UIImage imageNamed:@"influencer_sel60"];
            }
            
            if (button.tag==1002) {
                imgView.image = [UIImage imageNamed:@"not_sel60"];
            }
            if (button.tag==1003) {
                imgView.image = [UIImage imageNamed:@"groups_sel60"];
            }

            if (button.tag==1004) {
                imgView.image = [UIImage imageNamed:@"community_sel.60png"];
            }

        }
    }
    
    NSLog(@"sender.tag is%ld",(long)button.tag);
    scrolled=YES;
    buttonClikced=YES;
    button.selected=YES;
    //[button setTitleColor:UIColorFromRGB(0x278ddc) forState:UIControlStateNormal];
    [button setTitleColor:UIColorFromRGB(0x00BAF5) forState:UIControlStateNormal];
    
    if (button.tag==1000) {
        pagenumber=0;
        NSString *currentDate=[self createTimeStamp];
        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Last Page Load":currentDate}];
        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Last Page Load": currentDate}];
        [[AppDelegate sharedAppDelegate].mxPanel track:@"Page loaded"
            properties:@{ @"Page name":@"Feed",@"Loaded Time":currentDate,@"Last Page Load": currentDate}];
        
        feedFilter.hidden=YES;
//        [self centerIndicatorOnButton:button];

    }
    if (button.tag==1001) {
        pagenumber=1;
    }
    
    if (button.tag==1002) {
        pagenumber=2;
        
    }
    if (button.tag==1003) {
        pagenumber=3;
        mainVi.hidden=YES;
    }
    if (button.tag==1004) {
        pagenumber=4;
    }
    
    
}

-(NSString*)createTimeStamp {
    NSDate *today = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeZone *timeZone1 = [[NSTimeZone alloc] initWithName:@"UTC"];;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:timeZone1];
    NSString *dataFormatStr=[NSString stringWithFormat:@"yyyy/MM/dd HH:mm:ss.SSS '%@'",[timeZone1 localizedName:NSTimeZoneNameStyleShortStandard locale:[NSLocale currentLocale]]];
    [dateFormat setDateFormat:dataFormatStr];
    NSString *dateString = [dateFormat stringFromDate:today];
    NSString *timestamp = [NSString stringWithFormat:@"%@",dateString];
    return timestamp;
}




#pragma change backgroundcolor of tabber.

- (void)deselectTabButton:(UIButton *)button subview:(UIView*)subview
{
    UILabel *label;
    UIImageView *imgView;
    
    for(id object in subview.subviews)
    {
        if([object isKindOfClass:[UILabel class]]){
            label=(UILabel *)object;
            label.textAlignment=NSTextAlignmentCenter;
            //label.textColor=[UIColor darkGrayColor];
            label.textColor=UIColorFromRGB(0xC6CCCE);

        }
        else if([object isKindOfClass:[UIImageView class]]){
            imgView=(UIImageView *)object;
            
            if (button.tag==1000) {
                imgView.image = [UIImage imageNamed:@"feed_unsel60"];

            }
            if (button.tag==1001) {
                imgView.image = [UIImage imageNamed:@"influencer_unsel60"];
            }
            
            if (button.tag==1002) {
                imgView.image = [UIImage imageNamed:@"not_unsel60"];
            }
            if (button.tag==1003) {
                imgView.image = [UIImage imageNamed:@"groups_unsel60"];
            }
            
            if (button.tag==1004) {
                imgView.image = [UIImage imageNamed:@"community_unsel60"];
                
            }
            
        }
        
    }
    
    feedFilter.hidden=YES;
    button.selected=NO;
//   [self.view setBackgroundColor:UIColorFromRGB(0x278ddc)];
//    [tabButtonsContainerView setBackgroundColor:UIColorFromRGB(0xe2e3e5)];
    [self.view setBackgroundColor:UIColorFromRGB(0x00BAF5)];
    [self.safeView setBackgroundColor:UIColorFromRGB(0x00BAF5)];
    [tabButtonsContainerView setBackgroundColor:[UIColor whiteColor]];
    
}

- (CGFloat)tabBarHeight
{
    if (IS_IPHONE6PLUS || IS_IPHONE_X || IS_IPHONE_XS_Max) {
        return 75.0;
    }
    return 65.0f;
}
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ( [gestureRecognizer isMemberOfClass:[UITapGestureRecognizer class]] ) {
        // Return NO for views that don't support Taps
    } else if ( [gestureRecognizer isMemberOfClass:[UISwipeGestureRecognizer class]] ) {
        // Return NO for views that don't support Swipes
        
    }
    
    return YES;
}
-(void)scrollViewDidScroll:(UIScrollView *)sender
{
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"contentOffset is%f",topButtonscrollview.contentOffset.x);
    NSLog(@"widthofbutton is%f",widthofbutton);
    NSLog(@"indicatorImageView is%f",indicatorImageView.center.x);
    if(topButtonscrollview.contentOffset.x>=widthofbutton){
        NSLog(@"swiped left");
        if(indicatorImageView.center.x<widthofbutton){
            pagenumber=1;
            [self setSelectedIndex:pagenumber animated:YES];
        }
    }else if (topButtonscrollview.contentOffset.x==0){
        NSLog(@"back to feed left");
        pagenumber=0;
        [self setSelectedIndex:pagenumber animated:YES];
    }
    
}
@end
