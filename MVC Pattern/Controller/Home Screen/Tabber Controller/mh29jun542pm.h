

#import "MHTabBarController.h"
#import "FeedViewController.h"
#import "NotificationViewController.h"
#import "mycommunityViewcontroller.h"
#import "MLKMenuPopover.h"
#import "Profile_editViewController.h"
#import "WebServiceHelper.h"
#import "Constant1.h"
#import "DeviceConstant.h"
#import "HomeViewController.h"
#import "GroupViewController.h"







static const NSInteger TagOffset = 1000;


@interface MHTabBarController ()<MLKMenuPopoverDelegate,UIScrollViewDelegate>{
    CGRect MENU_POPOVER_FRAME;
    UIButton *feedFilter;
    UIView *vi,*mainVi;
    UIView *feedsubVi,*feedMainVi;
    
    int count;
    UILabel *notifylbl,*post_notifylbl;
    
    UIView *tabButtonsContainerView;
    UIView *contentContainerView;
    UIImageView *indicatorImageView;
    
    Profile_editViewController* listViewController5;
    UIButton *redviewbtn;
    UIView *popUpView;
    NSUInteger pagenumber;
    
    NSArray *imageArray;
    NSTimer *_timer;
    
    UIScrollView *topButtonscrollview;
    CGFloat widthofbutton;
    BOOL swiped,scrolled,swipedLeft,swipedright,buttonClikced;
}
@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property(nonatomic,strong) NSArray *menuItems;
@end
@implementation MHTabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    CGRect    screenRect = [[UIScreen mainScreen] bounds];
    topButtonscrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 10, screenRect.size.width,60)];
    //topButtonscrollview.backgroundColor=[UIColor redColor];
    topButtonscrollview.delegate=self;
    topButtonscrollview.scrollEnabled=YES;
    [self.view addSubview:topButtonscrollview];
    
    
    
    
    
    FeedViewController *listViewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedViewController"];
    GroupViewController *listViewController2= [self.storyboard instantiateViewControllerWithIdentifier:@"GroupviewPagecontrollerViewController"];
    mycommunityViewcontroller *listViewController3 = [self.storyboard instantiateViewControllerWithIdentifier:@"mycommunityViewcontroller"];
    NotificationViewController *listViewController4= [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    listViewController5 = [self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
    listViewController1.title = @"Feed";
    listViewController2.title=@"Group";
    listViewController3.title = @"Commmunity";
    listViewController4.title = @"Notification";
    listViewController5.title = @"Profile";
    
    listViewController1.tabBarItem.selectedImage = [UIImage imageNamed:@"feed_Selected"];
    listViewController1.tabBarItem.image = [UIImage imageNamed:@"feed_UnSelected"];
    listViewController2.tabBarItem.selectedImage = [UIImage imageNamed:@"group_selected"];
    listViewController2.tabBarItem.image = [UIImage imageNamed:@"group_unselected"];
    listViewController3.tabBarItem.selectedImage = [UIImage imageNamed:@"mycommunity_Selected"];
    listViewController3.tabBarItem.image = [UIImage imageNamed:@"my_community_Unselected"];
    listViewController4.tabBarItem.selectedImage = [UIImage imageNamed:@"notification_Selected"];
    listViewController4.tabBarItem.image = [UIImage imageNamed:@"notification_Unselected"];
    listViewController5.tabBarItem.selectedImage = [UIImage imageNamed:@"profile_Selected"];
    listViewController5.tabBarItem.image = [UIImage imageNamed:@"profile_Unselected"];
    
    
    
    
    
    NSArray *viewControllers = @[listViewController1,listViewController2,listViewController3,listViewController4,listViewController5];
    self.viewControllers = viewControllers;
    if (IS_IPHONE4) {
        listViewController1.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController1.tabBarItem.titlePositionAdjustment = UIOffsetMake(-25.0f, 25.0f);
        listViewController2.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController2.tabBarItem.titlePositionAdjustment = UIOffsetMake(-15.0f, 25.0f);
        listViewController3.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, 30.0f, 0.0f, 0.0f);
        listViewController3.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        listViewController4.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, 30.0f, 0.0f, 0.0f);
        listViewController4.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        listViewController5.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController5.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        
        
        
    }else if(IS_IPHONE6PLUS){
        //(top)
        listViewController1.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController1.tabBarItem.titlePositionAdjustment = UIOffsetMake(-30.0f, 35.0f);  //(left,top)
        listViewController2.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, 40.0f, 0.0f, 0.0f);
        listViewController2.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 35.0f);
        listViewController3.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, 40.0f, 0.0f, 0.0f);
        listViewController3.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 35.0f);
        listViewController4.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +40.0f, 0.0f, 0.0f);
        listViewController4.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 35.0f);
        listViewController5.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +40.0f, 0.0f, 0.0f);
        listViewController5.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 35.0f);
        
        
    }
    else if(IS_IPHONE5){
        listViewController1.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +25.0, 0.0f, 0.0f);//(top,left)
        listViewController1.tabBarItem.titlePositionAdjustment = UIOffsetMake(-25.0f, 30.0f);
        listViewController2.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +25.0, 0.0f, 0.0f);//(top,left)
        listViewController2.tabBarItem.titlePositionAdjustment = UIOffsetMake(-25.0f, 30.0f);
        listViewController3.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +25.0, 0.0f, 0.0f);//(top,left)
        listViewController3.tabBarItem.titlePositionAdjustment = UIOffsetMake(-25.0f, 30.0f);
        listViewController4.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +25.0, 0.0f, 0.0f);//(top,left)
        listViewController4.tabBarItem.titlePositionAdjustment = UIOffsetMake(-25.0f, 30.0f);
        listViewController5.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +25.0, 0.0f, 0.0f);//(top,left)
        listViewController5.tabBarItem.titlePositionAdjustment = UIOffsetMake(-25.0f, 30.0f);
    }
    else if(IS_IPHONE6){
        listViewController1.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +25.0, 0.0f, 0.0f);
        listViewController1.tabBarItem.titlePositionAdjustment = UIOffsetMake(-25.0f, 30.0f);
        listViewController2.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +25.0, 0.0f, 0.0f);
        listViewController2.tabBarItem.titlePositionAdjustment = UIOffsetMake(-35.0, 30.0f);
        listViewController3.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +35.0, 0.0f, 0.0f);
        listViewController3.tabBarItem.titlePositionAdjustment = UIOffsetMake(-25.0f, 30.0f);
        listViewController4.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +35.0, 0.0f, 0.0f);
        listViewController4.tabBarItem.titlePositionAdjustment = UIOffsetMake(-25.0f, 30.0f);
        listViewController5.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +35.0, 0.0f, 0.0f);
        listViewController5.tabBarItem.titlePositionAdjustment = UIOffsetMake(-25.0f, 30.0f);
    }
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    CGRect rect = CGRectMake(0.0f, 0, self.view.bounds.size.width, self.tabBarHeight);
    CGRect rect2 = CGRectMake(0.0f, 22.0f, self.view.bounds.size.width, self.tabBarHeight);
    
    topButtonscrollview.frame=rect2;
    
    tabButtonsContainerView = [[UIView alloc] initWithFrame:rect];
    tabButtonsContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    
    
    // tabButtonsContainerView.backgroundColor=[UIColor greenColor];
    
    
    rect.origin.y = self.tabBarHeight+22.0;
    rect.size.height = self.view.bounds.size.height -(self.tabBarHeight+22);
    
    contentContainerView = [[UIView alloc] initWithFrame:rect];
    contentContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    // contentContainerView.backgroundColor=[UIColor redColor];
    [self.view addSubview:contentContainerView];
    indicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MHTabBarIndicator"]];
    [self.view addSubview:indicatorImageView];
    
    
    
    
    
    MENU_POPOVER_FRAME=CGRectMake(screenRect.size.width-170, 56, 170, 130);
    self.menuItems = [NSArray arrayWithObjects:[UIImage imageNamed:@"prayer_selected"], [UIImage imageNamed:@"mystatus_selected"],[UIImage imageNamed:@"viewall_selected"],[UIImage imageNamed:@"viewall_selected"],nil];
    imageArray=[NSArray arrayWithObjects:[UIImage imageNamed:@"prayer_unselected"], [UIImage imageNamed:@"mystatus_unselected"],[UIImage imageNamed:@"viewall_unselected"],[UIImage imageNamed:@"viewall_selected"],nil];
    feedFilter= [UIButton buttonWithType:UIButtonTypeCustom];
    [feedFilter addTarget:self
                   action:@selector(popOverMenu)
         forControlEvents:UIControlEventTouchUpInside];
    [feedFilter setImage:[UIImage imageNamed:@"dot"] forState:UIControlStateNormal];
    feedFilter.frame =CGRectMake(screenRect.size.width-25, contentContainerView.frame.origin.y-44, 30, 44);
    if (IS_IPHONE6PLUS) {
        feedFilter.frame =CGRectMake(screenRect.size.width-25, contentContainerView.frame.origin.y-50, 30, 44);
    }
    [self.view addSubview:feedFilter];
    feedFilter.hidden=NO;
    [UIApplication sharedApplication].statusBarHidden=NO;
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swiperight];
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeleft];
    self.view.userInteractionEnabled=YES;
    
    
    
    
    vi=[[UIView alloc]init];
    vi.layer.cornerRadius=vi.frame.size.width/2;
    vi.layer.borderColor=[UIColor yellowColor].CGColor;
    vi.layer.borderWidth=1.0f;
    vi.backgroundColor=[UIColor redColor];
    notifylbl = [[UILabel alloc] init];
    notifylbl.textColor=[UIColor yellowColor];
    notifylbl.textAlignment=NSTextAlignmentCenter;
    [notifylbl setFont:[UIFont fontWithName:@"Avenir-Medium" size:8.0f]];
    notifylbl.userInteractionEnabled=NO;
    notifylbl.text= @"12";
    mainVi=[[UIView alloc]init];
    feedsubVi=[[UIView alloc]init];
    feedsubVi.layer.cornerRadius=vi.frame.size.width/2;
    feedsubVi.layer.borderColor=[UIColor yellowColor].CGColor;
    feedsubVi.layer.borderWidth=1.0f;
    feedsubVi.backgroundColor=[UIColor redColor];
    post_notifylbl = [[UILabel alloc] init];
    post_notifylbl.textColor=[UIColor yellowColor];
    post_notifylbl.textAlignment=NSTextAlignmentCenter;
    [post_notifylbl setFont:[UIFont fontWithName:@"Avenir-Medium" size:8.0f]];
    post_notifylbl.userInteractionEnabled=NO;
    post_notifylbl.text= @"12";
    feedMainVi=[[UIView alloc]init];
    CGRect mainrect,feedmainrect,feedrect;
    CGRect animateVIewrect;
    if (IS_IPHONE4||IS_IPHONE5) {
        rect.origin.x =screenRect.size.width-124;
        rect.origin.y = 0;
        
        feedrect.origin.x =40;
    }else if (IS_IPHONE6) {
        rect.origin.x =screenRect.size.width-144;
        rect.origin.y =0;
        
        
        feedrect.origin.x =45;
    }
    animateVIewrect=CGRectMake(5,5,12, 12);
    mainrect=CGRectMake(rect.origin.x, rect.origin.y, 15, 15);
    notifylbl.frame=CGRectMake(0, 0, 12, 12);
    post_notifylbl.frame=CGRectMake(0, 0, 12, 12);
    feedmainrect=CGRectMake(feedrect.origin.x, rect.origin.y, 15, 15);
    if (IS_IPHONE6PLUS) {
        rect.origin.x =screenRect.size.width-164;
        rect.origin.y =0;
        animateVIewrect=CGRectMake(5,5,17, 17);
        mainrect=CGRectMake(rect.origin.x, rect.origin.y, 20, 20);
        notifylbl.frame=CGRectMake(0, 0, 15, 15);
        notifylbl.frame=CGRectMake(0, 0, 15, 15);
        feedrect.origin.x =50;
        post_notifylbl.frame=CGRectMake(0, 0, 15, 15);
        feedmainrect=CGRectMake(feedrect.origin.x, rect.origin.y, 20, 20);
    }
    vi.layer.cornerRadius=animateVIewrect.size.width/2;
    vi.frame=animateVIewrect;
    vi.clipsToBounds=YES;
    mainVi.frame=feedmainrect;
    mainVi.backgroundColor=[UIColor clearColor];
    [mainVi addSubview:vi];
    [vi addSubview:notifylbl];
    
    mainVi.hidden=YES
    ;
    feedsubVi.layer.cornerRadius=animateVIewrect.size.width/2;
    feedsubVi.frame=animateVIewrect;
    feedsubVi.clipsToBounds=YES;
    feedMainVi.frame=feedmainrect;
    feedMainVi.backgroundColor=[UIColor clearColor];
    [feedMainVi addSubview:feedsubVi];
    [feedsubVi addSubview:post_notifylbl];
    
    
    feedMainVi.hidden=YES;
    
    
    [self reloadTabButtons];
    
    
    if (_notification==1) {
        pagenumber=_notification;
        listViewController3.subnotification=_subnotification;
        [self setSelectedIndex:pagenumber animated:YES];
    }
    pagenumber=0;
}


-(void)profileshow:(NSNotification *) notification;{
    pagenumber=4;
    [self setSelectedIndex:4 animated:YES];
}
-(void)getMatchListWS{
    
    
    if (count<8) {
        
        [UIView transitionWithView:vi
                          duration:1.0f
                           options:UIViewAnimationOptionAutoreverse  | UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            
                        } completion:NULL];
        
        [UIView transitionWithView:feedsubVi
                          duration:1.0f
                           options:UIViewAnimationOptionAutoreverse  | UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            
                        } completion:NULL];
        
        
        
        count++;
        return;
    }
    [_timer invalidate];
    //remember to set timer to nil after calling invalidate;
    _timer = nil;
    return;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileshow:)
                                                 name:@"profileshow"
                                               object:nil];
    
    
    
    self.navigationController.navigationBarHidden=YES;
    count=0;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:@"profileshow"];
}



- (void) receiveTestNotification:(NSNotification *) notification{
    count=0;
    
    NSDictionary   *userInfo = notification.userInfo;
    
    NSString *notification_count=[userInfo objectForKey:@"notification_count"];
    int not_count=[notification_count intValue];
    NSString *post_count=[userInfo objectForKey:@"post_count"];
    int pt_count=[post_count intValue];
//    NSLog(@"obpost_countject is%@",post_count);
    
    
    
    
    //NSNumber *myObject = [userInfo objectForKey:@"count"];
    //  NSLog(@"object is%@",myObject);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        notifylbl.text=[NSString stringWithFormat:@"%d",not_count];
        mainVi.hidden=NO;
        
        post_notifylbl.text=[NSString stringWithFormat:@"%d",pt_count];
        feedMainVi.hidden=NO;
        _timer=   [NSTimer scheduledTimerWithTimeInterval:1.0
                                                   target:self
                                                 selector:@selector(getMatchListWS)
                                                 userInfo:nil
                                                  repeats:YES];
        
    });
    
}



-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self.view endEditing:YES];
    buttonClikced=NO;
    swipedLeft=NO;
    swipedright=NO;
    
    if (pagenumber!=0) {
        pagenumber--;
        if (pagenumber==0) {
            swipedLeft=YES;
        }
        [self setSelectedIndex:pagenumber animated:YES];
        
    }
    if (pagenumber==3) {
        mainVi.hidden=YES;
    }
    
    scrolled=YES;
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    scrolled=YES;
    [self.view endEditing:YES];
    buttonClikced=NO;
    /* NSUInteger index;
     if (pagenumber!=1003) {
     
     if (pagenumber==1000) {
     index=0;
     }else if (pagenumber==1001){
     index=1;
     }else if (pagenumber==1002){
     index=2;
     }
     
     [self setSelectedIndex:index+1 animated:YES];
     pagenumber++;
     }*/
    swipedright=NO;
    
    
    if (pagenumber!=4) {
        
        
        pagenumber++;
        if (pagenumber== 1) {
            swipedright=YES;
        }
        
        [self setSelectedIndex:pagenumber animated:YES];
        
    }
    if (pagenumber==3) {
        mainVi.hidden=YES;
    }
//    NSLog(@"left");
    
    //   [self backPage];
}

-(void)popOverMenu{
    
    [self.menuPopover dismissMenuPopover];
    
    self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:MENU_POPOVER_FRAME menuItems:self.menuItems imagemenuItems:imageArray];
    
    self.menuPopover.menuPopoverDelegate = self;
    [self.menuPopover showInView:self.view];
    
    
    
}





#pragma mark -
#pragma mark MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex
{
    [self.menuPopover dismissMenuPopover];
    
    if (selectedIndex==0) {
        NSDictionary *dict=@{@"filtertype":@"1"
                             
                             };
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"filter" object:nil userInfo:dict];
    }else  if (selectedIndex==1){
        NSDictionary *dict=@{@"filtertype":@"2"
                             
                             };
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"filter" object:nil userInfo:dict];
        
    }else  if (selectedIndex==2){
        NSDictionary *dict=@{@"filtertype":@"3"
                             
                             };
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"filter" object:nil userInfo:dict];
        
    }
}



- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self layoutTabButtons];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Only rotate if all child view controllers agree on the new orientation.
    for (UIViewController *viewController in self.viewControllers)
    {
        if (![viewController shouldAutorotateToInterfaceOrientation:interfaceOrientation])
            return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    if ([self isViewLoaded] && self.view.window == nil)
    {
        self.view = nil;
        tabButtonsContainerView = nil;
        contentContainerView = nil;
        indicatorImageView = nil;
    }
}

- (void)reloadTabButtons
{
    [self removeTabButtons];
    [self addTabButtons];
    
    // Force redraw of the previously active tab.
    NSUInteger lastIndex = _selectedIndex;
    _selectedIndex = NSNotFound;
    self.selectedIndex = lastIndex;
}

- (void)addTabButtons
{
    NSUInteger index = 0;
    for (UIViewController *viewController in self.viewControllers)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = TagOffset + index;
        button.titleLabel.font = [UIFont boldSystemFontOfSize:9];
        
        button.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        button.layer.borderWidth=0.4f;
        button.layer.borderColor=[UIColor whiteColor].CGColor;
        //   button.titleLabel.textColor=[UIColor blackColor];
        
        if (IS_IPHONE6PLUS) {
            button.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
        }
        
        UIOffset offset = viewController.tabBarItem.titlePositionAdjustment;
        button.titleEdgeInsets = UIEdgeInsetsMake(offset.vertical, offset.horizontal, 0.0f, 0.0f);
        button.imageEdgeInsets = viewController.tabBarItem.imageInsets;
        [button setTitle:viewController.tabBarItem.title forState:UIControlStateNormal];
        [button setImage:viewController.tabBarItem.image forState:UIControlStateNormal];
        //[button setImage:viewController.tabBarItem.selectedImage forState:UIControlStateHighlighted];
        
        [button setImage:viewController.tabBarItem.selectedImage forState:UIControlStateSelected];
        
        [button addTarget:self action:@selector(tabButtonPressed:) forControlEvents:UIControlEventTouchDown];
        //
        if ([viewController isKindOfClass:[FeedViewController class]]) {
            
            UIView *vw=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
            vw.backgroundColor=[UIColor redColor];
            
            // [self.view addSubview:mainVi];
            [button addSubview:feedMainVi];
        }else   if ([viewController isKindOfClass:[NotificationViewController class]]) {
            [button addSubview:mainVi];
        }
        
        
        [self deselectTabButton:button];
        
        
        [tabButtonsContainerView addSubview:button];
        
        ++index;
    }
    
}

- (void)removeTabButtons
{
    while ([tabButtonsContainerView.subviews count] > 0)
    {
        [[tabButtonsContainerView.subviews lastObject] removeFromSuperview];
    }
}

- (void)layoutTabButtons
{
    NSUInteger index = 0;
    NSUInteger counts = [self.viewControllers count];
    widthofbutton=self.view.bounds.size.width/4;
    CGRect rect = CGRectMake(0.0f, 0.0f,widthofbutton, self.tabBarHeight);
    
    indicatorImageView.hidden = YES;
    
    NSArray *buttons = [tabButtonsContainerView subviews];
    for (UIButton *button in buttons)
    {
        if (index == counts - 1)
            //rect.size.width = self.view.bounds.size.width - rect.origin.x;
//            NSLog(@"rect is%f", rect.size.width );
        button.frame = rect;
        
        rect.origin.x += widthofbutton;
        
        if (index == self.selectedIndex){
            
            
            [self centerIndicatorOnButton:button];
            
        }
        ++index;
    }
    
    tabButtonsContainerView.frame=CGRectMake(0,-21,widthofbutton*counts,   self.tabBarHeight);
    topButtonscrollview.contentSize = CGSizeMake(widthofbutton*counts,0);
    
    topButtonscrollview.frame= CGRectMake(0.0f, 22.0f, self.view.bounds.size.width, self.tabBarHeight);
    [self.view layoutIfNeeded];
    [topButtonscrollview setShowsVerticalScrollIndicator:NO];
    [topButtonscrollview addSubview:tabButtonsContainerView];
    
}

- (void)centerIndicatorOnButton:(UIButton *)button
{
    if(pagenumber==4){
        // [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
        //        if (buttonClikced==YES) {
        //            NSLog(@"swiped");
        //            UIButton *btn=[tabButtonsContainerView viewWithTag:1004];
        //            CGRect rect = indicatorImageView.frame;
        //            rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        //            rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        //            indicatorImageView.frame = rect;
        //            indicatorImageView.hidden = NO;
        //        }else{
        //            NSLog(@"not swiped");
        //            UIButton *btn=[tabButtonsContainerView viewWithTag:1003];
        //            CGRect rect = indicatorImageView.frame;
        //            rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        //            rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        //            indicatorImageView.frame = rect;
        //            indicatorImageView.hidden = NO;
        //        }
        //        return;
        
        
        UIButton *btn;
        btn=[tabButtonsContainerView viewWithTag:1003];
        //        if (buttonClikced==YES) {
        //            btn=[tabButtonsContainerView viewWithTag:1004];
        //            if(topButtonscrollview.contentOffset.x>0){
        //                btn=[tabButtonsContainerView viewWithTag:1003];
        //            }
        //        }
        CGRect rect = indicatorImageView.frame;
        rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        indicatorImageView.frame = rect;
        indicatorImageView.hidden = NO;
        
        
        return;
        
    }
    if(pagenumber==1){
//        NSLog(@"not swiped");
        //        if (buttonClikced==YES) {
        //            UIButton *btn=[tabButtonsContainerView viewWithTag:1001];
        //            CGRect rect = indicatorImageView.frame;
        //            rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        //            rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        //            indicatorImageView.frame = rect;
        //            indicatorImageView.hidden = NO;
        //            return;
        //        }else{
        //        UIButton *btn=[tabButtonsContainerView viewWithTag:1000];
        //        CGRect rect = indicatorImageView.frame;
        //        rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        //        rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        //        indicatorImageView.frame = rect;
        //        indicatorImageView.hidden = NO;
        //          return;
        //}
        
        UIButton *btn;
        btn=[tabButtonsContainerView viewWithTag:1000];
        //        if (buttonClikced==YES) {
        //            btn=[tabButtonsContainerView viewWithTag:1001];
        //            if(topButtonscrollview.contentOffset.x>0){
        //                btn=[tabButtonsContainerView viewWithTag:1000];
        //            }
        //        }
        CGRect rect = indicatorImageView.frame;
        rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        indicatorImageView.frame = rect;
        indicatorImageView.hidden = NO;
        return;
        
        
        
        
        
    }
    if(pagenumber==2){
//        NSLog(@"not swiped");
        //     UIButton *btn;
        //         btn=[tabButtonsContainerView viewWithTag:1001];
        //        if (buttonClikced==YES) {
        //              btn=[tabButtonsContainerView viewWithTag:1002];
        //            if(topButtonscrollview.contentOffset.x>0){
        //                    btn=[tabButtonsContainerView viewWithTag:1001];
        //            }
        //        }
        //          btn=[tabButtonsContainerView viewWithTag:1002];
        //            CGRect rect = indicatorImageView.frame;
        //            rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        //            rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        //            indicatorImageView.frame = rect;
        //            indicatorImageView.hidden = NO;
        //            return;
        ////        }else{
        //
        UIButton *btn=[tabButtonsContainerView viewWithTag:1001];
        CGRect rect = indicatorImageView.frame;
        rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        indicatorImageView.frame = rect;
        indicatorImageView.hidden = NO;
        return;
        
    }  if(pagenumber==3){
        //        if (buttonClikced==YES) {
        //            UIButton *btn=[tabButtonsContainerView viewWithTag:1003];
        //            CGRect rect = indicatorImageView.frame;
        //            rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        //            rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        //            indicatorImageView.frame = rect;
        //            indicatorImageView.hidden = NO;
        //            return;
        //        }else{
        //
        //        NSLog(@"not swiped");
        //        UIButton *btn=[tabButtonsContainerView viewWithTag:1002];
        //        CGRect rect = indicatorImageView.frame;
        //        rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        //        rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        //        indicatorImageView.frame = rect;
        //        indicatorImageView.hidden = NO;
        //        return;
        //    }
        
        
        
        //        UIButton *btn;
        //        btn=[tabButtonsContainerView viewWithTag:1001];
        //        if (buttonClikced==YES) {
        //            btn=[tabButtonsContainerView viewWithTag:1003];
        //            if(topButtonscrollview.contentOffset.x>0){
        //                btn=[tabButtonsContainerView viewWithTag:1002];
        //            }
        //        }
        UIButton *btn=[tabButtonsContainerView viewWithTag:1002];
        CGRect rect = indicatorImageView.frame;
        rect.origin.x = btn.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
        rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
        indicatorImageView.frame = rect;
        indicatorImageView.hidden = NO;
        return;
        
    }
    // [topButtonscrollview setContentOffset:CGPointMake(0, -20) animated:YES];
    CGRect rect = indicatorImageView.frame;
    rect.origin.x = button.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
    rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
    indicatorImageView.frame = rect;
    indicatorImageView.hidden = NO;
}

- (void)setViewControllers:(NSArray *)newViewControllers
{
    NSAssert([newViewControllers count] >= 2, @"MHTabBarController requires at least two view controllers");
    
    UIViewController *oldSelectedViewController = self.selectedViewController;
    
    // Remove the old child view controllers.
    for (UIViewController *viewController in _viewControllers)
    {
        [viewController willMoveToParentViewController:nil];
        [viewController removeFromParentViewController];
    }
    
    _viewControllers = [newViewControllers copy];
    
    // This follows the same rules as UITabBarController for trying to
    // re-select the previously selected view controller.
    NSUInteger newIndex = [_viewControllers indexOfObject:oldSelectedViewController];
    if (newIndex != NSNotFound)
        _selectedIndex = newIndex;
    else if (newIndex < [_viewControllers count])
        _selectedIndex = newIndex;
    else
        _selectedIndex = 0;
    
    // Add the new child view controllers.
    for (UIViewController *viewController in _viewControllers)
    {
        [self addChildViewController:viewController];
        [viewController didMoveToParentViewController:self];
    }
    
    if ([self isViewLoaded])
        [self reloadTabButtons];
}

- (void)setSelectedIndex:(NSUInteger)newSelectedIndex
{
    ///NSLog(@"sender.tag is%ld",newSelectedIndex);
    [self setSelectedIndex:newSelectedIndex animated:NO];
}

- (void)setSelectedIndex:(NSUInteger)newSelectedIndex animated:(BOOL)animated
{
    
    
    NSAssert(newSelectedIndex < [self.viewControllers count], @"View controller index out of bounds");
    
    if ([self.delegate respondsToSelector:@selector(mh_tabBarController:shouldSelectViewController:atIndex:)])
    {
        UIViewController *toViewController = (self.viewControllers)[newSelectedIndex];
        if (![self.delegate mh_tabBarController:self shouldSelectViewController:toViewController atIndex:newSelectedIndex])
            return;
    }
    
    if (![self isViewLoaded])
    {
        _selectedIndex = newSelectedIndex;
    }
    else if (_selectedIndex != newSelectedIndex)
    {
        UIViewController *fromViewController;
        UIViewController *toViewController;
        
        if (_selectedIndex != NSNotFound)
        {
            UIButton *fromButton = (UIButton *)[tabButtonsContainerView viewWithTag:TagOffset + _selectedIndex];
            [self deselectTabButton:fromButton];
            fromViewController = self.selectedViewController;
        }
        
        NSUInteger oldSelectedIndex = _selectedIndex;
        _selectedIndex = newSelectedIndex;
        
        UIButton *toButton;
        if (_selectedIndex != NSNotFound)
        {
            toButton = (UIButton *)[tabButtonsContainerView viewWithTag:TagOffset + _selectedIndex];
            [self selectTabButton:toButton];
            toViewController = self.selectedViewController;
        }
        
        if (toViewController == nil)  // don't animate
        {
            [fromViewController.view removeFromSuperview];
        }
        else if (fromViewController == nil)  // don't animate
        {
            toViewController.view.frame = contentContainerView.bounds;
            [contentContainerView addSubview:toViewController.view];
            [self centerIndicatorOnButton:toButton];
            
            if ([self.delegate respondsToSelector:@selector(mh_tabBarController:didSelectViewController:atIndex:)])
                [self.delegate mh_tabBarController:self didSelectViewController:toViewController atIndex:newSelectedIndex];
        }
        else if (animated)
        {
            CGRect rect = contentContainerView.bounds;
            if (oldSelectedIndex < newSelectedIndex)
                rect.origin.x = rect.size.width;
            else
                rect.origin.x = -rect.size.width;
            
            toViewController.view.frame = rect;
            tabButtonsContainerView.userInteractionEnabled = NO;
            
            [self transitionFromViewController:fromViewController
                              toViewController:toViewController
                                      duration:0.3f
                                       options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionCurveEaseOut
                                    animations:^
             {
                 CGRect rect = fromViewController.view.frame;
                 if (oldSelectedIndex < newSelectedIndex)
                     rect.origin.x = -rect.size.width;
                 else
                     rect.origin.x = rect.size.width;
                 
                 fromViewController.view.frame = rect;
                 toViewController.view.frame = contentContainerView.bounds;
                 [self centerIndicatorOnButton:toButton];
             }
                                    completion:^(BOOL finished)
             {
                 tabButtonsContainerView.userInteractionEnabled = YES;
                 
                 if ([self.delegate respondsToSelector:@selector(mh_tabBarController:didSelectViewController:atIndex:)])
                     [self.delegate mh_tabBarController:self didSelectViewController:toViewController atIndex:newSelectedIndex];
             }];
        }
        else  // not animated
        {
            [fromViewController.view removeFromSuperview];
            
            toViewController.view.frame = contentContainerView.bounds;
            [contentContainerView addSubview:toViewController.view];
            [self centerIndicatorOnButton:toButton];
            
            
            
            
            
            if ([self.delegate respondsToSelector:@selector(mh_tabBarController:didSelectViewController:atIndex:)])
                [self.delegate mh_tabBarController:self didSelectViewController:toViewController atIndex:newSelectedIndex];
        }
    }
    //[topButtonscrollview setContentOffset:CGPointMake(0, -20) animated:YES];
    if (pagenumber==2) {
        [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
        
        
    }
    if (pagenumber==3) {
        [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
        
        
    }
    
    if (pagenumber==1) {
        if (swipedright==YES) {
            [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
        }else{
            [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
        }
    }
    if (pagenumber==0) {
        if (swipedLeft==YES) {
            
            [topButtonscrollview setContentOffset:CGPointMake(0, -20) animated:YES];
            
        }
    }
    if (pagenumber==4) {
        //[topButtonscrollview setContentOffset:CGPointMake(400, 0)];
//        NSLog(@"is%f",widthofbutton*4);
        [topButtonscrollview setContentOffset:CGPointMake(widthofbutton, -20) animated:YES];
    }
    scrolled=NO;
}

- (UIViewController *)selectedViewController
{
    if (self.selectedIndex != NSNotFound)
        return (self.viewControllers)[self.selectedIndex];
    else
        return nil;
}

- (void)setSelectedViewController:(UIViewController *)newSelectedViewController
{
    [self setSelectedViewController:newSelectedViewController animated:NO];
}

- (void)setSelectedViewController:(UIViewController *)newSelectedViewController animated:(BOOL)animated
{
    NSUInteger index = [self.viewControllers indexOfObject:newSelectedViewController];
    if (index != NSNotFound)
        [self setSelectedIndex:index animated:animated];
}

- (void)tabButtonPressed:(UIButton *)sender
{
//    NSLog(@"sender.tag is%ld",(long)sender.tag-TagOffset);
    [self setSelectedIndex:sender.tag - TagOffset animated:YES];
    pagenumber=sender.tag-TagOffset;
}

#pragma mark - Change these methods to customize the look of the buttons


- (void)selectTabButton:(UIButton *)button
{
    
//    NSLog(@"sender.tag is%ld",(long)button.tag);
    
    scrolled=YES;
    buttonClikced=YES;
    button.selected=YES;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if (button.tag==1004) {
        pagenumber=4;
        //  [self centerIndicatorOnButton:button];
    }
    if (button.tag==1000) {
        
        pagenumber=0;
        
    }
    if (button.tag==1001) {
        pagenumber=1;
        //  [self centerIndicatorOnButton:button];
    }
    
    if (button.tag==1002) {
        pagenumber=2;
        
        feedFilter.hidden=NO;
    }
    if (button.tag==1003) {
        pagenumber=3;
        mainVi.hidden=YES;
    }
    
}
- (void)deselectTabButton:(UIButton *)button
{
    
    
    feedFilter.hidden=YES;
    button.selected=NO;
    
    [self.view setBackgroundColor: [UIColor colorWithRed:20/255.0f green:135/255.0f blue:224/255.0f alpha:1.0f]];
    //  0099DC
    [button setBackgroundImage:[self imageWithColor:[self colorFromHexString:@"0099DC"] ] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (CGFloat)tabBarHeight
{
    if (IS_IPHONE6PLUS) {
        return 60.0f;
    }
    return 46.0f;
}
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ( [gestureRecognizer isMemberOfClass:[UITapGestureRecognizer class]] ) {
        // Return NO for views that don't support Taps
    } else if ( [gestureRecognizer isMemberOfClass:[UISwipeGestureRecognizer class]] ) {
        // Return NO for views that don't support Swipes
    }
    
    return YES;
}
-(void)scrollViewDidScroll:(UIScrollView *)sender
{
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    NSLog(@"contentOffset is%f",topButtonscrollview.contentOffset.x);
//    NSLog(@"widthofbutton is%f",widthofbutton);
//    NSLog(@"indicatorImageView is%f",indicatorImageView.center.x);
    if(topButtonscrollview.contentOffset.x>=widthofbutton){
//        NSLog(@"swiped left");
        if(indicatorImageView.center.x<widthofbutton){
            pagenumber=1;
            [self setSelectedIndex:pagenumber animated:YES];
        }
    }else if (topButtonscrollview.contentOffset.x==0){
//        NSLog(@"back to feed left");
        pagenumber=0;
        [self setSelectedIndex:pagenumber animated:YES];
    }
    
    
    
}
@end
