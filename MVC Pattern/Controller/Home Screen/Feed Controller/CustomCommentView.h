//
//  CustomCommentView.h
//  wireFrameSplash
//
//  Created by webwerks on 5/4/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCommentView : UIView<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property(nonatomic,weak)IBOutlet UITableView *tblView;
@property(nonatomic,weak)IBOutlet UIButton *btnSeeMore;
@property(nonatomic,weak)IBOutlet UIButton *btnSend;
@property(nonatomic,weak)IBOutlet UITextField *txtComment;
@property(nonatomic,weak)IBOutlet NSLayoutConstraint *tblViewHeight;
@property(nonatomic,strong)NSArray *arrData;

-(void)refereshData:(NSArray *)array;
@end
