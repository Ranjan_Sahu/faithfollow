//
//  HomeViewController.h
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WallView.h"
@interface FeedViewController : UIViewController<MyFirstControllerDelegate>
//@property (weak, nonatomic) IBOutlet UITabBar *customtabbar;
@property (strong, nonatomic) IBOutlet UITableView *tableViewOutlet;

@property (weak, nonatomic) IBOutlet UIButton *status_btn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *leadingconstraints;

@property (strong, nonatomic) IBOutlet UIView *upperView;
;
@property (strong, nonatomic) IBOutlet UIImageView *upperView_imageview;

@property (weak, nonatomic) IBOutlet UITextView *textViewTXT;
@property(weak,nonatomic)NSString *call_View;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *feedrateBottomConstraint;



@end
