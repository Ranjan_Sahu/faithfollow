//
//  CustomCommentView.m
//  wireFrameSplash
//
//  Created by webwerks on 5/4/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "CustomCommentView.h"
#import "CommentTableViewCell.h"
@implementation CustomCommentView
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
       
    }
    return self;
}
-(void)refereshData:(NSArray *)array
{
    if(self.tblView.delegate==nil){
        [self.tblView setDataSource:self];
        [self.tblView setDelegate:self];
       
        
        [self.tblView registerNib:[UINib nibWithNibName:@"CommentTableViewCell" bundle:nil] forCellReuseIdentifier:@"CommentTableViewCell"];
    
    }
    self.arrData=array;
    [self.tblView reloadData];
}
#pragma mark TableView Data Source methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrData count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentTableViewCell" forIndexPath:indexPath];
    [self setUpCell:cell atIndexPath:indexPath];
    
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static CommentTableViewCell *cell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"CommentTableViewCell"];
    });

    [self setUpCell:cell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:cell];

   
}
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsUpdateConstraints];
    [sizingCell updateConstraintsIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}
- (void)setUpCell:(CommentTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    float width=[UIScreen mainScreen].bounds.size.width-(20);
    cell.lblName.preferredMaxLayoutWidth = width;
//    cell.lblName.text=@"For the past 33 years, I have looked in the mirror every morning and asked myself: 'If today were the last day of my life, would I want to do what I am about to do today?' And whenever the answer has been 'No' for too many days in a row,";
    NSDictionary *dict=[self.arrData objectAtIndex:indexPath.row];
    cell.lblName.text=dict[@"body"];
    cell.lblComment.text=dict[@"created_at"];

}

@end
