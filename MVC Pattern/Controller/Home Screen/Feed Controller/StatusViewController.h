//
//  StatusViewController.h
//  wireFrameSplash
//
//  Created by Vikas on 05/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface StatusViewController : UIViewController
- (IBAction)tap:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *upperView_topConstraint;
@property (weak, nonatomic) IBOutlet UIButton *btn_statusUpdate;
@property (weak, nonatomic) IBOutlet UIButton *btn_prayerRequest;
@property (weak, nonatomic) IBOutlet UIButton *btn_photo;
@property (weak, nonatomic) IBOutlet UIButton *btn_submit;
@property (weak, nonatomic) IBOutlet UITextView *textView_status;

@property (weak, nonatomic) IBOutlet UIImageView *btnimageview_prayer;
@property (weak, nonatomic) IBOutlet UIImageView *btnimageview_status;

@property (weak, nonatomic) IBOutlet UILabel *lbl_attachment;
@property (assign,nonatomic)NSString* status_para;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightoftxtView;
@property(strong,nonatomic)NSString *addDiscusssionflag;
////////////////////////

@property (weak, nonatomic) IBOutlet UIImageView *uploadimageview1;

@property (weak, nonatomic) IBOutlet UIImageView *uploadimageview2;
@property (weak, nonatomic) IBOutlet UIImageView *uploadimageview3;

@property (weak, nonatomic) IBOutlet UIImageView *uploadimageview4;

@property (weak, nonatomic) IBOutlet UIImageView *uploadimageview5;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingofuploadIMage1;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *traileingofuploadimage5;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laedingofuploadimage4;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingofuploadimage2;
@property(strong,nonatomic)NSDictionary *dictofPost;
@property(strong,nonatomic)NSDictionary *dictofPostGroup;

@property (weak, nonatomic) IBOutlet UIImageView *imagePicked_imageView;
@property (weak, nonatomic) IBOutlet UIView *popView_outlet;

@property (weak, nonatomic) IBOutlet UIImageView *user_imageview;

@property (strong, nonatomic) NSString *checkpostinnormalProfile;









- (IBAction)btn_photoAct:(id)sender;
- (IBAction)btn_submitAct:(id)sender;
- (IBAction)popView_Cancelbtn:(id)sender;
- (IBAction)gallery_act:(id)sender;

- (IBAction)camera_act:(id)sender;
-(IBAction)CancealBTN:(id)sender;

// picker view handle here.

@property (weak, nonatomic) IBOutlet UIView *viewPostPicker;
@property (weak, nonatomic) IBOutlet UIView *choseGroupView;
@property (weak, nonatomic) IBOutlet UIView *postTypeView;
@property (weak, nonatomic) IBOutlet UIButton *postTypeBTN;
@property (weak, nonatomic) IBOutlet UIButton *choseGroupBTN;
@property (retain, nonatomic)UIPickerView *pickerView,*pickerView1;
@property (assign, nonatomic)BOOL isGroupPost;

- (IBAction)choseGroupMethod:(id)sender;
- (IBAction)posttypeMethod:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *posttypeMethod;
@property (strong, nonatomic)UIToolbar *toolbar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewpostAndchosegroupConstrants;

@property (weak, nonatomic) IBOutlet UIImageView *influencerimage;

@end
