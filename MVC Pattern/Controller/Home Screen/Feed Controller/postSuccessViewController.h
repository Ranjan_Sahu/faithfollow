//
//  postSuccessViewController.h
//  wireFrameSplash
//
//  Created by webwerks on 10/27/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface postSuccessViewController : UIViewController
-(IBAction)back:(id)sender;
- (IBAction)GotItMethod:(id)sender;
@property(strong,nonatomic)NSString *groupname;
@property (weak, nonatomic) IBOutlet UIButton *gotitBTN;
@property (weak, nonatomic) IBOutlet UILabel *groupnameLBL;
@end
