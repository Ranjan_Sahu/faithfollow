//
//  HomeViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//



#import "FeedViewController.h"
#import "FeedTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CustomCommentView.h"
#import "StatusViewController.h"
#import "FeedIMageTableViewCell.h"

#define SKY_BLUE_COLOR [UIColor colorWithRed:0.1137 green:0.4824 blue:0.8065 alpha:1.0]

@interface FeedViewController ()<UITabBarDelegate,UITextViewDelegate>
{
    CGFloat heightofCell;
    NSInteger selectedRow;
    UIView *lineView ;
    NSMutableArray *arrFeeds;
    NSMutableArray *arrWallFeeds;
    
}
@property (strong, nonatomic) FeedTableViewCell *customCell;
@end

@implementation FeedViewController
#define  kCellName @"FeedTableViewCell"
#define  kCellName1 @"FeedIMageTableViewCell"
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    self.view.backgroundColor=[UIColor whiteColor];
    [self.tableViewOutlet   registerNib:[UINib nibWithNibName:kCellName bundle:nil] forCellReuseIdentifier:kCellName];
    selectedRow=-1;
    _upperView.layer.borderWidth=0.5f;
    _upperView.layer.borderColor=[UIColor grayColor].CGColor;
    arrFeeds=[[NSMutableArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Feed" ofType:@"plist"]];
    _status_btn.layer.borderColor=[UIColor blackColor].CGColor;
    _status_btn.layer.borderWidth=0.5f;
    [self.tableViewOutlet   registerNib:[UINib nibWithNibName:kCellName1 bundle:nil] forCellReuseIdentifier:kCellName1];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [_tableViewOutlet reloadData];
    [_textViewTXT resignFirstResponder];
    [self.view endEditing:YES];
    [self feed];
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [_textViewTXT resignFirstResponder];
    StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
    [self presentViewController:statusScreen animated:YES completion:nil];
}
- (IBAction)btn_act:(id)sender {
}
- (IBAction)status_act:(id)sender {
    
    StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
    [self.navigationController pushViewController:statusScreen animated:YES];
}

- (IBAction)prayer_act:(id)sender {
}

- (IBAction)photo_act:(id)sender {
}
- (IBAction)comments_act:(UIButton*)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    if (sender.selected)
    {
        selectedRow=-1;
        [_tableViewOutlet reloadData];
        [sender setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        sender.selected=false;
    }else{
        if (indexPath != nil)
        {
            [sender setTitleColor:SKY_BLUE_COLOR forState:UIControlStateNormal];
            selectedRow=indexPath.row;
            [_tableViewOutlet reloadData];
        }
        sender.selected=true;
    }
}
#pragma mark TableView Data Source methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrWallFeeds count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName forIndexPath:indexPath];
    //    [self setUpCell:cell atIndexPath:indexPath];
    //
    //    if(indexPath.row == selectedRow)
    //    {
    //        NSArray *arr=[arrFeeds objectAtIndex:indexPath.row][@"comments"];
    //        [cell.commentsView refereshData:arr];
    //    }
    if (indexPath.row==0) {
        FeedIMageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName1 forIndexPath:indexPath];
        [self setUpCell1:cell atIndexPath:indexPath];
        return cell;
    }else{
        FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        return cell;
        
    }
    
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    static FeedTableViewCell *cell = nil;
    //    static dispatch_once_t onceToken;
    //    dispatch_once(&onceToken, ^{
    //        cell = [self.tableViewOutlet dequeueReusableCellWithIdentifier:kCellName];
    //    });
    //    [self setUpCell:cell atIndexPath:indexPath];
    //    return [self calculateHeightForConfiguredSizingCell:cell];
    if (indexPath.row==0) {
        static FeedIMageTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell = [self.tableViewOutlet dequeueReusableCellWithIdentifier:kCellName1];
        });
        [self setUpCell1:cell atIndexPath:indexPath];
        return [self calculateHeightForConfiguredSizingCell:cell];
    }else{
        static FeedTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell = [self.tableViewOutlet dequeueReusableCellWithIdentifier:kCellName];
        });
        [self setUpCell:cell atIndexPath:indexPath];
        return [self calculateHeightForConfiguredSizingCell:cell];
    }
}
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsUpdateConstraints];
    [sizingCell updateConstraintsIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}
- (void)setUpCell1:(FeedIMageTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    float width=[UIScreen mainScreen].bounds.size.width-(20);
    // cell.personName_lbl.text=@"hello";
    
    cell.heightConsatraint.constant=0;
    cell.commentsView.hidden=YES;
    cell.commentsView.tblViewHeight.constant=0;
    NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
    NSDictionary *get_object_user_details=dict[@"get_object_user_details"];
    NSString *objectName=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
    NSDictionary *get_subject_user_details=dict[@"get_subject_user_details"];
    NSDictionary *subject_user_details=get_subject_user_details[@"usersprofile"];
    NSString *subjectName=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
    
    
    
    
    
    NSString *str=[NSString stringWithFormat:@"%@ have posted %@",objectName,dict[@"content"]];
    cell.personName_lbl.text=subjectName;
    NSString *str1=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:subject_user_details[@"profile_picture"]]];
    str1=[NSString stringWithFormat:@"%@%@",ImageapiUrl,str1];
    
    //        cell.praise_lbl.text=[NSString stringWithFormat:@"%@ Praised",[Constant1 getStringObject:subject_user_details[@"get_total_praised"]]];
    
    cell.praise_lbl.text=@"0 Praised";
    
    cell.totalcomments_lbl.text=[NSString stringWithFormat:@"0 Comments"];
    
    [cell.person_imageview sd_setImageWithURL:[NSURL URLWithString:str1] placeholderImage:[UIImage imageNamed:@"person"]];
    [cell.praised_btn addTarget:self action:@selector(btnClick_like:) forControlEvents:UIControlEventTouchUpInside];
    cell.date_lbl.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:dict[@"updated_at"]]];
    
    
    cell.com_lbl.text=dict[@"content"];
    [cell.comments_btn addTarget:self action:@selector(comments_act:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(indexPath.row == selectedRow)
    {
        NSArray *arr=dict[@"comments"];
        float  height=([arr count]*60);
        cell.commentsView.hidden=NO;
        cell.heightConsatraint.constant=height+72;
        cell.commentsView.tblViewHeight.constant=height;
        cell.mainView.layer.borderWidth=0.0f;
        
        //        cell.mainView.layer.borderWidth=0.5f;
        //        cell.mainView.layer.cornerRadius=8.0;
        //        cell.mainView.layer.borderColor=[UIColor redColor].CGColor;
        //
        //        cell.commentsView.layer.borderWidth=0.5f;
        //        cell.commentsView.layer.cornerRadius=8.0;
        //        cell.commentsView.layer.borderColor=[UIColor redColor].CGColor;
    }
    else{
        cell.heightConsatraint.constant=0;
        cell.commentsView.hidden=YES;
        cell.commentsView.tblViewHeight.constant=0;
        //        cell.mainView.layer.borderWidth=0.5f;
        //        cell.mainView.layer.cornerRadius=8.0;
        //        cell.mainView.layer.borderColor=[UIColor redColor].CGColor;
        
    }
    
    
}
- (void)setUpCell:(FeedTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    float width=[UIScreen mainScreen].bounds.size.width-(20);
    cell.com_lbl.preferredMaxLayoutWidth = width; // This is necessary to allow the label
    NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
    NSDictionary *get_object_user_details=dict[@"get_object_user_details"];
    NSString *objectName=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
    NSDictionary *get_subject_user_details=dict[@"get_subject_user_details"];
    NSDictionary *subject_user_details=get_subject_user_details[@"usersprofile"];
    NSString *subjectName=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
    NSString *str=[NSString stringWithFormat:@"%@ have posted %@",objectName,dict[@"content"]];
    
    cell.personName_lbl.text=subjectName;
    NSString *str1=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:subject_user_details[@"profile_picture"]]];
    str1=[NSString stringWithFormat:@"%@%@",ImageapiUrl,str1];
    cell.praise_lbl.text=[NSString stringWithFormat:@"%@ Praised",[Constant1 getStringObject:subject_user_details[@"get_total_praised"]]];
    cell.totalcomments_lbl.text=[NSString stringWithFormat:@"%@ Comments",[Constant1 getStringObject:dict[@"get_total_commnets"]]];
    cell.praise_lbl.text=@"0 Praised";
    [cell.person_imageview sd_setImageWithURL:[NSURL URLWithString:str1] placeholderImage:[UIImage imageNamed:@"person"]];
    [cell.praised_btn addTarget:self action:@selector(btnClick_like:) forControlEvents:UIControlEventTouchUpInside];
    cell.date_lbl.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:dict[@"updated_at"]]];
    cell.com_lbl.text=dict[@"content"];
    [cell.comments_btn addTarget:self action:@selector(comments_act:) forControlEvents:UIControlEventTouchUpInside];
    if(indexPath.row == selectedRow)
    {
        NSArray *arr=dict[@"comments"];
        float  height=([arr count]*60);
        cell.commentsView.hidden=NO;
        cell.heightConsatraint.constant=height+72;
        cell.commentsView.tblViewHeight.constant=height;
        cell.mainView.layer.borderWidth=0.0f;
        [self.view layoutIfNeeded];
    }
    else{
        cell.heightConsatraint.constant=0;
        cell.commentsView.hidden=YES;
        cell.commentsView.tblViewHeight.constant=0;
        [self.view layoutIfNeeded];
        [self.view updateConstraints];
        
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 85;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"SectionHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    _textViewTXT=(UITextView*)[headerView viewWithTag:800];
    _textViewTXT.delegate=self;
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 85.0;
}

-(void)feed{
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"object_type":@"user",
                           @"object_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"filter_type":@"4",
                           
                           };
    [self sendRequest:dict andmethodName:wall_display];
    
    
}
-(void)btnClick_like:(UIButton*)sender{
    // if ([_call_View isEqualToString:@"Wall"]) {
    NSDictionary *object_dict=[arrWallFeeds objectAtIndex:sender.tag][@"get_object_user_details"];
    //NSLog(@"object is%@",dict);
    //        [self  like:[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]  andobject_Id:dict[@"id"] andobject_type:@"user"];
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"user_id	" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"object_type":@"user",
                           @"object_id":object_dict[@"id"],
                           };
    [self sendRequest:dict andmethodName:wall_like];
    //}
    
}
-(void)like:(NSString*)user_ID andobject_Id:(NSString*)object_Id andobject_type:(NSString*)object_type{
    
    
}
-(void)sendRequest:(NSDictionary*)dict andmethodName:(NSString*)methodName{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
            NSMutableArray *messageArray;
//            NSLog(@"response is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    
                    if([response[@"status"]intValue ] ==1){
                        arrWallFeeds=[response valueForKey:@"data"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_tableViewOutlet reloadData];
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        });
                    }
                    
                }
                
            }
            
        } errorBlock:^(id error)
         {
             NSLog(@"error is %@",error);
         }];
    }
    
}
@end




-(void)sendData{
    
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"subject_type":@"user",
                           @"subject_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"object_type":@"user",
                           @"object_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"content":_textView_status.text,
                           @"attachment":imageArray,
                           @"post_type":posttype,
                           
                           };
    
//    NSLog(@"dict is%@",dict);
    // Build the request body
    NSString *boundary = @"SportuondoFormBoundary";
    NSMutableData *body = [NSMutableData data];
    // Body part for "deviceId" parameter. This is a string.
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"token\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Disposition: form-data; name=\"subject_type\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"user"   dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"subject_id\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *str=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
    [body appendData:[str   dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"object_type\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"user"   dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Disposition: form-data; name=\"object_id\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[str   dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[@"Content-Disposition: form-data; name=\"content\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[_textView_status.text   dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Disposition: form-data; name=\"post_type\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[posttype  dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    for(int i=0;i<[imageArray count];i++)
    {
        //NSDictionary *mediaDetails =[selectedMedia objectAtIndex:i];
        
        UIImage *tmp_image = [imageArray objectAtIndex:i];
        NSData *imageData1 = UIImagePNGRepresentation(tmp_image);
        
        //Compress the image
        CGFloat compression = 1.0f;
        CGFloat maxCompression = 0.1f;
        
        //NSData *imageData1 = UIImageJPEGRepresentation(tmp_image, compression);
//        NSLog(@"length:%lu",(unsigned long)[imageData1 length]);
        BOOL pngImage = YES;
        while ([imageData1 length] > 500000 && compression > maxCompression)
        {
            compression -= 0.10;
            imageData1 = UIImageJPEGRepresentation(tmp_image, compression);
            
            pngImage = NO;
            
            
            
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *tmpfileinfo;
        if(pngImage){
            tmpfileinfo = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fuUpload%d\"; filename=\"_sample%d.png\"\r\n", i+1,i+1];
        }
        else{
            tmpfileinfo = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fuUpload%d\"; filename=\"_sample%d.jpeg\"\r\n", i+1,i+1];
        }
        //[bodyData appendData:[@"Content-Disposition: form-data; name=\"fuUpload1\"; filename=\"_sample.png\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[tmpfileinfo dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [body appendData:[@"Content-Disposition: form-data; name=\"attachment\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [body appendData:[NSData dataWithData:imageData1]];
        
        
    }
    
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    
    // Create the session
    // We can use the delegate to track upload progress
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    // Data uploading task. We could use NSURLSessionUploadTask instead of NSURLSessionDataTask if we needed to support uploads in the background
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:feedviewwallService]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = body;
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:body];
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Process the response
        NSDictionary*  _arrayData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"_arrayData is%@",myString);
        
//        NSLog(@"succes");
    }];
    [uploadTask resume];
    
    
    
    
}

NSData *imageData2 = UIImageJPEGRepresentation(chosenImage, 0.5);

[body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"wedshare.jpg\"\r\n", @"attachment[]"] dataUsingEncoding:NSUTF8StringEncoding]];
[body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
[body appendData:imageData2];
[body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];






//    NSData *imageData2 = UIImageJPEGRepresentation(chosenImage, 0.5);
//
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"wedshare.jpg\"\r\n", @"attachment[]"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:imageData2];
//    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//
