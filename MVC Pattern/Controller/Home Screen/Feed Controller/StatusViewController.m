//
//  StatusViewController.m
//  wireFrameSplash
//
//  Created by Vikas on 05/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "StatusViewController.h"
#import "DeviceConstant.h"
#import "KLCPopup.h"
#import "NeosPopup.h"
#import "postSuccessViewController.h"
#import "popGroupViewController.h"
#import "MHTabBarController.h"
#import "Mixpanel.h"
#import "Constant1.h"

@import MobileCoreServices;
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]
#define SKY_BLUE_COLOR [UIColor colorWithRed:0.1137 green:0.4824 blue:0.8065 alpha:1.0]
@interface StatusViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>{
    KLCPopup *popUpView1;
    UIImage* chosenImage;
    NSString *posttype,*txtView_text;
    NSMutableArray *imageArray;
    BOOL selected;
    CGFloat tagofiamg;
    UIImageView *ima;
    NeosPopup *nepopup;
    UIView *photo_view;
    int statusbackground;
    NSArray   *items,*posttypeArray;
    UIView   *viewPicker,*viewPicker1;
    NSString *groupid;
    UIButton *chatButton1;
    NSString *countCheck;
    popGroupViewController *login;
    CGFloat HeightForPickerView;

    
}
@property (nonatomic, strong) NSURLSession *session;
@end

@implementation StatusViewController
@synthesize pickerView,pickerView1,isGroupPost;
- (void)viewDidLoad {
    [super viewDidLoad];
    
     items =[[NSArray alloc]init];
     posttypeArray=[[NSArray alloc]initWithObjects:@"Prayer Request",@"Update Status",nil];
//    _postTypeView.layer.borderWidth=0.6f;
//    _postTypeView.layer.borderColor=[UIColor grayColor].CGColor;
//    _choseGroupView.layer.borderWidth=0.6f;
//    _choseGroupView.layer.borderColor=[UIColor grayColor].CGColor;
    
    HeightForPickerView = 140.0;
    if(IS_IPHONE4)
        HeightForPickerView = 180.0;
    else if (IS_IPHONE5)
        HeightForPickerView = 200.0;
    else if (IS_IPHONE6)
        HeightForPickerView = 208.0;
    else if (IS_IPHONE6PLUS)
         HeightForPickerView = 218.0;
    
    [self GrouplistinPost];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.topItem.title=@"";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationItem.title = @"Post To FaithFollow";
    
//    NSLog(@"val is%@",_status_para);
    if (IS_IPHONE4) {
        _upperView_topConstraint.constant=30.0f;
        _heightoftxtView.constant=270;
        [self.view layoutIfNeeded];
    }else     if (IS_IPHONE6) {
        _heightoftxtView.constant=400;
        _leadingofuploadIMage1.constant=12;
        _traileingofuploadimage5.constant=12;
        CGFloat trailingofimage2=((_uploadimageview3.frame.origin.x-(_uploadimageview1.frame.origin.x+_uploadimageview1.frame.size.width))*0.5)-5;
        CGFloat leadingofimage4 =((_uploadimageview5.frame.origin.x-(_uploadimageview3.frame.origin.x+_uploadimageview3.frame.size.width))*0.5)-5;
        _trailingofuploadimage2.constant=trailingofimage2;
        _laedingofuploadimage4.constant=leadingofimage4;
        [self.view layoutIfNeeded];
    }else   if (IS_IPHONE6PLUS) {
        _heightoftxtView.constant=500;
        _leadingofuploadIMage1.constant=20;
        _traileingofuploadimage5.constant=20;
        CGFloat trailingofimage2=((_uploadimageview3.frame.origin.x-(_uploadimageview1.frame.origin.x+_uploadimageview1.frame.size.width))*0.5)-5;
        CGFloat leadingofimage4 =((_uploadimageview5.frame.origin.x-(_uploadimageview3.frame.origin.x+_uploadimageview3.frame.size.width))*0.5)-5;
        _trailingofuploadimage2.constant=trailingofimage2;
        _laedingofuploadimage4.constant=leadingofimage4;
        [self.view layoutIfNeeded];

    }
    
    //self.navigationController.navigationBar.barTintColor=[self colorFromHexString:@"1AA8EA"];
    // self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    [_btn_statusUpdate setTitleColor:SKY_BLUE_COLOR forState:UIControlStateSelected];
    [_btn_prayerRequest setTitleColor:SKY_BLUE_COLOR forState:UIControlStateSelected];
  //  [_btn_photo setTitleColor:SKY_BLUE_COLOR forState:UIControlStateSelected];
//    [_btn_submit setTitleColor:SKY_BLUE_COLOR forState:UIControlStateSelected];
//    _textView_status.textColor = [UIColor blackColor];
    _textView_status.text = @"Enter your post";
    _popView_outlet.hidden=YES;
    
    //self.view.backgroundColor=[UIColor lightGrayColor];
    _imagePicked_imageView.layer.cornerRadius=(_imagePicked_imageView.frame.size.width/2);
    _imagePicked_imageView.clipsToBounds=YES;
    
    _btn_statusUpdate.layer.borderColor=[UIColor blackColor].CGColor;
    _btn_statusUpdate.layer.borderWidth=0.5f;
    _btn_statusUpdate.layer.cornerRadius=5.0f;
    _btn_prayerRequest.layer.borderColor=[UIColor blackColor].CGColor;
    _btn_prayerRequest.layer.borderWidth=0.5f;
    _btn_prayerRequest.layer.cornerRadius=5.0f;
    
    _btnimageview_prayer.image= [UIImage imageNamed:@"status"];
    _btnimageview_status.image= [UIImage imageNamed:@"status"];
    [_btn_prayerRequest setTitleColor:UIColorFromRGB(0x009AD9) forState:UIControlStateSelected];
    [_btn_prayerRequest setTitleColor:UIColorFromRGB(0x676767) forState:UIControlStateNormal];
    [_btn_statusUpdate setTitleColor:UIColorFromRGB(0x009AD9) forState:UIControlStateSelected];//selcted
    [_btn_statusUpdate setTitleColor:UIColorFromRGB(0x676767) forState:UIControlStateNormal];
    
    NSString *attach=[NSString stringWithFormat:@"0 :Attachment"];
    _lbl_attachment.text=attach;
    imageArray=[[NSMutableArray alloc]init];
    
//    _btn_submit.layer.borderWidth=0.8f;
//    _btn_submit.layer.borderColor=[UIColor whiteColor].CGColor;
//    _textView_status.layer.borderWidth=0.8f;
//    _textView_status.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _textView_status.delegate=self;
    [self.view setUserInteractionEnabled:YES];
    UITapGestureRecognizer *objectname_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeybod:)];
    [self.view addGestureRecognizer:objectname_gesture];
//    _btn_submit.layer.cornerRadius=5.0;
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_image"];
    UIImage* userimage = [UIImage imageWithData:imageData];
    _user_imageview.image=userimage;
//    _user_imageview.layer.borderColor=[self colorFromHexString:@"009AD9"].CGColor;
//    _user_imageview.layer.borderWidth=1.2f;
//    _user_imageview.layer.cornerRadius=5.0;
//    _user_imageview.clipsToBounds=YES;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[AppDelegate sharedAppDelegate] removeadd];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    NSString *influencerimage=[_dictofPost valueForKey:@"influcencerimagae"];
    if ([influencerimage isEqualToString:@"1"]) {
        _influencerimage.hidden=NO;
    }
    else{
        _influencerimage.hidden=YES;
    }
    if (IS_IPHONE4) {
        _heightoftxtView.constant=120;
        _viewpostAndchosegroupConstrants.constant=5;
        [self.view layoutIfNeeded];
    }else     if (IS_IPHONE6) {
         _heightoftxtView.constant=270;
        _viewpostAndchosegroupConstrants.constant=10;
        [self.view layoutIfNeeded];
    }else   if (IS_IPHONE6PLUS) {
        _heightoftxtView.constant=330;
        _viewpostAndchosegroupConstrants.constant=10;
        [self.view layoutIfNeeded];
    }else{
        _upperView_topConstraint.constant=30.0f;
        _heightoftxtView.constant=180;
        [self.view layoutIfNeeded];
        _viewpostAndchosegroupConstrants.constant=10;
        [self.view layoutIfNeeded];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterInForegroundStatus) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myNotificationMethod:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myNotificationMethod2:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    
//    self.postTypeView.layer.borderColor = [UIColor blackColor].CGColor;
//    self.choseGroupView.layer.borderColor = [UIColor blackColor].CGColor;
    
    if(isGroupPost)
        _viewPostPicker.hidden = YES;
    else
        _viewPostPicker.hidden = NO;

    if([_status_para isEqualToString:@"1"])
    {
        NSString *posttypeStr=[NSString stringWithFormat:@"%@",[posttypeArray objectAtIndex:1]];
        [_postTypeBTN setTitle:posttypeStr forState:UIControlStateNormal];
    }
    else if ([_status_para isEqualToString:@"2"])
    {
        NSString *posttypeStr=[NSString stringWithFormat:@"%@",[posttypeArray objectAtIndex:0]];
        [_postTypeBTN setTitle:posttypeStr forState:UIControlStateNormal];
    }
    
}

-(void)dismissKeybod:(UITapGestureRecognizer*)gesture
{
    txtView_text=_textView_status.text;
    [_textView_status resignFirstResponder];
    
//    if ([countCheck intValue]>0) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            chatButton1.alpha=1.0;
//            countCheck=[NSString stringWithFormat:@"%lu",(unsigned long)_textView_status.text.length];
////            countCheck =0;
//        });
//    }
//    else{
//        dispatch_async(dispatch_get_main_queue(), ^{
//            chatButton1.alpha=0.5;
//            countCheck=[NSString stringWithFormat:@"%lu",(unsigned long)_textView_status.text.length];
//        });
//    }
}




- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)buttonLayer:(id)sender{
    if([sender class] == [UIButton class]){
        
        
        UIButton *button = (UIButton*)sender;
        button.layer.borderColor=[UIColor whiteColor].CGColor;
        button.layer.borderWidth=2.0f;
        
    }
    if([sender class] == [UITextView class]){
        UITextView *button = (UITextView*)sender;
        button.layer.borderColor=[UIColor whiteColor].CGColor;
        button.layer.borderWidth=2.0f;
    }
    
}
-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//- (IBAction)prayer_act:(id)sender {
//    _btn_statusUpdate.selected=NO;
//    _btn_prayerRequest.selected=YES;
//    _btn_photo.selected=NO;
//    _btn_submit.selected=NO;
//    posttype=@"2";
//
//}
//
//- (IBAction)status_act:(id)sender {
//    _btn_statusUpdate.selected=YES;
//    _btn_prayerRequest.selected=NO;
//    _btn_photo.selected=NO;
//    _btn_submit.selected=NO;
//    posttype=@"1";
//
//
//
//}
- (IBAction)btn_photoAct:(UIButton*)sender {
    _btn_statusUpdate.selected=NO;
    _btn_prayerRequest.selected=NO;
    _btn_submit.selected=NO;
     [self openphoto_popup];
}
-(void)openphoto_popup{
     [_textView_status resignFirstResponder];
    
    CGFloat popUpViewWidth = self.view.frame.size.width-80;
    CGFloat btnHeight = 64.0;
    CGFloat closeBttnHeight = 30;
    CGFloat margin = 10;
    CGFloat hPadding = popUpViewWidth/6;

    if (IS_IPHONE5) {
        btnHeight = 44.0;
        closeBttnHeight = 24;
        margin = 6;
    }
    CGFloat popUpViewHeight = (2*margin)+closeBttnHeight+(2*margin)+btnHeight+closeBttnHeight+(4*margin);

    photo_view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, popUpViewWidth, popUpViewHeight)];
    photo_view.backgroundColor=[UIColor whiteColor];
    photo_view.layer.cornerRadius = 8;
    photo_view.layer.masksToBounds = YES;

    UIImageView *cancel_pop=[[UIImageView alloc]initWithFrame:CGRectMake(popUpViewWidth-closeBttnHeight-20, (2*margin), closeBttnHeight, closeBttnHeight)];
    cancel_pop.image=[UIImage imageNamed:@"ic_cancel"];
    cancel_pop.contentMode = UIViewContentModeCenter;
    [photo_view addSubview:cancel_pop];
    cancel_pop.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancel_popup)];
    [cancel_pop addGestureRecognizer:tapGestureRecognizer ];

    UILabel *titleLBL=[[UILabel alloc]initWithFrame:CGRectMake(0, (2*margin), popUpViewWidth, closeBttnHeight)];
    titleLBL.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0];
    titleLBL.text = @"Add Photo";
    titleLBL.font = [UIFont fontWithName:@"Avenir-Medium" size:18.0];
    titleLBL.textAlignment=NSTextAlignmentCenter;
    [photo_view addSubview:titleLBL];

    UIButton *camerabtn = [UIButton buttonWithType: UIButtonTypeSystem];
    camerabtn.frame = CGRectMake(popUpViewWidth/2 - btnHeight - hPadding/2, ((2*margin)+closeBttnHeight+(2*margin)), btnHeight, btnHeight);
    [camerabtn addTarget:self action:@selector(camera) forControlEvents:UIControlEventTouchUpInside];
    [camerabtn setImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
    [photo_view addSubview:camerabtn];
    
    UILabel *cameraLBL=[[UILabel alloc]initWithFrame:CGRectMake(popUpViewWidth/2 - btnHeight - hPadding/2, ((2*margin)+closeBttnHeight+(2*margin)+btnHeight), btnHeight, closeBttnHeight)];
    cameraLBL.textColor = [UIColor colorWithRed:163.0/255.0 green:174.0/255.0 blue:182.0/255.0 alpha:1.0];
    cameraLBL.text = @"Camera";
    cameraLBL.font = [UIFont fontWithName:@"Avenir-Medium" size:18.0];
    cameraLBL.textAlignment=NSTextAlignmentCenter;
    [photo_view addSubview:cameraLBL];
    
    UIButton *gallerybtn = [UIButton buttonWithType: UIButtonTypeSystem];
    gallerybtn.frame = CGRectMake(popUpViewWidth/2 + hPadding/2, ((2*margin)+closeBttnHeight+(2*margin)), btnHeight, btnHeight);
    [gallerybtn addTarget:self action:@selector(gallery) forControlEvents:UIControlEventTouchUpInside];
    [gallerybtn setImage:[UIImage imageNamed:@"gallery"] forState:UIControlStateNormal];
    [photo_view addSubview:gallerybtn];
    
    UILabel *galleryLBL=[[UILabel alloc]initWithFrame:CGRectMake(popUpViewWidth/2 + hPadding/2, ((2*margin)+closeBttnHeight+(2*margin)+btnHeight), btnHeight, closeBttnHeight)];
    galleryLBL.textColor = [UIColor colorWithRed:163.0/255.0 green:174.0/255.0 blue:182.0/255.0 alpha:1.0];
    galleryLBL.text = @"Gallery";
    galleryLBL.font = [UIFont fontWithName:@"Avenir-Medium" size:18.0];
    galleryLBL.textAlignment=NSTextAlignmentCenter;
    [photo_view addSubview:galleryLBL];

    
    nepopup=[[NeosPopup  alloc]initWithFrame:CGRectZero];
    nepopup.backcontentView=photo_view;
    [nepopup show];
    
}
-(void)cancel_popup{
    [nepopup dismiss];
}
- (IBAction)btn_submitAct:(id)sender {
    _btn_statusUpdate.selected=NO;
    _btn_prayerRequest.selected=NO;
    _btn_photo.selected=NO;
//    _btn_submit.layer.borderWidth=0.8f;
//    _btn_submit.layer.borderColor=[self colorFromHexString:@"009AD9"].CGColor;
    _btn_photo.selected=YES;
    if ([_textView_status.text length]==0) {
        [self.view makeToast:@"Please enter post" duration:1.0 position:CSToastPositionCenter];
    }else{
        [self submit];
    }
}

- (IBAction)popView_Cancelbtn:(id)sender {
    popUpView1.hidden=YES;
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([_textView_status.text isEqualToString:@"Enter your post"]) {
        _textView_status.text=@"";
    }
    _textView_status.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0];
    if ([textView isEqual:_textView_status]) {
        [self beginEditing:_textView_status];
    }
    return YES;
}


-(void) textViewDidChange:(UITextView *)textView
{
    
    if(_textView_status.text.length == 0){
        
        _textView_status.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0];
        _textView_status.text = @"Enter comments";
        
        if ([_textView_status.text isEqualToString:@"Enter comments"]) {
            _textView_status.text=@"";
        }
        [_textView_status resignFirstResponder];
    }
    else{
    [chatButton1 setImage:[UIImage imageNamed:@"1497284886_tick_30"] forState:UIControlStateDisabled];
        dispatch_async(dispatch_get_main_queue(), ^{
            chatButton1.alpha=1.0;
//            countCheck=[NSString stringWithFormat:@"%lu",(unsigned long)_textView_status.text.length];
        });

    }
}
#pragma mark - camera and gallery Methods.
-(void)gallery{
        [[NSNotificationCenter defaultCenter] postNotificationName: @"removeadd" object:nil userInfo:nil];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    if(nepopup)
        [nepopup dismiss];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"gallery"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)camera{
    
    if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [nepopup makeToast:@"This Device does not supports Camera !" duration:1.0 position:CSToastPositionCenter];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName: @"removeadd" object:nil userInfo:nil];
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        //popUpView1.hidden=YES;
        
        
        if(nepopup)
            [nepopup dismiss];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"camara"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"gallery"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}




#pragma mark - imagePickerController Delegate methods.

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"gallery"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    chosenImage = info[UIImagePickerControllerEditedImage];
    // changes here for Edited image
    if (!chosenImage) {
        chosenImage = [UIImage new];
    }
//      NSString* imagestr=[self base64String:chosenImage];
        if (imageArray.count<5) {
                [imageArray addObject:info[UIImagePickerControllerEditedImage]];
            NSString *attach=[NSString stringWithFormat:@"%lu :Attachment",(unsigned long)[imageArray count]];
            _lbl_attachment.text=attach;
            _imagePicked_imageView.image=chosenImage;
    
        }else{
        [self.view makeToast:@"Reached maximum photo upload limit" duration:1.0 position:CSToastPositionCenter];
        }
    [nepopup dismiss];
    [picker dismissViewControllerAnimated:NO completion:NULL];
    
    //  [self forwa]
    
}

- (IBAction)gallery_act:(id)sender {
    [self gallery];
}

- (IBAction)camera_act:(id)sender {
    [self camera];
}
-(IBAction)CancealBTN:(id)sender{
    if(isGroupPost){
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
       [self.navigationController popViewControllerAnimated:YES];
    }
//    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    if (getVar.dismischeckViewcontroller==YES) {
//        getVar.dismischeckViewcontroller=NO;
//        [self.navigationController popViewControllerAnimated:YES];
//
//    }
  
}
-(void)submit{
  
    [self.view endEditing:YES];
    
    if ([_addDiscusssionflag isEqualToString:@"GroupAddDiscussion"]) {
        [[NSUserDefaults standardUserDefaults] setObject:_status_para forKey:@"statuClick"];
        [self sendToserverAddDiscuss];
    }
    else{
        [self sendToserver1];
    }
    
}

-(void)sendToserverAddDiscuss{
    
    [self.view endEditing:YES];
    if ([_status_para isEqualToString:@"1"]) {
        [TenjinSDK sendEventWithName:@"StatusUpdate_Submit_Tapped"];
        [FBSDKAppEvents logEvent:@"StatusUpdate_Submit_Tapped"];
        [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"StatusUpdate_Submit_Tapped"]];
       
    }else   if ([_status_para isEqualToString:@"2"]) {
        [TenjinSDK sendEventWithName:@"PrayerRequest_Submit_Tapped"];
        [FBSDKAppEvents logEvent:@"PrayerRequest_Submit_Tapped"];
       [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"PrayerRequest_Submit_Tapped"]];
       
    }
    
    NSInteger lengthOfString = [[_textView_status.text stringByReplacingOccurrencesOfString:@" " withString:@""] length];
    
    
    if (!lengthOfString || [_textView_status.text isEqualToString:@"Enter your post"]|| _textView_status.text.length==0) {
        [self.view makeToast:@"Please enter post" duration:1.0 position:CSToastPositionCenter];
        return;
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate showhud];
        
    });

    NSString *boundary = @"SportuondoFormBoundary";
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"token\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"] dataUsingEncoding:NSUTF8StringEncoding]];
    
//    NSLog(@"user_id %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"]);
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"subject_type\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"user"   dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"subject_id\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
     NSString *subjectID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
    
//    NSLog(@"subject is %@",subjectID);
    
    
    [body appendData:[subjectID   dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"object_type\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"community"   dataUsingEncoding:NSUTF8StringEncoding]];//object type commnity
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"object_id\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *objectID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"comid"]];
    
    NSLog(@"object id %@",objectID);
    
    
    [body appendData:[objectID   dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"content\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[_textView_status.text   dataUsingEncoding:NSUTF8StringEncoding]];
    
//    NSLog(@"text is%@",_textView_status.text);
    
    //    [imageArray removeAllObjects];
    //    [imageArray addObject:[UIImage imageNamed:@"30-1"]];
    //    [imageArray addObject:[UIImage imageNamed:@"320-312"]];
    //    [imageArray addObject:[UIImage imageNamed:@"checked_checkbox"]];
    //    [imageArray addObject:[UIImage imageNamed:@"edit_unselected"]];
    
    for(int i=0;i<[imageArray count];i++)
    {
        //        UIImage *tmp_image = [imageArray objectAtIndex:i];
        //NSData *imageData1 = UIImagePNGRepresentation(tmp_image);
        
        //Compress the image
        CGFloat compression = 1.0f;
        CGFloat maxCompression = 0.1f;
        
        UIImage *img=[Constant1 compressImage:[imageArray objectAtIndex:i]];
        
        NSData *imageData1 = UIImageJPEGRepresentation(img, compression);
//        NSLog(@"length:%lu",(unsigned long)[imageData1 length]);
        BOOL pngImage = YES;
        if ([imageData1 length] > 500000 && compression > maxCompression)
        {
            compression -= 0.10;
            imageData1 = UIImageJPEGRepresentation(img, compression);
            pngImage = NO;
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        if(pngImage){
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"wedshare%d.jpg\"\r\n", @"attachment[]",i+1] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else{
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"wedshare_%d.jpg\"\r\n", @"attachment[]",i+1] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData1];
    }
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Disposition: form-data; name=\"post_type\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[_status_para  dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    
    // Create the session
    // We can use the delegate to track upload progress
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    // Data uploading task. We could use NSURLSessionUploadTask instead of NSURLSessionDataTask if we needed to support uploads in the background
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:wallStatusViewService]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = body;
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:body];
//    NSLog(@"Request body %@", [[NSString alloc] initWithData:body  encoding:NSUTF8StringEncoding]);
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Process the response
     
        [AppDelegate sharedAppDelegate].postimagecheckNotnull=YES;
        [AppDelegate sharedAppDelegate].postimageWebServiceNO=YES;


        NSDictionary*  _arrayData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//        NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"_arrayData is%@",myString);
        NSMutableArray *messageArray;
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"reloadtable" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"reloadtablegroup" object:nil userInfo:nil];
        
        if (![_arrayData isKindOfClass:[NSNull class]]){
            if([_arrayData[@"status"]intValue ] ==1){
                // mix panel
                NSString *objectIDMIxpanel,*contentType,*contentID;
                contentID=[NSString stringWithFormat:@"%@",_dictofPost[@"objectID"]];
                NSString *Posted_on,*PostGroupID;
                NSString *groupnamegot;
                for (int i=0;i<[items count]; i++) {
                    NSString *checkID=[NSString stringWithFormat:@"%@",[[items objectAtIndex:i]valueForKey:@"id"]];
                    if ([groupid isEqualToString:checkID]) {
                        groupnamegot=[[items objectAtIndex:i]valueForKey:@"name"];
                    }
                }
                
                //        Posted_on=groupnamegot;
                Posted_on=@"Group";
                PostGroupID=[NSString stringWithFormat:@"%@",[_arrayData objectForKey:@"postId"]];
                if ([PostGroupID isEqualToString:@""]) {
                    PostGroupID=@"";
                }
                
                
                if ([_status_para isEqualToString:@"1"]) {
                    contentType=@"Update";
                    
                }else   if ([_status_para isEqualToString:@"2"]) {
                    contentType=@"Prayer Request";
                }
                NSString *emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
                
                if ([emailID isEqualToString:@""]||[emailID isEqualToString:@"(null)"]) {
                    emailID=@"";
                }
                else{
                    emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
                }
                
                
                NSString *currentDate=[Constant1 createTimeStamp];
                // Post
                [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last post":currentDate}];
                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total posts" by:@1];
                
                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Content ID":PostGroupID,@"Posted by":emailID,@"Content type":contentType,@"Posted on":Posted_on,@"Date of last post":currentDate}];
                
                [[AppDelegate sharedAppDelegate].mxPanel track:@"Post"
                properties:@{@"Content ID":PostGroupID,@"Posted by":emailID,@"Content type":contentType,@"Posted on":Posted_on,@"Date of last post":currentDate}];
                //
                
                messageArray=[_arrayData valueForKey:@"message"];
                
            }else{
                messageArray=[_arrayData valueForKey:@"message"];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
            AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
                [appDelegate hidehud];
                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                
                [self.navigationController popViewControllerAnimated:NO];
                
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{

            AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            [appDelegate hidehud];
            });

        }
        
//        NSLog(@"succes");
    }];
    [uploadTask resume];
    

}


-(void)sendToserver1{
    
    [self.view endEditing:YES];
      if ([_status_para isEqualToString:@"1"]) {
              [TenjinSDK sendEventWithName:@"StatusUpdate_Submit_Tapped"];
             [FBSDKAppEvents logEvent:@"StatusUpdate_Submit_Tapped"];

          [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"StatusUpdate_Submit_Tapped"]];
         
        }else   if ([_status_para isEqualToString:@"2"]) {
          [TenjinSDK sendEventWithName:@"PrayerRequest_Submit_Tapped"];
         [FBSDKAppEvents logEvent:@"PrayerRequest_Submit_Tapped"];
         [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"PrayerRequest_Submit_Tapped"]];
    }
    
    NSInteger lengthOfString = [[_textView_status.text stringByReplacingOccurrencesOfString:@" " withString:@""] length];
    
    
    if (!lengthOfString || [_textView_status.text isEqualToString:@"Enter your post"]|| _textView_status.text.length==0) {
        [self.view makeToast:@"Please enter post" duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else if ([_choseGroupBTN.titleLabel.text isEqualToString:@"Select Where to Post "] && !isGroupPost)
    {
        [self.view makeToast:@"Please, Choose a group to Post !" duration:1.0 position:CSToastPositionCenter];
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate showhud];
        
    });

    
    NSString *boundary = @"SportuondoFormBoundary";
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"token\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"] dataUsingEncoding:NSUTF8StringEncoding]];
//     NSLog(@"api_token %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"]);
    // parameter Token
    

   
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"subject_type\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // object Type
    
    [body appendData:[@"user"   dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"subject_id\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    // subject type
    
    NSString *subjectID=[NSString stringWithFormat:@"%@",_dictofPost[@"subjectID"]];
    
//    NSLog(@"subject is %@",subjectID);
    
    [body appendData:[subjectID   dataUsingEncoding:NSUTF8StringEncoding]];
    
    // groupid

    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"object_type\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"user"   dataUsingEncoding:NSUTF8StringEncoding]];
    
    // changed by new WS services
    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"Content-Disposition: form-data; name=\"object_id\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"content\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[_textView_status.text   dataUsingEncoding:NSUTF8StringEncoding]];
    
        for(int i=0;i<[imageArray count];i++)
    {
        CGFloat compression = 1.0f;
        CGFloat maxCompression = 0.1f;
         UIImage *img=[Constant1 compressImage:[imageArray objectAtIndex:i]];
        NSData *imageData1 = UIImageJPEGRepresentation(img, compression);
        BOOL pngImage = YES;
        if ([imageData1 length] > 500000 && compression > maxCompression)
        {
            compression -= 0.10;
            imageData1 = UIImageJPEGRepresentation(img, compression);
            pngImage = NO;
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        if(pngImage){
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"wedshare%d.jpg\"\r\n", @"attachment[]",i+1] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else{
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"wedshare_%d.jpg\"\r\n", @"attachment[]",i+1] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData1];
        
    }
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"post_type\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[_status_para  dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   
                                                   @"Accept"        : @"application/json",
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    // Create the session
    // We can use the delegate to track upload progress
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    // group id in new service
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"object_id\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[groupid  dataUsingEncoding:NSUTF8StringEncoding]];
//    NSLog(@"group id is %@",groupid);
    //

   
    // posted on my wall
    
    
    NSURL *url;
    if ([_choseGroupBTN.titleLabel.text isEqualToString:@"On my wall"]) {
        NSString *objectID=[NSString stringWithFormat:@"%@",_dictofPost[@"objectID"]];
        [body appendData:[objectID   dataUsingEncoding:NSUTF8StringEncoding]];
        url = [NSURL URLWithString:[NSString stringWithFormat:mywallpostStatusViewService]];
    }
    else{
            url = [NSURL URLWithString:[NSString stringWithFormat:wallStatusViewService]];
    }
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = body;
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:body];
    
//    NSLog(@"Request body %@", [[NSString alloc] initWithData:body  encoding:NSUTF8StringEncoding]);
    
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Process the response
        NSDictionary*  _arrayData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"_arrayData is%@",myString);
        NSMutableArray *messageArray;
        [[NSNotificationCenter defaultCenter] postNotificationName: @"reloadtable" object:nil userInfo:nil];
        
        [AppDelegate sharedAppDelegate].postimagecheckNotnull=YES;
        [AppDelegate sharedAppDelegate].postimageWebServiceNO=YES;
        if ([_arrayData isKindOfClass:[NSDictionary class]]) {
        if (![_arrayData isKindOfClass:[NSNull class]]){
            if([_arrayData[@"status"]intValue ] ==1){
                
                
                if([_arrayData[@"is_show"]intValue] == 1)
                {
                    messageArray=[_arrayData valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
                        [appDelegate hidehud];
                            postSuccessViewController *postsucccessView=[self.storyboard instantiateViewControllerWithIdentifier:@"postSuccessViewController"];
                            NSString *groupnamegot,*getGroupID;
                            for (int i=0;i<[items count]; i++) {
                                NSString *checkID=[NSString stringWithFormat:@"%@",[[items objectAtIndex:i]valueForKey:@"id"]];
                                if ([groupid isEqualToString:checkID]) {
                                    groupnamegot=[[items objectAtIndex:i]valueForKey:@"name"];
                                }
                            }
                            postsucccessView.groupname=groupnamegot;
                        NSString *objectIDMIxpanel,*contentType,*Posted_on,*postID;
                        postID=[NSString stringWithFormat:@"%@",[_arrayData objectForKey:@"postId"]];
                        if ([postID isEqualToString:@""]) {
                            postID=@"";
                        }
                        
                        if ([_choseGroupBTN.titleLabel.text isEqualToString:@"On my wall"]) {
                        Posted_on=@"Wall";
                            objectIDMIxpanel=[NSString stringWithFormat:@"%@",_dictofPost[@"objectID"]];
                            
                        }
                        else{
                            //                            Posted_on=groupnamegot;
                            Posted_on=@"Group";
                            getGroupID=postID;
                        }
                        
                        NSString *emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
                        
                        if ([emailID isEqualToString:@""]||[emailID isEqualToString:@"(null)"]) {
                            emailID=@"";
                        }
                        else{
                            emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
                        }
                        
                        if ([_status_para isEqualToString:@"1"]) {
                            contentType=@"Update";
                            
                        }else   if ([_status_para isEqualToString:@"2"]) {
                            contentType=@"Prayer Request";
                        }
                        NSString *currentDate=[Constant1 createTimeStamp];
                        
                        // Post
                        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last post":currentDate}];
                        
                        [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total posts" by:@1];
                        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Content ID":getGroupID,@"Posted by":emailID,@"Content type":contentType,@"Posted on":Posted_on,@"Date of last post":currentDate}
                         ];
                        
                        [[AppDelegate sharedAppDelegate].mxPanel track:@"Post"
                                                            properties:@{@"Content ID":getGroupID,@"Posted by":emailID,@"Content type":contentType,@"Posted on":Posted_on,@"Date of last post":currentDate}];
                        
                        [self.navigationController pushViewController:postsucccessView animated:NO];
                            });
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
                        [appDelegate hidehud];
                        [self.view makeToast:@"You have posted successfully."  duration:1.0 position:CSToastPositionCenter];
                        NSString *Posted_on;
                        NSString *groupnamegot;
                        for (int i=0;i<[items count]; i++) {
                            NSString *checkID=[NSString stringWithFormat:@"%@",[[items objectAtIndex:i]valueForKey:@"id"]];
                            if ([groupid isEqualToString:checkID]) {
                                groupnamegot=[[items objectAtIndex:i]valueForKey:@"name"];
                            }
                        }
                        NSString *groupidsend,*contentType,*objectIDMIxpanel,*postID;
                        postID=[NSString stringWithFormat:@"%@",[_arrayData objectForKey:@"postId"]];
                        if ([postID isEqualToString:@""]) {
                            postID=@"";
                        }
                        if ([_choseGroupBTN.titleLabel.text isEqualToString:@"On my wall"]) {
                            Posted_on=@"Wall";
                        objectIDMIxpanel=[NSString stringWithFormat:@"%@",_dictofPost[@"objectID"]];
                        groupidsend=postID;
                        }
                        else{
//                            Posted_on=groupnamegot;
                            Posted_on=@"Group";
                            groupidsend=postID;
 
                        }
                        
                        NSString *emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
                        
                        if ([emailID isEqualToString:@""]||[emailID isEqualToString:@"(null)"]) {
                            emailID=@"";
                        }
                        else{
                            emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
                        }

                        
                        if ([_status_para isEqualToString:@"1"]) {
                            contentType=@"Update";
                            
                        }else   if ([_status_para isEqualToString:@"2"]) {
                        contentType=@"Prayer Request";

                        }
                    NSString *currentDate=[Constant1 createTimeStamp];
                    
                        // Post
                         [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last post":currentDate}];
                        
                        [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total posts" by:@1];
                        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Content ID":groupidsend,@"Posted by":emailID,@"Content type":contentType,@"Posted on":Posted_on,@"Date of last post":currentDate}
                         ];

                    [[AppDelegate sharedAppDelegate].mxPanel track:@"Post"
                             properties:@{@"Content ID":groupidsend,@"Posted by":emailID,@"Content type":contentType,@"Posted on":Posted_on,@"Date of last post":currentDate}];
                        
                AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        if (getVar.dismischeckViewcontroller==YES) {
                            getVar.dismischeckViewcontroller=NO;
                            [self dismissViewControllerAnimated:nil completion:nil];
                        }
                        [self performSelector:@selector(goBack) withObject:nil afterDelay:1.0];
                    });
                }
                
                
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
                    [appDelegate hidehud];
                    postSuccessViewController *postsucccessView=[self.storyboard instantiateViewControllerWithIdentifier:@"postSuccessViewController"];
                    NSString *groupnamegot;
                    for (int i=0;i<[items count]; i++) {
                        NSString *checkID=[NSString stringWithFormat:@"%@",[[items objectAtIndex:i]valueForKey:@"id"]];
                        if ([groupid isEqualToString:checkID]) {
                            groupnamegot=[[items objectAtIndex:i]valueForKey:@"name"];
                        }
                    }
                    postsucccessView.groupname=groupnamegot;
                    [self presentViewController:postsucccessView animated:YES completion:nil];
                    
                });
                
            }
            
        }
    }
        
//        NSLog(@"succes");
    }];
    
    [uploadTask resume];
}

                                   
                                   
                                   

-(void) goBack
{
    dispatch_after(1.0, dispatch_get_main_queue(), ^(void){
        [self.navigationController popViewControllerAnimated:NO];
    });
}


-(NSString*)base64String:(UIImage*)image{
    NSString *base64EncodeStr = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    base64EncodeStr = [base64EncodeStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    return base64EncodeStr;
}
- (IBAction)tap:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)beginEditing: (id)sender {
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    CGFloat _width = self.view.frame.size.width;
    CGFloat _height = 40.0;
    UIImage *chatImage = [UIImage imageNamed:@"camera_30"];
    UIButton *chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [chatButton setTitle:@"Add Photo" forState:UIControlStateNormal];
    chatButton.frame = (CGRect) {
        .size.width = 140,
        .size.height = 40,
    };
    [chatButton addTarget:self action:@selector(openphoto_popup) forControlEvents:UIControlEventTouchUpInside];
    [chatButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    chatButton.titleEdgeInsets=UIEdgeInsetsMake(0.0 , 0.0, 0.0f, -30);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, 30, 30)];
    [imageView setImage:chatImage];
    [chatButton addSubview:imageView];
      UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, -_height, _width, _height)];
    UIBarButtonItem *barButton= [[UIBarButtonItem alloc] initWithCustomView:chatButton];
    [barItems addObject:barButton];
    
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexible];
    
    // Done button
       chatButton1=[UIButton buttonWithType:UIButtonTypeCustom];
        [chatButton1 setFrame:CGRectMake(0, 2.0, 45.0, 40.0)];
        [chatButton1 addTarget:self action:@selector(closeButton) forControlEvents:UIControlEventTouchUpInside];
        [chatButton1 setImage:[UIImage imageNamed:@"1497284886_tick_30"] forState:UIControlStateNormal];
       UIBarButtonItem *barButtonItem1=  [[UIBarButtonItem alloc] initWithCustomView:chatButton1];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        chatButton1.alpha=0.5;
  
    });
    
    [barItems addObject:barButtonItem1];
    [toolbar setItems:barItems animated:YES];

    if ([sender isEqual:_textView_status]) {
        _textView_status.inputAccessoryView = toolbar;
    }
}

-(void)closeButton{
[self.view endEditing:YES];
}

-(void)btnSubmitClicked:(UIButton*)sender{
    [self.view endEditing:YES];
    //    [self.datePicker removeFromSuperview];
}
-(void)btnCancelClicked:(UIButton*)sender{
    [self.view endEditing:YES];
   }
- (void)myNotificationMethod:(NSNotification*)notification
{
    
}
- (void)myNotificationMethod2:(NSNotification*)notification
{
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (getVar.checkDisapper) {
        [[AppDelegate sharedAppDelegate] addsView];
//        getVar.checkDisapper=NO;
    }
   
}


-(void)appEnterInForegroundStatus{
    statusbackground=statusbackground+1;
    if (statusbackground%3==0) {
    [[NSUserDefaults standardUserDefaults]setObject:@"Yes" forKey:@"StatusScreen"];
     [[NSUserDefaults standardUserDefaults] synchronize];
    statusbackground=0;
    }
}


-(void)GrouplistinPost  {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                               };
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:grouplistFeedPost withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
//                            NSMutableArray *grouplistArray;
                            items=[response valueForKey:@"data"];
//                            items=[[[grouplistArray reverseObjectEnumerator] allObjects] mutableCopy];
                        });
                    }
                }
            }
            else{
                NSMutableArray  *messageArray=[response valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                });
            }
            
        } errorBlock:^(NSError *error)
         {
         }];
        
    }
}

- (IBAction)choseGroupMethod:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    
    if(viewPicker)
    {
        [viewPicker removeFromSuperview];
        viewPicker=nil;
    }
    
    if(viewPicker1)
    {
        [viewPicker1 removeFromSuperview];
        viewPicker1=nil;
    }

    
   viewPicker = [[UIView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height - HeightForPickerView, self.view.frame.size.width, HeightForPickerView)];
    viewPicker.backgroundColor=[UIColor whiteColor];
    
    UIToolbar* toolbar = [[UIToolbar alloc] init];
    toolbar.frame=CGRectMake(0,0,self.view.frame.size.width,35);
    [toolbar setBarStyle:UIBarStyleBlackTranslucent];
    //toolbar.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0];
    
    UIButton *CancelBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    [CancelBTN setTitle:@"Cancel" forState:UIControlStateNormal];
    CancelBTN.frame = CGRectMake(0, 0, 60, 30);
    [CancelBTN addTarget:self action:@selector(CancelBTN) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:CancelBTN];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIButton *Done = [UIButton buttonWithType:UIButtonTypeCustom];
    [Done setTitle:@"Done" forState:UIControlStateNormal];
    Done.frame = CGRectMake(self.view.frame.size.width-80,0 , 48, 30);
    [Done addTarget:self action:@selector(DoneBTN) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBarButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:Done];
    
    [toolbar setItems:[[NSArray alloc] initWithObjects:backBarButtonItem,flexibleItem,backBarButtonItem1, nil]];
    
    pickerView = [[UIPickerView alloc]init] ;
    pickerView.backgroundColor=[UIColor whiteColor];
    pickerView.frame=CGRectMake(0,44, self.view.frame.size.width,135 );
    pickerView.tag=9001;
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    [viewPicker addSubview:pickerView];
    [viewPicker addSubview:toolbar];
    [self.view addSubview:viewPicker];
}

- (IBAction)posttypeMethod:(id)sender {

    if(viewPicker)
    {
        [viewPicker removeFromSuperview];
        viewPicker=nil;
    }
    
    if(viewPicker1)
    {
        [viewPicker1 removeFromSuperview];
        viewPicker1=nil;
    }

    viewPicker1 = [[UIView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height - HeightForPickerView, self.view.frame.size.width, HeightForPickerView)];
    viewPicker1.backgroundColor=[UIColor whiteColor];
    
    UIToolbar* toolbar = [[UIToolbar alloc] init];
    toolbar.frame=CGRectMake(0,0,self.view.frame.size.width,35);
    [toolbar setBarStyle:UIBarStyleBlackOpaque];
    
    UIButton *CancelBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    [CancelBTN setTitle:@"Cancel" forState:UIControlStateNormal];
    CancelBTN.frame = CGRectMake(0, 0, 60, 30);
    [CancelBTN addTarget:self action:@selector(CancelBTN) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:CancelBTN];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIButton *Done = [UIButton buttonWithType:UIButtonTypeCustom];
    [Done setTitle:@"Done" forState:UIControlStateNormal];
    Done.frame = CGRectMake(self.view.frame.size.width-80,0 , 48, 30);
    [Done addTarget:self action:@selector(DoneBTN) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBarButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:Done];
    [toolbar setItems:[[NSArray alloc] initWithObjects:backBarButtonItem,flexibleItem,backBarButtonItem1, nil]];
    
    pickerView = [[UIPickerView alloc]init] ;
    pickerView.backgroundColor=[UIColor whiteColor];
    pickerView.frame=CGRectMake(0,44, self.view.frame.size.width, 135);
    pickerView.tag=9002;
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    [viewPicker1 addSubview:pickerView];
    [viewPicker1 addSubview:toolbar];
    [self.view addSubview:viewPicker1];
}



-(void)DoneBTN{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    
    if (pickerView.tag==9001) {
        NSString *groupitemName=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"groupChoose"]];
        
        
        NSDictionary *dictGrup = [items objectAtIndex:[[[NSUserDefaults standardUserDefaults] valueForKey:@"groupIndex"] integerValue]];
        NSString *check_restricted=[NSString stringWithFormat:@"%@",[dictGrup valueForKey:@"is_restricted"]];
        
        if ([check_restricted isEqualToString:@"1"]) {
            login=[self.storyboard instantiateViewControllerWithIdentifier:@"popGroupViewController"];
            login.owner = (id)self;
            self.definesPresentationContext = YES;
            login.modalPresentationStyle = UIModalPresentationOverFullScreen;
            login.view.backgroundColor=[UIColor clearColor];
            [self.navigationController presentViewController:login animated:YES completion:^{
                login.view.backgroundColor =[UIColor colorWithRed:(0.0/255.0f) green:(0.0/255.0f) blue:(0.0/255.0f) alpha:0.5];
            }];
            
            [[NSUserDefaults standardUserDefaults] setObject:[dictGrup valueForKey:@"object_id"] forKey:@"objectid"];
            [[NSUserDefaults standardUserDefaults] setObject:[dictGrup valueForKey:@"name"] forKey:@"name"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
        [_choseGroupBTN setTitle:groupitemName forState:UIControlStateNormal];
        [viewPicker removeFromSuperview];
        [pickerView removeFromSuperview];
    }
    else{
        NSString *posttypeStr=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"postType"]];
        [_postTypeBTN setTitle:posttypeStr forState:UIControlStateNormal];
        [viewPicker1 removeFromSuperview];
        [pickerView removeFromSuperview];
    }

}
-(void)CancelBTN{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    if (pickerView.tag==9001) {
        [viewPicker removeFromSuperview];
        [pickerView removeFromSuperview];
        [self.choseGroupBTN setTitle:@"Select Where to Post " forState:UIControlStateNormal];
    
    }
    else{
        [viewPicker1 removeFromSuperview];
        [pickerView removeFromSuperview];
    }
    
    
}
#pragma pickerview delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
    
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    if (pickerView.tag==9001) {
      return [items count];
    }
    else{
        return [posttypeArray count];
        
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
//    NSLog(@"item array %@",items);
    if (pickerView.tag==9001) {
       return[[items objectAtIndex:row]valueForKey:@"name"];
    }
    else{
        return[posttypeArray objectAtIndex:row];
        
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag==9001) {
      [_choseGroupBTN setTitle:[[items objectAtIndex:row]valueForKey:@"name"] forState:UIControlStateNormal];
        groupid=[NSString stringWithFormat:@"%@",[[items objectAtIndex:row]valueForKey:@"id"]];
        [[NSUserDefaults standardUserDefaults] setObject:[[items objectAtIndex:row]valueForKey:@"name"] forKey:@"groupChoose"];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",row] forKey:@"groupIndex"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else{
        [_postTypeBTN setTitle:[posttypeArray objectAtIndex:row] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setObject:[posttypeArray objectAtIndex:row] forKey:@"postType"];
        NSString *checktype=[NSString stringWithFormat:@"%@",[posttypeArray objectAtIndex:row]];
        if ([checktype  isEqualToString:@"Prayer Request"]) {
            _status_para=@"2";
        }
        else{
          _status_para=@"1";
        }
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
   
}


-(void)popUpSubmitClicked:(NSString *)dob gender:(NSString*)gender;
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        NSDictionary *dict = @{
                               @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"token" :[[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"date_of_birth":dob,
                               @"gender":gender,
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:updateProfile withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1)
                        {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (login) {
                                    [self dismissViewControllerAnimated:login completion:nil];
                                }
                            });
                        }
                        else{
                            NSMutableArray *messageArray=nil;
                            messageArray=[response valueForKey:@"message"];
                            if ([messageArray count]>0) {
                                UIAlertView *loginAlert=[[UIAlertView alloc]initWithTitle:appTitle message:[messageArray objectAtIndex:0] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] ;
                                [loginAlert show];
                            }
                        }
                    });
                }
            }
        } errorBlock:^(NSError *error)
         {
//             NSLog(@"error is %@",error);
         }];
    }
}


@end
