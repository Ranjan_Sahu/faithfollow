//
//  FeedTableViewCell.m
//  wireFrameSplash
//
//  Created by Vikas on 21/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "FeedTableViewCell.h"

@implementation FeedTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.formalView.layer.borderWidth=0.6f;
    self.formalView.layer.cornerRadius=8.0;
    
    //  self.formalView.backgroundColor=[UIColor blackColor];
    
    self.formalView.layer.borderColor=[UIColor grayColor].CGColor;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
