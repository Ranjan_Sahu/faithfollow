//
//  WallView.m
//  wireFrameSplash
//
//  Created by Vikas on 25/05/16.
//  Copyright © 2016 home. All rights reserved.
//



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
//
//  HomeViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//



#import "FeedViewController.h"
#import "FeedTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CustomCommentView.h"
#import "StatusViewController.h"
#import "FeedIMageTableViewCell.h"
#import "FeedpostTableViewCell.h"
#import "WallView.h"
#import "HeaderView.h"
#import "FeedwithImagePostTableViewCell.h"

#define SKY_BLUE_COLOR [UIColor colorWithRed:0.1137 green:0.4824 blue:0.8065 alpha:1.0]

@interface WallView ()<UITabBarDelegate,UITextViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    CGFloat heightofCell;
    NSInteger selectedRow;
    UIView *lineView ;
    NSMutableArray *arrFeeds;
    NSMutableArray *arrWallFeeds;
    NSString *comments;
    NSMutableArray *commentsarr;
    NSDictionary *get_total_commnet;
    BOOL comments_their;
    NSArray *arr;
    FeedpostTableViewCell *feed_cell;
    FeedwithImagePostTableViewCell *feedImage_cell;
    
    
    BOOL keyboard,liked_call;
    
    NSDictionary *userInfo;
    NSUInteger cellindex;
    NSString *sendComments_method;
    NSMutableArray *all_comments;
    NSDictionary *commment_info;
    int tagf;
}


@end

@implementation WallView
#define  kCellName @"FeedpostTableViewCell"
#define  kCellName1 @"FeedwithImagePostTableViewCell"

- (void) awakeFromNib
{
    [super awakeFromNib];
    
}


-(id) initWithframe:(CGRect)frame andUserInfo:(NSDictionary*)userinfo{
    self = [super init];
    if(self) {
//        NSLog(@"_init: %@", userinfo);
        self= [[[NSBundle mainBundle] loadNibNamed:@"WallView" owner:self options:nil] objectAtIndex:0];
        userInfo=userinfo;
        //_tableViewOutlet.backgroundColor=[UIColor greenColor];
        [self.tableViewOutlet   registerNib:[UINib nibWithNibName:kCellName bundle:nil] forCellReuseIdentifier:kCellName];
        arrFeeds=[[NSMutableArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Feed" ofType:@"plist"]];
        [self.tableViewOutlet   registerNib:[UINib nibWithNibName:kCellName1 bundle:nil] forCellReuseIdentifier:kCellName1];
        selectedRow=-1;
        _tableViewOutlet.delegate=self;
        _tableViewOutlet.dataSource=self;
        
        //  [self sendrequest:@""];
    }
    return(self);
}





-(void)webservice_call{
    // NSLog(@"hello");
    if ([userInfo[@"wallfor"]isEqualToString:@"myself"]) {
        _upperView.layer.borderWidth=0.5f;
        _upperView.layer.borderColor=[UIColor grayColor].CGColor;
        _status_btn.layer.borderColor=[UIColor blackColor].CGColor;
        _status_btn.layer.borderWidth=0.5f;
        commentsarr=[[NSMutableArray alloc]init];
        //    [_tableViewOutlet reloadData];
        [_textViewTXT resignFirstResponder];
        
        [self initial_FeedserviceCall];
        
        [_tableViewOutlet reloadData];
    }
    
    
    //
    
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [_textViewTXT resignFirstResponder];
    [self.delegate1 FunctionOne:@"open statusView"];
    
}
- (IBAction)btn_act:(id)sender {
    
}
- (IBAction)status_act:(id)sender {
}

- (IBAction)prayer_act:(id)sender {
}

- (IBAction)photo_act:(id)sender {
}
- (IBAction)comments_act:(UIButton*)sender {
    
    //    if (sender.selected)
    //    {
    //        selectedRow=-1;
    //        [_tableViewOutlet reloadData];
    //        [sender setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    //        sender.selected=false;
    //    }else{
    //        if (indexPath != nil)
    //        {
    //
    //            [sender setTitleColor:SKY_BLUE_COLOR forState:UIControlStateNormal];
    //            selectedRow=indexPath.row;
    //
    //
    //            FeedTableViewCell *cell = (FeedTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    //            cell.commentsView.txtComment.layer.borderColor=[UIColor blackColor].CGColor;
    //            cell.commentsView.txtComment.layer.borderWidth=1.0f;
    //            //            cell.commentsView.btnSend.backgroundColor=[UIColor grayColor];
    //            //            [cell.commentsView.btnSend setTitle:@"Send" forState:UIControlStateNormal];
    //            cell.commentsView.btnSeeMore.hidden=YES;
    //            cell.commentsView.txtComment.text=@"";
    //            cell.commentsView.txtComment.delegate=self;
    //            feed_cell=cell;
    //            NSUInteger index=indexPath.row;
    //            [self commentsView:index and:cell];
    //
    //            [_tableViewOutlet reloadData];
    //
    //        }
    //        sender.selected=true;
    //    }
    
    
    //    FeedTableViewCell *cell = (FeedTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    //    feed_cell=cell;
    //        NSUInteger index=indexPath.row;
    //     [self commentsView:index and:cell];
    
    
    //    [self.delegate1 btn_pressed:@"seemore" andtotal_comments:all_comments
    //
    //                        andpost:commment_info];
}
#pragma mark TableView Data Source methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrWallFeeds count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
    NSMutableArray *attachment=dict[@"get_attachments"];
    //  int attachment_count = [attachment intValue];
    if (attachment.count==0) {
        FeedpostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        return cell;
        
    }else{
        FeedwithImagePostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName1 forIndexPath:indexPath];
        [self setUpCell2:cell atIndexPath:indexPath];
        return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
    NSMutableArray *attachment=dict[@"get_attachments"];
    //  int attachment_count = [attachment intValue];
    if (attachment.count==0) {
        
        static FeedpostTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell = [self.tableViewOutlet dequeueReusableCellWithIdentifier:kCellName];
        });
        [self setUpCell:cell atIndexPath:indexPath];
        
        return [self calculateHeightForConfiguredSizingCell:cell];
        
    }else{
        static FeedwithImagePostTableViewCell *cell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell = [self.tableViewOutlet dequeueReusableCellWithIdentifier:kCellName1];
        });
        [self setUpCell2:cell atIndexPath:indexPath];
        
        return [self calculateHeightForConfiguredSizingCell:cell];
    }
    
    
    
    
}
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsUpdateConstraints];
    [sizingCell updateConstraintsIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}
- (void)setUpCell2:(FeedwithImagePostTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    float width=[UIScreen mainScreen].bounds.size.width-(20);
    cell.lbl_post.preferredMaxLayoutWidth = width; // This is necessary to allow the label
    
    NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
    NSDictionary *get_object_user_details=dict[@"get_object_user_details"];
    NSString *objectName=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
    NSDictionary *get_subject_user_details=dict[@"get_subject_user_details"];
    NSDictionary *subject_user_details=get_subject_user_details[@"usersprofile"];
    NSString *subjectName=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
    get_total_commnet=dict[@"get_total_commnets"];
    NSDictionary* get_total_praised=dict[@"get_total_praised"];
    NSDictionary *get_attachments=dict[@"get_attachments"];
    NSString *str=[NSString stringWithFormat:@"%@ have posted %@",objectName,dict[@"content"]];
    
    NSString *str1=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:subject_user_details[@"profile_picture"]]];
    str1=[NSString stringWithFormat:@"%@%@",ImageapiUrl,str1];
    //
    
    
    
    if ( [dict[@"get_total_praised"] isKindOfClass:[NSNull class]]) {
        
        [cell.btn_praisedCount setTitle:@"0 Praised" forState:UIControlStateNormal];
        [cell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_praisedCount addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        NSString *like=[NSString stringWithFormat:@"%@",dict[@"is_like"]];
        
        
        int like_count = [like intValue];
        
        if (like_count==0) {
            NSString *total_count=[NSString stringWithFormat:@"%@ Praised",[Constant1 getStringObject:get_total_praised[@"count"]]];
            [cell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn_praisedCount addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
            
            cell.imageview_praised.text=@"Praised";
            
        }else{
            NSString *like1=[NSString stringWithFormat:@"%@",get_total_praised[@"count"]];
            int main_like= [like1 intValue];
            NSString *total_count=[NSString stringWithFormat:@"You and %d Praised",main_like-1];
            [cell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
            [cell.btn_praised addTarget:self action:@selector(btnunpraised_act:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn_praisedCount addTarget:self action:@selector(btnunpraised_act:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.imageview_praised.text=@"UnPraised";
        }
    }
    
    if ([get_total_commnet isKindOfClass:[NSNull class]]){
        
        [cell.btn_commentsCount setTitle:@"0 Comments" forState:UIControlStateNormal];
        
    }else{
        NSString *total_count=[NSString stringWithFormat:@"%@ Comments",[Constant1 getStringObject:get_total_commnet[@"count"]]];
        [cell.btn_commentsCount setTitle:total_count forState:UIControlStateNormal];
    }
    //
    cell.lbl_personName.text=subjectName;
    [cell.imageview_person sd_setImageWithURL:[NSURL URLWithString:str1] placeholderImage:[UIImage imageNamed:@"person"]];
    
    cell.lbl_date.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:dict[@"updated_at"]]];
    //  cell.lbl_post.text=dict[@"content"];
    
    NSString *post_Imagestring;
    NSMutableArray *imagearr=dict[@"get_attachments"];
    for(NSDictionary *dictionary in imagearr) {
        post_Imagestring=[NSString stringWithFormat:@"%@",dictionary[@"file"]];
        
    }
    
    //NSString *str11=[NSString stringWithFormat:@"%@",dict[@"get_attachments"][@"file"]];
    NSString*  full_post=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,post_Imagestring];
    
    NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:full_post]];
    UIImage *image=[UIImage imageWithData:imageData];
    
    cell.imageview_post.image=image;
    if (image.size.height>image.size.width) {
        cell.heightofimageview.constant=cell.contentView.frame.size.height/3;
        cell.widthofimageview.constant=100;
        [cell.contentView layoutIfNeeded];
        
    }else{
        cell.heightofimageview.constant=(cell.contentView.frame.size.width/1.2)/2;
        cell.widthofimageview.constant=cell.contentView.frame.size.width/1.2;
        // cell.widthofimageview.constant=200;
        [cell.contentView layoutIfNeeded];
    }
//    NSLog(@"ima is%f %f",image.size.width,image.size.height);
    //  NSString *string1=@"Tor the past 33 years, I have looked in the mirror every morning and asked myself:answer has been 'No' for too many days in a row, I know I need to change something. -Steve Jobs,For the past 33 years, I have looked in the mirror every morning and asked myself: 'If today were the last day of my life, would I want to do what I am about to do today?' And whenever the answer has been 'No' for too many days in a row, I know I need to change something. -Steve Jobs,For the past 33 years, I have looked in the mirror every morning and asked mys  'No' for too many days in a row, I know I need to change something. -Steve Jobs,For the past 33 years, I have looked in the mirror every morning and asked my";
    //NSString *string1=@"Tor the past 33 years, I ";
    
    NSString *string1=dict[@"content"];
    
    
    //CGSize ex=[self getHeight:string1];
    
    UILabel  * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 275, 9999)];
    label.numberOfLines=0;
    label.font = [UIFont systemFontOfSize:17.0];
    label.text = string1;
    
    CGSize maximumLabelSize = CGSizeMake(275, 9999);
    
    CGSize expectedSize = [label sizeThatFits:maximumLabelSize];
    
    
    cell.lbl_post.text=  nil;
    cell.lbl_post.text=string1;
    
    if (expectedSize.height<80) {
        cell.heightofLabelconstraint.constant=expectedSize.height;
        [cell layoutIfNeeded];
        [cell.btn_seemore setHidden:YES];
        
        //NSLog( @"hello");
    }else{
        cell.heightofLabelconstraint.constant=80;
        [cell layoutIfNeeded];
        [cell.btn_seemore setHidden:NO];
    }
    cell.btn_seemore.tag=indexPath.row;
    [cell.btn_seemore addTarget:self action:@selector(readMore:) forControlEvents:UIControlEventTouchUpInside];
    
    //    [cell.lbl_post sizeToFit];
//    NSLog(@"ex is %@",cell.lbl_post.text);
    
    
    
}
-(void)readMore:(UIButton*)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    
//    NSLog(@"tag is%ld",(long)indexPath.row);
    FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    feedImage_cell=cell;
    [self commentsView:indexPath.row and:cell andcellType:@"unFeedpost"];
}
-(CGSize)getHeight:(NSString *)str
{
    UILabel  * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 275, 9999)];
    label.numberOfLines=0;
    label.font = [UIFont systemFontOfSize:17.0];
    label.text = str;
    
    CGSize maximumLabelSize = CGSizeMake(275, 9999);
    
    CGSize expectedSize = [label sizeThatFits:maximumLabelSize];
    return expectedSize;
}





- (void)setUpCell:(FeedpostTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    float width=[UIScreen mainScreen].bounds.size.width-(20);
    cell.lbl_post.preferredMaxLayoutWidth = width; // This is necessary to allow the label
    NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
    NSDictionary *get_object_user_details=dict[@"get_object_user_details"];
    NSString *objectName=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
    NSDictionary *get_subject_user_details=dict[@"get_subject_user_details"];
    NSDictionary *subject_user_details=get_subject_user_details[@"usersprofile"];
    NSString *subjectName=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
    get_total_commnet=dict[@"get_total_commnets"];
    NSDictionary* get_total_praised=nil;
    
    
    cell.lbl_post.text=@"For the past 33 years, I have looked in the mirror every morning and asked myself: 'If today were the last day of my life, would I want to do what I am about to do today?' And whenever the answer has been 'No' for too many days in a row, I know I need to change something. -Steve Jobs,For the past 33 years, I have looked in the mirror every morning and asked myself: 'If today were the last day of my life, would I want to do what I am about to do today?' And whenever the answer has been 'No' for too many days in a row, I know I need to change something. -Steve Jobs,For the past 33 years, I have looked in the mirror every morning and asked mys";
    
    
    
    NSString *str=[NSString stringWithFormat:@"%@ have posted %@",objectName,dict[@"content"]];
    
    NSString *str1=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:subject_user_details[@"profile_picture"]]];
    str1=[NSString stringWithFormat:@"%@%@",ImageapiUrl,str1];
    if ( [dict[@"get_total_praised"] isKindOfClass:[NSNull class]]) {
        
        [cell.btn_praisedCount setTitle:@"0 Praised" forState:UIControlStateNormal];
        [cell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_praisedCount addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
        cell.imageview_praised.text=@"Praised";
    }else{
        get_total_praised=dict[@"get_total_praised"];
        NSString *like=[NSString stringWithFormat:@"%@",dict[@"is_like"]];
        int like_count = [like intValue];
        if (like_count==1) {
            NSString *like1=[NSString stringWithFormat:@"%@",get_total_praised[@"count"]];
            int main_like= [like1 intValue];
            NSString *total_count=[NSString stringWithFormat:@"You and %d Praised",main_like-1];
            [cell.btn_praised addTarget:self action:@selector(btnunpraised_act:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn_praisedCount addTarget:self action:@selector(btnunpraised_act:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
            cell.imageview_praised.text=@"Unpraised";
        }else{
            
            
            NSString *total_count=[NSString stringWithFormat:@"%@ Praised",[Constant1 getStringObject:get_total_praised[@"count"]]];
            [cell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn_praisedCount addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
            cell.imageview_praised.text=@"Praised";
        }
    }
    
    
    if ([get_total_commnet isKindOfClass:[NSNull class]]){
        
        [cell.btn_commentsCount setTitle:@"0 Comments" forState:UIControlStateNormal];
        
    }else{
        NSString *total_count=[NSString stringWithFormat:@"%@ Comments",[Constant1 getStringObject:get_total_commnet[@"count"]]];
        [cell.btn_commentsCount setTitle:total_count forState:UIControlStateNormal];
    }
    //
    cell.lbl_personName.text=subjectName;
    [cell.imageview_person sd_setImageWithURL:[NSURL URLWithString:str1] placeholderImage:[UIImage imageNamed:@"person"]];
    
    cell.lbl_date.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:dict[@"updated_at"]]];
    cell.lbl_post.text=dict[@"content"];
    [cell.btn_Comments addTarget:self action:@selector(btncomments_act:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btn_commentsCount addTarget:self action:@selector(btncomments_act:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    cell.btn_Comments.tag=indexPath.row;
    cell.btn_commentsCount.tag=indexPath.row;
    cell.btn_praised.tag=indexPath.row;
    cell.btn_praisedCount.tag=indexPath.row;
    
}

-(void)btncomments_act:(UIButton*)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    
//    NSLog(@"tag is%ld",(long)indexPath.row);
    FeedpostTableViewCell *cell = (FeedpostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    feed_cell=cell;
    [self commentsView:indexPath.row and:cell andcellType:@"Feedpost"];
    
    //    [self.delegate1 btn_pressed:@"seemore" andtotal_comments:all_comments
    //
    //                             andpost:commment_info];
    
}
-(void)btnpraised_act:(UIButton*)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    
    UITableViewCell* cell = (UITableViewCell *)sender.superview.superview;
//    NSLog(@"cell is%@",cell);
    NSIndexPath *indexPath1 = [self.tableViewOutlet indexPathForCell:cell];
    //    FeedpostTableViewCell *cell = (FeedpostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    //    feed_cell=cell;
//    NSLog(@"tag is%ld",sender.tag);
    NSDictionary *object_dict=[arrWallFeeds objectAtIndex:sender.tag];
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"object_id":object_dict[@"id"],
                           @"object_type":@"post",
                           
                           };
    [self sendRequest:dict andmethodName:wall_like andcell_index:sender.tag ];
    
}
-(void)btnunpraised_act:(UIButton*)sender{
//    NSLog(@"tag is%ld",sender.tag);
    NSDictionary *object_dict=[arrWallFeeds objectAtIndex:sender.tag];
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"object_id":object_dict[@"id"],
                           @"object_type":@"post",
                           
                           };
    [self sendRequest:dict andmethodName:wall_unlike andcell_index:sender.tag ];
    
    
    //[self sendRequest:dict andmethodName:wall_unlike andcell_index:sender.tag ];
}

-(void)likedRequest:(NSDictionary*)dict andmethodName:(NSString*)methodName andcell_index:(NSUInteger)cell_inde andcellType:(NSString*)Celltype{
    
    
    if ([Celltype isEqualToString:@"Feedpost"]) {
        
    }
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self  makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
//        NSLog(@"dict is%@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
            NSMutableArray *messageArray;
//            NSLog(@"response is%@",response);
            
            
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    
                    if([response[@"status"]intValue ] ==1){
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self feed];
                            
                        });
                    }
                    
                }else{
                    messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    });
                }
                
            }
            
            
            
        } errorBlock:^(id error)
         {
//             NSLog(@"error is %@",error);
         }];
    }
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 85;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"HeaderView"
                                                      owner:self
                                                    options:nil];
    
    UIView *headerView = [ nibViews objectAtIndex: 0];
    _textViewTXT=(UITextView*)[headerView viewWithTag:800];
    _textViewTXT.delegate=self;
    
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 85.0;
}

-(void)feed{
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"object_type":@"user",
                           @"object_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"filter_type":@"4",
                           
                           };
    [self sendRequest:dict andmethodName:allwalllist andcell_index:0];
    
    
}

-(void)praised_act:(UIButton*)sender{
    // if ([_call_View isEqualToString:@"Wall"]) {
    
}
-(void)un_praised_act:(UIButton*)sender{
    
    NSDictionary *object_dict=[arrWallFeeds objectAtIndex:sender.tag];
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"object_id":object_dict[@"id"],
                           @"object_type":@"post",
                           
                           };
    [self sendRequest:dict andmethodName:wall_unlike andcell_index:sender.tag ];
}

-(void)see_more:(UIButton*)sender{
    //    NSLog(@"dict is%@",all_comments);
    //    NSLog(@"dict is%@",[all_comments objectAtIndex:0]);
    [self.delegate1 btn_pressed:@"seemore" andtotal_comments:all_comments
     
                        andpost:commment_info];
    
}
-(void)sendRequest:(NSDictionary*)dict andmethodName:(NSString*)methodName andcell_index:(NSUInteger)cell_index{
    
    
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self  makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
//        NSLog(@"dict is%@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
            NSMutableArray *messageArray;
//            NSLog(@"response is%@",response);
            
            
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    
                    if([response[@"status"]intValue ] ==1){
                        arrWallFeeds=nil;
                        if (methodName==wall_like||methodName==addcomments||methodName==wall_unlike) {
                            
                            [self feed];
                        }
                        arrWallFeeds=[response valueForKey:@"data"];
                        
                        if (methodName==wall_like||methodName==addcomments||methodName==wall_unlike){
                            liked_call=YES;
                            cellindex=cell_index;
                        }else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (liked_call==YES) {
                                    
                                    selectedRow=-1;//                                    [feed_cell.comments_btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                                    //                                    feed_cell.comments_btn .selected=false;
                                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellindex inSection:0];
                                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                                    [self.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                                }else{
                                    liked_call=NO;
                                    [_tableViewOutlet reloadData];
                                }
                            });
                        }
                        
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        });
                    }
                    
                }
                
            }
            
        } errorBlock:^(id error)
         {
//             NSLog(@"error is %@",error);
         }];
    }
    
}
-(void)send:(UIButton*)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    FeedTableViewCell *cell = (FeedTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    comments= cell.commentsView.txtComment.text;
    
    NSDictionary *object_dict=[arrWallFeeds objectAtIndex:sender.tag];
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"ref_type":@"post",
                           @"ref_id":object_dict[@"id"],
                           @"body" :comments,
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           };
//    NSLog(@"hello is%@",dict);
    
    [self sendRequest:dict andmethodName:addcomments andcell_index:sender.tag ];
    
    // [self sendComments:dict andmethodName:addcomments andCell:cell];
}
-(void)commentsView:(NSUInteger)userid and:(id)cell andcellType:(NSString*)celltype{
    
    
    
    
    
    NSDictionary *object_dict=[arrWallFeeds objectAtIndex:userid];
    NSDictionary *subject_info=object_dict[@"get_subject_user_details"];
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"ref_id":object_dict[@"id"],
                           @"ref_type":@"post",
                           };
//    NSLog(@"dict is%@",dict);
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:commentLists withParameters:dict withHud:YES success:^(id response){
            NSMutableArray *messageArray;
//            NSLog(@"response is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        NSMutableArray *commentsArray=[response valueForKey:@"data"];
                        all_comments=[[NSMutableArray alloc]init];
                        
                        all_comments=commentsArray;
                        
                        if ([celltype isEqualToString:@"Feedpost"]) {
                            commment_info=@{
                                            @"post_body" :feed_cell.lbl_post.text,
                                            @"post_picture" :feed_cell.imageview_person.image,
                                            @"post_personName" :feed_cell.lbl_personName.text,
                                            @"post_time" :feed_cell.lbl_date.text,
                                            @"ref_id" :object_dict[@"id"],
                                            @"ref_type" :@"post",
                                            };
                            
                        }else{
                            commment_info=@{
                                            @"post_body" :feedImage_cell.lbl_post.text,
                                            @"post_picture" :feedImage_cell.imageview_person.image,
                                            @"post_personName" :feedImage_cell.lbl_personName.text,
                                            @"post_time" :feedImage_cell.lbl_date.text,
                                            @"ref_id" :object_dict[@"id"],
                                            @"ref_type" :@"post",
                                            @"post_Image":feedImage_cell.imageview_post.image,
                                            };
                        }
                        
                        
                        
                        
                        
                        //                            [cell.commentsView.btnSeeMore setBackgroundColor:[UIColor colorWithRed:0/255 green:154/255 blue:217/255 alpha:1.0]];
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // [_tableViewOutlet reloadData];
                            [self.delegate1 btn_pressed:@"seemore" andtotal_comments:all_comments
                                                andpost:commment_info];
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        });
                    }
                    
                }
                
            }
            
        } errorBlock:^(id error)
         {
//             NSLog(@"error is %@",error);
         }];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
//    NSLog(@"textFieldDidBeginEditing");
    //    [feed_cell.commentsView.btnSend  setImage:[UIImage imageNamed:@"send_selected"] forState:UIControlStateNormal];
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect absoluteframe = [textField convertRect:textField.frame toView:[UIApplication sharedApplication].keyWindow];
//    NSLog(@"y is%f",screenRect.size.height);
    int y_textfield=screenRect.size.height-absoluteframe.origin.y;
    if (y_textfield<0) {
        
        [_tableViewOutlet setContentOffset:CGPointMake(0, _tableViewOutlet.contentOffset.y+abs(y_textfield)+abs(y_textfield) )];
        keyboard=YES;
        return;
    }else{
        if (y_textfield<100) {
            [_tableViewOutlet setContentOffset:CGPointMake(0, _tableViewOutlet.contentOffset.y+y_textfield)];
            keyboard=YES;
            return;
        }
    }
    keyboard=NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
//    NSLog(@"textFieldDidEndEditing");
    //    [feed_cell.commentsView.btnSend setImage:[UIImage imageNamed:@"send_unselected"] forState:UIControlStateNormal];
    if (keyboard==YES) {
        [_tableViewOutlet setContentOffset:CGPointMake(0, _tableViewOutlet.contentOffset.y-120)];
        return;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
//    NSLog(@"textFieldShouldReturn:");
    [textField resignFirstResponder];
    return YES;
}


-(void)initial_FeedserviceCall{
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"object_type":@"user",
                           @"object_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"filter_type":@"4",
                           
                           };
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:allwalllist withParameters:dict withHud:YES success:^(id response){
            NSMutableArray *messageArray;
//            NSLog(@"response is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        arrWallFeeds=nil;
                        arrWallFeeds=[response valueForKey:@"data"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_tableViewOutlet reloadData];
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        });
                    }
                    
                }
                
            }
            
        } errorBlock:^(id error)
         {
//             NSLog(@"error is %@",error);
         }];
    }
    
    
}
//-(void)sendComments:(NSDictionary*)dict andmethodName:(NSString*)methodName andCell:(FeedTableViewCell*)cell{
//    NSLog(@"cell tag is%ld",(long)cell.tag);
//    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
//    if (!appDelegate.isReachable) {
//        UIAlertView *message = [[UIAlertView alloc] initWithTitle:appTitle message:NoNetwork  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [message show];
//    }else{
//        NSLog(@"dict is%@",dict);
//        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
//            NSMutableArray *messageArray;
//            NSLog(@"response is%@",response);
//            if ([response isKindOfClass:[NSDictionary class]]){
//                if (![response isKindOfClass:[NSNull class]]) {
//
//                    if([response[@"status"]intValue ] ==1){
//                        arrWallFeeds=nil;
//                        arrWallFeeds=[response valueForKey:@"data"];
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            selectedRow=-1;
//                            [cell.comments_btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//                            cell.comments_btn .selected=false;
//                            sendComments_method=addcomments;
//                            //                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cell.tag inSection:0];
//                            //                            NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
//                            //                            [self.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
//                            //   [_tableViewOutlet reloadData];
//                            [self feed];
//                        });
//                    }else{
//                        messageArray=[response valueForKey:@"message"];
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
//                        });
//                    }
//
//                }
//
//            }
//
//        } errorBlock:^(id error)
//         {
//             NSLog(@"error is %@",error);
//         }];
//    }
//
//}
-(void)sendrequest :(NSString*)methodName{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : @"1",
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"sendrequest" withParameters:dict withHud:YES success:^(id response){
//        NSLog(@"respons is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                NSMutableArray *messageArray;
                if([response[@"status"]intValue ] ==1){
                    messageArray=[response valueForKey:@"message"];
                    [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    
                }else{
                    
                    messageArray=[response valueForKey:@"message"];
                    [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    
                }
                
            }
        }
        
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
}
@end
