//
//  CommentTableViewCell.h
//  wireFrameSplash
//
//  Created by webwerks on 5/4/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel *lblName;
@property(nonatomic,weak)IBOutlet UIImageView *imgProfile;
@property(nonatomic,weak)IBOutlet UILabel *lblComment;

@end