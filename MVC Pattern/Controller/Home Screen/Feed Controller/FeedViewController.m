//
//  HomeViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "FeedViewController.h"
#import "FeedTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CustomCommentView.h"
#import "StatusViewController.h"
#import "FeedIMageTableViewCell.h"
#import "WallView.h"
#import "SeeMoreCommentsViewController.h"

#import "FriendsProfileViewController.h"
#import "MWPhotoBrowser.h"
#import "Profile_editViewController.h"
#import <GoogleMobileAds/GADBannerView.h>
#import "AppDelegate.h"
#import "DetailMygroupViewController1.h"
#import "KLCPopup.h"
#import "LoginViewController.h"
#import "popRatefeedViewController.h"
#import "Profile_editViewController.h"
#import "InfluencerProfileController.h"
#import "newUpdateVersionController.h"
#import "InfluencerClassViewController.h"

//#define SKY_BLUE_COLOR [UIColor colorWithRed:0.1137 green:0.4824 blue:0.8065 alpha:1.0]

@interface FeedViewController ()<UITabBarDelegate,UITextViewDelegate,UITextFieldDelegate,MWPhotoBrowserDelegate,GADNativeAdDelegate>
{
    GADBannerView  *bottomAbMob;
    NSMutableArray *photos;
    MWPhotoBrowser *browser;
    CGFloat heightofCell;
    NSInteger selectedRow;
    UIView *lineView ;
    NSMutableArray *arrFeeds;
    NSMutableArray *arrWallFeeds;
    NSString *comments;
    NSMutableArray *commentsarr;
    NSDictionary *get_total_commnet;
    BOOL comments_their;
    NSArray *arr;
    FeedTableViewCell *feed_cell;
    WallView *wallvie;
    GADAdLoader *adLoader;
    BOOL previous_show_group_val;
    KLCPopup *popviewRate;

}
@property (strong, nonatomic) FeedTableViewCell *customCell;
@end

@implementation FeedViewController
#define  kCellName @"FeedTableViewCell"
#define  kCellName1 @"FeedIMageTableViewCell"
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    self.view.backgroundColor=[UIColor whiteColor];
    [AppDelegate sharedAppDelegate].selectedTabBool=NO;
    NSDictionary *eventLocation = @{@"wallfor": @"myself"};
    if (wallvie) {
        [wallvie removeFromSuperview];
        wallvie=nil;
    }
    previous_show_group_val=NO;
    
    
    wallvie=[[WallView alloc]initWithframe:CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.width) andUserInfo:eventLocation];
    [wallvie webservice_call];
    wallvie.delegate1=self;
    wallvie.bottomRateViewConstraint.constant=-143;
    [wallvie updateConstraintsIfNeeded];
    [wallvie layoutIfNeeded];
    wallvie.tableViewOutlet.contentOffset = CGPointZero;
    [self.view addSubview:wallvie];
    
}
-(void)addsView:(NSNotification *) notification{
    // Bottom ad implemented here not working
    
    bottomAbMob = [[GADBannerView alloc]
                   init];
    bottomAbMob.adUnitID =kBannerAdUnitID;
    bottomAbMob.rootViewController =self ;
    CGFloat x_admob;
    x_admob =0;
    if ([UIApplication sharedApplication].keyWindow.frame.size.width>320) {
        x_admob =(([UIApplication sharedApplication].keyWindow.frame.size.width)-320)/2;
    }
    bottomAbMob.frame=CGRectMake(x_admob,[UIApplication sharedApplication].keyWindow.frame.size.height-50,320,50);
    
    GADRequest *request= [[GADRequest alloc] init];
    request.testDevices = @[ @"42f31902adddbb070ac0716b1d3602d5",kGADSimulatorID];
    [bottomAbMob loadRequest:request];
    [[UIApplication sharedApplication].keyWindow addSubview:bottomAbMob];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
//    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"influcencerProfileView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"friendProfileView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"userProfileView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"feedGroupsuggestion" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    
    
    [self.tableViewOutlet setContentOffset:CGPointMake(0.0f, 0.0f) animated:YES];
    [self.tableViewOutlet setContentInset:UIEdgeInsetsZero];
    wallvie.rateViewFeed.hidden=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(craetrateViewPop:)
                                                 name:@"ratepopView"
                                               object:nil];

    if ([wallvie.checkviewwillApear isEqualToString:@"noviewappear"] ) {
        [wallvie webservice_call];
        wallvie.checkviewwillApear=@"";
    }
    
    NSString *influencer=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"Follow"]];
    if ([influencer isEqualToString:@"yesDone"]) {
         [wallvie webservice_call];
        influencer=@"";
        [[NSUserDefaults standardUserDefaults] setObject:@" " forKey:@"Follow"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(feedreloadcellwallview:)
                                                 name:@"reloadcellwall"
                                               object:nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(feedupdateCommentfromProfile:)
                                                 name:@"commentsNoti"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterInForeground) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(feedgroupMethod:)
                                                 name:@"feedGroupsuggestion"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(friendProfileView:)
                                                 name:@"friendProfileView"
                                               object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(reloadNewwall:)
//                                                 name:@"reloadwallView"
//                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(influcencerProfileViewMethod:)
                                                 name:@"influcencerProfileView"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(suggestedDynamicImage:)
                                                 name:@"suggestedDynamicImage"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(suggestedDynamicImage:)
                                                 name:@"DynamicImage"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(suggestedDynamicUsername:)
                                                 name:@"suggestUsername"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fellowAction:)
                                                 name:@"fellowAction"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(forcetopenViewcontroller:)
                                                 name:@"forcetoUpdate"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(userProfileView:)
                                                   name:@"userProfileView"
                                                 object:nil];
    
//     if ([AppDelegate sharedAppDelegate].selectedTabBool==YES){
//        [AppDelegate sharedAppDelegate].selectedTabBool=NO;
//         [wallvie webservice_call];
//    }
//    else{
//        
//        
//        
//    }
    
    [wallvie checkinfluencerlist];
//    AppDelegate *getdeepData = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    if ([getdeepData.tabFeedInfluenceDeep isEqualToString:@"feedclick"]) {
//        [wallvie webservice_call];
//        getdeepData.tabFeedInfluenceDeep=@"";
//    }
}





-(void)forcetopenViewcontroller:(NSNotification *) notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        newUpdateVersionController *newupdateclass=[self.storyboard instantiateViewControllerWithIdentifier:@"newUpdateVersionController"];
        [self presentViewController:newupdateclass animated:YES completion:nil];
    });
    
}


-(void)feedreloadcellwallview:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    [wallvie  reloadcellwallview:userInfo];
}

-(void)feedupdateCommentfromProfile:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
        [wallvie updateCommentfromProfile:userInfo];
}

-(void)fellowAction:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wallvie FollowbtnMethod:userInfo];
    });
    
}

-(void)suggestedDynamicUsername:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wallvie suggeteduserProfile:userInfo];
    });
    
}


-(void)suggestedDynamicImage:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wallvie suggetedImageview:userInfo];
    });
    
}


-(void)reloadNewwall:(NSNotification *) notification{
    dispatch_async(dispatch_get_main_queue(), ^{
      [wallvie webservice_call];
    });
    
}

-(void)craetrateViewPop:(NSNotification *) notification{
   
    dispatch_async(dispatch_get_main_queue(), ^{
        popRatefeedViewController *login=[self.storyboard instantiateViewControllerWithIdentifier:@"popRatefeedViewController"];
        self.definesPresentationContext = YES;
        login.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        login.view.backgroundColor=[UIColor clearColor];
        [self presentViewController:login animated:YES completion:^{
            login.view.backgroundColor =[UIColor colorWithRed:(0.0/255.0f) green:(0.0/255.0f) blue:(0.0/255.0f) alpha:0.5];
            
        }];
    });
    
}


-(void)appEnterInForeground{
    if([AppDelegate sharedAppDelegate].showsuggestedGroup!=previous_show_group_val)
    {
        previous_show_group_val=[AppDelegate sharedAppDelegate].showsuggestedGroup;
        [wallvie webservice_call];
    }
}



-(void)userProfileView:(NSNotification *) notification{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        Profile_editViewController *friend=[self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
        [self.navigationController pushViewController:friend animated:YES];
    });
    
}


-(void)friendProfileView:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        FriendsProfileViewController *friend=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
        NSMutableDictionary *dictcontent=[userInfo valueForKey:@"allData"];
        friend.friendProfileDict=dictcontent;
        friend.postFriendProfileDict=dictcontent;
        [self.navigationController pushViewController:friend animated:YES];
     
    });
    
}




-(void)influcencerProfileUniversalLink:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        InfluencerClassViewController *InfluencerControllerView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerClassViewController"];
        [InfluencerControllerView viewFrndProfile:[userInfo objectForKey:@"friendID"]];
        [self.navigationController pushViewController:InfluencerControllerView animated:YES];
    });
    
}


-(void)influcencerProfileViewMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        InfluencerProfileController *InfluencerProfileControllerView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
        NSMutableDictionary *influencerDictData=[userInfo valueForKey:@"allData"];
        InfluencerProfileControllerView.influencerDict=influencerDictData;
        [self.navigationController pushViewController:InfluencerProfileControllerView animated:YES];
    });
    
}

-(void)feedgroupMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        DetailMygroupViewController1 *detailgroupFeed=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
        detailgroupFeed.navigationTitle=[userInfo valueForKey:@"groupname"];
        detailgroupFeed.idUser=[userInfo valueForKey:@"groupis"];
        [self.navigationController pushViewController:detailgroupFeed animated:YES];
    });
    
}
- (void) FunctionOne: (NSString*) dataOne andArray:(NSMutableArray*)arraycontent
{
    if([dataOne isEqualToString:@"open statusView" ]){
        StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
        statusScreen.status_para=@"1";
        statusScreen.isGroupPost = NO;
        statusScreen.dictofPost=[arraycontent objectAtIndex:0];
          [self.navigationController pushViewController:statusScreen animated:YES];
        
        
        //[self.navigationController presentViewController:statusScreen animated:YES completion:nil];
    }else if ([dataOne isEqualToString:@"open prayer" ]){
        StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
        statusScreen.status_para=@"2";
        statusScreen.isGroupPost = NO;
        statusScreen.dictofPost=[arraycontent objectAtIndex:0];
        
        [self.navigationController pushViewController:statusScreen animated:YES];

        //[self.navigationController presentViewController:statusScreen animated:YES completion:nil];
    }else if ([dataOne isEqualToString:@"open seemoreImage" ]){
        
        NSMutableArray *imagearr=[[NSMutableArray alloc]init];
        
      //  photos=[[NSMutableArray alloc]init];
        for (int i=0; i<arraycontent.count; i++) {
            NSString *str=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,[arraycontent objectAtIndex:i]];
            MWPhoto *photo=[MWPhoto photoWithURL:[NSURL URLWithString:str]];
//            photo.caption = @"Biblefaithfollow.SocialApp";
            [imagearr addObject:photo];
        }
      photos=[[[imagearr reverseObjectEnumerator] allObjects] mutableCopy];
        browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        BOOL displayActionButton = YES;
        BOOL displaySelectionButtons = NO;
        BOOL enableGrid = YES;
        BOOL startOnGrid = NO;
        [AppDelegate sharedAppDelegate].clickonImage=YES;
        browser.displayActionButton = displayActionButton;
        //  browser.displayNavArrows = displayNavArrows;
        browser.displaySelectionButtons = displaySelectionButtons;
        browser.alwaysShowControls = NO;
        browser.zoomPhotosToFill = YES;
        browser.enableGrid = enableGrid;
        browser.startOnGrid = startOnGrid;
        browser.enableSwipeToDismiss = NO;
        //browser.autoPlayOnAppear = autoPlayOnAppear;
        [browser setCurrentPhotoIndex:0];
        
        enableGrid = NO;
        
        [self.navigationController pushViewController:browser animated:YES];
    }
}





-(void) btn_pressed: (NSString*) buttonName andtotal_comments:(NSMutableArray*)totalComments andpost:(NSDictionary*)post andCellindex:(NSUInteger)cell_index{
    SeeMoreCommentsViewController *seemoreScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"SeeMoreCommentsViewController"];
    seemoreScreen.totalComments=totalComments;
    seemoreScreen.post_dict=post;
    seemoreScreen.cellindex=cell_index;
    [self.navigationController pushViewController:seemoreScreen animated:YES];
}
-(void)openViewProfile: (NSString*) dataOne andArray:(NSMutableDictionary*)dictcontent{
    
    if([dataOne isEqualToString:@"open friend" ]){
        
        NSString *idofUser=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
        int val=[idofUser intValue];
        int val2=[dictcontent[@"id"] intValue];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (val==val2) {
                // showed my profile
                [self.navigationController popToRootViewControllerAnimated:YES];
                Profile_editViewController *profileView=[self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
                CGRect    screenRect = [[UIScreen mainScreen] bounds];
                UIView *viewProfile=[[UIView alloc]initWithFrame:CGRectMake(0, 0,screenRect.size.width,60)];
                viewProfile.backgroundColor=UIColorFromRGB(0x0088CF);
                [[profileView view] addSubview:viewProfile];
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button addTarget:self
                           action:@selector(back)
                 forControlEvents:UIControlEventTouchUpInside];
                [button setImage:[UIImage imageNamed:@"leftArrow"] forState:UIControlStateNormal];
                button.frame = CGRectMake(5, 25,30, 30);
                [viewProfile addSubview:button];
                
                UILabel *lbl1 = [[UILabel alloc] init];
                [lbl1 setFrame:CGRectMake(screenRect.size.width/2-20,15,100,50)];
                lbl1.font = [UIFont fontWithName:@"Avenir-Medium" size:16];
                
                lbl1.backgroundColor=[UIColor clearColor];
                lbl1.textColor=[UIColor whiteColor];
                lbl1.text= @"Profile";
                [viewProfile addSubview:lbl1];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"checkprofile"];
                [[NSUserDefaults standardUserDefaults] synchronize];
//                [self presentViewController:profileView animated:YES completion:nil];
                [self.navigationController pushViewController:profileView animated:YES];

                
            }else{
                
                FriendsProfileViewController *friend=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                friend.friendProfileDict=dictcontent;
                friend.postFriendProfileDict=dictcontent;
//                [self presentViewController:friend animated:YES completion:nil];
                [self.navigationController pushViewController:friend animated:YES];
                
            }
            
        });
    }
    
}
-(void)back{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
    
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
//    NSLog(@"Did finish modal presentation");
    // [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}






//- (void) receiveTestNotification:(NSNotification *) notification{
//    NSDictionary   *userInfo = notification.userInfo;
//    NSNumber *cellindex = [userInfo valueForKey:@"cellindex"];
//    NSNumber *count_val = [userInfo valueForKey:@"count_val"];
//    NSUInteger cell_index=[cellindex intValue];
//    
//    int count_val1=[count_val intValue];
//    if (count_val1==0) {
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cell_index inSection:0];
//        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
//        
//        [wallvie.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
//        
//        
//        
//        
//    }else   if (count_val1==1) {
//        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
//        [dict setObject:count_val forKey:@"count"];
//        [[arrWallFeeds objectAtIndex:cell_index]setObject:dict forKey:@"get_total_commnets"];
//        
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cell_index inSection:0];
//        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
//        [wallvie.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
//        
//    }else{
//        [wallvie.arrWallFeeds objectAtIndex:cell_index][@"get_total_praised"][@"count"]=count_val;
//        // [wallvie.arrWallFeeds objectAtIndex:cell_index][@"get_total_praised"][@"count"]=count_val;
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cell_index inSection:0];
//        NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
//        [wallvie.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
//        
//    }
//    
//}
//

@end
