//
//  FeedTableViewCell.h
//  wireFrameSplash
//
//  Created by Vikas on 21/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCommentView.h"

@interface FeedTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lbl_posttype;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *personName_lbl;
@property (weak, nonatomic) IBOutlet UILabel *date_lbl;
@property (weak, nonatomic) IBOutlet UIImageView *person_imageview;

@property (weak, nonatomic) IBOutlet UITextView *about_txtView;

@property (weak, nonatomic) IBOutlet UILabel *praise_lbl;
@property (weak, nonatomic) IBOutlet UILabel *totalcomments_lbl;
@property (weak, nonatomic) IBOutlet UIButton *praised_btn;

@property (weak, nonatomic) IBOutlet UIButton *comments_btn;
@property (weak, nonatomic) IBOutlet UIButton *moreComments_btn;
@property (weak, nonatomic) IBOutlet UITextView *writeComments_txtview;
@property (weak, nonatomic) IBOutlet UIButton *send_btn;
@property (weak, nonatomic) IBOutlet UIView *comment_btnView;
@property (weak, nonatomic) IBOutlet UILabel *com_lbl;

@property (weak, nonatomic) IBOutlet CustomCommentView *commentsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConsatraint;

@property (strong, nonatomic) IBOutlet UIView *praisedandCommentsView;
@property (weak, nonatomic) IBOutlet UIView *friendsCommentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *Praised_heightof;
@property (weak, nonatomic) IBOutlet UIView *formalView;

@end
