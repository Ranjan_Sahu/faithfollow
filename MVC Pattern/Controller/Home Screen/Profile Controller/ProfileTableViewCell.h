//
//  ProfileTableViewCell.h
//  wireFrameSplash
//
//  Created by home on 4/11/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTableViewCell : UITableViewCell
@property(weak,nonatomic)IBOutlet UILabel *textNameLabel;
@property(weak,nonatomic)IBOutlet UILabel *textNameValueLabel;


//
@property(weak,nonatomic)IBOutlet UILabel *nameLabel;
@property(weak,nonatomic)IBOutlet UILabel *emailLabel;
@property(weak,nonatomic)IBOutlet UILabel *mobileLabel;
@property(weak,nonatomic)IBOutlet UILabel *birthLabel;
@property(weak,nonatomic)IBOutlet UILabel *genderLabel;
//
@property(weak,nonatomic)IBOutlet UIImageView *imageViewNotification;
@property(weak,nonatomic)IBOutlet UILabel  *postedLabel;
@property(weak,nonatomic)IBOutlet UILabel  *hourLabel;
@property(weak,nonatomic)IBOutlet UIButton  *viewBTN;
@property(weak,nonatomic)IBOutlet UIButton *profileFriendViewList;
@property(weak,nonatomic)IBOutlet UIImageView *influencerImage;

@end
