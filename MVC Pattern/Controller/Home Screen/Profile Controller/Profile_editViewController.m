//
//  Profile_editViewController.m
//  wireFrameSplash
//
//  Created by home on 4/11/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "Profile_editViewController.h"
#import "ProfileTableViewCell.h"
#import "DeviceConstant.h"
#import "NSString+StringValidation.h"
#import "Constant1.h"
#import "VSDropdown.h"
#import "AdditionInfoViewController.h"
#import "DeviceConstant.h"
#import "AppDelegate.h"
#import "WebServiceHelper.h"
#import "CustomTextfield.h"
#import "UIImageView+WebCache.h"
#import "HomeViewController.h"
#import "FriendListController.h"
#import "HomeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "FeedViewController.h"
#import "FeedTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CustomCommentView.h"
#import "StatusViewController.h"
#import "FeedIMageTableViewCell.h"
#import "WallView.h"
#import "SeeMoreCommentsViewController.h"
#import "FriendsProfileViewController.h"
#import "MWPhotoBrowser.h"
#import "AdditionalDisplayView.h"
#import "NeosPopup.h"
#import "SignupViewController.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "StatusViewController.h"
#import "InfluencerProfileController.h"
#import "Mixpanel.h"

@interface Profile_editViewController ()<VSDropdownDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextFieldDelegate,UIAlertViewDelegate,MWPhotoBrowserDelegate,UIActionSheetDelegate,UITextFieldDelegate>
{
    NSMutableArray *photos;
    MWPhotoBrowser *browser;
    KLCPopup *popUp,*popUpView1;
    VSDropdown *_dropdown;
    NSMutableArray *dateArray,*years,*month,*meArrayEdit,*friendArrayEdit,*profileTestArray,*denominationArray,*arr,*denominationwebArray,*whoViewprofileArray,*tempArray,*whoViewprofileArray1;
    NSMutableArray *arrFeeds,*_quoteArray;
    NSMutableDictionary *usersprofileDict;
    NSString *maleFemaleStr1,*strDob,*mebtnText;
    BOOL mobileBool,checkBool;
    NSString *checkStr;
    WallView *wallvie;
    NSString *postOptionstr,*profileviewOption,*allProfilePostView;
    CGRect screenRect;
    NSString *dobStr;
    NeosPopup *nepopup;
    UIAlertView *message;
    NSMutableDictionary *dataDict;
    NSString *documentsDirectoryPath,*filePath,*imagefilePath,*imagenilStr;
    BOOL isNotNeedToRefresh,yearPicked;
  NSArray *paths;
    FBSDKAccessToken *accToken;
}
@end
@implementation Profile_editViewController
@synthesize scrollView,webResponseData;
- (void)viewDidLoad {
    [super viewDidLoad];
    checkStr=@"1";
    isNotNeedToRefresh=false;
    [self.demominationTitleBTN setTitleColor:UIColorFromRGB(0xACACAC) forState:UIControlStateNormal];
    _editSettingView.hidden=YES;
    _profileView.hidden=YES;
    _ProfileMain.hidden=NO;
    _popUpView.hidden=YES;
    _deactivatePopView.hidden=YES;
    _editSybolSaveContinue.hidden=YES;
    _additionalView.hidden=NO;
    _wallView.hidden=YES;
    _bckVIew2.hidden=YES;
    _bckview3.hidden=YES;
    _bckView1.hidden=NO;
    
    denominationArray=[[NSMutableArray alloc]init];
    _profileArray1=[[NSMutableArray alloc]init];
    years = [[NSMutableArray alloc] init];

    arr = [[NSMutableArray alloc] init];
    _textArray1=[[NSMutableArray alloc]initWithObjects:@"Name",@"Mobile",@"Birthdate",@"Gender",@"Denomination",nil];
    profileTestArray=[[NSMutableArray alloc]initWithObjects:@"John",@"John@gmail.COM",@"8005271071",@"12/10/1985",@"Male",@"Catholic",nil];
    denominationwebArray=[[NSMutableArray alloc]init];
    whoViewprofileArray=[[NSMutableArray alloc]init];
    whoViewprofileArray1=[[NSMutableArray alloc]init];
    
    if (IS_IPHONE5||IS_IPHONE6||IS_IPHONE6PLUS||IS_IPHONE4) {
        _savebuttonBottomC.constant=-50;
    }
    _wallViewHight.constant=550;

    
    if (IS_IPHONE6) {
        _tableHeight.constant=351;
        _ViewHeight.constant=321;
        _EditView.constant=321;
        _wallViewHight.constant=700;
        _profileViewContritns.constant=321;
        
    }
    if (IS_IPHONE6PLUS) {
        _tableHeight.constant=450;
        _ViewHeight.constant=370;
        _EditView.constant=370;
        _wallViewHight.constant=550;
        _profileViewContritns.constant=370;
    }

    self.popUpView.layer.cornerRadius = 8;
    self.deactivatePopView.layer.cornerRadius = 8;
    _btnprofile_imageview.image= [UIImage imageNamed:@"profile_selected-1"];
//    _btnprofile_label.textColor=UIColorFromRGB(0x009AD9);
    _profileBTN.selected=YES;
    //Dropdown button
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    dateArray=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",@"31",nil];
    month=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",nil];
    meArrayEdit=[[NSMutableArray alloc]initWithObjects:@"hello1",@"hello2",@"hello3", nil];
    friendArrayEdit=[[NSMutableArray alloc]initWithObjects:@"hello1",@"hello2",@"hello3", nil];
    _maleBTN.selected=YES;
//    [[_meView layer] setBorderWidth:1.0f];
//    [[_meView layer] setBorderColor:[UIColor grayColor].CGColor];
//    [[_friendView layer] setBorderWidth:1.0f];
//    [[_friendView layer] setBorderColor:[UIColor grayColor].CGColor];
//    [[_monthView layer] setBorderWidth:1.0f];
//    [[_monthView layer] setBorderColor:[UIColor grayColor].CGColor];
//    [[_daysView layer] setBorderWidth:1.0f];
//    [[_daysView layer] setBorderColor:[UIColor grayColor].CGColor];
//    [[_yearView layer] setBorderWidth:1.0f];
//    [[_yearView layer] setBorderColor:[UIColor grayColor].CGColor];
    
    webResponseData=[[NSMutableDictionary alloc]init];
    usersprofileDict=[[NSMutableDictionary alloc]init];
    [self webresponse];
    [self yearlistProfile];
    
    _profile_Picture.userInteractionEnabled=YES;
    _profile_Picture.multipleTouchEnabled=YES;
    
    self.nameTXT.delegate = self;
    self.nameTXT.tag = 1;
    self.emailTXT.delegate = self;
    self.emailTXT.tag = 2;
    self.mobileTXT.delegate = self;
    self.mobileTXT.tag = 3;
    
//    [[self.nameTXT layer] setBorderWidth:0.8f];
//    [[self.nameTXT layer] setBorderColor:[UIColor grayColor].CGColor];
//    [[self.emailTXT  layer] setBorderWidth:0.8f];
//    [[self.emailTXT  layer] setBorderColor:[UIColor grayColor].CGColor];
//    [[self.mobileTXT  layer] setBorderWidth:0.8f];
//    [[self.mobileTXT  layer] setBorderColor:[UIColor grayColor].CGColor];
//    [[self.editDenominationView  layer] setBorderWidth:0.8f];
//    [[self.editDenominationView   layer] setBorderColor:[UIColor grayColor].CGColor];
    
    // place holder color change
    [self.nameTXT setValue:[UIColor blackColor]
                forKeyPath:@"_placeholderLabel.textColor"];
    [self.emailTXT setValue:[UIColor blackColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    [self.mobileTXT setValue:[UIColor blackColor]
                  forKeyPath:@"_placeholderLabel.textColor"];
    [self.denominationTXT setValue:[UIColor blackColor]
                        forKeyPath:@"_placeholderLabel.textColor"];
    
    self.enterOldPawwordTXT.delegate = self;
    self.enterOldPawwordTXT.tag = 6;
    self.enterNewPasswordTXT.delegate = self;
    self.enterNewPasswordTXT.tag = 7;
    self.rePasswordTXT .delegate = self;
    self.rePasswordTXT .tag = 8;
    
    [self.rePasswordTXT addTarget:self
                           action:@selector(textFieldFinished:)
                 forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [self.enterOldPawwordTXT setValue:[UIColor blackColor]
                           forKeyPath:@"_placeholderLabel.textColor"];
    [self.enterNewPasswordTXT setValue:[UIColor blackColor]
                            forKeyPath:@"_placeholderLabel.textColor"];
    [self.rePasswordTXT setValue:[UIColor blackColor]
                      forKeyPath:@"_placeholderLabel.textColor"];
    
    [[self.enterOldPawwordTXT layer] setBorderWidth:0.8f];
    [[self.enterOldPawwordTXT layer] setBorderColor:[UIColor grayColor].CGColor];
    [[self.enterNewPasswordTXT  layer] setBorderWidth:0.8f];
    [[self.enterNewPasswordTXT  layer] setBorderColor:[UIColor grayColor].CGColor];
    [[self.rePasswordTXT  layer] setBorderWidth:0.8f];
    [[self.rePasswordTXT  layer] setBorderColor:[UIColor grayColor].CGColor];
    
    self.nameTXT.delegate=self;
    self.emailTXT.delegate=self;
    self.mobileTXT.delegate=self;
    self.denominationTXT.delegate=self;
    self.enterOldPawwordTXT.delegate=self;
    self.enterNewPasswordTXT.delegate=self;
    self.rePasswordTXT.delegate=self;
    UITapGestureRecognizer   *singleTapGestureRecognizer =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
    [_profile_Picture addGestureRecognizer:singleTapGestureRecognizer];
    
    [self  editDenominationDidload];
    [self whoProfileView];
    _btnEdit_imageview.image= [UIImage imageNamed:@"edit_unselected"];
    _btnprofile_imageview.image= [UIImage imageNamed:@"profile_selected-1"];
    _btnViewWall_imageview.image= [UIImage imageNamed:@"view_1x"];
//    _btnEdit_label.textColor=UIColorFromRGB(0x676767);
//    _btnprofile_label.textColor=UIColorFromRGB(0x009AD9);
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage* image = [self readFileFromPath];
                _profile_Picture.image=image;
                self.profile_Name.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"fullname"];
            });
    }
    
    self.navigationController.navigationBar.topItem.title =@"";
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"Profile";
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    UIImage *listImage = [UIImage imageNamed:@"leftArrow"];
    UIButton *listButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [listButton setFrame:CGRectMake(0, 0, 30, 30)];
    [listButton setImage:listImage forState:UIControlStateNormal];
    //[listButton setBackgroundImage:listImage forState:UIControlStateNormal];
    [listButton  addTarget:self action:@selector(cancelBTN) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *infoButtonImage = [[UIBarButtonItem alloc]initWithCustomView:listButton ];
    self.navigationItem.leftBarButtonItem =infoButtonImage;
  
}

-(void)cancelBTN{
        [AppDelegate sharedAppDelegate].wallviewSubjectclick=YES;
    [self.navigationController popViewControllerAnimated:YES];
}


//Method to Retrieve image file from local

#pragma mark RetrieveImageMethod

-(UIImage *)readFileFromPath{
    NSString *stringPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    stringPath  = [stringPath stringByAppendingPathComponent:@"profile_pic.png"];
    return [UIImage imageWithContentsOfFile:stringPath];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
//    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
}



#pragma Mark InfluencersMethod

-(void)influgroupMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        DetailMygroupViewController1 *detailgroupFeed=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
        detailgroupFeed.navigationTitle=[userInfo valueForKey:@"groupname"];
        detailgroupFeed.idUser=[userInfo valueForKey:@"groupis"];
        [self.navigationController pushViewController:detailgroupFeed animated:YES];
    });
    
}

#pragma mark FriendProfileMethod
-(void)friendProfileView:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        FriendsProfileViewController *friend=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
        NSMutableDictionary *dictcontent=[userInfo valueForKey:@"allData"];
        friend.friendProfileDict=dictcontent;
        friend.postFriendProfileDict=dictcontent;
        [self.navigationController pushViewController:friend animated:YES];
        
    });
    
}

#pragma InflucencerProfileMethod

-(void)influcencerProfileViewMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        InfluencerProfileController *InfluencerProfileControllerView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
        NSMutableDictionary *influencerDictData=[userInfo valueForKey:@"allData"];
        InfluencerProfileControllerView.influencerDict=influencerDictData;
        [self.navigationController pushViewController:InfluencerProfileControllerView animated:YES];
        
    });
    
}





-(void)updateComment :(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
        [wallvie updateCommentfromProfile:userInfo];
}

-(void)reloadcellwallviewProfile :(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    [wallvie reloadcellwallview:userInfo];
}





-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(influgroupMethod:)
                                                 name:@"feedGroupsuggestion"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(friendProfileView:)
                                                 name:@"friendProfileView"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(influcencerProfileViewMethod:)
                                                 name:@"influcencerProfileView"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateComment:)
                                                 name:@"commentsNoti"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadcellwallviewProfile:)
                                                 name:@"reloadcellwall"
                                               object:nil];
    
    
//
    if (!isNotNeedToRefresh)
    {
    [self webpictureResponsegettingImage];
    [self webresponse];
    [self attributelink];
        
    }
    else
    {
     isNotNeedToRefresh=false;
    }
    _logBTN.hidden=NO;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    //self.navigationController.view.backgroundColor = [UIColor colorWithRed:0.0784 green:0.5294 blue:0.8784 alpha:1.0];
    //self.navigationController.view.backgroundColor = UIColorFromRGB(0x00BAF5);
    self.navigationController.view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    
}

#pragma Mark Term&ConditionLinkMethod

-(void)attributelink {
    _linkattributetxt.userInteractionEnabled = YES;
    NSString *text=@"Terms and Conditions. Privacy Policy";
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:text];
    NSRange TermsRange = [text rangeOfString:@"Terms and Conditions"];
    NSRange PrivacyRange = [text rangeOfString:@"Privacy Policy"];
    [str addAttribute: NSLinkAttributeName value:urlTermCondiatiom range: TermsRange];
    [str addAttribute: NSLinkAttributeName value:urlPrivacy range: PrivacyRange];
    [_linkattributetxt  setTintColor:UIColorFromRGB(0x00ADEB)];
    [_linkattributetxt setAttributedText:str ];
    [_linkattributetxt setTextColor:[UIColor grayColor]];
    [_linkattributetxt setTextAlignment:NSTextAlignmentCenter];
    [_linkattributetxt setFont:[UIFont systemFontOfSize:14]];
}


- (void)textFieldDidEndEditing:(UITextField *)textField{
//    NSString *strMobile=_mobileTXT.text;
//    if (textField==_mobileTXT) {
//        BOOL isMobile = [Constant1 myMobileNumberValidate:strMobile];
//        if (!isMobile) {
//            [self.view makeToast:MobileNumber duration:1.0 position:CSToastPositionCenter];
//        }
//        
//    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyNext) {
        UIView *next = [[textField superview] viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    } else if (textField.returnKeyType==UIReturnKeyDone) {
        [textField resignFirstResponder];
    }
    return YES;
}


#pragma textfield delagte
-(void)textFieldDidBeginEditing:(UITextField *)textField{
//    NSString *strMobile=_mobileTXT.text;
//    if (textField==_mobileTXT) {
//        if ([strMobile length] == 0) {
//            [self.view makeToast:MobileNumberEmpty duration:1.0 position:CSToastPositionCenter];
//            
//        }
//        BOOL isMobile = [Constant1 myMobileNumberValidate:strMobile];
//        if (!isMobile) {
//            [self.view makeToast:MobileNumber duration:1.0 position:CSToastPositionCenter];
//        }
//        
//    }
    
    textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
}



- (void)handleSingleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer{
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Social Media" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Take your Picture",
                            @"Upload picture",nil];
    [popup showInView:self.view];
    }
    
}


- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==0) {
        [self takeCameraMethod];
    }
    if (buttonIndex==1) {
        [self uploadPictureMethod];
    }
     }

#pragma mark CameraOpenMethod

-(void)takeCameraMethod{
    isNotNeedToRefresh=true;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark GalleryMethod

-(void)uploadPictureMethod{
     isNotNeedToRefresh=true;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma profile_pictureMethod

-(void)webpictureResponse {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        if (message) {
            
        }else{
            message=nil;
            [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
            return;
        }
    }
    else{
      chosenImage=[Constant1 compressImage:chosenImage];
        NSString *imagestrweb=[self base64String:chosenImage];
        NSDictionary *dict = @{
                    @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                    @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                    @"image":imagestrweb,
                             };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"update/profilepic" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]])  {
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.profile_Picture.image = chosenImage;
                            [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(chosenImage) forKey:@"image"];
                            NSMutableArray *messageArray = nil;
                            messageArray=[response valueForKey:@"message"];
                            if ([messageArray count]>0) {
                              [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            }
                            
                        });
                    }
                }
                else{
                    NSMutableArray *messageArray =nil;
                    messageArray=[response valueForKey:@"message"];
                    if ([messageArray count]) {
                       [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                    }
                    
                    
                }
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
        
    }
    
}



//profile_picture

#pragma  webpictureMethod

-(void)webpictureResponsegettingImage {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        if (message) {
            
        }else{
            message=nil;
            [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
            return;        }
    }
    else{
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:getimage withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]])  {
                    if([response[@"status"]intValue ] ==1){
                        NSString *imageUrl= [response valueForKey:@"image"];
                        NSURL *imageURL = [NSURL URLWithString:imageUrl];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.profile_Picture sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                            [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(self.profile_Picture.image) forKey:@"image"];
                            
                        });
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self.profile_Picture sd_setImageWithURL:nil placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        });
                    
                }
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
    }
}

#pragma imageFromColorMethod
- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
        {
        color=[UIColor whiteColor];
        }
        CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
CGContextFillRect(context, rect);
UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image;
}
                                       
                                     
- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

                                       

#pragma mark - imagePickerController Delegate methods.

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self webpictureResponse];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}



#pragma mark WebresponseMethod

-(void)webresponse{
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
         if (message) {
         
         }else{
            message=nil;
             [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
             return;
        }
    }
    else{
        NSString *userid=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
        if ([userid isEqualToString:@""] || [userid isEqualToString:@"(null)"]) {
            userid=@"";
        }
        else{
            userid=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
        }
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"id" :userid,
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"profile" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        webResponseData=[response valueForKey:@"data"];
                        [usersprofileDict addEntriesFromDictionary:[webResponseData valueForKey:@"usersprofile"]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSMutableDictionary *userFrofile=[webResponseData valueForKey:@"usersprofile"];
                            dobStr=[userFrofile valueForKey:@"date_of_birth"];
                            postOptionstr=[userFrofile valueForKey:@"post_option"];
                            profileviewOption=[NSString stringWithFormat:@"%@",[userFrofile valueForKey:@"profileview_option"]];
                            NSString *fullName = [NSString stringWithFormat: @"%@  %@",[webResponseData valueForKey:@"first_name"],[webResponseData valueForKey:@"last_name"]];
                            self.profile_Name.text=fullName;
                            [[NSUserDefaults standardUserDefaults]setObject:fullName forKey:@"fullname"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            NSString *imagestrUrl=[userFrofile valueForKey:@"profile_picture"];
                            if ([imagestrUrl isEqualToString:@""]) {
                                
                            }
                            else{
                                NSString *stringPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,   NSUserDomainMask, YES)objectAtIndex:0];
                                NSError *error = nil;
                                if (![[NSFileManager defaultManager] fileExistsAtPath:stringPath])
                                    [[NSFileManager defaultManager] createDirectoryAtPath:stringPath  withIntermediateDirectories:NO attributes:nil error:&error];
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    NSString *str = imagestrUrl;
                                    str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                    NSString *imageName = [stringPath stringByAppendingString:@"/profile_pic.png"];
                                    NSData *data = [NSData dataWithContentsOfURL:[NSURL  URLWithString:str]];
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [data writeToFile:imageName atomically:YES];
                                        
                                    });
                                    
                                });

                            }
//                            [NSKeyedArchiver archiveRootObject:webResponseData toFile:filePath];
                            NSDictionary *dict;
                            for (int i=0; i<[tempArray count]; i++) {
                                dict=[tempArray objectAtIndex:i];
                                NSString *postId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
                                if ([postId isEqualToString:postOptionstr]) {
                                    [_myFriends setTitle:[dict valueForKey:@"title"] forState:UIControlStateNormal];
                                    [[NSUserDefaults standardUserDefaults] setObject:[dict valueForKey:@"title"] forKey:@"postOptionstrValue"];
                                }
                            }
                            
                            for (int i=0; i<[tempArray count]; i++) {
                                dict=[tempArray objectAtIndex:i];
                                NSString *postId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]];
                                if ([postId  isEqualToString:profileviewOption ]) {
                                    [_meBTN setTitle:[dict valueForKey:@"title"] forState:UIControlStateNormal];
                                    [[NSUserDefaults standardUserDefaults] setObject:[dict valueForKey:@"title"] forKey:@"profileviewOptionValue"];
                                }
                            }
                            [self.tableView reloadData];
                            
                        });
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableDictionary *emailDict = nil;
                        NSMutableArray *email =nil;
                        emailDict=[response valueForKey:@"errors"];
                        email=[emailDict valueForKey:@"email"];
                        if ([email count]>0) {
                           [self.view makeToast:[email objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
    }
    
}

-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE4||IS_IPHONE5) {
        scrollView.contentSize=CGSizeMake(scrollView.frame.size.width, 550);
    }
    if (IS_IPHONE6||IS_IPHONE6PLUS) {
        scrollView.contentSize=CGSizeMake(scrollView.frame.size.width, 600);

    }
    
}

#pragma Tableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([[webResponseData allKeys]count]>0)
    {
        return 5;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellidprofile"];
    NSString *fullName = [NSString stringWithFormat: @"%@  %@",[webResponseData valueForKey:@"first_name"],[webResponseData valueForKey:@"last_name"]];
    cell.textNameValueLabel.numberOfLines = 1;
    cell.textNameValueLabel.minimumScaleFactor = 0.5;
    cell.textNameValueLabel.adjustsFontSizeToFitWidth = YES;
    NSMutableDictionary *usersprofileDict1=webResponseData[@"usersprofile"];
    if (indexPath.row==0) {
        cell.textNameValueLabel.text=fullName;
    }
    if (indexPath.row==1) {
        NSString *phone=[NSString stringWithFormat:@"%@",[usersprofileDict1 valueForKey:@"phone"]];
        if (phone==nil || [phone isEqualToString:@""]) {
            cell.textNameValueLabel.text=@"-";
        }
        else
        {
            cell.textNameValueLabel.text=[NSString stringWithFormat:@"%@",[usersprofileDict1 valueForKey:@"phone"]];
        }
            //[dataDict setObject:phone forKey:@"phone"];
        
    }
    if (indexPath.row==2) {
        NSString *dob=nil;
        dob=[usersprofileDict1 valueForKey:@"date_of_birth"];
        if (dob==nil) {
            dob=@"";
        }
        else{
            cell.textNameValueLabel.text=[usersprofileDict1 valueForKey:@"date_of_birth"];
        }
 //   [dataDict setObject:dob forKey:@"dob"];
           }
    if (indexPath.row==3) {
        NSString *finderMale=[NSString stringWithFormat:@"%@",[usersprofileDict1 valueForKey:@"gender"]];
        if ([finderMale intValue]==0) {
            finderMale=@"Female";
        }
        if ([finderMale intValue]==1) {
             finderMale=@"Male";
        }
        if ([finderMale intValue]==2) {
            finderMale=@"-";
        }
       
        cell.textNameValueLabel.text=finderMale;
          //  [dataDict setObject:finderMale forKey:@"gender"];
          }
    if (indexPath.row==4) {
        NSMutableDictionary *denomination=[usersprofileDict1 valueForKey:@"get_denomination_details"];
        if ([denomination isKindOfClass:[NSMutableDictionary class]]) {
            NSString *denominationStr=[denomination valueForKey:@"name"];
            if ([denominationStr isEqualToString:@""] || denominationStr==nil) {
                cell.textNameValueLabel.text=@"-";
            }
            else{
                cell.textNameValueLabel.text=denominationStr;
            }
        }
        else{
            cell.textNameValueLabel.text=@"-";
        }
    }
    cell.textNameLabel.text=[_textArray1 objectAtIndex:indexPath.row];
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  44.0f ;
    
}
- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark ProfileBtnMethod

- (IBAction)profileBTN:(id)sender {
    AppDelegate *appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        _bottomViewheight.constant = 120;
        scrollView.scrollEnabled=YES;
        _additionalviewheight.constant=93;
        _scrollViewhieght.constant=458;
    [self webresponse];
    _bckview3.hidden=YES;
    _bckVIew2.hidden=YES;
    _bckView1.hidden=NO;
//    _editView2.backgroundColor=UIColorFromRGB(0xF6F6F6);
//    _viewwall1.backgroundColor=UIColorFromRGB(0xF6F6F6);
//    _profileView1.backgroundColor=UIColorFromRGB(0xFFFFFF);
    _editSymbolBTN.hidden=NO;
        _editIcon.hidden = NO;
    _editSettingView.hidden=YES;
    _profileView.hidden=YES;
    _ProfileMain.hidden=NO;
    _editSettingBTN.selected=NO;
    _viewWallBTN.selected=NO;
    _profileBTN.selected=YES;
    _additionalView.hidden=NO;
    //    _bottmSaveContiNueBTN.hidden=NO;
    _editSybolSaveContinue.hidden=YES;
    _btnEdit_imageview.image= [UIImage imageNamed:@"edit_unselected"];
    _btnprofile_imageview.image= [UIImage imageNamed:@"profile_selected-1"];
    _btnViewWall_imageview.image= [UIImage imageNamed:@"view_1x"];
//    _btnEdit_label.textColor=UIColorFromRGB(0x676767);
//    _btnprofile_label.textColor=UIColorFromRGB(0x009AD9);
//    _btnViewWall_label.textColor=UIColorFromRGB(0x676767);
//    _bckView1.backgroundColor=UIColorFromRGB(0x009AD9);
    _wallView.hidden=YES;
    }
}

#pragma mark EditSettingMethod

- (IBAction)editsettingBTN:(id)sender {
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        _bottomViewheight.constant = 120;
     scrollView.scrollEnabled=YES;
    _scrollViewhieght.constant=458;
    _viewdeactivateheight.constant=50;
    [_myFriends setTitle:[[NSUserDefaults standardUserDefaults] valueForKey:@"postOptionstrValue"] forState:UIControlStateNormal];
    [_meBTN setTitle:[[NSUserDefaults standardUserDefaults] valueForKey:@"profileviewOptionValue"] forState:UIControlStateNormal];
    _bckView1.hidden=YES;
    _bckVIew2.hidden=YES;
    _bckview3.hidden=NO;
//    _editView2.backgroundColor=UIColorFromRGB(0xFFFFFF);
//    _viewwall1.backgroundColor=UIColorFromRGB(0xF6F6F6);
//    _profileView1.backgroundColor=UIColorFromRGB(0xF6F6F6);
    _editSymbolBTN.hidden=YES;
        _editIcon.hidden = YES;
    _profileView.hidden=YES;
    _ProfileMain.hidden=YES;
    _editSettingView.hidden=NO;
    _profileBTN.selected=NO;
    _viewWallBTN.selected=NO;
    _editSettingBTN.selected=YES;
    _ViewdeactiveView.hidden=NO;
    _additionalView.hidden=YES;
    _editSybolSaveContinue.hidden=YES;
    _btnEdit_imageview.image= [UIImage imageNamed:@"edit_selected"];
    _btnprofile_imageview.image= [UIImage imageNamed:@"profile_unselected-1"];
    _btnViewWall_imageview.image= [UIImage imageNamed:@"view_1x"];
//    _btnEdit_label.textColor=UIColorFromRGB(0x009AD9);
//    _btnViewWall_label.textColor=UIColorFromRGB(0x676767);
//    _btnprofile_label.textColor=UIColorFromRGB(0x676767);
//    _bckview3.backgroundColor=UIColorFromRGB(0x009AD9);
    _wallView.hidden=YES;
        
    }
    
}

#pragma mark ViewwallbtnMethod

- (IBAction)viewmyallBTN:(UIButton*)sender {
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    _bottomViewheight.constant = 0;
scrollView.scrollEnabled=NO;
//scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,0);
//    _additionalviewheight.constant=35;
//    _viewdeactivateheight.constant=35;
//    _editsybolsaveheight.constant=35;
    _bckView1.hidden=YES;
    _bckview3.hidden=YES;
    _bckVIew2.hidden=NO;
    _editSybolSaveContinue.hidden=YES;
//    _editView2.backgroundColor=UIColorFromRGB(0xF6F6F6);
//    _viewwall1.backgroundColor=UIColorFromRGB(0xFFFFFF);
//    _profileView1.backgroundColor=UIColorFromRGB(0xF6F6F6);
    _additionalView.hidden=YES;
    _ViewdeactiveView.hidden=YES;
    _editSymbolBTN.hidden=YES;
    _editIcon.hidden = YES;
    _viewWallBTN.selected=YES;
    _editSettingBTN.selected=NO;
    _wallView.hidden=NO;
    _profileBTN.selected=NO;
    _profileView.hidden=YES;
    _ProfileMain.hidden=YES;
    _editSettingView.hidden=YES;
    _btnViewWall_imageview.image= [UIImage imageNamed:@"view_blue_1x"];
    _btnprofile_imageview.image= [UIImage imageNamed:@"profile_unselected-1"];
    _btnEdit_imageview.image= [UIImage imageNamed:@"edit_unselected"];
//    _btnViewWall_label.textColor=UIColorFromRGB(0x676767);
//    _btnViewWall_label.textColor=UIColorFromRGB(0x009AD9);
//    _btnprofile_label.textColor=UIColorFromRGB(0x676767);
//    _btnEdit_label.textColor=UIColorFromRGB(0x676767);
//    _bckVIew2.backgroundColor=UIColorFromRGB(0x009AD9);
//
    NSDictionary *eventLocation = @{@"wallfor": @"Mywall",
                                    @"userid":[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]],
                                    };
    NSLog(@"logedd in user %@",eventLocation);
    
    wallvie=[[WallView alloc]initWithframe:CGRectMake(0, 0, 50, 50) andUserInfo:eventLocation];
    if (IS_IPHONE5) {
    wallvie.frame= CGRectMake(0, 0,_wallView.frame.size.width,_wallView.frame.size.height);
    }
    else if (IS_IPHONE4|| IS_IPHONE6) {
    wallvie.frame = CGRectMake(0, 0,_wallView.frame.size.width,_wallView.frame.size.height+20);
    }
    else if (IS_IPHONE6PLUS) {
        wallvie.frame= CGRectMake(0, 0,_wallView.frame.size.width,_wallView.frame.size.height+90);
    }
//    else if (IS_IPHONE_X) {
//        wallvie.frame= CGRectMake(0, 0,_wallView.frame.size.width,_wallView.frame.size.height+90+75);
//    }
    
    StatusViewController *status=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
    status.checkpostinnormalProfile=@"clickheretopost";
//    _scrollViewhieght.constant=_wallView.frame.size.height;
    wallvie.delegate1=self;
    wallvie.dismischeck=YES;
    wallvie.influencerimageValue=YES;
    wallvie.checkPrayerCount=YES;
    wallvie.clipsToBounds=YES;
    wallvie.selfProfilewallViewHidden=@"selfProfilewallViewHiddenYes";
    [wallvie webservice_call];
//    wallvie.backgroundColor=[UIColor blueColor];
    [_wallView addSubview:wallvie];
}



-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

#pragma mark EditProfileMethod

- (IBAction)editprofile:(id)sender {
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    scrollView.scrollEnabled=YES;
    _scrollViewhieght.constant=458;
    _editsybolsaveheight.constant=50;
    _editSymbolBTN.hidden=YES;
        _editIcon.hidden = YES;
    _bckview3.hidden=YES;
    _bckVIew2.hidden=YES;
    _profileView.hidden=NO;
    _editSettingView.hidden=YES;
    _ProfileMain.hidden=YES;
    _editSybolSaveContinue.hidden=NO;
    _additionalView.hidden=YES;
    _ViewdeactiveView.hidden=YES;
    self.nameTXT.text=[webResponseData valueForKey:@"first_name"];
    self.emailTXT.text=[webResponseData valueForKey:@"last_name"];
    NSString *genderValue=[NSString stringWithFormat:@"%@",[usersprofileDict valueForKey:@"gender"]];
    if ([genderValue intValue]==1) {
        _maleBTN.selected=YES;
        _femaleBTN.selected=NO;
        maleFemaleStr1=@"1";
    }
    else
    {
        if ([genderValue intValue]==2) {
            _maleBTN.selected=NO;
            _femaleBTN.selected=NO;
             maleFemaleStr1=@"";
        }
        else{
            _maleBTN.selected=NO;
            _femaleBTN.selected=YES;
            maleFemaleStr1=@"0";
        }
    }
    NSString *phone=[usersprofileDict valueForKey:@"phone"];
    if (phone==nil ) {
        self.mobileTXT.text=@"-";
    }
    else
    {
    self.mobileTXT.text=[NSString stringWithFormat:@"%@",[usersprofileDict valueForKey:@"phone"]];
    }
    NSMutableDictionary *denomination=[usersprofileDict valueForKey:@"get_denomination_details"];
    if ([denomination isKindOfClass:[NSMutableDictionary class]]) {
        // Display the alert
        NSString *denominationStr=[denomination valueForKey:@"name"];
        if ([denominationStr isEqualToString:@""]) {
            [self.demominationTitleBTN setTitle:@"-" forState:UIControlStateNormal];
        }
        else
        {
            [self.demominationTitleBTN setTitle:denominationStr forState:UIControlStateNormal];
        }
    }
    else{
       [self.demominationTitleBTN setTitle:@"-" forState:UIControlStateNormal];
    }
    //    NSString *strDOB=[usersprofileDict valueForKey:@"date_of_birth"];
//
    if (dobStr==nil || [dobStr isEqualToString:@"-"] ) {
       dobStr=@"-";
        [ self.monthBTN setTitle:@"Months" forState:UIControlStateNormal];
        [ self.dayBTN setTitle:@"Days" forState:UIControlStateNormal];
        [ self.yearBTN setTitle:@"Years" forState:UIControlStateNormal];
        [self.monthBTN setTitleColor:UIColorFromRGB(0xACACAC) forState:UIControlStateNormal];
        [ self.dayBTN setTitleColor:UIColorFromRGB(0xACACAC) forState:UIControlStateNormal];
        [self.yearBTN setTitleColor:UIColorFromRGB(0xACACAC) forState:UIControlStateNormal];
    }
    else{
    NSString *yearValue = [dobStr componentsSeparatedByString:@"/"][2];
    NSString *monthValue = [dobStr componentsSeparatedByString:@"/"][0];
    NSString *dayValue = [dobStr componentsSeparatedByString:@"/"][1];
    [ self.monthBTN setTitle:monthValue forState:UIControlStateNormal];
    [ self.dayBTN setTitle:dayValue forState:UIControlStateNormal];
    [ self.yearBTN setTitle:yearValue forState:UIControlStateNormal];
    [self.monthBTN setTitleColor:UIColorFromRGB(0xACACAC) forState:UIControlStateNormal];
    [ self.dayBTN setTitleColor:UIColorFromRGB(0xACACAC) forState:UIControlStateNormal];
    [self.yearBTN setTitleColor:UIColorFromRGB(0xACACAC) forState:UIControlStateNormal];
        
    }
    }
    
}

#pragma mark VisibilityoptionsMethod

-(void)whoProfileView{
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        if (message) {
        }else{
            message=nil;
            [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
            return;
        }    }
    else {
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"visibilityoptions" withParameters:nil withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            tempArray=response[@"data"];
                            for (int i=0; i<[tempArray count]; i++) {
                                NSDictionary *dict=[tempArray objectAtIndex:i];
                                [whoViewprofileArray addObject:[dict valueForKey:@"title"]];
                                [whoViewprofileArray1 addObject:[dict valueForKey:@"id"]];
                            }
                            
                        });
                    }
                }
                else{
                    UIAlertView *at=[[UIAlertView alloc]initWithTitle:appTitle message:@"server Failed " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [at show];
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
}

#pragma mark GetdenominationMethod

-(void)editDenominationDidload{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        if (message) {
            
        }else{
            message=nil;
            [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
            return;        }
    }
    else {
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"getdenomination" withParameters:nil withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            denominationwebArray=response[@"data"];
                            arr=[[response valueForKey:@"data"]valueForKey:@"name"];
                            denominationArray=[[response valueForKey:@"data"]valueForKey:@"id"];
                        });
                    }
                }
                else{
                    UIAlertView *at=[[UIAlertView alloc]initWithTitle:appTitle message:@"server Failed " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [at show];
                    //                   [self.view makeToast:response[@"message"] duration:1.0 position:CSToastPositionCenter];
                }
            }
        } errorBlock:^(id error)
         {
         }];
    }
    
}

#pragma mark DropDenominatinMethod

-(IBAction) editDenominationDropdownMethod:(id)sender{
    self.demominationTitleBTN.tag=12;
    [self.view endEditing:YES];
    [[NSUserDefaults standardUserDefaults]setObject:@"denomination" forKey:@"denomination"];
    if(dropDown == nil) {
        if ([arr count]!=0) {
            CGFloat f = 200;
            dropDown = [[NIDropDown alloc]showDropDown:[sender superview] :&f :arr :nil :@"down" button:sender];
            dropDown.tag=111;
            dropDown.delegate = self;
        }
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}




#pragma mark - Validation

- (NSString *)validateFields {
    NSString *errorMessage ;
    NSString *strName=nil;
    NSString *strEmail=nil;
    NSString *strMobile=nil;
    NSString *strDenomination=nil;
    strName = [self.nameTXT.text removeWhiteSpaces];
    strEmail = [self.emailTXT.text removeWhiteSpaces];
    strMobile = [self.mobileTXT.text removeWhiteSpaces];
    strDenomination = [self.denominationTXT.text removeWhiteSpaces];
    
    if (self.profile_Picture.image==nil ) {
        errorMessage=ImageStr;
        return errorMessage;
        
    }
    if ([strName length] == 0) {
        errorMessage = FirstnameStr;
        return errorMessage;
    }
    //    if ([strEmail length] == 0) {
    //        errorMessage =EnterEmail;
    //        return errorMessage;
    //    }
    //    BOOL isEmailvalid = [Constant1 validateEmail:strEmail];
    //    if (!isEmailvalid) {
    //        errorMessage =InvalidEmail;
    //        return errorMessage;
    //    }
    
    if ([strEmail length] == 0) {
        errorMessage =LastnameStr;
        return errorMessage;
    }
    
    //    if ([strMobile length] == 0) {
    //        errorMessage =MobileNumberEmpty;
    //        return errorMessage;
    //    }
    //    BOOL isMobile = [Constant1 myMobileNumberValidate:strMobile];
    //    if (!isMobile) {
    //        errorMessage =MobileNumber;
    //        return errorMessage;
    //    }
    
    if ([strDob length]==0) {
        errorMessage =birthdayStr;
        return errorMessage;
    }
    return errorMessage;
}


- (NSString *) validateFieldResetPassword {
    NSString *errorMessage ;
    NSString *strenterOldPassword = [self.enterOldPawwordTXT.text removeWhiteSpaces];
    NSString *strenterNewPassword = [self.enterNewPasswordTXT.text removeWhiteSpaces];
    NSString *strrePassword = [self.rePasswordTXT.text removeWhiteSpaces];
    
    if ([strenterOldPassword length] == 0 || [strenterOldPassword length] < 6) {
        errorMessage = OldPasswordStr;
        return errorMessage;
    }
    
    if ([strenterNewPassword length] == 0 || [strenterNewPassword length] < 6) {
        errorMessage = EnterPassword;
        return errorMessage;
    }
    if ([strrePassword length] == 0 || [strrePassword length] < 6) {
        errorMessage =EnterPassword;
        return errorMessage;
    }
    
    if (![strenterNewPassword isEqualToString:strrePassword]){
        errorMessage=RetypePasswordStr;
        return errorMessage;
    }
    return errorMessage;
}



#pragma mark BottomSaveContinueMethod

- (IBAction)bottmSaveContinueBTN:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
    
    AdditionalDisplayView *displayView=[self.storyboard instantiateViewControllerWithIdentifier:@"AdditionalDisplayView"];
    displayView.additionDispaly=usersprofileDict;
    displayView.checkdict=@"check123";
    [self presentViewController:displayView animated:YES completion:nil];
    }
    
}
//#pragma Friendlist Method
//-(IBAction)friendListMethod:(id)sender{
//    FriendListController *friendListView=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendListController"];
//    [self presentViewController:friendListView animated:YES completion:nil];
//
//
//
//}



#pragma mark DeactivateMethod

-(IBAction)deactivate:(id)sender{
    AppDelegate *appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }
    else{
    
    UIAlertView *deactivateAlert=[[UIAlertView alloc]initWithTitle:appTitle message:@"Sure Do you want to deactivate" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    deactivateAlert.delegate=self;
    deactivateAlert.tag=777;
    [deactivateAlert show];
        
    }
}

#pragma DeactiveWebServiceMethod

-(void)DeactivateWebservice{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        UIAlertView *message1 = [[UIAlertView alloc] initWithTitle:appName message:NoNetwork  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message1 show];
        return;
    }
    else {
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"deactivate" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        NSMutableArray *messageArray =nil;
                        messageArray=[response valueForKey:@"message"];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            CGFloat resetViewWidth = 280;
                            CGFloat resetViewHeight = 300;
                            CGFloat margin = 10;
                            CGFloat imgWidth = 80;
                            CGFloat txtHeight = 70;
                            CGFloat okBtnHeight = 50;
                            
                            // create popup view
                            UIView  *reset_view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, resetViewWidth, resetViewHeight)];
                            reset_view.backgroundColor=[UIColor whiteColor];
                            reset_view.layer.cornerRadius = 8;
                            reset_view.layer.masksToBounds = YES;
                            
                            // create image view
                            UIImageView *okIMage=[[UIImageView alloc]initWithFrame:CGRectMake(reset_view.center.x-(imgWidth/2), (3*margin), imgWidth, imgWidth)];
                            okIMage.image=[UIImage imageNamed:@"tick_50"];
                            okIMage.contentMode = UIViewContentModeScaleAspectFit;
                            [reset_view addSubview:okIMage];
                            
                            // create success text label
                            UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake((2*margin), ((3*margin)+imgWidth+(2*margin)), (resetViewWidth - (4*margin)),txtHeight)];
                            fromLabel.text = [messageArray objectAtIndex:0];
                            fromLabel.font = [UIFont fontWithName:@"Avenir-Roman" size:16.0];
                            fromLabel.numberOfLines = 3;
                            fromLabel.backgroundColor = [UIColor clearColor];
                            fromLabel.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0];
                            fromLabel.textAlignment = NSTextAlignmentCenter;
                            [reset_view addSubview:fromLabel];
                            
                            // create ok btn
                            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                            [button addTarget:self action:@selector(deactiveOKMethod:)
                             forControlEvents:UIControlEventTouchUpInside];
                            [button setTitle:@"Ok" forState:UIControlStateNormal];
                            [button.titleLabel setFont:[UIFont fontWithName:@"Avenir-Roman" size:20.0]];
                            button.backgroundColor=[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f blue:245.0/255.0f alpha:1.0];
                            button.layer.cornerRadius=8.0f;
                            button.frame = CGRectMake((2*margin), ((3*margin)+imgWidth+(2*margin)+txtHeight+(2*margin)), (resetViewWidth - (4*margin)), okBtnHeight);
                            [reset_view addSubview:button];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                            nepopup=[[NeosPopup  alloc]initWithFrame:CGRectZero];
                            nepopup.taponContentView=YES;
                            nepopup.backcontentView=reset_view;
                            [nepopup show];
                            
                        });
                        
                    }
                }
                else{
                    UIAlertView *at=[[UIAlertView alloc]initWithTitle:appTitle message:@"Not Updated " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [at show];
                    //                   [self.view makeToast:response[@"message"] duration:1.0 position:CSToastPositionCenter];
                    
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
}



- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    message=nil;
    if(alertView.tag==777){
        if (buttonIndex == 1){
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(DeactivateWebservice) userInfo:nil repeats:NO];
        }
        
    }
    else if (alertView.tag==456){
        if (buttonIndex==1) {
            [self LogoutAlertActionProfile];
        }
    }

    
}

#pragma mark ClickResetBTNMethod

- (IBAction)clickResetBTN:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else {
        
        CGFloat popUpViewWidth = self.view.frame.size.width-80;
        CGFloat tfHeight = 50.0;
        CGFloat closeBttnHeight = 30;
        CGFloat margin = 10;

        if (IS_IPHONE5) {
            tfHeight = 40;
            closeBttnHeight = 24;
            margin = 6;
        }
        CGFloat popUpViewHeight = ((2*margin)+closeBttnHeight+(2*margin)+tfHeight+margin+tfHeight+margin+tfHeight+(2*margin)+tfHeight);
        
        UIView  *reset_view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, popUpViewWidth, popUpViewHeight)];
        reset_view.backgroundColor=[UIColor whiteColor];
        reset_view.layer.cornerRadius = 8;
        reset_view.layer.masksToBounds = YES;
        
        UIImageView *cancel_pop=[[UIImageView alloc]initWithFrame:CGRectMake(popUpViewWidth-closeBttnHeight-20, (2*margin), closeBttnHeight, closeBttnHeight)];
        cancel_pop.image=[UIImage imageNamed:@"ic_cancel"];
        cancel_pop.contentMode = UIViewContentModeCenter;
        [reset_view addSubview:cancel_pop];
        cancel_pop.userInteractionEnabled=YES;
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancel_popup)];
        [cancel_pop addGestureRecognizer:tapGestureRecognizer ];
        
        UILabel *resetLBL=[[UILabel alloc]initWithFrame:CGRectMake(0, (2*margin), popUpViewWidth, closeBttnHeight)];
        resetLBL.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0];
        resetLBL.text = @"Reset Password";
        resetLBL.font = [UIFont fontWithName:@"Avenir-Medium" size:18.0];
        resetLBL.textAlignment=NSTextAlignmentCenter;
        [reset_view addSubview:resetLBL];
        
        _enterOldPawwordTXT=[[UITextField alloc]init];
        _enterOldPawwordTXT.frame = CGRectMake(20, ((2*margin)+closeBttnHeight+(2*margin)), popUpViewWidth-40, tfHeight);
        _enterOldPawwordTXT.textColor=[UIColor colorWithRed:163.0/255.0 green:174.0/255.0 blue:182.0/255.0 alpha:1.0];
        _enterOldPawwordTXT.font = [UIFont fontWithName:@"Avenir-Roman" size:18.0];
        [_enterOldPawwordTXT.layer setBorderColor:[UIColor colorWithRed:238/255.0f green:242/255.0f  blue:243/255.0f  alpha:1.0].CGColor];
        [_enterOldPawwordTXT.layer setBorderWidth:1.0];
        _enterOldPawwordTXT.layer.cornerRadius = 8;
        _enterOldPawwordTXT.clipsToBounds = YES;
        _enterOldPawwordTXT.backgroundColor=[UIColor whiteColor];
        _enterOldPawwordTXT.placeholder=@"Old Password";
        _enterOldPawwordTXT.textAlignment=NSTextAlignmentCenter;
        [_enterOldPawwordTXT setSecureTextEntry:YES];
        [reset_view addSubview:_enterOldPawwordTXT];
        
        _enterNewPasswordTXT=[[UITextField alloc]init];
        _enterNewPasswordTXT.frame = CGRectMake(20, (((2*margin)+closeBttnHeight+(2*margin))+tfHeight+margin), popUpViewWidth-40, tfHeight);
        _enterNewPasswordTXT.textColor=[UIColor colorWithRed:163.0/255.0 green:174.0/255.0 blue:182.0/255.0 alpha:1.0];
        _enterNewPasswordTXT.font = [UIFont fontWithName:@"Avenir-Roman" size:18.0];
        [_enterNewPasswordTXT.layer setBorderColor:[[UIColor colorWithRed:238/255.0f green:242/255.0f  blue:243/255.0f  alpha:1.0] CGColor]];
        [_enterNewPasswordTXT.layer setBorderWidth:1.0];
        _enterNewPasswordTXT.layer.cornerRadius = 8;
        _enterNewPasswordTXT.clipsToBounds = YES;
        _enterNewPasswordTXT.backgroundColor=[UIColor whiteColor];
        _enterNewPasswordTXT.placeholder=@"New Password";
        _enterNewPasswordTXT.textAlignment=NSTextAlignmentCenter;
        [_enterNewPasswordTXT setSecureTextEntry:YES];
        [reset_view addSubview:_enterNewPasswordTXT];
        
        _rePasswordTXT=[[UITextField alloc]init];
        _rePasswordTXT.frame =CGRectMake(20, (((2*margin)+closeBttnHeight+(2*margin))+tfHeight+margin+tfHeight+margin), popUpViewWidth-40, tfHeight);
        _rePasswordTXT.textColor=[UIColor colorWithRed:163.0/255.0 green:174.0/255.0 blue:182.0/255.0 alpha:1.0];
        _rePasswordTXT.font = [UIFont fontWithName:@"Avenir-Roman" size:18.0];
        [_rePasswordTXT.layer setBorderColor:[[UIColor colorWithRed:238/255.0f green:242/255.0f  blue:243/255.0f  alpha:1.0] CGColor]];
        [_rePasswordTXT.layer setBorderWidth:1.0];
        _rePasswordTXT.layer.cornerRadius = 8;
        _rePasswordTXT.clipsToBounds = YES;
        _rePasswordTXT.backgroundColor=[UIColor whiteColor];
        _rePasswordTXT.placeholder=@"Retype Password";
        _rePasswordTXT.textAlignment=NSTextAlignmentCenter;
        [_rePasswordTXT setSecureTextEntry:YES];
        [reset_view addSubview:_rePasswordTXT];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, (((2*margin)+closeBttnHeight+(2*margin))+tfHeight+margin+tfHeight+margin+tfHeight+(2*margin)), popUpViewWidth, tfHeight);
        [button addTarget:self action:@selector(okMethodReset:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Save" forState:UIControlStateNormal];
        button.backgroundColor=[UIColor colorWithRed:0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0];
        //button.layer.cornerRadius=8.0f;
        [reset_view addSubview:button];
        
        nepopup=[[NeosPopup  alloc]initWithFrame:CGRectZero];
        nepopup.taponContentView=YES;
        nepopup.backcontentView=reset_view;
        [nepopup show];
        
    }
    
    
}
-(void)cancel_popup{
    [nepopup dismiss];
}
- (IBAction)savEditBTN:(id)sender {
    
    
}
#pragma mark CalculatedDOBMEthod

- (NSInteger)age:(NSDate *)dateOfBirth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *dateComponentsNow = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDateComponents *dateComponentsBirth = [calendar components:unitFlags fromDate:dateOfBirth];
    
    if (([dateComponentsNow month] < [dateComponentsBirth month]) ||
        (([dateComponentsNow month] == [dateComponentsBirth month]) && ([dateComponentsNow day] < [dateComponentsBirth day]))) {
        return [dateComponentsNow year] - [dateComponentsBirth year] - 1;
    } else {
        return [dateComponentsNow year] - [dateComponentsBirth year];
    }
}

-(NSString*)btn_brthday{
    NSString *errorMessage=nil;
    strDob = [NSString stringWithFormat: @"%@%@%@%@%@ ",_yearBTN.titleLabel.text,@"/",_monthBTN.titleLabel.text,@"/",_dayBTN.titleLabel.text];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy/mm/dd"];
    NSDate *date1 = [dateFormatter dateFromString:strDob];
        NSInteger ageBirth=[self age:date1];
        [[NSUserDefaults standardUserDefaults]setInteger:ageBirth forKey:@"agebirthprofile"];
    [[NSUserDefaults standardUserDefaults] synchronize];
        if (yearPicked==YES) {
            if (ageBirth<=12) {
                errorMessage=@"you must be 13 years old";
                return errorMessage;
            }
        }
    return 0;
}


#pragma mark EditSybolSaveContinueMethod

- (IBAction)editSybolSaveContinueMethod:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }
    else {
     NSString *errorMassege;
    NSInteger valueint =[[NSUserDefaults standardUserDefaults] integerForKey:@"indexpath"];

    NSString *valueID=[NSString stringWithFormat:@"%@",[denominationArray objectAtIndex:valueint]];
    [[NSUserDefaults standardUserDefaults]setObject:valueID forKey:@"denominationValue"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([valueID intValue]==0) {
        valueID=@"";
    }
        
    NSString *mobNumber,*finalDOb,*MixBirthdate,*strDate;
    strDob = [[NSString stringWithFormat: @"%@%@%@%@%@ ",_yearBTN.titleLabel.text,@"/",_monthBTN.titleLabel.text,@"/",_dayBTN.titleLabel.text] stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSDateFormatter *df1 = [[NSDateFormatter alloc] init] ;
        [df1 setTimeZone:[NSTimeZone localTimeZone ]];
        [df1 setDateFormat:@"yyyy/mm/dd"];
        NSDate *dtPostDate = [df1 dateFromString:strDob];
//        strDate=[df1 stringFromDate:dtPostDate];
//        NSLog(@"nsadate %@",strDate);
            if ([strDob isEqualToString:@"(null)/(null)/(null)"]) {
            [self.view makeToast:STDStr duration:1.0 position:CSToastPositionCenter];
            return;
        }
        else{
            errorMassege=[self btn_brthday];
            if ([errorMassege isEqualToString:@"you must be 13 years old"]) {
                [self.view makeToast:errorMassege duration:1.0 position:CSToastPositionCenter];
                return;
            }
            else{
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone ]];
                [dateFormatter setDateFormat:@"yyyy/mm/dd"];
                NSDate *date = [dateFormatter dateFromString:strDob];
                finalDOb=[dateFormatter stringFromDate:date];
            }
        }
    [[NSUserDefaults standardUserDefaults]setObject:self.nameTXT.text forKey:@"nameupdate"];
    [[NSUserDefaults standardUserDefaults]setObject:self.emailTXT.text forKey:@"lastnameupdate"];
    [[NSUserDefaults standardUserDefaults]setObject:finalDOb forKey:@"dobupdate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSString *strMobile=_mobileTXT.text;
//    BOOL isMobile = [Constant1 myMobileNumberValidate:strMobile];
//    if (!isMobile) {
//        [self.view makeToast:MobileNumber duration:1.0 position:CSToastPositionCenter];
//  
//    }
//    else{
        
        if ([_mobileTXT.text intValue]==0) {
            mobNumber=@"";
        }
        else
        {
            mobNumber=_mobileTXT.text;
        }
        [[NSUserDefaults standardUserDefaults]setObject:_mobileTXT.text forKey:@"phone"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSDictionary *dict;
        NSString *imagestr=[self base64String:chosenImage];
        
         NSString *emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
        
        if ([emailID isEqualToString:@""]||[emailID isEqualToString:@"(null)"]) {
            emailID=@"";
        }
        else{
            emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
        }
       
        NSLog(@"maleFemaleStr1 %@",maleFemaleStr1);
        // mix panel event
        if (_maleBTN.selected==NO && _femaleBTN.selected==NO) {
            [self.view makeToast:genderStr duration:1.0 position:CSToastPositionCenter];
        }
        else{
        if (imagestr==nil) {
        dict = @{@"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                  @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                  @"first_name" :self.nameTXT.text,
                  @"last_name" :self.emailTXT.text,
                  @"date_of_birth" :finalDOb,
                  @"gender":maleFemaleStr1,
                  @"denomination":valueID,
                  @"phone":mobNumber,
//                  @"image":[[NSUserDefaults standardUserDefaults]valueForKey:@"imageValue"],

                };
            
            // mix panel event
            NSString *genderMixpanle;
            if ([maleFemaleStr1 isEqualToString:@"1"]) {
                genderMixpanle=@"M";
            }
            if ([maleFemaleStr1 isEqualToString:@"2"]) {
                genderMixpanle=@"U";
            }
            if([maleFemaleStr1 isEqualToString:@"0"]) {
                genderMixpanle=@"F";
            }
            NSString *accountCreate=[Constant1 createTimeStamp];
            [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Email":emailID,@"Registration method":@"standard",@"Gender":genderMixpanle,@"Birthdate":dtPostDate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"$first_name":self.nameTXT.text,@"$last_name":self.emailTXT.text,@"Last Profile Update":accountCreate}];
            [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"$email":emailID,@"Registration method":@"standard",@"gender":genderMixpanle,@"Birthdate":dtPostDate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"$first_name":self.nameTXT.text,@"$last_name":self.emailTXT.text,@"Last Profile Update":accountCreate}];
            [[AppDelegate sharedAppDelegate].mxPanel track:@"Update Profile"
                    properties:@{@"Email":emailID,@"Registration method":@"standard",@"Gender":genderMixpanle,@"Birthdate":dtPostDate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"Last Profile Update":accountCreate}];
        }
        else{
            dict = @{
                @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                 @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                 @"first_name" :self.nameTXT.text,
                 @"last_name" :self.emailTXT.text,
                 @"date_of_birth" :finalDOb,
                 @"image":imagestr,
                 @"gender":maleFemaleStr1,
                 @"denomination":valueID,
                 @"phone":mobNumber,
                };
            // mix panel event
            NSString *genderMixpanle;
            if ([maleFemaleStr1 isEqualToString:@"1"]) {
                genderMixpanle=@"M";
            }
            if ([maleFemaleStr1 isEqualToString:@"2"]) {
                genderMixpanle=@"U";
            }
            if([maleFemaleStr1 isEqualToString:@"0"]) {
                genderMixpanle=@"F";
            }
            NSString *accountCreate=[Constant1 createTimeStamp];
            [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Email":emailID,@"Registration method":@"standard",@"Gender":genderMixpanle,@"Birthdate":dtPostDate,@"Uploaded Picture":[NSNumber numberWithBool:true],@"$first_name":self.nameTXT.text,@"$last_name":self.emailTXT.text,@"Last Profile Update":accountCreate}];
            
            [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"$email":emailID,@"Registration method":@"standard",@"gender":genderMixpanle,@"Birthdate":dtPostDate,@"Uploaded Picture":[NSNumber numberWithBool:true],@"$first_name":self.nameTXT.text,@"$last_name":self.emailTXT.text,@"Last Profile Update":accountCreate}];
            
            [[AppDelegate sharedAppDelegate].mxPanel track:@"Update Profile"
                                                properties:@{@"Email":emailID,@"Registration method":@"standard",@"Gender":genderMixpanle,@"Birthdate":dtPostDate,@"Uploaded Picture":[NSNumber numberWithBool:true],@"Last Profile Update":accountCreate}];
        }
        
//        NSLog(@"dict parametr  %@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"update" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                           AdditionInfoViewController *addInfoView=[self.storyboard instantiateViewControllerWithIdentifier:@"AdditionInfoViewController"];
                            addInfoView.additionDispaly=usersprofileDict;
                            [self presentViewController:addInfoView animated:YES completion:nil];
                            }
                        else{
                            NSMutableArray *messageArray = nil;
                            messageArray=[response valueForKey:@"message"];
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
//    }
 }
}
}
-(NSString*)base64String:(UIImage*)image{
    NSString *base64EncodeStr = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    base64EncodeStr = [base64EncodeStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    return base64EncodeStr;
}


-(IBAction)closePopViewChange:(id)sender{
    popUp.hidden=YES;
}
-(IBAction)closePopViewDeactivate:(id)sender{
    popUpView1.hidden=YES;
}

- (IBAction)meBTNMethod:(id)sender {
    [[NSUserDefaults standardUserDefaults]setObject:@"me" forKey:@"me"];
    [[NSUserDefaults standardUserDefaults]setObject:@"me" forKey:@"meDropdown"];
    
    NSMutableArray *WhoViewProfileArrayData1=[[NSMutableArray alloc]init];
    for (int i=0; i<[whoViewprofileArray count]; i++) {
        NSString *name=[whoViewprofileArray objectAtIndex:i];
        [WhoViewProfileArrayData1 addObject:name];
    }
    if (WhoViewProfileArrayData1!=nil) {
        if(dropDown == nil) {
            CGFloat f = 200;
            dropDown = [[NIDropDown alloc]showDropDown:[sender superview] :&f :WhoViewProfileArrayData1 :nil :@"down" button:sender];
            dropDown.delegate = self;
            dropDown.tag=666;
        }
        else{
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    
}

- (IBAction)myfriendMethod:(id)sender {
    [[NSUserDefaults standardUserDefaults]setObject:@"me" forKey:@"me"];
    [[NSUserDefaults standardUserDefaults]setObject:@"me" forKey:@"meDropdown2"];
    NSMutableArray *WhoViewProfileArrayData=[[NSMutableArray alloc]init];
    for (int i=0; i<[whoViewprofileArray count]-1; i++) {
        NSString *name=[whoViewprofileArray objectAtIndex:i];
        [WhoViewProfileArrayData addObject:name];
    }
    if (WhoViewProfileArrayData!=nil) {
        if(dropDown == nil) {
            CGFloat f = 200;
            dropDown = [[NIDropDown alloc]showDropDown:[sender superview] :&f :WhoViewProfileArrayData :nil :@"down" button:sender];
            dropDown.delegate = self;
            dropDown.tag=777;

        }
        else{
            [dropDown hideDropDown:sender];
            [self rel];
            
        }
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
    
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [sender removeFromSuperview];
    sender = nil;
    [self rel];
    
    if ( [[[NSUserDefaults standardUserDefaults] valueForKey:@"meDropdown"] isEqualToString:@"me"])
    {
        allProfilePostView=@"profileview_option";
        [self meProfileWebservceDropDwon];
        [[NSUserDefaults standardUserDefaults]setObject:@"out" forKey:@"meDropdown"];
        
    }
    if ( [[[NSUserDefaults standardUserDefaults] valueForKey:@"meDropdown2"] isEqualToString:@"me"])
    {              checkStr=@"2";
        allProfilePostView=@"post_option";
        
        [self meProfileWebservceDropDwon];
        [[NSUserDefaults standardUserDefaults]setObject:@"out2" forKey:@"meDropdown2"];
        
    }
    
}

-(void)meProfileWebservceDropDwon{
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
      
    NSString *idTitle;
    NSString *indexPath=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"indexpath"]];
    int indexP=[indexPath intValue];
    
    
    if ([checkStr isEqualToString:@"2"]) {
        checkStr=@"3";
        for (int i=0; i<[whoViewprofileArray1 count]; i++)
        {
            if (i==indexP) {
                idTitle =[whoViewprofileArray1 objectAtIndex:indexP];
            }
        }
    }
    else{
        for (int i=0; i<[whoViewprofileArray1 count]; i++) {
            if (i==indexP) {
                idTitle =[whoViewprofileArray1 objectAtIndex:indexP];
            }
        }
        
    }
  
        NSDictionary *dict = @{
                               @"field_value" :idTitle,
                               @"field_name" :allProfilePostView,
                               @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"addprofilesetting" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]])  {
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSArray *messageArray=nil;
                            messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            
                        });
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
//                        UIImage *placeholderImage = [UIImage imageNamed:@"profile_image"];
//                        self.profile_Picture.image=placeholderImage;
                    });
                    
                }
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
    
    }
}
-(void)rel{
    dropDown = nil;
}

//Dropdown menu action

#pragma mark MonthDropdownMethod

-(IBAction)monthDrop:(id)sender{
    
    [[NSUserDefaults standardUserDefaults]setObject:@"month" forKey:@"month"];
    if(dropDown == nil) {
        CGFloat f = 200;
        
        [[[sender superview] superview] setTag:159];
        [[sender superview]  setTag:160];
        dropDown = [[NIDropDown alloc]showDropDown:[[sender superview] superview] :&f :month :nil :@"down" button:sender];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
 
}
#pragma mark DayDropdownMethod

-(IBAction)dayDrop:(id)sender{
    [[NSUserDefaults standardUserDefaults]setObject:@"month" forKey:@"month"];
    
    if(dropDown == nil) {
        CGFloat f = 200;
        [[[sender superview] superview] setTag:169];
        [[sender superview]  setTag:170];
        
        dropDown = [[NIDropDown alloc]showDropDown:[[sender superview] superview] :&f :dateArray :nil :@"down" button:sender];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
}
// get year web response

#pragma mark YearListDropdownMethod

-(void)yearlistProfile{
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (!appDelegate.isReachable) {
        return ;
    }
    else{
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"getyears" withParameters:nil withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        for (int k=0; k<[[response valueForKey:@"data"] count]; k++) {
                            [years addObject:[NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] objectAtIndex:k]]];
                            
                        }
                        years=[[[years reverseObjectEnumerator] allObjects] mutableCopy];
//                        NSLog(@"year profile %@",years);
                    }
                }
                
            }
            
        } errorBlock:^(id error)
         
         {
             
         }];
    }
}

#pragma mark YearDropdownMethod


-(IBAction)yearDrop:(id)sender{
    yearPicked=YES;
    [[NSUserDefaults standardUserDefaults]setObject:@"month" forKey:@"month"];
//    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyy"];
//    int i2  = [[formatter stringFromDate:[NSDate date]] intValue];
//    years = [[NSMutableArray alloc] init];
//    for (int i=1915; i<=i2; i++) {
//        [years addObject:[NSString stringWithFormat:@"%d",i]];
//    }

    if(dropDown == nil) {
        CGFloat f = 200;
        [[[sender superview] superview] setTag:179];
        [[sender superview]  setTag:180];
        
        dropDown = [[NIDropDown alloc]showDropDown:[[sender superview] superview] :&f :years :nil :@"down" button:sender];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
}

-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents multipleSelection:(BOOL)multipleSelection
{
    
    // [_dropdown setDrodownAnimation:rand()%2];
    
    //  [_dropdown setAllowMultipleSelection:multipleSelection];
    [_dropdown setupDropdownForView:self.view direction:VSDropdownDirection_Down withBaseColor:[UIColor grayColor] scale:1.0];
    
    [_dropdown setupDropdownForView:sender];
    
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    
    if (_dropdown.allowMultipleSelection)
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:[[sender titleForState:UIControlStateNormal] componentsSeparatedByString:@";"]];
        
    }
    else
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
        
    }
    
}
#pragma mark - VSDropdown Delegate methods.

- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected
{
    UIButton *btn = (UIButton *)dropDown.dropDownView;
    
    NSString *allSelectedItems = nil;
    if (dropDown.selectedItems.count > 1)
    {
        allSelectedItems = [dropDown.selectedItems componentsJoinedByString:@";"];
        
    }
    else
    {
        allSelectedItems = [dropDown.selectedItems firstObject];
        
    }
    [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    
}

- (IBAction)male_act:(id)sender {
        _maleBTN.selected=YES;
        _femaleBTN.selected=NO;
        maleFemaleStr1=@"1";
    
    
}

- (IBAction)female_act:(id)sender {
        _maleBTN.selected=NO;
        _femaleBTN.selected=YES;
        maleFemaleStr1=@"0";
    
}

- (IBAction)textFieldFinished:(id)sender
{
    [sender resignFirstResponder];
}



#pragma Reset password method

- (IBAction)okMethodReset:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    } else {
        
        NSString *errorMessage = [self validateFieldResetPassword];
        
        if (errorMessage) {
            [[[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
            return;
            
        }
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"old_password" :self.enterOldPawwordTXT.text,
                               @"new_password" :self.enterNewPasswordTXT.text,
                               @"retype_password" :self.rePasswordTXT.text,
                               };
//        NSLog(@"dict parametr  %@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"resetpassword" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            [nepopup dismiss];
                            NSMutableArray *messageArray = nil;
                            messageArray=[response valueForKey:@"message"];
                            UIAlertView *at=[[UIAlertView alloc]initWithTitle:appTitle message:[messageArray objectAtIndex:0] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            at.tag=2001;
                            [at show];
                            
                            
                            
                        }
                        else{
                            UIAlertView *at=[[UIAlertView alloc]initWithTitle:appTitle message:@"Not Updated " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [at show];
                            
                            
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
    
    
}
#pragma DeactiveOKMethod

-(IBAction)deactiveOKMethod:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{
        [nepopup dismiss];
        NSString *logouttime=[Constant1 createTimeStamp];
        NSDate *logoutDate=[NSDate date];
        [AppDelegate sharedAppDelegate].logdate=logoutDate;
        NSTimeInterval distanceBetweenDates = [[AppDelegate sharedAppDelegate].logdate timeIntervalSinceDate:[AppDelegate sharedAppDelegate].logindate];
        NSString *timeinterval=[self stringFromTimeInterval:distanceBetweenDates];
        NSLog(@"time interval %@",timeinterval);
        NSString *sessionTime=[Constant1 logoutsession:timeinterval];
        NSLog(@"tsessionTime %@",sessionTime);
        
        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Last seen": logouttime}];
        
        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Last seen": logouttime}];
        [[AppDelegate sharedAppDelegate].mxPanel track:@"Logout" properties:@{
                                                                              @"Last seen": logouttime,
                                                                              @"Session length":sessionTime
                                                                              }];
        
        [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"Login"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"fbvalue"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        SignupViewController *controler=[self.storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
        [self.navigationController setViewControllers:@[controler]];     
});
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[AppDelegate sharedAppDelegate].mxPanel reset];
           });
}


- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}



-(IBAction)logBTN:(id)sender{
    UIAlertView *alertViewaction=[[UIAlertView alloc] initWithTitle:appTitle message:@"Are you sure you want to logout?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alertViewaction.tag=456;
    [alertViewaction show];
}


#pragma LogoutAlertActionProfile

-(void)LogoutAlertActionProfile{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }
    else {
        
        if ([[FBSDKAccessToken currentAccessToken] isKindOfClass:[FBSDKAccessToken class]]) {
            
            AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me/permissions/" parameters:@{@"access_token":[FBSDKAccessToken currentAccessToken]} HTTPMethod:@"DELETE"];
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                  id result,
                                                  NSError *error){
                if (!error) {
//                    NSLog(@"logout successfully");
                }
                else {
//                    NSLog(@"error :: %@",error);
                }
            }];
            
            if (!app.fbManager) {
                app.fbManager = [[FBSDKLoginManager alloc] init];
            }
            
            [app.fbManager logOut];
            
            [FBSDKAccessToken setCurrentAccessToken:nil];
            [FBSDKProfile setCurrentProfile:nil];
            
        }
        
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"device_token":  [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"]?:@"",
                               @"device_type":@"IOS",
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"logout" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSString *logouttime=[Constant1 createTimeStamp];
                                NSDate *logoutDate=[NSDate date];
                                [AppDelegate sharedAppDelegate].logdate=logoutDate;
                                NSTimeInterval distanceBetweenDates = [[AppDelegate sharedAppDelegate].logdate timeIntervalSinceDate:[AppDelegate sharedAppDelegate].logindate];
                                NSString *timeinterval=[self stringFromTimeInterval:distanceBetweenDates];
                                NSLog(@"time interval %@",timeinterval);
                                NSString *sessionTime=[Constant1 logoutsession:timeinterval];
                                NSLog(@"tsessionTime %@",sessionTime);
                                
                                [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Last seen": logouttime}];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Last seen": logouttime}];
                                [[AppDelegate sharedAppDelegate].mxPanel track:@"Logout" properties:@{
                                                @"Last seen": logouttime,
                                                @"Session length":sessionTime
                                                }];
                               

                                [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"Login"];
                                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"fbvalue"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                SignupViewController *controler=[self.storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
                                [self.navigationController setViewControllers:@[controler]];
                                
                            });
                           
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [[AppDelegate sharedAppDelegate].mxPanel reset];
                            
                            });

                        }
                        else{
                            NSArray *messageArray=nil;
                            messageArray=[response valueForKey:@"message"];
                            if ([messageArray count]>0) {
                                NSString *mess=[messageArray objectAtIndex:0];
                                [self.view makeToast:mess duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
  
}





- (void) FunctionOne: (NSString*) dataOne andArray:(NSMutableArray*)arraycontent
{
    
    
    if([dataOne isEqualToString:@"open statusView" ]){
        StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
        statusScreen.status_para=@"1";
        statusScreen.dictofPost=[arraycontent objectAtIndex:0];
//        [self presentViewController:statusScreen animated:YES completion:nil];
        [self.navigationController pushViewController:statusScreen animated:YES];

    }else if ([dataOne isEqualToString:@"open prayer" ]){
        StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
        statusScreen.status_para=@"2";
        // chnages here for passing dictioanary for post of prayer
        statusScreen.dictofPost=[arraycontent objectAtIndex:0];
//        [self presentViewController:statusScreen animated:YES completion:nil];
         [self.navigationController pushViewController:statusScreen animated:YES];
    }else if ([dataOne isEqualToString:@"open seemoreImage" ]){
        
        NSMutableArray *imagearr=[[NSMutableArray alloc]init];
        //  photos=[[NSMutableArray alloc]init];
        for (int i=0; i<arraycontent.count; i++) {
        NSString *str=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,[arraycontent objectAtIndex:i]];
        MWPhoto *photo=[MWPhoto photoWithURL:[NSURL URLWithString:str]];
//        photo.caption = @"Biblefaithfollow.SocialApp";
        [imagearr addObject:photo];
        }
        photos=[[[imagearr reverseObjectEnumerator] allObjects] mutableCopy];
        browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        BOOL displayActionButton = YES;
        BOOL displaySelectionButtons = NO;
        BOOL displayNavArrows = NO;
        BOOL enableGrid = YES;
        BOOL startOnGrid = NO;
        BOOL autoPlayOnAppear = NO;
        browser.displayActionButton = displayActionButton;
        //  browser.displayNavArrows = displayNavArrows;
        browser.displaySelectionButtons = displaySelectionButtons;
        browser.alwaysShowControls = NO;
        browser.zoomPhotosToFill = YES;
        browser.enableGrid = enableGrid;
        browser.startOnGrid = startOnGrid;
        browser.enableSwipeToDismiss = NO;
        //browser.autoPlayOnAppear = autoPlayOnAppear;
        [browser setCurrentPhotoIndex:0];
        enableGrid = NO;
             
        [self.navigationController pushViewController:browser animated:YES];
    }
}





-(void) btn_pressed: (NSString*) buttonName andtotal_comments:(NSMutableArray*)totalComments andpost:(NSDictionary*)post andCellindex:(NSUInteger)cell_index{
    SeeMoreCommentsViewController *seemoreScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"SeeMoreCommentsViewController"];
    seemoreScreen.totalComments=totalComments;
    seemoreScreen.post_dict=post;
    seemoreScreen.cellindex=cell_index;
//     [self presentViewController:seemoreScreen animated:YES completion:nil];
    [self.navigationController pushViewController:seemoreScreen animated:YES];
}
-(void)openViewProfile: (NSString*) dataOne andArray:(NSMutableDictionary*)dictcontent{
    
    if([dataOne isEqualToString:@"open friend" ]){
        
        NSString *idofUser=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
        int val=[idofUser intValue];
        int val2=[dictcontent[@"id"] intValue];
        dispatch_async(dispatch_get_main_queue(), ^{
//            NSLog(@"id is%@",dictcontent);
            
            if (val==val2) {
                
                //[[NSNotificationCenter defaultCenter] postNotificationName: @"profileshow" object:nil userInfo:nil];
            }else{
                
                FriendsProfileViewController *friend=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                friend.friendProfileDict=dictcontent;
                friend.postFriendProfileDict=dictcontent;
//                [self presentViewController:friend animated:YES completion:nil];
                [self.navigationController pushViewController:friend animated:YES];
                
            }
            
        });
    }
    
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
    
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
//    NSLog(@"Did finish modal presentation");
    // [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}



@end
