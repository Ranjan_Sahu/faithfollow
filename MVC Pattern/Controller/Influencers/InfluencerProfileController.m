//
//  InfluencerProfileController.m
//  wireFrameSplash
//
//  Created by webwerks on 11/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]


#import "InfluencerProfileController.h"
#import "AboutMeTableViewCell.h"
#import "MysideTableViewCell.h"
#import "customCellTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "FeedViewController.h"
#import "Constant1.h"
#import "LoginViewController.h"
#import "MWPhotoBrowser.h"
#import "SeeMoreCommentsViewController.h"
#import "Profile_editViewController.h"


@interface InfluencerProfileController ()<MWPhotoBrowserDelegate>
{
    NSArray *dataArray,*messageArray;
    WallView *wallvie;
    NSDictionary *responseDict;
    NSString *str,*checkdismissPhoto;
    MWPhotoBrowser *browser;
     NSMutableArray *photos;
    NSDictionary *userProfileDict;

}
@end

@implementation InfluencerProfileController
@synthesize influencerDict;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    responseDict=[[NSDictionary alloc]init];
    dataArray =[[NSArray alloc]init];
    _postImge_tab2.image= [UIImage imageNamed:@"view_1x"];
    _profileImage_Tab1.image= [UIImage imageNamed:@"profile_selected-1"];
    _blueIndicatorPost.hidden=YES;
    //    _postlabel_tab2.textColor=UIColorFromRGB(0x676767);
    //    _profileLabel_Tab1.textColor=UIColorFromRGB(0x009AD9);
    _postWallView.hidden=YES;
    _bottomView.hidden=NO;
    
    [self.tableview registerNib:[UINib nibWithNibName:@"AboutMeTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"AboutMeTableViewCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"MysideTableViewCell" bundle:nil] forCellReuseIdentifier:@"MysideTableViewCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"customCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"customCellTableViewCell"];
    self.tableview.estimatedRowHeight = 10.0f;
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    [self.tableview layoutIfNeeded];
    self.tableview.tableFooterView = [UIView new];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"influcencerProfileView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"feedGroupsuggestion" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"friendProfileView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"userProfileView" object:nil];
}
#pragma mark influgroupMethod

-(void)influgroupMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        DetailMygroupViewController1 *detailgroupFeed=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
        detailgroupFeed.navigationTitle=[userInfo valueForKey:@"groupname"];
        detailgroupFeed.idUser=[userInfo valueForKey:@"groupis"];
        [self.navigationController pushViewController:detailgroupFeed animated:YES];
    });
    
}

#pragma mark friendProfileViewMethod

-(void)friendProfileView:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        FriendsProfileViewController *friend=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
        NSMutableDictionary *dictcontent=[userInfo valueForKey:@"allData"];
        friend.friendProfileDict=dictcontent;
        friend.postFriendProfileDict=dictcontent;
        [self.navigationController pushViewController:friend animated:YES];
        
    });
    
}
#pragma mark influcencerProfileViewMethod

-(void)influcencerProfileViewMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        InfluencerProfileController *InfluencerProfileControllerView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
        NSMutableDictionary *influencerDictData=[userInfo valueForKey:@"allData"];
        InfluencerProfileControllerView.influencerDict=influencerDictData;
        [self.navigationController pushViewController:InfluencerProfileControllerView animated:YES];
        
    });
    
}

-(void)userProfileViewfriendProfile:(NSNotification *) notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        Profile_editViewController *friendprofile=[self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
        [self.navigationController pushViewController:friendprofile animated:YES];
    });
}

-(void)updateCommentfriend :(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    [wallvie updateCommentfromProfile:userInfo];
}

-(void)reloadcellwallviewProfilefriend :(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    [wallvie reloadcellwallview:userInfo];
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateCommentfriend:)
                                                 name:@"commentsNoti"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadcellwallviewProfilefriend:)
                                                 name:@"reloadcellwall"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userProfileViewfriendProfile:)
                                                 name:@"userProfileView"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(influncerPhoto:)
                                                 name:@"influencerPhoto"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(influgroupMethod:)
                                                 name:@"feedGroupsuggestion"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(friendProfileView:)
                                                 name:@"friendProfileView"
                                               object:nil];
    
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (getVar.checksuggestedInfluencerNotification==YES) {
        getVar.checksuggestedInfluencerNotification=NO;;
    }
    else{
        [[NSNotificationCenter defaultCenter] addObserver:self
                            selector:@selector(influcencerProfileViewMethod:)
                                    name:@"influcencerProfileView"
                                                   object:nil];
    }

    if([AppDelegate sharedAppDelegate].isDeeplink)
    {
        [self friendProfile:[AppDelegate sharedAppDelegate].influencerId];
        [AppDelegate sharedAppDelegate].isDeeplink =NO;
    }
    else
    {
        if ([influencerDict isKindOfClass:[NSMutableDictionary class]]) {
            NSString *frinedStatus;
            _nameInluencer.text=[NSString stringWithFormat:@"%@ %@",[influencerDict valueForKey:@"first_name"],[influencerDict valueForKey:@"last_name"]];
            frinedStatus=[influencerDict valueForKey:@"friend_status"];
            
            if ([frinedStatus isEqualToString:@"Follow"]) {
                [self.followBTN setTitle:@"Follow" forState:UIControlStateNormal];
                [self.followBTN setBackgroundColor:[UIColor whiteColor]];
                self.followBTN.layer.borderWidth  = 1.0f;
                self.followBTN.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f].CGColor;
                [self.followBTN setTitleColor:[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
                [self.followBTN addTarget:self action:@selector(followInfluecerProfile:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            if ([frinedStatus isEqualToString:@"Unfollow"] || [frinedStatus isEqualToString:@"See their post"] )
            {
                [self.followBTN setTitle:@"Unfollow" forState:UIControlStateNormal];
                [self.followBTN setBackgroundColor:[UIColor orangeColor]];
                [self.followBTN setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                self.followBTN.layer.borderWidth  = 1.0f;
                self.followBTN.layer.borderColor = [UIColor orangeColor].CGColor;
                [self.followBTN addTarget:self action:@selector(followInfluecerProfile:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            
            userProfileDict=[[NSDictionary alloc]init];
            userProfileDict=[influencerDict valueForKey:@"usersprofile"];
            if ([userProfileDict isKindOfClass:[NSDictionary class]]) {
                
                //                self.inlurncerImgae.layer.borderColor=[UIColor colorWithRed:59.0/255.0f green:147.0/255.0f  blue:192.0/255.0f  alpha:1.0f].CGColor;
                //                self.inlurncerImgae.layer.borderWidth=1.0f;
                //                self.inlurncerImgae.layer.cornerRadius =3;
                self.inlurncerImgae.clipsToBounds = YES;
                str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[userProfileDict valueForKey:@"profile_picture"]];
                NSURL *imageURL = [NSURL URLWithString:str];
                UITapGestureRecognizer   *singleTapGestureProfile =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapImage:)];
                self.inlurncerImgae.userInteractionEnabled=YES;
                self.inlurncerImgae.multipleTouchEnabled=YES;
                [self.inlurncerImgae addGestureRecognizer:singleTapGestureProfile];
                
                [self.inlurncerImgae sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
                
            }
            
        }
        
        //    NSLog(@"data %@",influencerDict);
        if ([checkdismissPhoto isEqualToString:@"donedismiss"]) {
            checkdismissPhoto=@"";
        }
        else{
            NSString *user_id=[NSString stringWithFormat:@"%@",[influencerDict valueForKey:@"id"]];
            [self friendoffriendMethod:user_id];
            
        }

    }
    
    
}

#pragma mark friendProfileMethod

-(void)friendProfile:(NSString *)friend_id{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        return;
    }
    else{
        NSDictionary *dict = @{
                               @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"friend_id" : friend_id,
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if([response[@"status"]intValue ] ==1){
                    NSMutableDictionary  *influencerdataDict=[[response valueForKey:@"data"] mutableCopy];
                    if ([influencerdataDict isKindOfClass:[NSMutableDictionary class]]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            influencerDict=influencerdataDict;
                            if ([influencerDict isKindOfClass:[NSMutableDictionary class]]) {
                                NSString *frinedStatus;
                                _nameInluencer.text=[NSString stringWithFormat:@"%@ %@",[influencerDict valueForKey:@"first_name"],[influencerDict valueForKey:@"last_name"]];
                                frinedStatus=[influencerDict valueForKey:@"friend_status"];
                                
                                if ([frinedStatus isEqualToString:@"Follow"]) {
                                    [self.followBTN setTitle:@"Follow" forState:UIControlStateNormal];
                                    [self.followBTN setBackgroundColor:[UIColor whiteColor]];
                                    self.followBTN.layer.borderWidth  = 1.0f;
                                    self.followBTN.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f].CGColor;
                                    [self.followBTN setTitleColor:[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
                                    [self.followBTN addTarget:self action:@selector(followInfluecerProfile:) forControlEvents:UIControlEventTouchUpInside];
                                }
                                
                                if ([frinedStatus isEqualToString:@"Unfollow"] || [frinedStatus isEqualToString:@"See their post"] )
                                {
                                    [self.followBTN setTitle:@"Unfollow" forState:UIControlStateNormal];
                                    [self.followBTN setBackgroundColor:[UIColor orangeColor]];
                                    [self.followBTN setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                    self.followBTN.layer.borderWidth  = 1.0f;
                                    self.followBTN.layer.borderColor = [UIColor orangeColor].CGColor;
                                    [self.followBTN addTarget:self action:@selector(followInfluecerProfile:) forControlEvents:UIControlEventTouchUpInside];
                                }
                                
                                
                                userProfileDict=[[NSDictionary alloc]init];
                                userProfileDict=[influencerDict valueForKey:@"usersprofile"];
                                if ([userProfileDict isKindOfClass:[NSDictionary class]]) {
                                    
//                                    self.inlurncerImgae.layer.borderColor=[UIColor colorWithRed:59.0/255.0f green:147.0/255.0f  blue:192.0/255.0f  alpha:1.0f].CGColor;
//                                    self.inlurncerImgae.layer.borderWidth=1.0f;
//                                    self.inlurncerImgae.layer.cornerRadius =3;
                                    self.inlurncerImgae.clipsToBounds = YES;
                                    str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[userProfileDict valueForKey:@"profile_picture"]];
                                    NSURL *imageURL = [NSURL URLWithString:str];
                                    UITapGestureRecognizer   *singleTapGestureProfile =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapImage:)];
                                    self.inlurncerImgae.userInteractionEnabled=YES;
                                    self.inlurncerImgae.multipleTouchEnabled=YES;
                                    [self.inlurncerImgae addGestureRecognizer:singleTapGestureProfile];
                                    
                                    [self.inlurncerImgae sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
                                    
                                    if ([checkdismissPhoto isEqualToString:@"donedismiss"]) {
                                        checkdismissPhoto=@"";
                                    }
                                    else{
                                        NSString *user_id=[NSString stringWithFormat:@"%@",[influencerDict valueForKey:@"id"]];
                                        [self friendoffriendMethod:user_id];
                                        
                                    }

                                    
                                }
                                
                            }

                        });
                    }
                }
                else
                {
                    messageArray=[response valueForKey:@"message"];
                    if ([messageArray count]>0) {
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        return;
                        
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
             
         }];
    }

}


-(void)openViewProfile: (NSString*) dataOne andArray:(NSMutableDictionary*)dictcontent{
    if([dataOne isEqualToString:@"open friend" ]){
        NSString *idofUser=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
        int val=[idofUser intValue];
        int val2=[dictcontent[@"id"] intValue];
        dispatch_async(dispatch_get_main_queue(), ^{
            //            NSLog(@"id is%@",dictcontent);
            if (val==val2) {
                [self.navigationController popToRootViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName: @"profileshow" object:nil userInfo:nil];
            }else{
                
                FriendsProfileViewController *friend=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                friend.friendProfileDict=dictcontent;
                friend.postFriendProfileDict=dictcontent;
                    [self.navigationController pushViewController:friend animated:YES];
//                [self presentViewController:friend animated:YES completion:nil];
                
            }
            
            
            
            
        });
    }
    
}

#pragma mark influncerPhoto
-(void)influncerPhoto:(NSNotification *) notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
        post_imageArray = [[notification userInfo] objectForKey:@"myArray"];
//        NSLog(@"post_imagearray %@",post_imageArray);
        [self openphoto:post_imageArray influencerPhoto:@"yes"];
    });
    
}
#pragma mark friendoffriendMethod

-(void)friendoffriendMethod:(NSString *)friend_id{
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        
        NSDictionary *dict = @{
                               @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"friend_id" : friend_id,
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if([response[@"status"]intValue ] ==1){
                    responseDict=[response valueForKey:@"data"];
                    if ([responseDict isKindOfClass:[NSDictionary class]]) {
                        dataArray=[responseDict valueForKey:@"inf_images"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSDictionary *userProfile=[responseDict valueForKey:@"usersprofile"];
                            if ([userProfile isKindOfClass:[NSDictionary class]]) {
                                NSString *subtext=[userProfile valueForKey:@"subtext"];
                                self.influencerPost.text=subtext;
                            }
                            [self.tableview reloadData];
                        });
                        
                    }
                 }
                else
                {
                    messageArray=[response valueForKey:@"message"];
                    if ([messageArray count]>0) {
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        return;
                        
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
            [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//             NSLog(@"error");
             
         }];
    }
    
}

#pragma mark followInfluecerProfileMethod

-(void)followInfluecerProfile:(UIButton*)sender{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *frind_id;
       
        if ([influencerDict isKindOfClass:[NSMutableDictionary class]]) {
            NSMutableDictionary *usersprofile=influencerDict[@"usersprofile"];
            if ([usersprofile isKindOfClass:[NSMutableDictionary class]]) {
                if (usersprofile!=NULL) {
                  frind_id=[usersprofile valueForKey:@"user_id"];
                }
            }
            else{
                frind_id=[influencerDict valueForKey:@"id"];
            }
        }
        NSDictionary *dict;
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 @"friend_id" : frind_id,
                 
                 };
        NSString *methodname=nil;
//        NSLog(@"The button title is %@",sender.titleLabel.text);
        UIButton *btn = (UIButton*)sender;
        if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
            methodname=follow;
        }
        else{
            methodname=unfollow;
            
        }
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodname withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
                                AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                if (getVar.afterFolloSuggetedProfile==YES) {
                                [[NSUserDefaults standardUserDefaults] setObject:@"yesDone" forKey:@"Follow"];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    getVar.afterFolloSuggetedProfile=NO;
                                }
                                [self.followBTN setTitle:@"UnFollow" forState:UIControlStateNormal];
                                [self.followBTN setBackgroundColor:[UIColor orangeColor]];
                                [self.followBTN setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                self.followBTN.layer.borderWidth  = 1.0f;
                                self.followBTN.layer.borderColor = [UIColor orangeColor].CGColor;
                            }
                            else{
                                [self.followBTN setTitle:@"Follow" forState:UIControlStateNormal];
                                [self.followBTN setBackgroundColor:[UIColor whiteColor]];
                                self.followBTN.layer.borderWidth  = 1.0f;
                                self.followBTN.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f].CGColor;
                                [self.followBTN setTitleColor:[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
                            }
    
                            
                        });
                        
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableview reloadData];
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
            [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//             NSLog(@"error");
             
         }];
        
    }
    
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

#pragma mark TableView Data Source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (indexPath.section==0) {
        AboutMeTableViewCell *cell = (AboutMeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AboutMeTableViewCell"                                                                    forIndexPath:indexPath];
        NSDictionary *userconetent=[responseDict valueForKey:@"usersprofile"];
        if ([userconetent isKindOfClass:[NSDictionary class]]) {
            NSString *nodataStr= [userconetent valueForKey:@"about_me"];
            if ([nodataStr isEqualToString:@""]) {
                cell.lblContent.text=@"No Data";
            }
            else{
                cell.lblContent.text=nodataStr;
  
            }
        }
        return cell;
    }
    else if (indexPath.section==1){
        MysideTableViewCell *cell = (MysideTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MysideTableViewCell"                                                                    forIndexPath:indexPath];
        NSDictionary *userconetent=[responseDict valueForKey:@"usersprofile"];
        if ([userconetent isKindOfClass:[NSDictionary class]]) {
            
            NSString *appendString1=[NSString stringWithFormat:@"%@ : %@",[userconetent valueForKey:@"link1_lable"],[userconetent valueForKey:@"link1_url"]];
            NSString *linkLabel1=[userconetent valueForKey:@"link1_lable"];
            
             NSString *appendString2=[NSString stringWithFormat:@"%@ : %@",[userconetent valueForKey:@"link2_lable"],[userconetent valueForKey:@"link2_url"]];
            NSString *linkLabel2=[userconetent valueForKey:@"link2_lable"];

            NSString *appendString3=[NSString stringWithFormat:@"%@ : %@",[userconetent valueForKey:@"link3_lable"],[userconetent valueForKey:@"link3_url"]];
            NSString *linkLabel3=[userconetent valueForKey:@"link3_lable"];

            NSString *appendString4=[NSString stringWithFormat:@"%@ : %@",[userconetent valueForKey:@"link4_lable"],[userconetent valueForKey:@"link4_url"]];
            NSString *linkLabel4=[userconetent valueForKey:@"link4_lable"];
            
            if ([appendString1 isEqualToString:@" : "]) {
                appendString1=@"";
            }
            if ([appendString2 isEqualToString:@" : "]) {
                appendString2=@"";
            }
            if ([appendString3 isEqualToString:@" : "]) {
                appendString3=@"";
            }
            if ([appendString4 isEqualToString:@" : "]) {
                appendString4=@"";
            }
            if ([appendString1 isEqualToString:@""] && [appendString2 isEqualToString:@""] && [appendString3 isEqualToString:@""] && [appendString4 isEqualToString:@""]) {
                NSString *strnodata=@"No Data";
                cell.labelLink1.font = [UIFont fontWithName:@"Avenir-Roman" size:14];
            cell.labelLink1.attributedText=[Constant1 customizeString :strnodata range:0 rangeOne:0];
            }
            else{
                cell.labelLink1.numberOfLines = 0;
                [cell.labelLink1 sizeToFit];
                
                cell.labelLink2.numberOfLines = 0;
                [cell.labelLink2 sizeToFit];
                
                cell.labelLink3.numberOfLines = 0;
                [cell.labelLink3 sizeToFit];
                
                cell.lableLink4.numberOfLines = 0;
                [cell.lableLink4 sizeToFit];
                
                cell.labelLink1.attributedText=[Constant1 customizeString :appendString1 range:[linkLabel1 length] rangeOne:[appendString1 length]-[linkLabel1 length]];
                
                cell.labelLink2.attributedText=[Constant1 customizeString :appendString2 range:[linkLabel2 length] rangeOne:[appendString2 length]-[linkLabel2 length]];
                
                cell.labelLink3.attributedText=[Constant1 customizeString :appendString3 range:[linkLabel3 length] rangeOne:[appendString3 length]-[linkLabel3 length]];
                
                cell.lableLink4.attributedText=[Constant1 customizeString :appendString4 range:[linkLabel4 length] rangeOne:[appendString4 length]-[linkLabel4 length]];
                
                
                UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
                cell.labelLink1.tag=10;
                [ cell.labelLink1 addGestureRecognizer:tapGestureRecognizer];
                cell.labelLink1.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *tapGestureRecognizer1= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
                cell.labelLink2.tag=11;
                [ cell.labelLink2 addGestureRecognizer:tapGestureRecognizer1];
                cell.labelLink2.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *tapGestureRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
                cell.labelLink3.tag=12;
                [ cell.labelLink3 addGestureRecognizer:tapGestureRecognizer2];
                cell.labelLink3.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *tapGestureRecognizer3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
                cell.lableLink4.tag=13;
                [ cell.lableLink4 addGestureRecognizer:tapGestureRecognizer3];
                cell.lableLink4.userInteractionEnabled = YES;
   
            }
           

        }

        return cell;
    }
    else
    if (indexPath.section==2){
        
        static NSString *CellIdentifier = @"customCellTableViewCell";
        customCellTableViewCell *cell = (customCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"customCellTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        if ([dataArray count]>0) {
            cell.NodataLBL.hidden=YES;
            [cell setData:dataArray];
        }
        else{
            cell.NodataLBL.text=@"No Image";
             [cell setData:dataArray];
        }
        
        return cell;
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *viewHeader = [UIView.alloc initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    UILabel *lblTitle =[UILabel.alloc initWithFrame:CGRectMake(24, 15, tableView.frame.size.width, 35)];
    [lblTitle setFont:[UIFont fontWithName:@"Avenir-Medium" size:16]];
    [lblTitle setTextColor:[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1.0]];
    
    switch (section){
        case 0:
            lblTitle.text=@"About Me";
            break;
        case 1:
            lblTitle.text=@"My Site and More";
            break;
        case 2:
            lblTitle.text=@"My Pictures";
            break;
    }
    
    [lblTitle setTextAlignment:NSTextAlignmentLeft];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [viewHeader setBackgroundColor:[UIColor whiteColor]];
    [viewHeader addSubview:lblTitle];
    return viewHeader;
}


#pragma mark labelTappedMethod

- (void)labelTapped:(UITapGestureRecognizer *)tapGesture {
    NSDictionary *userconetent=[responseDict valueForKey:@"usersprofile"];
    NSString *StringUrl=nil;
    if (tapGesture.view.tag==10) {
        StringUrl=[userconetent valueForKey:@"link1_url"];
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:StringUrl]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:StringUrl]];
        }
        else{
        NSString *combinedHttps = [NSString stringWithFormat:@"%@%@",@"https://",StringUrl];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:combinedHttps]];
        }
        
    }
    else if (tapGesture.view.tag==11){
        StringUrl=[userconetent valueForKey:@"link2_url"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:StringUrl]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:StringUrl]];
        }
        else{
            NSString *combinedHttps = [NSString stringWithFormat:@"%@%@",@"https://",StringUrl];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:combinedHttps]];
        }

    }
    else if (tapGesture.view.tag==12){
       StringUrl=[userconetent valueForKey:@"link3_url"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:StringUrl]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:StringUrl]];
        }
        else{
            NSString *combinedHttps = [NSString stringWithFormat:@"%@%@",@"https://",StringUrl];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:combinedHttps]];
        }
    }
    else{
        StringUrl=[userconetent valueForKey:@"link4_url"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:StringUrl]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:StringUrl]];
        }
        else{
            NSString *combinedHttps = [NSString stringWithFormat:@"%@%@",@"https://",StringUrl];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:combinedHttps]];
        }
    }
    
    
}
#pragma mark handleSingleTapImageMethod

- (void)handleSingleTapImage:(UITapGestureRecognizer *)tapGestureRecognizer{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
        [post_imageArray addObject:[userProfileDict valueForKey:@"profile_picture"]];
        [self openphoto:post_imageArray influencerPhoto:@"no"];
    }

    
}

#pragma mark btn_pressedMethod

-(void) btn_pressed: (NSString*) buttonName andtotal_comments:(NSMutableArray*)totalComments andpost:(NSDictionary*)post andCellindex:(NSUInteger)cell_index{
    SeeMoreCommentsViewController *seemoreScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"SeeMoreCommentsViewController"];
    seemoreScreen.totalComments=totalComments;
    seemoreScreen.post_dict=post;
    seemoreScreen.cellindex=cell_index;
    [self.navigationController pushViewController:seemoreScreen animated:YES];
    
    //    [self presentViewController:seemoreScreen animated:YES completion:nil];
}




- (void) FunctionOne: (NSString*) dataOne andArray:(NSMutableArray*)arraycontent
{
    
    if([dataOne isEqualToString:@"open statusView" ]){
        StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
        statusScreen.status_para=@"1";
        statusScreen.dictofPost=[arraycontent objectAtIndex:0];
//        [self presentViewController:statusScreen animated:YES completion:nil];
        [self.navigationController pushViewController:statusScreen animated:YES];

    }else if ([dataOne isEqualToString:@"open prayer" ]){
        StatusViewController *statusScreen=[self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
        statusScreen.status_para=@"2";
        // chnages here for passing dictioanary for post of prayer
        statusScreen.dictofPost=[arraycontent objectAtIndex:0];
//        [self presentViewController:statusScreen animated:YES completion:nil];
         [self.navigationController pushViewController:statusScreen animated:YES];
    }else if ([dataOne isEqualToString:@"open seemoreImage" ]){
        
        NSMutableArray *imagearr=[[NSMutableArray alloc]init];
        //  photos=[[NSMutableArray alloc]init];
        for (int i=0; i<arraycontent.count; i++) {
            NSString *str=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,[arraycontent objectAtIndex:i]];
            MWPhoto *photo=[MWPhoto photoWithURL:[NSURL URLWithString:str]];
            //        photo.caption = @"Biblefaithfollow.SocialApp";
            [imagearr addObject:photo];
        }
        photos=[[[imagearr reverseObjectEnumerator] allObjects] mutableCopy];
        browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        
//        CGRect screenRect = [[UIScreen mainScreen] bounds];
//        CGFloat screenWidth = screenRect.size.width;
//        UIView  *paintView=[[UIView alloc]initWithFrame:CGRectMake(0,0, screenWidth, 50)];
//        [paintView setBackgroundColor:UIColorFromRGB(0x101010)];
//        
//        UIImageView *dot =[[UIImageView alloc] initWithFrame:CGRectMake(0,20.0,20,30)];
//        dot.image=[UIImage imageNamed:@"leftArrow"];
//        [paintView addSubview:dot];
//        
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button addTarget:self
//                   action:@selector(cancelphoto)
//         forControlEvents:UIControlEventTouchUpInside];
//        [button setTitle:@"Back" forState:UIControlStateNormal];
//        button.frame = CGRectMake(15.0,25.0, 50.0, 20.0);
//        [paintView addSubview:button];
//        [browser.view addSubview:paintView];
        
        BOOL displayActionButton = YES;
        BOOL displaySelectionButtons = NO;
        BOOL displayNavArrows = NO;
        BOOL enableGrid = YES;
        BOOL startOnGrid = NO;
        BOOL autoPlayOnAppear = NO;
        browser.displayActionButton = displayActionButton;
        //  browser.displayNavArrows = displayNavArrows;
        browser.displaySelectionButtons = displaySelectionButtons;
        browser.alwaysShowControls = NO;
        browser.zoomPhotosToFill = YES;
        browser.enableGrid = enableGrid;
        browser.startOnGrid = startOnGrid;
        browser.enableSwipeToDismiss = NO;
        //browser.autoPlayOnAppear = autoPlayOnAppear;
        [browser setCurrentPhotoIndex:0];
//        enableGrid = NO;
//        [self presentViewController:browser animated:NO completion:nil];

        [self.navigationController pushViewController:browser animated:YES];
    }
}



-(void)openphoto:(NSMutableArray*)imagearra influencerPhoto:(NSString *)checkInfluencer{
    NSMutableArray *imagearr=[[NSMutableArray alloc]init];
    
    if ([checkInfluencer isEqualToString:@"yes"]) {
        for (int i=0; i<imagearra.count; i++) {
            NSDictionary *dictPhoto=[[NSDictionary alloc]init];
            dictPhoto=[imagearra objectAtIndex:i];
            if ([dictPhoto isKindOfClass:[NSDictionary class]]) {
            NSString *strUrl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[dictPhoto valueForKey:@"img_path"]];
            MWPhoto *photo=[MWPhoto photoWithURL:[NSURL URLWithString:strUrl]];
            photo.caption = [dictPhoto valueForKey:@"caption"];
           [imagearr addObject:photo];
            }
            
            
        }
    }
    else{
        for (int i=0; i<imagearra.count; i++) {
            NSString *strUrl=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[imagearra objectAtIndex:i]];
            MWPhoto *photo=[MWPhoto photoWithURL:[NSURL URLWithString:strUrl]];
            [imagearr addObject:photo];
        }
    }
    
    photos=[[[imagearr reverseObjectEnumerator] allObjects] mutableCopy];
    browser = [[MWPhotoBrowser alloc] initWithDelegate:(id)self];
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGFloat screenWidth = screenRect.size.width;
//   UIView  *paintView=[[UIView alloc]initWithFrame:CGRectMake(0,0, screenWidth, 50)];
//    [paintView setBackgroundColor:UIColorFromRGB(0x101010)];
//    
//    UIImageView *dot =[[UIImageView alloc] initWithFrame:CGRectMake(0,20.0,20,30)];
//    dot.image=[UIImage imageNamed:@"leftArrow"];
//    [paintView addSubview:dot];
//
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button addTarget:self
//               action:@selector(cancelphoto)
//     forControlEvents:UIControlEventTouchUpInside];
//    [button setTitle:@"Back" forState:UIControlStateNormal];
//    button.frame = CGRectMake(15.0,25.0, 50.0, 20.0);
//    [paintView addSubview:button];
//    [browser.view addSubview:paintView];
//      
    BOOL displayActionButton = YES;
    BOOL displaySelectionButtons = NO;
    BOOL enableGrid = YES;
    BOOL startOnGrid = NO;
    browser.displayActionButton = displayActionButton;
    //  browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = NO;
    browser.zoomPhotosToFill = YES;
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = NO;
    //browser.autoPlayOnAppear = autoPlayOnAppear;
    [browser setCurrentPhotoIndex:0];
    enableGrid = NO;
//    [self presentViewController:browser animated:NO completion:nil];
    [self.navigationController pushViewController:browser animated:YES];
}
#pragma mark MWPhotoBrowser delegate

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
//        [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark CancelphotoMethod

-(void)cancelphoto{
    checkdismissPhoto=@"donedismiss";
    [self.navigationController popViewControllerAnimated:YES];

}


- (IBAction)cancelMethod:(id)sender {
//    [self.navigationController popToRootViewControllerAnimated:NO];

[self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)FollowMethod:(id)sender {
}
#pragma mark profile_tab1Method

- (IBAction)profile_tab1Method:(id)sender {
    _bottomView.hidden=NO;

    _postWallView.hidden=YES;
    _tableview.hidden=NO;
    _blueIndicatorViewProfile.hidden=NO;
    _postImge_tab2.image= [UIImage imageNamed:@"view_1x"];
    _profileImage_Tab1.image= [UIImage imageNamed:@"profile_selected-1"];
    _blueIndicatorPost.hidden=YES;
//    _postlabel_tab2.textColor=UIColorFromRGB(0x676767);
//    _profileLabel_Tab1.textColor=UIColorFromRGB(0x009AD9);
}

#pragma mark postMethod
- (IBAction)postMethod:(id)sender {
    _bottomView.hidden=YES;

    _postWallView.hidden=NO;
    _tableview.hidden=YES;
     _blueIndicatorViewProfile.hidden=YES;
    _blueIndicatorPost.hidden=NO;
    _postImge_tab2.image= [UIImage imageNamed:@"view_blue_1x"];
    _profileImage_Tab1.image= [UIImage imageNamed:@"profile_unselected-1"];
//    _postlabel_tab2.textColor=UIColorFromRGB(0x009AD9);
//    _profileLabel_Tab1.textColor=UIColorFromRGB(0x676767);
    
    _scrollView.contentOffset=CGPointZero;
    _scrollView.scrollEnabled=NO;
    _scrollView.contentSize=CGSizeMake(_scrollView.frame.size.width,0);
    
//    NSDictionary *eventLocation = @{@"wallfor": @"myself",@"userid":influencerDict[@"id"],@"friendWall":@"confirmedfriend",@"post_option":influencerDict[@"usersprofile"][@"post_option"],@"profile_picture":influencerDict[@"usersprofile"][@"profile_picture"]};
    
    // change here
    
     NSDictionary *eventLocation = @{@"wallfor": @"Mywall",
            @"userid":influencerDict[@"id"],
            @"friendWall":@"confirmedfriend",
            @"post_option":@"2"
                        };
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    wallvie=[[WallView alloc]initWithframe:CGRectMake(0, 0, screenHeight, screenWidth) andUserInfo:eventLocation];
    wallvie.frame= CGRectMake(0, 0,screenWidth,wallvie.frame.size.height);
    wallvie.hideheader=YES;
    wallvie.delegate1=self;
    [wallvie webservice_call];
    [_postWallView addSubview:wallvie];
}
@end
