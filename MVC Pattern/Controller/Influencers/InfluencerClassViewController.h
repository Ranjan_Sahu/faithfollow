//
//  InfluencerClassViewController.h
//  wireFrameSplash
//
//  Created by webwerks on 11/21/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfluencerClassViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property(strong,nonatomic)NSString *friendId;
-(void)viewFrndProfile :(NSString*)friend_id;
@end
