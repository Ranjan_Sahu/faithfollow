//
//  InfluencerProfileController.h
//  wireFrameSplash
//
//  Created by webwerks on 11/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WallView.h"

@interface InfluencerProfileController : UIViewController<MyFirstControllerDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(weak,nonatomic)IBOutlet UITableView *tableview;
- (IBAction)cancelMethod:(id)sender;
// profile influencer
@property (weak, nonatomic) IBOutlet UIImageView *inlurncerImgae;
@property (weak, nonatomic) IBOutlet UILabel *nameInluencer;
@property (weak, nonatomic) IBOutlet UILabel *influencerPost;
@property (weak, nonatomic) IBOutlet UIButton *followBTN;

- (IBAction)FollowMethod:(id)sender;
// tab1 details
@property (weak, nonatomic) IBOutlet UIView *ProfileView;
@property (weak, nonatomic) IBOutlet UIView *blueIndicatorViewProfile;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage_Tab1;
@property (weak, nonatomic) IBOutlet UILabel *profileLabel_Tab1;
- (IBAction)profile_tab1Method:(id)sender;
// tab2 details
@property (weak, nonatomic) IBOutlet UIView *editViewtab2View;
@property (weak, nonatomic) IBOutlet UIView *blueIndicatorPost;
@property (weak, nonatomic) IBOutlet UILabel *postlabel_tab2;
@property (weak, nonatomic) IBOutlet UIImageView *postImge_tab2;
@property (weak, nonatomic) IBOutlet UIButton *postBTN;
- (IBAction)postMethod:(id)sender;
//
@property(strong,nonatomic)NSMutableDictionary *influencerDict;
@property (weak, nonatomic) IBOutlet UIView *postWallView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;








@end
