//
//  InfluencerClassViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 11/21/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "InfluencerClassViewController.h"
#import "myinfluencerCell.h"
#import "InfluencerProfileController.h"
#import "UIImageView+WebCache.h"
#import "dummyTableViewCell2.h"
#import "Mixpanel.h"


@interface InfluencerClassViewController ()
{
    NSMutableArray *inluencerListArray,*messageArray;
    NSMutableDictionary *influencerdataDict;
    BOOL checkSeeThierPost;
     NSInteger countadd;
}
@end

@implementation InfluencerClassViewController
@synthesize tableview;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    inluencerListArray=[[NSMutableArray alloc]init];
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    self.tableview.tableFooterView = footerView;
     [self.tableview registerNib:[UINib nibWithNibName:@"myinfluencerCell" bundle:nil] forCellReuseIdentifier:@"myinfluencerCell"];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
       [self influencerMethod];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark InfluencerMethod

-(void)influencerMethod{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict=nil;
        dict = @{
                 @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:influencerList withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            inluencerListArray=[response valueForKey:@"data"];
                            NSInteger countcheck;
                            countcheck =[inluencerListArray count];
                            countadd=countcheck+1;
                            [self.tableview reloadData];
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableview reloadData];
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                        });
                    }
                }
            }
        } errorBlock:^(id error)
         {
        [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
         }];
        
    }
    
    
}



#pragma mark TableView Data Source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
//    if (countadd>0) {
//        return countadd;
//    }
//    else{
//        return 0;
//    }
    return inluencerListArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    myinfluencerCell *cell;
    
    if (cell==nil) {
        cell = (myinfluencerCell *)[tableview dequeueReusableCellWithIdentifier:@"myinfluencerCell"                                                                    forIndexPath:indexPath];
    }
    
    NSDictionary *DictData=[inluencerListArray objectAtIndex:indexPath.row];
    if ([DictData isKindOfClass:[NSDictionary class]]) {
        
        cell.profile_name.text=[NSString stringWithFormat:@"%@ %@",[DictData valueForKey:@"first_name"],[DictData valueForKey:@"last_name"]];
        
        NSString *followcountDisplaycheck=[NSString stringWithFormat:@"%@",[DictData valueForKey:@"display_followers_count"]];
        NSString *follow=[NSString stringWithFormat:@"%@",[DictData valueForKey:@"followers"]];
        
        if ([follow intValue]>[followcountDisplaycheck intValue]) {
            cell.countFollowers.hidden=NO;
            NSString *append=[NSString stringWithFormat:@"%@ %@",follow,@"Followers"];
            cell.countFollowers.text=append;
        }else{
            cell.countFollowers.hidden=YES;
        }
        
        NSString *frindStatus;
        frindStatus=[DictData valueForKey:@"friend_status"];
        cell.followBTN.tag=indexPath.row;
        
        if ([frindStatus isEqualToString:@"Follow"]) {
            [cell.followBTN setTitle:@"Follow" forState:UIControlStateNormal];
            //[cell.followBTN setBackgroundColor:UIColorFromRGB(0x3C95C2)];
            [cell.followBTN setBackgroundColor:[UIColor whiteColor]];
            cell.followBTN.layer.borderWidth  = 1.0f;
            cell.followBTN.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f].CGColor;
            [cell.followBTN setTitleColor:[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
            [cell.followBTN addTarget:self action:@selector(followInfluecer:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([frindStatus isEqualToString:@"Unfollow"]) {
            [cell.followBTN setTitle:@"Unfollow" forState:UIControlStateNormal];
            [cell.followBTN setBackgroundColor:[UIColor orangeColor]];
            [cell.followBTN setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            cell.followBTN.layer.borderWidth  = 1.0f;
            cell.followBTN.layer.borderColor = [UIColor orangeColor].CGColor;
            [cell.followBTN addTarget:self action:@selector(followInfluecer:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        if ([frindStatus isEqualToString:@"See their post"]) {
            //[cell.followBTN setBackgroundColor:UIColorFromRGB(0x0080FF)];
            [cell.followBTN setBackgroundColor:[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f]];
            cell.followBTN.layer.borderWidth  = 1.0f;
            //cell.followBTN.layer.borderColor = UIColorFromRGB(0x0080FF).CGColor;
            cell.followBTN.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f].CGColor;
            [cell.followBTN setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            cell.followBTN.alpha=0.9;
            [cell.followBTN setTitle:@"See their post" forState:UIControlStateNormal];
            [cell.followBTN addTarget:self action:@selector(followInfluecer:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        //        NSDictionary *userDictData=[DictData valueForKey:@"usersprofile"];
        if ([DictData isKindOfClass:[NSDictionary class]]) {
            cell.contentLBL.text=[DictData valueForKey:@"subtext"];
            NSString *strHtml = [DictData valueForKey:@"tags"];
            strHtml = [strHtml stringByReplacingOccurrencesOfString:@", "
                                                         withString:[NSString stringWithFormat:@" %@ ",@"\u2022"]];
            //cell.taghtmllbl.textColor = [UIColor darkGrayColor];
            cell.taghtmllbl.text=strHtml;
            NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[DictData valueForKey:@"profile_picture"]];
            NSURL *imageURL = [NSURL URLWithString:str];
            [cell.imageView_profile sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
        }
        // add tap geture to show profile of user
        cell.imageView_profile.tag=indexPath.row;
        cell.imageView_profile.userInteractionEnabled=YES;
        UITapGestureRecognizer *imagegallery_open = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(InfluencerProfile:)];
        [cell.imageView_profile addGestureRecognizer:imagegallery_open];
        //add tapgesture to show profile
        
        cell.profile_name.tag = indexPath.row;
        cell.profile_name.userInteractionEnabled=YES;
        UITapGestureRecognizer *profilename = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(InfluencerProfilename:)];
        [cell.profile_name addGestureRecognizer:profilename];
        [cell setNeedsUpdateConstraints];
        [cell updateConstraintsIfNeeded];
        [cell layoutIfNeeded];
        
    }
    
    return cell;
    
}


- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}

#pragma  mark FollowInfluecerMethod

-(void)followInfluecer:(UIButton*)sender{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *frind_id,*name_Influencer;
        CGPoint center= sender.center;
        CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableview];
        NSIndexPath *indexPathvalue = [self.tableview indexPathForRowAtPoint:rootViewPoint];
        NSMutableDictionary *arrayDict=[inluencerListArray objectAtIndex:indexPathvalue.row];
        if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
            frind_id=[arrayDict valueForKey:@"id"];
            name_Influencer=[NSString stringWithFormat:@"%@ %@",[arrayDict valueForKey:@"first_name"],[arrayDict valueForKey:@"last_name"]];
        }
        NSDictionary *dict=nil;
        NSString *methodname=nil;
        UIButton *btn = (UIButton*)sender;
        
        if ([btn.titleLabel.text isEqualToString:@"Unfollow"]) {
            dict = @{
                     @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                     @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                     @"friend_id" : frind_id,
                     
                     };
             methodname=unfollow;
        }
       else if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
           dict = @{
                    @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                    @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                    @"friend_id" : frind_id,
                    
                    };
            methodname=follow;
        }
        else{
            dict = @{
                     @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                     @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                     };
           methodname=singleUserPost;
            
        }
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodname withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
                                // Follow mixpanel
                                NSString *currentDate=[Constant1 createTimeStamp];
                                 [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Number of follows" by:@1];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last follow":currentDate}];
                                 [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Date of last follow":currentDate}];
                                
                                [ [AppDelegate sharedAppDelegate].mxPanel track:@"Follow"
                                    properties:@{@"Influencer?":[NSNumber numberWithBool:true],@"Date of last follow":currentDate,@"Following":name_Influencer}];
                                
                                
                                NSMutableDictionary *arrayDict=[inluencerListArray objectAtIndex:indexPathvalue.row];
                                if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
                            [arrayDict setObject:@"See their post" forKey:@"friend_status"];
                                }
                                
                            }
                            else if([btn.titleLabel.text isEqualToString:@"Unfollow"])
                            {
                                // Follow mixpanel
//                                NSString *currentDate=[Constant1 createTimeStamp];
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Number of follows" by:@-1];
                                
//                                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Date of last follow":currentDate}];
//                                
//                                [ [AppDelegate sharedAppDelegate].mxPanel track:@"Follow"
//                                                                     properties:@{@"Influencer?":[NSNumber numberWithBool:true],@"Date of last follow":currentDate,@"Following":name_Influencer}];
                                
                                NSMutableDictionary *arrayDict=[inluencerListArray objectAtIndex:indexPathvalue.row];
                                if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
                                    [arrayDict setObject:@"Follow" forKey:@"friend_status"];
                                }
                            }
                            else{
                                 NSMutableDictionary *arrayDictInfluencer=[inluencerListArray objectAtIndex:indexPathvalue.row];
                                InfluencerProfileController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                                allprofileView.influencerDict=arrayDictInfluencer;
                                [self.navigationController pushViewController:allprofileView animated:YES];
                                
                            }
                            [self reloadCellCom:indexPathvalue.row];
                        });
                        
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableview reloadData];
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
             [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
             
         }];
        
    }
    
}

#pragma mark reloadCellComMethod

-(void)reloadCellCom:(NSUInteger)cell_index{
    [self.tableview beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cell_index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableview reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [self.tableview endUpdates];
    
}
// taped image and profile name to  show profile of user
#pragma mark InfluencerProfileMethod

- (void)InfluencerProfile:(UITapGestureRecognizer*)sender {
    NSMutableDictionary *arrayProfileDict=[inluencerListArray objectAtIndex:sender.view.tag];
    NSString *frindID;
    if ([arrayProfileDict isKindOfClass:[NSDictionary class]]) {
        frindID=[arrayProfileDict valueForKey:@"id"];
        [self viewFrndProfile:frindID];
    }
}

- (void)InfluencerProfilename:(UITapGestureRecognizer*)sender {
    NSMutableDictionary *arrayProfileDict=[inluencerListArray objectAtIndex:sender.view.tag];
    NSString *frindID;
    if ([arrayProfileDict isKindOfClass:[NSDictionary class]]) {
        frindID=[arrayProfileDict valueForKey:@"id"];
        [self viewFrndProfile:frindID];
    }
}

#pragma mark viewFrndProfileMethod

-(void)viewFrndProfile :(NSString*)friend_id{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict = @{
                               @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"friend_id" : friend_id,
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if([response[@"status"]intValue ] ==1){
                    influencerdataDict=[response valueForKey:@"data"];
                    if ([influencerdataDict isKindOfClass:[NSMutableDictionary class]]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            InfluencerProfileController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
                            allprofileView.influencerDict=influencerdataDict;
                            [self.navigationController pushViewController:allprofileView animated:YES];
                        });
                    }
                }
                else
                {
                messageArray=[response valueForKey:@"message"];
                    if ([messageArray count]>0) {
                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        return;
   
                    }
              
                }
                
            }
        } errorBlock:^(id error)
         {
            [self.view makeToast:error duration:1.0 position:CSToastPositionCenter];
//             NSLog(@"error");
         }];
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


@end
