//
//  popGroupViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 10/14/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "popGroupViewController.h"
#import "VSDropdown.h"
#import "Mixpanel.h"

@interface popGroupViewController ()<VSDropdownDelegate>
{
    VSDropdown *_dropdown;
    NSString *checkMonth,*checkDay,*checkYear;
    NSMutableArray  *dateArray,*monthArray,*yearArray;
    BOOL yearPicked;
    NSData *date1;
    NSString *maleFemaleStr;
    NSString *errorMessage;
    NSString *checkgender;
}
@end

@implementation popGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _contentView.layer.cornerRadius=8.0f;
    [_contentView.layer setMasksToBounds:YES];
    
    _monthView.layer.borderWidth=1.0f;
    _monthView.layer.borderColor=[UIColor grayColor].CGColor;
    _monthView.userInteractionEnabled = YES;
    [_monthView.layer setMasksToBounds:YES];
    
    _dayView.layer.borderWidth=1.0f;
    _dayView.layer.borderColor=[UIColor grayColor].CGColor;
    _dayView.userInteractionEnabled = YES;
    [_dayView.layer setMasksToBounds:YES];
    
    _yearView.layer.borderWidth=1.0f;
    _yearView.layer.borderColor=[UIColor grayColor].CGColor;
    _yearView.userInteractionEnabled = YES;
    [_yearView.layer setMasksToBounds:YES];
    
//    [_femaleBTN setTitle:@"checkgender" forState:UIControlStateNormal];
//    [_maleBTN setTitle:@"checkgender" forState:UIControlStateNormal];

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    // dropdown
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    _dropdown.signupScreen=@"Yes";
    dateArray=[[NSMutableArray alloc]init];
    monthArray=[[NSMutableArray alloc]init];
    yearArray=[[NSMutableArray alloc]init];
    
    [monthArray addObject:@"Jan"];
    [monthArray addObject:@"Feb"];
    [monthArray addObject:@"Mar"];
    [monthArray addObject:@"Apr"];
    [monthArray addObject:@"May"];
    [monthArray addObject:@"June"];
    [monthArray addObject:@"July"];
    [monthArray addObject:@"Aug"];
    [monthArray addObject:@"Sept"];
    [monthArray addObject:@"Oct"];
    [monthArray addObject:@"Nov"];
    [monthArray addObject:@"Dec"];
    for (int i=1; i<32; i++) {
        [dateArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    [self yearlist];
    
    
    
}



-(void)yearlist{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    if (!appDelegate.isReachable) {
        
        return ;
    }
    
    else{
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"getyears" withParameters:nil withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        for (int k=0; k<[[response valueForKey:@"data"] count]; k++) {
                            [yearArray addObject:[NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] objectAtIndex:k]]];
                            
                        }
                        yearArray=[[[yearArray reverseObjectEnumerator] allObjects] mutableCopy];
                    }
                }
                
            }
            
        } errorBlock:^(id error)
         
         {
             
         }];
    }
}



- (IBAction)monthDropdown:(id)sender {
    checkMonth=@"M";
    [_dayBTN.titleLabel setTextAlignment: NSTextAlignmentLeft];
    [self showDropDownForButton:sender adContents:monthArray multipleSelection:NO];

    
}

- (IBAction)dayDropdown:(id)sender {
    checkDay=@"D";
    [_monthBTN.titleLabel setTextAlignment: NSTextAlignmentLeft];
    [self showDropDownForButton:sender adContents:dateArray multipleSelection:NO];
    
}

- (IBAction)yearDropdown:(id)sender {
    yearPicked=YES;
    checkYear=@"Y";
    [_yearBTN.titleLabel setTextAlignment: NSTextAlignmentLeft];
    [self showDropDownForButton:sender adContents:yearArray multipleSelection:NO];
}

- (IBAction)femaleMethod:(id)sender {
    _maleBTN.selected=NO;
    _femaleBTN.selected=YES;
    maleFemaleStr=@"0";
}

- (IBAction)maleMEthod:(id)sender {
    _maleBTN.selected=YES;
    _femaleBTN.selected=NO;
    maleFemaleStr=@"1";
    
}

-(IBAction)submit:(id)sender{
    
 errorMessage=[self btn_brthday];
if ([errorMessage isEqualToString:birthday] ||[errorMessage isEqualToString:birthday13]|| [errorMessage isEqualToString:selectgender] ) {
        [self.view makeToast:errorMessage  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *getpositionMonth;
        NSString *monthStr=_monthBTN.titleLabel.text;
        for (int i=0; i<[monthArray count]; i++) {
            NSString *getMont=[NSString stringWithFormat:@"%@",[monthArray objectAtIndex:i]];
            if ([monthStr isEqualToString:getMont]) {
                getpositionMonth=[NSString stringWithFormat:@"%d",i+1];
            }
        }
       
        [TenjinSDK sendEventWithName:@"confirmed_birthdate_and_gender"];
        [FBSDKAppEvents logEvent:@"confirmed_birthdate_and_gender"];
        [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"confirmed_birthdate_and_gender"]];
        
    NSString  *strDob = [NSString stringWithFormat: @"%@%@%@%@%@ ",_yearBTN.titleLabel.text,@"/",getpositionMonth,@"/",_dayBTN.titleLabel.text];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        NSDate *date = [dateFormatter dateFromString:strDob];
        NSString *finalDOb=[dateFormatter stringFromDate:date];
        
        // mix panel event
        NSString *emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
        
        if ([emailID isEqualToString:@""]||[emailID isEqualToString:@"(null)"]) {
            emailID=@"";
        }
        else{
            emailID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailID"]];
        }

        // mix panel event fired here
        
        NSString *accountCreate=[Constant1 createTimeStamp];
        
        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"$email":emailID,@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false]}];
        
            [[AppDelegate sharedAppDelegate].mxPanel track:@"Update Profile"
            properties:@{@"Email":emailID,@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false]}];
        
        [self.owner popUpSubmitClicked:finalDOb gender:maleFemaleStr];
    }
}

-(IBAction)closeBTN:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - VSDropdown Delegate methods.

-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents multipleSelection:(BOOL)multipleSelection
{
    [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"signup"];
    [_dropdown setupDropdownForView:sender];
    
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    
    if (_dropdown.allowMultipleSelection)
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:[[sender titleForState:UIControlStateNormal] componentsSeparatedByString:@";"]];
        
    }
    else
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
        
    }
    
}
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected
{
    UIButton *btn = (UIButton *)dropDown.dropDownView;
    
    NSString *allSelectedItems = nil;
    if (dropDown.selectedItems.count > 1)
    {
        allSelectedItems = [dropDown.selectedItems componentsJoinedByString:@";"];
        
    }
    else
    {
        allSelectedItems = [dropDown.selectedItems firstObject];
        
    }
    [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    if ([checkMonth isEqualToString:@"M"]) {
        checkMonth=allSelectedItems;
        [[NSUserDefaults standardUserDefaults] setObject:allSelectedItems forKey:@"month"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    if ([checkDay isEqualToString:@"D"]) {
        checkDay=allSelectedItems;
        
        [[NSUserDefaults standardUserDefaults] setObject:allSelectedItems forKey:@"day"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if ([checkYear isEqualToString:@"Y"]) {
        checkYear=allSelectedItems;
        [[NSUserDefaults standardUserDefaults] setObject:allSelectedItems forKey:@"year"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
      
    NSString *birthday=[NSString stringWithFormat:@"%@/%@/%@", [[NSUserDefaults standardUserDefaults]valueForKey:@"month"], [[NSUserDefaults standardUserDefaults]valueForKey:@"day"], [[NSUserDefaults standardUserDefaults]valueForKey:@"year"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    date1 = [formatter dateFromString:birthday];
    NSInteger ageBirth=[self age:date1];
    [[NSUserDefaults standardUserDefaults]setInteger:ageBirth forKey:@"agebirth"];
    
}

- (NSInteger)age:(NSDate *)dateOfBirth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *dateComponentsNow = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDateComponents *dateComponentsBirth = [calendar components:unitFlags fromDate:dateOfBirth];
    
    if (([dateComponentsNow month] < [dateComponentsBirth month]) ||
        (([dateComponentsNow month] == [dateComponentsBirth month]) && ([dateComponentsNow day] < [dateComponentsBirth day]))) {
        return [dateComponentsNow year] - [dateComponentsBirth year] - 1;
    } else {
        return [dateComponentsNow year] - [dateComponentsBirth year];
    }
}

-(NSString *)btn_brthday{
  
    if ([_dayBTN.titleLabel.text isEqualToString:@"Day"]|| ([_yearBTN.titleLabel.text isEqualToString:@"Year"])||([_monthBTN.titleLabel.text isEqualToString:@"Month"])) {
        errorMessage=birthday;
        return errorMessage;
        
    }else{
        NSInteger ageBirth=[self age:date1];
        [[NSUserDefaults standardUserDefaults]setInteger:ageBirth forKey:@"agebirth"];
        if (yearPicked==YES) {
            if (ageBirth<=12) {
                errorMessage=birthday13;
                return errorMessage;
            }
        }
    }
    
    
    if (_maleBTN.selected ==NO && _femaleBTN.selected==NO) {
        errorMessage=selectgender;
        return errorMessage;
    }
    
    return nil;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
