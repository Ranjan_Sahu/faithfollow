//
//  MyFirstControllerDelegate.h
//  wireFrameSplash
//
//  Created by Vikas on 25/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#ifndef MyFirstControllerDelegate_h
#define MyFirstControllerDelegate_h

@protocol MyFirstControllerDelegate<NSObject>
-(void) FunctionOne: (NSString*) dataOne;

@end

#endif /* MyFirstControllerDelegate_h */
