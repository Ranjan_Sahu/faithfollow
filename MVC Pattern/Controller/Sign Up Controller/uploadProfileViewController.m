//
//  uploadProfileViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 10/13/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "uploadProfileViewController.h"
#import "TutorialViewController.h"
#import "Mixpanel.h"
#import "Constant1.h"
@interface uploadProfileViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImage *chosenImage,*imgfacebook;
    NSString *imagestrdata;
}
@end

@implementation uploadProfileViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    _continueBTN.hidden=YES;
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
//    _imageUpload.layer.cornerRadius=_imageUpload.frame.size.width/2;
//    _imageUpload.layer.borderColor=[UIColor whiteColor].CGColor;
//    _imageUpload.layer.borderWidth=0.5f;
//    _imageUpload.clipsToBounds=YES;
    
    _takePhotoBTN.layer.borderWidth = 1.0f;
    _takePhotoBTN.layer.borderColor = [UIColor colorWithRed:238.0/255.0 green:242.0/255.0 blue:243.0/255.0 alpha:1.0].CGColor;
    
    if ([_fbimage isEqualToString:@"YESImage"]) {
        _SkipBTN.hidden=YES;
        _continueBTN.hidden=NO;
        NSURL *tempURL = [NSURL URLWithString:_imageSTRfromFB];
        NSData *tempData = [NSData dataWithContentsOfURL:tempURL];
        imgfacebook =[[UIImage alloc] initWithData:tempData];
        if (imgfacebook!=nil) {
            imgfacebook=[Constant1 compressImage:imgfacebook];
            _imageUpload.image=imgfacebook;
            imagestrdata=[self base64String:imgfacebook];
        }else{
            imagestrdata=@"";
        }
    }

}


- (IBAction)UploadPictureBTN:(id)sender {
    _SkipBTN.hidden=YES;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [TenjinSDK sendEventWithName:@"Register_ProfilePic_UploadPic_Tapped"];
    [FBSDKAppEvents logEvent:@"Register_ProfilePic_UploadPic_Tapped"];
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_ProfilePic_UploadPic_Tapped"]];
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)takecamareBTN:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [TenjinSDK sendEventWithName:@"Register_ProfilePic_TakePic_Tapped"];
    [FBSDKAppEvents logEvent:@"Register_ProfilePic_TakePic_Tapped"];
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_ProfilePic_TakePic_Tapped"]];
    [self presentViewController:picker animated:YES completion:NULL];

}

#pragma mark - imagePickerController Delegate methods.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //[picker dismissViewControllerAnimated:NO completion:NULL];
    [picker dismissViewControllerAnimated:NO completion:^{
        _SkipBTN.hidden=YES;
        chosenImage = info[UIImagePickerControllerEditedImage];
        if (chosenImage!=nil) {
            _imageUpload.image=chosenImage;
            _continueBTN.hidden=NO;
        }
    }];
   
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    _continueBTN.hidden=NO;
    _SkipBTN.hidden=YES;
    
}

- (IBAction)backAction:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)SkipMethod:(id)sender {
    
    [TenjinSDK sendEventWithName:@"Register_ProfilePic_Skip_Tapped"];
    [FBSDKAppEvents logEvent:@"Register_ProfilePic_Skip_Tapped"];
    UACustomEvent *event = [UACustomEvent eventWithName:@"Register_ProfilePic_Skip_Tapped"];
    [[UAirship shared].analytics addEvent:event];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else
    {
        NSDictionary *dict;
        dict = @{
                 @"first_name" :_firstnameStr,
                 @"last_name" :_lastnameStr,
                 @"email" :_emailStr,
                 @"password" :_passwordStr,
                 @"date_of_birth" :@"",
                 @"gender":@"",
                 @"device_type":@"IOS",
                 @"advertise_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"adID"]
                 };
        [[NSUserDefaults standardUserDefaults]setObject:_emailStr forKey:@"emailRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults]setObject:_passwordStr forKey:@"emailRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:signupQuick withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            [[NSUserDefaults standardUserDefaults]setObject:_emailStr forKey:@"emailID"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            
                            NSDictionary *dataDict=response[@"data"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"token"] forKey:@"api_token"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"id"] forKey:@"userid"];
                            [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            [TenjinSDK sendEventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [FBSDKAppEvents logEvent:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            UACustomEvent *event = [UACustomEvent eventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [[UAirship shared].analytics addEvent:event];
                            
                            //TutorialViewController *tut=[[TutorialViewController alloc]initWithNibName:@"TutorialViewController" bundle:nil];
                            
                            TutorialViewController *tut=[self.storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
                            tut.isFromFacebook = YES;
                            
                            NSString *accountCreate=[Constant1 createTimeStamp];
                            
                            
                                NSString *userid=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"id"]];
                                if ([userid isEqualToString:@"(null)"] || [userid isEqualToString:@""] )
                                {
                                    userid=@"";
                                }
                                else{
                                    userid=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"id"]];
                                }
                            NSDate *loginDate=[NSDate date];
                            [AppDelegate sharedAppDelegate].logindate=loginDate;
                             [[AppDelegate sharedAppDelegate].mxPanel identify:userid];
                            
//                            if ([AppDelegate sharedAppDelegate].tokendata!=nil) {
//                                [[AppDelegate sharedAppDelegate].mxPanel.people addPushDeviceToken:[AppDelegate sharedAppDelegate].tokendata];
//                                NSLog(@"mix device token %@",[AppDelegate sharedAppDelegate].tokendata);
//                            }
                            
                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                            NSData *myEncodedObject = [prefs objectForKey:@"mixdeviceToken"];
                            if (myEncodedObject!=nil) {
                                NSLog(@"mix device token %@",myEncodedObject);
                            [[AppDelegate sharedAppDelegate].mxPanel.people addPushDeviceToken:myEncodedObject];
                            }
                            
                            
                            
                            
                    [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total logins" by:@1];
                    
                  [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Email":_emailStr,@"Registration method":@"standard",@"Gender":@"U",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"$first_name":_firstnameStr,@"$last_name":_lastnameStr,@"Last Profile Update":accountCreate}];
                            
                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"$email":_emailStr,@"Registration method":@"standard",@"gender":@"U",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"$first_name":_firstnameStr,@"$last_name":_lastnameStr,@"Last Profile Update":accountCreate}];
                            
                    [[AppDelegate sharedAppDelegate].mxPanel track:@"Sign up"
                                properties:@{@"Email":_emailStr,@"Registration method":@"standard",@"Gender":@"U",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:false],@"Last Profile Update":accountCreate}];
                            
                            [self.navigationController pushViewController:tut animated:YES];
                            
                        }
                        else{
                            NSMutableArray *emailDict =nil;
                            emailDict=[response valueForKey:@"message"];
                            NSString *message=[emailDict objectAtIndex:0];
                            [self.view makeToast:message duration:1.0 position:CSToastPositionCenter];
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
    
   
}

-(void)fbloginContinue{
    
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else
    {
        
        NSString *deviceToken=[[NSUserDefaults standardUserDefaults]  objectForKey:@"apnsToken"];
        if (deviceToken.length==0) {
            deviceToken=@"32e06ecb6890317a192ad121e17ea9635d6d7ee16a5d6a8e550736a9a3d89015";
            [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"apnsToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }

        
    NSDictionary *facebookDict=[[NSUserDefaults standardUserDefaults]valueForKey:@"fbDictionay"];
        NSString *facebookId=[facebookDict valueForKey:@"id"];
        NSDictionary *dict;
        dict = @{
                 @"first_name" :_firstnameStr,
                 @"last_name" :_lastnameStr,
                 @"email" :_emailStr,
                 @"password" :@"",
                 @"date_of_birth" :@"",
                 @"image":imagestrdata,
                 @"gender":@"",
                 @"facebookId":facebookId,
                 @"device_type":@"IOS",
                 @"advertise_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"adID"],
                 @"device_token":deviceToken
                 };
        
        [[NSUserDefaults standardUserDefaults]setObject:_emailStr forKey:@"emailRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"passRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:signupfb withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            
                            [[NSUserDefaults standardUserDefaults]setObject:_emailStr forKey:@"emailID"];
                            [[NSUserDefaults standardUserDefaults] synchronize];

                            
                            [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"api_token"] forKey:@"api_token"];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"id"] forKey:@"userid"];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            NSString *is_register=[NSString stringWithFormat:@"%@",[response valueForKey:@"is_register"]];
                            
                            NSDate *loginDate=[NSDate date];
                            [AppDelegate sharedAppDelegate].logindate=loginDate;
                            
                            if ([is_register isEqualToString:@"1"]) {
                                [TenjinSDK sendEventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                                [FBSDKAppEvents logEvent:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                                UACustomEvent *event = [UACustomEvent eventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                                [[UAirship shared].analytics addEvent:event];
                                
                                //TutorialViewController *tut=[[TutorialViewController alloc]initWithNibName:@"TutorialViewController" bundle:nil];
                                
                                TutorialViewController *tut=[self.storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
                                tut.isFromFacebook = NO;
                                
                                NSString *accountCreate=[Constant1 createTimeStamp];
                                
                                NSString *userid=[NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
                                if ([userid isEqualToString:@"(null)"] || [userid isEqualToString:@""] )
                                {
                                    userid=@"";
                                }
                                else{
                                    userid=[NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
                                }
                                [[AppDelegate sharedAppDelegate].mxPanel identify:userid];
                                
                                
                                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                NSData *myEncodedObject = [prefs objectForKey:@"mixdeviceToken"];
                                if (myEncodedObject!=nil) {
                                    NSLog(@"mix device token %@",myEncodedObject);
                                    
                                    [[AppDelegate sharedAppDelegate].mxPanel.people addPushDeviceToken:myEncodedObject];
                                }
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total logins" by:@1];
                                
                                 [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Email":_emailStr,@"Registration method":@"facebook",@"Gender":@"",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:true],@"$first_name":_firstnameStr,@"$last_name":_lastnameStr,@"Last Profile Update":accountCreate}];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"$email":_emailStr,@"Registration method":@"facebook",@"gender":@"U",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:true],@"$first_name":_firstnameStr,@"$last_name":_lastnameStr,@"Last Profile Update":accountCreate}];

                                
                                [[AppDelegate sharedAppDelegate].mxPanel track:@"Sign up"
                                properties:@{@"Email":_emailStr,@"Registration method":@"facebook",@"Gender":@"U",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:true],@"Last Profile Update":accountCreate}];
                                
                                [self.navigationController pushViewController:tut animated:YES];
                                
                            }
                            else{
                                NSString *accountCreate=[Constant1 getCurrentDate];
                                NSString *userid=[NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
                                if ([userid isEqualToString:@"(null)"] || [userid isEqualToString:@""] )
                                {
                                    userid=@"";
                                }
                                else{
                                    userid=[NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
                                }
                                
                                [[AppDelegate sharedAppDelegate].mxPanel identify:userid];
                                if ([AppDelegate sharedAppDelegate].deviceTokenMix!=nil) {
                                    [[AppDelegate sharedAppDelegate].mxPanel.people addPushDeviceToken:[AppDelegate sharedAppDelegate].deviceTokenMix];
                                }
                                
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total logins" by:@1];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"$email":_emailStr,@"Registration method":@"standard",@"Gender":@"",@"Birthdate":@"",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:true]}];
                                
                                
                                [[AppDelegate sharedAppDelegate].mxPanel track:@"Sign up"
                                properties:@{@"Email":_emailStr,@"Registration method":@"standard",@"Gender":@"",@"Birthdate":@"",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:true],@"$email":_emailStr}];
                                
                                [appDelegate setRoots];
                            }
                            
                        }
                        else{
                            NSMutableArray *emailDict =nil;
                            emailDict=[response valueForKey:@"message"];
                            if ([emailDict count]>0) {
                            NSString *message=[emailDict objectAtIndex:0];
                            [self.view makeToast:message duration:1.0 position:CSToastPositionCenter];
                            }
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
}


- (IBAction)continueMethod:(id)sender {
    if ([_fbimage isEqualToString:@"YESImage"]) {
        [self fbloginContinue];
        _fbimage=@"";
        // change here
        return;
    }
    else{
    NSString *imagestr=nil;
    if (chosenImage!=nil) {
        chosenImage=[Constant1 compressImage:chosenImage];
        imagestr=[self base64String:chosenImage];
    }
    AppDelegate *appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else
    {
        NSDictionary *dict;
        dict = @{
                 @"first_name" :_firstnameStr,
                 @"last_name" :_lastnameStr,
                 @"email" :_emailStr,
                 @"password" :_passwordStr,
                 @"date_of_birth" :@"",
                 @"image":imagestr,
                 @"gender":@"",
                 @"device_type":@"IOS",
                 @"advertise_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"adID"]
                 };
        
        [[NSUserDefaults standardUserDefaults]setObject:_emailStr forKey:@"emailRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults]setObject:_emailStr forKey:@"passRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:signupQuick withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            [[NSUserDefaults standardUserDefaults]setObject:_emailStr forKey:@"emailID"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            NSDictionary *dataDict=response[@"data"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"token"] forKey:@"api_token"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"id"] forKey:@"userid"];
                            [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            [TenjinSDK sendEventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [FBSDKAppEvents logEvent:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            UACustomEvent *event = [UACustomEvent eventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [[UAirship shared].analytics addEvent:event];
                            
                            //TutorialViewController *tut=[[TutorialViewController alloc]initWithNibName:@"TutorialViewController" bundle:nil];
                            TutorialViewController *tut=[self.storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
                            tut.isFromFacebook = NO;
                            
                            NSString *accountCreate=[Constant1 createTimeStamp];
                            
                            
                            NSString *userid=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"id"]];
                            if ([userid isEqualToString:@"(null)"] || [userid isEqualToString:@""] )
                            {
                                userid=@"0";
                            }
                            else{
                                userid=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
                            }
                            NSDate *loginDate=[NSDate date];
                            [AppDelegate sharedAppDelegate].logindate=loginDate;
                            [[AppDelegate sharedAppDelegate].mxPanel identify:userid];
                            
                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                            NSData *myEncodedObject = [prefs objectForKey:@"mixdeviceToken"];
                            if (myEncodedObject!=nil) {
                                NSLog(@"mix device token %@",myEncodedObject);
                            [[AppDelegate sharedAppDelegate].mxPanel.people addPushDeviceToken:myEncodedObject];
                            }
                            
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total logins" by:@1];
                            
                            
                             [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Email":_emailStr,@"Registration method":@"standard",@"Gender":@"U",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:true],@"$first_name":_firstnameStr,@"$last_name":_lastnameStr,@"Last Profile Update":accountCreate}];
                            
                            
                            [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"$email":_emailStr,@"Registration method":@"standard",@"gender":@"U",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:true],@"$first_name":_firstnameStr,@"$last_name":_lastnameStr,@"Last Profile Update":accountCreate}];

                            
                        
                    [[AppDelegate sharedAppDelegate].mxPanel track:@"Sign up"
                        properties:@{@"Email":_emailStr,@"Registration method":@"standard",@"Gender":@"U",@"Account created date":accountCreate,@"Uploaded Picture":[NSNumber numberWithBool:true],@"Last Profile Update":accountCreate}];
                            
                            [self.navigationController pushViewController:tut animated:YES];
                          
                        }
                        else{
                            NSMutableArray *emailDict =nil;
                            emailDict=[response valueForKey:@"message"];
                            if ([emailDict count]>0) {
                            NSString *message=[emailDict objectAtIndex:0];
                            [self.view makeToast:message duration:1.0 position:CSToastPositionCenter];
                        }
                        
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
   
    }
    
}
-(NSString*)base64String:(UIImage*)image{
    NSString *base64EncodeStr = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    base64EncodeStr = [base64EncodeStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    return base64EncodeStr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
