//
//  CustomPageViewController.m
//  PageViewController

#import "CustomPagerViewController.h"
#import "HomeViewController.h"
#import "Constant1.h"
#import "NSString+StringValidation.h"

@interface CustomPagerViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>{
    CGRect screenRect;
    int pagenumber;
    
    UIView *vs;
    NSMutableArray *dateArray,*monthArray,*yearArray;
    UIButton* male_btn,* female_btn;
}
@end

@implementation CustomPagerViewController
- (void)viewDidLoad
{
    // Do any additional setup after loading the view, typically from a nib.
    [super viewDidLoad];
   
    self.navigationController.navigationBar.topItem.title =@"";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"signup"];
    if (vs) {
        [vs removeFromSuperview];
    }
}


-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title =@"";
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"SIGN UP";
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [[AppDelegate sharedAppDelegate] removeadd];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor colorWithRed:0.0784 green:0.5294 blue:0.8784 alpha:1.0];
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:0.0784 green:0.5294 blue:0.8784 alpha:1.0];
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    vs = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];
    [vs setBackgroundColor:[UIColor colorWithRed:0.0784 green:0.5294 blue:0.8784 alpha:1.0]];
    [[[UIApplication sharedApplication] keyWindow] addSubview:vs];

    
}

@end
