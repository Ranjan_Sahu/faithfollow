//
//  WallView.m
//  wireFrameSplash
//
//  Created by Vikas on 25/05/16.
//  Copyright © 2016 home. All rights reserved.
//
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]


#import "MLKMenuPopover.h"
#import "FeedViewController.h"
#import "FeedTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CustomCommentView.h"
#import "StatusViewController.h"
#import "FeedIMageTableViewCell.h"
#import "FeedpostTableViewCell.h"
#import "WallView.h"
#import "HeaderView.h"
#import "FeedwithImagePostTableViewCell.h"
#import "MNMBottomPullToRefreshManager.h"
#import "FriendsProfileViewController.h"
#import "VSDropdown.h"
#import "lblClass.h"
#import "AddvertiseTableViewCell.h"
#import <GoogleMobileAds/GADBannerView.h>
#import "NeosPopup.h"
#import <GoogleMobileAds/GADNativeAd.h>
#import <GoogleMobileAds/GADNativeExpressAdView.h>
#import "Constant1.h"
#import "SuggestedGroupTableViewCell.h"
#import "DetailMygroupViewController1.h"
#import "AdcellfeedTableViewCell.h"
#import "Mixpanel.h"
#import "PPLabel.h"


#define TimeStamp [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]
#define SKY_BLUE_COLOR [UIColor colorWithRed:0.1137 green:0.4824 blue:0.8065 alpha:1.0]

@interface WallView ()<UITabBarDelegate,UITextViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,MNMBottomPullToRefreshManagerClient,VSDropdownDelegate,MLKMenuPopoverDelegate,PPLabelDelegate>
{
    NSString *totalcountPrayer,*checkYouprayed;
    GADBannerView  *bottomAbMob;
    GADNativeExpressAdView  *nativeAbMob;
    NSDataDetector *detector,*detector1;
    UIFont *postfont;
    CGRect MENU_POPOVER_FRAME;
    CGFloat widthofName,widthofpost;
    NSInteger selectedRow;
    UIView *lineView ;
    NSMutableArray *arrFeeds;
    NSMutableArray *messageArray;
    NSString *comments,*checkpostTypeTittle,*checkcountZeroinfluencer;
    UIAlertView *message;
    NSDictionary *get_total_commnet;
    BOOL comments_their;
    NSArray *arr;
    FeedpostTableViewCell *feed_cell;
    FeedwithImagePostTableViewCell *feedImage_cell;
    NSMutableArray *arrWallFeeds,*inluencerListArrayFeed;
    NSMutableArray *all_content;
    BOOL keyboard,liked_call;
    NSDictionary *userInfo;
    NSUInteger cellindex;
    NSString *sendComments_method;
    NSMutableArray *all_comments;
    NSDictionary *commment_info;
    int tagf,countcheck;
    int pagenumber,total_pagenumber;
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
    BOOL commentOpen;
    BOOL addcommentsV;
    BOOL ValidUrl;
    CGFloat heightofView;
    CGFloat label_personViewHeight;
    CGFloat see_morecomment;
    NSMutableArray *postimageArray,*groupcountArray;
    BOOL myselfwall,downpullRefresh,checkScrollcontent;
    NSString *textstring,*filterType;
    NSArray *paths;
    NSString *documentsDirectoryPath,*filePath,*otheruserProfile;
    VSDropdown *_dropdown;
    NSString *userID,*postType,*friend_status,*hprofile_picture;
    lblClass *lbl;
    int perpage;
    int addval;
    int countLineOfLable;
    UIView* mainpop,*pop,*addvertiseView;
    NeosPopup *nepopup;
    NSString *postcheck;
    CGFloat imageHeight;
    CGFloat imageWidth;
    CGFloat imageviewwidth,imageviewheight;
    UILabel *prayerTotalCountStr1;
    NSString *checkinclencerImageGlobal;
    NSString *token;
    NSMutableArray *adheaderArray;
    NSMutableDictionary *tableImageDict;
}
@property (nonatomic) NSInteger page;
@property (nonatomic, strong) NSMutableArray *imageHeights;
@property (nonatomic, strong) NSMutableArray *imageURLs;
@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property(nonatomic,strong) MLKMenuPopover *menuPopover2;
@property(strong,nonatomic) NSMutableArray *commentsarr;
@property(nonatomic,strong) NSArray *menuItems;
@property(nonatomic, strong) NSArray* matches;
@end

@implementation WallView
#define  kCellName @"FeedpostTableViewCell"
#define  kCellName1 @"FeedwithImagePostTableViewCell"
#define  kCellName2 @"AddvertiseTableViewCell"
@synthesize commentsarr,rateViewFeed;
@synthesize arrWallFeeds,checkviewwillApear,suggetedArray,hideheader;

-(id) initWithframe:(CGRect)frame andUserInfo:(NSDictionary*)userinfo{
    self = [super init];
    if(self) {
        AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [self notificationcount];
        label_personViewHeight=0;
        heightofView=0;
        see_morecomment=0;
        getVar.headerDict=nil;
        userID=nil;
         self= [[[NSBundle mainBundle] loadNibNamed:@"WallView" owner:self options:nil] objectAtIndex:0];
        userInfo=userinfo;
        [self.tableViewOutlet   registerNib:[UINib nibWithNibName:kCellName bundle:nil] forCellReuseIdentifier:kCellName];
        [self.tableViewOutlet   registerNib:[UINib nibWithNibName:kCellName1 bundle:nil] forCellReuseIdentifier:kCellName1];
        [self.tableViewOutlet   registerNib:[UINib nibWithNibName:kCellName2 bundle:nil] forCellReuseIdentifier:kCellName2];
        [self.tableViewOutlet registerNib:[UINib nibWithNibName:@"SuggestedGroupTableViewCell" bundle:nil] forCellReuseIdentifier:@"SuggestedGroupTableViewCell"];
         [self.tableViewOutlet registerNib:[UINib nibWithNibName:@"AdcellfeedTableViewCell" bundle:nil] forCellReuseIdentifier:@"AdcellfeedTableViewCell"];
        selectedRow=-1;
        [self.tableViewOutlet setContentOffset:CGPointMake(0.0f, 0.0f) animated:YES];
        [self.tableViewOutlet setContentInset:UIEdgeInsetsZero];
        self.tableViewOutlet.contentOffset = CGPointZero;
        _tableViewOutlet.delegate=self;
        _tableViewOutlet.dataSource=self;
        //_tableViewOutlet.backgroundColor=[UIColor colorWithRed:216/255.0f green:214/255.0f  blue:214/255.0f  alpha:1.0];
        _tableViewOutlet.backgroundColor=[UIColor colorWithRed:238/255.0f green:242/255.0f  blue:243/255.0f  alpha:1.0];
        _tableViewOutlet.frame=frame;
        checkScrollcontent=NO;
        ValidUrl=NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                        selector:@selector(posttablereload:)
                                        name:@"reloadtable"
                                        object:nil];
               hprofile_picture=nil;
        suggetedArray=[[NSMutableArray alloc]init];
        inluencerListArrayFeed=[[NSMutableArray alloc]init];
        //    textstring= @"And whenever the answer, And whenever the answer And whenever the answer             And whenever the answerAnd whenever the answer  And whenever the answer  And whenever the answer whenever the ansdonesdfsdfsdfsfdsdfsdfsfdssdfsfdsdsdfsfd funfhjfhjfjvbbjkuihjkhj gun hgfghfghfhjfhjgfjhfgjhg shun   mnb hgfkjhmnbnb run";
        //  textstring= @"And whenever the answer, n";
        textstring= @"And whenever the answer, And whenever the answer And whenever the answer             And whenever the answerAnd whenever the answer  And whenever the answer  And whenever the answer alvin";
        // HIDE PARTICUAR TABLEVIEW HEADER
        self.hideheader=NO;
        self.influencerimageValue=NO;
         self.checkPrayerCount=NO;
        paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectoryPath = [paths objectAtIndex:0];
        filePath = [documentsDirectoryPath stringByAppendingPathComponent:@"appData"];
        _dropdown = [[VSDropdown alloc]initWithDelegate:self];
        [_dropdown setAdoptParentTheme:YES];
        [_dropdown setShouldSortItems:YES];
        self.refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl.backgroundColor = [self colorFromHexString:@"#d8d6d6"];
        self.refreshControl.tintColor = [UIColor whiteColor];
        [self.refreshControl addTarget:self
                      action:@selector(refreshView:)
                      forControlEvents:UIControlEventValueChanged];
        [self.tableViewOutlet addSubview:self.refreshControl];
        arrWallFeeds=[[NSMutableArray alloc]init];
        filterType=@"4";
        heightofView=204;
        pagenumber=1;
        postfont=[UIFont fontWithName:@"Avenir-Medium" size:15];
        AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
        self.imageArray=[[NSMutableArray alloc]init];
        appDelegate.isheaderAD=@"0";
        appDelegate.postimagecheckNotnull=NO;
        appDelegate.headeradcheck=NO;
        [self imageProfile];
//        [self notification_alert];
        
        [self layoutIfNeeded];
    }
    return(self);
}


#pragma mark -notification

-(void) notification_alert{
    [NSTimer scheduledTimerWithTimeInterval:20.0
                                 target:self
                                   selector:@selector(notificationcount)
                                   userInfo:nil
                                    repeats:YES];
}


#pragma mark -notificationcount

-(void)notificationcount{
    
    AppDelegate *appDelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        return;
    }
    else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSDictionary *dict = @{
                @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                };
    [[WebServiceHelper sharedInstance] call_notificationPostDataWithMethod:getnotificationcount withParameters:dict withHud:NO success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            NSString *countnoti=[NSString stringWithFormat:@"%@",[response valueForKey:@"data"]];
                            if ([countnoti  intValue]>0) {
                                if ([countnoti  intValue]>99) {
                                    appDelegate.notificationLBL.text=@"99+";
                                }
                                appDelegate.notificationLBL.text=[NSString stringWithFormat:@"%@",[response valueForKey:@"data"]];
                                appDelegate.notificationView.hidden=NO;
                                appDelegate.notificationLBL.hidden=NO;
                            }
                            else{
                                appDelegate.notificationView.hidden=YES;
                                appDelegate.notificationLBL.hidden=YES;
                            }
                        });
                    }
                }
            } errorBlock:^(id error)
             {
             }];});
    }
    
}

#pragma mark Header Image Method
-(void)imageProfile{
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        if (message) {
        }else{
            message=nil;
            [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
            return;
        }
    }
    else{
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"getimage" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]])  {
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        hprofile_picture =[response valueForKey:@"image"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_tableViewOutlet reloadData];
                        });
                    }
                }
                else{
                    
                }
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
    }
    
}


#pragma  mark GroupPostReloadWallTable Method
-(void)posttablereload:(NSNotification *) notification{
    pagenumber=1;
    [self  pullUptorefresh];
}
-(void)refreshView:(UIRefreshControl *)refresh {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.refreshControl endRefreshing];
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
        
    }else{
        pagenumber=1;
        [self  pullUptorefresh];
    }
}

#pragma mark reload wallView by Praise and Pray method

- (void) reloadcellwallview:(NSDictionary *) comment_dict{
    NSString* cellnumber=comment_dict[@"cellindex"];
    NSMutableDictionary *dict=[arrWallFeeds objectAtIndex:[cellnumber integerValue]];
    
    if ([dict isKindOfClass:[NSDictionary class]]) {
        NSString *walllike=[NSString stringWithFormat:@"%@",[dict objectForKey:@"is_like"]];
        NSString *seemoreLike=[NSString stringWithFormat:@"%@",[comment_dict objectForKey:@"likefromseemore"]];
        NSString *countvalue=[NSString stringWithFormat:@"%@",[comment_dict objectForKey:@"countcheck"]];
        NSMutableDictionary *gettotalpraised=[dict valueForKey:@"get_total_praised"];
        if ([gettotalpraised isKindOfClass:[NSNull class]]) {
            if (![walllike isEqualToString:seemoreLike]) {
                if ([seemoreLike isEqualToString:@"0"]) {
                    [dict setObject:@"0" forKey:@"is_like"];
                    [arrWallFeeds replaceObjectAtIndex:[cellnumber integerValue] withObject:dict];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[cellnumber integerValue] inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [self.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                }
                else{
                    gettotalpraised=[[NSMutableDictionary alloc]init];
                    [gettotalpraised setObject:@"1" forKey:@"is_like"];
                    [gettotalpraised setObject:countvalue forKey:@"count"];
                    [dict setObject:gettotalpraised forKey:@"get_total_praised"];
                    [dict setObject:@"1" forKey:@"is_like"];
                    [arrWallFeeds replaceObjectAtIndex:[cellnumber integerValue] withObject:dict];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[cellnumber integerValue] inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [self.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                }
            }
        }
        else{
            if (![walllike isEqualToString:seemoreLike]) {
                if ([seemoreLike isEqualToString:@"0"]) {
                    [dict setObject:@"0" forKey:@"is_like"];
                    [arrWallFeeds replaceObjectAtIndex:[cellnumber integerValue] withObject:dict];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[cellnumber integerValue] inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [self.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                }
                else{
                    gettotalpraised=[[NSMutableDictionary alloc]init];
                    [gettotalpraised setObject:@"1" forKey:@"is_like"];
                    [gettotalpraised setObject:countvalue forKey:@"count"];
                    [dict setObject:gettotalpraised forKey:@"get_total_praised"];
                    [dict setObject:@"1" forKey:@"is_like"];
                    [arrWallFeeds replaceObjectAtIndex:[cellnumber integerValue] withObject:dict];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[cellnumber integerValue] inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [self.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                }
            }
        }
    }
    
}




-(void)updateCommentfromProfile:(NSDictionary*)comment_dict{
    
    NSMutableArray*comment_arr=comment_dict[@"commenst_Arr"];
    NSString* cellnumber=comment_dict[@"cellindex"];
    [arrWallFeeds objectAtIndex:[cellnumber integerValue] ][@"latest_comments"]=comment_arr;
    if (  [[arrWallFeeds objectAtIndex:[cellnumber integerValue]] [@"get_total_commnets"] isKindOfClass:[NSNull class]]) {
        NSDictionary *dict=@{@"count":[NSString stringWithFormat:@"%@",comment_dict[@"countofcomments"]]
                             };
        [[arrWallFeeds objectAtIndex:[cellnumber integerValue]] setObject:dict forKey:@"get_total_commnets"];
        
    }else{
        NSDictionary *dict=@{@"count":[NSString stringWithFormat:@"%@",comment_dict[@"countofcomments"]]
                             };
        [[arrWallFeeds objectAtIndex:[cellnumber integerValue]] setObject:dict forKey:@"get_total_commnets"];
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[cellnumber integerValue] inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}


#pragma mark web service Method

-(void)webservice_call{
    if(arrWallFeeds)
    {
        [arrWallFeeds removeAllObjects];
    }
    
    if(self.imageArray)
    {
         [self.imageArray removeAllObjects];
    }
    
    suggetedArray=nil;

    if ([userInfo[@"wallfor"]isEqualToString:@"Mywall"]) {
        _upperView.layer.borderWidth=0.5f;
        _upperView.layer.borderColor=[UIColor grayColor].CGColor;
        _status_btn.layer.borderColor=[UIColor blackColor].CGColor;
        _status_btn.layer.borderWidth=0.5f;
        commentsarr=[[NSMutableArray alloc]init];
        [_textViewTXT resignFirstResponder];
        pagenumber=1;
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        if (!appDelegate.isReachable) {
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                NSData *data = [NSData dataWithContentsOfFile:filePath];
                NSDictionary *savedData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                if ([savedData objectForKey:@"backuparray"] != nil) {
                    arrWallFeeds=[savedData objectForKey:@"backuparray"];
                    for (int i=0; i<[arrWallFeeds count]; i++) {
                        [self.imageArray addObject:[NSNull null]];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        userID=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
                        [_tableViewOutlet reloadData];
                    });
                }
            }
        }else{
            [self initial_FeedserviceCall:1 andfiletype:@"4"];
            userID=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            commentOpen=YES;
//            [_tableViewOutlet reloadData];
        }
    }else if ([userInfo[@"wallfor"]isEqualToString:@"myself"]) {
        _upperView.layer.borderWidth=0.5f;
        _upperView.layer.borderColor=[UIColor grayColor].CGColor;
        _status_btn.layer.borderColor=[UIColor blackColor].CGColor;
        _status_btn.layer.borderWidth=0.5f;
        commentsarr=[[NSMutableArray alloc]init];
        [_textViewTXT resignFirstResponder];
        pagenumber=1;
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (!appDelegate.isReachable) {
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                NSData *data = [NSData dataWithContentsOfFile:filePath];
                NSDictionary *savedData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                if ([savedData objectForKey:@"backuparray"] != nil) {
                    arrWallFeeds=[savedData objectForKey:@"backuparray"];
                    for (int i=0; i<[arrWallFeeds count]; i++) {
                        [self.imageArray addObject:[NSNull null]];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        userID=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
                        [_tableViewOutlet reloadData];
                    });
                }
            }
        }else{
            if ([userInfo[@"friendWall"]isEqualToString:@"confirmedfriend"]){

                otheruserProfile=@"removesuggestedArray";
                myselfwall=YES;
                userID=userInfo[@"userid"];
                int postVal=[userInfo[@"post_option"] intValue];
                if (postVal==3) {
                    postType=userInfo[@"post_option"];
                }else   if (postVal==1||postVal==2) {
                    hprofile_picture=userInfo[@"profile_picture"];
                }
            }else if ([userInfo[@"friendWall"]isEqualToString:@"commonFriend"]){
                otheruserProfile=@"removesuggestedArray";
                myselfwall=YES;
                friend_status=userInfo[@"friend_status"];
                userID=userInfo[@"userid"];
                int postVal=[userInfo[@"post_option"] intValue];
                if (postVal==1) {
                    hprofile_picture=userInfo[@"profile_picture"];
                }
                // chnges here unfriend to Unfollow
                if ([friend_status  isEqualToString:@"Unfollow"]) {
                    if (postVal==3) {
                        postType=userInfo[@"post_option"];
                    }
                }else{
                    postType=userInfo[@"post_option"];
                    if (postVal==2) {
                        postType=@"3";
                    }
                }
                
            }else{
                userID=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            }
            [self initial_FeedserviceCall:1 andfiletype:@"4"];
//            [_tableViewOutlet reloadData];
        }
    }
}

#pragma mark TableView Data Source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arrWallFeeds count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.menuPopover2 dismissMenuPopover];
    id dataresponse=[arrWallFeeds objectAtIndex:indexPath.row];
    
    if (indexPath.row==0 ) {
        if ([dataresponse isKindOfClass:[NSString class]] && [dataresponse isEqualToString:@"Group"]) {
            {
            SuggestedGroupTableViewCell  *cell=nil;
            if(cell==nil)
            {
                SuggestedGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SuggestedGroupTableViewCell"];
                if ([suggetedArray count]>0) {
                    if ([suggetedArray count]==1) {
                        cell.mainViewHeight.constant=104;
                    }
                    else if([suggetedArray count]==2)
{
                        cell.mainViewHeight.constant=165;
                    }
                    else{
                        cell.mainViewHeight.constant=226;
                    }
                }
                [cell setData:suggetedArray];
                [cell setNeedsUpdateConstraints];
                [cell updateConstraintsIfNeeded];
                [cell layoutIfNeeded];
                cell.backgroundColor = [self colorFromHexString:@"#EEF2F3" ];
                return cell;
            }
            }
        }
        else{
            if ([[arrWallFeeds objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]){
                NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
                NSMutableArray *attachment=dict[@"get_attachments"];
                if (attachment.count==0) {
                    FeedpostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName forIndexPath:indexPath];
                    //cell.backgroundColor = [self colorFromHexString:@"#d8d6d6" ];
                    cell.backgroundColor = [self colorFromHexString:@"#EEF2F3" ];
                    cell.labelPP.delegate=self;
                    cell.pplableComment1.delegate=self;
                    cell.pplableComment2.delegate=self;
                    cell.pplableComment3.delegate=self;
                    [self commonsetUpCell:nil andfeedpost:cell  andadvertisecell:nil atIndexPath:indexPath ];
                    return cell;
                }else if(attachment.count>0){
                    FeedwithImagePostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName1 forIndexPath:indexPath];
                    //cell.backgroundColor = [self colorFromHexString:@"#d8d6d6" ];
                    cell.backgroundColor = [self colorFromHexString:@"#EEF2F3" ];
                    cell.labelPP.delegate=self;
                    [self commonsetUpCell:cell andfeedpost:nil andadvertisecell:nil atIndexPath:indexPath];
                    return cell;
                }
            }else
            {
                AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                AdcellfeedTableViewCell *cell =nil;
                if ([appDelegate.notfoundadsArray containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"AdcellfeedTableViewCell" forIndexPath:indexPath];
                    cell.hidden=YES;
                    return cell;
                }
                else{
                    if (cell==nil) {
                        cell=  [tableView dequeueReusableCellWithIdentifier:@"AdcellfeedTableViewCell" forIndexPath:indexPath];
                    }
                    
                    //cell.backgroundColor = [self colorFromHexString:@"#d8d6d6" ];
                    cell.backgroundColor = [self colorFromHexString:@"#EEF2F3" ];
                    [self commonsetUpCell:nil andfeedpost:nil   andadvertisecell:cell atIndexPath:indexPath];
                    return cell;
                }
            }
            }
        }
       else
         {
        if ([[arrWallFeeds objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]){
            NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
            NSMutableArray *attachment=dict[@"get_attachments"];
            if (attachment.count==0) {
                FeedpostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName forIndexPath:indexPath];
                //cell.backgroundColor = [self colorFromHexString:@"#d8d6d6" ];
                cell.backgroundColor = [self colorFromHexString:@"#EEF2F3" ];
                cell.labelPP.delegate=self;
                cell.pplableComment1.delegate=self;
                cell.pplableComment2.delegate=self;
                cell.pplableComment3.delegate=self;
                [self commonsetUpCell:nil andfeedpost:cell  andadvertisecell:nil atIndexPath:indexPath ];
                return cell;
            }else if(attachment.count>0){
                FeedwithImagePostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellName1 forIndexPath:indexPath];
                cell.labelPP.delegate=self;
                //cell.backgroundColor = [self colorFromHexString:@"#d8d6d6" ];
                cell.backgroundColor = [self colorFromHexString:@"#EEF2F3" ];
                [self commonsetUpCell:cell andfeedpost:nil andadvertisecell:nil atIndexPath:indexPath];
                return cell;
            }
        }else{
            AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            if ([appDelegate.notfoundadsArray containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                AdcellfeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AdcellfeedTableViewCell" forIndexPath:indexPath];
                cell.hidden=YES;
                return cell;
            }
            else{
                AdcellfeedTableViewCell *cell =nil;
                if (cell==nil) {
                cell=  [tableView dequeueReusableCellWithIdentifier:@"AdcellfeedTableViewCell" forIndexPath:indexPath];
                }
                //cell.backgroundColor = [self colorFromHexString:@"#d8d6d6" ];
                cell.backgroundColor = [self colorFromHexString:@"#EEF2F3" ];
                [self commonsetUpCell:nil andfeedpost:nil   andadvertisecell:cell atIndexPath:indexPath];
                return cell;
            }
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (self.hideheader==YES) {
        return 0;
    }
    else{
        if ([getVar.headerDict count]>0 ) {
            CGFloat widthad,heightad;
            NSString *sizeAD=[getVar.headerDict objectForKey:@"slot_size"];
            if (![sizeAD isEqualToString:@""]) {
                NSString *widthadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:0] ;
                widthad = (CGFloat)[widthadStr floatValue];
                NSString *heightadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:1];
                heightad = (CGFloat)[heightadStr floatValue];
                if ( getVar.headeradcheck==YES) {
                
                    return 163+32;
                }
                return heightad+161+32;
            }
            else{
                return 213+32;
            }
        }
        else{
            return 163+32;
        }
        return 0;
  
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id dataresponse=[arrWallFeeds objectAtIndex:indexPath.row];
    float postCommentHeight=0;

    if (indexPath.row==0 ) {
        
        if ([dataresponse isKindOfClass:[NSString class]]) {
            CGFloat hieghtcell;
            
            if([dataresponse isEqualToString:@"Group"])
            {
                if ([suggetedArray count]==1) {
                    hieghtcell=114;
                }
                else if ([suggetedArray count]==2){
                    hieghtcell= 175;
                }
                else{
                    hieghtcell=236 ;
                }
                return hieghtcell;
            }
            else
            {
                
                AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                if ([appDelegate.notfoundadsArray containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                    return 0;
                }
                NSMutableArray *targetArray=[[NSUserDefaults standardUserDefaults] valueForKey:@"Targetarray"];
                for (int i=0; i<[targetArray count]; i++) {
                    NSDictionary *dictcellposition=[targetArray objectAtIndex:i];
                    if ([dictcellposition isKindOfClass:[NSDictionary class]]) {
                        CGFloat widthad,heightad;
                        NSInteger anchorID=[[dictcellposition objectForKey:@"anchor_no"] integerValue];
                        if (anchorID==indexPath.row) {
                            NSString *sizeAD=[dictcellposition objectForKey:@"slot_size"];
                            if (![sizeAD isEqualToString:@""]) {
                                NSString *widthadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:0] ;
                                widthad = (CGFloat)[widthadStr floatValue];
                                NSString *heightadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:1];
                                heightad = (CGFloat)[heightadStr floatValue];
                                return heightad+10;
                                
                            }
                        }
                    }
                }
            }
            
        }
        else{
            if ([[arrWallFeeds objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
                NSMutableArray *attachment=dict[@"get_attachments"];
                
                if (attachment.count==0) {
                    // No post image
                    
                    float heightForPopularGroupSection=0;
                    NSString *checkMostPopularGroup= @"0";
                    if ([[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                        checkMostPopularGroup=@"1";
                    }
                    // check top group name
                    // checkMostPopularGroup=1
                    
                    if([checkMostPopularGroup isEqualToString:@"1"] || [[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                        heightForPopularGroupSection=40;
                    }
                    
                    NSString *string1=dict[@"content"];
                    UIFont *font = semibold;
                    CGRect rect = [string1 boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{NSFontAttributeName : font}
                                        context:nil];

                    CGFloat heightofcell;
                    see_morecomment=0;
                    if ([dict[@"get_total_commnets"] isKindOfClass:[NSNull class]]){
                        heightofView=0;
                    }else{
                        int totalcount=[dict[@"get_total_commnets"][@"count"] intValue];
                        if(totalcount==1)  {
                            heightofView=61;
                        }else if(totalcount==2)  {
                            heightofView=61*2;
                        }else if(totalcount==3)  {
                            heightofView=61*3;
                        }else if(totalcount>3)  {
                            heightofView=61*3;
                            see_morecomment=32;
                        }
                    }
                    if (rect.size.height<120) {
                        postCommentHeight=rect.size.height;
                    }else{
                        postCommentHeight=120;
                    }
                    if([checkMostPopularGroup isEqualToString:@"1"]) {
                        //heightofcell=88+postCommentHeight+10+15+heightofView+40+see_morecomment+heightForPopularGroupSection;
            heightofcell=4+32+6+74+postCommentHeight+30+44+44+heightofView+see_morecomment+heightForPopularGroupSection+10;
                    }
                    else{
                        //heightofcell=88+postCommentHeight+10+37+heightofView+40+see_morecomment+heightForPopularGroupSection;
            heightofcell=4+74+postCommentHeight+30+44+44+heightofView+see_morecomment+heightForPopularGroupSection+10;
                    }
                    return  heightofcell;
                }
                else
                {
                    // Has post image
                    
                    float heightForPopularGroupSection=0;
                    NSString *checkMostPopularGroup=@"0";
                    // Community post
                    if ([[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                        checkMostPopularGroup=@"1";
                    }
                    if([checkMostPopularGroup isEqualToString:@"1"] || [[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                        heightForPopularGroupSection=40;
                    }
                    NSString *string1=dict[@"content"];
                    
                    UIFont *font = semibold;
                    CGRect rect = [string1 boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:@{NSFontAttributeName : font}
                                                        context:nil];
                    see_morecomment=0;
//                    NSLog(@"text height %f and content %@",rect.size.height,string1);
                    
                    if ([dict[@"get_total_commnets"] isKindOfClass:[NSNull class]]){
                        heightofView=0;
                    }else{
                        int totalcount=[dict[@"get_total_commnets"][@"count"] intValue];
                        if(totalcount==1)  {
                            heightofView=61;
                        }else if(totalcount==2)  {
                            heightofView=61*2;
                        }else if(totalcount==3)  {
                            heightofView=61*3;
                        }else if(totalcount>3)  {
                            heightofView=61*3;
                            see_morecomment=32;
                        }
                    }
                    
                    CGFloat heightofcell;
                    if (rect.size.height<120) {
                        postCommentHeight=rect.size.height;
                    }else{
                        postCommentHeight=120;
                    }
                    
                    // Get imageview height
                    CGFloat widthView1,widthView;
                    if ([self.imageArray count]>0) {
                        NSDictionary *dict=[self.imageArray objectAtIndex:indexPath.row];
                        if (![dict isKindOfClass:[NSNull class]])
                        {
                            NSString *imageHeight1=[NSString stringWithFormat:@"%@",[dict objectForKey:@"imgHeight"]];
                            NSString *imagewidth1=[NSString stringWithFormat:@"%@",[dict objectForKey:@"imgWidth"]];
                            widthView=[self calculateImageHeight:tableView.frame.size.width imageHeight:[imageHeight1 floatValue] imageWidth:[imagewidth1 floatValue]]+10;
                        }
                        else{
                            widthView1=[Constant1 checkimagehieght];
//                            widthView=widthView1+30;
                            widthView=178;
                        }
                        
                    }
                    else{
                        widthView1=[Constant1 checkimagehieght];
                        widthView=widthView1+30;
                    }
                    
//                    NSLog(@"image height %f and index path %ld",widthView,indexPath.row);
               
                    //heightofcell = 80+postCommentHeight+70+heightofView+see_morecomment+widthView+heightForPopularGroupSection+30+15;
                    //heightofcell = 4+32+6+74+widthView+10+postCommentHeight+30+44+44+heightofView+see_morecomment+32+10;
                    
                    if([checkMostPopularGroup isEqualToString:@"1"]) {
                        heightofcell = 4+32+6+74+widthView+10+postCommentHeight+30+44+44+heightofView+see_morecomment+heightForPopularGroupSection+10;
                    }
                    else{
                        heightofcell = 4+74+widthView+10+postCommentHeight+30+44+44+heightofView+see_morecomment+heightForPopularGroupSection+10;
                    }
                    
                    return  heightofcell;
                }
            }else{
                
                //Set up Ad cell height
                AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                if ([appDelegate.notfoundadsArray containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                    return 0;
                }
                NSMutableArray *targetArray=[[NSUserDefaults standardUserDefaults] valueForKey:@"Targetarray"];
                for (int i=0; i<[targetArray count]; i++) {
                    NSDictionary *dictcellposition=[targetArray objectAtIndex:i];
                    if ([dictcellposition isKindOfClass:[NSDictionary class]]) {
                        CGFloat widthad,heightad;
                        NSInteger anchorID=[[dictcellposition objectForKey:@"anchor_no"] integerValue];
                        if (anchorID==indexPath.row) {
                            NSString *sizeAD=[dictcellposition objectForKey:@"slot_size"];
                            if (![sizeAD isEqualToString:@""]) {
                                NSString *widthadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:0] ;
                                widthad = (CGFloat)[widthadStr floatValue];
                                NSString *heightadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:1];
                                heightad = (CGFloat)[heightadStr floatValue];
                                return heightad+10;
                                
                            }
                        }
                    }
                }
            }
            
        }
        
    }
    else{
        
        if ([[arrWallFeeds objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
            NSMutableArray *attachment=dict[@"get_attachments"];
            
            if (attachment.count==0) {
                
                float heightForPopularGroupSection=0;
                NSString *checkMostPopularGroup=@"0";
                if ([[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                    checkMostPopularGroup=@"1";
                }
                if([checkMostPopularGroup isEqualToString:@"1"] || [[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                    heightForPopularGroupSection=40;
                }
                
                NSString *string1=dict[@"content"];
                UIFont *font = semibold;
                CGRect rect = [string1 boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{NSFontAttributeName : font}
                                                    context:nil];
                CGFloat heightofcell;
                see_morecomment=0;
                if ([dict[@"get_total_commnets"] isKindOfClass:[NSNull class]]){
                    heightofView=0;
                }else{
                    
                    int totalcount=[dict[@"get_total_commnets"][@"count"] intValue];
                    if(totalcount==1)  {
                        heightofView=61;
                    }else if(totalcount==2)  {
                        heightofView=61*2;
                    }else if(totalcount==3)  {
                        heightofView=61*3;
                    }else if(totalcount>3)  {
                        heightofView=61*3;
                        see_morecomment=32;
                    }
                }
                if (rect.size.height<120) {
                    postCommentHeight=rect.size.height;
                }else{
                    postCommentHeight=120;
                }
                
                if([checkMostPopularGroup isEqualToString:@"1"]) {
                    //heightofcell=88+postCommentHeight+10+15+heightofView+40+see_morecomment+heightForPopularGroupSection;
            heightofcell=4+32+6+74+postCommentHeight+30+44+44+heightofView+see_morecomment+heightForPopularGroupSection+10;
                }
                else{
                    //heightofcell=88+postCommentHeight+10+37+heightofView+40+see_morecomment+heightForPopularGroupSection;
            heightofcell=4+74+postCommentHeight+30+44+44+heightofView+see_morecomment+heightForPopularGroupSection+10;
                }
                
                return  heightofcell;
            }else{
                
                float heightForPopularGroupSection=0;
                NSString *checkMostPopularGroup=@"0";
                if ([[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                    checkMostPopularGroup=@"1";
                }
                if([checkMostPopularGroup isEqualToString:@"1"] || [[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                    heightForPopularGroupSection=40;
                }
                NSString *string1=dict[@"content"];
                UIFont *font = semibold;
                CGRect rect = [string1 boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:@{NSFontAttributeName : font}
                                                    context:nil];
                see_morecomment=0;
                if ([dict[@"get_total_commnets"] isKindOfClass:[NSNull class]]){
                    heightofView=0;
                }else{
                    int totalcount=[dict[@"get_total_commnets"][@"count"] intValue];
                    if(totalcount==1)  {
                        heightofView=61;
                    }else if(totalcount==2)  {
                        heightofView=61*2;
                    }else if(totalcount==3)  {
                        heightofView=61*3;
                    }else if(totalcount>3)  {
                        heightofView=61*3;
                        see_morecomment=32;
                    }
                }
                CGFloat heightofcell;
                if (rect.size.height<120) {
                    postCommentHeight=rect.size.height;
                }else{
                    postCommentHeight=120;
                }
                // get image height
                CGFloat widthView1,widthView;
                if ([self.imageArray count]>0) {
                    NSDictionary *dict=[self.imageArray objectAtIndex:indexPath.row];
                    if (![dict isKindOfClass:[NSNull class]])
                    {
                        NSString *imageHeight2=[NSString stringWithFormat:@"%@",[dict objectForKey:@"imgHeight"]];
                        NSString *imagewidth2=[NSString stringWithFormat:@"%@",[dict objectForKey:@"imgWidth"]];
                        widthView=[self calculateImageHeight:tableView.frame.size.width imageHeight:[imageHeight2 floatValue] imageWidth:[imagewidth2 floatValue]]+10;
                    }
                    else{
                        widthView1=[Constant1 checkimagehieght];
                        widthView=178;
                    }
                }
                else{
                    widthView1=[Constant1 checkimagehieght];
                    widthView=widthView1+30;
                }
//                NSLog(@"image height %f and index path %ld",widthView,indexPath.row);
                
                //heightofcell=80+postCommentHeight+70+heightofView+see_morecomment+widthView+heightForPopularGroupSection+30+15;
                heightofcell = 4+32+6+74+widthView+10+postCommentHeight+30+44+44+heightofView+30+32+10;
                
                if([checkMostPopularGroup isEqualToString:@"1"]) {
                //heightofcell=88+postCommentHeight+10+15+heightofView+40+see_morecomment+heightForPopularGroupSection;
                heightofcell = 4+32+6+74+widthView+10+postCommentHeight+30+44+44+heightofView+see_morecomment+heightForPopularGroupSection+10;
                }
                else{
                    //heightofcell=88+postCommentHeight+10+37+heightofView+40+see_morecomment+heightForPopularGroupSection;
                    heightofcell = 4+74+widthView+10+postCommentHeight+30+44+44+heightofView+see_morecomment+heightForPopularGroupSection+10;
                }
                
                return  heightofcell;
                
            }
        }else{
            AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            if ([appDelegate.notfoundadsArray containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
                return 0;
            }
            NSMutableArray *targetArray=[[NSUserDefaults standardUserDefaults] valueForKey:@"Targetarray"];
            for (int i=0; i<[targetArray count]; i++) {
                NSDictionary *dictcellposition=[targetArray objectAtIndex:i];
                if ([dictcellposition isKindOfClass:[NSDictionary class]]) {
                    CGFloat widthad,heightad;
                    NSInteger anchorID=[[dictcellposition objectForKey:@"anchor_no"] integerValue];
                    if (anchorID==indexPath.row) {
                        NSString *sizeAD=[dictcellposition objectForKey:@"slot_size"];
                        if (![sizeAD isEqualToString:@""]) {
                            NSString *widthadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:0] ;
                            widthad = (CGFloat)[widthadStr floatValue];
                            NSString *heightadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:1];
                            heightad = (CGFloat)[heightadStr floatValue];
                            return heightad+10;
                        }
                    }
                }
            }
        }
    }
    return 0;
}


-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"HeaderView"
                                                      owner:self
                                                    options:nil];
    //    HeaderView *headerView = (HeaderView *)[nibViews objectAtIndex: 0];
    HeaderView *headerView = [nibViews objectAtIndex: 0];
    _textViewTXT=(UITextView*)[headerView viewWithTag:800];
    UIImageView *imag=(UIImageView*)[headerView viewWithTag:400];
    UIButton *requestedBTN=(UIButton*)[headerView viewWithTag:2001];
    [requestedBTN addTarget:self action:@selector(request_Prayer:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *updateBTN=(UIButton*)[headerView viewWithTag:2002];
    [updateBTN addTarget:self action:@selector(request_Status:) forControlEvents:UIControlEventTouchUpInside];
    prayerTotalCountStr1=(UILabel*)[headerView viewWithTag:2003];
    // header ad implemention
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([getVar.headerDict count]>0 ) {
        dispatch_async(dispatch_get_main_queue(), ^{
            CGFloat widthad,heightad;
            DFPBannerView *feedAdView3;
            NSString *adID=[getVar.headerDict objectForKey:@"ad_id"];
            NSString *sizeAD=[getVar.headerDict objectForKey:@"slot_size"];
            if (![sizeAD isEqualToString:@""]) {
                NSString *widthadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:0] ;
                widthad = (CGFloat)[widthadStr floatValue];
                NSString *heightadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:1];
                heightad = (CGFloat)[heightadStr floatValue];
                feedAdView3=[[DFPBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
                GADAdSize customAdSize = GADAdSizeFromCGSize(CGSizeMake(widthad,heightad));
                feedAdView3 = [[DFPBannerView alloc] initWithAdSize:customAdSize];
                feedAdView3.validAdSizes = @[
                        NSValueFromGADAdSize(GADAdSizeFromCGSize(CGSizeMake(widthad,heightad)))
                                    ];
                feedAdView3.adUnitID=adID;
                if ( getVar.headeradcheck==YES) {
                 headerView.adheightConstraints.constant=0;
                    feedAdView3=nil;
                 }
                else{
                    headerView.adheightConstraints.constant=heightad;
                    headerView.adwidthConstraints.constant=widthad;
                    if (IS_IPHONE4) {
                        feedAdView3.frame=CGRectMake(1,1,widthad,heightad);
                    }
                    if (IS_IPHONE5) {
                        feedAdView3.frame=CGRectMake(10,1,widthad,heightad);
                    }
                    if (IS_IPHONE6) {
                        feedAdView3.frame=CGRectMake(32,7,widthad,heightad);
                    }
                    if (IS_IPHONE6PLUS) {
                        feedAdView3.frame=CGRectMake(55,1,widthad,heightad);
                    }
                    headerView.banner_mainView.clipsToBounds=YES;
                    feedAdView3.delegate = (id)self;
                    feedAdView3.rootViewController =(id)self ;
                    [feedAdView3 loadRequest:[DFPRequest request]];
                    [headerView addSubview:feedAdView3];
                    headerView.mainView.clipsToBounds=YES;
                    [headerView setNeedsUpdateConstraints];
                    [headerView updateConstraintsIfNeeded];
                    [headerView layoutIfNeeded];

                }
                
            }
            else{
                headerView.adheightConstraints.constant=0;
            }
        });
        
    }
    else{
        headerView.adheightConstraints.constant=0;
        [headerView setNeedsUpdateConstraints];
        [headerView updateConstraintsIfNeeded];
        [headerView layoutIfNeeded];

        
    }
    if(!totalcountPrayer)
        totalcountPrayer = @"0";
    NSString *combinedStr = [[NSString alloc] init];
    if([totalcountPrayer isEqualToString:@"1"])
    {
        combinedStr = [NSString stringWithFormat:@"%@ %@ %@",@"You have Prayed",totalcountPrayer,@"time for fellow Christians"];
    }
    else
    {
        combinedStr = [NSString stringWithFormat:@"%@ %@ %@",@"You have Prayed",totalcountPrayer,@"times for fellow Christians"];
    }
    NSAttributedString *attributedString=[Constant1 customizeString:combinedStr range:16 rangeOne:totalcountPrayer.length];
    prayerTotalCountStr1.attributedText=attributedString;
    if(hprofile_picture && hprofile_picture!=nil)
    {
        [imag sd_setImageWithURL:[NSURL URLWithString:hprofile_picture] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
    }
    else
    {
        imag.image=[self imageFromColorDefault:nil];
    }
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(imag.image) forKey:@"user_image"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSDictionary *checkDict;
    if ([inluencerListArrayFeed count]>0) {
        for (int i=0; i<[inluencerListArrayFeed count]; i++) {
            checkDict=[inluencerListArrayFeed objectAtIndex:i];
            if ([checkDict isKindOfClass:[NSDictionary class]]) {
                NSString *user_idcheck=[NSString stringWithFormat:@"%@",[checkDict valueForKey:@"id"]];
                NSString *logedUserId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
                if ([user_idcheck isEqualToString:logedUserId]) {
                    NSString *usertypecheck=[NSString stringWithFormat:@"%@",[checkDict valueForKey:@"user_type"]];
                    if ([usertypecheck isEqualToString:@"2"]) {
                        UIImageView *imagfluencer=(UIImageView*)[headerView viewWithTag:5003];
                        imagfluencer.hidden=NO;
                        [[NSUserDefaults standardUserDefaults] setObject:@"yesGot" forKey:@"mila"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        break ;
                    }
                    else{
                        UIImageView *imagfluencer=(UIImageView*)[headerView viewWithTag:5003];
                        imagfluencer.hidden=YES;
                    }
                }
                else{
                    UIImageView *imagfluencer=(UIImageView*)[headerView viewWithTag:5003];
                    imagfluencer.hidden=YES;
                }
            }
        }
    }
    else{
        NSString *checkimage=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mila"]];
        if ([checkimage isEqualToString:@"yesGot"]) {
            if (self.influencerimageValue==YES) {
                UIImageView *imagfluencer=(UIImageView*)[headerView viewWithTag:5003];
                imagfluencer.hidden=YES;
                self.influencerimageValue=NO;
            }
            else{
                checkcountZeroinfluencer=@"";
                UIImageView *imagfluencer=(UIImageView*)[headerView viewWithTag:5003];
                imagfluencer.hidden=NO;
            }
        }
        else{
            UIImageView *imagfluencer=(UIImageView*)[headerView viewWithTag:5003];
            imagfluencer.hidden=YES;
        }
    }
    [headerView setNeedsUpdateConstraints];
    [headerView updateConstraintsIfNeeded];
    [headerView layoutIfNeeded];
    return headerView;
}


#pragma mark influencerFollow method

-(void)followInfluecerMethod:(NSString*)sender_id sender_tag:(NSString*)senderTag influencerName:(NSString*)influencers{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict=nil;
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 @"friend_id" : sender_id,
                 };
[[WebServiceHelper sharedInstance] callPostDataWithMethod:follow withParameters:dict withHud:YES success:^(id response){
                         if ([response isKindOfClass:[NSDictionary class]]){
                         if (![response isKindOfClass:[NSNull class]]) {
                         if([response[@"status"]intValue ] ==1){
                         dispatch_async(dispatch_get_main_queue(), ^{
                            NSString *currentDate=[Constant1 createTimeStamp];
                            [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Number of follows" by:@1];
                            [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last follow":currentDate}];
                            [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Date of last follow":currentDate}];
                            [[AppDelegate sharedAppDelegate].mxPanel track:@"Follow"
                                    properties:@{@"Influencer?":[NSNumber numberWithBool:true],@"Date of last follow":currentDate,@"Following":influencers}];
                            [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            NSMutableDictionary *influencerDict;
                            for (int i=0; i<[suggetedArray count]; i++) {
                                influencerDict=[suggetedArray objectAtIndex:i];
                                NSString *idInfluencer=[NSString stringWithFormat:@"%@",[influencerDict objectForKey:@"id"]];
                                if ([sender_id isEqualToString:idInfluencer]){
                                    [influencerDict setObject:@"Unfollow" forKey:@"friend_status"];
                                    NSDictionary *userInfogroup = @{
                                                                    @"allData" : influencerDict,
                                                                    };
                                    [[NSNotificationCenter defaultCenter] postNotificationName: @"influcencerProfileView" object:nil userInfo:userInfogroup];
                                }
                            }
                        });
                    }
                    else
                        {
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([messageArray count]>0) {
                                [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                        });
                    }
                }
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
    }
}



#pragma direct to group

-(void)groupjoinGroup:(NSString*)groupcomid groupname:(NSString*)groupname{
    NSString *groupStr=[NSString stringWithFormat:@"clicked_top_post_for_group_%@",groupname];
    [TenjinSDK sendEventWithName:groupStr];
    [FBSDKAppEvents logEvent:groupStr];
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:groupStr]];
     dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *userInfogroup = @{
                                        @"groupis" : groupcomid,
                                        @"groupname" : groupname,
                                        };
[[NSNotificationCenter defaultCenter] postNotificationName: @"feedGroupsuggestion" object:nil userInfo:userInfogroup];
    });
}

#pragma influencerList Method

-(void)checkinfluencerlist{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict=nil;
        dict = @{
                 @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:influencerList withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            inluencerListArrayFeed=[response valueForKey:@"data"];
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([messageArray count]>0) {
//                                [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
         }];
        
    }
 
}


#pragma countLineNumber for lable 
- (int)lineCountForText:(NSString *) text
{
    UIFont *font = semibold;
    CGRect rect = [text boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName : font}
                                     context:nil];
    return ceil(rect.size.height / font.lineHeight);
}



#pragma  commonsetUpCell Method

-(void)commonsetUpCell:(FeedwithImagePostTableViewCell *)postimagecell andfeedpost:(FeedpostTableViewCell *)postcell andadvertisecell:(AdcellfeedTableViewCell*)advertisecell atIndexPath:(NSIndexPath *)indexPath {
    
    float width=[UIScreen mainScreen].bounds.size.width-(20);
    if ([[arrWallFeeds objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *get_object_user_details;
        NSString *objectName,*checkprofile;
        NSString *defaultFrindID;
        NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
        if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]] && [dict[@"get_subject_user_details"] isKindOfClass:[NSDictionary class]]) {
            if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *checkDictUser=dict[@"get_object_user_details"];
                if (checkDictUser.count!=0) {
                    get_object_user_details=dict[@"get_object_user_details"];
                    NSString   *objectNametxt=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
                    objectName=[NSString stringWithFormat:@"%@%@ %@",objectNametxt,@"'s",@"Wall"];
                    defaultFrindID=[NSString stringWithFormat:@"%@",[get_object_user_details valueForKey:@"id"]];
                }
            }
            if ( [dict[@"get_object_community_details"] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *checkDictCommunity=dict[@"get_object_community_details"];
                if (checkDictCommunity.count!=0) {
                    get_object_user_details=dict[@"get_object_community_details"];
                    NSString  *objectNametxt=[NSString stringWithFormat:@"%@",get_object_user_details[@"name"]];
                    objectName=[NSString stringWithFormat:@"%@%@ %@",objectNametxt,@"'s",@"Wall"];
                    objectName=@"";
                }
            }
        }
        NSDictionary *get_subject_user_details=dict[@"get_subject_user_details"];
        NSDictionary *subject_user_details=get_subject_user_details[@"usersprofile"];
        NSString *subjectName=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
        get_total_commnet=dict[@"get_total_commnets"];
        NSDictionary* get_total_praised=dict[@"get_total_praised"];
        NSMutableArray *get_attachments=dict[@"get_attachments"];
        NSString *profile_picture=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:subject_user_details[@"profile_picture"]]];
        profile_picture=[NSString stringWithFormat:@"%@%@",ImageapiUrl,profile_picture];
        NSString *post_image;
        postimageArray=[[NSMutableArray alloc]init];
        for (NSDictionary* image_dict in get_attachments){
            post_image=image_dict[@"file"];
            [postimageArray addObject:image_dict[@"file"]];
        }
        NSString *post_picture=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:post_image]];
        post_picture=[NSString stringWithFormat:@"%@%@",post_ImageapiUrl,post_picture];
        if (postimagecell) {
            
            NSString *postType1=dict[@"post_type"];
            int post_val=[postType1 intValue];
            postimagecell.labelPP.preferredMaxLayoutWidth = width;
            postimagecell.bottomofcommentView.constant=4;
            [postimagecell.imageview_post sd_setImageWithURL:[NSURL URLWithString:post_picture] placeholderImage:[self imageFromColor:[UIColor lightGrayColor]]];
            postimagecell.widthofimageview.constant=postimagecell.frame.size.width-16;
            if (IS_IPHONE4||IS_IPHONE5) {
                postimagecell.lbl_postStatus.numberOfLines = 1;
                postimagecell.lbl_postStatus.minimumScaleFactor = 0.01;
                postimagecell.lbl_postStatus.adjustsFontSizeToFitWidth = YES;
                
            }
            // for Most popular cell
            NSString *checkMostPopularGroup;
            if (![[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                checkMostPopularGroup=@"nomost_popularData";
            }
            else{
                checkMostPopularGroup=@"1";
            }
            NSDictionary *GroupDictNameDict=[dict valueForKey:@"get_object_community_details"];
            NSString *groupName;
            if ([GroupDictNameDict isKindOfClass:[NSDictionary class]]) {
                groupName=[GroupDictNameDict valueForKey:@"name"];
            }
            //            start 0
            if([checkMostPopularGroup isEqualToString:@"0"]||[checkMostPopularGroup isEqualToString:@"nomost_popularData"] ) {
                postimagecell.toppostImagelblHeight.constant=0;
                postimagecell.seeMoreGroupRequestbtn.hidden=YES;
                postimagecell.toppostbetviewinImage.constant=0;
                postimagecell.toppostImagelbl.hidden=YES;
                postimagecell.seeMoreGroupRequestHeght.constant=0;
          
                postimagecell.heightofBottomButtonView.constant=0.0;
                postimagecell.topgroupHeight.constant=0;
            }
            else{
                postimagecell.heightofBottomButtonView.constant=40.0;
                postimagecell.topgroupHeight.constant=7;
                postimagecell.seeMoreGroupRequestbtn.hidden=NO;
                postimagecell.toppostImagelbl.hidden=NO;
                postimagecell.seeMoreGroupRequestHeght.constant=32;
                postimagecell.toppostImagelblHeight.constant=32;
                postimagecell.toppostbetviewinImage.constant=6;
                NSString *combinedString=[NSString stringWithFormat:@"Recent posts in %@",groupName];
                NSString *combinedStringbtn=[NSString stringWithFormat:@"See More in %@>>",groupName];
                postimagecell.seeMoreGroupRequestbtn.tag=indexPath.row;
                UIFont *seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:16];
                if(IS_IPHONE4)
                {
                    seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:11];
                    postimagecell.toppostImagelbl.text=combinedString;
                    [postimagecell.toppostImagelbl setFont:[UIFont fontWithName:@"Avenir-Medium" size:14]];
                }
                else if (IS_IPHONE5)
                {
                    seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:12];
                    postimagecell.toppostImagelbl.text=combinedString;
                     [postimagecell.toppostImagelbl setFont:[UIFont fontWithName:@"Avenir-Medium" size:16]];
                }
                else if (IS_IPHONE6)
                {
                    seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:14];
                    postimagecell.toppostImagelbl.text=combinedString;
                }
                else if (IS_IPHONE6PLUS)
                {
                    seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:16];
                    postimagecell.toppostImagelbl.text=combinedString;
                }
                else if (IS_IPHONE_X || IS_IPHONE_XS_Max)
                {
                    seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:16];
                    postimagecell.toppostImagelbl.text=combinedString;
                }
                NSMutableAttributedString * string =[[NSMutableAttributedString alloc] initWithString:combinedStringbtn attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone),NSFontAttributeName : seeMoreGroupFont,NSForegroundColorAttributeName: [self colorFromHexString:@"00BAF5"]}];
                [postimagecell.seeMoreGroupRequestbtn setAttributedTitle:string forState:UIControlStateNormal];
                postimagecell.seeMoreGroupRequestbtn.tag=indexPath.row;
                [postimagecell.seeMoreGroupRequestbtn  addTarget:self action:@selector(imagecellSeemoreGroup:) forControlEvents:UIControlEventTouchUpInside];
            }
            CGFloat widthView1,widthView;
            postimagecell.imageview_post.tag=indexPath.row;
            // see more Group Comment and height for image manage
            
            [postimagecell.imageview_post sd_setImageWithURL:[NSURL URLWithString:post_picture] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL, NSString *imageTag) {
                NSString *key = [[SDWebImageManager sharedManager] cacheKeyForURL:imageURL];
                UIImage *lastPreviousCachedImage1 = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:key];

                if (image != nil) {
                    id arrObj=[self.imageArray objectAtIndex:[imageTag intValue]];
                    if(![arrObj isKindOfClass:[NSNull class]] && lastPreviousCachedImage1!=nil)
                    {             NSDictionary *dataDict;
                        if ([AppDelegate sharedAppDelegate].postimagecheckNotnull==YES && [imageTag intValue]==0) {
                            dataDict=[self.imageArray objectAtIndex:0];
                            if (dataDict!= nil && [dataDict count]>0) {
                                NSDictionary *dictimage;
                                NSString *imgAbsoluteUrl= imageURL.absoluteString;
                                NSString *imgHeight=[NSString stringWithFormat:@"%f",image.size.height];
                                NSString *imgWidth=[NSString stringWithFormat:@"%f",image.size.width];
                                NSString *tag=[NSString stringWithFormat:@"%d",[imageTag intValue]];
                                dictimage= @{
                                             @"imgAbsoluteUrl" :imgAbsoluteUrl ,
                                             @"imgHeight" :imgHeight,
                                             @"imgWidth":imgWidth,
                                             @"imageTag":tag,
                                             };
                                [self.imageArray replaceObjectAtIndex:0 withObject:dictimage];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [AppDelegate sharedAppDelegate].postimagecheckNotnull=NO;
                                    [self.tableViewOutlet reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:NO];
                                });
                            }
                        }
                        else{
                            //    skiped image already added.
 
                        }
                    }
                    else
                    {
                        NSDictionary *dictimage;
                        NSString *imgAbsoluteUrl= imageURL.absoluteString;
                        NSString *imgHeight=[NSString stringWithFormat:@"%f",image.size.height];
                        NSString *imgWidth=[NSString stringWithFormat:@"%f",image.size.width];
                        NSString *tag=[NSString stringWithFormat:@"%d",[imageTag intValue]];
                        dictimage= @{
                                @"imgAbsoluteUrl" :imgAbsoluteUrl ,
                                @"imgHeight" :imgHeight,
                                @"imgWidth":imgWidth,
                                @"imageTag":tag,
                                };

                        [self.imageArray replaceObjectAtIndex:[imageTag intValue] withObject:dictimage];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableViewOutlet reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:[imageTag intValue] inSection:0], nil] withRowAnimation:NO];
                        });
                    }
                }
                
            }];
        
            NSString *key = [[SDWebImageManager sharedManager] cacheKeyForURL:[NSURL URLWithString:post_picture]];
            UIImage *lastPreviousCachedImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:key];
            id imageDict=[self.imageArray objectAtIndex:indexPath.row];
            if(lastPreviousCachedImage && ![imageDict isKindOfClass:[NSNull class]]){
                widthView1 = [self calculateImageHeight:self.tableViewOutlet.frame.size.width imageHeight:lastPreviousCachedImage.size.height imageWidth:lastPreviousCachedImage.size.width];
            }
            else{
                widthView=[Constant1 checkimagehieght];
                widthView1=164;
            }
            
            postimagecell.heightofimageview.constant=widthView1;
            
            
            postimagecell.imageview_post.userInteractionEnabled=YES;
            UITapGestureRecognizer *imagegallery_open = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moreImages:)];
            [postimagecell.imageview_post addGestureRecognizer:imagegallery_open];
            postimagecell.btn_moreImages.tag=indexPath.row;
            [postimagecell.btn_moreImages addTarget:self action:@selector(btn_moreImages:) forControlEvents:UIControlEventTouchUpInside];
            
            
//             NSLog(@"image height cellfor  %f and index path %ld and content %@",widthView1,indexPath.row,dict[@"content"]);
            
            
            if ( [dict[@"get_total_praised"] isKindOfClass:[NSNull class]]) {
                [postimagecell.btn_praisedCount setTitle:@"No Praises yet" forState:UIControlStateNormal];
                [postimagecell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
                if (post_val==1||post_val==3) {
                    postimagecell.imageview_praised.text=@"Praise This";
                    [postimagecell.btn_praised setTitle:@"Praise This" forState:UIControlStateNormal];
                    [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                    [postimagecell.btn_praisedCount setTitle:@"No Praises yet" forState:UIControlStateNormal];
//                    [postimagecell.btn_praised setTitle:@"Praise This" forState:UIControlStateNormal];
//                    [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                }else{
                    postimagecell.imageview_praised.text=@"Pray For This";
                    [postimagecell.btn_praised setTitle:@"Pray For This" forState:UIControlStateNormal];
                    [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                    [postimagecell.btn_praisedCount setTitle:@"No Prayers yet" forState:UIControlStateNormal];
//                    [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                }
                postimagecell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_unselected"];
                postcell.imageview_praised.textColor=[UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1.0];
                
            }else{
                get_total_praised=dict[@"get_total_praised"];
                NSString *like=[NSString stringWithFormat:@"%@",dict[@"is_like"]];
                int like_count = [like intValue];
                if (like_count==1) {
                    NSString *like1=[NSString stringWithFormat:@"%@",get_total_praised[@"count"]];
                    int main_like= [like1 intValue];
                    NSString *total_count;
                    if (post_val==1||post_val==3) {
                        if (main_like>=3) {
                    total_count=[NSString stringWithFormat:@"You and %d others Praised This",main_like-1];
                        }
                        else{
                         total_count=[NSString stringWithFormat:@"You and %d other Praised This",main_like-1];
                        }
                        if (main_like==1) {
                            total_count=[NSString stringWithFormat:@"You Praised This"];
                        }
                        postimagecell.imageview_praised.text=@"You Praised This";
                        [postimagecell.btn_praised setTitle:@"You Praised This" forState:UIControlStateNormal];
                        postimagecell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_selected"];
                        postimagecell.imageview_praised.textColor=[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
                        [postimagecell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
                        [postimagecell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                        [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                        
                    }else{
                        if (main_like>=3) {
                           total_count=[NSString stringWithFormat:@"You and %d others Prayed for This",main_like-1];
                        }
                        else{
                           total_count=[NSString stringWithFormat:@"You and %d other Prayed for This",main_like-1];
                        }
                        if (main_like==1) {
                            total_count=[NSString stringWithFormat:@"You Prayed for This"];
                        }
                        postimagecell.imageview_praised.text=@"You Prayed";
                        [postimagecell.btn_praised setTitle:@"You Prayed" forState:UIControlStateNormal];
                        postimagecell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_selected"];
                        postimagecell.imageview_praised.textColor=[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
                        [postimagecell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
                        [postimagecell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                        [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                    }
                  }
                else
                   {
                    if (post_val==1||post_val==3) {
                        postimagecell.imageview_praised.text=@"Praise This";
                        [postimagecell.btn_praised setTitle:@"Praise This" forState:UIControlStateNormal];
                        [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                        NSString *total_count=[NSString stringWithFormat:@"%@ Praised This",[Constant1 getStringObject:get_total_praised[@"count"]]];
                        [postimagecell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
                        [postimagecell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                    }else{
                        postimagecell.imageview_praised.text=@"Pray For This";
                        [postimagecell.btn_praised setTitle:@"Pray For This" forState:UIControlStateNormal];
                        [postimagecell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                        NSString *total_count=[NSString stringWithFormat:@"%@ Prayed for This",[Constant1 getStringObject:get_total_praised[@"count"]]];
                        [postimagecell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
                        [postimagecell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                    }
                    postimagecell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_unselected"];
                    postcell.imageview_praised.textColor=[UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1.0];
                }
            }
            if ([get_total_commnet isKindOfClass:[NSNull class]]){
                [postimagecell.btn_commentsCount setTitle:@"0 Comments" forState:UIControlStateNormal];
            }else{
                NSString *total_count=[NSString stringWithFormat:@"%@ Comments",[Constant1 getStringObject:get_total_commnet[@"count"]]];
                [postimagecell.btn_commentsCount setTitle:total_count forState:UIControlStateNormal];
            }
            // seting for influcer profile image
            checkinclencerImageGlobal=[NSString stringWithFormat:@"%@",[dict valueForKey:@"is_influencer"]];
            if ([checkinclencerImageGlobal isEqualToString:@"0"]) {
                postimagecell.main_profile_image.hidden=YES;
                [postimagecell.imageview_person sd_setImageWithURL:[NSURL URLWithString:profile_picture] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
            }
            else{
                postimagecell.main_profile_image.hidden=NO;
                [postimagecell.imageview_person sd_setImageWithURL:[NSURL URLWithString:profile_picture] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
            }
            postimagecell.lbl_date.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:dict[@"formatted_date"]]];
            postimagecell.btn_readmorepost.tag=indexPath.row;
            [postimagecell.btn_readmorepost addTarget:self action:@selector(readMore:) forControlEvents:UIControlEventTouchUpInside];
            [postimagecell.btn_Comments addTarget:self action:@selector(btncomments_act:) forControlEvents:UIControlEventTouchUpInside];
            [postimagecell.btn_Comments addTarget:self action:@selector(btncomments_act:) forControlEvents:UIControlEventTouchUpInside];
            postimagecell.btn_Comments.tag=indexPath.row;
            postimagecell.btn_commentsCount.tag=indexPath.row;
            postimagecell.btn_praised.tag=indexPath.row;
            postimagecell.btn_praisedCount.tag=indexPath.row;
            NSString *string1=dict[@"content"];
            // pplabel implementation
            NSError *error = nil;
            postimagecell.labelPP.text=string1;
            UIFont *font = semibold;
            CGRect rect = [string1 boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{NSFontAttributeName : font}
                                                context:nil];

            countLineOfLable=[self lineCountForText:string1];
            detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
            self.matches = [detector matchesInString:postimagecell.labelPP.text options:0 range:NSMakeRange(0, postimagecell.labelPP.text.length)];
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            [postimagecell.labelPP addGestureRecognizer:singleTap];
            [self highlightLinksWithIndex:NSNotFound custum:postimagecell.labelPP];
            postimagecell.btn_seemorecomments.hidden=YES;
            postimagecell.heightofLabelconstraint.constant= label_personViewHeight;
//            NSLog(@"text height %f ",rect.size.height);
//            NSLog(@"line  %d",countLineOfLable);
            
                if (countLineOfLable>=6) {
                    postimagecell.btn_readmorepost.hidden=NO;
                    label_personViewHeight=120;
                }
                else{
                    postimagecell.btn_readmorepost.hidden=YES;
                    label_personViewHeight=rect.size.height;
                }
            
            postimagecell.btn_readmorepost.tag=indexPath.row;
            [postimagecell.btn_readmorepost addTarget:self action:@selector(readMore:) forControlEvents:UIControlEventTouchUpInside];
            postimagecell.lbl_objectname.userInteractionEnabled=YES;
            postimagecell.lbl_personName.userInteractionEnabled=YES;
            postimagecell.imageview_person.userInteractionEnabled=YES;
            // tap gesture to open Profile subject
            UITapGestureRecognizer *personImage_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch:)];
            [postimagecell.imageview_person addGestureRecognizer:personImage_gesture];
            UITapGestureRecognizer *subjectname_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch:)];
            [postimagecell.lbl_personName addGestureRecognizer:subjectname_gesture];
            UITapGestureRecognizer *objectname_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(object_touch:)];
            [postimagecell.lbl_objectname addGestureRecognizer:objectname_gesture];
            [postimagecell.lbl_objectname addGestureRecognizer:objectname_gesture];
            postimagecell.lbl_objectname.tag=indexPath.row;
            postimagecell.lbl_personName.tag=indexPath.row;
            postimagecell.imageview_person.tag=indexPath.row;
            if (post_val==1||post_val==3) {
                postimagecell.lbl_postStatus.text=@"";
                if ([checkprofile isEqualToString:@"check"]) {
                    postimagecell.lbl_postStatus.text=@" ";
                    checkprofile=@"";
                }
                else
                {
                    
                    if ( [dict[@"get_object_community_details"] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *checkDictCommunity=dict[@"get_object_community_details"];
                        if (checkDictCommunity.count!=0) {
                            postimagecell.lbl_postStatus.text=@" ";
                        }
                        else
                        {
                            if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]] && [dict[@"get_subject_user_details"] isKindOfClass:[NSDictionary class]]) {
                                if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]]) {
                                    NSDictionary *checkDictUser=dict[@"get_object_user_details"];
                                    NSDictionary *checkDictSubUser=dict[@"get_object_user_details"];
                                    if (checkDictUser.count!=0 && checkDictSubUser.count!=0) {
                                        NSString   *objectNametxt=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
                                        NSString   *SubjectNametxt=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
                                        NSString *userid=[NSString stringWithFormat:@"%@",get_subject_user_details[@"id"]];
                                        NSString *loguserId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
                                        
                                        if([SubjectNametxt isEqualToString:objectNametxt])
                                        {
                                            if ([userid isEqualToString:loguserId]) {
                                            }
                                            else{
                                                postimagecell.lbl_postStatus.text=@" ";
                                            }
                                        }
                                        else{
                                            postimagecell.lbl_postStatus.text=@"  wrote on ";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                postimagecell.lbl_postStatus.textAlignment =NSTextAlignmentLeft;
                
            }else{
                NSString *defaultuser=[NSString stringWithFormat:@"%@",get_object_user_details[@"id"]];
                if ([defaultuser isEqualToString:@"1"]) {
                    postimagecell.lbl_postStatus.text=@"";
                }
                else{
                    // chages here for requested a prayer
                    postimagecell.lbl_postStatus.text=@" Requested A Prayer";
                    if ( [dict[@"get_object_community_details"] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *checkDictCommunity=dict[@"get_object_community_details"];
                        if (checkDictCommunity.count!=0) {
                            postimagecell.lbl_postStatus.text=@" ";
                        }
                        else
                        {
                            if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]] && [dict[@"get_subject_user_details"] isKindOfClass:[NSDictionary class]]) {
                                if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]]) {
                                    NSDictionary *checkDictUser=dict[@"get_object_user_details"];
                                    NSDictionary *checkDictSubUser=dict[@"get_object_user_details"];
                                    if (checkDictUser.count!=0 && checkDictSubUser.count!=0) {
                                        NSString   *objectNametxt=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
                                        NSString   *SubjectNametxt=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
                                        NSString *userid=[NSString stringWithFormat:@"%@",get_subject_user_details[@"id"]];
                                        NSString *loguserId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
                                        if([SubjectNametxt isEqualToString:objectNametxt])
                                        {
                                            if ([userid isEqualToString:loguserId]) {
                                                postimagecell.lbl_postStatus.text=@" Requested A Prayer";
                                            }
                                            else{
                                                postimagecell.lbl_postStatus.text=@" ";
                                            }
                                        }
                                        else{
                                            postimagecell.lbl_postStatus.text=@" Requested A Prayer";
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            //            }
            postimagecell.heightofobjectname.constant=21;
            postimagecell.topofdatelabel.constant=21.0;
            if ((get_object_user_details[@"id"] ==get_object_user_details[@"id"])&& (get_subject_user_details[@"id"] ==get_subject_user_details[@"id"])){
                if ((get_object_user_details[@"id"] ==[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"])&& (get_subject_user_details[@"id"] ==[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"])){
                    postimagecell.lbl_personName.text=@"You";
                    postimagecell.heightofobjectname.constant=0;
                    postimagecell.lbl_objectname.text=@"";
                    postimagecell.topofdatelabel.constant=0.0;
                }else{
                    NSString *userid=[NSString stringWithFormat:@"%@",get_subject_user_details[@"id"]];
                    NSString *loguserId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
                    if ( [dict[@"get_object_community_details"] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *checkDictCommunity=dict[@"get_object_community_details"];
                        if (checkDictCommunity.count!=0) {
                            if ([userid isEqualToString:loguserId]) {
                                postimagecell.lbl_personName.text=@"You";
                            }
                            else{
                                postimagecell.lbl_personName.text=[subjectName capitalizedString];
                            }
                        }
                        else
                        {
                            postimagecell.lbl_personName.text=[subjectName capitalizedString];
                        }
                    }
                    else{
                        postimagecell.lbl_personName.text=[subjectName capitalizedString];
                    }
                    // chnages to check subject name and object name equal and requested prayer
                    if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]] && [dict[@"get_subject_user_details"] isKindOfClass:[NSDictionary class]]) {
                        if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]]) {
                            NSDictionary *checkDictUser=dict[@"get_object_user_details"];
                            NSDictionary *checkDictSubUser=dict[@"get_object_user_details"];
                            if (checkDictUser.count!=0 && checkDictSubUser.count!=0) {
                                NSString   *objectNametxt=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
                                NSString   *SubjectNametxt=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
                                if([SubjectNametxt isEqualToString:objectNametxt])
                                {
                                    postimagecell.topofdatelabel.constant=0.0;
                                    postimagecell.lbl_objectname.text=@"";
                                }
                                else{
                                    NSString *defaultuser=[NSString stringWithFormat:@"%@",dict[@"post_type"]];
                                    if ([defaultuser isEqualToString:@"1"]) {
                                        postimagecell.topofdatelabel.constant=21.0;
                                        postimagecell.lbl_objectname.text=[objectName capitalizedString];
                                        
                                    }
                                    else{
                                        postimagecell.topofdatelabel.constant=0.0;
                                        postimagecell.lbl_objectname.text=@"";
                                    }
                                }
                            }
                        }
                    }
                    
                    if ([objectName isEqualToString:subjectName]||[objectName isEqualToString:@""]) {
                        postimagecell.lbl_objectname.text=@"";
                        postimagecell.topofdatelabel.constant=0.0;
                    }
                }
            }
            UILabel  * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 45, 9999)];
            label.numberOfLines=0;
            label.font=semibold;
            label.text =postimagecell.lbl_personName.text;
            CGSize maximumLabelSize = CGSizeMake(250, 9999);
            CGSize expectedSize = [label sizeThatFits:maximumLabelSize];
            postimagecell.widthofnamelbl.constant=expectedSize.width;
            postimagecell.lbl_personName.font=semibold;
            postimagecell.btn_seemorecomments.hidden=YES;
            if ([get_total_commnet isKindOfClass:[NSNull class]]){
                
                postimagecell.mainview_height.constant=0;
                postimagecell.heightof_seemorecomments.constant=0;
                postimagecell.heightofLabelconstraint.constant=label_personViewHeight;
                postimagecell.transparenttopHeight.constant=-5;
                
            }else{
                
                NSMutableArray *get_latest=dict[@"latest_comments"];
                NSString *like=[NSString stringWithFormat:@"%@",get_total_commnet[@"count"]];
                int like_count = [like intValue];
                // postimage cell
                int indexpathOfRow=10*indexPath.row;
                if (like_count==1) {
                    postimagecell.mainview_height.constant=61;
                    postimagecell.heightof_seemorecomments.constant=0;
                    postimagecell.view_comment2.hidden=YES;
                    postimagecell.view_comment3.hidden=YES;
                    postimagecell.view_comment1.hidden=NO;
                    postimagecell.heightofLabelconstraint.constant=label_personViewHeight;
                    NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                    if ([getobjcet_details isKindOfClass:[NSNull class]]){
                        postimagecell.imageview_comment1.image=[self imageFromColorDefault:[UIColor whiteColor]];
                    }
                    else
                    {
                        postimagecell.imageview_comment1.tag=indexpathOfRow;
                        postimagecell.lbl_commentname1.tag=indexpathOfRow;
                        
                        postimagecell.imageview_comment1.userInteractionEnabled=YES;
                        postimagecell.lbl_commentname1.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesturelike1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.imageview_comment1 addGestureRecognizer:commentImage_gesturelike1];
                        UITapGestureRecognizer *commentpersongesturelike1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.lbl_commentname1 addGestureRecognizer:commentpersongesturelike1];
                        //  check influencer image
                        
                        NSString *inclucercheckimage=[getobjcet_details valueForKey:@"user_type"];
                        if ([inclucercheckimage isEqualToString:@"2"]) {
                            postimagecell.commentimage1.hidden=NO;
                          [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        else{
                            postimagecell.commentimage1.hidden=YES;
                            [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        postimagecell.lbl_commentname1.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:0][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:0][@"get_object_user_details"][@"last_name"]];
                    }
                    postimagecell.pplableComment1.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:0][@"body"]];
                    detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                    self.matches = [detector matchesInString:postimagecell.pplableComment3.text options:0 range:NSMakeRange(0, postimagecell.pplableComment3.text.length)];
                    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postimagecell.pplableComment1 addGestureRecognizer:singleTap];
                    [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment3];
                }else  if (like_count==2) {
                    postimagecell.view_comment3.hidden=YES;
                    postimagecell.view_comment1.hidden=NO;
                    postimagecell.view_comment2.hidden=NO;
                    postimagecell.mainview_height.constant=122;
                    postimagecell.heightof_seemorecomments.constant=0;
                    postimagecell.heightofLabelconstraint.constant=label_personViewHeight;
                    NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                    NSDictionary *getobjcet_details2=[get_latest objectAtIndex:1][@"get_object_user_details"];
                    if ([getobjcet_details isKindOfClass:[NSNull class]]){
                        postimagecell.imageview_comment3.image=[self imageFromColorDefault:[UIColor whiteColor]];
                    }
                    else
                    {
                        postimagecell.imageview_comment1.tag=indexpathOfRow;
                        postimagecell.lbl_commentname1.tag=indexpathOfRow;
                        
                        postimagecell.imageview_comment1.userInteractionEnabled=YES;
                        postimagecell.lbl_commentname1.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.imageview_comment1 addGestureRecognizer:commentImage_gesture1];
                        UITapGestureRecognizer *commentpersongesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.lbl_commentname1 addGestureRecognizer:commentpersongesture1];
                        NSString *inclucerimagecomment=[getobjcet_details valueForKey:@"user_type"];
                        if ([inclucerimagecomment isEqualToString:@"2"]) {
                            postimagecell.commentimage1.hidden=NO;
                           [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        else
                        {
                            postimagecell.commentimage1.hidden=YES;
                            [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                                          
                        postimagecell.lbl_commentname1.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:0][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:0][@"get_object_user_details"][@"last_name"]];
                    }
                    if ([getobjcet_details2 isKindOfClass:[NSNull class]]){
                        postimagecell.imageview_comment2.image=[UIImage imageNamed:@"person"];
                    }
                    else
                    {
                        postimagecell.imageview_comment2.tag=indexpathOfRow+1;
                        postimagecell.lbl_commentname2.tag=indexpathOfRow+1;
                        postimagecell.imageview_comment2.userInteractionEnabled=YES;
                        postimagecell.lbl_commentname2.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.imageview_comment2 addGestureRecognizer:commentImage_gesture2];
                        UITapGestureRecognizer *commentpersongesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.lbl_commentname2 addGestureRecognizer:commentpersongesture2];
                        NSString *imageinfluencer=[NSString stringWithFormat:@"%@",[getobjcet_details2 valueForKey:@"user_type"]];
                        if ([imageinfluencer isEqualToString:@"2"]) {
                            postimagecell.commentimage2.hidden=NO;
                          [postimagecell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        else{
                            postimagecell.commentimage2.hidden=YES;
                            [postimagecell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        postimagecell.lbl_commentname2.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:1][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:0][@"get_object_user_details"][@"last_name"]];
                    }
                    postimagecell.pplableComment1.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:0][@"body"]];
                    self.matches = [detector matchesInString:postimagecell.pplableComment3.text options:0 range:NSMakeRange(0, postimagecell.pplableComment3.text.length)];
                    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postimagecell.pplableComment1 addGestureRecognizer:singleTap];
                    [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment3];
                    postimagecell.pplableComment2.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:1][@"body"]];
                    self.matches = [detector matchesInString:postimagecell.pplableComment2.text options:0 range:NSMakeRange(0, postimagecell.pplableComment2.text.length)];
                    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postimagecell.pplableComment2 addGestureRecognizer:singleTap1];
                    [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment2];
                }else  if (like_count>=3) {
                    
                    postimagecell.mainview_height.constant=183;
                    postimagecell.view_comment1.hidden=NO;
                    postimagecell.view_comment2.hidden=NO;
                    postimagecell.view_comment3.hidden=NO;
                    NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                    NSDictionary *getobjcet_details2=[get_latest objectAtIndex:1][@"get_object_user_details"];
                    NSDictionary *getobjcet_details3=[get_latest objectAtIndex:1][@"get_object_user_details"];
                    if ([getobjcet_details isKindOfClass:[NSNull class]]){
                        postimagecell.imageview_comment1.image=[self imageFromColorDefault:[UIColor whiteColor]];
                    }
                    else
                    {
                        //    comment1 tap gesture
                        postimagecell.imageview_comment1.tag=indexpathOfRow;
                        postimagecell.lbl_commentname1.tag=indexpathOfRow;
                        postimagecell.imageview_comment1.userInteractionEnabled=YES;
                        postimagecell.lbl_commentname1.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.imageview_comment1 addGestureRecognizer:commentImage_gesture];
                        UITapGestureRecognizer *commentpersongesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.lbl_commentname1 addGestureRecognizer:commentpersongesture];
                        NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                        NSDictionary *getobjcet_details1=[get_latest objectAtIndex:0][@"get_object_user_details"];
                        if([getobjcet_details isKindOfClass:[NSDictionary class]]&&[getobjcet_details1 isKindOfClass:[NSDictionary class]])
                        {
                            NSString *checkinfluencerikmagecomment=[NSString stringWithFormat:@"%@",[getobjcet_details1 valueForKey:@"user_type"]];
                            if ([checkinfluencerikmagecomment isEqualToString:@"2"]) {
                                postimagecell.commentimage1.hidden=NO;
                              [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                            }
                            else{
                                 postimagecell.commentimage1.hidden=YES;
                                [postimagecell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                            }
                            postimagecell.lbl_commentname1.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:0][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:0][@"get_object_user_details"][@"last_name"]];
                        }
                    }
                    if ([getobjcet_details2 isKindOfClass:[NSNull class]]){
                        postimagecell.imageview_comment2.image=[self imageFromColorDefault:[UIColor whiteColor]];
                    }
                    else
                    {
                        ////                       second comment
                        //
                        postimagecell.imageview_comment2.tag=indexpathOfRow+1;
                        postimagecell.lbl_commentname2.tag=indexpathOfRow+1;
                        postimagecell.imageview_comment2.userInteractionEnabled=YES;
                        postimagecell.lbl_commentname2.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.imageview_comment2 addGestureRecognizer:commentImage_gesture2];
                        UITapGestureRecognizer *commentpersongesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.lbl_commentname2 addGestureRecognizer:commentpersongesture2];
                        //
                        //                        // end
                        
                        NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                        NSDictionary *getobjcet_details1=[get_latest objectAtIndex:1][@"get_object_user_details"];
                        // commnetview 2
                        
                        if([getobjcet_details isKindOfClass:[NSDictionary class]]&&[getobjcet_details1 isKindOfClass:[NSDictionary class]])
                        {
                             NSString *checkinfluencerikmagecomment2=[NSString stringWithFormat:@"%@",[getobjcet_details1 valueForKey:@"user_type"]];
                            
                            if ([checkinfluencerikmagecomment2 isEqualToString:@"2"]) {
                                postimagecell.commentimage2.hidden=NO;
                             [postimagecell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                            }
                            else{
                              postimagecell.commentimage2.hidden=YES;
                              [postimagecell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                            }
                            postimagecell.lbl_commentname2.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:1][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:1][@"get_object_user_details"][@"last_name"]];
                        }
                    }
                    
                    // commmet view 3
                    if ([getobjcet_details3 isKindOfClass:[NSNull class]]){
                        postimagecell.imageview_comment3.image=[UIImage imageNamed:@"person"];
                    }else{
                        
                        //
                        //                    // comment3 ttap gesture
                        postimagecell.imageview_comment3.tag=indexpathOfRow+2;
                        postimagecell.lbl_commentname3.tag=indexpathOfRow+2;
                        postimagecell.imageview_comment3.userInteractionEnabled=YES;
                        postimagecell.lbl_commentname3.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.imageview_comment3 addGestureRecognizer:commentImage_gesture3];
                        
                        UITapGestureRecognizer *commentpersongesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postimagecell.lbl_commentname3 addGestureRecognizer:commentpersongesture3];
                        //
                        //                      // end
                        
                        NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                        NSDictionary *getobjcet_details1=[get_latest objectAtIndex:2][@"get_object_user_details"];
                        if([getobjcet_details isKindOfClass:[NSDictionary class]]&&[getobjcet_details1 isKindOfClass:[NSDictionary class]]){
                             NSString *checkinfluencerikmagecomment3=[NSString stringWithFormat:@"%@",[getobjcet_details1 valueForKey:@"user_type"]];
                            if ([checkinfluencerikmagecomment3 isEqualToString:@"2"]) {
                                postimagecell.commentimage3.hidden=NO;
                                 [postimagecell.imageview_comment3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:2][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                            }
                            else{
                                postimagecell.commentimage3.hidden=YES;
                               [postimagecell.imageview_comment3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:2][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                            }
                            postimagecell.lbl_commentname3.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:2][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:2][@"get_object_user_details"][@"last_name"]];
                        }
                        
                        
                    }
                    postimagecell.pplableComment1.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:0][@"body"]];
                    self.matches = [detector matchesInString:postimagecell.pplableComment1.text options:0 range:NSMakeRange(0, postimagecell.pplableComment1.text.length)];
                    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postimagecell.pplableComment1 addGestureRecognizer:singleTap];
                    [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment1];
                    postimagecell.pplableComment2.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:1][@"body"]];
                    self.matches = [detector matchesInString:postimagecell.pplableComment2.text options:0 range:NSMakeRange(0, postimagecell.pplableComment2.text.length)];
                    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postimagecell.pplableComment2 addGestureRecognizer:singleTap1];
                    [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment2];
                    postimagecell.pplableComment3.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:2][@"body"]];
                    self.matches = [detector matchesInString:postimagecell.pplableComment3.text options:0 range:NSMakeRange(0, postimagecell.pplableComment3.text.length)];
                    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postimagecell.pplableComment3 addGestureRecognizer:singleTap2];
                    [self highlightLinksWithIndex:NSNotFound custum:postimagecell.pplableComment3];
                    postimagecell.heightof_seemorecomments.constant=0;
                    // main view height
                    postimagecell.bottomofcommentView.constant=8;
                    int totalcount=[get_total_commnet[@"count"] intValue];
                    if (totalcount>3) {
                        NSString *checkMostPopularGroupSeemore=@"0";
                        if ([[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                            checkMostPopularGroupSeemore=@"1";
                        }
                        if([checkMostPopularGroupSeemore isEqualToString:@"0"])
                        {
                            postimagecell.commentViewSeemoretopHeight.constant=-40;
                            postimagecell.transparenttopHeight.constant=0;
                        }
                        else{
                            postimagecell.commentViewSeemoretopHeight.constant=-25;
                            postimagecell.transparenttopHeight.constant=0;
                        }
                        postimagecell.bottomofcommentView.constant=25;
                        postimagecell.heightof_seemorecomments.constant=32;
                    }
                    postimagecell.heightofLabelconstraint.constant=label_personViewHeight;
                    postimagecell.btn_seemorecomments.hidden=NO;
                    postimagecell.topgroupHeight.constant=37;


                }

            }
            postimagecell.btn_seemorecomments.tag=indexPath.row;
            [postimagecell.btn_seemorecomments addTarget:self action:@selector(btncommentsmore_act:) forControlEvents:UIControlEventTouchUpInside];
            postimagecell.lbl_post.font=light;
            if (myselfwall==YES) {
                postimagecell.dropdownbutton.hidden=NO;
            }
            postimagecell.view_btnmoareimage.hidden=YES;
            if (get_attachments.count==1) {
                postimagecell.view_btnmoareimage.hidden=YES;
            }else{
                NSString *countofimage=[NSString stringWithFormat:@"+%lu",[get_attachments count]-1];
                [postimagecell.btn_moreImages setTitle:countofimage forState:UIControlStateNormal];
                postimagecell.view_btnmoareimage.hidden=NO;
            }
            postimagecell.dropdownbutton.tag=indexPath.row;
            [postimagecell.dropdownbutton addTarget:self action:@selector(dropdownbtnact:) forControlEvents:UIControlEventTouchUpInside];
            // changed here top data label
            postimagecell.topofdatelabel.constant=-3;
        }
        else if(postcell)
        {
            NSString *postType1=dict[@"post_type"];
            int post_val=[postType1 intValue];
            float width=[UIScreen mainScreen].bounds.size.width-(20);
            postcell.labelPP.preferredMaxLayoutWidth = width; // This is necessary to allow the label
            if (IS_IPHONE4||IS_IPHONE5) {
                postcell.lbl_postStatus.numberOfLines = 1;
                postcell.lbl_postStatus.minimumScaleFactor = 0.1;
                postcell.lbl_postStatus.adjustsFontSizeToFitWidth = YES;
            }
            
            // Most_popular cell
            NSString *checkMostPopularGroup;
            if (![[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                
                checkMostPopularGroup=@"nomost_popularData";
            }
            else{
                
                checkMostPopularGroup=@"1";
            }
            
            NSDictionary *GroupDictNameDict=[dict valueForKey:@"get_object_community_details"];
            NSString *groupName;
            if ([GroupDictNameDict isKindOfClass:[NSDictionary class]]) {
                groupName=[GroupDictNameDict valueForKey:@"name"];
            }
            // check group top
            
            if([checkMostPopularGroup isEqualToString:@"0"]||[checkMostPopularGroup isEqualToString:@"nomost_popularData"] ) {
                postcell.topPostGroup.hidden=YES;
                postcell.groupViewname.hidden=YES;
                postcell.seeMoreGroupComment.hidden=YES;
                postcell.ToppostBitView.constant=0;
                postcell.topPostGroupHeight.constant=0;
                postcell.groupviewnameHeight.constant=0;
                postcell.testingXpos.constant=0;
                
            }
            else{
                NSString *combinedString=[NSString stringWithFormat:@"Recent posts in %@",groupName];
                NSString *combinedStringbtn=[NSString stringWithFormat:@"See More in %@ >>",groupName];
                
                CGSize maximumLabelSize = CGSizeMake(9999, 9999);
                CGSize expectedSize = [postcell.topPostGroup sizeThatFits:maximumLabelSize];
                postcell.topPostGroup.hidden=NO;
                postcell.seeMoreGroupComment.hidden=NO;
                postcell.seeMoreGroupCommentHeight.constant=39;
                postcell.ToppostBitView.constant=9;
                if(expectedSize.width>350  && !IS_IPHONE6PLUS)
                {
                    postcell.topPostGroupHeight.constant=42;
                }
                else
                {
                    postcell.topPostGroupHeight.constant=22;
                }
                postcell.groupviewnameHeight.constant=40;
                postcell.testingXpos.constant=11;
                UIFont *seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:16];
                if(IS_IPHONE4)
                {
                    seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:12];
                    [postcell.topPostGroup setFont:[UIFont fontWithName:@"Avenir-Medium" size:16]];
                    postcell.topPostGroup.text=combinedString;
                    
                }
                else if (IS_IPHONE5)
                {
                    seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:12];
                    [postcell.topPostGroup setFont:[UIFont fontWithName:@"Avenir-Medium" size:16]];
                    postcell.topPostGroup.text=combinedString;
                }
                else if (IS_IPHONE6)
                {
                    seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:14];
                    postcell.topPostGroup.text=combinedString;

                }
                else if (IS_IPHONE6PLUS)
                {
                    seeMoreGroupFont = [UIFont fontWithName:@"Avenir-Medium" size:16];
                    postcell.topPostGroup.text=combinedString;

                }
                
                NSMutableAttributedString * string =[[NSMutableAttributedString alloc] initWithString:combinedStringbtn attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone),NSFontAttributeName : seeMoreGroupFont,NSForegroundColorAttributeName: [self colorFromHexString:@"00BAF5"]}];
                postcell.seeMoreGroupComment.tag=indexPath.row;
                [postcell.seeMoreGroupComment setAttributedTitle:string forState:UIControlStateNormal];
                [postcell.seeMoreGroupComment  addTarget:self action:@selector(imagecellSeemoreGroup:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            //           see more popular Comment
            
            postcell.bottomofcommentView.constant=16;
            if ( [dict[@"get_total_praised"] isKindOfClass:[NSNull class]]) {
                [postcell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
                if (post_val==1||post_val==3) {
                    postcell.imageview_praised.text=@"Praise This";
                    [postcell.btn_praised setTitle:@"Praise This" forState:UIControlStateNormal];
                    [postcell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                    [postcell.btn_praisedCount setTitle:@"No Praises yet" forState:UIControlStateNormal];
//                    [postcell.btn_praised setTitle:@"Praise This" forState:UIControlStateNormal];
//                    [postcell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                }else{
                    postcell.imageview_praised.text=@"Pray For This";
                    [postcell.btn_praised setTitle:@"Pray For This" forState:UIControlStateNormal];
                    [postcell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                    [postcell.btn_praisedCount setTitle:@"No Prayers yet" forState:UIControlStateNormal];
//                    [postcell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                }
                postcell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_unselected"];
                postcell.imageview_praised.textColor=[UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1.0];
                
            }else{
                get_total_praised=dict[@"get_total_praised"];
                NSString *like=[NSString stringWithFormat:@"%@",dict[@"is_like"]];
                int like_count = [like intValue];
                if (like_count==1) {
                    NSString *like1=[NSString stringWithFormat:@"%@",get_total_praised[@"count"]];
                    int main_like= [like1 intValue];
                    NSString *total_count;
                    if (post_val==1||post_val==3) {
                        
                        if (main_like>=3) {
                            total_count=[NSString stringWithFormat:@"You and %d others Praised This",main_like-1];
                        }
                        else{
                           total_count=[NSString stringWithFormat:@"You and %d other Praised This",main_like-1];
                        }
                        if (main_like==1) {
                            total_count=[NSString stringWithFormat:@"You Praised This"];
                        }
                        postcell.imageview_praised.text=@"You Praised This";
                        [postcell.btn_praised setTitle:@"You Praised This" forState:UIControlStateNormal];
                        postcell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_selected"];
                        postcell.imageview_praised.textColor=[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
                        [postcell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
                        [postcell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                        [postcell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                        
                    }else{
                       
                        if (main_like>=3) {
                             total_count=[NSString stringWithFormat:@"You and %d others Prayed for This",main_like-1];
                        }
                        else{
                            total_count=[NSString stringWithFormat:@"You and %d other Prayed for This",main_like-1];
                        }
                        
                        if (main_like==1) {
                            total_count=[NSString stringWithFormat:@"You Prayed for This"];
                        }
                        postcell.imageview_praised.text=@"You Prayed";
                        [postcell.btn_praised setTitle:@"You Prayed" forState:UIControlStateNormal];
                        postcell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_selected"];
                        postcell.imageview_praised.textColor=[UIColor colorWithRed:0.0/255.0 green:186.0/255.0 blue:245.0/255.0 alpha:1];
                        [postcell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
                        [postcell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                        [postcell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                    }
                    
                }else{
                    if (post_val==1||post_val==3) {
                        postcell.imageview_praised.text=@"Praise This";
                        [postcell.btn_praised setTitle:@"Praise This" forState:UIControlStateNormal];
                        [postcell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                        NSString *total_count=[NSString stringWithFormat:@"%@ Praised This",[Constant1 getStringObject:get_total_praised[@"count"]]];
                        [postcell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
                        [postcell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                    }else{
                        postcell.imageview_praised.text=@"Pray For This";
                        [postcell.btn_praised setTitle:@"Pray For This" forState:UIControlStateNormal];
                        [postcell.btn_praised setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                        NSString *total_count=[NSString stringWithFormat:@"%@ Prayed for This",[Constant1 getStringObject:get_total_praised[@"count"]]];
                        [postcell.btn_praised addTarget:self action:@selector(btnpraised_act:) forControlEvents:UIControlEventTouchUpInside];
                        [postcell.btn_praisedCount setTitle:total_count forState:UIControlStateNormal];
                    }
                    postcell.praisedbtn_imageview.image=[UIImage imageNamed:@"praised_unselected"];
                    postcell.imageview_praised.textColor=[UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1.0];
                }
                
            }
            if ([get_total_commnet isKindOfClass:[NSNull class]]){
                [postcell.btn_commentsCount setTitle:@"0 Comments" forState:UIControlStateNormal];
            }else{
                NSString *total_count=[NSString stringWithFormat:@"%@ Comments",[Constant1 getStringObject:get_total_commnet[@"count"]]];
                [postcell.btn_commentsCount setTitle:total_count forState:UIControlStateNormal];
            }
            //1
            postcell.lbl_personName.text=subjectName;
            //check for influencer image
            checkinclencerImageGlobal=[NSString stringWithFormat:@"%@",[dict valueForKey:@"is_influencer"]];
            if ([checkinclencerImageGlobal isEqualToString:@"0"]) {
                postcell.main_profile_image.hidden=YES;
                [postcell.imageview_person sd_setImageWithURL:[NSURL URLWithString:profile_picture] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
            }
            else{
                postcell.main_profile_image.hidden=NO;
                [postcell.imageview_person sd_setImageWithURL:[NSURL URLWithString:profile_picture] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
            }
            postcell.lbl_date.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:dict[@"updated_at"]]];
            NSString *post_Imagestring;
            NSMutableArray *imagearr=dict[@"get_attachments"];
            for(NSDictionary *dictionary in imagearr) {
                post_Imagestring=[NSString stringWithFormat:@"%@",dictionary[@"file"]];
            }
            [postcell.btn_Comments addTarget:self action:@selector(btncomments_act:) forControlEvents:UIControlEventTouchUpInside];
            [postcell.btn_commentsCount addTarget:self action:@selector(btncomments_act:) forControlEvents:UIControlEventTouchUpInside];
            postcell.btn_Comments.tag=indexPath.row;
            postcell.btn_commentsCount.tag=indexPath.row;
            postcell.btn_praised.tag=indexPath.row;
            postcell.btn_praisedCount.tag=indexPath.row;
            NSString *string1=dict[@"content"];
            // pplable implementation
            
            NSError *error = nil;
            postcell.labelPP.text=string1;
            UIFont *font = semibold;
            CGRect rect = [string1 boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{NSFontAttributeName : font}
                                                context:nil];
            countLineOfLable=[self lineCountForText:string1];
//            NSLog(@"countLineof lable %d",countLineOfLable);
            detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
            self.matches = [detector matchesInString:postcell.labelPP.text options:0 range:NSMakeRange(0, postcell.labelPP.text.length)];
//            postcell.labelPP.tag=indexPath.row;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            [postcell.labelPP addGestureRecognizer:singleTap];
            [self highlightLinksWithIndex:NSNotFound custum:postcell.labelPP];
            postcell.btn_seemorecomments.hidden=YES;
            
            if (countLineOfLable>=6) {
                postcell.btn_readmorepost.hidden=NO;
                label_personViewHeight=120;
            }
            else{
                postcell.btn_readmorepost.hidden=YES;
                label_personViewHeight=rect.size.height;
            }
            postcell.heightofpostLabelconstraint.constant= label_personViewHeight;
            postcell.btn_readmorepost.tag=indexPath.row;
            [postcell.btn_readmorepost addTarget:self action:@selector(readMore:) forControlEvents:UIControlEventTouchUpInside];
            postcell.lbl_objectname.userInteractionEnabled=YES;
            postcell.lbl_personName.userInteractionEnabled=YES;
            postcell.imageview_person.userInteractionEnabled=YES;
            UITapGestureRecognizer *personImage_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch:)];
            [postcell.imageview_person addGestureRecognizer:personImage_gesture];
            UITapGestureRecognizer *subjectname_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch:)];
            [postcell.lbl_personName addGestureRecognizer:subjectname_gesture];
            UITapGestureRecognizer *objectname_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(object_touch:)];
            [postcell.lbl_objectname addGestureRecognizer:objectname_gesture];
            [postcell.lbl_objectname addGestureRecognizer:objectname_gesture];
            postcell.lbl_objectname.tag=indexPath.row;
            postcell.lbl_personName.tag=indexPath.row;
            postcell.imageview_person.tag=indexPath.row;
            if (post_val==1||post_val==3) {
                postcell.lbl_postStatus.text=@"";
                if ([checkprofile isEqualToString:@"check"]) {
                    postcell.lbl_postStatus.text=@" ";
                    checkprofile=@"";
                }
                else
                {
                    if ( [dict[@"get_object_community_details"] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *checkDictCommunity=dict[@"get_object_community_details"];
                        if (checkDictCommunity.count!=0) {
                            postcell.lbl_postStatus.text=@" ";
                        }
                        else
                        {
                            if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]] && [dict[@"get_subject_user_details"] isKindOfClass:[NSDictionary class]]) {
                                if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]]) {
                                    NSDictionary *checkDictUser=dict[@"get_object_user_details"];
                                    NSDictionary *checkDictSubUser=dict[@"get_object_user_details"];
                                    if (checkDictUser.count!=0 && checkDictSubUser.count!=0) {
                                        NSString   *objectNametxt=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
                                        NSString   *SubjectNametxt=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
                                        NSString *userid=[NSString stringWithFormat:@"%@",get_subject_user_details[@"id"]];
                                        NSString *loguserId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
                                        
                                        if([SubjectNametxt isEqualToString:objectNametxt])
                                        {
                                            if ([userid isEqualToString:loguserId]) {
                                            }
                                            else{
                                                postcell.lbl_postStatus.text=@" ";
                                            }
                                        }
                                        else{
                                            postcell.lbl_postStatus.text=@"  wrote on ";
                                            
                                        }
                                    }
                                }
                            }
                            
                        }
                        
                    }
                }
                
                postcell.lbl_postStatus.textAlignment =NSTextAlignmentLeft;
            }else{
                NSString *defaultuser=[NSString stringWithFormat:@"%@",get_object_user_details[@"id"]];
                if ([defaultuser isEqualToString:@"1"]) {
                    postcell.lbl_postStatus.text=@"";
                }
                else{
                    // changes requested prayer
                    postimagecell.lbl_postStatus.text=@" Requested A Prayer";
                    if ( [dict[@"get_object_community_details"] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *checkDictCommunity=dict[@"get_object_community_details"];
                        if (checkDictCommunity.count!=0) {
                            postcell.lbl_postStatus.text=@" ";
                        }
                        else
                        {
                            if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]] && [dict[@"get_subject_user_details"] isKindOfClass:[NSDictionary class]]) {
                                if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]]) {
                                    NSDictionary *checkDictUser=dict[@"get_object_user_details"];
                                    NSDictionary *checkDictSubUser=dict[@"get_object_user_details"];
                                    if (checkDictUser.count!=0 && checkDictSubUser.count!=0) {
                                        NSString   *objectNametxt=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
                                        NSString   *SubjectNametxt=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
                                        NSString *userid=[NSString stringWithFormat:@"%@",get_subject_user_details[@"id"]];
                                        NSString *loguserId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
                                        if([SubjectNametxt isEqualToString:objectNametxt])
                                        {
                                            if ([userid isEqualToString:loguserId]) {
                                                postcell.lbl_postStatus.text=@" Requested A Prayer";
                                            }
                                            else{
                                                postcell.lbl_postStatus.text=@" ";
                                            }
                                        }
                                        else{
                                            postcell.lbl_postStatus.text=@" Requested A Prayer";
                                        }
                                    }
                                    
                                }
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            //            }
            postcell.heightofobjectname.constant=21;
            postcell.topofdatelabel.constant=21.0;
            if ((get_object_user_details[@"id"] ==get_object_user_details[@"id"])&& (get_subject_user_details[@"id"] ==get_subject_user_details[@"id"])){
                if ((get_object_user_details[@"id"] ==[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"])&& (get_subject_user_details[@"id"] ==[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"])){
                    postcell.lbl_personName.text=@"You";
                    postcell.heightofobjectname.constant=0;
                    postcell.lbl_objectname.text=@"";
                    postcell.topofdatelabel.constant=0.0;
                }else{
                    // changes subject name  ac to group
                    NSString *userid=[NSString stringWithFormat:@"%@",get_subject_user_details[@"id"]];
                    NSString *loguserId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]];
                    if ( [dict[@"get_object_community_details"] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *checkDictCommunity=dict[@"get_object_community_details"];
                        if (checkDictCommunity.count!=0) {
                            if ([userid isEqualToString:loguserId]) {
                                postcell.lbl_personName.text=@"You";
                            }
                            else{
                                postcell.lbl_personName.text=[subjectName capitalizedString];
                            }
                        }
                        else
                        {
                            postcell.lbl_personName.text=[subjectName capitalizedString];
                            
                        }
                    }
                    // changes object name ac to requirment
                    if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]] && [dict[@"get_subject_user_details"] isKindOfClass:[NSDictionary class]]) {
                        if ( [dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]]) {
                            NSDictionary *checkDictUser=dict[@"get_object_user_details"];
                            NSDictionary *checkDictSubUser=dict[@"get_object_user_details"];
                            if (checkDictUser.count!=0 && checkDictSubUser.count!=0) {
                                NSString   *objectNametxt=[NSString stringWithFormat:@"%@ %@",get_object_user_details[@"first_name"],get_object_user_details[@"last_name"]];
                                
                                NSString   *SubjectNametxt=[NSString stringWithFormat:@"%@ %@",get_subject_user_details[@"first_name"],get_subject_user_details[@"last_name"]];
                                if([SubjectNametxt isEqualToString:objectNametxt])
                                {
                                    postcell.topofdatelabel.constant=0.0;
                                    postcell.lbl_objectname.text=@"";
                                }
                                else{
                                    NSString *defaultuser=[NSString stringWithFormat:@"%@",dict[@"post_type"]];
                                    if ([defaultuser isEqualToString:@"1"]) {
                                        postcell.topofdatelabel.constant=21.0;
                                        postcell.lbl_objectname.text=[objectName capitalizedString];
                                    }
                                    else{
                                        postcell.topofdatelabel.constant=0.0;
                                        postcell.lbl_objectname.text=@"";
                                    }
                                }
                            }
                        }
                    }
                    if ([objectName isEqualToString:subjectName]||[objectName isEqualToString:@""]) {
                        postcell.lbl_objectname.text=@"";
                        postcell.topofdatelabel.constant=0.0;
                    }
                    
                }
            }
            UILabel  * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 275, 9999)];
            
            label.font=semibold;
            label.text =postcell.lbl_personName.text;
            CGSize maximumLabelSize = CGSizeMake(275, 9999);
            CGSize expectedSize = [label sizeThatFits:maximumLabelSize];
            postcell.widthofnamelbl.constant=expectedSize.width+5;
            postcell.lbl_personName.font= semibold;
            postcell.btn_seemorecomments.hidden=YES;
            if ([get_total_commnet isKindOfClass:[NSNull class]]){
                postcell.mainview_height.constant=0;
                postcell.heightof_seemorecomments.constant=0;
                postcell.heightofpostLabelconstraint.constant=label_personViewHeight;
            }else{
                NSMutableArray *get_latest=dict[@"latest_comments"];
                NSString *like=[NSString stringWithFormat:@"%@",get_total_commnet[@"count"]];
                int like_count = [like intValue];
                
                int indexpathOfRow=10*indexPath.row;
                // comment by like_count
                if (like_count==1) {
                    postcell.mainview_height.constant=61;
                    postcell.heightof_seemorecomments.constant=0;
                    postcell.view_comment2.hidden=NO;
                    postcell.view_comment3.hidden=NO;
                    postcell.view_comment1.hidden=NO;
                    postcell.heightofpostLabelconstraint.constant=label_personViewHeight;
                    NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                    if ([getobjcet_details isKindOfClass:[NSNull class]]){
                        postcell.imageview_comment3.image=[self imageFromColorDefault:[UIColor whiteColor]];
//                        NSLog(@"no image contain number %l---%ld",(long)indexPath.row,getobjcet_details);
                    }else{
                        
                        postcell.imageview_comment3.tag=indexpathOfRow;
                        postcell.lbl_commentname3.tag=indexpathOfRow;
                        postcell.imageview_comment3.userInteractionEnabled=YES;
                        postcell.lbl_commentname3.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.imageview_comment3 addGestureRecognizer:commentImage_gesture3];
                        UITapGestureRecognizer *commentpersongesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.lbl_commentname1 addGestureRecognizer:commentpersongesture3];
                        //influcencer image check
                        NSString *incluencerimage1=[NSString stringWithFormat:@"%@",[getobjcet_details valueForKey:@"user_type"]];
                        if ([incluencerimage1 isEqualToString:@"2"]) {
                            postcell.commentimage3.hidden=NO;
                         [postcell.imageview_comment3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        else{
                            postcell.commentimage3.hidden=YES;
                           [postcell.imageview_comment3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        postcell.lbl_commentname3.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:0][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:0][@"get_object_user_details"][@"last_name"]];
                    }
                    postcell.pplableComment3.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:0][@"body"]];
                    detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                    self.matches = [detector matchesInString:postcell.pplableComment1.text options:0 range:NSMakeRange(0, postcell.pplableComment1.text.length)];
                    //            postcell.labelPP.tag=indexPath.row;
                    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postcell.pplableComment3 addGestureRecognizer:singleTap];
                    
                    [self highlightLinksWithIndex:NSNotFound custum:postcell.pplableComment1];
                    
                }else  if (like_count==2) {
                    postcell.mainview_height.constant=122;
                    postcell.heightof_seemorecomments.constant=0;
                    postcell.view_comment3.hidden=NO;
                    postcell.view_comment2.hidden=NO;
                    postcell.view_comment1.hidden=NO;

                    postcell.heightofpostLabelconstraint.constant=label_personViewHeight;
                    NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                    NSDictionary *getobjcet_details2=[get_latest objectAtIndex:1][@"get_object_user_details"];
                    if ([getobjcet_details isKindOfClass:[NSNull class]]){
                        postcell.imageview_comment3.image=[self imageFromColorDefault:[UIColor whiteColor]];
                    }else
                    {
                        postcell.imageview_comment3.tag=indexpathOfRow;
                        postcell.lbl_commentname3.tag=indexpathOfRow;
                        postcell.imageview_comment3.userInteractionEnabled=YES;
                        postcell.lbl_commentname3.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.imageview_comment3 addGestureRecognizer:commentImage_gesture3];
                        UITapGestureRecognizer *commentpersongesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.lbl_commentname3 addGestureRecognizer:commentpersongesture3];
                        // influencer image
                        NSString *incluencerimage=[NSString stringWithFormat:@"%@",[getobjcet_details valueForKey:@"user_type"]];
                        if ([incluencerimage isEqualToString:@"2"]) {
                            postcell.commentimage3.hidden=NO;
                           [postcell.imageview_comment3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                    else{
                        postcell.commentimage3.hidden=YES;
                        [postcell.imageview_comment3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        postcell.lbl_commentname3.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:0][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:0][@"get_object_user_details"][@"last_name"]];
                    }
                    if ([getobjcet_details2 isKindOfClass:[NSNull class]]){
                        postcell.imageview_comment2.image=[self imageFromColorDefault:[UIColor whiteColor]];
                        
                    }else{
                        postcell.imageview_comment2.tag=indexpathOfRow+1;
                        postcell.lbl_commentname2.tag=indexpathOfRow+1;
                        postcell.imageview_comment2.userInteractionEnabled=YES;
                        postcell.lbl_commentname2.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.imageview_comment2 addGestureRecognizer:commentImage_gesture3];
                        UITapGestureRecognizer *commentpersongesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.lbl_commentname2 addGestureRecognizer:commentpersongesture3];
                        NSString *checkinflucen21=[NSString stringWithFormat:@"%@",[getobjcet_details2 valueForKey:@"user_type"]];
                        if ([checkinflucen21 isEqualToString:@"2"]) {
                            postcell.commentimage2.hidden=NO;
                           [postcell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        else{
                            postcell.commentimage2.hidden=YES;
                             [postcell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        postcell.lbl_commentname2.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:1][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:1][@"get_object_user_details"][@"last_name"]];
                    }
                    postcell.pplableComment3.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:0][@"body"]];
                    detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                    self.matches = [detector matchesInString:postcell.pplableComment3.text options:0 range:NSMakeRange(0, postcell.pplableComment3.text.length)];
                    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postcell.pplableComment3 addGestureRecognizer:singleTap];
                    [self highlightLinksWithIndex:NSNotFound custum:postcell.pplableComment3];
                    postcell.pplableComment2.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:1][@"body"]];
                    detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                    self.matches = [detector matchesInString:postcell.pplableComment2.text options:0 range:NSMakeRange(0, postcell.pplableComment2.text.length)];
                    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postcell.pplableComment2 addGestureRecognizer:singleTap1];
                    [self highlightLinksWithIndex:NSNotFound custum:postcell.pplableComment2];
                }else  if (like_count>=3) {
                    NSDictionary *getobjcet_details=[get_latest objectAtIndex:0][@"get_object_user_details"];
                    NSDictionary *getobjcet_details2=[get_latest objectAtIndex:1][@"get_object_user_details"];
                    NSDictionary *getobjcet_details3=[get_latest objectAtIndex:2][@"get_object_user_details"];
                    postcell.view_comment1.hidden=NO;
                    postcell.view_comment2.hidden=NO;
                    postcell.view_comment3.hidden=NO;
                    if ([getobjcet_details isKindOfClass:[NSNull class]]){
                        postcell.imageview_comment1.image=[self imageFromColorDefault:[UIColor whiteColor]];
                    }else{
                        postcell.imageview_comment1.tag=indexpathOfRow;
                        postcell.lbl_commentname1.tag=indexpathOfRow;
                        postcell.imageview_comment1.userInteractionEnabled=YES;
                        postcell.lbl_commentname1.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.imageview_comment1 addGestureRecognizer:commentImage_gesture3];
                        UITapGestureRecognizer *commentpersongesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.lbl_commentname1 addGestureRecognizer:commentpersongesture3];
                        NSString *incluencercheck=[NSString stringWithFormat:@"%@",[getobjcet_details valueForKey:@"user_type"]];
                        if ([incluencercheck isEqualToString:@"2"]) {
                            postcell.commentimage1.hidden=NO;
                            [postcell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        else{
                        postcell.commentimage1.hidden=YES;
                          [postcell.imageview_comment1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:0][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        postcell.lbl_commentname1.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:0][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:0][@"get_object_user_details"][@"last_name"]];
                    }
                    if ([getobjcet_details2 isKindOfClass:[NSNull class]]){
                        postcell.imageview_comment2.image=[self imageFromColorDefault:[UIColor whiteColor]];
                    }else{
                        postcell.imageview_comment2.tag=indexpathOfRow+1;
                        postcell.lbl_commentname2.tag=indexpathOfRow+1;
                        postcell.imageview_comment2.userInteractionEnabled=YES;
                        postcell.lbl_commentname2.userInteractionEnabled=YES;
                        UITapGestureRecognizer *commentImage_gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.imageview_comment2 addGestureRecognizer:commentImage_gesture3];
                        UITapGestureRecognizer *commentpersongesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.lbl_commentname2 addGestureRecognizer:commentpersongesture3];
                        NSString *inclucenerimage2=[NSString stringWithFormat:@"%@",[getobjcet_details2 valueForKey:@"user_type"]];
                        if ([inclucenerimage2 isEqualToString:@"2"]) {
                            postcell.commentimage2.hidden=NO;
                           [postcell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        else{
                            postcell.commentimage2.hidden=YES;
                             [postcell.imageview_comment2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:1][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        
                        postcell.lbl_commentname2.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:1][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:1][@"get_object_user_details"][@"last_name"]];
                    }
                    
                    if ([getobjcet_details3 isKindOfClass:[NSNull class]]){
                        postcell.imageview_comment3.image=[self imageFromColorDefault:[UIColor whiteColor]];
                        
                    }else
                    {
                        postcell.imageview_comment3.tag=indexpathOfRow+2;
                        postcell.lbl_commentname3.tag=indexpathOfRow+2;
                        postcell.imageview_comment3.userInteractionEnabled=YES;
                        postcell.lbl_commentname3.userInteractionEnabled=YES;
                        
                        UITapGestureRecognizer *commentImage_gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.imageview_comment3 addGestureRecognizer:commentImage_gesture3];
                        
                        UITapGestureRecognizer *commentpersongesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subject_touch_comment:)];
                        [postcell.lbl_commentname3 addGestureRecognizer:commentpersongesture3];
                        
                        NSString *inclucenerimage3=[NSString stringWithFormat:@"%@",[getobjcet_details3 valueForKey:@"user_type"]];
                        if ([inclucenerimage3 isEqualToString:@"2"]) {
                            postcell.commentimage3.hidden=NO;
                           [postcell.imageview_comment3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:2][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        else{
                            postcell.commentimage3.hidden=YES;
                            [postcell.imageview_comment3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageapiUrl,[Constant1 getStringObject:[get_latest objectAtIndex:2][@"get_object_user_details"][@"usersprofile"][@"profile_picture"]]]] placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]]];
                        }
                        
                        
                        postcell.lbl_commentname3.text=[NSString stringWithFormat:@"%@ %@",[get_latest objectAtIndex:2][@"get_object_user_details"][@"first_name"],[get_latest objectAtIndex:2][@"get_object_user_details"][@"last_name"]];
                    }
                    
                    postcell.pplableComment1.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:0][@"body"]];
                    detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
                    self.matches = [detector matchesInString:postcell.pplableComment1.text options:0 range:NSMakeRange(0, postcell.pplableComment1.text.length)];
                    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postcell.pplableComment1 addGestureRecognizer:singleTap];
                    [self highlightLinksWithIndex:NSNotFound custum:postcell.pplableComment1];
                    postcell.pplableComment2.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:1][@"body"]];
                    
                    self.matches = [detector matchesInString:postcell.pplableComment2.text options:0 range:NSMakeRange(0, postcell.pplableComment2.text.length)];
                    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postcell.pplableComment2 addGestureRecognizer:singleTap1];
                    [self highlightLinksWithIndex:NSNotFound custum:postcell.pplableComment2];
                    postcell.pplableComment3.text=[NSString stringWithFormat:@"%@",[get_latest objectAtIndex:2][@"body"]];
                    self.matches = [detector matchesInString:postcell.pplableComment3.text options:0 range:NSMakeRange(0, postcell.pplableComment3.text.length)];
                    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    [postcell.pplableComment3 addGestureRecognizer:singleTap2];
                    [self highlightLinksWithIndex:NSNotFound custum:postcell.pplableComment3];
                    postcell.heightof_seemorecomments.constant=0;
                    postcell.mainview_height.constant=183;
                    postcell.bottomofcommentView.constant=8;
                    int totalcount=[get_total_commnet[@"count"] intValue];
                    if (totalcount>3) {
                        postcell.bottomofcommentView.constant=25;
                        postcell.heightof_seemorecomments.constant=32;
                        postcell.btn_seemorecomments.hidden=NO;
                    }
                    postcell.heightofpostLabelconstraint.constant=label_personViewHeight;
                }
                //                [postcell layoutIfNeeded];
                [self setNeedsUpdateConstraints];
                [self layoutIfNeeded];
            }
            // chnaged here for top of date label
            postcell.objectHeight.constant=0;
            postcell.topofdatelabel.constant=-3;
            postcell.btndeletedropdown.tag=indexPath.row;
            postcell.btndeletedropdown.layer.borderWidth=1.0f;
            postcell.btndeletedropdown.layer.borderColor=[UIColor blackColor].CGColor;
            postcell.btn_seemorecomments.tag=indexPath.row;
            [postcell.btn_seemorecomments addTarget:self action:@selector(btncommentsmore_act:) forControlEvents:UIControlEventTouchUpInside];
            postcell.lbl_date.text=[NSString stringWithFormat:@"%@",[Constant1 getStringObject:dict[@"formatted_date"]]];
            postcell.dropdownbutton.tag=indexPath.row;
            [postcell.dropdownbutton addTarget:self action:@selector(dropdownbtnact:) forControlEvents:UIControlEventTouchUpInside];
            postcell.lbl_post.font=light;
            if (myselfwall==YES) {
                postcell.dropdownbutton.hidden=NO;
            }
            
        }
        
        
    }else{
        
        //google add DFP
        NSMutableArray *targetArray=[[NSUserDefaults standardUserDefaults] valueForKey:@"Targetarray"];
        for (int i=0; i<[targetArray count]; i++) {
            NSDictionary *dictcellposition=[targetArray objectAtIndex:i];
            if ([dictcellposition isKindOfClass:[NSDictionary class]]) {
                CGFloat widthad,heightad;
                NSInteger anchorID=[[dictcellposition objectForKey:@"anchor_no"] integerValue];
                if (anchorID==indexPath.row) {
                  NSString *adID=[dictcellposition objectForKey:@"ad_id"];
                    NSString *sizeAD=[dictcellposition objectForKey:@"slot_size"];
                    if (![sizeAD isEqualToString:@""]) {
                    NSString *widthadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:0] ;
                        widthad = (CGFloat)[widthadStr floatValue];
                    NSString *heightadStr = [[sizeAD componentsSeparatedByString:@","] objectAtIndex:1];
                        heightad = (CGFloat)[heightadStr floatValue];
                        
                    CGRect screenRect = [[UIScreen mainScreen] bounds];
                    CGFloat screenWidth = screenRect.size.width;
                    advertisecell.bannerView_Heightconstraits.constant=heightad;
                        
                        advertisecell.bannerView.validAdSizes = @[
                                NSValueFromGADAdSize(GADAdSizeFromCGSize(CGSizeMake(widthad,heightad)))
                                    ];
                        if (screenWidth==widthad) {
                            advertisecell.bannerView_Widthconstraits.constant=widthad-5;
                        }
                        if (screenWidth>widthad){
                          advertisecell.bannerView_Widthconstraits.constant=widthad;
                        }
                        else{
                            
                        }
                        
                    }
                    advertisecell.bannerView.tag=indexPath.row;
                     advertisecell.bannerView.adUnitID=adID;
                     advertisecell.bannerView.delegate = (id)self;
                     advertisecell.bannerView.rootViewController =(id)self ;
                    advertisecell.main_View.clipsToBounds=YES;
                    [advertisecell.main_View addSubview: advertisecell.bannerView];
                    [advertisecell.bannerView loadRequest:[DFPRequest request]];

                }
            }
        }
        [self updateConstraints];
        [self setNeedsDisplay];
        [self layoutIfNeeded];
    }
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
}

#pragma mark DropDownMethod

-(void)dropdownbtnact:(UIButton*)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
        NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
        
        NSDictionary *dict=[arrWallFeeds objectAtIndex:indexPath.row];
        if (dict[@"get_subject_user_details"][@"id"] ==[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]){
            self.menuItems = [NSArray arrayWithObjects:@"Delete",nil];
        }else{
            self.menuItems = [NSArray arrayWithObjects:@"Report abuse",nil];
        }
        
        UIView *parentCell = sender.superview.superview.superview.superview;
        NSString *postid=[NSString stringWithFormat:@"%@",[arrWallFeeds objectAtIndex:sender.tag][@"id"]];
        if ([parentCell isKindOfClass:[FeedpostTableViewCell class]]) {
            [self.menuPopover2 dismissMenuPopover];
            FeedpostTableViewCell *cell = (FeedpostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
            // change dropdown
            CGRect dropdownViewframe;
            NSString *checkMostPopularGroupDropdown;
            if (![[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                checkMostPopularGroupDropdown=@"0";
            }
            else{
                checkMostPopularGroupDropdown=@"1";
            }
            
            if ([checkMostPopularGroupDropdown isEqualToString:@"0"]) {
                dropdownViewframe =CGRectMake(cell.dropdownbutton.frame.origin.x-35, cell.dropdownbutton.frame.origin.y+15, 70, 35);
            }
            else{
                dropdownViewframe =CGRectMake(cell.dropdownbutton.frame.origin.x-35, cell.dropdownbutton.frame.origin.y+45, 70, 35);
            }
            if (self.menuPopover2 ) {
                self.menuPopover2=nil;
            }
            self.menuPopover2=[[MLKMenuPopover alloc]initWithFrame:dropdownViewframe menuItems:self.menuItems imagemenuItems:nil isSearch:NO];
            self.menuPopover2.postid=postid;
            //            self.menuPopover2.cellindex=(int)indexPath.row;
            self.menuPopover2.cellindex=(int)sender.tag;
            self.menuPopover2.menuPopoverDelegate = self;
            
            UIView *paren=sender.superview.superview.superview;
            [self.menuPopover2 showInView:paren];
        }
        else
        {
            FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
            [self.menuPopover2 dismissMenuPopover];
            CGRect dropdownViewframe;
            // changes for dropdown menu in view profile
            //            NSString *checkMostPopularGroupDropdownimage=[NSString stringWithFormat:@"%@",[dict valueForKey:@"most_popular"]];
            NSString *checkMostPopularGroupDropdownimage;
            if (![[dict valueForKey:@"object_type"] isEqualToString:@"community"]) {
                checkMostPopularGroupDropdownimage=@"0";
            }
            else{
                checkMostPopularGroupDropdownimage=@"1";
            }
            if ([checkMostPopularGroupDropdownimage isEqualToString:@"0"]) {
                dropdownViewframe =CGRectMake(cell.dropdownbutton.frame.origin.x-32, cell.dropdownbutton.frame.origin.y+17, 70, 35);
            }
            else{
                dropdownViewframe =CGRectMake(cell.dropdownbutton.frame.origin.x-32, cell.dropdownbutton.frame.origin.y+55, 70, 35);
            }
            //            CGRect dropdownViewframe =CGRectMake(cell.dropdownbutton.frame.origin.x-35, cell.dropdownbutton.frame.origin.y+28, 70, 35);
            
            if (self.menuPopover2 ) {
                self.menuPopover2=nil;
            }
            self.menuPopover2=[[MLKMenuPopover alloc]initWithFrame:dropdownViewframe menuItems:self.menuItems imagemenuItems:nil isSearch:NO];
            self.menuPopover2.postid=postid;
            self.menuPopover2.cellindex=sender.tag;
            self.menuPopover2.menuPopoverDelegate = self;
            [self.menuPopover2 showInView:parentCell];
        }
        
    });
    
}


#pragma moreImage Method

- (void)moreImages:(UITapGestureRecognizer*)sender {
    NSDictionary *dict=[arrWallFeeds objectAtIndex:sender.view.tag];
    NSMutableArray *get_attachments=dict[@"get_attachments"];
    NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
    for (NSDictionary* image_dict in get_attachments){
        [post_imageArray addObject:image_dict[@"file"]];
    }
    [self.delegate1 FunctionOne:@"open seemoreImage" andArray:post_imageArray];
}

#pragma moreImageBtn  Method

-(void)btn_moreImages:(UIButton*)sender{
    NSDictionary *dict=[arrWallFeeds objectAtIndex:sender.tag];
    NSMutableArray *get_attachments=dict[@"get_attachments"];
    NSMutableArray* post_imageArray=[[NSMutableArray alloc]init];
    for (NSDictionary* image_dict in get_attachments){
        
        [post_imageArray addObject:image_dict[@"file"]];
    }
    [self.delegate1 FunctionOne:@"open seemoreImage" andArray:post_imageArray];
}


#pragma readmore Method
-(void)readMore:(UIButton*)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    UIView *parentCell = sender.superview;
    UIView *parentView = parentCell.superview.superview;
    if ([parentView isKindOfClass:[FeedpostTableViewCell class]]) {
        FeedpostTableViewCell *cell = (FeedpostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
        feed_cell=cell;
        [self commentsView:indexPath.row and:cell andcellType:@"Feedpost" andcell_index:sender.tag];
        return;
    }
    FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    feedImage_cell=cell;
    [self commentsView:indexPath.row and:cell andcellType:@"Feedpost1" andcell_index:sender.tag];
}

#pragma mark - VSDropdown Delegate methods.


-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents multipleSelection:(BOOL)multipleSelection
{
    
    [_dropdown setupDropdownForView:sender];
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    if (_dropdown.allowMultipleSelection)
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:[[sender titleForState:UIControlStateNormal] componentsSeparatedByString:@";"]];
    }
    else
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
        
    }
    
}
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected
{
    UIButton *btn = (UIButton *)dropDown.dropDownView;
    
    NSString *allSelectedItems = nil;
    if (dropDown.selectedItems.count > 1)
    {
        allSelectedItems = [dropDown.selectedItems componentsJoinedByString:@";"];
        
    }
    else
    {
        allSelectedItems = [dropDown.selectedItems firstObject];
        
    }
    [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    
}
//--------------------------------------------------------------------------------------------




-(void)btncommentsmore_act:(UIButton*)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    
    UIView *parentView = sender.superview.superview.superview;
    if ([parentView isKindOfClass:[FeedpostTableViewCell class]]) {
        FeedpostTableViewCell *cell = (FeedpostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
        feed_cell=cell;
        
        [self commentsView:indexPath.row and:cell andcellType:@"Feedpost" andcell_index:sender.tag];
        return;
    }
    FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    feedImage_cell=cell;
    [self commentsView:indexPath.row and:cell andcellType:@"Feedpost1" andcell_index:sender.tag];
    
    
}


#pragma mark SeemoreGroupTitleMethod

-(void)imagecellSeemoreGroup:(UIButton*)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    NSDictionary *GroupDictNameDictionary=[arrWallFeeds objectAtIndex:indexPath.row];
    NSDictionary *GroupDictNameCommunity=[GroupDictNameDictionary valueForKey:@"get_object_community_details"];
    NSString *userid,*groupname;
    if ([GroupDictNameCommunity isKindOfClass:[NSDictionary class]]) {
        userid=[NSString stringWithFormat:@"%@",[GroupDictNameCommunity valueForKey:@"id"]];
        groupname=[GroupDictNameCommunity valueForKey:@"name"];
        [self groupjoinGroup:userid groupname:groupname];
        
    }
    
}



#pragma mark CommentMethod

-(void)btncomments_act:(UIButton*)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    UIView *parentCell = sender.superview;
    UIView *parentView = parentCell.superview.superview.superview.superview;
    if ([parentView isKindOfClass:[FeedpostTableViewCell class]]) {
        //        parentView = parentView.superview;
        // [self liked:dict andmethodName:wall_like andcell_index:[sender tag] andType:@"Feedpost" andcell:parentView];
        FeedpostTableViewCell *cell = (FeedpostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
        feed_cell=cell;
        
        [self commentsView:indexPath.row and:cell andcellType:@"Feedpost" andcell_index:sender.tag];
        return;
    }
    FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    feedImage_cell=cell;
    [self commentsView:indexPath.row and:cell andcellType:@"Feedpost1" andcell_index:sender.tag];
    
}

#pragma mark PraisedPrayMethod

-(void)btnpraised_act:(UIButton*)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    NSDictionary *postcheckDict=[arrWallFeeds objectAtIndex:sender.tag];
    NSDictionary *getSubjectDetail=[postcheckDict objectForKey:@"get_subject_user_details"];
    NSString *subjectName;
    if ([getSubjectDetail isKindOfClass:[NSDictionary class]]) {
        subjectName=[NSString stringWithFormat:@"%@ %@",[getSubjectDetail objectForKey:@"first_name"],[getSubjectDetail objectForKey:@"last_name"]];
    }
    else{
        subjectName=@"";
    }
    if ([sender.titleLabel.text isEqualToString:@"You Prayed"]){
        checkYouprayed=@"yes";
    }
    
    if ([postcheckDict isKindOfClass:[NSDictionary class]]) {
        postcheck=[NSString stringWithFormat:@"%@",[postcheckDict valueForKey:@"post_type"]];
    }
    if ([sender.titleLabel.text isEqualToString:@"Unpraised"]){
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
        NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
        NSDictionary *object_dict=[arrWallFeeds objectAtIndex:sender.tag];
        NSDictionary *dict = @{
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"user_id" :userID,
                               @"object_id":object_dict[@"id"],
                               @"object_type":@"post",
                               
                               };
        UIView *parentCell = sender.superview;
        UIView *parentView = parentCell.superview.superview.superview.superview;
        if ([parentView isKindOfClass:[FeedpostTableViewCell class]]) {
            FeedpostTableViewCell *cell = (FeedpostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
            feed_cell=cell;
            [self liked1:dict andmethodName:wall_unlike andcell_index:[sender tag] andType:@"Feedpost" andfeedcell:feed_cell andfeedimage_cell:nil checkservicelikeunlike:@"wall_unlike" subjectNamePost:subjectName];
            return;
        }
        FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
        feedImage_cell=cell;
        [self liked1:dict andmethodName:wall_unlike andcell_index:[sender tag] andType:@"Feedpost1" andfeedcell:nil andfeedimage_cell:feedImage_cell checkservicelikeunlike:@"wall_unlike" subjectNamePost:subjectName];
        
    }
    if ([sender.titleLabel.text isEqualToString:@"Praise This"]||[sender.titleLabel.text isEqualToString:@"Pray For This"]){
        
        if ([sender.titleLabel.text isEqualToString:@"Praise"]){
            [TenjinSDK sendEventWithName:@"Praise_Button_Feed_Tapped"];
            [FBSDKAppEvents logEvent:@"Praise_Button_Feed_Tapped"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Praise_Button_Feed_Tapped"]];
            
        }else if ([sender.titleLabel.text isEqualToString:@"Pray For This"]){
            [TenjinSDK sendEventWithName:@"PrayForThis_Button_Feed_Tapped"];
            [FBSDKAppEvents logEvent:@"PrayForThis_Button_Feed_Tapped"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"PrayForThis_Button_Feed_Tapped"]];
        }
        NSDictionary *object_dict=[arrWallFeeds objectAtIndex:sender.tag];
        NSDictionary *dict = @{
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"user_id" :userID,
                               @"object_id":object_dict[@"id"],
                               @"object_type":@"post",
                               };
        UIView *parentCell = sender.superview;
        UIView *parentView = parentCell.superview.superview.superview.superview;
        if ([parentView isKindOfClass:[FeedpostTableViewCell class]]) {
            FeedpostTableViewCell *cell = (FeedpostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
            feed_cell=cell;
            [self liked1:dict andmethodName:wall_like andcell_index:[sender tag] andType:@"Feedpost" andfeedcell:feed_cell andfeedimage_cell:nil checkservicelikeunlike:@"wall_like" subjectNamePost:subjectName];
            return;
        }
        FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
        feedImage_cell=cell;
        [self liked1:dict andmethodName:wall_like andcell_index:[sender tag] andType:@"Feedpost1" andfeedcell:nil andfeedimage_cell:feedImage_cell checkservicelikeunlike:@"wall_like" subjectNamePost:subjectName];
    }else{
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
        NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
        NSDictionary *object_dict=[arrWallFeeds objectAtIndex:sender.tag];
        NSDictionary *dict = @{
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"user_id" :userID,
                               @"object_id":object_dict[@"id"],
                               @"object_type":@"post",
                               };
        UIView *parentCell = sender.superview;
        
        UIView *parentView = parentCell.superview.superview.superview.superview;
        if ([parentView isKindOfClass:[FeedpostTableViewCell class]]) {
            FeedpostTableViewCell *cell = (FeedpostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
            feed_cell=cell;
            [self liked1:dict andmethodName:wall_unlike andcell_index:[sender tag] andType:@"Feedpost" andfeedcell:feed_cell andfeedimage_cell:nil checkservicelikeunlike:@"wall_unlike" subjectNamePost:subjectName];
            return;
        }
        FeedwithImagePostTableViewCell *cell = (FeedwithImagePostTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
        feedImage_cell=cell;
        [self liked1:dict andmethodName:wall_unlike andcell_index:[sender tag] andType:@"Feedpost1" andfeedcell:nil andfeedimage_cell:feedImage_cell checkservicelikeunlike:@"wall_unlike" subjectNamePost:subjectName];
        
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [pullToRefreshManager_ tableViewScrolled];
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    CGFloat sectionHeaderHeight=0;
    if ([getVar.headerDict count]>0) {
        sectionHeaderHeight=390;
    }
    else{
     sectionHeaderHeight = 170;

    }
    if (scrollView.contentOffset.y<=sectionHeaderHeight && scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
    
}

#pragma SuggestedInfluencerImageMethod

-(void)suggetedImageview:(NSDictionary*)getDict{
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    getVar.checkSuggetedProfile=YES;
    getVar.checksuggestedInfluencerNotification=YES;
//    NSLog(@"dict %@",getDict);
    NSDictionary *DictValue=[getDict valueForKey:@"groupData"];
    if ([DictValue isKindOfClass:[NSDictionary class]]) {
        [self profileView:friendprofile anfriend_ID:DictValue[@"id"] influencerValue:@"1"];
    }
}
#pragma SuggestedInfluencerProfileMethod

-(void)suggeteduserProfile:(NSDictionary*)getDict{
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    getVar.checkSuggetedProfile=YES;
    getVar.checksuggestedInfluencerNotification=YES;
    NSDictionary *DictValue=[getDict valueForKey:@"groupData"];
    if ([DictValue isKindOfClass:[NSDictionary class]]) {
        [self profileView:friendprofile anfriend_ID:DictValue[@"id"] influencerValue:@"1"];
    }
 
}

#pragma  mark Follow suggetsed method

-(void)FollowbtnMethod:(NSDictionary*)getDict{
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    getVar.checkSuggetedProfile=YES;
    getVar.checksuggestedInfluencerNotification=YES;
    checkviewwillApear=@"noviewappear";
    NSDictionary *DictValue=[getDict valueForKey:@"groupData"];
    NSString *user_id=[NSString stringWithFormat:@"%@",[DictValue valueForKey:@"id"]];
    NSString *nameInfluences=[NSString stringWithFormat:@"%@ %@",[DictValue valueForKey:@"first_name"],[DictValue valueForKey:@"last_name"]];
    [self followInfluecerMethod:user_id sender_tag:@"95" influencerName:nameInfluences];
}


- (void)object_touch:(UITapGestureRecognizer*)sender {
    NSDictionary *dict=[arrWallFeeds objectAtIndex:sender.view.tag];
    NSString *checkInfluecer=[NSString stringWithFormat:@"%@",[dict valueForKey:@"is_influencer"]];
    [self profileView:friendprofile anfriend_ID:dict[@"subject_id"] influencerValue:checkInfluecer];
}
#pragma wallSubjectProfile Method

- (void)subject_touch:(UITapGestureRecognizer*)sender {
    NSDictionary *dict=[arrWallFeeds objectAtIndex:sender.view.tag];
    // for incfluencer profile
    NSString *checkInfluecer=[NSString stringWithFormat:@"%@",[dict valueForKey:@"is_influencer"]];
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.wallviewSubjectclick=YES;

    [self profileView:friendprofile anfriend_ID:dict[@"subject_id"] influencerValue:checkInfluecer];
    
}
// comment profile

#pragma mark commentwallViewProfileMethod

- (void)subject_touch_comment:(UITapGestureRecognizer*)sender {
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.wallviewSubjectclick=YES;
    int index=(int)(sender.view.tag/10);
    int subIndex=(int)(sender.view.tag%10);
    NSDictionary *dict=[arrWallFeeds objectAtIndex:index];
    NSArray *latestComment=[dict valueForKey:@"latest_comments"];
    NSDictionary *userData=[latestComment objectAtIndex:subIndex];
    NSDictionary *get_object_user_detailsDict=[userData valueForKey:@"get_object_user_details"];
    if ([get_object_user_detailsDict isKindOfClass:[NSDictionary class]]) {
        if (get_object_user_detailsDict !=NULL) {
            NSString *userid=[NSString stringWithFormat:@"%@",[get_object_user_detailsDict valueForKey:@"id"]];
            NSString *checkInfluecer=[NSString stringWithFormat:@"%@",[get_object_user_detailsDict valueForKey:@"user_type"]];
            [self profileView:friendprofile anfriend_ID:userid influencerValue:checkInfluecer];
        }
    }
}

-(void)cancel_popup{
//    [enterOldPawwordTXT resignFirstResponder];
    [nepopup dismiss];
}

#pragma mark RequestStatusMethod
-(void)request_Status:(UIButton*)sender{
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if (self.dismischeck==YES) {
        getVar.dismischeckViewcontroller=YES;
    }
    NSString *checkinfluencer=[Constant1 isStrEmpty:checkinclencerImageGlobal];
    
    if (userID) {
        NSDictionary *dict=@{@"objectID":userID,
                             @"subjectID":[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                             @"influcencerimagae":checkinfluencer,
                             };
        NSMutableArray *arrofdict=[[NSMutableArray alloc]initWithObjects:dict, nil];
        [self.delegate1 FunctionOne:@"open statusView"andArray:arrofdict];
    }
    
}

#pragma mark RequestPrayerMethod

-(void)request_Prayer:(UIButton*)sender{
    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (self.dismischeck==YES) {
        getVar.dismischeckViewcontroller=YES;
    }
    NSString *checkinfluencer=[Constant1 isStrEmpty:checkinclencerImageGlobal];
    if (userID) {
        NSDictionary *dict=@{@"objectID":userID,
                             @"subjectID":[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                             @"influcencerimagae":checkinfluencer,
                             };
        NSMutableArray *arrofdict=[[NSMutableArray alloc]initWithObjects:dict, nil];
        [self.delegate1 FunctionOne:@"open prayer"andArray:arrofdict];
    }
}


-(void)see_more:(UIButton*)sender{
    
    [self.delegate1 btn_pressed:@"seemore" andtotal_comments:all_comments
                        andpost:commment_info  andCellindex:sender.tag];
    
}




-(void)commentsView:(NSUInteger)userid and:(id)cell andcellType:(NSString*)celltype andcell_index:(NSUInteger)cell_index{
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSString *imageUrlseemore;
    NSDictionary *object_dict=[arrWallFeeds objectAtIndex:userid];
    NSDictionary *getSubjectDict=[object_dict objectForKey:@"get_subject_user_details"];
    NSString *subjectName;
    if ([getSubjectDict isKindOfClass:[NSDictionary class]]) {
        subjectName=[NSString stringWithFormat:@"%@ %@",[getSubjectDict objectForKey:@"first_name"],[getSubjectDict objectForKey:@"last_name"]];
        NSDictionary *userProfilepicDict1=[getSubjectDict objectForKey:@"usersprofile"];
        imageUrlseemore=[userProfilepicDict1 objectForKey:@"profile_picture"];
        
    }
    else{
        subjectName=@"";
    }
    NSString  *objectNametxt=@"";
    NSString *cellindexpath=[NSString stringWithFormat:@"%lu",(unsigned long)cell_index];
    if ( [object_dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]] && [object_dict[@"get_subject_user_details"] isKindOfClass:[NSDictionary class]]) {
        
        if ( [object_dict[@"get_object_user_details"] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *checkDictUser=object_dict[@"get_object_user_details"];
            if (checkDictUser.count!=0) {
                objectNametxt=[NSString stringWithFormat:@"%@ %@",checkDictUser[@"first_name"],checkDictUser[@"last_name"]];
                NSDictionary *userProfilepicDict=[checkDictUser objectForKey:@"usersprofile"];
               imageUrlseemore=[userProfilepicDict objectForKey:@"profile_picture"];
                
            }
        }
        
        if ( [object_dict[@"get_object_community_details"] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *checkDictCommunity=object_dict[@"get_object_community_details"];
            if (checkDictCommunity.count!=0) {
                objectNametxt=[NSString stringWithFormat:@"%@",checkDictCommunity[@"name"]];
            }
            
        }
        
    }
    
    NSDictionary *dict = @{
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                           @"ref_id":object_dict[@"id"],
                           @"ref_type":@"post",
                           };
    
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:commentLists withParameters:dict withHud:YES success:^(id response){
        NSMutableArray *messageArray;
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    NSMutableArray *commentsArray=[response valueForKey:@"data"];
                    all_comments=[[NSMutableArray alloc]init];
                    all_comments=commentsArray;
                    UIImage *ima=feedImage_cell.imageview_person.image;
                    if(ima==nil){
                        ima=[self imageFromColorDefault:[UIColor whiteColor]];
                    }
                    if ([celltype isEqualToString:@"Feedpost"]) {
                        commment_info=@{
                                        @"post_body" :feed_cell.labelPP.text,
                                        @"post_picture" :imageUrlseemore,
                                        @"post_personName" :feed_cell.lbl_personName.text,
                                        @"post_time" :feed_cell.lbl_date.text,
                                        @"ref_id" :object_dict[@"id"],
                                        @"ref_type" :@"post",
                                        @"postimage_array":[arrWallFeeds objectAtIndex:cell_index][@"get_attachments"],
                                        @"get_total_praised":[arrWallFeeds objectAtIndex:cell_index][@"get_total_praised"],
                                        @"post_type":[arrWallFeeds objectAtIndex:cell_index][@"post_type"],
                                        @"is_like":[arrWallFeeds objectAtIndex:cell_index][@"is_like"],
                                        @"objectname":objectNametxt,
                                        @"objectID":[arrWallFeeds objectAtIndex:cell_index][@"get_subject_user_details"][@"id"],
                                        @"cellindex":cellindexpath,
                                         @"subjectName":subjectName,
                                         @"Influencercheck":[arrWallFeeds objectAtIndex:cell_index][@"get_subject_user_details"][@"user_type"]
                                        
                                        };
                        
                    }else{
                        UIImage *ima=feedImage_cell.imageview_post.image;
                        if(ima==nil){
                            ima=[self imageFromColorDefault:[UIColor whiteColor]];
                        }
                        
                        UIImage *ima1=feedImage_cell.imageview_person.image;
                        if(ima1==nil){
                            ima1=[self imageFromColorDefault:[UIColor whiteColor]];
                        }
                        commment_info=@{
                                        @"post_body" :feedImage_cell.labelPP.text,
                                        @"post_picture" :imageUrlseemore,
                                        @"post_personName" :feedImage_cell.lbl_personName.text,
                                        @"post_time" :feedImage_cell.lbl_date.text,
                                        @"ref_id" :object_dict[@"id"],
                                        @"ref_type" :@"post",
                                        @"post_Image":ima,
                                        @"postimage_array":[arrWallFeeds objectAtIndex:cell_index][@"get_attachments"],
                                        @"get_total_praised":[arrWallFeeds objectAtIndex:cell_index][@"get_total_praised"],
                                        @"post_type":[arrWallFeeds objectAtIndex:cell_index][@"post_type"],
                                        @"is_like":[arrWallFeeds objectAtIndex:cell_index][@"is_like"],
                                        @"objectname":feedImage_cell.lbl_objectname.text,
                                        @"objectID":[arrWallFeeds objectAtIndex:cell_index][@"get_subject_user_details"][@"id"],
                                        @"cellindex":cellindexpath,
                                        @"subjectName":subjectName,
                                        @"Influencercheck":[arrWallFeeds objectAtIndex:cell_index][@"get_subject_user_details"][@"user_type"]
                                       
                                        };
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate1 btn_pressed:@"seemore" andtotal_comments:all_comments
                                            andpost:commment_info  andCellindex:cell_index];
                    });
                    
                }else{
                    messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([Constant1 validateArray:messageArray]) {
                            [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        }
                        
                    });
                }
                
            }
            
        }
        
    } errorBlock:^(id error)
     {
//         NSLog(@"error is %@",error);
     }];
    
}

#pragma mark PullUptoRefreshMethod

-(void)pullUptorefresh{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSDictionary *dict;
    suggetedArray=nil;
    NSString *methodname;
    
   
     if ([userInfo[@"wallfor"]isEqualToString:@"Mywall"])
    {
        NSString * logeedInUser=[NSString stringWithFormat:@"%@",[self isStrEmpty:[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]]];
        NSString *compareUseerId=[NSString stringWithFormat:@"%@",[self isStrEmpty:userInfo[@"userid"]]];
        
        if ([ logeedInUser isEqualToString:compareUseerId]) {
            dict = @{
                     @"token" :[self isStrEmpty:token],
                     @"id" :[self isStrEmpty:[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]],
                     @"object_type":@"user",
                     @"object_id":[self isStrEmpty:[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]],
                     @"filter_type":[self isStrEmpty:filterType],
                     @"page":@"1",
                     };
        }
        else
        {
            dict = @{
                     @"token" :[self isStrEmpty:token],
                     @"id" :[self isStrEmpty:[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]],
                     @"object_type":@"user",
                     @"object_id":[self isStrEmpty:userInfo[@"userid"]],
                     @"filter_type":[self isStrEmpty:filterType],
                     @"page":@"1",
                     };
        }
        
        methodname=wall_display;
        
    }
     else{
         dict = @{
                @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                @"filter_type":filterType,
                @"page":@"1"
                
                };
                methodname=allwalllist;
     }
    
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodname withParameters:dict withHud:YES success:^(id response){
        if(arrWallFeeds && arrWallFeeds!=nil)
        {
            [arrWallFeeds removeAllObjects];
        }
        if(self.imageArray)
        {
            [self.imageArray removeAllObjects];
        }
        
        all_content=nil;
        all_content=[response valueForKey:@"data"][@"data"];
        [arrWallFeeds addObjectsFromArray:all_content];
        if ([otheruserProfile isEqualToString:@"removesuggestedArray"]||[_selfProfilewallViewHidden isEqualToString:@"selfProfilewallViewHiddenYes"]) {
        }
        else{
            suggetedArray=[response valueForKey:@"suggestedInfluencer"];
        }
        NSString *current_page=[NSString stringWithFormat:@"%@",[response valueForKey:@"data"][@"current_page"]];
        NSString *last_page=[NSString stringWithFormat:@"%@",[response valueForKey:@"data"][@"last_page"]];
        NSString *per_page=[NSString stringWithFormat:@"%@",[response valueForKey:@"data"][@"per_page"]];
        pagenumber= [current_page intValue];
        total_pagenumber= [last_page intValue];
        perpage=[per_page intValue];
        // Adding suggested influencer list
        if ([methodname isEqualToString:@"newWallFeed"]){
            if([suggetedArray count]>0 && appDelegate.showsuggestedGroup)
            {
                if ([arrWallFeeds count]>0) {
                    if ([[arrWallFeeds objectAtIndex:0] isKindOfClass:[NSString class]]) {
                        if ([[arrWallFeeds objectAtIndex:0]isEqualToString:@"Group"]) {
                        }
                        else{
                            [arrWallFeeds insertObject:@"Group" atIndex:0];
                        }
                    }
                    else{
                        [arrWallFeeds insertObject:@"Group" atIndex:0];
                    }
                }
                else{
                    [arrWallFeeds insertObject:@"Group" atIndex:0];
                }
            }
 
        }
        AppDelegate *getVarHeader = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        getVarHeader.headerDict=[response objectForKey:@"HeaderAds"];
        getVarHeader.isheaderAD=[NSString stringWithFormat:@"%@",[response objectForKey:@"is_header_ad"]];
        
        if ([methodname isEqualToString:@"newWallFeed"]){
            NSMutableArray *targetFeedcellArray=[response objectForKey:@"TargetGroup"];
            [[NSUserDefaults standardUserDefaults] setObject:targetFeedcellArray forKey:@"Targetarray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            for (int i=0; i<[targetFeedcellArray count]; i++) {
                NSDictionary *dictAdfeedsDict=[targetFeedcellArray objectAtIndex:i];
                if ([dictAdfeedsDict isKindOfClass:[NSDictionary class]]) {
                    NSString *cellPositionads=[NSString stringWithFormat:@"%@",[dictAdfeedsDict objectForKey:@"anchor_no"]];
                    if ([cellPositionads intValue]>=[arrWallFeeds count]) {
                        break;
                    }
                    id adObj= [arrWallFeeds objectAtIndex:[cellPositionads intValue]];
                    if(![adObj isKindOfClass:[NSString class]])
                    {
                        [arrWallFeeds insertObject:@"hello" atIndex:[cellPositionads intValue]];
                    }
                }
            }
 
        }
        if([arrWallFeeds count]>=2)
        {
            pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:40.0f tableView:_tableViewOutlet withClient:self];
        }
        if (arrWallFeeds.count==0) {
            
        }else{
            NSMutableArray *backup_array=[[NSMutableArray alloc]init];
            
            for (int i=0; i<all_content.count; i++) {
                [backup_array addObject:[arrWallFeeds objectAtIndex:i]];
            }
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] initWithCapacity:3];
            if (backup_array != nil) {
                [dataDict setObject:backup_array forKey:@"backuparray"];
            }
            [NSKeyedArchiver archiveRootObject:dataDict toFile:filePath];
        }
        
        if ([arrWallFeeds count]>0) {
            if ([self.imageArray count]>0) {
                for (int i=(int)[self.imageArray count]; i<[arrWallFeeds count]; i++) {
                    [self.imageArray addObject:[NSNull null]];
                }
            }
            else{
                for (int i=0; i<[arrWallFeeds count]; i++) {
                    [self.imageArray addObject:[NSNull null]];
                }
            }
        }

        
        NSString * timestamp = TimeStamp;
        [[NSUserDefaults standardUserDefaults]setObject:timestamp forKey:@"timeStamp"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_tableViewOutlet reloadData];
            [pullToRefreshManager_ tableViewReloadFinished];
            [_refreshControl endRefreshing];
        });
        
    } errorBlock:^(id error)
     {
//         NSLog(@"error is %@",error);
     }];
    
    
}

#pragma mark - To Check String is Empty
-(id)isStrEmpty:(NSString *) str {
    
    if([str isKindOfClass:[NSNumber class]])
    {
        return str;
    }
    if([str isKindOfClass:[NSNull class]] || str==nil)
    {
        return @"";
    }
    if([str length] == 0) {
        
        return @"";
    }
    if(![[str stringByTrimmingCharactersInSet:[NSCharacterSet
                                               whitespaceAndNewlineCharacterSet]] length]) {
        return @"";
    }
    if([str isEqualToString:@"(null)"])
    {
        return @"";
    }
    if([str isEqualToString:@"<null>"])
    {
        return @"";
    }
    return str;
}


#pragma mark initial_FeedserviceCall Method

-(void)initial_FeedserviceCall:(int)url_pagenumber  andfiletype:(NSString*)filtertypes{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (!appDelegate.isReachable)
    {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    
    if(appDelegate.notfoundadsArray)
    {
        [appDelegate.notfoundadsArray removeAllObjects];
    }
    
    NSString *methodname=nil;
    NSDictionary *dict;
    
    token=[NSString stringWithFormat:@"%@",[self isStrEmpty: [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"]]];

    if ([token isEqualToString:@""]) {
        token=@"MR1YUa8LPuDlAqVS5usRlXYqwoaqWszb4dI60R9moGk9paDVadzcuK0KPwV2";
    }
    
    if ([userInfo[@"wallfor"]isEqualToString:@"myself"])
    {
        NSString *checkpagenumberUrl=[NSString stringWithFormat:@"%d",url_pagenumber];
        if ([checkpagenumberUrl isEqualToString:@"1"])
        {
            dict= @{
                    @"token" : [self isStrEmpty:token],
                    @"id" :[self isStrEmpty:userID],
                    @"filter_type":[self isStrEmpty:filterType],
                    @"page":[self isStrEmpty:[NSString stringWithFormat:@"%d",url_pagenumber]],
                    };
        }
        else
        {
            dict= @{
                    @"token" :[self isStrEmpty:token] ,
                    @"id" :[self isStrEmpty:userID],
                    @"filter_type":[self isStrEmpty:filterType],
                    @"page":[self isStrEmpty:[NSString stringWithFormat:@"%d",url_pagenumber]],
                    @"group_count":[self isStrEmpty:[[NSUserDefaults standardUserDefaults] valueForKey:@"GroupcountDict"]],

                    };
        }
        methodname=allwalllist;

    }
    else  if ([userInfo[@"wallfor"]isEqualToString:@"Mywall"])
    {
        NSString * logeedInUser=[NSString stringWithFormat:@"%@",[self isStrEmpty:[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]]];
        NSString *compareUseerId=[NSString stringWithFormat:@"%@",[self isStrEmpty:userInfo[@"userid"]]];
        
        if ([ logeedInUser isEqualToString:compareUseerId]) {
            dict = @{
                    @"token" :[self isStrEmpty:token],
                    @"id" :[self isStrEmpty:[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]],
                    @"object_type":@"user",
                    @"object_id":[self isStrEmpty:[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]],
                    @"filter_type":[self isStrEmpty:filterType],
                    @"page":[self isStrEmpty:[NSString stringWithFormat:@"%d",url_pagenumber]],
                    };
        }
        else
        {
            dict = @{
                     @"token" :[self isStrEmpty:token],
                     @"id" :[self isStrEmpty:[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]],
                     @"object_type":@"user",
                     @"object_id":[self isStrEmpty:userInfo[@"userid"]],
                     @"filter_type":[self isStrEmpty:filterType],
                     @"page":[self isStrEmpty:[NSString stringWithFormat:@"%d",url_pagenumber]],
                     };
        }
        
        methodname=wall_display;
        
    }
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodname withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                   // header ad implementation
            AppDelegate *getVarHeader = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                getVarHeader.headerDict=[response objectForKey:@"HeaderAds"];
                getVarHeader.isheaderAD=[NSString stringWithFormat:@"%@",[response objectForKey:@"is_header_ad"]];
                    NSString  *threshold=[NSString stringWithFormat:@"%@",[response objectForKey:@"threshold"]];
                    threshold=[self isStrEmpty:threshold];
                    if ([currentVersion intValue]<[threshold intValue]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName: @"forcetoUpdate" object:nil userInfo:nil];
                        return ;
                    }
               
                    // to find group_count
                    NSDictionary *groupcount_dict=[[response valueForKey:@"data" ] valueForKey:@"group_count"];
                    if ([groupcount_dict isKindOfClass:[NSDictionary class]]) {
                        NSError *error;
                        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:groupcount_dict
                                                                           options:NSJSONWritingPrettyPrinted
                                                                             error:&error];
                        if (! jsonData) {
                        } else {
                            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                            [[NSUserDefaults standardUserDefaults] setObject:jsonString forKey:@"GroupcountDict"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                    }
                    // count total prayer here
                    if ([methodname isEqualToString:allwalllist]) {
                        totalcountPrayer=[NSString stringWithFormat:@"%@",[response valueForKey:@"total_prayer"]];
                    }
                    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    // rateviewpop
                    NSInteger pageNumber = [[[response valueForKey:@"data"] valueForKey:@"current_page"] integerValue];
                    NSString *feedbackCheck=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"feedback"]];
                    if (pageNumber==2 && getVar.showrateView) {
                        getVar.showrateView=NO;
                        if ([feedbackCheck isEqualToString:@"yes"]) {
                            return ;
                        }
                        else{
                            
                        [[NSNotificationCenter defaultCenter] postNotificationName: @"ratepopView" object:nil userInfo:nil];
                        }
                        
                    }
                    
                all_content=nil;
                    all_content=[response valueForKey:@"data"][@"data"];
                    [arrWallFeeds addObjectsFromArray:all_content];
                    NSString *current_page=[NSString stringWithFormat:@"%@",[response valueForKey:@"data"][@"current_page"]];
                    NSString *last_page=[NSString stringWithFormat:@"%@",[response valueForKey:@"data"][@"last_page"]];
                    NSString *per_page=[NSString stringWithFormat:@"%@",[response valueForKey:@"data"][@"per_page"]];
                    pagenumber= [current_page intValue];
                    total_pagenumber= [last_page intValue];
                    perpage=[per_page intValue];
                    
                    // Adding suggested influencer list
                    
                    
                    if ([otheruserProfile isEqualToString:@"removesuggestedArray"]||[_selfProfilewallViewHidden isEqualToString:@"selfProfilewallViewHiddenYes"]) {
                    }
                    else{
                    if (pageNumber==1)
                    {
                        suggetedArray=[response valueForKey:@"suggestedInfluencer"];
                    }
                    }

                     if ([methodname isEqualToString:@"newWallFeed"]) {
                         if([suggetedArray count]>0 && appDelegate.showsuggestedGroup)
                         {
                             if ([arrWallFeeds count]>0) {
                                 if ([[arrWallFeeds objectAtIndex:0] isKindOfClass:[NSString class]]) {
                                     //
                                     if ([[arrWallFeeds objectAtIndex:0]isEqualToString:@"Group"]) {
                                     }
                                     else{
                                         [arrWallFeeds insertObject:@"Group" atIndex:0];
                                     }
                                 }
                                 else{
                                     [arrWallFeeds insertObject:@"Group" atIndex:0];
                                 }
                             }
                             else{
                                 [arrWallFeeds insertObject:@"Group" atIndex:0];
                             }
                             
                         }

                     }
                    
                    if ([methodname isEqualToString:@"newWallFeed"]) {
                        // Adding ads on provided index
                        NSMutableArray *targetFeedcellArray=[response objectForKey:@"TargetGroup"];
                        [[NSUserDefaults standardUserDefaults] setObject:targetFeedcellArray forKey:@"Targetarray"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        for (int i=0; i<[targetFeedcellArray count]; i++) {
                            NSDictionary *dictAdfeedsDict=[targetFeedcellArray objectAtIndex:i];
                            if ([dictAdfeedsDict isKindOfClass:[NSDictionary class]]) {
                                NSString *cellPositionads=[NSString stringWithFormat:@"%@",[dictAdfeedsDict objectForKey:@"anchor_no"]];
                                if ([cellPositionads intValue]>=[arrWallFeeds count]) {
                                    break;
                                }
                                
                                id adObj= [arrWallFeeds objectAtIndex:[cellPositionads intValue]];
                                if(![adObj isKindOfClass:[NSString class]])
                                {
                                    [arrWallFeeds insertObject:@"hello" atIndex:[cellPositionads intValue]];
                                }
                            }
                        }
                    }
                   
                    if([arrWallFeeds count]>=2)
                    {
                        pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:40.0f tableView:_tableViewOutlet withClient:self];
                    }
                    
                    if (arrWallFeeds.count==0)
                    {
                        
                    }
                    else{
                        NSMutableArray *backup_array=[[NSMutableArray alloc]init];
                        for (int i=0; i<all_content.count; i++) {
                            [backup_array addObject:[arrWallFeeds objectAtIndex:i]];
                        }
                        NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] initWithCapacity:3];
                        if (backup_array != nil) {
                            [dataDict setObject:backup_array forKey:@"backuparray"];  // save the games array
                        }
                        [NSKeyedArchiver archiveRootObject:dataDict toFile:filePath];
                    }
                    
                    if ([arrWallFeeds count]>0) {
                        if ([self.imageArray count]>0) {
                            for (int i=(int)[self.imageArray count]; i<[arrWallFeeds count]; i++) {
                                [self.imageArray addObject:[NSNull null]];
                            }
                        }
                        else{
                            for (int i=0; i<[arrWallFeeds count]; i++) {
                                [self.imageArray addObject:[NSNull null]];
                            }
                        }
                    }
                    
                    
                    
                    
                    
                    
                    
                    NSString * timestamp = TimeStamp;
                    [[NSUserDefaults standardUserDefaults]setObject:timestamp forKey:@"timeStamp"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_tableViewOutlet reloadData];
                        [pullToRefreshManager_ tableViewReloadFinished];
                    });
                }else{
                    messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([Constant1 validateArray:messageArray]) {
                            [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        }
                    });
                }
            }
        }
        
    } errorBlock:^(id error)
     {
//         NSLog(@"error is %@",error);
     }];
    
}
#pragma mark reloadcellMethod

-(void)reloadCell:(NSUInteger)cell_index{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellindex inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableViewOutlet reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
}
-(void)liked1:(NSDictionary*)dict andmethodName:(NSString*)methodName andcell_index:(NSUInteger)cell_index   andType:(NSString*)cellType  andfeedcell:(FeedpostTableViewCell*)feedcell andfeedimage_cell:(FeedwithImagePostTableViewCell*)feedimagecell checkservicelikeunlike:(NSString *)checklikeunlike subjectNamePost:(NSString*)subjectname{
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                messageArray=[response valueForKey:@"message"];
                NSString *str=[messageArray objectAtIndex:0];
                if([response[@"status"]intValue ] ==1){
                    // prayer count implemented
                    dispatch_async(dispatch_get_main_queue(), ^{
//                       int i,j,k;
                        NSString *checkContentType;
                        if ([checklikeunlike isEqualToString:@"wall_like"]) {
                            if ([postcheck isEqualToString:@"2"]) {
                                checkContentType=@"Prayer Request";
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Prayers" by:@1];
                                 [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@1];
                                
                            }
                            if ([postcheck isEqualToString:@"3"]) {
                                checkContentType=@"Update";
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@1];
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@1];
                            }
                            if ([postcheck isEqualToString:@"1"]) {
                                checkContentType=@"Update";
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@1];
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@1];
                            }
                            
                            NSString *dataLast=[Constant1 createTimeStamp];
                             NSString *contenID=[NSString stringWithFormat:@"%@",[dict objectForKey:@"object_id"]];
                            
                             [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last like": dataLast}];
                            
                            [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Content ID":contenID,@"Posted by":subjectname,@"Content type":checkContentType,@"Date of last like":dataLast}];
                            
                            [[AppDelegate sharedAppDelegate].mxPanel track:@"Like"
                                properties:@{@"Content ID":contenID,@"Posted by":subjectname,@"Content type":checkContentType,@"Date of last like":dataLast}
                             ];
                        
                        }
                        else{
                            NSString *dataLast=[Constant1 createTimeStamp];
                             NSString *contenID=[NSString stringWithFormat:@"%@",[dict objectForKey:@"object_id"]];
                            if ([postcheck isEqualToString:@"2"]) {
                                checkContentType=@"Prayer Request";
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Prayers" by:@-1];
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@-1];
                                
                            }
                            if ([postcheck isEqualToString:@"3"]) {
                                checkContentType=@"Update";
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@-1];
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@-1];
                            }
                            if ([postcheck isEqualToString:@"1"]) {
                                checkContentType=@"Update";
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total Praises" by:@-1];
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Total likes" by:@-1];
                            }
                            
                           [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Content ID":contenID,@"Posted by":subjectname,@"Content type":checkContentType,@"Date of last like":dataLast}];
  
                        }
                        
                        ////// mix panel//////
                        
                        if ([postcheck isEqualToString:@"2"]) {
                         if ([checkYouprayed isEqualToString:@"yes"]) {
                                checkYouprayed=@"";
                                NSString *totalprayerCount=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"totalPrayerCount"]];
                                int removeoneValue=[totalprayerCount intValue]-1;
                                NSString *actualValuePrayed=[NSString stringWithFormat:@"%d",removeoneValue];
                             
                                //RS
                                //[self makeToast:prayermsg  duration:1.0 position:CSToastPositionCenter];
                                totalcountPrayer=[NSString stringWithFormat:@"%@",[response valueForKey:@"total_prayer"]];
                                
                                NSString *combinedStr = [[NSString alloc] init];
                                
                                if([totalcountPrayer isEqualToString:@"1"])
                                {
                                    combinedStr = [NSString stringWithFormat:@"%@ %@ %@",@"You have Prayed",totalcountPrayer,@"time for fellow Christians"];
                                   
                                }
                                else
                                {
                                    combinedStr = [NSString stringWithFormat:@"%@ %@ %@",@"You have Prayed",totalcountPrayer,@"times for fellow Christians"];
                                }
                                
                                NSAttributedString *attributedString=[Constant1 customizeString:combinedStr range:16 rangeOne:totalcountPrayer.length];
                                prayerTotalCountStr1.attributedText=attributedString;
                                //RS
                                
                            }
                            else{
                                NSString  *totalcountPrayer1=[NSString stringWithFormat:@"%@",[response valueForKey:@"total_prayer"]];
                                [[NSUserDefaults standardUserDefaults]setObject:totalcountPrayer1 forKey:@"totalPrayerCount"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                NSString *prayermsg=[NSString stringWithFormat:@"%@ %@ %@",@"You have now prayed for",totalcountPrayer1,@"Christians"];
                                [self makeToast:prayermsg  duration:1.0 position:CSToastPositionCenter];
                                //RS
                                totalcountPrayer=[NSString stringWithFormat:@"%@",[response valueForKey:@"total_prayer"]];
                                
                                NSString *combinedStr = [[NSString alloc] init];
                                
                                if([totalcountPrayer isEqualToString:@"1"])
                                {
                                    combinedStr = [NSString stringWithFormat:@"%@ %@ %@",@"You have Prayed",totalcountPrayer,@"time for fellow Christians"];
                                }
                                else
                                {
                                    combinedStr = [NSString stringWithFormat:@"%@ %@ %@",@"You have Prayed",totalcountPrayer,@"times for fellow Christians"];
                                }
                                
                                NSAttributedString *attributedString=[Constant1 customizeString:combinedStr range:16 rangeOne:totalcountPrayer.length];
                                prayerTotalCountStr1.attributedText=attributedString;
                                //RS
                            }
                            
                        }
                    });
                    

                    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                    NSMutableArray *dict_arr=[response valueForKey:@"like_count"];
                    NSString *count;
                    for (NSDictionary* dict in dict_arr) {
                        count=[dict valueForKey:@"count"];
                    }int count_val=[count intValue];
                    
                    [dict setObject:count forKey:@"count"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cellindex=cell_index;
                        if ([str  isEqualToString:@"Like Successfully added"]) {
                            if (count_val<=1) {
                                
                                [[arrWallFeeds objectAtIndex:cell_index]setObject:dict forKey:@"get_total_praised"];
                                [[arrWallFeeds objectAtIndex:cell_index]setObject:@"1" forKey:@"is_like"];
                                [self reloadCell:cellindex];
                            }else{
                                
                                [arrWallFeeds objectAtIndex:cell_index][@"get_total_praised"][@"count"]=count;
                                [[arrWallFeeds objectAtIndex:cell_index]setObject:@"1" forKey:@"is_like"];
                                [self reloadCell:cellindex];
                            }
                        }
                        else{
                            if (count_val==0) {
                                [[arrWallFeeds objectAtIndex:cell_index]setObject:[NSNull null] forKey:@"get_total_praised"];
                                [[arrWallFeeds objectAtIndex:cell_index]setObject:@"0" forKey:@"is_like"];
                                [self reloadCell:cellindex];
                            }else
                            {
                                [arrWallFeeds objectAtIndex:cell_index][@"get_total_praised"][@"count"]=count;
                                [[arrWallFeeds objectAtIndex:cell_index]setObject:@"0" forKey:@"is_like"];
                                [self reloadCell:cellindex];
                            }
                        }
                        //   [pullToRefreshManager_ tableViewReloadFinished];
                    });
                }else{
                    messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([Constant1 validateArray:messageArray]) {
                            [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        }
                    });
                }
            }
        }
    } errorBlock:^(id error)
     {
//         NSLog(@"error is %@",error);
     }];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate: (BOOL)decelerate
{
    [pullToRefreshManager_ tableViewReleased];
}


- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        if (pagenumber==total_pagenumber) {
        }
        else
        {
            [self initial_FeedserviceCall:pagenumber+1 andfiletype:filterType];
        }
    }
    
}



-(void)profileView :(NSString*)methodName anfriend_ID:(NSString*)friendID influencerValue:(NSString*)influencerdata
{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSDictionary *dict = @{
                           @"user_id" :userID,
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id" : friendID,
                           };
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                
            NSDictionary *dataDict=[response objectForKey:@"data"];
            NSString *user_id=[NSString stringWithFormat:@"%@",[dataDict objectForKey:@"id"]];
                if([response[@"status"]intValue ] ==1){
                    if ([influencerdata isEqualToString:@"0"]) {
                NSString *loginId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
                        if ([user_id isEqualToString:loginId]) {
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"userProfileView" object:nil userInfo:nil];
                        }
                        else{
                            //friend profile
                            NSDictionary *userInfogroup = @{
                                                            @"allData" : response[@"data"],
                                                            };
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"friendProfileView" object:nil userInfo:userInfogroup];
                        }
                    
                    }
                    else{
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                    AppDelegate *getVar = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                            if (getVar.checkSuggetedProfile==YES) {
                                getVar.afterFolloSuggetedProfile=YES;
                                getVar.checkSuggetedProfile=NO;
                            }
                            
                            NSDictionary *userInfogroup = @{
                                                            @"allData" : response[@"data"],
                                                            };
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"influcencerProfileView" object:nil userInfo:userInfogroup];
                        });
                        
                    }
                    
                }else{
                    messageArray=[response valueForKey:@"message"];
                    if ([Constant1 validateArray:messageArray]) {
                        [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                    }
                    
                    
                }
                
            }
        }
        
    } errorBlock:^(id error)
     {
//         NSLog(@"error");
         
     }];
}


#pragma mark pop menu

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex andid:(NSString *)postid andcellindex:(int)cellindexpath
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    NSString *methodname;
    NSDictionary *dict ;
    if (selectedIndex==5) {
        methodname=delete_wall;
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 @"id" : postid,
                 };
    }
    else    if (selectedIndex==6) {
//        NSLog(@"update ");
        methodname=abuse;
        dict = @{
                 @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 @"post_id" : postid,
                 };
    }
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodname withParameters:dict withHud:YES success:^(id response){
        //        NSLog(@"respons is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    messageArray=[response valueForKey:@"message"];
                    
                    NSString *str=[messageArray objectAtIndex:0];
                    if ( [str isEqualToString:@"You have successfully left the Post!"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.menuPopover2 dismissMenuPopover];
                            if ([methodname isEqualToString:delete_wall]) {
                                [arrWallFeeds removeObjectAtIndex:cellindexpath];
                                [self.imageArray removeObjectAtIndex:cellindexpath];
                                [_tableViewOutlet reloadData];
                            }
                            if ([Constant1 validateArray:messageArray]) {
                                [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                        });
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.menuPopover2 dismissMenuPopover];
                            if ([Constant1 validateArray:messageArray]) {
                                [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                        });
                    }
                    
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.menuPopover2 dismissMenuPopover];
                    });
                    messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([Constant1 validateArray:messageArray]) {
                            [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        }
                    });
                }
                
            }
        }
        
    } errorBlock:^(id error)
     {
         [self.menuPopover2 dismissMenuPopover];
//         NSLog(@"error");
         
     }];
    
    [self.menuPopover dismissMenuPopover];
    
}
-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
- (UIImage *)imageFromColor:(UIColor *)color {
    if(color==nil)
    {
        color=[UIColor grayColor];
    }
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}


- (void)viewDidLayoutSubviews {
    
    //   [super viewDidLayoutSubviews];
    
    [pullToRefreshManager_ relocatePullToRefreshView];
}

-(void)removeSuggetedGroup{
    if ([arrWallFeeds count]>0 && [suggetedArray count]>0) {
        if ([[arrWallFeeds objectAtIndex:0] isKindOfClass:[NSString class]]) {
            
            [arrWallFeeds removeObjectAtIndex:0];
        }
        [self.tableViewOutlet reloadData];
        
    }
}

// post content URl tap gesture Method here

- (void)URlhandleTap:(UITapGestureRecognizer *)tapRecognizer
{
    NSString *StringUrl;
    StringUrl=nil;
    NSDictionary *dictURl=[arrWallFeeds objectAtIndex:tapRecognizer.view.tag];
//    NSLog(@"index path %ld",tapRecognizer.view.tag);
    
   StringUrl =[NSString stringWithFormat:@"%@",[dictURl objectForKey:@"content"]];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:StringUrl]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:StringUrl]];
    }
    else{
        NSString *combinedHttps = [NSString stringWithFormat:@"%@%@",@"https://",StringUrl];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:combinedHttps]];
    }
}


#pragma mark- 
#pragma mark- DFP Delegate

// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(DFPBannerView *)adView {
//    NSLog(@"adViewDidReceiveAd");
//    feedAdView1.hidden = NO;
//       feedAdView1.enableManualImpressions = YES;
}

// Tells the delegate an ad request failed.
- (void)adView:(DFPBannerView *)adView
didFailToReceiveAdWithError:(GADRequestError *)error {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    HeaderView *headerCell= (HeaderView*)[adView superview];
    if ([headerCell isKindOfClass:[HeaderView class]]) {
        dispatch_async(dispatch_get_main_queue()
                       , ^{
                           if(appDelegate.headeradcheck==NO)
                           {
                               appDelegate.headeradcheck=YES;
                               [self.tableViewOutlet reloadData];
                           }
                    });
    }
    else{
        NSString *adTag=[NSString stringWithFormat:@"%ld",(long)adView.tag];
        if (![appDelegate.notfoundadsArray containsObject:adTag]) {
            [appDelegate.notfoundadsArray addObject:adTag];
            if ([arrWallFeeds count]>0) {
                [self reloadCell:adView.tag];
            }
        }
    }

}

// Tells the delegate that a full screen view will be presented in response
// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(DFPBannerView *)adView {
//    NSLog(@"adViewWillPresentScreen");
//        [feedAdView1 recordImpression];
    
}

// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(DFPBannerView *)adView {
//    NSLog(@"adViewWillDismissScreen");
}

// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(DFPBannerView *)adView {
//    NSLog(@"adViewDidDismissScreen");
}

// Tells the delegate that a user click will open another app (such as
// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(DFPBannerView *)adView {
//    NSLog(@"adViewWillLeaveApplication");
}

#pragma pplabel delegate
#pragma mark -

- (void)label:(PPLabel *)label didBeginTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
}

- (void)label:(PPLabel *)label didMoveTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
}

- (void)label:(PPLabel *)label didEndTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
}

- (void)label:(PPLabel *)label didCancelTouch:(UITouch *)touch {
}

#pragma mark -

- (BOOL)isIndex:(CFIndex)index inRange:(NSRange)range {
    return index > range.location && index < range.location+range.length;
}

- (void)highlightLinksWithIndex:(CFIndex)index custum:(PPLabel*)label {
    
    NSMutableAttributedString* attributedString = [label.attributedText mutableCopy];
    
    for (NSTextCheckingResult *match in self.matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSRange matchRange = [match range];
            if ([self isIndex:index inRange:matchRange]) {
                [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:matchRange];
                
            }
            else {
                [attributedString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x00ADEB) range:matchRange];
            }
            [attributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:matchRange];
        }
    }
    
    label.attributedText = attributedString;
}

- (void)handleTap:(UITapGestureRecognizer *)tapRecognizer
{
    PPLabel *textLabel = (PPLabel *)tapRecognizer.view;
    CGPoint tapLocation = [tapRecognizer locationInView:textLabel];
    NSString *testURL=[NSString stringWithFormat:@"%@",textLabel.text];
    // init text storage
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:textLabel.attributedText];
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    [textStorage addLayoutManager:layoutManager];
    // init text container
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(textLabel.frame.size.width, textLabel.frame.size.height+100) ];
    textContainer.lineFragmentPadding  = 0;
    textContainer.maximumNumberOfLines = textLabel.numberOfLines;
    textContainer.lineBreakMode        = textLabel.lineBreakMode;
    [layoutManager addTextContainer:textContainer];
    NSUInteger characterIndex = [layoutManager characterIndexForPoint:tapLocation
                                                      inTextContainer:textContainer
                             fractionOfDistanceBetweenInsertionPoints:NULL];
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:testURL options:0 range:NSMakeRange(0, [testURL length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSRange matchRange = [match range];
            if ([self isIndex:characterIndex inRange:matchRange]) {
                [[UIApplication sharedApplication] openURL:match.URL];
                break;
            }
        }
    }

    
}


-(float)calculateImageHeight:(float)boxWidth imageHeight:(float)imageHeight1 imageWidth:(float)imagewidth1{
    CGFloat screenHeight=[UIScreen mainScreen].bounds.size.height;
    CGFloat preferImageHeight=screenHeight-300;
//    CGFloat actualImageHeight=imageHeight1;
    CGFloat widthView1,widthView;
    if (imagewidth1>imageHeight1) {
        // landscape
        widthView1=[Constant1 checkimagehieght];
        widthView=widthView1+30;
    }
    else{
      // portraits
        widthView=preferImageHeight;
        
    }
    return widthView;
}

-(void)setUpStaticData
{
    
    
}


@end
