//
//  CustomPageViewController.h
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//

#import "PagerViewController.h"

@interface CustomPagerViewController : PagerViewController

@property (strong, nonatomic) IBOutlet UIScrollView *pscrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topScroloViewCOnstraint;

@end
