//
//  ViewController.h
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//

#import <UIKit/UIKit.h>

@interface PagerViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (assign) NSUInteger page;
@property (strong, nonatomic) IBOutlet UIButton *but_submitOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btn_skipOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraitTopsubmit_btn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraitTopskip_btn;
@property(strong,nonatomic)NSDictionary *dictFacebook;
- (IBAction)btn_submit:(id)sender;
- (IBAction)btn_skip:(id)sender;
-(IBAction)skipMethod:(id)sender;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scroll_topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scroolVIew_heightConstraint;

@end
