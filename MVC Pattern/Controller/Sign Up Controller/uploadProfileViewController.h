//
//  uploadProfileViewController.h
//  wireFrameSplash
//
//  Created by webwerks on 10/13/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface uploadProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *SkipBTN;
@property (weak, nonatomic) IBOutlet UIButton *continueBTN;
@property (weak, nonatomic) IBOutlet UIImageView *imageUpload;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoBTN;

@property (strong, nonatomic) NSString *emailStr;
@property (strong, nonatomic) NSString *firstnameStr;
@property (strong, nonatomic) NSString *lastnameStr;
@property (strong, nonatomic) NSString *passwordStr;
@property (strong, nonatomic) NSString *imageSTRfromFB;
@property (strong, nonatomic) NSString *fbimage;


- (IBAction)UploadPictureBTN:(id)sender;
- (IBAction)takecamareBTN:(id)sender;
- (IBAction)SkipMethod:(id)sender;
- (IBAction)continueMethod:(id)sender;
- (IBAction)backAction:(id)sender;
@end
