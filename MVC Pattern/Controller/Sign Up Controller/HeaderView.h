//
//  HeaderView.h
//  wireFrameSplash
//
//  Created by Vikas on 25/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView
@property (strong, nonatomic) IBOutlet UIImageView *upperView_imageview;

@property (weak, nonatomic) IBOutlet UITextView *textViewTXT;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UIView *subView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomheightheader;
@property (weak, nonatomic) IBOutlet UIView *RequestView;
@property (weak, nonatomic) IBOutlet UIView *updateView;
@property (weak, nonatomic) IBOutlet UIButton *updateviewMethod;
@property (weak, nonatomic) IBOutlet UIButton *requestedBTN;
@property (weak, nonatomic) IBOutlet UIButton *updateBTN;
//RS
@property (weak, nonatomic) IBOutlet UILabel *lbl_HeaderSubView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_RequestPray;
@property (weak, nonatomic) IBOutlet UILabel *lbl_UpdateStatus;
@property (weak, nonatomic) IBOutlet UIImageView *influcerImage;
@property (weak, nonatomic) IBOutlet UIImageView *headerADimageview;
@property (weak, nonatomic) IBOutlet UIView *headerviewAD;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstrait;
@property (weak, nonatomic) IBOutlet UIButton *requestBTN;
@property (weak, nonatomic) IBOutlet UIButton *updateheaderBTN;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraints;
@property (weak, nonatomic) IBOutlet UIView *banner_mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adheightConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adwidthConstraints;






@end
