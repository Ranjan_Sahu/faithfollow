//
//  popGroupViewController.h
//  wireFrameSplash
//
//  Created by webwerks on 10/14/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupSearchViewController.h"
@interface popGroupViewController : UIViewController
@property (strong, nonatomic) GroupSearchViewController *owner;
@property (weak, nonatomic) IBOutlet UIView *monthView;
@property (weak, nonatomic) IBOutlet UIButton *monthBTN;
@property (weak, nonatomic) IBOutlet UIButton *dayBTN;
@property (weak, nonatomic) IBOutlet UIButton *yearBTN;
@property (weak, nonatomic) IBOutlet UIButton *maleBTN;
@property (weak, nonatomic) IBOutlet UIButton *femaleBTN;


@property (weak, nonatomic) IBOutlet UIView *dayView;
@property (weak, nonatomic) IBOutlet UIView *yearView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *submit;
@property (weak, nonatomic) IBOutlet UIButton *closeBTN;

-(IBAction)submit:(id)sender;
-(IBAction)closeBTN:(id)sender;
@end
