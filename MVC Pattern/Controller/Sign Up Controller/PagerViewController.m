//
//  ViewController.m
//  PageViewController
//
//

#import "PagerViewController.h"
#import "Constant1.h"
#import "NSString+StringValidation.h"
#import "UIImageView+WebCache.h"
#import "CongratulationViewController.h"
#import "WebServiceHelper.h"
#import "VSDropdown.h"
#import "AppDelegate.h"
#import "DeviceConstant.h"
#import "Constant1.h"
#import "MHTabBarController.h"
#import "TutorialViewController.h"
#import "Mixpanel.h"

@interface PagerViewController ()<UITextFieldDelegate,VSDropdownDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>{
    CGRect screenRect;
    NSUInteger pagenumber;
    UITextField *activefield;
    BOOL Keyboard,alertshown;
    UIAlertView *alert;
    int pagenice;
    NSString *placeholdertext1;
    NSMutableArray *dateArray,*monthArray,*yearArray;
    UIButton* male_btn,* female_btn;
    VSDropdown *_dropdown;
    UIButton *btnDate,*btnMonth,*btnYear;
    UIImage *chosenImage,*imgfacebook;
    NSString *maleFemaleStr;
    UIAlertView *skpiAlert;
    NSString *strDob ;
    BOOL pictureTaken;
    CGFloat top;
    UIImageView *picturetaken_image;
    NSString *checkMonth,*checkDay,*checkYear;
    BOOL yearPicked;
    NSDate *date1;
    BOOL isFacebookData;
     NSString *imagestrdata;
}
@property (strong, nonatomic) UITextField *emailTxt1,*passwordTXT1,*fisrtNameTXT,*lastNmaeTXT;
@property (assign) BOOL pageControlUsed;

@property (assign) BOOL rotating;
- (void)loadScrollViewWithPage:(int)page;
@end

@implementation PagerViewController

@synthesize scrollView,dictFacebook;
@synthesize pageControl;
@synthesize pageControlUsed = _pageControlUsed;
@synthesize page = _page;
@synthesize rotating = _rotating;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setScrollEnabled:YES];
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.scrollView setShowsVerticalScrollIndicator:NO];
    [self.scrollView setDelegate:self];
    screenRect = [[UIScreen mainScreen] bounds];
    CGRect rect=CGRectMake(0,10,screenRect.size.width,self.scrollView.frame.size.height);
    scrollView.frame=rect;
    pagenumber=0;
    isFacebookData=NO;
    
    UIViewController *viewcon1,*viewcon2,*viewcon3,*viewcon4,*viewcon5,*viewcon6;
    
       NSString *fbcheck=[[NSUserDefaults standardUserDefaults] objectForKey:@"fbvalue"];
    if ([fbcheck isEqualToString:@"fbvalue"]) {
        
        isFacebookData=YES;
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View1"]];
//        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View2"]];
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View3"]];
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View4"]];
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View5"]];
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View6"]];
        
        viewcon1 = [self.childViewControllers objectAtIndex:0];
        //        viewcon2= [self.childViewControllers objectAtIndex:1];
        viewcon3= [self.childViewControllers objectAtIndex:1];
        viewcon4= [self.childViewControllers objectAtIndex:2];
        viewcon5= [self.childViewControllers objectAtIndex:3];
        viewcon6= [self.childViewControllers objectAtIndex:4];
        
        UIButton *View1Button,*View3Button,*View4Button,*View5Button ;
        View1Button=[viewcon1.view viewWithTag:112];
        if ([[viewcon1.view viewWithTag:999] class]==[UITextView class]) {
            [[viewcon1.view viewWithTag:999] setUserInteractionEnabled:true];
            NSString *text=@"By Tapping Contine, you agree to our Terms of Service and Privacy Policy";
            NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:text];
            NSRange TermsRange = [text rangeOfString:@"Terms of Service"];
            NSRange PrivacyRange = [text rangeOfString:@"Privacy Policy"];
            [str addAttribute: NSLinkAttributeName value:urlTermCondiatiom range: TermsRange];
            [str addAttribute: NSLinkAttributeName value:urlPrivacy range: PrivacyRange];
            [(UITextView *)[viewcon1.view viewWithTag:999] setTintColor:View1Button.backgroundColor];
            [(UITextView *)[viewcon1.view viewWithTag:999] setAttributedText:str ];
            [(UITextView *)[viewcon1.view viewWithTag:999] setTextColor:[UIColor grayColor]];
            [(UITextView *)[viewcon1.view viewWithTag:999] setTextAlignment:NSTextAlignmentCenter];
            [(UITextView *)[viewcon1.view viewWithTag:999] setFont:[UIFont systemFontOfSize:13]];
        }
        
        
        
        [View1Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
//        View2Button=[viewcon2.view viewWithTag:212];
//        [View2Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
        View3Button=[viewcon3.view viewWithTag:312];
        [View3Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
        View4Button=[viewcon4.view viewWithTag:411];
        [View4Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
        View5Button=[viewcon5.view viewWithTag:511];
        [View5Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton* cameraPick=[viewcon6.view viewWithTag:611];
        [cameraPick addTarget:self action:@selector(gallery) forControlEvents:UIControlEventTouchUpInside];
        UIButton*  galleryPick=[viewcon6.view viewWithTag:612];
        [galleryPick addTarget:self action:@selector(camera) forControlEvents:UIControlEventTouchUpInside];
        btnDate=[viewcon4.view viewWithTag:408];
        [btnDate addTarget:self action:@selector(datepicker:) forControlEvents:UIControlEventTouchUpInside];
        btnMonth=[viewcon4.view viewWithTag:409];
        [btnMonth addTarget:self action:@selector(monthpicker:) forControlEvents:UIControlEventTouchUpInside];
        btnYear=[viewcon4.view viewWithTag:410];
        [btnYear addTarget:self action:@selector(yearpicker:) forControlEvents:UIControlEventTouchUpInside];
        picturetaken_image=[viewcon6.view viewWithTag:606];
        picturetaken_image.layer.cornerRadius=picturetaken_image.frame.size.width/2;
        picturetaken_image.layer.borderColor=[UIColor whiteColor].CGColor;
        picturetaken_image.layer.borderWidth=0.5f;
        picturetaken_image.clipsToBounds=YES;
        
        btnDate.layer.borderWidth=1.0f;
        btnDate.layer.borderColor=[UIColor lightGrayColor].CGColor;
        btnMonth.layer.borderWidth=1.0f;
        btnMonth.layer.borderColor=[UIColor lightGrayColor].CGColor;
        btnYear.layer.borderWidth=1.0f;
        btnYear.layer.borderColor=[UIColor lightGrayColor].CGColor;
        _btn_skipOutlet.hidden=YES;
        _but_submitOutlet.hidden=YES;

        _emailTxt1=[viewcon1.view viewWithTag:110];
        _emailTxt1.delegate=self;
        _passwordTXT1=[viewcon2.view viewWithTag:210];
        _passwordTXT1.delegate=self;
        _fisrtNameTXT=[viewcon3.view viewWithTag:309];
        _fisrtNameTXT.delegate=self;
        _lastNmaeTXT=[viewcon3.view viewWithTag:310];
        _lastNmaeTXT.delegate=self;
        male_btn=[viewcon5.view viewWithTag:510];
        [male_btn addTarget:self action:@selector(male_act:) forControlEvents:UIControlEventTouchUpInside];
        female_btn=[viewcon5.view viewWithTag:509];
        [female_btn addTarget:self action:@selector(female_act:) forControlEvents:UIControlEventTouchUpInside];
        
        dictFacebook = [[NSUserDefaults standardUserDefaults] objectForKey:@"fbDictionay"];
        NSString *email=[dictFacebook valueForKey:@"email"];
        if ([email isEqualToString:@""]) {
            email=@"";
        }
        else{
            email=[dictFacebook valueForKey:@"email"];
        }
        
        NSString *firstname=[dictFacebook valueForKey:@"first_name"];
        
        if ([firstname isEqualToString:@""]) {
            firstname=@"";
        }
        else{
            firstname=[dictFacebook valueForKey:@"first_name"];
        }
        
        NSString *last_name=[dictFacebook valueForKey:@"last_name"];
        
        if ([last_name isEqualToString:@""]) {
            last_name=@"";
            
        }
        else{
            last_name=[dictFacebook valueForKey:@"last_name"];
        }
        
        NSString *gender=[dictFacebook valueForKey:@"gender"];
        if([gender isEqualToString:@""]) {
            gender=@"";
            
        }
        else{
            gender=[dictFacebook valueForKey:@"gender"];
        }
        
        if ([gender  isEqualToString:@"male"]) {
                male_btn.selected=NO;
                female_btn.selected=YES;
            maleFemaleStr=@"1";
        }
        else{
            male_btn.selected=YES;
            female_btn.selected=NO;
            maleFemaleStr=@"0";
        }
        NSString *fbid=nil;
        fbid=[dictFacebook valueForKey:@"id"];
        if([fbid isEqualToString:@""]) {
            fbid=@"";
            
        }
        else{
            fbid=[dictFacebook valueForKey:@"id"];
        }
        
       
       
        NSDictionary *picFBDict=[dictFacebook valueForKey:@"picture"];
        if ([picFBDict isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDict=[picFBDict valueForKey:@"data"];
            if ([dataDict isKindOfClass:[NSDictionary class]]) {
                NSString *imageStrUrl=[dataDict valueForKey:@"url"];
            NSURL *tempURL = [NSURL URLWithString:imageStrUrl];
            NSData *tempData = [NSData dataWithContentsOfURL:tempURL];
            imgfacebook =[[UIImage alloc] initWithData:tempData];
            if (imgfacebook!=nil) {
                imgfacebook=[Constant1 compressImage:imgfacebook];
                picturetaken_image.image=imgfacebook;
                imagestrdata=[self base64String:imgfacebook];
            }
            else{
                imagestrdata=@"";
            }
            }
        }

        _emailTxt1.text=email;
        _emailTxt1.delegate=self;
        _passwordTXT1.text=@"";
        _passwordTXT1.delegate=self;
        _fisrtNameTXT.text=firstname;
        _fisrtNameTXT.delegate=self;
        _lastNmaeTXT.text=last_name;
        _lastNmaeTXT.delegate=self;
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"fbvalue"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else{
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View1"]];
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View2"]];
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View3"]];
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View4"]];
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View5"]];
        [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View6"]];
        

        
        viewcon1= [self.childViewControllers objectAtIndex:0];
        viewcon2= [self.childViewControllers objectAtIndex:1];
        viewcon3= [self.childViewControllers objectAtIndex:2];
        viewcon4= [self.childViewControllers objectAtIndex:3];
        viewcon5= [self.childViewControllers objectAtIndex:4];
        viewcon6= [self.childViewControllers objectAtIndex:5];
        
        UIButton *View1Button,*View2Button,*View3Button,*View4Button,*View5Button ;
        View1Button=[viewcon1.view viewWithTag:112];
        if ([[viewcon1.view viewWithTag:999] class]==[UITextView class]) {
            [[viewcon1.view viewWithTag:999] setUserInteractionEnabled:true];
            NSString *text=@"By Tapping Contine, you agree to our Terms of Service and Privacy Policy";
            NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:text];
            NSRange TermsRange = [text rangeOfString:@"Terms of Service"];
            NSRange PrivacyRange = [text rangeOfString:@"Privacy Policy"];
            
            [str addAttribute: NSLinkAttributeName value:urlTermCondiatiom range: TermsRange];
            [str addAttribute: NSLinkAttributeName value:urlPrivacy range: PrivacyRange];

            
            
            [(UITextView *)[viewcon1.view viewWithTag:999] setTintColor:View1Button.backgroundColor];
            [(UITextView *)[viewcon1.view viewWithTag:999] setAttributedText:str ];
            [(UITextView *)[viewcon1.view viewWithTag:999] setTextColor:[UIColor grayColor]];
            [(UITextView *)[viewcon1.view viewWithTag:999] setTextAlignment:NSTextAlignmentCenter];
            [(UITextView *)[viewcon1.view viewWithTag:999] setFont:[UIFont systemFontOfSize:13]];
        }
        

        
        [View1Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
        View2Button=[viewcon2.view viewWithTag:212];
        [View2Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
        View3Button=[viewcon3.view viewWithTag:312];
        [View3Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
        View4Button=[viewcon4.view viewWithTag:411];
        [View4Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
        View5Button=[viewcon5.view viewWithTag:511];
        [View5Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton* cameraPick=[viewcon6.view viewWithTag:611];
        [cameraPick addTarget:self action:@selector(gallery) forControlEvents:UIControlEventTouchUpInside];
        UIButton*  galleryPick=[viewcon6.view viewWithTag:612];
        [galleryPick addTarget:self action:@selector(camera) forControlEvents:UIControlEventTouchUpInside];
        btnDate=[viewcon4.view viewWithTag:408];
        [btnDate addTarget:self action:@selector(datepicker:) forControlEvents:UIControlEventTouchUpInside];
        btnMonth=[viewcon4.view viewWithTag:409];
        [btnMonth addTarget:self action:@selector(monthpicker:) forControlEvents:UIControlEventTouchUpInside];
        btnYear=[viewcon4.view viewWithTag:410];
        [btnYear addTarget:self action:@selector(yearpicker:) forControlEvents:UIControlEventTouchUpInside];
        picturetaken_image=[viewcon6.view viewWithTag:606];
        picturetaken_image.layer.cornerRadius=picturetaken_image.frame.size.width/2;
        picturetaken_image.layer.borderColor=[UIColor whiteColor].CGColor;
        picturetaken_image.layer.borderWidth=0.5f;
        picturetaken_image.clipsToBounds=YES;
        
        btnDate.layer.borderWidth=1.0f;
        btnDate.layer.borderColor=[UIColor lightGrayColor].CGColor;
        btnMonth.layer.borderWidth=1.0f;
        btnMonth.layer.borderColor=[UIColor lightGrayColor].CGColor;
        btnYear.layer.borderWidth=1.0f;
        btnYear.layer.borderColor=[UIColor lightGrayColor].CGColor;
        _btn_skipOutlet.hidden=YES;
        _but_submitOutlet.hidden=YES;
        
        male_btn=[viewcon5.view viewWithTag:510];
        [male_btn addTarget:self action:@selector(male_act:) forControlEvents:UIControlEventTouchUpInside];
        female_btn=[viewcon5.view viewWithTag:509];
        [female_btn addTarget:self action:@selector(female_act:) forControlEvents:UIControlEventTouchUpInside];
        
        _emailTxt1=[viewcon1.view viewWithTag:110];
        _emailTxt1.delegate=self;
        _passwordTXT1=[viewcon2.view viewWithTag:210];
        _passwordTXT1.delegate=self;
        _fisrtNameTXT=[viewcon3.view viewWithTag:309];
        _fisrtNameTXT.delegate=self;
        _lastNmaeTXT=[viewcon3.view viewWithTag:310];
        _lastNmaeTXT.delegate=self;
    }
    
    _emailTxt1.returnKeyType=UIReturnKeyDone;
//    _emailTxt1.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _passwordTXT1.returnKeyType=UIReturnKeyDone;
    _lastNmaeTXT.returnKeyType=UIReturnKeyDone;
    placeholdertext1=[[NSString alloc]init];
    scrollView.scrollEnabled=false;
    
//    [[_fisrtNameTXT layer] setBorderWidth:1.0f];
//    [[_fisrtNameTXT layer] setBorderColor:[UIColor grayColor].CGColor];
//    
//    [[_lastNmaeTXT layer] setBorderWidth:1.0f];
//    [[_lastNmaeTXT layer] setBorderColor:[UIColor grayColor].CGColor];
//    
//    [[_emailTxt1 layer] setBorderWidth:1.0f];
//    [[ _emailTxt1 layer] setBorderColor:[UIColor grayColor].CGColor];
//    
//    [[_passwordTXT1 layer] setBorderWidth:1.0f];
//    [[_passwordTXT1 layer] setBorderColor:[UIColor grayColor].CGColor];
    
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionLeft;
    [scrollView addGestureRecognizer:swiperight];
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionRight;
    [scrollView addGestureRecognizer:swipeleft];
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    _dropdown.signupScreen=@"Yes";
  //  _dropdown.font=[UIFont systemFontOfSize:30.0f];
    dateArray=[[NSMutableArray alloc]init];
    monthArray=[[NSMutableArray alloc]init];
    yearArray=[[NSMutableArray alloc]init];
    
    [monthArray addObject:@"Jan"];
    [monthArray addObject:@"Feb"];
    [monthArray addObject:@"Mar"];
    [monthArray addObject:@"Apr"];
    [monthArray addObject:@"May"];
    [monthArray addObject:@"June"];
    [monthArray addObject:@"July"];
    [monthArray addObject:@"Aug"];
    [monthArray addObject:@"Sept"];
    [monthArray addObject:@"Oct"];
    [monthArray addObject:@"Nov"];
    [monthArray addObject:@"Dec"];

    for (int i=1; i<32; i++) {
        [dateArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    [self yearlist];
    
    //    male_btn.selected=YES;
   
    if (IS_IPHONE6PLUS){
        _constraitTopskip_btn.constant=40;
        _constraitTopsubmit_btn.constant=20;
    }else if (IS_IPHONE6||IS_IPHONE5){
        _constraitTopskip_btn.constant=26;
        _constraitTopsubmit_btn.constant=13;
    }else if (IS_IPHONE4){
        _scroolVIew_heightConstraint.constant=320;
        
    }
    
    top=60;
    
    _fisrtNameTXT.returnKeyType=UIReturnKeyNext;
    
    _scroll_topConstraint.constant=top;
    [self.view  layoutIfNeeded];
    
    _emailTxt1.keyboardType=UIKeyboardTypeEmailAddress;
    self.pageControl.userInteractionEnabled=NO;
    
    UIImageView *userImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"eamil_icon"]];
    userImage.frame = CGRectMake(0, 0, userImage.image.size.width+20.0, userImage.image.size.height);
    userImage.contentMode = UIViewContentModeCenter;
    _emailTxt1.leftViewMode = UITextFieldViewModeAlways;
   _emailTxt1.leftView = userImage;
    
    
    
    UIImageView *password_image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password_icon"]];
    password_image.frame = CGRectMake(0, 0, password_image.image.size.width+20.0, password_image.image.size.height);
    password_image.contentMode = UIViewContentModeCenter;
    _passwordTXT1.leftViewMode = UITextFieldViewModeAlways;
    _passwordTXT1.leftView = password_image;
    
    
//
    UIImageView *firstname_image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_male_icon"]];
    firstname_image.frame = CGRectMake(0, 0, firstname_image.image.size.width+20.0, firstname_image.image.size.height);
    firstname_image.contentMode = UIViewContentModeCenter;
    _fisrtNameTXT.leftViewMode = UITextFieldViewModeAlways;
    _fisrtNameTXT.leftView = firstname_image;
//
    UIImageView *lastname_image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_male_icon"]];
    lastname_image.frame = CGRectMake(0, 0, firstname_image.image.size.width+20.0, firstname_image.image.size.height);
    lastname_image.contentMode = UIViewContentModeCenter;
    _lastNmaeTXT.leftViewMode = UITextFieldViewModeAlways;
    _lastNmaeTXT.leftView = lastname_image;
}


-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        Keyboard=YES;
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activefield=textField;
    placeholdertext1=activefield.placeholder;
    if (_emailTxt1==textField) {
        _emailTxt1.autocapitalizationType = UITextAutocapitalizationTypeNone;
    }
    else
    {
        activefield.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    }
    textField.placeholder=nil;
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    NSLog(@"Text field ended editing");
    activefield=textField;
    if ([textField.text isEqualToString:@""] || [[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
    {
        [textField setText:@""];
        textField.placeholder = placeholdertext1;
    }
    [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -60; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement = (up ? movementDistance : -movementDistance);
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    [self.view layoutIfNeeded];
    
}




- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] removeadd];
    if (pageControl.currentPage==5) {
        return;
    }
    for (NSUInteger i =0; i < [self.childViewControllers count]; i++) {
        [self loadScrollViewWithPage:i];
    }
    self.pageControl.currentPage = 0;
    _page = 0;
    [self.pageControl setNumberOfPages:[self.childViewControllers count]];
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    if (viewController.view.superview != nil) {
        [viewController viewWillAppear:animated];
    }
    self.scrollView.contentSize = CGSizeMake(screenRect.size.width * [self.childViewControllers count], scrollView.frame.size.height);
    self.scrollView.contentOffset=CGPointZero;
    self.navigationController.navigationBar.barTintColor=[self colorFromHexString:@"1AA8EA"];
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor  ];
    [ self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor  ]}];
}
-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (pageControl.currentPage==5) {
//        NSLog(@"hi");
        return;
    }
    if ([self.childViewControllers count]) {
        UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewDidAppear:animated];
        }
    }

    
  
    
}

- (void)viewWillDisappear:(BOOL)animated {
    if ([self.childViewControllers count]) {
        UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewWillDisappear:animated];
        }
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    if (viewController.view.superview != nil) {
        [viewController viewDidDisappear:animated];
    }
    [super viewDidDisappear:animated];
    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"signup"];

}



#pragma Registration mehtod
-(IBAction)skipMethod:(id)sender{
    [TenjinSDK sendEventWithName:@"Register_ProfilePic_Skip_Tapped"];
    [FBSDKAppEvents logEvent:@"Register_ProfilePic_Skip_Tapped"];
    UACustomEvent *event = [UACustomEvent eventWithName:@"Register_ProfilePic_Skip_Tapped"];
    [[UAirship shared].analytics addEvent:event];
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"Register_ProfilePic_Skip_Tapped"];

    

    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else
    {
        NSString *getpositionMonth;
        NSString *monthStr=btnMonth.titleLabel.text;
        for (int i=0; i<[monthArray count]; i++) {
            NSString *getMont=[NSString stringWithFormat:@"%@",[monthArray objectAtIndex:i]];
            if ([monthStr isEqualToString:getMont]) {
                getpositionMonth=[NSString stringWithFormat:@"%d",i+1];
            }
        }
        strDob = [NSString stringWithFormat: @"%@%@%@%@%@ ",btnYear.titleLabel.text,@"/",getpositionMonth,@"/",btnDate.titleLabel.text];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        NSDate *date = [dateFormatter dateFromString:strDob];
        NSString *finalDOb=[dateFormatter stringFromDate:date];
//        NSLog(@"final dob %@",finalDOb);
        NSDictionary *dict;
        dict = @{
                 @"first_name" :_fisrtNameTXT.text,
                 @"last_name" :_lastNmaeTXT.text,
                 @"email" :_emailTxt1.text,
                 @"password" :_passwordTXT1.text,
                 @"date_of_birth" :finalDOb,
                 @"gender":maleFemaleStr,
                 @"device_type":@"IOS",
                 @"advertise_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"adID"]
                 };
//        NSLog(@"dob is %@",dict);
        [[NSUserDefaults standardUserDefaults]setObject:_emailTxt1.text forKey:@"emailRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults]setObject:_passwordTXT1.text forKey:@"emailRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:signup withParameters:dict withHud:YES success:^(id response){
//            NSLog(@"response is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            NSDictionary *dataDict=response[@"data"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"token"] forKey:@"api_token"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"id"] forKey:@"userid"];
                            [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
//                            CongratulationViewController *congrats=[self.storyboard instantiateViewControllerWithIdentifier:@"CongratulationViewController"];
//                            [self.navigationController pushViewController:congrats animated:YES];
                            
                            // new tutorial screen
                            
                            [TenjinSDK sendEventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [FBSDKAppEvents logEvent:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            UACustomEvent *event = [UACustomEvent eventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [[UAirship shared].analytics addEvent:event];
                            
                                TutorialViewController *tut=[[TutorialViewController alloc]initWithNibName:@"TutorialViewController" bundle:nil];
                                tut.isFromFacebook = YES;
                            NSString *accountCreate=[Constant1 getCurrentDate];
                            Mixpanel *mixpanel = [Mixpanel sharedInstance];
                            
//                            [mixpanel registerSuperProperties:@{@"Email":_emailTxt1 ,@"Registration method":@"SkipMethod",@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":@"NO"}];
                            
                            [mixpanel.people set:@{@"$email":_emailTxt1,@"Registration method":@"SkipMethod",@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":@"NO"}];
                            
                            [mixpanel track:@"Sign up"
                                 properties:@{@"Email":_emailTxt1,@"Registration method":@"SkipMethod",@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":@"NO"}];
                            
                            
                            
                            
                                [self.navigationController pushViewController:tut animated:YES];
                            
                        }
                        else{
                            NSMutableArray *emailDict =nil;
                            emailDict=[response valueForKey:@"message"];
                            NSString *message=[emailDict objectAtIndex:0];
                            [self.view makeToast:message duration:1.0 position:CSToastPositionCenter];
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
    //    CongratulationViewController *congrats=[self.storyboard instantiateViewControllerWithIdentifier:@"CongratulationViewController"];
    //    [self.navigationController pushViewController:congrats animated:YES];
    
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==4){
        if (buttonIndex == 0){
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                CongratulationViewController *congrats=[self.storyboard instantiateViewControllerWithIdentifier:@"CongratulationViewController"];
                [self.navigationController pushViewController:congrats animated:YES];
                
            });
            
            
        }
    }
    
    if(alertView.tag==10){
        if (buttonIndex == 0){
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                CongratulationViewController *congrats=[self.storyboard instantiateViewControllerWithIdentifier:@"CongratulationViewController"];
                [self.navigationController pushViewController:congrats animated:YES];
                
            });
            
            
        }
    }
    
    
}

-(void)fbloginContinue{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else
    {
        NSDictionary *picFBDict=[dictFacebook valueForKey:@"picture"];
        if ([picFBDict isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDict=[picFBDict valueForKey:@"data"];
            if ([dataDict isKindOfClass:[NSDictionary class]]) {
                NSString *imageStrUrl=[dataDict valueForKey:@"url"];
                NSURL *tempURL = [NSURL URLWithString:imageStrUrl];
                NSData *tempData = [NSData dataWithContentsOfURL:tempURL];
                imgfacebook =[[UIImage alloc] initWithData:tempData];
                if (imgfacebook!=nil) {
                    imgfacebook=[Constant1 compressImage:imgfacebook];
                    imagestrdata=[self base64String:imgfacebook];
                }
                else{
                    imagestrdata=@"";
                }
            }
        }
        
        NSDictionary *facebookDict=[[NSUserDefaults standardUserDefaults]valueForKey:@"fbDictionay"];
        NSString *facebookId=[facebookDict valueForKey:@"id"];
        
        NSString *getpositionMonth;
        NSString *monthStr=btnMonth.titleLabel.text;
        for (int i=0; i<[monthArray count]; i++) {
            NSString *getMont=[NSString stringWithFormat:@"%@",[monthArray objectAtIndex:i]];
            if ([monthStr isEqualToString:getMont]) {
                getpositionMonth=[NSString stringWithFormat:@"%d",i+1];
            }
        }
        strDob = [NSString stringWithFormat: @"%@%@%@%@%@ ",btnYear.titleLabel.text,@"/",getpositionMonth,@"/",btnDate.titleLabel.text];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        NSDate *date = [dateFormatter dateFromString:strDob];
        NSString *finalDOb=[dateFormatter stringFromDate:date];
        NSDictionary *dict;
        dict = @{
                 @"first_name" :_fisrtNameTXT.text,
                 @"last_name" :_lastNmaeTXT.text,
                 @"email" :_emailTxt1.text,
                 @"password" :@"",
                 @"date_of_birth" :finalDOb,
                 @"image":imagestrdata,
                 @"gender":maleFemaleStr,
                 @"facebookId":facebookId,
                 @"device_type":@"IOS",
                 @"advertise_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"adID"]
                 };
        
        [[NSUserDefaults standardUserDefaults]setObject:_emailTxt1.text forKey:@"emailRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults]setObject:_passwordTXT1.text forKey:@"passRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:signupfb withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            
                            [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"api_token"] forKey:@"api_token"];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"id"] forKey:@"userid"];
                            
                            [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                                                        
                            NSString *is_register=[NSString stringWithFormat:@"%@",[response valueForKey:@"is_register"]];
                            
                            if ([is_register isEqualToString:@"1"]) {
                                
                            [TenjinSDK sendEventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [FBSDKAppEvents logEvent:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                                [mixpanel track:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                             UACustomEvent *event = [UACustomEvent eventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                                
                                [[UAirship shared].analytics addEvent:event];
                                TutorialViewController *tut=[[TutorialViewController alloc]initWithNibName:@"TutorialViewController" bundle:nil];
                                tut.isFromFacebook = NO;
                            [self.navigationController pushViewController:tut animated:YES];
                                
                             }
                            else{
                                [appDelegate setRoots];
                                NSString *accountCreate=[Constant1 getCurrentDate];
                                Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                                [mixpanel registerSuperProperties:@{@"Email":_emailTxt1 ,@"Registration method":@"fbloginContinue",@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":@"YES"}];
                                
                                [mixpanel.people set:@{@"$email":_emailTxt1,@"Registration method":@"fbloginContinue",@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":@"YES"}];
                                
                                [mixpanel track:@"Sign up"
                                     properties:@{@"Email":_emailTxt1,@"Registration method":@"fbloginContinue",@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":@"YES"}];
                            }
                            
                        }
                        else{
                            NSMutableArray *emailDict =nil;
                            emailDict=[response valueForKey:@"message"];
                            NSString *message=[emailDict objectAtIndex:0];
                            [self.view makeToast:message duration:1.0 position:CSToastPositionCenter];
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }

}





-(void)sendToServer{
    
    
    NSString *imagestr=nil;
    if (chosenImage!=nil) {
        chosenImage=[Constant1 compressImage:chosenImage];
        imagestr=[self base64String:chosenImage];
    }
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else
    {
        
        NSString *getpositionMonth;
        NSString *monthStr=btnMonth.titleLabel.text;
        for (int i=0; i<[monthArray count]; i++) {
            NSString *getMont=[NSString stringWithFormat:@"%@",[monthArray objectAtIndex:i]];
            if ([monthStr isEqualToString:getMont]) {
                getpositionMonth=[NSString stringWithFormat:@"%d",i+1];
            }
        }
        strDob = [NSString stringWithFormat: @"%@%@%@%@%@ ",btnYear.titleLabel.text,@"/",getpositionMonth,@"/",btnDate.titleLabel.text];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        NSDate *date = [dateFormatter dateFromString:strDob];
        NSString *finalDOb=[dateFormatter stringFromDate:date];
//        NSLog(@"final dob %@",finalDOb);
        
        NSDictionary *dict;
        dict = @{
                 @"first_name" :_fisrtNameTXT.text,
                 @"last_name" :_lastNmaeTXT.text,
                 @"email" :_emailTxt1.text,
                 @"password" :_passwordTXT1.text,
                 @"date_of_birth" :finalDOb,
                 @"image":imagestr,
                 @"gender":maleFemaleStr,
                 @"device_type":@"IOS",
                 @"advertise_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"adID"]
                 };
        
//        NSLog(@"male check %@",maleFemaleStr);
//        NSLog(@"dict %@",dict);
        
        [[NSUserDefaults standardUserDefaults]setObject:_emailTxt1.text forKey:@"emailRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults]setObject:_passwordTXT1.text forKey:@"passRegistrationBTN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:signup withParameters:dict withHud:YES success:^(id response){
//            NSLog(@"dict is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            NSDictionary *dataDict=response[@"data"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"token"] forKey:@"api_token"];
                            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"id"] forKey:@"userid"];
                            [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
//                            CongratulationViewController *congrats=[self.storyboard instantiateViewControllerWithIdentifier:@"CongratulationViewController"];
//                            [self.navigationController pushViewController:congrats animated:YES];
                            
                            [TenjinSDK sendEventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [FBSDKAppEvents logEvent:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            UACustomEvent *event = [UACustomEvent eventWithName:@"EVENT_NAME_COMPLETED_REGISTRATION"];
                            [[UAirship shared].analytics addEvent:event];
 
                            
                            TutorialViewController *tut=[[TutorialViewController alloc]initWithNibName:@"TutorialViewController" bundle:nil];
                            tut.isFromFacebook = NO;
                            
                            NSString *accountCreate=[Constant1 getCurrentDate];
                            Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                            [mixpanel registerSuperProperties:@{@"Email":_emailTxt1 ,@"Registration method":@"sendToServer",@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":@"YES"}];
                            
                            [mixpanel.people set:@{@"$email":_emailTxt1,@"Registration method":@"sendToServer",@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":@"YES"}];
                            
                            [mixpanel track:@"Sign up"
                                 properties:@{@"Email":_emailTxt1,@"Registration method":@"sendToServer",@"Gender":maleFemaleStr,@"Birthdate":finalDOb,@"Account created date":accountCreate,@"Uploaded Picture":@"YES"}];
                            
                            [self.navigationController pushViewController:tut animated:YES];

                            
                            
                            
                            //                            NSMutableArray *email = [[NSMutableArray alloc]init];
                            //                            email=[response valueForKey:@"message"];
                            //                            UIAlertView  *skpiAlert1=[[UIAlertView alloc]initWithTitle:appTitle message:@"Thank you for Registration !" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            //                            skpiAlert1.delegate=self;
                            //                            skpiAlert1.tag=10;
                            //                            [skpiAlert1 show];
                        }
                        else{
                            NSMutableArray *emailDict =nil;
                            emailDict=[response valueForKey:@"message"];
                            NSString *message=[emailDict objectAtIndex:0];
                            
                            [self.view makeToast:message duration:1.0 position:CSToastPositionCenter];
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
}

#pragma mark - camera and gallery Methods.
-(void)gallery{
    _btn_skipOutlet.hidden=YES;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [TenjinSDK sendEventWithName:@"Register_ProfilePic_UploadPic_Tapped"];
    [FBSDKAppEvents logEvent:@"Register_ProfilePic_UploadPic_Tapped"];
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_ProfilePic_UploadPic_Tapped"]];
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"Register_ProfilePic_UploadPic_Tapped"];

    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)camera{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [TenjinSDK sendEventWithName:@"Register_ProfilePic_TakePic_Tapped"];
    [FBSDKAppEvents logEvent:@"Register_ProfilePic_TakePic_Tapped"];
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"Register_ProfilePic_TakePic_Tapped"];
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_ProfilePic_TakePic_Tapped"]];
    [self presentViewController:picker animated:YES completion:NULL];
}
#pragma mark - imagePickerController Delegate methods.

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    NSLog(@"page is%ld",(long)pageControl.currentPage);
    chosenImage = info[UIImagePickerControllerEditedImage];
    if (chosenImage!=nil) {
        picturetaken_image.image=chosenImage;
        _but_submitOutlet.hidden=NO;
        if (IS_IPHONE4){
            _scroll_topConstraint.constant=top-32;
            [self.view layoutIfNeeded];
        }
    }
//    NSLog(@"current apge is%ld",(long)self.pageControl.currentPage);
    [picker dismissViewControllerAnimated:NO completion:NULL];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
#pragma female&malemethod
-(void)male_act:(id)sender{
    male_btn.selected=YES;
    female_btn.selected=NO;
    maleFemaleStr=@"0";
    
}
-(void)female_act:(id)sender{
    male_btn.selected=NO;
    female_btn.selected=YES;
    maleFemaleStr=@"1";
}
-(NSString*)base64String:(UIImage*)image{
    NSString *base64EncodeStr = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    base64EncodeStr = [base64EncodeStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    return base64EncodeStr;
}
#pragma datepicker&monthpicker&yearpicker
-(void)datepicker:(id)sender{
    checkDay=@"D";
    [btnDate.titleLabel setTextAlignment: NSTextAlignmentLeft];
    [self showDropDownForButton:sender adContents:dateArray multipleSelection:NO];
}
-(void)monthpicker:(id)sender{
    checkMonth=@"M";
    [btnMonth.titleLabel setTextAlignment: NSTextAlignmentLeft];
    [self showDropDownForButton:sender adContents:monthArray multipleSelection:NO];
}

-(void)yearpicker:(id)sender{
    yearPicked=YES;
    checkYear=@"Y";
    [btnYear.titleLabel setTextAlignment: NSTextAlignmentLeft];
    [self showDropDownForButton:sender adContents:yearArray multipleSelection:NO];
}


-(void)yearlist{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    if (!appDelegate.isReachable) {
        
        return ;
    }
    
    else{
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"getyears" withParameters:nil withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        for (int k=0; k<[[response valueForKey:@"data"] count]; k++) {
                            [yearArray addObject:[NSString stringWithFormat:@"%@",[[response valueForKey:@"data"] objectAtIndex:k]]];
                            
                        }
                        yearArray=[[[yearArray reverseObjectEnumerator] allObjects] mutableCopy];
                    }
                }
                
            }
            
        } errorBlock:^(id error)
         
         {
             
         }];
    }
}




#pragma mark - VSDropdown Delegate methods.


-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents multipleSelection:(BOOL)multipleSelection
{
    [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"signup"];
    [_dropdown setupDropdownForView:sender];
    
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    
    if (_dropdown.allowMultipleSelection)
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:[[sender titleForState:UIControlStateNormal] componentsSeparatedByString:@";"]];
        
    }
    else
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
        
    }
    
}
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected
{
    UIButton *btn = (UIButton *)dropDown.dropDownView;
    
    NSString *allSelectedItems = nil;
    if (dropDown.selectedItems.count > 1)
    {
        allSelectedItems = [dropDown.selectedItems componentsJoinedByString:@";"];
        
    }
    else
    {
        allSelectedItems = [dropDown.selectedItems firstObject];
        
    }
    [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    if ([checkMonth isEqualToString:@"M"]) {
        checkMonth=allSelectedItems;
        [[NSUserDefaults standardUserDefaults] setObject:allSelectedItems forKey:@"month"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    
    if ([checkDay isEqualToString:@"D"]) {
        checkDay=allSelectedItems;
        
        [[NSUserDefaults standardUserDefaults] setObject:allSelectedItems forKey:@"day"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    if ([checkYear isEqualToString:@"Y"]) {
        checkYear=allSelectedItems;
        [[NSUserDefaults standardUserDefaults] setObject:allSelectedItems forKey:@"year"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    

    
    NSString *birthday=[NSString stringWithFormat:@"%@/%@/%@", [[NSUserDefaults standardUserDefaults]valueForKey:@"month"], [[NSUserDefaults standardUserDefaults]valueForKey:@"day"], [[NSUserDefaults standardUserDefaults]valueForKey:@"year"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    date1 = [formatter dateFromString:birthday];
    
    NSInteger ageBirth=[self age:date1];
    [[NSUserDefaults standardUserDefaults]setInteger:ageBirth forKey:@"agebirth"];
    
}

- (NSInteger)age:(NSDate *)dateOfBirth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *dateComponentsNow = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDateComponents *dateComponentsBirth = [calendar components:unitFlags fromDate:dateOfBirth];
    
    if (([dateComponentsNow month] < [dateComponentsBirth month]) ||
        (([dateComponentsNow month] == [dateComponentsBirth month]) && ([dateComponentsNow day] < [dateComponentsBirth day]))) {
        return [dateComponentsNow year] - [dateComponentsBirth year] - 1;
    } else {
        return [dateComponentsNow year] - [dateComponentsBirth year];
    }
}

#pragma mark - swipeleft & swiperight methods.
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self.view endEditing:YES];
    [self forwardPage];
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self.view endEditing:YES];
    [self backPage];
}
-(void)backPage{
    
    if(_page==0){
        
    }else{
        // update the scroll view to the appropriate page
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * (_page - 1);
        frame.origin.y = 0;
        UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
        UIViewController *newViewController = [self.childViewControllers objectAtIndex:_page - 1];
        [oldViewController viewWillDisappear:YES];
        [newViewController viewWillAppear:YES];
        [self.scrollView scrollRectToVisible:frame animated:YES];
        self.pageControl.currentPage = _page - 1;
        // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
        _pageControlUsed = YES;
        _but_submitOutlet.hidden=YES;
        _btn_skipOutlet.hidden=YES;
        
        
        
    }
    
    //   _scroll_topConstraint.constant=  _scroll_topConstraint.constant+15;
    _scroll_topConstraint.constant=top;
    [self.view  layoutIfNeeded];
    
}
-(void)forwardPage{
    
//    NSLog(@"constra Scroll %f",_scroll_topConstraint.constant);
//    
//    NSLog(@"forwardPage Scrollview x%f \n %f \n ",scrollView.contentOffset.x,scrollView.contentOffset.y);
    
    [activefield resignFirstResponder];
    
    if ([self check:pageControl.currentPage]==NO){
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * (_page+1);
        frame.origin.y = 0;
        
        if(_page==[self.childViewControllers count]-1)
        {
            return;
        }
        
        UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
        UIViewController *newViewController = [self.childViewControllers objectAtIndex:_page +1];
        [oldViewController viewWillDisappear:YES];
        [newViewController viewWillAppear:YES];
        [self.scrollView scrollRectToVisible:frame animated:YES];
        self.pageControl.currentPage = _page+1 ;
        // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
        _pageControlUsed = YES;
        if (isFacebookData) {
            if (self.pageControl.currentPage==4) {
                if (imgfacebook!=nil) {
                    if (IS_IPHONE4){
                        _scroll_topConstraint.constant=top-32;
                        [self.view layoutIfNeeded];
                    }
                   _but_submitOutlet.hidden=NO;
                }
                else{
                    _btn_skipOutlet.hidden=NO;
                }
                }
            }
        else{
        if (self.pageControl.currentPage==5) {
            if (chosenImage!=nil) {
                if (IS_IPHONE4){
                    _scroll_topConstraint.constant=top-32;
                    [self.view layoutIfNeeded];
                }
                _but_submitOutlet.hidden=NO;
            }
            else{
            _btn_skipOutlet.hidden=NO;
            }
        }
    }
    
    }
    
}


-(BOOL)check:(NSUInteger  )methodNum
{
    NSString *errorMessage=nil;
    if (methodNum==0) {
        errorMessage = [self validateFields];
        if (errorMessage==nil) {
            [TenjinSDK sendEventWithName:@"Register_Email_Continue_Tapped"];
            [FBSDKAppEvents logEvent:@"Register_Email_Continue_Tapped"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_Email_Continue_Tapped"]];
//            Mixpanel *mixpanel = [Mixpanel sharedInstance];
//            [mixpanel track:@"Register_Email_Continue_Tapped"];

            
        }
    }else if(methodNum==1){
        
        if (isFacebookData) {
            errorMessage = [self validateFieldlastname];
            if (errorMessage==nil) {
                [TenjinSDK sendEventWithName:@"Register_Name_Continue_Tapped"];
                [FBSDKAppEvents logEvent:@"Register_Name_Continue_Tapped"];
                [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_Name_Continue_Tapped"]];
//                Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                [mixpanel track:@"Register_Name_Continue_Tapped"];
            }
 
        }
        else{
            errorMessage = [self validateFieldpassword];
        if (errorMessage==nil) {
            
            [TenjinSDK sendEventWithName:@"Register_Password_Continue_Tapped"];
            [FBSDKAppEvents logEvent:@"Register_Password_Continue_Tapped"];
             [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_Password_Continue_Tapped"]];
//            Mixpanel *mixpanel = [Mixpanel sharedInstance];
//            [mixpanel track:@"Register_Password_Continue_Tapped"];
        }
            
        }
    }else if(methodNum==2){
        if (isFacebookData) {
             errorMessage = [self btn_brthday];
            if (errorMessage==nil) {
                if (isFacebookData) {
                    [TenjinSDK sendEventWithName:@"facebook_Register_Birthday_Continue_Tapped"];
                    [FBSDKAppEvents logEvent:@"facebook_Register_Birthday_Continue_Tapped"];
                    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"facebook_Register_Birthday_Continue_Tapped"]];
//                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                    [mixpanel track:@"facebook_Register_Birthday_Continue_Tapped"];

                }

            }
            
        }
        else{
        errorMessage = [self validateFieldlastname];
        if (errorMessage==nil) {
            [TenjinSDK sendEventWithName:@"Register_Name_Continue_Tapped"];
            [FBSDKAppEvents logEvent:@"Register_Name_Continue_Tapped"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_Name_Continue_Tapped"]];
//            Mixpanel *mixpanel = [Mixpanel sharedInstance];
//            [mixpanel track:@"Register_Name_Continue_Tapped"];
        }
            
        }
    }else if(methodNum==3){
        errorMessage = [self btn_brthday];
        if (errorMessage==nil) {
        if ([maleFemaleStr isEqualToString:@"0"]||[maleFemaleStr isEqualToString:@"1"]) {
            if (isFacebookData) {
            [TenjinSDK sendEventWithName:@"facebook_Register_Gender_Continue_Tapped"];
            [FBSDKAppEvents logEvent:@"facebook_Register_Gender_Continue_Tapped"];
            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"facebook_Register_Gender_Continue_Tapped"]];
//                Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                [mixpanel track:@"facebook_Register_Gender_Continue_Tapped"];

            }
            }
            else{
                [TenjinSDK sendEventWithName:@"Register_Birthday_Continue_Tapped"];
                [FBSDKAppEvents logEvent:@"Register_Birthday_Continue_Tapped"];
                [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_Birthday_Continue_Tapped"]];
//                Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                [mixpanel track:@"Register_Birthday_Continue_Tapped"];
            }
            
        }
    }else if(methodNum==4){
        
        if ([maleFemaleStr isEqualToString:@"0"]||[maleFemaleStr isEqualToString:@"1"]) {
            if (isFacebookData) {
               
                }
            else{
                [TenjinSDK sendEventWithName:@"Register_Gender_Continue_Tapped"];
                [FBSDKAppEvents logEvent:@"Register_Gender_Continue_Tapped"];
                [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_Gender_Continue_Tapped"]];
//                Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                [mixpanel track:@"Register_Gender_Continue_Tapped"];

            }
            
        }else{
            if (isFacebookData) {
                
            }
            else{
            errorMessage=@"Please select gender";
            }
        }
        
    }
    
    if (errorMessage) {
        if (!alert) {
            alert= [[UIAlertView alloc] initWithTitle:appTitle message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] ;
            [alert show];
            alert=nil;
        }
        return YES;
        
    }
    return NO;
    
}

-(NSString*)btn_brthday{
    NSString *errorMessage=nil;
    if ([btnDate.titleLabel.text isEqualToString:@"Day"]|| ([btnYear.titleLabel.text isEqualToString:@"Year"])||([btnMonth.titleLabel.text isEqualToString:@"Month"])) {
        errorMessage=birthday;
        return errorMessage;
    }else{
        NSInteger ageBirth=[self age:date1];
        [[NSUserDefaults standardUserDefaults]setInteger:ageBirth forKey:@"agebirth"];
        if (yearPicked==YES) {
            if (ageBirth<=12) {
                errorMessage=@"you must be 13 years old to sign up";
                return errorMessage;
            }
        }
        
    }
    return 0;
}
- (NSString *)validateFieldlastname {
    NSString *errorMessage ;
    NSString *strlastname = [_lastNmaeTXT.text removeWhiteSpaces];
    NSString *firstrlastname = [_fisrtNameTXT.text removeWhiteSpaces];
    if ([strlastname length] == 0) {
        errorMessage =LastnameStr;
        return errorMessage;
    }else{
        if ([firstrlastname length] == 0){
            errorMessage =FirstnameStr;
            return errorMessage;
        }
        return 0;
    }
    
    return 0;
}
- (NSString *)validateFieldpassword {
    NSString *errorMessage ;
    NSString *strPassword = [self.passwordTXT1.text removeWhiteSpaces];
    if ([strPassword length] == 0 || [strPassword length] < 6) {
        errorMessage =EnterPassword;
        return errorMessage;
    }
    
    
    
    
    return 0;
}
- (NSString *)validateFields {
    NSString *errorMessage ;
    NSString *strEmail = [_emailTxt1.text removeWhiteSpaces];
    if ([strEmail length] == 0) {
        errorMessage =EnterEmail;
        return errorMessage;
    }
    BOOL isEmailvalid = [Constant1 validateEmail:strEmail];
    if (!isEmailvalid) {
        errorMessage =InvalidEmail;
        return errorMessage;
    }
    return 0;
}
#pragma mark scrollview animate methods
- (BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers {
    return NO;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    [viewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    _rotating = YES;
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    [viewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    self.scrollView.contentSize = CGSizeMake(screenRect.size.width * [self.childViewControllers count], scrollView.frame.size.height);
    NSUInteger page = 0;
    for (viewController in self.childViewControllers) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        viewController.view.frame = frame;
        page++;
    }
    
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * _page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:NO];
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    _rotating = NO;
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    [viewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (void)loadScrollViewWithPage:(int)page {
    if (page < 0)
        return;
    if (page >= [self.childViewControllers count])
        return;
    // replace the placeholder if necessary
    UIViewController *controller = [self.childViewControllers objectAtIndex:page];
    if (controller == nil) {
        return;
    }
    
    // add the controller's view to the scroll view
    if (controller.view.superview == nil) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        frame.size.width = screenRect.size.width;
        controller.view.frame = frame;
        [self.scrollView addSubview:controller.view];
    }
}


- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
    UIViewController *newViewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    [oldViewController viewDidDisappear:YES];
    [newViewController viewDidAppear:YES];
    
    _page = self.pageControl.currentPage;
}

#pragma mark -
#pragma mark UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (_pageControlUsed || _rotating) {
        
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (self.pageControl.currentPage != page) {
        UIViewController *oldViewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
        UIViewController *newViewController = [self.childViewControllers objectAtIndex:page];
        [oldViewController viewWillDisappear:YES];
        [newViewController viewWillAppear:YES];
        self.pageControl.currentPage = page;
        [oldViewController viewDidDisappear:YES];
        [newViewController viewDidAppear:YES];
        _page = page;
        pagenumber=_page;
    }
    
}
// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    _pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _pageControlUsed = NO;
}
- (IBAction)btn_submit:(id)sender {
    [TenjinSDK sendEventWithName:@"Register_ProfilePic_Continue_Tapped"];
    [FBSDKAppEvents logEvent:@"Register_ProfilePic_Continue_Tapped"];
     [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"Register_ProfilePic_Continue_Tapped"]];
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"Register_ProfilePic_Continue_Tapped"];
    if (isFacebookData) {
       [self fbloginContinue];
    }
    else
    {
    [self sendToServer];
    }
}


@end
