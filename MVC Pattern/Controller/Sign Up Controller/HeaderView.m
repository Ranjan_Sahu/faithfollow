//
//  HeaderView.m
//  wireFrameSplash
//
//  Created by Vikas on 25/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "HeaderView.h"

@implementation HeaderView
@synthesize requestedBTN,updateheaderBTN;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (void) awakeFromNib
{
    [super awakeFromNib];
    
    //_mainView.layer.borderWidth=0.6f;
    //_mainView.layer.cornerRadius=8.0;
    //_mainView.layer.borderColor=[UIColor grayColor].CGColor;
    //_subView.layer.borderWidth=0.6f;
    //_subView.layer.cornerRadius=8.0;
   // _subView.layer.borderColor=[UIColor grayColor].CGColor;
    
    
    // header ad imageview
    self.headerADimageview.layer.borderWidth = 0.6f;
    self.headerADimageview.layer.borderColor = [UIColor grayColor].CGColor;
    self.headerADimageview.layer.cornerRadius = 8.0;
    self.headerADimageview.clipsToBounds = true;
    
    self.headerviewAD.layer.cornerRadius =8.0;
    self.headerviewAD.layer.masksToBounds = YES;
    
//    _upperView_imageview.layer.borderColor=[self colorFromHexString:@"009AD9"].CGColor;
//    _upperView_imageview.layer.borderWidth=0.8f;
//    _upperView_imageview.layer.cornerRadius=5.0;
//    _RequestView.layer.cornerRadius=5.0;
//    _updateView.layer.cornerRadius=5.0;
    
    //RS
    _lbl_HeaderSubView.adjustsFontSizeToFitWidth=true;
    if(IS_IPHONE4 || IS_IPHONE5)
    {
        [_textViewTXT setFont:[UIFont fontWithName:@"Avenir-Roman" size:12]];
        [_lbl_RequestPray setFont:[UIFont systemFontOfSize:12]];
        [_lbl_UpdateStatus setFont:[UIFont systemFontOfSize:12]];
        [_lbl_HeaderSubView setFont:[UIFont systemFontOfSize:13]];
    }
    else if(IS_IPHONE6)
    {
        [_textViewTXT setFont:[UIFont fontWithName:@"Avenir-Roman" size:13]];
        [_lbl_RequestPray setFont:[UIFont systemFontOfSize:13]];
        [_lbl_UpdateStatus setFont:[UIFont systemFontOfSize:13]];
        [_lbl_HeaderSubView setFont:[UIFont systemFontOfSize:14]];
    }
    else if (IS_IPHONE6PLUS)
    {
        [_textViewTXT setFont:[UIFont fontWithName:@"Avenir-Roman" size:14]];
        [_lbl_RequestPray setFont:[UIFont systemFontOfSize:14]];
        [_lbl_UpdateStatus setFont:[UIFont systemFontOfSize:14]];
        [_lbl_HeaderSubView setFont:[UIFont systemFontOfSize:15]];
    }
    //RS
}

-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
