//
//  WallView.h
//  wireFrameSplash
//
//  Created by Vikas on 25/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyFirstControllerDelegate<NSObject>
-(void) FunctionOne: (NSString*) dataOne andArray:(NSMutableArray*)arraycontent;
-(void) btn_pressed: (NSString*) buttonName andtotal_comments:(NSMutableArray*)totalComments  andpost:(NSDictionary*)post  andCellindex:(NSUInteger)cell_index;
-(void)openViewProfile: (NSString*) dataOne andArray:(NSMutableDictionary*)dictcontent;

@end
@interface WallView : UIView
@property (strong, nonatomic) IBOutlet UITableView *tableViewOutlet;

@property (weak, nonatomic) IBOutlet UIButton *status_btn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *leadingconstraints;

@property (strong, nonatomic) IBOutlet UIView *upperView;
@property (strong, nonatomic) NSString *checkviewwillApear;

@property (strong, nonatomic) NSString *selfProfilewallViewHidden;


@property (strong, nonatomic) NSMutableArray *suggetedArray;
@property (strong, nonatomic) IBOutlet UIImageView *upperView_imageview;

@property (weak, nonatomic) IBOutlet UITextView *textViewTXT;
@property (weak, nonatomic)  UIView *mainView;
@property(weak,nonatomic)NSString *call_View;
@property(assign,nonatomic)BOOL hideheader,checkPrayerCount;
@property(assign,nonatomic)BOOL dismischeck;
@property(assign,nonatomic)BOOL influencerimageValue;

@property(strong,nonatomic) NSMutableArray *arrWallFeeds;
@property (nonatomic, weak) id <MyFirstControllerDelegate>delegate1;
@property(strong,nonatomic) UIRefreshControl*  refreshControl;
@property (weak, nonatomic) IBOutlet UIView *rateViewFeed;
@property(strong,nonatomic)NSMutableArray *imageArray;




-(id) initWithframe:(CGRect)frame andUserInfo:(NSDictionary*)userinfo;
-(void)webservice_call;
-(void)initial_FeedserviceCall:(int)url_pagenumber;
-(void)imageProfile;
-(void)removeSuggetedGroup;
-(void)checkinfluencerlist;

-(void)suggetedImageview:(NSDictionary*)getDict;
-(void)suggeteduserProfile:(NSDictionary*)getDict;
-(void)FollowbtnMethod:(NSDictionary*)getDict;
-(void)updateCommentfromProfile:(NSDictionary*)updateComment;
-(void) reloadcellwallview:(NSDictionary*)reloadDict;
-(void)subject_touch:(UITapGestureRecognizer*)tap;







@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomRateViewConstraint;


@end
