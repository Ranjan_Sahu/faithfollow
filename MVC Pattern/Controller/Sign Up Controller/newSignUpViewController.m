//
//  newSignUpViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 10/12/16.
//  Copyright © 2016 home. All rights reserved.
//

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "newSignUpViewController.h"
#import "DeviceConstant.h"
#import "uploadProfileViewController.h"
#import "Mixpanel.h"
@interface newSignUpViewController ()
{
    UIView *vs;
    UITextField *activefield;
}
@end

@implementation newSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)attributelinknewSign {
    _linkAttributed.userInteractionEnabled = YES;
    NSString *text=@"By Clicking Join you agree to accept our terms and conditions";
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:text];
    NSRange TermsRange = [text rangeOfString:@"terms and conditions"];
    [str addAttribute: NSLinkAttributeName value:byclickingtermcondition range: TermsRange];
    [_linkAttributed  setTintColor:UIColorFromRGB(0x00ADEB)];
    [_linkAttributed setAttributedText:str ];
    [_linkAttributed setTextColor:[UIColor grayColor]];
    [_linkAttributed setTextAlignment:NSTextAlignmentCenter];
    if (IS_IPHONE4 || IS_IPHONE5) {
        [_linkAttributed setFont:[UIFont systemFontOfSize:10]];
    }
    else{
    [_linkAttributed setFont:[UIFont systemFontOfSize:13]];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if (vs) {
        [vs removeFromSuperview];
    }
    [self attributelinknewSign];
      _contenViewCorner.layer.cornerRadius=8.0f;
    [_contenViewCorner.layer setMasksToBounds:YES];
    self.navigationController.navigationBar.topItem.title =@"";
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
//    self.navigationItem.title = @"SIGN UP";
//    [self.navigationController.navigationBar
//     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.view.backgroundColor = [UIColor colorWithRed:0.0784 green:0.5294 blue:0.8784 alpha:1.0];
//    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:0.0784 green:0.5294 blue:0.8784 alpha:1.0];
//    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
//    vs = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];
//    [vs setBackgroundColor:[UIColor colorWithRed:0.0784 green:0.5294 blue:0.8784 alpha:1.0]];
//    [[[UIApplication sharedApplication] keyWindow] addSubview:vs];
    
//    [_emailTXT setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
//    [_firstnameTXT setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
//    [_lastnameTXT setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
//    [_passwordTXT setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.firstnameTXT.delegate=(id)self;
    self.emailTXT.delegate=(id)self;
    self.lastnameTXT.delegate=(id)self;
    self.passwordTXT.delegate=(id)self;
    
    _contentViewHeight.constant=40;
    
//   _isFromFacebooknew
    if (_isFromFacebooknew) {
        _emailTXT.text=_emailFBSTR;
        _firstnameTXT.text=_fisrtnameFBSTR;
        _lastnameTXT.text=_secondnameFBSTR;
        
        if (IS_IPHONE4) {
//            _contentViewHeight.constant=50;
        }else if (IS_IPHONE5) {
//            _contentViewHeight.constant=80;
            _Viewhiegth.constant=360;
            _OneStepBottomHeight.constant=10;
            _View1Height.constant=50;
            _view2Height.constant=50;
            _view3Height.constant=50;
            _view4Height.constant=0;
        }else if (IS_IPHONE6) {
//            _contentViewHeight.constant=100;
            _Viewhiegth.constant=400;
            _OneStepBottomHeight.constant=10;
            _View1Height.constant=60;
            _view2Height.constant=60;
            _view3Height.constant=60;
            _view4Height.constant=0;
        }else if (IS_IPHONE6PLUS) {
//            _contentViewHeight.constant=140;
            _Viewhiegth.constant=440;
            _OneStepBottomHeight.constant=10;
            _View1Height.constant=70;
            _view2Height.constant=70;
            _view3Height.constant=70;
            _view4Height.constant=0;
        }
        _fbView4HiddenView.hidden=YES;
    }
    else{
        
        if (IS_IPHONE4) {
//            _contentViewHeight.constant=30;
        }else if (IS_IPHONE5) {
//            _contentViewHeight.constant=60;
            //        _Viewhiegth.constant=360;
            _Viewhiegth.constant=380;
            //        _OneStepBottomHeight.constant=10;
            _View1Height.constant=50;
            _view2Height.constant=50;
            _view3Height.constant=50;
            _view4Height.constant=50;
            _joinBtnHeight.constant=50;
        }else if (IS_IPHONE6) {
//            _contentViewHeight.constant=75;
            _Viewhiegth.constant=430;
            _OneStepBottomHeight.constant=10;
            _View1Height.constant=60;
            _view2Height.constant=60;
            _view3Height.constant=60;
            _view4Height.constant=60;
            _joinBtnHeight.constant=60;
        }else if (IS_IPHONE6PLUS) {
//            _contentViewHeight.constant=90;
            _Viewhiegth.constant=480;
            _OneStepBottomHeight.constant=10;
            _View1Height.constant=70;
            _view2Height.constant=70;
            _view3Height.constant=70;
            _view4Height.constant=70;
            _joinBtnHeight.constant=70;
        }
        
    }

    self.emailView.layer.borderWidth = 1.0f;
    self.emailView.layer.borderColor = [UIColor colorWithRed:214.0/255.0 green:221.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    self.firstnameView.layer.borderWidth = 1.0f;
    self.firstnameView.layer.borderColor = [UIColor colorWithRed:214.0/255.0 green:221.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    self.lastnameView.layer.borderWidth = 1.0f;
    self.lastnameView.layer.borderColor = [UIColor colorWithRed:214.0/255.0 green:221.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    self.passwordView.layer.borderWidth = 1.0f;
    self.passwordView.layer.borderColor = [UIColor colorWithRed:214.0/255.0 green:221.0/255.0 blue:226.0/255.0 alpha:1.0].CGColor;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (_emailTXT==textField || _passwordTXT==textField ) {
        _emailTXT.autocapitalizationType = UITextAutocapitalizationTypeNone;
    }
    else
    {
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    }
   
}

-(void)viewWillDisappear:(BOOL)animated{
    if (vs) {
        [vs removeFromSuperview];
    }

}

#pragma mark - Validation

- (NSString *)validateFields {
    if (_isFromFacebooknew) {
        NSString *errorMessage ;
        NSString *strfirstname=nil;
        NSString *strEmail=nil;
        NSString *strlastname=nil;
        NSString *strpassword=nil;
        strfirstname = [self.firstnameTXT.text removeWhiteSpaces];
        strEmail = [self.emailTXT.text removeWhiteSpaces];
        strlastname = [self.lastnameTXT.text removeWhiteSpaces];
        strpassword = [self.passwordTXT.text removeWhiteSpaces];
        
        if ([strEmail length] == 0) {
            errorMessage =EnterEmail;
            return errorMessage;
        }
        BOOL isEmailvalid = [Constant1 validateEmail:strEmail];
        if (!isEmailvalid) {
            errorMessage =InvalidEmail;
            return errorMessage;
        }
        
        
        if ([strfirstname length] == 0) {
            errorMessage = FirstnameStr;
            return errorMessage;
        }
        if ([strlastname length] == 0) {
            errorMessage = LastnameStr;
            return errorMessage;
        }
    return errorMessage;
        
    }
    else{
    
    NSString *errorMessage ;
    NSString *strfirstname=nil;
    NSString *strEmail=nil;
    NSString *strlastname=nil;
    NSString *strpassword=nil;
    strfirstname = [self.firstnameTXT.text removeWhiteSpaces];
    strEmail = [self.emailTXT.text removeWhiteSpaces];
    strlastname = [self.lastnameTXT.text removeWhiteSpaces];
    strpassword = [self.passwordTXT.text removeWhiteSpaces];
   
    if ([strEmail length] == 0) {
        errorMessage =EnterEmail;
        return errorMessage;
    }
    BOOL isEmailvalid = [Constant1 validateEmail:strEmail];
    if (!isEmailvalid) {
        errorMessage =InvalidEmail;
        return errorMessage;
    }

    
    if ([strfirstname length] == 0) {
        errorMessage = FirstnameStr;
        return errorMessage;
    }
    if ([strlastname length] == 0) {
        errorMessage = LastnameStr;
        return errorMessage;
    }
    
    if ([strpassword length] == 0 || [strpassword length] < 6) {
            errorMessage =EnterPassword;
            return errorMessage;
        }
        
        
    return errorMessage;
        
    }
   return  nil;

}

- (IBAction)joinbuttonMethod:(id)sender {
    AppDelegate *appDelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    } else {
        NSString *errorMessage = [self validateFields];
        if (errorMessage) {
            [self.view makeToast:errorMessage  duration:1.0 position:CSToastPositionCenter];
            return;
        }
        [TenjinSDK sendEventWithName:@"one_step_join_clicked"];
        [FBSDKAppEvents logEvent:@"one_step_join_clicked"];
//        Mixpanel *mixpanel = [Mixpanel sharedInstance];
//        [mixpanel track:@"one_step_join_clicked"];
        [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"one_step_join_clicked"]];
    uploadProfileViewController *uploadView=[self.storyboard instantiateViewControllerWithIdentifier:@"uploadProfileViewController"];
        uploadView.emailStr=_emailTXT.text;
        uploadView.firstnameStr=_firstnameTXT.text;
        uploadView.lastnameStr=_lastnameTXT.text;
        uploadView.passwordStr=_passwordTXT.text;
        if (_isFromFacebooknew) {
            uploadView.imageSTRfromFB=_imageFBSTR;
            uploadView.fbimage=@"YESImage";
        }
    [self.navigationController pushViewController:uploadView animated:YES];
        
    }
}

- (IBAction)closeAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

@end
