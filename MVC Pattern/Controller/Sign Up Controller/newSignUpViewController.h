//
//  newSignUpViewController.h
//  wireFrameSplash
//
//  Created by webwerks on 10/12/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface newSignUpViewController : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *Viewhiegth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *OneStepBottomHeight;
@property(assign,nonatomic)BOOL isFromFacebooknew;
@property (weak, nonatomic) IBOutlet UIView *fbView4HiddenView;

@property (weak, nonatomic) IBOutlet UITextField *emailTXT;
@property (weak, nonatomic) IBOutlet UITextField *firstnameTXT;
@property (weak, nonatomic) IBOutlet UITextField *lastnameTXT;
@property (weak, nonatomic) IBOutlet UITextField *passwordTXT;

@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *firstnameView;
@property (weak, nonatomic) IBOutlet UIView *lastnameView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;

// view heigth
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *View1Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view3Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view4Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *joinBtnHeight;
@property (weak, nonatomic) IBOutlet UIView *contenViewCorner;
@property (weak, nonatomic) IBOutlet UIButton *joinBTN;

@property(strong,nonatomic)NSString *emailFBSTR;
@property(strong,nonatomic)NSString *fisrtnameFBSTR;
@property(strong,nonatomic)NSString *secondnameFBSTR;
@property(strong,nonatomic)NSString *imageFBSTR;

@property (weak, nonatomic) IBOutlet UITextView *linkAttributed;


- (IBAction)joinbuttonMethod:(id)sender;

@end
