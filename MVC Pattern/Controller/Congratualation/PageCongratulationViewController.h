//
//  PageCongratulationViewController.h
//  wireFrameSplash
//
//  Created by webwerks on 14/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageCongratulationViewController : UIViewController <UIScrollViewDelegate>
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;

- (IBAction)changePage:(id)sender;
@property (assign) NSUInteger page;
- (void)previousPage;
- (void)nextPage;

- (IBAction)next:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topscroolview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightofscrollview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *invitebuttontop;

@property (weak, nonatomic) IBOutlet UIButton *conitunebtn;


@end
//
//  ViewController.h
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//
