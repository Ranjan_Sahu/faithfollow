//
//  CongratulationViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 14/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "CongratulationViewController.h"
#import "InviteFriends.h"

@interface CongratulationViewController ()

@end

@implementation CongratulationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"CongratsView1"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"CongratsView2"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"CongratsView3"]];
    self.navigationController.navigationBarHidden=YES;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark invite_actMethod

- (IBAction)invite_act:(id)sender {
    InviteFriends *invite=[self.storyboard instantiateViewControllerWithIdentifier:@"InviteFriends"];
    [self.navigationController pushViewController:invite animated:YES];
}
@end
