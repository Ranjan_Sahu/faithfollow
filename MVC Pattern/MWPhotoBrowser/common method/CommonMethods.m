//
//  CommonMethods.m
//  Mytha
//
//  Created by webwerks on 07/10/15.
//  Copyright © 2015 webwerks. All rights reserved.
//

#import "CommonMethods.h"
#import <Foundation/Foundation.h>
#import "Reachability.h"
//#import "Constant.h"
#import "AppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>


@implementation CommonMethods
{
}
-(void)saveDataInFolder:(NSString *)folder withName:(NSString *)name forData:(NSArray *)dataDictionary
{
    NSData *tempData = [self requestBodyGeneratorWith:dataDictionary];
   [self Write:tempData toCacheFolder:folder WithIdentifier:name];
}

-(NSData *)requestBodyGeneratorWith:(NSArray *)contentDictionary
{
    NSString * string = nil;
    if ([NSJSONSerialization isValidJSONObject:contentDictionary]) {
        string = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:contentDictionary options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding];
    }
    // NSLog(@"%@",string);
    return [string dataUsingEncoding:NSUTF8StringEncoding];
    
}

-(void)Write:(NSData *)data toCacheFolder:(NSString *) folderName WithIdentifier:(NSString *)identifier
{
    NSString *folderPath = [self setCachePathInFolder:folderName];
    if(![[NSFileManager defaultManager] fileExistsAtPath:folderPath isDirectory:nil]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",identifier];
    fileName = [folderPath stringByAppendingPathComponent:fileName];
    [data writeToFile:fileName atomically:NO];
    
}

//-(NSString *)setCachePathInFolder:(NSString *)folderName
//{
//    NSString *applicationName = APP_TITLE;//[[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleDisplayName"];
//    NSString *cachePath = [NSString stringWithFormat:@"Library/Caches/%@/%@",applicationName,folderName];
//    return [NSHomeDirectory() stringByAppendingPathComponent:cachePath];
//}
//
//- (BOOL)checkComponentAtCacheFolder:(NSString *) folderName WithIdentifier:(NSString *)identifier {
//    BOOL isExist = NO;
//    NSString *folderPath = [self setCachePathInFolder:folderName];
//    if([[NSFileManager defaultManager] fileExistsAtPath:[folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt",identifier]] isDirectory:nil]) {
//        isExist = YES;
//    }
//    return isExist;
//}
//
//+(BOOL)validateEmail:(NSString*)string
//{
//    NSString *trimmedString = [string stringByTrimmingCharactersInSet:
//                               [NSCharacterSet whitespaceCharacterSet]];
//    
//    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    return [emailTest evaluateWithObject:trimmedString];
//}
//
//
//+(UIAlertController *)showAlert :(NSString *)appTitle message:(NSString *)message
//{
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:appTitle
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//    
////    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
////    [alert addAction:ok];
//    return alert;
//}
//
//+(void)showAlert:(NSString *)title withStatus:(NSString *)message delegate:(UIViewController *)controller tag:(NSString *)tagstr canel:(NSString *)cancelTitle
//{
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle:title
//                          message:message
//                          delegate:controller
//                          cancelButtonTitle:cancelTitle
//                          otherButtonTitles:@"OK", nil];
//    alert.tag=[tagstr integerValue];
//    [alert show];
//}
//
//+(BOOL)isReachable
//{
//    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
//    BOOL isReachable;
//    if (networkStatus == NotReachable)
//        isReachable=NO;
//    else
//            isReachable=YES;
//    return isReachable;
//}
//
//+ (void)setNavigationTitle:(NSString *)title withViewController:(UIViewController*)delegate
//{
//    UILabel *titleView = (UILabel *)delegate.navigationItem.titleView;
//    if (!titleView)
//    {
//        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
//        titleView.tag=1212;
//        titleView.backgroundColor = [UIColor clearColor];
//        titleView.font=[UIFont fontWithName:@"GothamMedium" size:15.0];
//        titleView.textColor = [UIColor redColor];
//        titleView.text=title;
//        [titleView sizeToFit];
//        delegate.navigationItem.titleView = titleView;
//    }
//}
//
//+(NSAttributedString *) customizeString :(NSString *) str range:(NSInteger )substrrange
//{
//    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:str];
//    UIFont *font=[UIFont fontWithName:@"GothamBook-Italic" size:13.0f];
//    [attString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, substrrange)];
//    
////    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
////    [style setLineSpacing:1.2f];
////    [attString addAttribute:NSParagraphStyleAttributeName
////                       value:style
////                       range:NSMakeRange(0, substrrange)];
//    
//    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, substrrange)];
//    
//    return attString;
//}
//
//+(NSAttributedString *) customizeStringWithFont :(NSString *) str range:(NSInteger )substrrange1 font:(UIFont *)font1
//{
//    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:str];
//    
//    [attString addAttribute:NSFontAttributeName value:font1 range:NSMakeRange(0, substrrange1)];
//    
//    //    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
//    //    [style setLineSpacing:1.2f];
//    //    [attString addAttribute:NSParagraphStyleAttributeName
//    //                       value:style
//    //                       range:NSMakeRange(0, substrrange)];
//    
//    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, substrrange1)];
//    
//    return attString;
//}
//
//+(void)postOnGoogle:(NSString *)text url:(NSURL *)shareUrl image:(UIImage *)shareImg controller:(UIViewController *)vwController
//{
//    
//   // [USER_DEFAULTS setObject:text forKey:@"google_post_text"];
//   // [USER_DEFAULTS synchronize];
//    
//    
//    if([[GPPSignIn sharedInstance] authentication] != nil)
//    {
//        
//        // Use the native share dialog in your app:
//        id<GPPShareBuilder> shareBuilder = [[GPPShare sharedInstance] shareDialog];
//        [GPPShare sharedInstance].delegate = (id)self;
//        
//        
//        // The share preview, which includes the title, description, and a thumbnail,
//        // is generated from the page at the specified URL location.
//        //[shareBuilder setURLToShare:[NSURL URLWithString:@"https://www.example.com/restaurant/sf/1234567/"]];
//        
//        [shareBuilder setPrefillText:text];
//        
//        // This line passes the string "rest=1234567" to your native application
//        // if somebody opens the link on a supported mobile device
//        //[shareBuilder setContentDeepLinkID:@"rest=1234567"];
//        
//        // This method creates a call-to-action button with the label "RSVP".
//        // - URL specifies where people will go if they click the button on a platform
//        // that doesn't support deep linking.
//        // - deepLinkID specifies the deep-link identifier that is passed to your native
//        // application on platforms that do support deep linking
//        /*[shareBuilder setCallToActionButtonWithLabel:@"RSVP"
//         URL:[NSURL URLWithString:@"https://www.example.com/reservation/4815162342/"]
//         deepLinkID:@"rsvp=4815162342"];*/
//        
//        [shareBuilder open];
//        
//    }
//    else
//    {
//        
//        GPPSignIn *signIn = [GPPSignIn sharedInstance];
//        // You previously set kClientId in the "Initialize the Google+ client" step
//        signIn.clientID = kClientId;
//        //---- Extra features to access other properties ----
//        signIn.shouldFetchGooglePlusUser = YES;
//        signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
//        signIn.shouldFetchGoogleUserID = YES;
//        //--------
//        // Uncomment one of these two statements for the scope you chose in the previous step
//        //signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
//        signIn.scopes = @[ @"profile" ];            // "profile" scope
//        // Optional: declare signIn.actions, see "app activities"
//        
//        signIn.delegate = (id)self;
//        
//        [[GPPSignIn sharedInstance] authenticate];
//    }
//    
//    
//}
//
//#pragma mark - GPPSignInDelegate
//
//+(void)finishedWithAuth:(GTMOAuth2Authentication *)auth
//                  error:(NSError *)error {
//    if (!error) {
//        
//        // Use the native share dialog in your app:
//        id<GPPShareBuilder> shareBuilder = [[GPPShare sharedInstance] shareDialog];
//        [GPPShare sharedInstance].delegate = (id)self;
//        
//        
//        // The share preview, which includes the title, description, and a thumbnail,
//        // is generated from the page at the specified URL location.
//        [shareBuilder setURLToShare:[NSURL URLWithString:@"http://cwikwin.ae/"]];
//        
//        [shareBuilder setPrefillText:@"google"];
//        
//        // This line passes the string "rest=1234567" to your native application
//        // if somebody opens the link on a supported mobile device
//        //[shareBuilder setContentDeepLinkID:@"rest=1234567"];
//        
//        // This method creates a call-to-action button with the label "RSVP".
//        // - URL specifies where people will go if they click the button on a platform
//        // that doesn't support deep linking.
//        // - deepLinkID specifies the deep-link identifier that is passed to your native
//        // application on platforms that do support deep linking
//        /*[shareBuilder setCallToActionButtonWithLabel:@"RSVP"
//         URL:[NSURL URLWithString:@"https://www.example.com/reservation/4815162342/"]
//         deepLinkID:@"rsvp=4815162342"];*/
//        
//        [shareBuilder open];
//        
//    }
//    else
//        
//    {
////        
////        UIAlertView *customAlertView = [[UIAlertView alloc] initWithTitle:app_name message:[NSString stringWithFormat:@"Status: Authentication error: %@", error] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
////        [customAlertView show];
//        
//        [NSString stringWithFormat:@"Status: Authentication error: %@", error];
//    }
//    
//}
//
//
//
//+(void)finishedSharingWithError:(NSError *)error {
//    NSString *text;
//    
//    if (!error) {
//        text = @"Message posted.";
//    } else if (error.code == kGPPErrorShareboxCanceled) {
//        text = @"Message cancelled.";
//    } else {
//        text = [NSString stringWithFormat:@"Error (%@)", [error localizedDescription]];
//    }
//    
//    UIAlertView *customAlertView = [[UIAlertView alloc] initWithTitle:APP_TITLE message:text delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [customAlertView show];
//    
//    //NSLog(@"Status: %@", text);
//}
//
//
//#pragma mark- bold segment control text
//+(void)changeSegmentFont:(UISegmentedControl *)segmentControl
//{
//    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                [UIFont fontWithName:@"GothamMedium" size:12], UITextAttributeFont,
//                                [UIColor colorWithRed:214/255.0 green:65/255.0 blue:51/255.0 alpha:1.0], UITextAttributeTextColor,
//                                nil];
//    [segmentControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
//    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
//    [segmentControl setTitleTextAttributes:highlightedAttributes forState:UIControlStateHighlighted];
//}
//
//
//+(void)saveBookId:(NSDictionary *)dict
//{
//    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"bookIDArray"]) {
//        
//        NSMutableArray *bookId=[[[NSUserDefaults standardUserDefaults] objectForKey:@"bookIDArray"]mutableCopy];
//        
//        [bookId addObject:dict];
//        
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setObject:bookId forKey:@"bookIDArray"];
//        [userDefaults synchronize];
//    }
//    else{
//        
//        NSMutableArray *bookId=[[NSMutableArray alloc]init];
//        [bookId addObject:dict];
//        
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setObject:bookId forKey:@"bookIDArray"];
//        [userDefaults synchronize];
//    }
//}
//
//+(BOOL) checkEmptyString :(id ) str
//{
//    if([str isKindOfClass:[NSString class]])
//    {
//        if([str length] >0 && ![str isEqualToString:@""] && ![str isEqualToString:@"<null>"] && ![str isEqualToString:@"(null)"] && ![str isKindOfClass:[NSNull class]])
//        {
//            return false;
//        }
//        else
//            return true;
//    }
//    
//    return true;
//}
//
//+(void)changeSearchBarTextColor:(UISearchBar *)searchBar
//{
//    for (UIView *subView in searchBar.subviews)
//    {
//        for (UIView *secondLevelSubview in subView.subviews){
//            if ([secondLevelSubview isKindOfClass:[UITextField class]])
//            {
//                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
//                //set font color here
//                searchBarTextField.textColor = [UIColor colorWithRed:238/255.0f green:238/255.0f blue:238/255.0f alpha:1.0f];
//                
//                searchBarTextField.leftView=nil;
//            }
//        }
//    }
//}
//
//+(void)changeNavBarColor:(UINavigationBar *)navBar
//{
//    [navBar setBarTintColor:[UIColor colorWithRed:(246/255.0) green:(221/255.0) blue:(190/255.0) alpha:1]];
//    //224 191 161
//}
//
//+(void)addNetworkErrorLabel:(NSString *)str view:(UIView *)view
//{
//    UILabel *network=[[UILabel alloc]init];
//    UIFont *font=[UIFont fontWithName:@"GothamMedium" size:15];
//    CGSize size = [str sizeWithAttributes:@{NSFontAttributeName:font}];
//    network.frame=CGRectMake(0, 0, size.width, size.height);
//    network.text=str;
//    network.center=view.center;
//    [view addSubview:network];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [network removeFromSuperview];
//    });
//    
//    //In a method called hideLabel
//}
//
//+(void)getPathForTerms_Consitions:(UIWebView *)webView
//{
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"mythatermsandconditions" ofType:@"html"];
//    NSURL *url = [NSURL fileURLWithPath:path];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    NSString *bodyStyleVertical = @"document.getElementsByTagName('body')[0].style.verticalAlign = 'middle';";
//    NSString *bodyStyleHorizontal = @"document.getElementsByTagName('body')[0].style.textAlign = 'center';";
//    NSString *mapStyle = @"document.getElementById('mapid').style.margin = 'auto';";
//    
//    [webView stringByEvaluatingJavaScriptFromString:bodyStyleVertical];
//    [webView stringByEvaluatingJavaScriptFromString:bodyStyleHorizontal];
//    [webView stringByEvaluatingJavaScriptFromString:mapStyle];
//    
//    [webView loadRequest:request];
//}
//
//+(BOOL)addReadMore:(UILabel *)label
//{
//    NSString *str=label.text;
//    CGRect rect=CGRectMake(51, 0, 257, 0);
//    CGSize size=[str sizeWithFont:label.font constrainedToSize:CGSizeMake(257, 3000) lineBreakMode:label.lineBreakMode];
//    int lines=(size.height/label.font.pointSize);
//    label.numberOfLines=lines;
//    rect.size=size;
//    if (lines>5) {
//        return YES;
//    }
//    else
//        return NO;
//
//}
//
//
//+(void)sendEmailSendGrip :(NSDictionary *)dict
//{
//    SendGrid *sendgrid = [SendGrid apiUser:KSendGripUname apiKey:KSendGripPassword];
//    /*
//     NSDictionary *message=[[NSDictionary alloc]initWithObjectsAndKeys:self.mailBody.text,@"html",self.subject.text,@"subject",@"shaila@mytha-tech.com",@"from_email",@"",@"from_name",to,@"to", nil];
//     */
//    [[AppDelegate sharedInstance]showProgressHud];
//    SendGridEmail *email = [[SendGridEmail alloc] init];
//    email.to = [dict objectForKey:@"emailTo"];//@"info@mytha-tech.com";
//    email.from = [dict objectForKey:@"emailFrom"];
//    //@"shaila@mytha-tech.com";
//    email.subject = [dict objectForKey:@"subject"];//self.subject.text;
//    email.text = [dict objectForKey:@"body"];//self.mailBody.text;
//    
//    [sendgrid sendWithWeb:email successBlock:^(id responseObject) {
//        [[AppDelegate sharedInstance]dismissProgressHud];
//        [CommonMethods showAlert:APP_TITLE withStatus:[NSString stringWithFormat:@"Mail sent successfully"] delegate:nil tag:nil canel:nil];
//        
//    } failureBlock:^(NSError *error) {
//        [[AppDelegate sharedInstance]dismissProgressHud];
//        [[AppDelegate sharedInstance]showProgressHudPopUp:error.localizedDescription];
//    }];
//}
//#pragma mark MediaPlayer
+(float) getVolumeLevel
{
        float volume = [MPMusicPlayerController applicationMusicPlayer].volume;
//    MPVolumeView *slide = [MPVolumeView new];
//    UISlider *volumeViewSlider;
//    
//    for (UIView *view in [slide subviews]){
//        if ([[[view class] description] isEqualToString:@"MPVolumeSlider"]) {
//            volumeViewSlider = (UISlider *) view;
//        }
//    }
    
    //    float val = [volumeViewSlider value];
    //    return (float)(val * (float)AVHELPER_MAX_VOLUME);
    
    //float val = [volumeViewSlider value];
    
    return volume;
    
}
+(void) setVolumeLevel:(float)val
{
    //    float volume = [MPMusicPlayerController applicationMusicPlayer].volume;
    MPVolumeView *slide = [MPVolumeView new];
    UISlider *volumeViewSlider;
    
    for (UIView *view in [slide subviews]){
        if ([[[view class] description] isEqualToString:@"MPVolumeSlider"]) {
            volumeViewSlider = (UISlider *) view;
        }
    }
    //    float vol = (float)val / (float)AVHELPER_MAX_VOLUME;
    //    volumeViewSlider.value=vol;
    
    volumeViewSlider.value=val;
    
}

@end
