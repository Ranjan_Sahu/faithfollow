//
//  CommonMethods.h
//  Mytha
//
//  Created by webwerks on 07/10/15.
//  Copyright © 2015 webwerks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <Accounts/ACAccountStore.h>
#import <Accounts/ACAccount.h>
//#import <GoogleOpenSource/GoogleOpenSource.h>
//#import <GooglePlus/GooglePlus.h>
//#import <Google/GoogleOpenSource.framework>
//#import <GooglePlus/GooglePlus.h>
//#import <GoogleSignIn/GoogleSignIn.h>
//#import <SendGrid/SendGrid.h>
//#import <SendGrid/SendGridEmail.h>


@interface CommonMethods : NSObject


-(void)saveDataInFolder:(NSString *)folder withName:(NSString *)name forData:(NSArray *)dataDictionary;
-(NSData *)requestBodyGeneratorWith:(NSArray *)contentDictionary;

-(void)Write:(NSData *)data toCacheFolder:(NSString *) folderName WithIdentifier:(NSString *)identifier;
-(NSString *)setCachePathInFolder:(NSString *)folderName;

- (BOOL)checkComponentAtCacheFolder:(NSString *) folderName WithIdentifier:(NSString *)identifier;
+(BOOL)validateEmail:(NSString*)string;
+(UIAlertController *)showAlert :(NSString *)appTitle message:(NSString *)message;
+(BOOL)isReachable;

+(void)showAlert:(NSString *)title withStatus:(NSString *)message delegate:(UIViewController *)controller tag:(NSString *)tagstr canel:(NSString *)cancelTitle;

+ (void)setNavigationTitle:(NSString *)title withViewController:(UIViewController*)delegate;
+(NSAttributedString *) customizeString :(NSString *) str range:(NSInteger )substrrange
;
+(void )postOnGoogle:(NSString *)text url:(NSURL *)shareUrl image:(UIImage *)shareImg controller:(UIViewController *)vwController;

+(void)changeSegmentFont:(UISegmentedControl *)segmentControl;

+(void)saveBookId:(NSDictionary *)dict;
+(BOOL) checkEmptyString :(id ) str;
+(void)changeSearchBarTextColor:(UISearchBar *)searchBar;
+(void)changeNavBarColor:(UINavigationBar *)navBar;
+(void)addNetworkErrorLabel:(NSString *)str view:(UIView *)view;
+(void)getPathForTerms_Consitions:(UIWebView *)webView;
+(NSAttributedString *) customizeStringWithFont :(NSString *) str range:(NSInteger )substrrange1 font:(UIFont *)font1;
+(BOOL)addReadMore:(UILabel *)label;

+(void)sendEmailSendGrip :(NSDictionary *)dict;
+(float) getVolumeLevel;
+(void) setVolumeLevel:(float)val;

@end
