//
//  TutorialViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 16/09/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "TutorialViewController.h"
#import "TutorialCollectionViewCell.h"
#import "CustomPagerViewController.h"
#import "InviteFriends.h"
#import "CongratulationViewController.h"
#import "myinfluencerCell.h"
#import "SecondCollectionViewCell.h"
#import "InfluencerProfileController.h"
#import "Mixpanel.h"
#import "TutorialACollectionViewCell.h"
#import "TutorialBCollectionViewCell.h"

@interface TutorialViewController ()
{
    NSArray *imgArray,*inluencerListTutorailArray,*messageArray;
    long currentPage;
}
@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    imgArray = [[NSArray alloc]initWithObjects:@"mobile_tutorial_1.png",@"mobile_tutorial_2.png",@"mobile_tutorial_3.png", nil];
    inluencerListTutorailArray=[[NSArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"TutorialACollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TutorialACollectionViewCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"TutorialBCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TutorialBCollectionViewCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"SecondCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SecondCollectionViewCell"];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self influencerTutorialMethod];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(influTutorialViewMethod:)
                                                 name:@"influTutorialView"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(TutorialDoneButtonMethod:)
                                                 name:@"TutorialDoneButton"
                                               object:nil];
    
    
    
    [self.navigationController setNavigationBarHidden:YES];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    currentPage=1;
    NSString *currentDate=[Constant1 createTimeStamp];
    [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Last Page Load": currentDate}];
    [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Last Page Load": currentDate}];
    [[AppDelegate sharedAppDelegate].mxPanel track:@"Page loaded"
                                        properties:@{ @"Page name":@"Tutorial Screen 1",@"Loaded Time":currentDate,@"Last Page Load": currentDate, }];

}

-(void)TutorialDoneButtonMethod:(NSNotification *) notification{
    
    [TenjinSDK sendEventWithName:@"EVENT_NAME_COMPLETED_TUTORIAL"];
    [FBSDKAppEvents logEvent:@"EVENT_NAME_COMPLETED_TUTORIAL"];
    UACustomEvent *event = [UACustomEvent eventWithName:@"EVENT_NAME_COMPLETED_TUTORIAL"];
    [[UAirship shared].analytics addEvent:event];
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate setRoots];
    });
    
}

#pragma mark influTutorialViewMethod

-(void)influTutorialViewMethod:(NSNotification *) notification{
    NSDictionary* userInfo = notification.userInfo;
    dispatch_async(dispatch_get_main_queue(), ^{
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        InfluencerProfileController *InfluencerProfileControllerView=[storyboard instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
        NSMutableDictionary *influencerDictData=[userInfo valueForKey:@"allDataTutorail"];
        InfluencerProfileControllerView.influencerDict=influencerDictData;
//        [self presentViewController:InfluencerProfileControllerView animated:YES completion:nil];
        [self.navigationController pushViewController:InfluencerProfileControllerView animated:YES];

    });
    
}

#pragma mark influencerTutorialMethod

-(void)influencerTutorialMethod{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        
        NSDictionary *dict=nil;
        
        dict = @{
                 @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:influencerList withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            inluencerListTutorailArray=[response valueForKey:@"data"];
                            [self.collectionView reloadData];
                            
                        });
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([messageArray count]>0) {
                                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
//             NSLog(@"error");
             
         }];
        
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Collectionview delegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [imgArray count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        TutorialACollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TutorialACollectionViewCell" forIndexPath:indexPath];
                
        cell.btnNext.tag = indexPath.row;
        [cell.btnNext addTarget:self action:@selector(onClickNext:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    else if (indexPath.row == 1) {
        TutorialBCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TutorialBCollectionViewCell" forIndexPath:indexPath];
        
        cell.btnNext.tag = indexPath.row;
        [cell.btnNext addTarget:self action:@selector(onClickNext:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    else {
        SecondCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SecondCollectionViewCell" forIndexPath:indexPath];
        cell.pagecontroll.currentPage =3;
        cell.pagecontroll.pageIndicatorTintColor = [UIColor grayColor];
        cell.pagecontroll.currentPageIndicatorTintColor = [UIColor blackColor];
        [cell setData:inluencerListTutorailArray];
        
        return cell;
    }
    
    //    TutorialCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TutorialCollectionViewCell" forIndexPath:indexPath];

//    cell.btnNext.hidden = NO;
//    if (indexPath.row == 0)
//    {
//        cell.imgTutorial.image = [UIImage imageNamed:[imgArray objectAtIndex:0]];
//        cell.btnNext.tag = indexPath.row;
//        cell.pagecount.currentPage =0;
//        cell.pagecount.pageIndicatorTintColor = [UIColor darkGrayColor];
//        cell.pagecount.currentPageIndicatorTintColor = [UIColor blackColor];
//        cell.btnInviteFriends.hidden = YES;
//        cell.btnSkip.hidden = YES;
//        [cell.btnNext addTarget:self action:@selector(onClickNext:) forControlEvents:UIControlEventTouchUpInside];
//
//    }
    
//    if (indexPath.row == 1) {
    
//        cell.imgTutorial.image = [UIImage imageNamed:[imgArray objectAtIndex:1]];
//        cell.btnNext.tag = indexPath.row;
//        cell.pagecount.currentPage =1;
//        cell.pagecount.pageIndicatorTintColor = [UIColor grayColor];
//        cell.pagecount.currentPageIndicatorTintColor = [UIColor blackColor];
//        cell.btnInviteFriends.hidden = YES;
//        cell.btnSkip.hidden = YES;
//        [cell.btnNext addTarget:self action:@selector(onClickNext:) forControlEvents:UIControlEventTouchUpInside];
//    }
//
//    if (indexPath.row == 2)
//    {
//        SecondCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SecondCollectionViewCell" forIndexPath:indexPath];
//        cell.pagecontroll.currentPage =3;
//        cell.pagecontroll.pageIndicatorTintColor = [UIColor grayColor];
//        cell.pagecontroll.currentPageIndicatorTintColor = [UIColor blackColor];
//        [cell setData:inluencerListTutorailArray];
//
//        return cell;
//    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //return CGSizeMake([UIScreen mainScreen].bounds.size.width , [UIScreen mainScreen].bounds.size.height);
    return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGRect visibleRect = (CGRect){.origin = self.collectionView.contentOffset, .size = self.collectionView.bounds.size};
    CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
    NSIndexPath *visibleIndexPath = [self.collectionView indexPathForItemAtPoint:visiblePoint];
    NSString *indexpath=[NSString stringWithFormat:@"%ld",visibleIndexPath.row];
    long tempPage=visibleIndexPath.row+1;
    NSString *currentDate=[Constant1 createTimeStamp];
    if (currentPage!=tempPage) {
        NSLog(@"currentPage %ld temp page %ld ",currentPage,tempPage);
        currentPage=tempPage;
        NSString *tutorialScreen=[NSString stringWithFormat:@"Tutorial Screen %d",[indexpath intValue]+1];
        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Last Page Load": currentDate}];
        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Last Page Load": currentDate}];
        [[AppDelegate sharedAppDelegate].mxPanel track:@"Page loaded"
                                            properties:@{ @"Page name":tutorialScreen,@"Loaded Time":currentDate,@"Last Page Load": currentDate, }];
    }
}


#pragma mark - Button Action

-(void)onClickNext:(UIButton *)btn
{
    NSLog(@"button tag %ld",btn.tag);
    
    NSString *currentDate=[Constant1 createTimeStamp];
    if (btn.tag == 0)
    {
        
        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Last Page Load": currentDate}];
        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Last Page Load": currentDate}];
        [[AppDelegate sharedAppDelegate].mxPanel track:@"Page loaded"
                                            properties:@{ @"Page name":@"Tutorial Screen 2",@"Loaded Time":currentDate,@"Last Page Load": currentDate, }];
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
        
        
    }
    if (btn.tag == 1) {
        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Last Page Load": currentDate}];
        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Last Page Load": currentDate}];
        [[AppDelegate sharedAppDelegate].mxPanel track:@"Page loaded"
                                            properties:@{ @"Page name":@"Tutorial Screen 3",@"Loaded Time":currentDate,@"Last Page Load": currentDate, }];
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
    }
    
    if (btn.tag==2) {
        [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Last Page Load": currentDate}];
        [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Last Page Load": currentDate}];
        [[AppDelegate sharedAppDelegate].mxPanel track:@"Page loaded"
                                            properties:@{ @"Page name":@"Tutorial Screen 3",@"Loaded Time":currentDate,@"Last Page Load": currentDate, }];
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
    }
    
}



-(void)retriveUserData
{
    NSMutableArray *SelectedArray = [[NSMutableArray alloc]init];
    NSArray *array=[[NSArray alloc]initWithObjects:@"picture.width(960).height(1704)" ,@"email", @"first_name", @"last_name" ,@"gender",@"about",@"birthday", @"bio", @"verified",@"age_range", nil];
    
    [SelectedArray addObjectsFromArray:array];
    
    NSMutableString *strFinal=[[NSMutableString alloc]initWithString:@"id"];
    
    for (NSString *str in SelectedArray)
    {
        strFinal = [NSMutableString stringWithFormat:@"%@,%@",strFinal,str];
    }
    if ([FBSDKAccessToken currentAccessToken])
    {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": strFinal}]
         
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             if (!error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate sharedAppDelegate] hidehud];
                     [self getFacebookProfileInfo:result];
                 });
             }
             else
             {
                 [[AppDelegate sharedAppDelegate] hidehud];
                 [self.view makeToast:@"Seems there is some problem ,please try after some time "  duration:2.0 position:CSToastPositionCenter];
                 
                 //                 NSLog(@"error %@",error.description);
             }
         }];
    }
}



-(void)onClickSkip:(UIButton *)btn
{
    [TenjinSDK sendEventWithName:@"EVENT_NAME_COMPLETED_TUTORIAL"];
    [FBSDKAppEvents logEvent:@"EVENT_NAME_COMPLETED_TUTORIAL"];
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"EVENT_NAME_COMPLETED_TUTORIAL"];

    UACustomEvent *event = [UACustomEvent eventWithName:@"EVENT_NAME_COMPLETED_TUTORIAL"];
    [[UAirship shared].analytics addEvent:event];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate setRoots];
        
    });
    
    
}

#pragma mark onClickInviteFriendsMethod

-(void)onClickInviteFriends:(UIButton *)btn
{
    [TenjinSDK sendEventWithName:@"click_invite_friends_from_contacts"];
    [FBSDKAppEvents logEvent:@"click_invite_friends_from_contacts"];
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"click_invite_friends_from_contacts"];
    [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"click_invite_friends_from_contacts"]];
    UIStoryboard* storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    InviteFriends *invite=[storyboard instantiateViewControllerWithIdentifier:@"InviteFriends"];
    [self.navigationController pushViewController:invite animated:NO];
}

-(void)getFacebookProfileInfo:(NSDictionary *)dict
{
    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"fbDictionay"];
    [[NSUserDefaults standardUserDefaults] setObject:@"fbvalue" forKey:@"fbvalue"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    NSLog(@"dict %@",dict);
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self.view makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *firstname=[dict valueForKey:@"first_name"];
        if ([firstname isEqualToString:@""]) {
            firstname=@"";
        }
        else{
            firstname=[dict valueForKey:@"first_name"];
        }
        
        
        NSString *last_name=[dict valueForKey:@"last_name"];
        if ([last_name isEqualToString:@""]) {
            last_name=@"";
        }
        else{
            last_name=[dict valueForKey:@"last_name"];
        }
        
        NSString *emailid;
        if ([dict objectForKey:@"email"] && ![[dict objectForKey:@"email"] isKindOfClass:[NSNull class]])
        {
            emailid=[dict valueForKey:@"email"];
        }
        else
        {
            emailid = @"";
        }
        
        NSString *dob;
        if ([dict objectForKey:@"birthday"] && ![[dict objectForKey:@"birthday"] isKindOfClass:[NSNull class]])
        {
            dob=[dict valueForKey:@"birthday"];
        }
        else
        {
            dob = @"";
        }
        
        NSString *gendercheck;
        NSString *gender=[dict valueForKey:@"gender"];
        if ([gender isEqualToString:@""]) {
            gender=@"";
        }
        else{
            gender=[[dict valueForKey:@"gender"] capitalizedString];
            if ([gender isEqualToString:@"Male"]) {
                gendercheck=@"1";
            }
            else{
                gendercheck=@"0";
            }
            
            
        }
        
        NSString *facebookId=[dict valueForKey:@"id"];
        
        if ([facebookId isEqualToString:@""]) {
            facebookId=@"";
        }
        else{
            facebookId=[dict valueForKey:@"id"];
        }
        NSDictionary *profile_pic=[dict valueForKey:@"picture"];
        NSString *imageString;
        UIImage *img;
        if([profile_pic isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dataDict=[profile_pic valueForKey:@"data"];
            if ([dataDict isKindOfClass:[NSDictionary class]]) {
                NSString  *imageprofileURL=[dataDict valueForKey:@"url"];
                NSURL *tempURL = [NSURL URLWithString:imageprofileURL];
                NSData *tempData = [NSData dataWithContentsOfURL:tempURL];
                img =[[UIImage alloc] initWithData:tempData];
                if (img!=nil) {
                    img=[Constant1 compressImage:img];
                    imageString=[self base64String:img];
                }
                else{
                    imageString=@"";
                }
            }
        }
        
        
        if ([firstname isEqualToString:@""]||[last_name isEqualToString:@""]||[emailid isEqualToString:@""]|| [dob isEqualToString:@""]||[gender isEqualToString:@""]) {
            NSDictionary *dictparameter;
            dictparameter = @{
                              @"first_name" :firstname,
                              @"last_name" :last_name,
                              @"email" :emailid,
                              @"date_of_birth" :dob,
                              @"gender":gendercheck,
                              @"image":imageString,
                              @"facebookId":facebookId,
                              @"password":@"",
                              };
            
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:incompleteFbData withParameters:dictparameter withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSString *status=[NSString stringWithFormat:@"%@",[response valueForKey:@"status"]];
                        if ([status isEqualToString:@"0"]) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [TenjinSDK sendEventWithName:@"signup_with_facebook"];
                                [FBSDKAppEvents logEvent:@"signup_with_facebook"];
//                                Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                                [mixpanel track:@"signup_with_facebook"];

                                [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"signup_with_facebook"]];
                                
                                UIStoryboard * storyboard =[UIStoryboard storyboardWithName:@"Main" bundle: nil];
                                
                                CustomPagerViewController *signup=[storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
                                [self.navigationController pushViewController:signup animated:YES];
                            });
                            
                            
                        }
                        else{
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"api_token"] forKey:@"api_token"];
                                [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"id"] forKey:@"userid"];
                                [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                [TenjinSDK sendEventWithName:@"connect_with_facebook"];
//                                Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                                [mixpanel track:@"connect_with_facebook"];
                                [FBSDKAppEvents logEvent:@"connect_with_facebook"];
                                [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"connect_with_facebook"]];
                                [appDelegate setRoots];
                                
                            });
                            
                            
                            
                        }
                        
                    });
                    
                }
                else{
                    [self.view makeToast:@"Seems there is some problem,Please try after some time "  duration:1.0 position:CSToastPositionCenter];
                    
                }
            } errorBlock:^(id error)
             {
//                 NSLog(@"service error");
             }];
            
        }
        else{
            NSDictionary *dictparameter;
            dictparameter = @{
                              @"first_name" :firstname,
                              @"last_name" :last_name,
                              @"email" :emailid,
                              @"date_of_birth" :dob,
                              @"gender":gendercheck,
                              @"image":imageString,
                              @"facebookId":facebookId,
                              @"password":@"",
                              };
            
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:signupfb withParameters:dictparameter withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"api_token"] forKey:@"api_token"];
                        [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"id"] forKey:@"userid"];
                        [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"Login"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        NSString *is_registersign=[NSString stringWithFormat:@"%@",[response valueForKey:@"is_register"]];
                        
                        if ([is_registersign isEqualToString:@"1"]) {
                            [TenjinSDK sendEventWithName:@"signup_with_facebook"];
                            [FBSDKAppEvents logEvent:@"signup_with_facebook"];
                            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"signup_with_facebook"]];
                            CongratulationViewController *welcomeView=[self.storyboard instantiateViewControllerWithIdentifier:@"CongratulationViewController"];
                            [self.navigationController pushViewController:welcomeView animated:YES];
                            
                        }
                        else{
                            [TenjinSDK sendEventWithName:@"connect_with_facebook"];
                            [FBSDKAppEvents logEvent:@"connect_with_facebook"];
                            [[UAirship shared].analytics addEvent:[UACustomEvent eventWithName:@"connect_with_facebook"]];
                            [appDelegate setRoots];
                            
                        }
                        
                    });
                }
                else{
                    [self.view makeToast:@"Seems there is some problem,Please try after some time "  duration:1.0 position:CSToastPositionCenter];
                    
                }
            } errorBlock:^(NSError *error)
             {
//                 NSLog(@"service error %@",error.description);
             }];
            
        }
        
    }
    
}

-(NSString*)base64String:(UIImage*)image{
    NSString *base64EncodeStr = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    base64EncodeStr = [base64EncodeStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    return base64EncodeStr;
}



@end
