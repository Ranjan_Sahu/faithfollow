//
//  SecondCollectionViewCell.h
//  wireFrameSplash
//
//  Created by webwerks on 12/1/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic,strong)NSArray *influencelistArray;
@property(nonatomic,strong)NSArray *messageArray;
@property(nonatomic,strong)NSMutableDictionary *influencerdataDict;
-(IBAction)DoneMethod:(id)sender;
-(void)setData:(NSArray *)array;
@property (weak, nonatomic) IBOutlet UIPageControl *pagecontroll;

@end
