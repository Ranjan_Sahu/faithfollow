//
//  SecondCollectionViewCell.m
//  wireFrameSplash
//
//  Created by webwerks on 12/1/16.
//  Copyright © 2016 home. All rights reserved.
//

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#import "SecondCollectionViewCell.h"
#import "myinfluencerCell.h"
#import "UIImageView+WebCache.h"
#import "InfluencerProfileController.h"


@implementation SecondCollectionViewCell
@synthesize messageArray;
- (void)awakeFromNib {
    [super awakeFromNib];
    [self.tableview registerNib:[UINib nibWithNibName:@"myinfluencerCell" bundle:nil] forCellReuseIdentifier:@"myinfluencerCell"];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    [self.tableview setContentInset:(UIEdgeInsetsMake(0, 0, 0, 0))];
    self.tableview.estimatedRowHeight = 10.0f;
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    [self.tableview layoutIfNeeded];
    [self.tableview setSeparatorStyle:(UITableViewCellSeparatorStyleNone)];
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 100)];
    self.tableview.tableFooterView = footerView;
    
    // Initialization code
}

-(void)setData:(NSArray *)array{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.influencelistArray=array;
        [self.tableview reloadData];
    });
}

-(IBAction)DoneMethod:(id)sender{
    NSString *currentDate=[Constant1 getLoginCurrentDateTime];
    [[NSUserDefaults standardUserDefaults] setObject:currentDate forKey:@"startDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"TutorialDoneButton" object:nil userInfo:nil];
}

#pragma mark TableView Data Source methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.influencelistArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    myinfluencerCell *cell = (myinfluencerCell *)[self.tableview dequeueReusableCellWithIdentifier:@"myinfluencerCell"                                                                    forIndexPath:indexPath];
    
    NSDictionary *DictData=[self.influencelistArray objectAtIndex:indexPath.row];
    if ([DictData isKindOfClass:[NSDictionary class]]) {
        cell.profile_name.text=[NSString stringWithFormat:@"%@ %@",[DictData valueForKey:@"first_name"],[DictData valueForKey:@"last_name"]];
        NSString *followcountDisplaycheck=[NSString stringWithFormat:@"%@",[DictData valueForKey:@"display_followers_count"]];
        NSString *followcountcheck=[NSString stringWithFormat:@"%@",[DictData valueForKey:@"followers"]];
        
        cell.seperatorView.hidden = YES;
        if ([followcountDisplaycheck intValue]>100){
            cell.countFollowers.hidden=NO;
            cell.countFollowers.text=followcountcheck;
        }else{
            cell.countFollowers.hidden=YES;
        }
        
        NSString *frindStatus;
        frindStatus=[DictData valueForKey:@"friend_status"];
        cell.followBTN.tag=indexPath.row;
        
        if ([frindStatus isEqualToString:@"Follow"]) {
            [cell.followBTN setTitle:@"Follow" forState:UIControlStateNormal];
            //[cell.followBTN setBackgroundColor:UIColorFromRGB(0x3C95C2)];
            [cell.followBTN setBackgroundColor:[UIColor whiteColor]];
            cell.followBTN.layer.borderWidth  = 1.0f;
            cell.followBTN.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f].CGColor;
            [cell.followBTN setTitleColor:[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f] forState:UIControlStateNormal];
            [cell.followBTN addTarget:self action:@selector(followInfluecerTutorial:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([frindStatus isEqualToString:@"Unfollow"]) {
            [cell.followBTN setTitle:@"Unfollow" forState:UIControlStateNormal];
            [cell.followBTN setBackgroundColor:[UIColor orangeColor]];
            [cell.followBTN setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            cell.followBTN.layer.borderWidth  = 1.0f;
            cell.followBTN.layer.borderColor = [UIColor orangeColor].CGColor;
            [cell.followBTN addTarget:self action:@selector(followInfluecerTutorial:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([frindStatus isEqualToString:@"See their post"]) {
            //[cell.followBTN setBackgroundColor:UIColorFromRGB(0x0080FF)];
            [cell.followBTN setBackgroundColor:[UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f]];
            cell.followBTN.layer.borderWidth  = 1.0f;
            //cell.followBTN.layer.borderColor = UIColorFromRGB(0x0080FF).CGColor;
            cell.followBTN.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:186.0/255.0f  blue:245.0/255.0f  alpha:1.0f].CGColor;
            [cell.followBTN setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            cell.followBTN.alpha=0.9;
            [cell.followBTN setTitle:@"See their post" forState:UIControlStateNormal];
            [cell.followBTN addTarget:self action:@selector(seetheirpost:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        //        NSDictionary *userDictData=[DictData valueForKey:@"usersprofile"];
        if ([DictData isKindOfClass:[NSDictionary class]]) {
            cell.contentLBL.text=[DictData valueForKey:@"subtext"];
            NSString *strHtml = [DictData valueForKey:@"tags"];
            strHtml = [strHtml stringByReplacingOccurrencesOfString:@", "
                                                         withString:[NSString stringWithFormat:@" %@ ",@"\u2022"]];
            //cell.taghtmllbl.textColor = [UIColor darkGrayColor];
            cell.taghtmllbl.text=strHtml;
            NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[DictData valueForKey:@"profile_picture"]];
            NSURL *imageURL = [NSURL URLWithString:str];
            [cell.imageView_profile sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
        }
        
        // add tap geture to show profile of user
        cell.imageView_profile.tag=indexPath.row;
        cell.imageView_profile.userInteractionEnabled=YES;
        UITapGestureRecognizer *imagegallery_open = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageviewGesture:)];
        [cell.imageView_profile addGestureRecognizer:imagegallery_open];
        
        //add tapgesture to show profile
        cell.profile_name.tag = indexPath.row;
        cell.profile_name.userInteractionEnabled=YES;
        UITapGestureRecognizer *profilename = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilenamegesture:)];
        [cell.profile_name addGestureRecognizer:profilename];
        [cell setNeedsUpdateConstraints];
        [cell updateConstraintsIfNeeded];
        [cell layoutIfNeeded];
        return cell;
        
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}



// taped image and profile name to  show profile of user

- (void)imageviewGesture:(UITapGestureRecognizer*)sender {
    NSMutableDictionary *arrayProfileDict=[self.influencelistArray objectAtIndex:sender.view.tag];
    NSString *frindID;
    if ([arrayProfileDict isKindOfClass:[NSDictionary class]]) {
        frindID=[arrayProfileDict valueForKey:@"id"];
        [self viewFrndProfile:frindID];
    }
}

- (void)profilenamegesture:(UITapGestureRecognizer*)sender {
    NSMutableDictionary *arrayProfileDict=[self.influencelistArray objectAtIndex:sender.view.tag];
    NSString *frindID;
    if ([arrayProfileDict isKindOfClass:[NSDictionary class]]) {
        frindID=[arrayProfileDict valueForKey:@"id"];
        [self viewFrndProfile:frindID];
    }
}




-(void)viewFrndProfile :(NSString*)friend_id{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSDictionary *dict = @{
                               @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                               @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                               @"friend_id" : friend_id,
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if([response[@"status"]intValue ] ==1){
                    self.influencerdataDict=[[NSMutableDictionary alloc]init];
                    self.influencerdataDict=[response valueForKey:@"data"];
                    if ([self.influencerdataDict isKindOfClass:[NSMutableDictionary class]]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSDictionary *userInfogroup = @{
                                                            @"allDataTutorail" : self.influencerdataDict,
                                                            };
                            [[NSNotificationCenter defaultCenter] postNotificationName: @"influTutorialView" object:nil userInfo:userInfogroup];
                            
                        });
                        
                    }
                }
                else
                {
                    messageArray=[response valueForKey:@"message"];
                    if ([messageArray count]>0) {
                        [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                        return;
                        
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
             //             NSLog(@"error");
             
         }];
    }
    
}


-(void)seetheirpost:(UIButton*)sender{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }
    else{
        NSString *frind_id;
        CGPoint center= sender.center;
        CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableview];
        NSIndexPath *indexPathvalue = [self.tableview indexPathForRowAtPoint:rootViewPoint];
        NSMutableDictionary *arrayDict=[self.influencelistArray objectAtIndex:indexPathvalue.row];
        if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
            NSMutableDictionary *usersprofile=arrayDict[@"usersprofile"];
            frind_id=[usersprofile valueForKey:@"user_id"];
        }
        NSDictionary *dict=nil;
        //        NSLog(@"The button title is %@",sender.titleLabel.text);
        dict = @{
                 @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                 @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                 
                 };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:singleUserPost withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSMutableDictionary *arrayDictInfluencer=[self.influencelistArray objectAtIndex:indexPathvalue.row];
                            NSDictionary *userInfogroup = @{
                                                            @"allDataTutorail" : arrayDictInfluencer,
                                                            };
                            [[NSNotificationCenter defaultCenter] postNotificationName: @"influTutorialView" object:nil userInfo:userInfogroup];
                        });
                        
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableview reloadData];
                            if ([messageArray count]>0) {
                                [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
             //             NSLog(@"error");
             
         }];
        
    }
}

-(void)followInfluecerTutorial:(UIButton*)sender{
    
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        [self makeToast:NoNetwork  duration:1.0 position:CSToastPositionCenter];
        return;
    }else{
        NSString *frind_id,*influecer_Tutorail;
        CGPoint center= sender.center;
        CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableview];
        NSIndexPath *indexPathvalue = [self.tableview indexPathForRowAtPoint:rootViewPoint];
        NSMutableDictionary *arrayDict=[self.influencelistArray objectAtIndex:indexPathvalue.row];
        if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
            frind_id=[arrayDict valueForKey:@"id"];
            influecer_Tutorail=[NSString stringWithFormat:@"%@ %@",[arrayDict valueForKey:@"first_name"],[arrayDict valueForKey:@"last_name"]];
        }
        NSDictionary *dict=nil;
        
        NSString *methodname=nil;
        //        NSLog(@"The button title is %@",sender.titleLabel.text);
        UIButton *btn = (UIButton*)sender;
        
        if ([btn.titleLabel.text isEqualToString:@"Unfollow"]) {
            dict = @{
                     @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                     @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                     @"friend_id" : frind_id,
                     };
            methodname=unfollow;
        }
        else if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
            
            dict = @{
                     @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                     @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                     @"friend_id" : frind_id,
                     };
            methodname=follow;
        }
        else{
            dict = @{
                     @"id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                     @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                     };
            methodname=singleUserPost;
            
        }
        
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodname withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            if ([btn.titleLabel.text isEqualToString:@"Follow"]) {
                                
                                // Follow mixpanel
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Number of follows" by:@1];
                                
                                NSString *currentDate=[Constant1 createTimeStamp];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel registerSuperProperties:@{@"Date of last follow":currentDate}];
                                
                                [[AppDelegate sharedAppDelegate].mxPanel.people set:@{@"Date of last follow":currentDate}];
                                
                                [ [AppDelegate sharedAppDelegate].mxPanel track:@"Follow"
                                                                     properties:@{ @"Influencer?":[NSNumber numberWithBool:true],@"Date of last follow":currentDate,@"Following":influecer_Tutorail}];
                                
                                NSMutableDictionary *arrayDict=[self.influencelistArray objectAtIndex:indexPathvalue.row];
                                if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
                                    [arrayDict setObject:@"See their post" forKey:@"friend_status"];
                                }
                                [self reloadCellCom:indexPathvalue.row];
                                
                            }
                            else if([btn.titleLabel.text isEqualToString:@"Unfollow"])
                            {
                                [[AppDelegate sharedAppDelegate].mxPanel.people increment:@"Number of follows" by:@-1];
                                NSMutableDictionary *arrayDict=[self.influencelistArray objectAtIndex:indexPathvalue.row];
                                if ([arrayDict isKindOfClass:[NSMutableDictionary class]]) {
                                    [arrayDict setObject:@"Follow" forKey:@"friend_status"];
                                }
                                [self reloadCellCom:indexPathvalue.row];
                            }
                            else{
                                NSMutableDictionary *arrayDictInfluencer=[self.influencelistArray objectAtIndex:indexPathvalue.row];
                                NSDictionary *userInfogroup = @{
                                                                @"allDataTutorail" : arrayDictInfluencer,
                                                                };
                                [[NSNotificationCenter defaultCenter] postNotificationName: @"influTutorialView" object:nil userInfo:userInfogroup];
                            }
                            
                        });
                        
                    }else{
                        messageArray=[response valueForKey:@"message"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableview reloadData];
                            if ([messageArray count]>0) {
                                [self makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
                            }
                            
                            
                        });
                    }
                    
                }
                
            }
        } errorBlock:^(id error)
         {
             //             NSLog(@"error");
         }];
        
    }
    
}

-(void)reloadCellCom:(NSUInteger)cell_index{
    
    [self.tableview beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cell_index inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableview reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [self.tableview endUpdates];
    
}


@end
