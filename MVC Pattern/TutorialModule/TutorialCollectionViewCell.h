//
//  TutorialCollectionViewCell.h
//  wireFrameSplash
//
//  Created by webwerks on 16/09/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIImageView *imgTutorial;

@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteFriends;
@property (weak, nonatomic) IBOutlet UIPageControl *pagecount;

@end
