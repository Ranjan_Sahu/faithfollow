//
//  TutorialACollectionViewCell.h
//  wireFrameSplash
//
//  Created by webwerks on 11/15/18.
//  Copyright © 2018 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialACollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@end
