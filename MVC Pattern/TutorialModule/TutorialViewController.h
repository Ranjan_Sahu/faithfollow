//
//  TutorialViewController.h
//  wireFrameSplash
//
//  Created by webwerks on 16/09/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(assign,nonatomic)BOOL isFromFacebook;



@end
