//
//  MLKMenuPopover.h
//  MLKMenuPopover
//
//  Created by NagaMalleswar on 20/11/14.
//  Copyright (c) 2014 NagaMalleswar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MLKMenuPopover;

@protocol MLKMenuPopoverDelegate<NSObject>

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex;
- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex andid:(NSString*)postid  andcellindex:(int)cellindexpath;

@end

@interface MLKMenuPopover : UIView <UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)  NSString* postid;
@property(nonatomic,assign)  int cellindex;
@property(nonatomic,assign)  BOOL isSearchPopview;


@property(nonatomic,assign) id<MLKMenuPopoverDelegate> menuPopoverDelegate;

//- (id)initWithFrame:(CGRect)frame menuItems:(NSArray *)menuItems;
- (id)initWithFrame:(CGRect)frame menuItems:(NSArray *)aMenuItems  imagemenuItems:(NSArray *)imageMenu isSearch:(BOOL)searrchPopOver;

- (void)showInView:(UIView *)view;
- (void)dismissMenuPopover;
- (void)layoutUIForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

@end
