//
//  MLKMenuPopover.m
//  MLKMenuPopover
//
//  Created by NagaMalleswar on 20/11/14.
//  Copyright (c) 2014 NagaMalleswar. All rights reserved.
//

#import "MLKMenuPopover.h"
#import <QuartzCore/QuartzCore.h>

#define RGBA(a, b, c, d) [UIColor colorWithRed:(a / 255.0f) green:(b / 255.0f) blue:(c / 255.0f) alpha:d]

#define MENU_ITEM_HEIGHT        44
#define FONT_SIZE               15
#define CELL_IDENTIGIER         @"MenuPopoverCell"
#define MENU_TABLE_VIEW_FRAME   CGRectMake(0, 0, frame.size.width, frame.size.height)
#define SEPERATOR_LINE_RECT     CGRectMake(10, MENU_ITEM_HEIGHT - 1, self.frame.size.width - 20, 1)

#define MENU_POINTER_RECT       CGRectMake(frame.origin.x+frame.size.width-23, frame.origin.y+5, 23, 11)

#define MENU_POINTER_RECTLeft   CGRectMake(10, frame.origin.y+5, 23, 11)



#define CONTAINER_BG_COLOR      RGBA(0, 0, 0, 0.1f)

#define ZERO                    0.0f
#define ONE                     1.0f
#define ANIMATION_DURATION      0.5f

#define MENU_POINTER_TAG        1011
#define MENU_TABLE_VIEW_TAG     1012

#define LANDSCAPE_WIDTH_PADDING 50

@interface MLKMenuPopover (){
    UIButton *btn1,*btn2,*btn3,*btn4;
    BOOL dropdown;
}

@property(nonatomic,retain) NSArray *menuItems;

@property(nonatomic,retain) NSArray *imagemenuItems;
@property(nonatomic,retain) UIButton *containerButton;

- (void)hide;
- (void)addSeparatorImageToCell:(UITableViewCell *)cell;


@end

@implementation MLKMenuPopover

@synthesize menuPopoverDelegate;
@synthesize menuItems,imagemenuItems;
@synthesize containerButton,isSearchPopview;

- (id)initWithFrame:(CGRect)frame menuItems:(NSArray *)aMenuItems  imagemenuItems:(NSArray *)imageMenu isSearch:(BOOL)searrchPopOver
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.menuItems = aMenuItems;
        self.imagemenuItems=imageMenu;
        isSearchPopview=searrchPopOver;
        // Adding Container Button which will take care of hiding menu when user taps outside of menu area
        self.containerButton = [[UIButton alloc] init];
        [self.containerButton setBackgroundColor:CONTAINER_BG_COLOR];
        [self.containerButton addTarget:self action:@selector(dismissMenuPopover) forControlEvents:UIControlEventTouchUpInside];
        [self.containerButton setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin];
        
        // Adding Menu Options Pointer
        UITableView *menuItemsTableView;
        
        
        // adding left indicator image
        
        if (isSearchPopview) {
         menuItemsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 11, frame.size.width-20, frame.size.height-5)];
            
            menuItemsTableView.layer.cornerRadius=10;
            UIImageView *menuPointerView = [[UIImageView alloc] initWithFrame:MENU_POINTER_RECTLeft];
            menuPointerView.image = [UIImage imageNamed:@"white_arrow"];
            menuPointerView.tag = MENU_POINTER_TAG;
            [self.containerButton addSubview:menuPointerView];
        }
        else{
          menuItemsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 11, frame.size.width, frame.size.height)];
            
            UIImageView *menuPointerView = [[UIImageView alloc] initWithFrame:MENU_POINTER_RECT];
            menuPointerView.image = [UIImage imageNamed:@"white_arrow"];
            menuPointerView.tag = MENU_POINTER_TAG;
            [self.containerButton addSubview:menuPointerView];
        }
    
        // Adding menu Items table
        
        menuItemsTableView.dataSource = self;
        menuItemsTableView.delegate = self;
        menuItemsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        menuItemsTableView.scrollEnabled = NO;
        menuItemsTableView.backgroundColor = [UIColor whiteColor];
        menuItemsTableView.tag = MENU_TABLE_VIEW_TAG;
        UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Menu_PopOver_BG"]];
        menuItemsTableView.backgroundView = bgView;
        [self addSubview:menuItemsTableView];
        [self.containerButton addSubview:self];
    }
    
    if (imageMenu.count==0) {
        dropdown=YES;
    }
    
    return self;
}

#pragma mark -
#pragma mark UITableViewDatasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return MENU_ITEM_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return [self.menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = CELL_IDENTIGIER;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell.textLabel setFont:[UIFont systemFontOfSize:12.0f]];
        [cell.textLabel setTextColor:[UIColor blackColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSInteger numberOfRows = [tableView numberOfRowsInSection:indexPath.section];
    
    
    if( [tableView numberOfRowsInSection:indexPath.section] > ONE && !(indexPath.row == numberOfRows - 1) )
    {
        if(!isSearchPopview)
        {
        
            [self addSeparatorImageToCell:cell];
        }
        
    }
    
    if(isSearchPopview)
    {
        if (indexPath.row==0) {
            btn1=[UIButton buttonWithType:UIButtonTypeCustom];
            btn1.frame=CGRectMake(-3, 5, 150, 35);
            btn1.tag=indexPath.row;
            [btn1 addTarget:self action:@selector(SearchFriendsMethod:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btn1];
            [btn1 setTitle:@"Search Followers"forState:UIControlStateNormal];
            [btn1.titleLabel setFont:[UIFont fontWithName:@"Avenir-Roman" size:15.0f]];
            btn1.titleLabel.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0f];
            //btn1.titleLabel.font=[UIFont systemFontOfSize:15.0];
            [btn1 setTitleColor:[UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0f] forState:UIControlStateNormal];
            
        }
        else{
            
            btn2=[UIButton buttonWithType:UIButtonTypeCustom];
            btn2.frame=CGRectMake(-11, -10, 150, 35);
            btn2.tag=indexPath.row;
            [btn2 addTarget:self action:@selector(SearchGroupsMethod:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btn2];
            [btn2 setTitle:@"Search Groups" forState:UIControlStateNormal];
            [btn2.titleLabel setFont:[UIFont fontWithName:@"Avenir-Roman" size:15.0f]];
            btn2.titleLabel.textColor = [UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0f];
            //btn2.titleLabel.font=[UIFont systemFontOfSize:15.0];
            [btn2 setTitleColor:[UIColor colorWithRed:72.0/255.0 green:93.0/255.0 blue:107.0/255.0 alpha:1.0f] forState:UIControlStateNormal];
        }
        
    }
    
    
   else if (dropdown==YES) {
            if (indexPath.row==0) {
         btn1=[UIButton buttonWithType:UIButtonTypeCustom];
        cell.contentView.layer.cornerRadius=5.0f;
                
        btn1.frame=CGRectMake(0, 0, 70, 35);
                         [btn1 setTitle:[self.menuItems objectAtIndex:indexPath.row]  forState:UIControlStateNormal];
                if ([[self.menuItems objectAtIndex:indexPath.row] isEqualToString:@"Delete"]) {
                         [btn1 addTarget:self action:@selector(dropdowndelete:) forControlEvents:UIControlEventTouchUpInside];
                }else{
                               [btn1 addTarget:self action:@selector(dropdownUpdate:) forControlEvents:UIControlEventTouchUpInside];
                }
    
        btn1.titleLabel.font=[UIFont systemFontOfSize:9.0];
        [btn1 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
       // btn1.backgroundColor=[UIColor redColor];
              
        [cell.contentView addSubview:btn1];
       
            }
    }
    else
    {
    if (indexPath.row==0) {
        btn1=[UIButton buttonWithType:UIButtonTypeCustom];
        btn1.frame=CGRectMake(10, 5, 150, 35);
        btn1.tag=indexPath.row;
        [btn1 addTarget:self action:@selector(button1:) forControlEvents:UIControlEventTouchUpInside];
     [cell.contentView addSubview:btn1];
        [btn1 setTitle:@"My Prayer Request Only"forState:UIControlStateNormal];
        btn1.titleLabel.font=[UIFont systemFontOfSize:11.0];
        // btn4.backgroundColor=[UIColor greenColor];
        //bottom,right,top
        UIImageView *ima=[[UIImageView alloc]initWithFrame:CGRectMake(0, 8, 20, 20)];
        ima.image=[UIImage imageNamed:@"praised_unselected"];
        [btn1 addSubview:ima];
        btn1.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -30);
        [btn1 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    else if (indexPath.row==1){
        btn2=[UIButton buttonWithType:UIButtonTypeCustom];
        //  btn3.backgroundColor=[UIColor blueColor];
        btn2.frame=CGRectMake(10, 5, 150, 35);
        btn2.tag=indexPath.row;
        [btn2 addTarget:self action:@selector(button2:) forControlEvents:UIControlEventTouchUpInside];
        // [btn4 setImage:[menuItems objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        //  [btn4 setImage:[imagemenuItems objectAtIndex:indexPath.row] forState:UIControlStateSelected];
        [cell.contentView addSubview:btn2];
        [btn2 setTitle:@"My Status Update Only" forState:UIControlStateNormal];
        btn2.titleLabel.font=[UIFont systemFontOfSize:11.0];
        // btn4.backgroundColor=[UIColor greenColor];
        //bottom,right,top
        UIImageView *ima=[[UIImageView alloc]initWithFrame:CGRectMake(0, 8, 20, 20)];
        ima.image=[UIImage imageNamed:@"filter_status"];
        [btn2 addSubview:ima];
        btn2.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -25);
        [btn2 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        

    }
    else if (indexPath.row==2){
        btn3=[UIButton buttonWithType:UIButtonTypeCustom];
      //  btn3.backgroundColor=[UIColor blueColor];
        btn3.frame=CGRectMake(10, 5, 150, 35);
        btn3.tag=indexPath.row;
        [btn3 addTarget:self action:@selector(button3:) forControlEvents:UIControlEventTouchUpInside];
        // [btn4 setImage:[menuItems objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        //  [btn4 setImage:[imagemenuItems objectAtIndex:indexPath.row] forState:UIControlStateSelected];
        [cell.contentView addSubview:btn3];
        [btn3 setTitle:@"View all" forState:UIControlStateNormal];
        btn3.titleLabel.font=[UIFont systemFontOfSize:11.0];
        // btn4.backgroundColor=[UIColor greenColor];
        //bottom,right,top
        UIImageView *ima=[[UIImageView alloc]initWithFrame:CGRectMake(0, 8, 20, 20)];
        ima.image=[UIImage imageNamed:@"EyeIcon"];
        [btn3 addSubview:ima];
        btn3.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 57);
        [btn3 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }
   
    return cell;
}

-(void)SearchFriendsMethod:(UIButton*)btn{
    btn1.selected=YES;
    btn2.selected=false;
    btn1.backgroundColor=[self colorFromHexString:@"1AA8EA"];
    btn2.backgroundColor=[UIColor clearColor];
    // [self hide];
    [self.menuPopoverDelegate menuPopover:self didSelectMenuItemAtIndex:btn1.tag];
    
    
}


-(void)SearchGroupsMethod:(UIButton*)btn{
    
    btn2.selected=YES;
    btn2.backgroundColor=[self colorFromHexString:@"1AA8EA"];
    btn1.backgroundColor=[UIColor clearColor];
    [self.menuPopoverDelegate menuPopover:self didSelectMenuItemAtIndex:btn2.tag];
}

-(void)button1:(UIButton*)btn{
//    NSLog(@"button 1");
    btn1.selected=YES;
    btn2.selected=false;
    btn3.selected=false;
    btn1.backgroundColor=[self colorFromHexString:@"1AA8EA"];
    btn2.backgroundColor=[UIColor clearColor];
    btn3.backgroundColor=[UIColor clearColor];
   // [self hide];
    [self.menuPopoverDelegate menuPopover:self didSelectMenuItemAtIndex:btn1.tag];
    
    
}
-(void)button2:(UIButton*)btn{
    btn1.selected=false;
    btn2.selected=YES;
    btn3.selected=false;
    btn2.backgroundColor=[self colorFromHexString:@"1AA8EA"];
    btn1.backgroundColor=[UIColor clearColor];
    btn3.backgroundColor=[UIColor clearColor];
 //   [self hide];
    [self.menuPopoverDelegate menuPopover:self didSelectMenuItemAtIndex:btn2.tag];
    
}
-(void)button3:(UIButton*)btn{
    btn1.selected=false;
    btn2.selected=false;
    btn4.selected=false;
    btn3.selected=YES;
    btn3.backgroundColor=[self colorFromHexString:@"1AA8EA"];
    btn2.backgroundColor=[UIColor clearColor];
    btn1.backgroundColor=[UIColor clearColor];
    btn4.backgroundColor=[UIColor clearColor];
   // [self hide];
  [self.menuPopoverDelegate menuPopover:self didSelectMenuItemAtIndex:btn3.tag];
    
    
    
}
-(void)dropdowndelete:(UIButton*)btn{
    
    int index=5;
    [self.menuPopoverDelegate menuPopover:self didSelectMenuItemAtIndex:index  andid:_postid  andcellindex:_cellindex];
}
-(void)dropdownUpdate:(UIButton*)btn{
    
    int index=6;
    if([self.menuPopoverDelegate respondsToSelector:@selector(menuPopover:didSelectMenuItemAtIndex:andid:andcellindex:)])
            [self.menuPopoverDelegate menuPopover:self didSelectMenuItemAtIndex:index  andid:_postid  andcellindex:_cellindex];
        
}
//-(void)button4:(UIButton*)btn{
//    btn1.selected=false;
//    btn2.selected=false;
//    btn3.selected=false;
//    btn4.selected=YES;
//    btn4.backgroundColor=[self colorFromHexString:@"1AA8EA"];
//    btn2.backgroundColor=[UIColor clearColor];
//    btn1.backgroundColor=[UIColor clearColor];
//    btn3.backgroundColor=[UIColor clearColor];
//    
// //   [self hide];
//   // [self.menuPopoverDelegate menuPopover:self didSelectMenuItemAtIndex:btn4.tag];
//}


#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

#pragma mark -
#pragma mark Actions

- (void)dismissMenuPopover
{
    [self hide];
}

- (void)showInView:(UIView *)view
{
    self.containerButton.alpha = ZERO;
    self.containerButton.frame = view.bounds;
    [view addSubview:self.containerButton];
    
    [UIView animateWithDuration:ANIMATION_DURATION
                     animations:^{
                         self.containerButton.alpha = ONE;
                     }
                     completion:^(BOOL finished) {}];
}

- (void)hide
{
    [UIView animateWithDuration:ANIMATION_DURATION
                     animations:^{
                         self.containerButton.alpha = ZERO;
                     }
                     completion:^(BOOL finished) {
                         [self.containerButton removeFromSuperview];
                     }];
}

#pragma mark -
#pragma mark Separator Methods

- (void)addSeparatorImageToCell:(UITableViewCell *)cell
{
    UIImageView *separatorImageView = [[UIImageView alloc] initWithFrame:SEPERATOR_LINE_RECT];
    [separatorImageView setImage:[UIImage imageNamed:@"DefaultLine"]];
    separatorImageView.opaque = YES;
    [cell.contentView addSubview:separatorImageView];
}

#pragma mark -
#pragma mark Orientation Methods

- (void)layoutUIForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    BOOL landscape = (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    
    UIImageView *menuPointerView = (UIImageView *)[self.containerButton viewWithTag:MENU_POINTER_TAG];
    UITableView *menuItemsTableView = (UITableView *)[self.containerButton viewWithTag:MENU_TABLE_VIEW_TAG];
    
    if( landscape )
    {
        menuPointerView.frame = CGRectMake(menuPointerView.frame.origin.x + LANDSCAPE_WIDTH_PADDING, menuPointerView.frame.origin.y, menuPointerView.frame.size.width, menuPointerView.frame.size.height);
        
        menuItemsTableView.frame = CGRectMake(menuItemsTableView.frame.origin.x + LANDSCAPE_WIDTH_PADDING, menuItemsTableView.frame.origin.y, menuItemsTableView.frame.size.width, menuItemsTableView.frame.size.height);
    }
    else
    {
        menuPointerView.frame = CGRectMake(menuPointerView.frame.origin.x - LANDSCAPE_WIDTH_PADDING, menuPointerView.frame.origin.y, menuPointerView.frame.size.width, menuPointerView.frame.size.height);
        
        menuItemsTableView.frame = CGRectMake(menuItemsTableView.frame.origin.x - LANDSCAPE_WIDTH_PADDING, menuItemsTableView.frame.origin.y, menuItemsTableView.frame.size.width, menuItemsTableView.frame.size.height);
    }
}

-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


@end
