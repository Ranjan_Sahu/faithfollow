//
//  GroupSearchViewController.h
//  wireFrameSplash
//
//  Created by home on 6/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupSearchViewController : UIViewController
@property(strong,nonatomic)IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *airlines;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property(nonatomic,weak)IBOutlet  UISearchBar *searchbar;
@property (nonatomic, assign) bool isFiltered;
- (IBAction)mygroupMethod:(id)sender;

@end
