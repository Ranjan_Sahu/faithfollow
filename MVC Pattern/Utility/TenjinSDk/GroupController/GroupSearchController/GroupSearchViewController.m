//
//  GroupSearchViewController.m
//  wireFrameSplash
//
//  Created by home on 6/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "GroupSearchViewController.h"
#import "CustomTableViewCell.h"
#import "FriendsProfileViewController.h"
#import "MyGroupViewController.h"
@interface GroupSearchViewController ()
{
    NSMutableArray *searcharray,*request_dataArray,*webSearchArray;
    NSString *searchString;
    NSMutableDictionary *dictwebSearchArray;

}
@end

@implementation GroupSearchViewController
@synthesize isFiltered;

- (void)viewDidLoad {
    [super viewDidLoad];
    _airlines=[[NSMutableArray alloc]initWithObjects:@"raj",@"Amit",@"Candan",@"ashok",@"depak",nil];
    searcharray=[[NSMutableArray alloc]init];
    webSearchArray=[[NSMutableArray alloc]init];
    [self.view makeToast:@"please search " duration:1.0 position:CSToastPositionCenter];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [webSearchArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict=[webSearchArray objectAtIndex:indexPath.row];
    CustomTableViewCell *cell;
    if (cell==nil) {
        
        cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    }
    cell.namelabel.text=[dict valueForKey:@"name"];
    [cell.btn_GroupJoin  addTarget:self action:@selector(joincommunityGroup:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btn_GroupJoin setTag:indexPath.row];
      return cell;
    
}

#pragma Identify method friend or community



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}



#pragma Join community details

-(void)joincommunityGroup :(id)sender{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    dictwebSearchArray=[webSearchArray objectAtIndex:indexPath.row];

    if ([[dictwebSearchArray valueForKey:@"object_type"]isEqualToString:@"community"]) {
        [self viewCommunityProfile:dictwebSearchArray[@"object_id"] communityId:dictwebSearchArray[@"id"]];
    }
  
}

#pragma viewFriendProfile

-(void)viewFriendProfile :(NSString*)friend_object FriendId:(NSString*)Id{
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           @"friend_id":friend_object
                           };
    NSLog(@"disct is%@",dict);
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"friendsprofile" withParameters:dict withHud:YES success:^(id response){
        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    NSMutableDictionary *FriendDict=response[@"data"];
                    dispatch_async(dispatch_get_main_queue(), ^{

                        NSString *friebdStatus=[FriendDict valueForKey:@"friend_status"];
                        NSDictionary *profileviewDict=[FriendDict valueForKey:@"usersprofile"];
                        NSString *profileview_option=[NSString stringWithFormat:@"%@",[profileviewDict valueForKey:@"profileview_option"]];
                        NSString *profile_picture=[profileviewDict valueForKey:@"profile_picture"];
                        NSString *nameTxt=[NSString stringWithFormat:@"%@",[Constant1 getStringObject: [NSString stringWithFormat:@"%@ %@",FriendDict[@"first_name"],FriendDict[@"last_name"]]]];
                        [[NSUserDefaults standardUserDefaults] setObject:nameTxt forKey:@"name"];
                        [[NSUserDefaults standardUserDefaults] setObject:profile_picture forKey:@"profile_picture12"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        FriendsProfileViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"FriendsProfileViewController"];
                        allprofileView.friendProfileDict=FriendDict;
                        allprofileView.postFriendProfileDict=FriendDict;
                        [self.navigationController pushViewController:allprofileView animated:YES];
                        
                        // USE PRESEENTED VIEW
                        
                    });
                }
            }
        }
        else{
            
            NSMutableArray  *messageArray=[response valueForKey:@"message"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
            });
        }
        
    } errorBlock:^(id error)
     {
         NSLog(@"error");
         
     }];
    
}




#pragma viewCommunityJOIN

-(void)viewCommunityProfile :(NSString*)community_object communityId:(NSString*)Id{
    NSDictionary *dict = @{
                           @"user_id" : [[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                           @"com_id" : community_object,
                           };

    NSLog(@"disct is%@",dict);
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:communitydetails withParameters:dict withHud:YES success:^(id response){
        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==0){
                    NSMutableArray  *messageArray=[response valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
 
                    });
                }
            }
        }
        else{

            NSMutableArray  *messageArray=[response valueForKey:@"message"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
            });
        }

    } errorBlock:^(id error)
     {
         NSLog(@"error");

     }];

}


-(void)InvitesFriendMethod {
    NSLog(@"hello invited");
}

-(void)TickClickBTN:(UIButton*)sender {
    
    sender.selected=!sender.selected;
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [self.tableView convertPoint:buttonOrigin fromView:[sender superview]];
    NSIndexPath *rowIndexPath = [self.tableView indexPathForRowAtPoint:originInTableView];
    NSInteger section = [rowIndexPath section];
}


#pragma mark - UISearchControllerDelegate & UISearchResultsDelegate


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length == 0)
    {
    }
    else{
        searchString=searchText;
        
    }
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self serachTextString:searchString];
    
    
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    dispatch_async(dispatch_get_main_queue(), ^{
        [searchBar resignFirstResponder];
        //        [self serachTextString:searchString];
        //        [self.tableView reloadData];
    });
    
    
}

-(void)serachTextString:(NSString*)searchText {
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:appTitle message:NoNetwork  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message show];
        
    }
    else{
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"searchterm" :searchText,
                               };
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"search" withParameters:dict withHud:YES success:^(id response){
            NSLog(@"responmse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                            [_searchbar resignFirstResponder];
                            webSearchArray=[response valueForKey:@"data"];
                            [self.tableView reloadData];
                            
                        }
                        else{
                            NSMutableArray *messageArray=[[NSMutableArray alloc]init];
                            messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            
                        }
                    });
                }
            }
        } errorBlock:^(id error)
         {
             NSLog(@"error");
             
         }];
    }
}
-(void)sendRequestforViewprofile:(NSString*)methodName anddict:(NSDictionary*)dict{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:appTitle message:NoNetwork  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message show];
        
    }
    else{
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:methodName withParameters:dict withHud:YES success:^(id response){
            NSLog(@"responmse is%@",response);
            if ([response isKindOfClass:[NSDictionary class]]){
                if (![response isKindOfClass:[NSNull class]]) {
                    if([response[@"status"]intValue ] ==1){
                        NSMutableDictionary*    friendsdataDict=[response valueForKey:@"data"];
                        if (friendsdataDict) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if ([methodName isEqualToString:communityDisplay]) {
//                                    ViewMycommunityViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewMycommunityViewController"];
//                                    allprofileView.friendProfileDict=friendsdataDict;
//                                    [self.navigationController pushViewController:allprofileView animated:YES];
                                }
                                
                            });
                        }else{
                            NSMutableArray *messageArray=[[NSMutableArray alloc]init];
                            messageArray=[response valueForKey:@"message"];
                            [self.view makeToast:[messageArray objectAtIndex:0] duration:1.0 position:CSToastPositionCenter];
                            
                        }
                        
                    }
                }
            }
            
        } errorBlock:^(id error)
         {
             NSLog(@"error");
             
         }];
    }
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)mygroupMethod:(id)sender {
    
    MyGroupViewController *grouplistView=[self.storyboard instantiateViewControllerWithIdentifier:@"MyGroupViewController"];
    [self.navigationController pushViewController:grouplistView animated:YES];
    
    
}
@end
