//
//  DetailMygroupViewController1.m
//  wireFrameSplash
//
//  Created by home on 6/23/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "DetailMygroupViewController1.h"
#import "DiscusscustomTableViewCell.h"
#import "PrayerCustomTableViewCell.h"
#import "DetailMygruopViewController2.h"
@interface DetailMygroupViewController1 ()
{
    NSMutableArray *webgroupArray;
}
@end

@implementation DetailMygroupViewController1

- (void)viewDidLoad {
    [super viewDidLoad];
    webgroupArray=[[NSMutableArray alloc]init];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return [webgroupArray count];
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
         DiscusscustomTableViewCell  *cell1 ;
          PrayerCustomTableViewCell  *cell2;
    
      if (indexPath.row!=2) {
        if (cell1 == nil) {
           cell1 =(DiscusscustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DiscussCell"];
//            UIImage *placeholderImage = [UIImage imageNamed:@"profile_image"];
//            cell1.imageView.image=placeholderImage;
            cell1.discussionLBL.text=@"Discussion1";
            cell1.textLBL.text=@"This is the first couple of lines from the person's discussion.";
            return cell1;
        }
    }
    
    
    if (indexPath.row==2) {
      if (cell2 == nil) {
        cell2 =(PrayerCustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PrayerCell"];
//          UIImage *placeholderImage = [UIImage imageNamed:@"profile_image"];
//        cell2.imageView.image=placeholderImage;
        cell2.nameLBL.text=@"Fisrtnamelastname";
        cell2.requestedLBL.text=@"Requested prayer";
        cell2.textLBL.text=@"Please pray for my family.";
        [cell2.pryerBTN addTarget:self action:@selector(prayermethod:) forControlEvents:UIControlEventTouchUpInside];
        [cell2.commentBTN addTarget:self action:@selector(commentmethod:) forControlEvents:UIControlEventTouchUpInside];
        [cell2.pryerBTN addTarget:self action:@selector(pryermethod:) forControlEvents:UIControlEventTouchUpInside];
        return cell2;
    }
    
    }
  
 return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != 2) {
        return 70;
    }
    else {
        return 175;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma prayermethod
-(void)prayermethod:(id)sender{
    
}
#pragma commentmethod
-(void)commentmethod:(id)sender{
    
}
#pragma pryermethod
-(void)pryermethod:(id)sender{
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
           DetailMygruopViewController2 *detailViewGroup=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygruopViewController2"];
        [self.navigationController pushViewController:detailViewGroup animated:YES];
 
    
}










- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
