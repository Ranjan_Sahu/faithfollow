//
//  MyGroupViewController.m
//  wireFrameSplash
//
//  Created by home on 6/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "MyGroupViewController.h"
#import "CustomTableViewCell.h"
#import "DetailMygroupViewController1.h"

@interface MyGroupViewController ()
{
    NSMutableArray *searcharray,*request_dataArray,*webSearchArray;
    NSString *searchString;
    NSMutableDictionary *dictwebSearchArray;
    
}
@end

@implementation MyGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self mygroupListMethod];
    
    // Do any additional setup after loading the view.
}

-(void)mygroupListMethod{
    //
    NSDictionary *dict = @{
                           @"user_id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                           @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                           };
    NSLog(@"disct is%@",dict);
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:communitylist withParameters:dict withHud:YES success:^(id response){
        NSLog(@"resposnse is%@",response);
        if ([response isKindOfClass:[NSDictionary class]]){
            if (![response isKindOfClass:[NSNull class]]) {
                if([response[@"status"]intValue ] ==1){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        webSearchArray=[response valueForKey:@"data"];
                        [self.tableView reloadData];
                    });
                }
            }
        }
        else{
            
            NSMutableArray  *messageArray=[response valueForKey:@"message"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view makeToast:[messageArray objectAtIndex:0]  duration:1.0 position:CSToastPositionCenter];
            });
        }
        
    } errorBlock:^(id error)
     {
         NSLog(@"error");
         
     }];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [webSearchArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict=[webSearchArray objectAtIndex:indexPath.row];
    NSDictionary *getcommuntiyDict=[dict valueForKey:@"get_community"];
    CustomTableViewCell *cell;
    
    if (cell==nil) {
        
        cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    }
    cell.namelabel.text=[getcommuntiyDict valueForKey:@"name"];
    
    return cell;
    
    return 0;
}

#pragma Identify method friend or community
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailMygroupViewController1 *detailView=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailMygroupViewController1"];
    [self.navigationController pushViewController:detailView animated:YES];
    
}




@end
