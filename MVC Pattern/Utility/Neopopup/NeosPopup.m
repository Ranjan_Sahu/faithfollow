//
//  NeosPopup.m
//  wireFrameSplash
//
//  Created by Vikas on 17/06/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "NeosPopup.h"


@implementation NeosPopup
- (id)init {
    return [self initWithFrame:[[UIScreen mainScreen] bounds]];
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:CGRectMake(0, 0,  [UIApplication sharedApplication].keyWindow.frame.size.width, [UIApplication sharedApplication].keyWindow.frame.size.height)];
    if (self) {
          self.userInteractionEnabled = YES;

        
        self.backgroundView=[[UIView alloc]init];
        self.backgroundView.frame=self.bounds;
        self.backgroundView.backgroundColor= [UIColor colorWithRed:(0.0/255.0f) green:(0.0/255.0f) blue:(0.0/255.0f) alpha:0.5];
     //   self.backgroundView.backgroundColor=[UIColor yellowColor];
        [self addSubview:  self.backgroundView];
   
    
        
        MyScrollVw= [[TPKeyboardAvoidingScrollView alloc]initWithFrame:self.backgroundView.frame];
       //MyScrollVw.backgroundColor=[UIColor redColor];
        MyScrollVw.delegate=(id)self;
        [self.backgroundView addSubview:MyScrollVw];
    }
    return self;
}

- (void) setBackcontentView:(UIView*)Vw{
    Vw.center=MyScrollVw.center;

    if (_taponContentView==YES) {
            [MyScrollVw addSubview:Vw];
    }else{
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
        [self.backgroundView addGestureRecognizer:tapGestureRecognizer];
            [self addSubview:Vw];
    }


    
    
}
- (void)show{

    
    
          [[UIApplication sharedApplication].keyWindow addSubview:self];
}
-(void)dismiss{
    [self removeFromSuperview];
}
- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
        [self removeFromSuperview];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
