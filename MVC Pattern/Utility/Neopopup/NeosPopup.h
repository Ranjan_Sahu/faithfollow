//
//  NeosPopup.h
//  wireFrameSplash
//
//  Created by Vikas on 17/06/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "UIScrollView+TPKeyboardAvoidingAdditions.h"
@interface NeosPopup : UIView{
    TPKeyboardAvoidingScrollView  *MyScrollVw;
}


@property (nonatomic, strong) UIView* backcontentView;
@property (nonatomic, strong) UIView* backgroundView;
@property(nonatomic,assign)BOOL taponContentView;
- (void) setBackcontentView:(UIView*)Vw;
- (void)show;
- (void)dismiss;

@end
