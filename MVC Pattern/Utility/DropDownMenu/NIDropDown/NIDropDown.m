//
//  NIDropDown.m
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import "NIDropDown.h"
#import "QuartzCore/QuartzCore.h"

@interface NIDropDown ()
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSArray *list;
@property(nonatomic, retain) NSArray *imageList;
@end

@implementation NIDropDown
@synthesize table;
@synthesize btnSender;
@synthesize list;
@synthesize imageList;
@synthesize delegate;
@synthesize animationDirection;

- (id)showDropDown:(UIView *)b :(CGFloat *)height :(NSArray *)arr :(NSArray *)imgArr :(NSString *)direction button:(UIButton *)button{
    btnSender = button;
    animationDirection = direction;
    self.table = (UITableView *)[super init];
    if (self) {
        // Initialization code
        CGRect btn = b.frame;
        
        
        if(b.tag == 143) {
            CGRect frameBtn = b.frame;
            frameBtn.size.width = [b viewWithTag:147].frame.size.width;
            frameBtn.origin.x=[b viewWithTag:147].frame.origin.x+b.frame.origin.x;
            btn=frameBtn;
        }
        
        
        if(b.tag == 119) {
            CGRect frameBtn = b.frame;
            frameBtn.size.width = [b viewWithTag:120].frame.size.width;
            frameBtn.origin.x=[b viewWithTag:120].frame.origin.x+b.frame.origin.x;
            btn=frameBtn;
        }
        
        if(b.tag == 159) {
            CGRect frameBtn = b.frame;
            frameBtn.size.width = [b viewWithTag:160].frame.size.width;
            frameBtn.origin.x=[b viewWithTag:160].frame.origin.x+b.frame.origin.x;
            btn=frameBtn;
        }
        
        if(b.tag == 169) {
            CGRect frameBtn = b.frame;
            frameBtn.size.width = [b viewWithTag:170].frame.size.width;
            frameBtn.origin.x=[b viewWithTag:170].frame.origin.x+b.frame.origin.x;
            btn=frameBtn;
        }
        if(b.tag == 179) {
            CGRect frameBtn = b.frame;
            frameBtn.size.width = [b viewWithTag:180].frame.size.width;
            frameBtn.origin.x=[b viewWithTag:180].frame.origin.x+b.frame.origin.x;
            btn=frameBtn;
        }
        
        
        self.list = [NSArray arrayWithArray:arr];
        self.imageList = [NSArray arrayWithArray:imgArr];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, -5);
        }else if ([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, 5);
        }
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width, 0)];
        table.delegate = self;
        table.dataSource = self;
        table.layer.cornerRadius = 5;
        //        table.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
        
        //        table.backgroundColor=(__bridge UIColor * _Nullable)([UIColor whiteColor].CGColor);
        table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        table.separatorColor = [UIColor grayColor];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width, *height);
        } else if([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *height);
        }
        
        
        if ( [[[NSUserDefaults standardUserDefaults] valueForKey:@"selectRelationMethod123"] isEqualToString:@"selectRelationMethod123"])
        {
            table.frame = CGRectMake(0, 0, btn.size.width,100);
            [[NSUserDefaults standardUserDefaults] setObject:@"selectRelationMethod1" forKey:@"selectRelationMethod1"];
        }

        
        if ( [[[NSUserDefaults standardUserDefaults] valueForKey:@"Dropdowncountry"] isEqualToString:@"Dropdowncountry"])
        {
            table.frame = CGRectMake(0, 0, btn.size.width,100);
            [[NSUserDefaults standardUserDefaults] setObject:@"Dropdowncountry1" forKey:@"Dropdowncountry1"];
        }

        
        
        
        
        if ( [[[NSUserDefaults standardUserDefaults] valueForKey:@"Dropdown"] isEqualToString:@"Dropdown"])
        {
            table.frame = CGRectMake(0, 0, btn.size.width,100);
            [[NSUserDefaults standardUserDefaults] setObject:@"Dropdown1" forKey:@"Dropdown"];
        }
        
        if ( [[[NSUserDefaults standardUserDefaults] valueForKey:@"denomination"] isEqualToString:@"denomination"])
        {
            table.frame = CGRectMake(0, 0, btn.size.width,110);
            
            [[NSUserDefaults standardUserDefaults] setObject:@"denomination1" forKey:@"denomination"];
        }
        
        if ( [[[NSUserDefaults standardUserDefaults] valueForKey:@"month"] isEqualToString:@"month"])
        {
            table.frame = CGRectMake(0, 0, btn.size.width,120);
            
            [[NSUserDefaults standardUserDefaults] setObject:@"month1" forKey:@"month"];
        }
        if ( [[[NSUserDefaults standardUserDefaults] valueForKey:@"me"] isEqualToString:@"me"])
        {
            table.frame = CGRectMake(0, 0, btn.size.width,70);
            [[NSUserDefaults standardUserDefaults] setObject:@"me1" forKey:@"me"];
        }
        
        [UIView commitAnimations];
        [b.superview addSubview:self];
        [self addSubview:table];
    }
    return self;
}

-(void)hideDropDown:(UIButton *)b {
    CGRect btn = b.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.0];
    if ([animationDirection isEqualToString:@"up"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 25;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.textColor=[UIColor darkGrayColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
    }
    cell.textLabel.text =[list objectAtIndex:indexPath.row];
    UIView * v = [[UIView alloc] init];
    v.backgroundColor = [UIColor blackColor];
    cell.selectedBackgroundView = v;
    
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hideDropDown:btnSender];
    UITableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
    [btnSender setTitle:c.textLabel.text forState:UIControlStateNormal];

    if (self.tag==111) {
        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"indexpath"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if (self.tag==222) {
        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"indexpathcountry"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    
    if (self.tag==333) {
        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"indexpathcountry"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if (self.tag==666) {
        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"indexpath"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if (self.tag==777) {
        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"indexpath"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    

    
//
//    NSLog(@"nsuer %ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"indexpath"]);
    [self myDelegate];
}

- (void) myDelegate {
    [self.delegate niDropDownDelegateMethod:self];
}

-(void)dealloc {
    //    [super dealloc];
    //    [table release];
    //    [self release];
}

@end
