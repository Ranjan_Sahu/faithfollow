//
//  MycommunityTableViewCell.h
//  wireFrameSplash
//
//  Created by Vikas on 10/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MycommunityTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UIButton *btn_viewprofile;
@property (weak, nonatomic) IBOutlet UIImageView *influencerImage;

@end
