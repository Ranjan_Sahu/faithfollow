//
//  myinfluencerCell.h
//  wireFrameSplash
//
//  Created by webwerks on 11/18/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface myinfluencerCell : UITableViewCell

@property(weak,nonatomic)IBOutlet UIImageView *imageView_profile;
@property (weak, nonatomic) IBOutlet UILabel *profile_name;
@property (weak, nonatomic) IBOutlet UIButton *followBTN;
@property (weak, nonatomic) IBOutlet UILabel *contentLBL;
@property (weak, nonatomic) IBOutlet UILabel *countFollowers;
@property (weak, nonatomic) IBOutlet UIWebView *webview;
// constraints height
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profilenameConstraintsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentPostConstraintsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webContentConstraintsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followerCountConstraintsHeight;

@property (weak, nonatomic) IBOutlet UILabel *taghtmllbl;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;

@end
