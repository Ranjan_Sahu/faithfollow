//
//  PrayerCustomTableViewCell.h
//  wireFrameSplash
//
//  Created by home on 6/23/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrayerCustomTableViewCell : UITableViewCell
@property(weak,nonatomic)IBOutlet UIImageView *imageview;
@property(weak,nonatomic)IBOutlet UILabel *nameLBL;
@property(weak,nonatomic)IBOutlet UILabel *requestedLBL;
@property(weak,nonatomic)IBOutlet UILabel *textLBL;
@property(weak,nonatomic)IBOutlet UIButton *pryerBTN;
@property(weak,nonatomic)IBOutlet UIButton *commentBTN;
@property(weak,nonatomic)IBOutlet UIButton *seemoreBTN;

@property(weak,nonatomic)IBOutlet UIImageView *commentImage;
@property(weak,nonatomic)IBOutlet UILabel *preyerLBL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hightcontentLBL;
@property (weak, nonatomic) IBOutlet UIView *PrayerContentView;
@property (weak, nonatomic) IBOutlet UIView *commentPrayerView;

//new connection






@end
