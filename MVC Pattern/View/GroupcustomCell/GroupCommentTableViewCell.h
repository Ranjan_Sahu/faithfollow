//
//  GroupCommentTableViewCell.h
//  wireFrameSplash
//
//  Created by home on 6/30/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLabel.h"

@interface GroupCommentTableViewCell : UITableViewCell
@property(weak,nonatomic)IBOutlet UIImageView *commentImage;
@property(weak,nonatomic)IBOutlet UILabel *commentedlabel;
@property(weak,nonatomic)IBOutlet UILabel *timelable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *groupHeight;
@property(weak,nonatomic)IBOutlet UILabel *namelabel;
@property(weak,nonatomic)IBOutlet UIImageView *influencerimage;
@property (weak, nonatomic) IBOutlet PPLabel *pplableComment;
@end
