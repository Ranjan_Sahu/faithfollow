//
//  DiscusscustomTableViewCell.h
//  wireFrameSplash
//
//  Created by home on 6/23/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLabel.h"

@interface DiscusscustomTableViewCell : UITableViewCell
@property(weak,nonatomic)IBOutlet UIImageView *imageview;
@property(weak,nonatomic)IBOutlet UILabel *discussionLBL;
@property(weak,nonatomic)IBOutlet UILabel *textDiscussLBL;
@property(weak,nonatomic)IBOutlet UILabel *praisedLikeLBL;
@property(weak,nonatomic)IBOutlet UILabel *namediscuss2LBL;
@property(weak,nonatomic)IBOutlet UILabel *singleMotherLBL;
@property(weak,nonatomic)IBOutlet UILabel *dataLabel;
@property(weak,nonatomic)IBOutlet UIButton *praisedBtn;
@property(weak,nonatomic)IBOutlet UIButton *commentBtn;
@property(weak,nonatomic)IBOutlet UIImageView *attachementImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discuss2textLBL;
@property(weak,nonatomic)IBOutlet UIButton *heartBTN;
@property (weak, nonatomic) IBOutlet UIView *view_btnmoareimage;
@property (weak, nonatomic) IBOutlet UIButton *btn_moreImages;
@property(weak,nonatomic)IBOutlet UIImageView *gruopfavoriteImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hightTXTD;
@property(weak,nonatomic)IBOutlet UIButton *pryerBTN;
@property(weak,nonatomic)IBOutlet UIButton *commentBTN;
@property(weak,nonatomic)IBOutlet UIImageView *commentImage;
@property(weak,nonatomic)IBOutlet UILabel *preyerLBL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *text2heightheader;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *text2heightwidth;

@property(weak,nonatomic)IBOutlet UIView *discusomView;
@property(weak,nonatomic)IBOutlet UIImageView *influencerimage;
@property(weak,nonatomic)IBOutlet PPLabel *labelPP;

// new lable for name and time
@property(weak,nonatomic)IBOutlet UILabel *nameLBL;
@property(weak,nonatomic)IBOutlet UILabel *timeLBL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLBLHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraits;





@end
