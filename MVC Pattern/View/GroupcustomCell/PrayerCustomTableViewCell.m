//
//  PrayerCustomTableViewCell.m
//  wireFrameSplash
//
//  Created by home on 6/23/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "PrayerCustomTableViewCell.h"

@implementation PrayerCustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _pryerBTN.layer.cornerRadius = 5;
    _pryerBTN.clipsToBounds = YES;
    _commentBTN.layer.cornerRadius = 5;
    _commentBTN.clipsToBounds = YES;
    
    self.imageview.layer.borderColor=[UIColor colorWithRed:59.0/255.0f green:147.0/255.0f  blue:192.0/255.0f  alpha:1.0f].CGColor;
    self.imageview.layer.cornerRadius=5.0f;
    self.imageview.layer.borderWidth=1.0f;
    self.imageview.clipsToBounds = YES;
    
    self.contentView.backgroundColor=[UIColor clearColor];
    self.PrayerContentView.layer.borderWidth = 1.2;
    self.PrayerContentView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.PrayerContentView.layer.cornerRadius=5.0f;
    self.PrayerContentView.layer.masksToBounds = YES;
    self.commentPrayerView.layer.borderWidth = 1.2;
    self.commentPrayerView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.commentPrayerView.layer.masksToBounds = YES;
    
    //    self.PrayerContentView.layer.borderColor = [UIColor colorWithRed:216/255.0f green:214.0/255.0f  blue:214/255.0f  alpha:1.0f].CGColor;
    // self.PrayerContentView.layer.cornerRadius = 6.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
