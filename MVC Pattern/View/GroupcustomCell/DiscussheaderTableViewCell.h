//
//  DiscussheaderTableViewCell.h
//  wireFrameSplash
//
//  Created by home on 6/30/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLabel.h"

@interface DiscussheaderTableViewCell : UITableViewCell
@property(weak,nonatomic)IBOutlet UILabel *discussionLBL;

//@property(weak,nonatomic)IBOutlet UITextView *textDiscussLBL;

@property(weak,nonatomic)IBOutlet UILabel *textDiscussLBL;

@property(weak,nonatomic)IBOutlet UILabel *praisedLikeLBL;
@property(weak,nonatomic)IBOutlet UILabel *namediscuss2LBL;
@property(weak,nonatomic)IBOutlet UILabel *singleMotherLBL;
@property(weak,nonatomic)IBOutlet UILabel *dataLabel;
@property(weak,nonatomic)IBOutlet UIButton *praisedBtn;
@property(weak,nonatomic)IBOutlet UIButton *commentBtn;
@property(weak,nonatomic)IBOutlet UIImageView *attachementImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discuss2textLBL;
@property(weak,nonatomic)IBOutlet UIImageView  *headerImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *disheaderheight;
@property(weak,nonatomic)IBOutlet UIButton *heartBTN;
@property(weak,nonatomic)IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTextconstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heartimageWidthConstant;
@property(weak,nonatomic)IBOutlet UIImageView *influencerimage;
@property(weak,nonatomic)IBOutlet PPLabel *labelPP;
@property(weak,nonatomic)IBOutlet UILabel *nameLBL;
@property(weak,nonatomic)IBOutlet UILabel *timeLBL;




@end
