//
//  PraiseTableViewCell.h
//  wireFrameSplash
//
//  Created by webwerks on 6/21/17.
//  Copyright © 2017 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PraiseTableViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel *praiseLBL;
@property(nonatomic,weak)IBOutlet UILabel *commentLBL;

@end
