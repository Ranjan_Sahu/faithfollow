//
//  friendProfilrTableViewCell.h
//  wireFrameSplash
//
//  Created by Vikas on 09/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface friendProfilrTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageview_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UIButton *btn_addFriend;
@property (weak, nonatomic) IBOutlet UIButton *btn_cancelfriend;

@end
