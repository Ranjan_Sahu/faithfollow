//
//  MysideTableViewCell.h
//  wireFrameSplash
//
//  Created by webwerks on 11/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MysideTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelLink1;
@property (weak, nonatomic) IBOutlet UIButton *buttonLink1;
@property (weak, nonatomic) IBOutlet UILabel *labelLink2;
@property (weak, nonatomic) IBOutlet UIButton *buttonLink2;
@property (weak, nonatomic) IBOutlet UILabel *labelLink3;
@property (weak, nonatomic) IBOutlet UIButton *buttonLink3;
@property (weak, nonatomic) IBOutlet UILabel *lableLink4;
@property (weak, nonatomic) IBOutlet UIButton *buttonLink4;

@end
