
#import "customCellTableViewCell.h"
#import "CustomCollectionViewCell.h"
#import "UIImageView+WebCache.h"

@interface customCellTableViewCell()

@property(nonatomic,strong)NSArray *arrData;

@end

@implementation customCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
-(void)setData:(NSArray *)array
{
    [self layoutSubviews];
    
    self.collectionView.dataSource=self;
    self.collectionView.delegate=self;
    self.collectionView.pagingEnabled = TRUE;
    self.collectionView.showsHorizontalScrollIndicator = FALSE;
    [self.collectionView registerNib:[UINib nibWithNibName:@"CustomCollectionViewCell" bundle:nil]forCellWithReuseIdentifier:@"CustomCollectionViewCell"];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.arrData = array;
//    NSLog(@"array is %@",self.arrData);
    [self.collectionView reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView;
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier1 = @"CustomCollectionViewCell";
    CustomCollectionViewCell *cell = (CustomCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier1 forIndexPath:indexPath];
//    NSLog(@"array is %@",_arrData);
    NSDictionary *dictcollection;
    dictcollection=[self.arrData objectAtIndex:indexPath.row];
    cell.lblAppName.text = [NSString stringWithFormat:@"%@",[dictcollection valueForKey:@"caption"]];
    NSString *str=[NSString stringWithFormat:@"%@%@",ImageapiUrl,[dictcollection valueForKey:@"img_path"]];
    NSURL *imageURL = [NSURL URLWithString:str];
    [cell.imgView    sd_setImageWithURL:imageURL placeholderImage:[self imageFromColorDefault:[UIColor whiteColor]] completed:nil];
    
    return  cell;
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
  
    NSDictionary *theInfo =[NSDictionary dictionaryWithObjectsAndKeys:self.arrData,@"myArray", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"influencerPhoto" object:nil userInfo:theInfo];
  }

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake(80,80);
//}

- (UIImage *)imageFromColorDefault:(UIColor *)color {
    
    UIImage *image=[UIImage imageNamed:@"user"];
    return image;
}


@end
