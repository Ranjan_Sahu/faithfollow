//
//  customCellTableViewCell.h
//  DemoAppStore
//
//  Created by user on 28/09/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customCellTableViewCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property(nonatomic,weak)IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UICollectionView *collectionView;

-(void)setData:(NSArray *)array;

@property (weak, nonatomic) IBOutlet UILabel *NodataLBL;
@end
