//
//  CustomCollectionViewCell.h
//  DemoAppStore
//
//  Created by user on 28/09/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewCell : UICollectionViewCell
@property(nonatomic,weak)IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblAppName;
@property (weak, nonatomic) IBOutlet UIView *collectionCustomView;




@end
