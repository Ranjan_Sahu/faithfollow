//
//  AboutMeTableViewCell.h
//  wireFrameSplash
//
//  Created by webwerks on 11/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutMeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@end
