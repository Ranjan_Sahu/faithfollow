//
//  AboutMeTableViewCell.m
//  wireFrameSplash
//
//  Created by webwerks on 11/22/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "AboutMeTableViewCell.h"

@implementation AboutMeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.lblContent.text = @"";
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
