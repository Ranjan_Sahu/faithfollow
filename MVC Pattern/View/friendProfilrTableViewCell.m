//
//  friendProfilrTableViewCell.m
//  wireFrameSplash
//
//  Created by Vikas on 09/05/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "friendProfilrTableViewCell.h"

@implementation friendProfilrTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.imageview_profile.layer.borderColor=[UIColor colorWithRed:59.0/255.0f green:147.0/255.0f  blue:192.0/255.0f  alpha:1.0f].CGColor;
    self.imageview_profile.layer.cornerRadius=5.0f;
    self.imageview_profile.layer.borderWidth=1.0f;
     self.imageview_profile.clipsToBounds = YES;
    
    
    _btn_addFriend.layer.borderWidth=0.5f;
    _btn_addFriend.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _btn_addFriend.layer.cornerRadius=4.0f;
    
    _btn_cancelfriend.layer.borderWidth=0.5f;
    _btn_cancelfriend.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _btn_cancelfriend.layer.cornerRadius=4.0f;
    _btn_cancelfriend.backgroundColor=[UIColor whiteColor];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
