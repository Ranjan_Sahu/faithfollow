//
//  FeedTableViewCell.m
//  wireFrameSplash
//
//  Created by Vikas on 21/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "FeedTableViewCell.h"

@implementation FeedTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _mainView.layer.borderWidth=1.0f;
    _mainView.layer.cornerRadius=8.0;
    _mainView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    //_comment_btnView.layer.borderWidth=0.5f;
    _comment_btnView.layer.borderColor=[UIColor grayColor].CGColor;
    

    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {


    [super layoutSubviews];
    [self.contentView layoutIfNeeded];
    self.com_lbl.preferredMaxLayoutWidth=self.com_lbl.frame.size.width;
       self.personName_lbl.preferredMaxLayoutWidth=self.personName_lbl.frame.size.width;
      self.date_lbl.preferredMaxLayoutWidth=self.date_lbl.frame.size.width;
    self.praise_lbl.preferredMaxLayoutWidth=self.praise_lbl.frame.size.width;
      self.totalcomments_lbl.preferredMaxLayoutWidth=self.totalcomments_lbl.frame.size.width;

}
@end
