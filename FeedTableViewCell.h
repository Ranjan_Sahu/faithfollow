//
//  FeedTableViewCell.h
//  wireFrameSplash
//
//  Created by Vikas on 21/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UILabel *personName_lbl;
@property (strong, nonatomic) IBOutlet UILabel *date_lbl;
@property (strong, nonatomic) IBOutlet UIImageView *person_imageview;

@property (strong, nonatomic) IBOutlet UITextView *about_txtView;

@property (strong, nonatomic) IBOutlet UILabel *praise_lbl;
@property (strong, nonatomic) IBOutlet UILabel *totalcomments_lbl;
@property (strong, nonatomic) IBOutlet UIButton *praised_btn;

@property (strong, nonatomic) IBOutlet UIButton *comments_btn;
@property (strong, nonatomic) IBOutlet UIButton *moreComments_btn;
@property (strong, nonatomic) IBOutlet UITextView *writeComments_txtview;
@property (strong, nonatomic) IBOutlet UIButton *send_btn;
@property (strong, nonatomic) IBOutlet UIView *comment_btnView;
@property (strong, nonatomic) IBOutlet UILabel *com_lbl;


@end
