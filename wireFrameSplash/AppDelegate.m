
//
//  AppDelegate.m
//  wireFrameSplash
//
//  Created by home on 4/1/16.
//  Copyright © 2016 home. All rights reserved.
//
#import "Branch.h"
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"
#import "AppDelegate.h"
#import "InviteFriends.h"
#import "YahooContactViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "CreateCommunityViewController.h"
#import "HomeViewController.h"
#import "MHTabBarController.h"
#import <GoogleMobileAds/GADBannerView.h>
#import "FeedViewController.h"
#import "HomeViewController.h"
#import "CustomPagerViewController.h"
#import "PagerViewController.h"
#import "TenjinSDK.h"
#import "Firebase.h"
#import "SignupViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "InviteFriends.h"
#import "RightMenuViewController.h"
#import "SlideNavigationController.h"
#import "iRate.h"
#import <AdSupport/ASIdentifierManager.h>
#import "ProfileFriendViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "WebServiceHelper.h"
#import "Webservice_Constant.h"
#import "UIView+Toast.h"
#import "Constant1.h"
#import "InfluencerProfileController.h"
#import "SeeMoreCommentsViewController.h"
#import "ACTReporter.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define TimeStamp [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()<GADBannerViewDelegate>{
    NSArray *parsedContactList;
    UIStoryboard *storyboard;
    int backgroundCount;
    UIView *backgroundView;
    GADBannerView  *bottomAbMob;
    NSString *checkfb;

    NSMutableArray *messageArray;
    UIView *overlayInstructionView;
    NSString *typeoverlayScreen;

}

@property (strong) AVAudioPlayer *audioPlayer;
@end

@implementation AppDelegate
@synthesize isReachable,showrateView,checkClass,checkDisapper,checkSuggetedProfile;
@synthesize mxPanel,deviceTokenMix,postimagecheckNotnull,isNotPrayedLongTime,seeMorecommnetNotnull,groupMorenotnull;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        // FB updated by raju 23/1/19 Native FB behaviour
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else {
        if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
            [application registerUserNotificationSettings:settings];
            [application registerForRemoteNotifications];
        }
    }
    
    // Enable automated usage reporting.
    [ACTConversionReporter reportWithConversionID:@"880587237" label:@"kj2rCIPPv3AQ5ePyowM" value:@"0.00" isRepeatable:NO];
    self.isNotPrayedLongTime=NO;
    _notfoundadsArray=[[NSMutableArray alloc]init];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAppAlreadyLaunchedOnce"];

    // mix panel token
    //    //    live mix panel token   02c7aa898402a244c0a14124b53830a8
    [Mixpanel sharedInstanceWithToken:mixPanelTokenLive];
    mxPanel=[Mixpanel sharedInstance];
    [mxPanel identify:mxPanel.distinctId];
    //beta 7b17e248c635fd1449145d29b0fc7b94
    NSString *currentDate=[self createTimeStamp];
    [mxPanel registerSuperProperties:@{@"Last Page Load": currentDate}];
    [mxPanel.people set:@{@"Last Page Load": currentDate}];
    [mxPanel track:@"Page loaded"
        properties:@{ @"Page name":@"Homepage",@"Loaded Time":currentDate,@"Last Page Load": currentDate, }];
  //UAirship
    
    [UAirship takeOff];
    backgroundCount=0;
    [UAirship push].userNotificationTypes = (UIUserNotificationTypeAlert |
                                            UIUserNotificationTypeBadge |
                                            UIUserNotificationTypeSound);
       [UAirship push].userPushNotificationsEnabled = YES;

   // branch Handler
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        if (!error && params) {
            NSLog(@"deepLinkIOS %@",params);
            NSString *tabNumber=[NSString stringWithFormat:@"%@",[params objectForKey:@"tab"]];
            if(![tabNumber isEqualToString:@"(null)"] ){
                
                if([tabNumber isEqualToString:@"feeds"]){
                    NSDictionary *dict=@{
                                         @"tabScreen":@"0"
                                         };
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"tabdeepLinking" object:nil userInfo:dict];
                }
                else if ([tabNumber isEqualToString:@"influencers"]){
                    NSDictionary *dict=@{
                                         @"tabScreen":@"1"
                                         };
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"tabdeepLinking" object:nil userInfo:dict];
                }
                else if ([tabNumber isEqualToString:@"groups"]){
                    NSDictionary *dict=@{
                                         @"tabScreen":@"3"
                                         };
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"tabdeepLinking" object:nil userInfo:dict];
                }
                else if ([tabNumber isEqualToString:@"communities"]){
                    NSDictionary *dict=@{
                                         @"tabScreen":@"4"
                                         };
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"tabdeepLinking" object:nil userInfo:dict];
                }
                else{
                    NSDictionary *dict=@{
                                         @"tabScreen":@"2"
                                         };
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"tabdeepLinking" object:nil userInfo:dict];
                }
                
            }
            
            NSString *friendID=[NSString stringWithFormat:@"%@",[params objectForKey:@"friend_id"]];
            friendID=[self isStrEmpty:friendID];
            NSString *ref_idData=[NSString stringWithFormat:@"%@",[params objectForKey:@"post_id"]];
            ref_idData=[self isStrEmpty:ref_idData];
            if ([ref_idData length]>0) {
                [AppDelegate sharedAppDelegate].ref_idData=ref_idData;
                [AppDelegate sharedAppDelegate].isDeeplinkPost=YES;
                UINavigationController *navController =(UINavigationController*) self.window.rootViewController;
                if(navController)
                {
                    [self seemoreCommentView];
                }
            }
            else if ([friendID length]>0){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [AppDelegate sharedAppDelegate].influencerId=friendID;
                    [AppDelegate sharedAppDelegate].isDeeplink=YES;
                   UINavigationController *navController =(UINavigationController*) self.window.rootViewController;
                    if(navController)
                   {
                        [self viewFrndProfile:friendID];
                   }
    
                });
            }
        }
   }];
    
    // issue for device token
    
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [FIRApp configure];
        });
    [Fabric with:@[[Crashlytics class]]];
    showrateView=NO;
    [[FBSDKApplicationDelegate sharedInstance] application:application
                            didFinishLaunchingWithOptions:launchOptions];
    [FBSDKLoginButton class];
    [iRate sharedInstance].applicationBundleID = @"com.brain.socialapp";
    [iRate sharedInstance].onlyPromptIfLatestVersion=NO;
    [iRate sharedInstance].eventCount = 0;
    [iRate sharedInstance].eventsUntilPrompt=3;
    [iRate sharedInstance].daysUntilPrompt=0;
    [iRate sharedInstance].previewMode=NO;
    
    [self logUser];
    NSUUID *adId = [[ASIdentifierManager sharedManager] advertisingIdentifier];
        [[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled];
        NSString *str = [adId UUIDString];
       [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"adID"];
       [[NSUserDefaults standardUserDefaults] synchronize];
       [TenjinSDK sharedInstanceWithToken:@"DNEWDXIVZ7FZSIVXXTGMCNVHQZ4WPSWD"];
    [self configureReachability];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
   UINavigationController *navController=(UINavigationController*)self.window.rootViewController;
   //1AA8EA
    navController.navigationBar.barTintColor=[self colorFromHexString:@"1AA8EA"];    navController.navigationBar.tintColor=[UIColor whiteColor  ];
    [navController.navigationBar
    setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor  ]}];
    
        NSString * storyboardName = @"Main";
    storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"Login"] isEqualToString:@"yes"]) {
    
        //        [self notification_alert];
       MHTabBarController*   otherviewController = [storyboard instantiateViewControllerWithIdentifier:@"MHTabBarController"];
        SlideNavigationController * slideNavCtl = [[SlideNavigationController alloc] initWithRootViewController:otherviewController];
       slideNavCtl.navigationBar.tintColor=[UIColor blueColor];
        RightMenuViewController *rightMenu =[storyboard instantiateViewControllerWithIdentifier:@"RightMenuViewController"];
        slideNavCtl.rightMenu = rightMenu;
        slideNavCtl.menuRevealAnimationDuration = .18;
       [self.window setRootViewController:slideNavCtl];
    
        [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidClose object:nil queue:nil usingBlock:^(NSNotification *note) {
            //            NSLog(@"Closed %@", menu);
        }];
    
        [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidOpen object:nil queue:nil usingBlock:^(NSNotification *note) {
        }];
    
        [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidReveal object:nil queue:nil usingBlock:^(NSNotification *note) {
        }];
    
   }else{
    
        SignupViewController *controler=[storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controler];
       self.window.rootViewController = navigationController;
  }
    
   

    
    return YES;
}


-(NSString*)createTimeStamp {
//    UTC Date 
    NSDate *today = [NSDate dateWithTimeIntervalSinceNow:0];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *dataFormatStr=[NSString stringWithFormat:@"yyyy/MM/dd HH:mm:ss a"];
    [dateFormat setDateFormat:dataFormatStr];
    NSString *dateString = [dateFormat stringFromDate:today];
    NSString *timestamp = [NSString stringWithFormat:@"%@",dateString];
    return timestamp;
}







#pragma mark seemoreCommentView(Branch)Method

-(void)seemoreCommentView {
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"]==nil ||[[[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"] isKindOfClass:[NSNull class]])
    {
        return;
    }
    UINavigationController *navController =(UINavigationController*) self.window.rootViewController;
    UIStoryboard *storyboardObj = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    SeeMoreCommentsViewController *allprofileView=[storyboardObj instantiateViewControllerWithIdentifier:@"SeeMoreCommentsViewController"];
    [navController pushViewController:allprofileView animated:YES];
    return;
}

#pragma mark viewFrndProfileBranchMethod

-(void)viewFrndProfile :(NSString*)friend_id{
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"]==nil ||[[[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"] isKindOfClass:[NSNull class]])
    {
        return;
    }
    UINavigationController *navController =(UINavigationController*) self.window.rootViewController;
    UIStoryboard *storyboardObj = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    InfluencerProfileController *allprofileView=[storyboardObj instantiateViewControllerWithIdentifier:@"InfluencerProfileController"];
    [navController pushViewController:allprofileView animated:YES];
    return;

}

#pragma mark - To Check String is Empty
-(NSString *)isStrEmpty:(NSString *) str {
    
    if([str isKindOfClass:[NSNumber class]])
    {
        return str;
    }
    if([str isKindOfClass:[NSNull class]] || str==nil)
    {
        return @"";
    }
    if([str length] == 0) {
        
        return @"";
    }
    if(![[str stringByTrimmingCharactersInSet:[NSCharacterSet
                                               whitespaceAndNewlineCharacterSet]] length]) {
        return @"";
    }
    if([str isEqualToString:@"(null)"])
    {
        return @"";
    }
    if([str isEqualToString:@"<null>"])
    {
        return @"";
    }
    return str;
}



- (void) logUser {
    [CrashlyticsKit setUserEmail:@"rajukumar.sharma@wwindia.com"];
}

+ (void)initialize
{
    //configure iRate
    [iRate sharedInstance].appStoreID = 1137673443;
}


-(void)application:(UIApplication *)app didReceiveRemoteNotification:(NSDictionary *)userInfo
{
//    NSString *message = [[userInfo objectForKey:@"aps"]
//                         objectForKey:@"alert"];
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle:@""
//                          message:message
//                          delegate:nil
//                          cancelButtonTitle:@"OK"
//                          otherButtonTitles:nil];
//    [alert show];
    
}



#pragma mark SetRootsMethod

- (void)setRoots
{
    MHTabBarController*   otherviewController = [storyboard instantiateViewControllerWithIdentifier:@"MHTabBarController"];
    SlideNavigationController *singletonInstance = [[SlideNavigationController alloc] initWithRootViewController:otherviewController];
    singletonInstance.navigationBar.tintColor=[UIColor blueColor];
    RightMenuViewController *rightMenu =[storyboard instantiateViewControllerWithIdentifier:@"RightMenuViewController"];
    singletonInstance.rightMenu = rightMenu;
    singletonInstance.menuRevealAnimationDuration = .18;
    [self.window setRootViewController:singletonInstance];
    
}
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"apnsToken"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"apnsTokenSentSuccessfully"];
    [[NSUserDefaults standardUserDefaults] synchronize];
//    
//           UIAlertView *alert = [[UIAlertView alloc]
//                              initWithTitle:@""
//                              message:token
//                              delegate:nil
//                              cancelButtonTitle:@"OK"
//                              otherButtonTitles:nil];
//        [alert show];
    
//    [AppDelegate sharedAppDelegate].tokendata=deviceToken;
//    NSDictionary* userInfo = @{@"tokenDevice": token
//                               };
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceToken" object:nil userInfo:userInfo];
    NSUserDefaults *prefs   = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedObject = [prefs objectForKey:@"mixdeviceToken"];
    if (!myEncodedObject || myEncodedObject==nil) {
        [prefs setObject:deviceToken forKey:@"mixdeviceToken"];
        [prefs synchronize];
    }
    NSLog(@"***device token is %@",token);
 
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"push error %@",err.description);
    
#if TARGET_IPHONE_SIMULATOR
//    NSLog(@"%@ push registration error is expected on simulator", self);
#else
    NSLog(@"%@ push registration error: %@", self, err);
#endif
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    
    application.applicationIconBadgeNumber = 0;
    
    // checking view present view classs
    
    UINavigationController *navController =(UINavigationController*) self.window.rootViewController;
    MHTabBarController *tab=(MHTabBarController *)[navController.viewControllers objectAtIndex:0];
    
    if (backgroundCount%2==0 && backgroundCount!=0) {
        _showsuggestedGroup=YES;
    }
    else{
        _showsuggestedGroup=NO;
    }
    
    if (backgroundCount%3==0 && backgroundCount!=0) {
        showrateView=YES;
        checkDisapper=YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            if([tab isKindOfClass:[SignupViewController class]] || [tab isKindOfClass:[CustomPagerViewController class]] || [tab isKindOfClass:[PagerViewController class]])
            {
                backgroundCount=0;
            }
            else{
                [self addsView];
            }
        });
        
    }
    else{
        [self hideaddView];
        [self removeadd];
        
    }
    // add to hide ad in profileFriendcontroller
    if([tab isKindOfClass:[MHTabBarController class]])
    {
        UIViewController *obj=[tab selectedViewController];
        if([obj isKindOfClass:[SignupViewController class]] || [obj isKindOfClass:[CustomPagerViewController class]]||[obj isKindOfClass:[PagerViewController class]]||[obj  isKindOfClass:[InviteFriends class]] )
        {
            [self hideaddView];
        }
        else
        {
            [self unhideView];
        }
    }
    else if([tab isKindOfClass:[SignupViewController class]] || [tab isKindOfClass:[CustomPagerViewController class]] || [tab isKindOfClass:[PagerViewController class]] )
    {
        backgroundCount=0;
        [self removeadd];
    }
    
    backgroundCount=  backgroundCount+1;
    [iRate sharedInstance].promptAtLaunch = FALSE;
    
    
}

+ (AppDelegate *)sharedAppDelegate{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

-(void)addsView{
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gallery"]isEqualToString:@"yes" ]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"gallery"];
        return;
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"camara"]isEqualToString:@"yes" ]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"camara"];
        return;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"ProfileScreen"]isEqualToString:@"yes" ]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ProfileScreen"];
        return;
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"freindProfile"]isEqualToString:@"Yes" ]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"freindProfile"];
        return;
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"StatusScreen"]isEqualToString:@"Yes" ]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"StatusScreen"];
        return;
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"GroupDetail2"]isEqualToString:@"Yes" ]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"GroupDetail2"];
        return;
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"SeeMoreComment"]isEqualToString:@"Yes" ]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SeeMoreComment"];
        return;
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"newupdateclass"]isEqualToString:@"Yes" ]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"newupdateclass"];
        return;
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"Frindlist"]isEqualToString:@"Yes" ]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Frindlist"];
        return;
    }
    
    if(bottomAbMob)
    {
        [bottomAbMob removeFromSuperview];
        bottomAbMob=nil;
    }
    
    UINavigationController *navController =(UINavigationController*) self.window.rootViewController;
    UIViewController *visibleScreen=[navController visibleViewController];
    bottomAbMob = [[GADBannerView alloc]
                   init];
    bottomAbMob.adUnitID =kBannerAdUnitID;
    bottomAbMob.rootViewController =visibleScreen ;
    CGFloat x_admob;
    x_admob =0;
    if ([UIApplication sharedApplication].keyWindow.frame.size.width>320) {
        x_admob =(([UIApplication sharedApplication].keyWindow.frame.size.width)-320)/2;
    }
    bottomAbMob.frame=CGRectMake(x_admob,[UIApplication sharedApplication].keyWindow.frame.size.height-50,320,50);
    GADRequest *request= [[GADRequest alloc] init];
    request.testDevices = @[ @"42f31902adddbb070ac0716b1d3602d5",kGADSimulatorID];
    [bottomAbMob loadRequest:request];
    [[UIApplication sharedApplication].keyWindow addSubview:bottomAbMob];
    
    // DFP implemented here
    
    
//    if(bottomAbMob)
//    {
//        [bottomAbMob removeFromSuperview];
//        bottomAbMob=nil;
//    }
//    
//    UINavigationController *navController =(UINavigationController*) self.window.rootViewController;
//    UIViewController *visibleScreen=[navController visibleViewController];
//    
//    bottomAbMob = [[DFPBannerView alloc]
//                   init];
//    bottomAbMob.delegate = self;
//    bottomAbMob.adUnitID =kBannerAdUnitID;
//    bottomAbMob.rootViewController =visibleScreen ;
//    CGFloat x_admob;
//    x_admob =0;
//    if ([UIApplication sharedApplication].keyWindow.frame.size.width>320) {
//        x_admob =(([UIApplication sharedApplication].keyWindow.frame.size.width)-320)/2;
//    }
//    bottomAbMob.frame=CGRectMake(x_admob,[UIApplication sharedApplication].keyWindow.frame.size.height-50,320,50);
//    GADRequest *request= [[GADRequest alloc] init];
////    request.testDevices = @[ @"42f31902adddbb070ac0716b1d3602d5",kGADSimulatorID];
//    [bottomAbMob loadRequest:request];
////    [bottomAbMob loadRequest:[DFPRequest request]];
//    [[UIApplication sharedApplication].keyWindow addSubview:bottomAbMob];
}




-(void)hideaddView{
    
    if(bottomAbMob)
    { bottomAbMob.hidden=YES;
    }
    
}
-(void)unhideView{
    if(bottomAbMob)
    { bottomAbMob.hidden=NO;
    }
    
}

-(void)removeadd{
    if(bottomAbMob)
    {   [bottomAbMob removeFromSuperview];
        bottomAbMob=nil;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// Respond to Universal Links

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
    BOOL handledByBranch = [[Branch getInstance] continueUserActivity:userActivity];
    if (userActivity.activityType == NSUserActivityTypeBrowsingWeb) {
        [ACTConversionReporter reportUniversalLinkWithUserActivity:userActivity];
        return YES;
    }
    return NO;
    
    return handledByBranch;
}




- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL handled;
    if ([[FBSDKApplicationDelegate sharedInstance] application:application
                                                       openURL:url
                                             sourceApplication:sourceApplication
                                                    annotation:annotation
         ])
    {
        handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                 openURL:url
                                                       sourceApplication:sourceApplication
                                                              annotation:annotation
                   ];
    }
    else{
        handled=[[Branch getInstance] handleDeepLink:url];
    }
    return handled;
    // Add any custom logic here.
}




#pragma mark - Reachability

- (void)configureReachability
{
    // To check the reachability for the Internet Connection
    //--- register notification to handle network change -----
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    
    if ([self.reachability currentReachabilityStatus] == NotReachable)
    {
        self.isReachable = NO;
    }
    else
    {
        self.isReachable = YES;
    }
}

- (void)handleNetworkChange:(NSNotification *)notice {
    //--- set the isReachable flag to YES or NO when network changes ---
    if ([self.reachability currentReachabilityStatus] == NotReachable) {
        self.isReachable = NO;
    }
    else if ([self.reachability currentReachabilityStatus] == ReachableViaWiFi) {
        self.isReachable = YES;
    }
    else if ([self.reachability currentReachabilityStatus] == ReachableViaWWAN) {
        self.isReachable = YES;
    }
}
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}

-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


#pragma mark -notification
-(void) notification_alert{
    [NSTimer scheduledTimerWithTimeInterval:20.0
                                     target:self
                                   selector:@selector(callnotification)
                                   userInfo:nil
                                    repeats:YES];
}



-(void)callnotification{
    
    AppDelegate *appDelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    if (!appDelegate.isReachable) {
        return;
    }
    if (isReachable) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSDictionary *dict = @{
                                   @"user_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],
                                   @"token" : [[NSUserDefaults standardUserDefaults]objectForKey:@"api_token"],
                                   @"timestamp" :[[NSUserDefaults standardUserDefaults]objectForKey:@"timeStamp"]};
            [[WebServiceHelper sharedInstance] call_notificationPostDataWithMethod:getpostcount withParameters:dict withHud:NO success:^(id response){
                //                NSLog(@" notification resposnse is%@",response);
                if ([response isKindOfClass:[NSDictionary class]]){
                    if([response[@"status"]intValue ] ==1){
                        NSDictionary *notificaion_count=[response valueForKey:@"notificaion_count"];
                        NSDictionary *post_count=[response valueForKey:@"post_count"];
                        NSString *notifi_count,*post_cnt;
                        if (notificaion_count.count==0) {
                        }else{
                            notifi_count=notificaion_count[@"notification_count"];
                        }
                        if (post_count.count==0) {
                            
                            post_cnt=@"0";
                            
                        }else{
                            post_cnt=post_count[@"post_count"];
                        }
                        
                        NSDictionary *userinfo=@{@"notification_count":notifi_count,
                                                 @"post_count":post_cnt,
                                                 };
                        [[NSNotificationCenter defaultCenter] postNotificationName: @"TestNotification" object:nil userInfo:userinfo];
                        
                    }else{
                    }
                }
            } errorBlock:^(id error)
             {
                 
             }];});
    }
}

#pragma mark ShowhudMethod

-(void)showhud{
    if (backgroundView) {
        [backgroundView.layer removeAllAnimations];
        [backgroundView removeFromSuperview];
        backgroundView=nil;
    }
    
    CGRect  screenRect = [[UIScreen mainScreen] bounds];
    backgroundView=[[UIView alloc]init];
    backgroundView.frame=screenRect;
    backgroundView.backgroundColor= [UIColor colorWithRed:(0.0/255.0f) green:(0.0/255.0f) blue:(0.0/255.0f) alpha:0.5];
    
    UIImageView *ima=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"indicator_logo"]];
    ima.frame=CGRectMake(0, 0, 40, 40);
    UIView *vi=[[UIView alloc]initWithFrame:CGRectMake((screenRect.size.width/2)-20, screenRect.size.height/2, 40, 40)];
    vi.backgroundColor=[UIColor clearColor];
    [vi addSubview:ima];
    
    //  hudbackground
    // backgroundView.backgroundColor=[UIColor blackColor];
    backgroundView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"hudbackground"]];
    [backgroundView addSubview:vi];
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ *4 * 60.0 ];
    rotationAnimation.duration = 60.0;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 3;
    [vi.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    [[UIApplication sharedApplication].keyWindow addSubview:backgroundView];
}
#pragma mark HidehudMethod

-(void)hidehud{
    [backgroundView.layer removeAllAnimations];
    [backgroundView removeFromSuperview];
}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    //Called when a notification is delivered to a foreground app.
    
    NSLog(@"Userinfo %@",notification.request.content.userInfo);
    
    completionHandler(UNNotificationPresentationOptionAlert);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    //Called to let your app know which action was selected by the user for a given notification.
    
    NSLog(@"Userinfo %@",response.notification.request.content.userInfo);
    
}

-(void)showInstructionOverlay:(NSString *) typeoverlayScreen
{
    if(overlayInstructionView)
    {
        [overlayInstructionView removeFromSuperview];
        overlayInstructionView=nil;
    }
    _notificationcheckPray=typeoverlayScreen;
    overlayInstructionView = [[UIView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.frame];
    overlayInstructionView.backgroundColor = [UIColor clearColor];
    overlayInstructionView.userInteractionEnabled=YES;
    UIImageView *overlayImageView = [[UIImageView alloc]initWithFrame:overlayInstructionView.frame];
    overlayImageView.alpha = 0.95;
    
    if ([typeoverlayScreen isEqualToString:@"Pray"]) {
        overlayImageView.image = [UIImage imageNamed:@"MOCKUP_IPHONE4_640X960"];
    }
    else{
        overlayImageView.image = [UIImage imageNamed:@"sampleOverlay"];
  
    }
    
    UITapGestureRecognizer *tapGuesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeOverlayImage)];
    tapGuesture.numberOfTapsRequired = 1;
    [overlayInstructionView addGestureRecognizer:tapGuesture];
    [overlayInstructionView addSubview:overlayImageView];
    [[UIApplication sharedApplication].keyWindow addSubview:overlayInstructionView];
}
-(void)removeOverlayImage{
    if(overlayInstructionView)
    {
        [overlayInstructionView removeFromSuperview];
        overlayInstructionView=nil;
    }
    if ([_notificationcheckPray isEqualToString:@"Pray"]) {
        NSDictionary *dict=@{
                             @"feedScreen":@"0"
                             };
        [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationtoFeed" object:nil userInfo:dict];
    }
    
    
    
}

@end
