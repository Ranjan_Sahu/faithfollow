//
//  ViewController.h
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface mycommunityViewcontroller : UIViewController<UIPageViewControllerDataSource>
- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

- (IBAction)search_act:(id)sender;

- (IBAction)mycommunity_act:(id)sender;
- (IBAction)freiend_act:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *mycommunity_btn;
@property (weak, nonatomic) IBOutlet UIButton *search_btn;
@property (weak, nonatomic) IBOutlet UIButton *friendrequest_btn;

@property (weak, nonatomic) IBOutlet UIView *bckView1;
@property (weak, nonatomic) IBOutlet UIView *bckVIew2;
@property (weak, nonatomic) IBOutlet UIView *bckview3;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topconstraint;

@end
