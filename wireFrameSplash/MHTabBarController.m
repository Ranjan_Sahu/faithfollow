/*
 * Copyright (c) 2011-2012 Matthijs Hollemans
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#import "MHTabBarController.h"
#import "FeedViewController.h"
#import "NotificationViewController.h"
#import "mycommunityViewcontroller.h"

#import "DeviceConstant.h"
#import "AppDelegate.h"
#import "MLKMenuPopover.h"
#import "Profile_editViewController.h"

//#define MENU_POPOVER_FRAME  CGRectMake(275, 50, 140, 88)
static const NSInteger TagOffset = 1000;


@interface MHTabBarController ()<MLKMenuPopoverDelegate>{
    CGRect MENU_POPOVER_FRAME;
    UIButton *feedFilter;
}
@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property(nonatomic,strong) NSArray *menuItems;
@end
@implementation MHTabBarController
{
	UIView *tabButtonsContainerView;
	UIView *contentContainerView;
	UIImageView *indicatorImageView;
    
     Profile_editViewController* listViewController4;
    UIButton *redviewbtn;
    UIView *popUpView;
    NSUInteger pagenumber;
}

- (void)viewDidLoad
{
	[super viewDidLoad];

    CGRect    screenRect = [[UIScreen mainScreen] bounds];
   
    FeedViewController *listViewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedViewController"];
    NotificationViewController *listViewController3= [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    mycommunityViewcontroller *listViewController2 = [self.storyboard instantiateViewControllerWithIdentifier:@"mycommunityViewcontroller"];
    listViewController4 = [self.storyboard instantiateViewControllerWithIdentifier:@"Profile_editViewController"];
    
    listViewController1.title = @"Feed";
    listViewController2.title = @"My Commmunity";
    listViewController3.title = @"Notification";
    listViewController4.title = @"Profile";
   
    listViewController1.tabBarItem.selectedImage = [UIImage imageNamed:@"feed_Selected"];
    listViewController1.tabBarItem.image = [UIImage imageNamed:@"feed_UnSelected"];

    
    listViewController2.tabBarItem.selectedImage = [UIImage imageNamed:@"mycommunity_Selected"];
    listViewController2.tabBarItem.image = [UIImage imageNamed:@"my_community_Unselected"];

    
    listViewController3.tabBarItem.selectedImage = [UIImage imageNamed:@"notification_Selected"];
    listViewController3.tabBarItem.image = [UIImage imageNamed:@"notification_Unselected"];

    
   listViewController4.tabBarItem.selectedImage = [UIImage imageNamed:@"profile_Selected"];
   listViewController4.tabBarItem.image = [UIImage imageNamed:@"profile_Unselected"];
   
   NSArray *viewControllers = @[listViewController1,listViewController2,listViewController3,listViewController4];
    self.viewControllers = viewControllers;
    if (IS_IPHONE4) {
        listViewController1.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController1.tabBarItem.titlePositionAdjustment = UIOffsetMake(-15.0f, 25.0f);
        listViewController2.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, 30.0f, 0.0f, 0.0f);
        listViewController2.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        listViewController3.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, 30.0f, 0.0f, 0.0f);
        listViewController3.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        listViewController4.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController4.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
    }else if(IS_IPHONE6PLUS){
        listViewController1.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController1.tabBarItem.titlePositionAdjustment = UIOffsetMake(-15.0f, 25.0f);
        listViewController2.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, 40.0f, 0.0f, 0.0f);
        listViewController2.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        listViewController3.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +40.0f, 0.0f, 0.0f);
        listViewController3.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        listViewController4.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController4.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
    }
    else if(IS_IPHONE5){
        listViewController1.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController1.tabBarItem.titlePositionAdjustment = UIOffsetMake(-15.0f, 30.0f);
        listViewController2.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, 30.0f, 0.0f, 0.0f);
        listViewController2.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        listViewController3.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController3.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        listViewController4.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController4.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
    }
    else if(IS_IPHONE6){
        listViewController1.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController1.tabBarItem.titlePositionAdjustment = UIOffsetMake(-15.0f, 30.0f);
        listViewController2.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, 35.0f, 0.0f, 0.0f);
        listViewController2.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        listViewController3.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +35.0f, 0.0f, 0.0f);
        listViewController3.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
        listViewController4.tabBarItem.imageInsets = UIEdgeInsetsMake(-10.0f, +30.0f, 0.0f, 0.0f);
        listViewController4.tabBarItem.titlePositionAdjustment = UIOffsetMake(-20.0f, 25.0f);
    }
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    CGRect rect = CGRectMake(0.0f, 22.0f, self.view.bounds.size.width, self.tabBarHeight);
	tabButtonsContainerView = [[UIView alloc] initWithFrame:rect];
	tabButtonsContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	[self.view addSubview:tabButtonsContainerView];

	rect.origin.y = self.tabBarHeight+22.0;
	rect.size.height = self.view.bounds.size.height -(self.tabBarHeight+22);
    
    contentContainerView = [[UIView alloc] initWithFrame:rect];
    contentContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:contentContainerView];
    indicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MHTabBarIndicator"]];
    [self.view addSubview:indicatorImageView];


    
    [self reloadTabButtons];
    MENU_POPOVER_FRAME=CGRectMake(screenRect.size.width-145, 56, 140, 130);
    
    self.menuItems = [NSArray arrayWithObjects:@"View All Updates", @"View Status Updates",@"View Prayer Requests", nil];

    feedFilter= [UIButton buttonWithType:UIButtonTypeCustom];
    [feedFilter addTarget:self
                   action:@selector(popOverMenu)
         forControlEvents:UIControlEventTouchUpInside];
    [feedFilter setImage:[UIImage imageNamed:@"dot"] forState:UIControlStateNormal];
    feedFilter.frame =CGRectMake(screenRect.size.width-30, contentContainerView.frame.origin.y-44, 30, 44);
    [self.view addSubview:feedFilter];
    feedFilter.hidden=NO;
    
    
    [UIApplication sharedApplication].statusBarHidden=NO;
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swiperight];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeleft];
    
    
    self.view.userInteractionEnabled=YES;
    //pagenumber=1000;
    
    pagenumber=0;
}


-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self.view endEditing:YES];
    // [self forwardPage];
    
    
    //Do what you want here
    /*if (pagenumber!=1000) {
        pagenumber--;
        [self setSelectedIndex:pagenumber animated:YES];
    }*/
    if (pagenumber!=0) {
         pagenumber--;
        [self setSelectedIndex:pagenumber animated:YES];
        
    }

}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self.view endEditing:YES];
   /* NSUInteger index;
    if (pagenumber!=1003) {

        if (pagenumber==1000) {
            index=0;
        }else if (pagenumber==1001){
            index=1;
        }else if (pagenumber==1002){
            index=2;
        }
        
            [self setSelectedIndex:index+1 animated:YES];
           pagenumber++;
    }*/
        if (pagenumber!=3) {
            pagenumber++;
            [self setSelectedIndex:pagenumber animated:YES];

        }

    NSLog(@"left");

    //   [self backPage];
}
-(void)popOverMenu{
 
       
    
    [self.menuPopover dismissMenuPopover];
    
    self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:MENU_POPOVER_FRAME menuItems:self.menuItems];
    
    self.menuPopover.menuPopoverDelegate = self;
    [self.menuPopover showInView:self.view];

}





#pragma mark -
#pragma mark MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex
{
    [self.menuPopover dismissMenuPopover];
    
    NSString *title = [NSString stringWithFormat:@"%@ selected.",[self.menuItems objectAtIndex:selectedIndex]];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alertView show];
}

- (void)viewWillLayoutSubviews
{
	[super viewWillLayoutSubviews];
	[self layoutTabButtons];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// Only rotate if all child view controllers agree on the new orientation.
	for (UIViewController *viewController in self.viewControllers)
	{
		if (![viewController shouldAutorotateToInterfaceOrientation:interfaceOrientation])
			return NO;
	}
	return YES;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];

	if ([self isViewLoaded] && self.view.window == nil)
	{
		self.view = nil;
		tabButtonsContainerView = nil;
		contentContainerView = nil;
		indicatorImageView = nil;
	}
}

- (void)reloadTabButtons
{
	[self removeTabButtons];
	[self addTabButtons];

	// Force redraw of the previously active tab.
	NSUInteger lastIndex = _selectedIndex;
	_selectedIndex = NSNotFound;
	self.selectedIndex = lastIndex;
}

- (void)addTabButtons
{
	NSUInteger index = 0;
	for (UIViewController *viewController in self.viewControllers)
	{
		UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
		button.tag = TagOffset + index;
		button.titleLabel.font = [UIFont boldSystemFontOfSize:9];
		button.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        button.layer.borderWidth=0.4f;
        button.layer.borderColor=[UIColor whiteColor].CGColor;
     //   button.titleLabel.textColor=[UIColor blackColor];

		UIOffset offset = viewController.tabBarItem.titlePositionAdjustment;
		button.titleEdgeInsets = UIEdgeInsetsMake(offset.vertical, offset.horizontal, 0.0f, 0.0f);
		button.imageEdgeInsets = viewController.tabBarItem.imageInsets;
		[button setTitle:viewController.tabBarItem.title forState:UIControlStateNormal];
		[button setImage:viewController.tabBarItem.image forState:UIControlStateNormal];
        //[button setImage:viewController.tabBarItem.selectedImage forState:UIControlStateHighlighted];
        
        [button setImage:viewController.tabBarItem.selectedImage forState:UIControlStateSelected];

		[button addTarget:self action:@selector(tabButtonPressed:) forControlEvents:UIControlEventTouchDown];

		[self deselectTabButton:button];

        
		[tabButtonsContainerView addSubview:button];

		++index;
	}
}

- (void)removeTabButtons
{
	while ([tabButtonsContainerView.subviews count] > 0)
	{
		[[tabButtonsContainerView.subviews lastObject] removeFromSuperview];
	}
}

- (void)layoutTabButtons
{
	NSUInteger index = 0;
	NSUInteger count = [self.viewControllers count];

	CGRect rect = CGRectMake(0.0f, 0.0f, floorf(self.view.bounds.size.width / count), self.tabBarHeight);

	indicatorImageView.hidden = YES;

	NSArray *buttons = [tabButtonsContainerView subviews];
	for (UIButton *button in buttons)
	{
		if (index == count - 1)
			rect.size.width = self.view.bounds.size.width - rect.origin.x;

		button.frame = rect;
		rect.origin.x += rect.size.width;

		if (index == self.selectedIndex)
			[self centerIndicatorOnButton:button];

		++index;
	}
}

- (void)centerIndicatorOnButton:(UIButton *)button
{
	CGRect rect = indicatorImageView.frame;
	rect.origin.x = button.center.x - floorf(indicatorImageView.frame.size.width/2.0f);
	rect.origin.y = (self.tabBarHeight+22.0) - indicatorImageView.frame.size.height;
	indicatorImageView.frame = rect;
	indicatorImageView.hidden = NO;
}

- (void)setViewControllers:(NSArray *)newViewControllers
{
	NSAssert([newViewControllers count] >= 2, @"MHTabBarController requires at least two view controllers");

	UIViewController *oldSelectedViewController = self.selectedViewController;

	// Remove the old child view controllers.
	for (UIViewController *viewController in _viewControllers)
	{
		[viewController willMoveToParentViewController:nil];
		[viewController removeFromParentViewController];
	}

	_viewControllers = [newViewControllers copy];

	// This follows the same rules as UITabBarController for trying to
	// re-select the previously selected view controller.
	NSUInteger newIndex = [_viewControllers indexOfObject:oldSelectedViewController];
	if (newIndex != NSNotFound)
		_selectedIndex = newIndex;
	else if (newIndex < [_viewControllers count])
		_selectedIndex = newIndex;
	else
		_selectedIndex = 0;

	// Add the new child view controllers.
	for (UIViewController *viewController in _viewControllers)
	{
		[self addChildViewController:viewController];
		[viewController didMoveToParentViewController:self];
	}

	if ([self isViewLoaded])
		[self reloadTabButtons];
}

- (void)setSelectedIndex:(NSUInteger)newSelectedIndex
{
     ///NSLog(@"sender.tag is%ld",newSelectedIndex);
	[self setSelectedIndex:newSelectedIndex animated:NO];
}

- (void)setSelectedIndex:(NSUInteger)newSelectedIndex animated:(BOOL)animated
{
      NSLog(@"sender.tag is%ld",newSelectedIndex);
 
    
	NSAssert(newSelectedIndex < [self.viewControllers count], @"View controller index out of bounds");

	if ([self.delegate respondsToSelector:@selector(mh_tabBarController:shouldSelectViewController:atIndex:)])
	{
		UIViewController *toViewController = (self.viewControllers)[newSelectedIndex];
		if (![self.delegate mh_tabBarController:self shouldSelectViewController:toViewController atIndex:newSelectedIndex])
			return;
	}

	if (![self isViewLoaded])
	{
		_selectedIndex = newSelectedIndex;
	}
	else if (_selectedIndex != newSelectedIndex)
	{
		UIViewController *fromViewController;
		UIViewController *toViewController;

		if (_selectedIndex != NSNotFound)
		{
			UIButton *fromButton = (UIButton *)[tabButtonsContainerView viewWithTag:TagOffset + _selectedIndex];
			[self deselectTabButton:fromButton];
			fromViewController = self.selectedViewController;
		}

		NSUInteger oldSelectedIndex = _selectedIndex;
		_selectedIndex = newSelectedIndex;

		UIButton *toButton;
		if (_selectedIndex != NSNotFound)
		{
			toButton = (UIButton *)[tabButtonsContainerView viewWithTag:TagOffset + _selectedIndex];
			[self selectTabButton:toButton];
			toViewController = self.selectedViewController;
		}

		if (toViewController == nil)  // don't animate
		{
			[fromViewController.view removeFromSuperview];
		}
		else if (fromViewController == nil)  // don't animate
		{
			toViewController.view.frame = contentContainerView.bounds;
			[contentContainerView addSubview:toViewController.view];
			[self centerIndicatorOnButton:toButton];

			if ([self.delegate respondsToSelector:@selector(mh_tabBarController:didSelectViewController:atIndex:)])
				[self.delegate mh_tabBarController:self didSelectViewController:toViewController atIndex:newSelectedIndex];
		}
		else if (animated)
		{
			CGRect rect = contentContainerView.bounds;
			if (oldSelectedIndex < newSelectedIndex)
				rect.origin.x = rect.size.width;
			else
				rect.origin.x = -rect.size.width;

			toViewController.view.frame = rect;
			tabButtonsContainerView.userInteractionEnabled = NO;

			[self transitionFromViewController:fromViewController
				toViewController:toViewController
				duration:0.3f
				options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionCurveEaseOut
				animations:^
				{
					CGRect rect = fromViewController.view.frame;
					if (oldSelectedIndex < newSelectedIndex)
						rect.origin.x = -rect.size.width;
					else
						rect.origin.x = rect.size.width;

					fromViewController.view.frame = rect;
					toViewController.view.frame = contentContainerView.bounds;
					[self centerIndicatorOnButton:toButton];
				}
				completion:^(BOOL finished)
				{
					tabButtonsContainerView.userInteractionEnabled = YES;

					if ([self.delegate respondsToSelector:@selector(mh_tabBarController:didSelectViewController:atIndex:)])
						[self.delegate mh_tabBarController:self didSelectViewController:toViewController atIndex:newSelectedIndex];
				}];
		}
		else  // not animated
		{
			[fromViewController.view removeFromSuperview];

			toViewController.view.frame = contentContainerView.bounds;
			[contentContainerView addSubview:toViewController.view];
			[self centerIndicatorOnButton:toButton];

			if ([self.delegate respondsToSelector:@selector(mh_tabBarController:didSelectViewController:atIndex:)])
				[self.delegate mh_tabBarController:self didSelectViewController:toViewController atIndex:newSelectedIndex];
		}
	}
}

- (UIViewController *)selectedViewController
{
	if (self.selectedIndex != NSNotFound)
		return (self.viewControllers)[self.selectedIndex];
	else
		return nil;
}

- (void)setSelectedViewController:(UIViewController *)newSelectedViewController
{
	[self setSelectedViewController:newSelectedViewController animated:NO];
}

- (void)setSelectedViewController:(UIViewController *)newSelectedViewController animated:(BOOL)animated
{
	NSUInteger index = [self.viewControllers indexOfObject:newSelectedViewController];
	if (index != NSNotFound)
		[self setSelectedIndex:index animated:animated];
}

- (void)tabButtonPressed:(UIButton *)sender
{
    NSLog(@"sender.tag is%ld",(long)sender.tag-TagOffset);
	[self setSelectedIndex:sender.tag - TagOffset animated:YES];
    pagenumber=sender.tag-TagOffset;
}

#pragma mark - Change these methods to customize the look of the buttons


- (void)selectTabButton:(UIButton *)button
{

   button.selected=YES;
 	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  

    
    if (button.tag==1000) {
        feedFilter.hidden=NO;
    }
   
}
- (void)deselectTabButton:(UIButton *)button
{

    
    feedFilter.hidden=YES;
    button.selected=NO;
    
    [self.view setBackgroundColor: [UIColor colorWithRed:20/255.0f green:135/255.0f blue:224/255.0f alpha:1.0f]];
  //  0099DC
        [button setBackgroundImage:[self imageWithColor:[self colorFromHexString:@"0099DC"] ] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (CGFloat)tabBarHeight
{
	return 46.0f;
}
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
@end
