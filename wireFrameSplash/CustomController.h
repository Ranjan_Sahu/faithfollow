//
//  CustomController.h
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomController : UITabBarController <UITabBarControllerDelegate>
@property (weak, nonatomic) IBOutlet UITabBar *tabbarOutlet;
@property  UIView *view1;
@end
