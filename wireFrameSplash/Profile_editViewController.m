//
//  Profile_editViewController.m
//  wireFrameSplash
//
//  Created by home on 4/11/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "Profile_editViewController.h"
#import "ProfileTableViewCell.h"
#import "DeviceConstant.h"
#import "NSString+StringValidation.h"
#import "Constant1.h"
#import "VSDropdown.h"
#import "AdditionInfoViewController.h"
#import "DeviceConstant.h"
#import "AppDelegate.h"
#import "WebServiceHelper.h"

@interface Profile_editViewController ()<VSDropdownDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    KLCPopup *popUp,*popUpView1;
    VSDropdown *_dropdown;
    NSMutableArray *dateArray,*years,*month,*meArrayEdit,*friendArrayEdit;
    NSMutableDictionary *webResponseData,*usersprofileDict;
    NSString *maleFemaleStr;
}
@end
@implementation Profile_editViewController
@synthesize scrollView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _bckview3.hidden=YES;
    _bckVIew2.hidden=YES;
    _editSettingView.hidden=YES;
    _profileView.hidden=YES;
    _ProfileMain.hidden=NO;
    _popUpView.hidden=YES;
    _deactivatePopView.hidden=YES;
    _editSybolSaveContinue.hidden=YES;
    
    
    [_bottmSaveContiNueBTN setTitle:@"VIEW ADDTIONAL INFOMATION" forState:UIControlStateNormal];
    _profileArray1=[[NSMutableArray alloc]init];
    _textArray1=[[NSMutableArray alloc]initWithObjects:@"Name",@"Email",@"Moblie",@"Birthdate",@"Gender",@"Denomination",nil];
    
    if (IS_IPHONE5||IS_IPHONE6||IS_IPHONE6PLUS||IS_IPHONE4) {
        _savebuttonBottomC.constant=-50;
        
    }
    self.popUpView.layer.cornerRadius = 8;
    self.deactivatePopView.layer.cornerRadius = 8;
    self.viewdectiveView.layer.borderWidth=1;
    
    self.ViewdeactiveView.hidden=YES;
    self.viewdectiveView.layer.borderColor=[UIColor grayColor].CGColor;
    popUp=[[KLCPopup alloc]init];
    popUpView1=[[KLCPopup alloc]init];
    _profileBTN.selected=YES;
    
    //Dropdown button
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    
    dateArray=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",@"31",nil];
    month=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",nil];
    
    meArrayEdit=[[NSMutableArray alloc]initWithObjects:@"hello1",@"hello2",@"hello3", nil];
    friendArrayEdit=[[NSMutableArray alloc]initWithObjects:@"hello1",@"hello2",@"hello3", nil];
    
    _maleBTN.selected=YES;
    
    [[_meView layer] setBorderWidth:1.0f];
    [[_meView layer] setBorderColor:[UIColor grayColor].CGColor];
    [[_friendView layer] setBorderWidth:1.0f];
    [[_friendView layer] setBorderColor:[UIColor grayColor].CGColor];
   
    webResponseData=[[NSMutableDictionary alloc]init];
    usersprofileDict=[[NSMutableDictionary alloc]init];
    [self webresponse];
    
    UITapGestureRecognizer   *singleTapGestureRecognizer =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
    [_profile_Picture addGestureRecognizer:singleTapGestureRecognizer];
    _profile_Picture.userInteractionEnabled=YES;
    _profile_Picture.multipleTouchEnabled=YES;

    
    
}


#pragma Tap gesture Method

-(void)handleSingleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer {
UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:picker animated:YES completion:NULL];

}


#pragma mark - imagePickerController Delegate methods.

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    self.profile_Picture.image = chosenImage;

    [picker dismissViewControllerAnimated:YES completion:NULL];

}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
 [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma

-(void)webresponse{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (appDelegate.isReachable) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please Note" message:NoNetwork  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message show];
        
    }
      else{
            NSDictionary *dict = @{
                    @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                    @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"profile" withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]])
                {
                     if([response[@"status"]intValue ] ==1){
                        webResponseData=[response valueForKey:@"data"];
                        [usersprofileDict addEntriesFromDictionary:[webResponseData valueForKey:@"usersprofile"]];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableView reloadData];
                        });
                    }
                    else{
                         dispatch_async(dispatch_get_main_queue(), ^{
                             NSMutableDictionary *emailDict = [[NSMutableDictionary alloc]init];
                            NSMutableArray *email = [[NSMutableArray alloc]init];
                            emailDict=[response valueForKey:@"errors"];
                            email=[emailDict valueForKey:@"email"];
                            NSLog(@"the dict is to be %@",email);
                            UIAlertView *at=[[UIAlertView alloc]initWithTitle:@"Pls Notes!" message:[email objectAtIndex:0] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [at show];
                        });
                     }
                 }
            } errorBlock:^(id error)
             {
                 NSLog(@"error");
                 
             }];
          
        }
  
    }
  -(void)viewWillLayoutSubviews{
    if (IS_IPHONE4||IS_IPHONE5) {
        scrollView.contentSize=CGSizeMake(scrollView.frame.size.width, 600);
        
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if([[webResponseData allKeys]count]>0)
    {
        return 6;
        
       
    }
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellidprofile"];
    if (indexPath.row==0) {
        cell.textNameValueLabel.text=[webResponseData valueForKey:@"first_name"];

    }
    if (indexPath.row==1) {
        cell.textNameValueLabel.text=[webResponseData valueForKey:@"email"];
        
    }
    if (indexPath.row==2) {
        cell.textNameValueLabel.text=@"8888888888";
        
    }
    if (indexPath.row==3) {
        cell.textNameValueLabel.text=[usersprofileDict valueForKey:@"date_of_birth"];

        
        
    }
    if (indexPath.row==4) {
        NSString *finderMale;
        if ([[usersprofileDict valueForKey:@"gender"]isEqualToString:@"0"]) {
            finderMale=@"Female";
        }
        else{
            finderMale=@"Male";
        }
        
        
        cell.textNameValueLabel.text=finderMale;

        
    }
    if (indexPath.row==5) {
        cell.textNameValueLabel.text=[usersprofileDict valueForKey:@"denomination"];
        
    }


    
    cell.textNameLabel.text=[_textArray1 objectAtIndex:indexPath.row];
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  50.0f ;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)profileBTN:(id)sender {
    _bckview3.hidden=YES;
    _bckVIew2.hidden=YES;
    _bckView1.hidden=NO;
    _editSymbolBTN.hidden=NO;
    _editSettingView.hidden=YES;
    _profileView.hidden=NO;
    _ProfileMain.hidden=NO;
    _editSettingBTN.selected=NO;
    _viewWallBTN.selected=NO;
    _profileBTN.selected=YES;
    _bottmSaveContiNueBTN.hidden=NO;
    _editSybolSaveContinue.hidden=YES;
    
    
}



- (IBAction)editsettingBTN:(id)sender {
    _bckView1.hidden=YES;
    _bckVIew2.hidden=YES;
    _bckview3.hidden=NO;
    _editSymbolBTN.hidden=YES;
    _profileView.hidden=YES;
    _ProfileMain.hidden=YES;
    _editSettingView.hidden=NO;
    _profileBTN.selected=NO;
    _viewWallBTN.selected=NO;
    _editSettingBTN.selected=YES;
    _ViewdeactiveView.hidden=NO;
    _bottmSaveContiNueBTN.hidden=YES;
    _editSybolSaveContinue.hidden=YES;
    
    
}

- (IBAction)editprofile:(id)sender {
    _bckView1.hidden=YES;
    _bckVIew2.hidden=YES;
    _bckview3.hidden=YES;
    _profileView.hidden=NO;
    _editSettingView.hidden=YES;
    _ProfileMain.hidden=YES;
    _editSybolSaveContinue.hidden=NO;
    
}

#pragma mark - Validation

- (NSString *)validateFields {
    
    NSString *errorMessage ;
    NSString *strName = [self.nameTXT.text removeWhiteSpaces];
    NSString *strEmail = [self.emailTXT.text removeWhiteSpaces];
    NSString *strMobile = [self.mobileTXT.text removeWhiteSpaces];
    NSString *strDenomination = [self.denominationTXT.text removeWhiteSpaces];
    
    if ([strName length] == 0) {
        errorMessage = username;
        return errorMessage;
    }
    
    
    if ([strEmail length] == 0) {
        errorMessage =EnterEmail;
        return errorMessage;
    }
    
    BOOL isEmailvalid = [Constant1 validateEmail:strEmail];
    if (!isEmailvalid) {
        errorMessage =InvalidEmail;
        return errorMessage;
    }
    
    if ([strMobile length] == 0) {
        errorMessage =MobileNumber;
        return errorMessage;
    }
    if ([strDenomination length] == 0) {
        errorMessage =denomination;
        return errorMessage;
    }
    
    return errorMessage;
}


- (IBAction)bottmSaveContinueBTN:(id)sender {
    
    if([_profileBTN isSelected]){
        NSLog(@"Selected!");
        
        AdditionInfoViewController *addition=[self.storyboard instantiateViewControllerWithIdentifier:@"AdditionInfoViewController"];
        [self presentViewController:addition animated:YES completion:nil];
    }
    
    
    
}



-(IBAction)deactivate:(id)sender{
    _deactivatePopView.hidden=NO;
    popUpView1=[KLCPopup popupWithContentView:self.deactivatePopView];
    [popUpView1 show];
    
}

- (IBAction)clickResetBTN:(id)sender {
    _popUpView.hidden=NO;
    popUp=[KLCPopup popupWithContentView:self.popUpView];
    [popUp show];
    
}
- (IBAction)savEditBTN:(id)sender {
    
    NSLog(@"save editbtn");
    
    
}


- (IBAction)editSybolSaveContinueMethod:(id)sender {
    
    NSString *strDob = [NSString stringWithFormat: @"%@%@%@%@%@ ",_yearBTN.titleLabel.text,@"/",_monthBTN.titleLabel.text,@"/",_dayBTN.titleLabel.text];
    NSLog(@"dob %@",strDob);
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (appDelegate.isReachable) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please Note" message:NoNetwork  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message show];
    }
    else
    {
        NSString *imagestr=[self base64String:chosenImage];
        NSDictionary *dict = @{
                               @"token" :[[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                               @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                               @"first_name" :self.nameTXT.text,
                               @"last_name" :@"sharma",
                               @"date_of_birth" :strDob,
                               @"image":imagestr,
                               @"gender":maleFemaleStr,
                               };
        NSLog(@"dict parametr  %@",dict);
        [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"update" withParameters:dict withHud:YES success:^(id response){
            if ([response isKindOfClass:[NSDictionary class]]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([response[@"status"]intValue ] ==1){
//
                        UIAlertView *at=[[UIAlertView alloc]initWithTitle:@"Pls Notes!" message:@"hello" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [at show];

                        
                    }
                    else{
                        NSMutableDictionary *DictError = [[NSMutableDictionary alloc]init];
                        NSMutableDictionary *emailDict = [[NSMutableDictionary alloc]init];
                        NSMutableArray *email = [[NSMutableArray alloc]init];
                        emailDict=[response valueForKey:@"errors"];
                        email=[emailDict valueForKey:@"email"];
                        NSLog(@"the dict is to be %@",email);
                        UIAlertView *at=[[UIAlertView alloc]initWithTitle:@"Pls Notes!" message:[email objectAtIndex:0] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [at show];
                        
                        
                    }
                });
            }
        } errorBlock:^(id error)
         {
         }];
        
    }
    
   
    
}

-(NSString*)base64String:(UIImage*)image{
    NSString *base64EncodeStr = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    base64EncodeStr = [base64EncodeStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    return base64EncodeStr;
}


-(IBAction)closePopViewChange:(id)sender{
    popUp.hidden=YES;
}
-(IBAction)closePopViewDeactivate:(id)sender{
    popUpView1.hidden=YES;
}

- (IBAction)meBTNMethod:(id)sender {
    
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Hello 0", @"Hello 1", @"Hello 2",nil];
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:[sender superview] :&f :arr :nil :@"down" button:sender];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [sender removeFromSuperview];
    sender = nil;
    [self rel];
}

-(void)rel{
    dropDown = nil;
}



- (IBAction)myfriendMethod:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Hello 0", @"Hello 1", @"Hello 2",nil];
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:[sender superview] :&f :arr :nil :@"down" button:sender];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
    
}

//Dropdown menu action

-(IBAction)monthDrop:(id)sender{
    
    [self showDropDownForButton:sender adContents:month multipleSelection:NO];
}
-(IBAction)dayDrop:(id)sender{
    [self showDropDownForButton:sender adContents:dateArray multipleSelection:NO];
}
-(IBAction)yearDrop:(id)sender{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int i2  = [[formatter stringFromDate:[NSDate date]] intValue];
    years = [[NSMutableArray alloc] init];
    for (int i=1960; i<=i2; i++) {
        [years addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    [self showDropDownForButton:sender adContents:years multipleSelection:NO];
}

-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents multipleSelection:(BOOL)multipleSelection
{
    
    // [_dropdown setDrodownAnimation:rand()%2];
    
    //  [_dropdown setAllowMultipleSelection:multipleSelection];
    [_dropdown setupDropdownForView:self.view direction:VSDropdownDirection_Down withBaseColor:[UIColor grayColor] scale:1.0];
    
    [_dropdown setupDropdownForView:sender];
    
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    
    if (_dropdown.allowMultipleSelection)
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:[[sender titleForState:UIControlStateNormal] componentsSeparatedByString:@";"]];
        
    }
    else
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
        
    }
    
}
#pragma mark - VSDropdown Delegate methods.
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected
{
    UIButton *btn = (UIButton *)dropDown.dropDownView;
    
    NSString *allSelectedItems = nil;
    if (dropDown.selectedItems.count > 1)
    {
        allSelectedItems = [dropDown.selectedItems componentsJoinedByString:@";"];
        
    }
    else
    {
        allSelectedItems = [dropDown.selectedItems firstObject];
        
    }
    [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    
}

- (IBAction)male_act:(id)sender {
    _maleBTN.selected=YES;
    _femaleBTN.selected=NO;
    maleFemaleStr=@"1";
    
    
}

- (IBAction)female_act:(id)sender {
    _maleBTN.selected=NO;
    _femaleBTN.selected=YES;
    maleFemaleStr=@"0";
    
}



- (IBAction)logout_Profilemethod:(id)sender {
}
@end
