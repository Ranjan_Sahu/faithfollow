//
//  AppDelegate.h
//  wireFrameSplash
//
//  Created by home on 4/1/16.
//  Copyright © 2016 home. All rights reserved.
//
///final 16th may 7:50

///final 20th may 5.07
#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "InfluencerClassViewController.h"
#import "Mixpanel.h"
#import <UserNotifications/UserNotifications.h>

//#import "GroupviewPagecontrollerViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

@property (assign, nonatomic) BOOL startValue,isProfileFriendViewControllerOnScreen;
@property (strong, nonatomic) UIWindow *window;
@property(assign,nonatomic)BOOL isReachable;
@property (strong, nonatomic) Reachability *reachability;
@property (nonatomic,retain)FBSDKLoginManager *fbManager;
@property(assign,nonatomic)BOOL showsuggestedGroup,checkClass;
@property(assign,nonatomic)BOOL showrateView,checkDisapper,isCommentSelected,dismischeckViewcontroller,checkSuggetedProfile,afterFolloSuggetedProfile,mycommunityCancelCheck;
@property (strong, nonatomic) NSString *influencerId;
@property(assign,nonatomic)BOOL isDeeplink;
@property (strong, nonatomic) NSString *ref_idData;
@property (strong, nonatomic) NSString *ref_typeData;
@property(assign,nonatomic)BOOL isDeeplinkPost;
@property(strong,nonatomic)Mixpanel *mxPanel;
@property(strong,nonatomic)NSDate *logindate;
@property(strong,nonatomic)NSDate *logdate;
@property(strong,nonatomic)NSData *deviceTokenMix;
@property(strong,nonatomic)NSMutableDictionary *headerDict;
@property(strong,nonatomic)NSString *isheaderAD;
@property(nonatomic,strong)  NSMutableArray *notfoundadsArray,*imageArray,*groupimageArray;
@property(assign,nonatomic)BOOL postimagecheckNotnull,checkwebviewService,postimageWebServiceNO,clickonImage,wallviewSubjectclick,selectedTabBool,groupMorenotnull,headeradcheck,checksuggestedInfluencerNotification,firstNotification;
@property(nonatomic,strong)NSData *tokendata;

@property(strong,nonatomic)UIView *notificationView;
@property(strong,nonatomic)UILabel *notificationLBL;
@property(assign,nonatomic)BOOL isNotPrayedLongTime ,seeMorecommnetNotnull;
@property(strong,nonatomic)NSString *notificationcheckPray;
@property(strong,nonatomic)NSString *tabFeedInfluenceDeep;



//@property (strong, nonatomic) GroupviewPagecontrollerViewController *grouppage;

//- (void)createYahooSession;
//- (void)pushContactListToVC;
//- (void)sendUserContactsRequest;
-(void)sendCommunityRequset:(NSString*)methodName andParadict:(NSDictionary*)param;
-(void)notification_alert;
- (void)setRoots;
-(void)showhud;
-(void)hidehud;
+ (AppDelegate *)sharedAppDelegate;
-(void)hideaddView;
-(void)unhideView;
-(void)removeadd;
-(void)addsView;
-(void)showInstructionOverlay:(NSString*)typeoverlayScreen;

@end

