//
//  Profile_editViewController.h
//  wireFrameSplash
//
//  Created by home on 4/11/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "KLCPopup.h"
#import "NIDropDown.h"

@interface Profile_editViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NIDropDownDelegate>
{
    NIDropDown *dropDown;
    UIImage *chosenImage;
 
}
@property (weak, nonatomic) IBOutlet UIButton *editBTN;
@property (weak, nonatomic) IBOutlet UIButton *viewWallBTN;
@property (weak, nonatomic) IBOutlet UIButton *profileBTN;
@property (weak, nonatomic) IBOutlet UIButton *editSettingBTN;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (weak, nonatomic) IBOutlet UIView *bckView1;
@property (weak, nonatomic) IBOutlet UIView *bckVIew2;
@property (weak, nonatomic) IBOutlet UIView *bckview3;


- (IBAction)profileBTN:(id)sender;
- (IBAction)viewmyallBTN:(id)sender;
- (IBAction)editsettingBTN:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *editSymbolBTN;
@property (weak, nonatomic) IBOutlet UIView *PagecontrollerVIew;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

//View outlet


@property (weak, nonatomic) IBOutlet UIView *profileView;
@property (weak, nonatomic) IBOutlet UIView *editSettingView;

@property (weak, nonatomic) IBOutlet UIView *ProfileMain;




@property(strong,nonatomic)IBOutlet UITableView *tableView;
@property(strong,nonatomic)NSMutableArray *profileArray1,*textArray1;

@property(weak,nonatomic)IBOutlet UIButton *saveContinueBTN;


// outlet for Profile View
@property (weak, nonatomic) IBOutlet UITextField *nameTXT;
@property (weak, nonatomic) IBOutlet UITextField *emailTXT;
@property (weak, nonatomic) IBOutlet UITextField *mobileTXT;
@property (weak, nonatomic) IBOutlet UITextField *denominationTXT;
@property (weak, nonatomic) IBOutlet UIButton *monthBTN;
@property (weak, nonatomic) IBOutlet UIButton *dayBTN;
@property (weak, nonatomic) IBOutlet UIButton *yearBTN;
@property (weak, nonatomic) IBOutlet UIButton *maleBTN;
@property (weak, nonatomic) IBOutlet UIButton *femaleBTN;

-(IBAction)monthDrop:(id)sender;
-(IBAction)dayDrop:(id)sender;
-(IBAction)yearDrop:(id)sender;

// outlet for  editSetting View

@property (weak, nonatomic) IBOutlet UITextField *appNameTXT;
@property (weak, nonatomic) IBOutlet UITextField *myfriendsTXT;
@property (weak, nonatomic) IBOutlet UITextField *enterpasswordTXT;
@property (weak, nonatomic) IBOutlet UITextField *retypePasswordTXT;

@property (weak, nonatomic) IBOutlet UIButton *changeBTN;
@property (weak, nonatomic) IBOutlet UIView *viewdectiveView;
@property (weak, nonatomic) IBOutlet UIButton *DeactiveView;


@property (weak, nonatomic) IBOutlet UIButton *deactivemethod;
// modified

@property (weak, nonatomic) IBOutlet UIButton *meBTN;
- (IBAction)meBTNMethod:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *myFriends;

- (IBAction)myfriendMethod:(id)sender;



@property (weak, nonatomic) IBOutlet UIButton *clicktoResetBTN;
- (IBAction)clickResetBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *saveEdit;

- (IBAction)savEditBTN:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *meView;


@property (weak, nonatomic) IBOutlet UIView *friendView;




@property (weak, nonatomic) IBOutlet UIView *ViewdeactiveView;



@property (weak, nonatomic) IBOutlet UIButton *bottmSaveContiNueBTN;

- (IBAction)bottmSaveContinueBTN:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *editSybolSaveContinue;

- (IBAction)editSybolSaveContinueMethod:(id)sender;



//- (IBAction)chnagemthod:(id)sender;
-(IBAction)deactivate:(id)sender;

//bottom conctaints

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *savebuttonBottomC;

// action on Save and continu button
- (IBAction)savecontinueMethod:(id)sender;
// male and female
-(IBAction)maleFemale:(id)sender;
-(IBAction)closePopViewChange:(id)sender;
-(IBAction)closePopViewDeactivate:(id)sender;

/* pop Up */

@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIView *deactivatePopView;
// outlet of slider menu button
- (IBAction)male_act:(id)sender;

- (IBAction)female_act:(id)sender;
// profile picture outlet
@property (weak, nonatomic) IBOutlet UIImageView *profile_Picture;


@property (weak, nonatomic) IBOutlet UILabel *profile_Name;
@property (weak, nonatomic) IBOutlet UIButton *logout_profile;
- (IBAction)logout_Profilemethod:(id)sender;

@end
