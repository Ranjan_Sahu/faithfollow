//
//  CustomController.m
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "CustomController.h"

@interface CustomController (){
    CGRect screenRect;
}

@end

@implementation CustomController
@synthesize view1;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.delegate = self;
   /* UITabBarItem *item0 = [_tabbarOutlet.items objectAtIndex:0];
    
    [item0 setFinishedSelectedImage:[UIImage imageNamed:@"success"] withFinishedUnselectedImage:[UIImage imageNamed:@"success"]];

    [item0 setImageInsets:UIEdgeInsetsMake(0, 0, 0, 0)];*/
    //[[UITabBar appearance] setTintColor:[UIColor redColor]];
   // [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:69.0/255.0f green:155.0/255.0f  blue:211.0/255.0f  alpha:1.0f]];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:59.0/255.0f green:147.0/255.0f  blue:192.0/255.0f  alpha:1.0f]];

     screenRect = [[UIScreen mainScreen] bounds];
 
    UIView *statusBarView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0,screenRect.size.width , 22)];
    statusBarView.backgroundColor  =  [UIColor colorWithRed:72/255.0f green:177/255.0f blue:222/255.0f alpha:1.0f];
    [self.view addSubview:statusBarView];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                             forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor yellowColor] }
                                             forState:UIControlStateSelected];
    
    
    UITabBarItem *tabBarItem = [[self.tabBarController.tabBar items] objectAtIndex:1];
    UIImage *unselectedImage = [UIImage imageNamed:@"logo.png"];
    UIImage *selectedImage = [UIImage imageNamed:@"logo.png"];
    [tabBarItem setImage: [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem setSelectedImage: [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
 
         
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    // Do Stuff!
     if(item.title == @"Feed")
     {
     }
}
- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [self.tabBar invalidateIntrinsicContentSize];
    
    
   
    CGFloat tabSize = 50;
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        tabSize = 32.0;
    }
    
    CGRect tabFrame = self.tabBar.frame;
    
   // tabFrame.size.width=(screenRect.size.width)-20;
    tabFrame.size.height = tabSize;
    
    tabFrame.origin.y = self.view.frame.origin.y+19;
    //tabFrame.size.width=320-10;
    
    self.tabBar.frame = tabFrame;
     NSLog(@"tabitem width is%f",tabSize);
    NSLog(@"tab width is%f",self.tabBar.frame.size.width);
    
    // Set the translucent property to NO then back to YES to
    // force the UITabBar to reblur, otherwise part of the
    // new frame will be completely transparent if we rotate
    // from a landscape orientation to a portrait orientation.
    
    self.tabBar.translucent = NO;
    self.tabBar.translucent = YES;
   view1=[[UIView alloc]initWithFrame:CGRectMake(300, tabFrame.origin.y, 20, 50)];
    view1.backgroundColor=[UIColor redColor];
  
    
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter)];
    letterTapRecognizer.numberOfTapsRequired = 1;
    [view1 addGestureRecognizer:letterTapRecognizer];
    [self.view addSubview:view1];

    
 
}
-(void)highlightLetter{
    NSLog(@"hi");
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
