//
//  ViewController.h
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//

#import <UIKit/UIKit.h>

@interface PagerViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (assign) NSUInteger page;




- (IBAction)next:(id)sender;
- (IBAction)changePage:(id)sender;
- (IBAction)nextPage:(id)sender;


@end
