//
//  AdditionInfoViewController.h
//  wireFrameSplash
//
//  Created by home on 4/14/16.
//  Copyright © 2016 home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "NIDropDown.h"

@interface AdditionInfoViewController : UIViewController<NIDropDownDelegate>
{
    NIDropDown *dropDown;

}

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *srcollView;

@property (weak, nonatomic) IBOutlet UITextField *jodtitleTXT;
@property (weak, nonatomic) IBOutlet UITextField *parishTXT;

@property (weak, nonatomic) IBOutlet UITextField *employeeTXT;
@property (weak, nonatomic) IBOutlet UITextField *highschoollTXT;


@property (weak, nonatomic) IBOutlet UITextField *siblingTXT;

@property (weak, nonatomic) IBOutlet UITextField *childrenTXT;

@property (weak, nonatomic) IBOutlet UITextView *fovorituteTXT;

@property (weak, nonatomic) IBOutlet UIImageView *commentImage;
@property (weak, nonatomic) IBOutlet UILabel *commentLBL;
@property (weak, nonatomic) IBOutlet UIImageView *likeImage;
@property (weak, nonatomic) IBOutlet UILabel *likeLBL;

@property (weak, nonatomic) IBOutlet UITextField *inspirationTXT;
@property (weak, nonatomic) IBOutlet UIButton *selectRelationBTN;

- (IBAction)selectRelationMethod:(id)sender;


- (IBAction)back_act:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *submitaddMethod;

- (IBAction)submitMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *selectView;


@end
