//
//  ViewController.m
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//

#import "PagerViewController.h"
#import "UIImageView+WebCache.h"
#import "CongratulationViewController.h"
#import "WebServiceHelper.h"
#import "VSDropdown.h"
#import "AppDelegate.h"
#import "Constant1.h"
#import "NSString+StringValidation.h"


@interface PagerViewController ()<UITextFieldDelegate,VSDropdownDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    CGRect screenRect;
    NSUInteger pagenumber;
    UITextField *activefield;
    BOOL Keyboard,alertshown;
    UIAlertView *alert;
    int pagenice;
    NSString *placeholdertext1;
    NSMutableArray *dateArray,*monthArray,*yearArray;
    UIButton* male_btn,* female_btn;
    VSDropdown *_dropdown;
    UIButton *btnDate,*btnMonth,*btnYear;
    UIImage *chosenImage;
    NSString *maleFemaleStr;

    
    
}
@property (strong, nonatomic) UITextField *emailTxt1,*passwordTXT1,*fisrtNameTXT,*lastNmaeTXT;
@property (assign) BOOL pageControlUsed;

@property (assign) BOOL rotating;
- (void)loadScrollViewWithPage:(int)page;
@end

@implementation PagerViewController

@synthesize scrollView;
@synthesize pageControl;
@synthesize pageControlUsed = _pageControlUsed;
@synthesize page = _page;
@synthesize rotating = _rotating;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setScrollEnabled:YES];
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.scrollView setShowsVerticalScrollIndicator:NO];
    [self.scrollView setDelegate:self];
    screenRect = [[UIScreen mainScreen] bounds];
    CGRect rect=CGRectMake(0,10,screenRect.size.width,self.scrollView.frame.size.height);
    scrollView.frame=rect;
    pagenumber=0;
    
    
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View1"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View2"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View3"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View4"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View5"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View6"]];
  
    
    UIViewController *viewcon1= [self.childViewControllers objectAtIndex:0];
    UIViewController *viewcon2= [self.childViewControllers objectAtIndex:1];
    UIViewController *viewcon3= [self.childViewControllers objectAtIndex:2];
    UIViewController *viewcon4= [self.childViewControllers objectAtIndex:3];
    UIViewController *viewcon5= [self.childViewControllers objectAtIndex:4];
    UIViewController *viewcon6= [self.childViewControllers objectAtIndex:5];
    
    UIButton *View1Button,*View2Button,*View3Button ;
    View1Button=[viewcon1.view viewWithTag:111];
    [View1Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
    _emailTxt1=[viewcon1.view viewWithTag:110];
    _emailTxt1.delegate=self;
    View2Button=[viewcon2.view viewWithTag:211];
    [View2Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
    
    _passwordTXT1=[viewcon2.view viewWithTag:210];
    _passwordTXT1.delegate=self;
    
    View3Button=[viewcon3.view viewWithTag:311];
    [View3Button addTarget:self action:@selector(forwardPage) forControlEvents:UIControlEventTouchUpInside];
    _fisrtNameTXT=[viewcon3.view viewWithTag:309];
    _fisrtNameTXT.delegate=self;
    _lastNmaeTXT=[viewcon3.view viewWithTag:310];
    _lastNmaeTXT.delegate=self;
    
    _emailTxt1.returnKeyType=UIReturnKeyDone;
    _passwordTXT1.returnKeyType=UIReturnKeyDone;
    _lastNmaeTXT.returnKeyType=UIReturnKeyDone;
    
    placeholdertext1=[[NSString alloc]init];
   // [[UIColor blueColor] setFill];
  // [[_emailTxt1 placeholder] drawInRect:rect withFont:[UIFont systemFontOfSize:16]];
    
    
    scrollView.scrollEnabled=false;
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionLeft;
    [scrollView addGestureRecognizer:swiperight];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionRight;
    [scrollView addGestureRecognizer:swipeleft];
    
//    pageControl.frame=CGRectMake( pageControl.frame.origin.x, pageControl.frame.origin.y, 450, 90);
    NSLog(@"height is%f",pageControl.frame.size.height);
     NSLog(@"width is%f",pageControl.frame.size.width);
    
    
    male_btn=[viewcon5.view viewWithTag:510];
    [male_btn addTarget:self action:@selector(male_act:) forControlEvents:UIControlEventTouchUpInside];
    female_btn=[viewcon5.view viewWithTag:509];
    [female_btn addTarget:self action:@selector(female_act:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* cameraPick=[viewcon6.view viewWithTag:611];
    [cameraPick addTarget:self action:@selector(camera) forControlEvents:UIControlEventTouchUpInside];
    UIButton*  galleryPick=[viewcon6.view viewWithTag:612];
    [galleryPick addTarget:self action:@selector(gallery) forControlEvents:UIControlEventTouchUpInside];
    UIButton* dateButton=[viewcon4.view viewWithTag:408];
    [dateButton addTarget:self action:@selector(datepicker:) forControlEvents:UIControlEventTouchUpInside];
    dateButton.layer.borderWidth=1.0f;
    dateButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    UIButton* monthButton=[viewcon4.view viewWithTag:409];
    [monthButton addTarget:self action:@selector(monthpicker:) forControlEvents:UIControlEventTouchUpInside];
    monthButton.layer.borderWidth=1.0f;
    monthButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    UIButton* yearButton=[viewcon4.view viewWithTag:410];
    [yearButton addTarget:self action:@selector(yearpicker:) forControlEvents:UIControlEventTouchUpInside];
    yearButton.layer.borderWidth=1.0f;
    yearButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [_dropdown setAdoptParentTheme:YES];
    [_dropdown setShouldSortItems:YES];
    dateArray=[[NSMutableArray alloc]init];
    monthArray=[[NSMutableArray alloc]init];
    yearArray=[[NSMutableArray alloc]init];
    for (int i=1; i<13; i++) {
        [monthArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    for (int i=1; i<32; i++) {
        [dateArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    for (int i=1950; i<2010; i++) {
        [yearArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
//    top=_topScroloViewCOnstraint.constant;
    
    male_btn.selected=YES;

    
    
  
    
}


#pragma Registration mehtod
-(void)continuePage5{
    
      NSString *strDob = [NSString stringWithFormat: @"%@%@%@%@%@ ",btnYear.titleLabel.text,@"/",btnMonth.titleLabel.text,@"/",btnDate.titleLabel.text];
    NSLog(@"dob %@",strDob);
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (appDelegate.isReachable) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please Note" message:NoNetwork  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message show];
      }
    else
    {
    NSString *imagestr=[self base64String:chosenImage];
    NSDictionary *dict = @{
                           @"first_name" :_fisrtNameTXT.text,
                           @"last_name" :_lastNmaeTXT.text,
                           @"email" :_emailTxt1.text,
                           @"password" :_passwordTXT1.text,
                           @"date_of_birth" :strDob,
                           @"image":imagestr,
                           @"gender":maleFemaleStr,
                           };
        
        NSLog(@"dict parametr  %@",dict);
    [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"user" withParameters:dict withHud:YES success:^(id response){
        if ([response isKindOfClass:[NSDictionary class]]){
            dispatch_async(dispatch_get_main_queue(), ^{
                if([response[@"status"]intValue ] ==1){
                    CongratulationViewController *congrats=[self.storyboard instantiateViewControllerWithIdentifier:@"CongratulationViewController"];
                    [self.navigationController pushViewController:congrats animated:YES];
                    
                    
                }
                else{
                    NSMutableDictionary *DictError = [[NSMutableDictionary alloc]init];
                    NSMutableDictionary *emailDict = [[NSMutableDictionary alloc]init];
                    NSMutableArray *email = [[NSMutableArray alloc]init];
                    emailDict=[response valueForKey:@"errors"];
                    email=[emailDict valueForKey:@"email"];
                    NSLog(@"the dict is to be %@",email);
                    UIAlertView *at=[[UIAlertView alloc]initWithTitle:@"Pls Notes!" message:[email objectAtIndex:0] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [at show];
  
                    
                }
            });
        }
    } errorBlock:^(id error)
     {
    }];

}
}



-(void)gallery{
    NSLog(@"gellery");
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)camera{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
#pragma mark - imagePickerController Delegate methods.

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    chosenImage = info[UIImagePickerControllerEditedImage];
   
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma female&malemethod
-(void)male_act:(id)sender{
    male_btn.selected=YES;
    female_btn.selected=NO;
    maleFemaleStr=@"0";
    
    
}
-(void)female_act:(id)sender{
    
    male_btn.selected=NO;
    female_btn.selected=YES;
    maleFemaleStr=@"1";
    
}


-(NSString*)base64String:(UIImage*)image{
    NSString *base64EncodeStr = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    base64EncodeStr = [base64EncodeStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    return base64EncodeStr;
}




-(void)datepicker:(id)sender{
    btnDate = (UIButton*)sender;
    [btnDate.titleLabel setTextAlignment: NSTextAlignmentLeft];
    NSLog(@"date is %@",btnDate.titleLabel.text);
    [self showDropDownForButton:sender adContents:dateArray multipleSelection:NO];
}
-(void)monthpicker:(id)sender{
     btnMonth = (UIButton*)sender;
    [btnMonth.titleLabel setTextAlignment: NSTextAlignmentLeft];
    NSLog(@"date is %@",btnMonth.titleLabel.text);

    [self showDropDownForButton:sender adContents:monthArray multipleSelection:NO];
}

-(void)yearpicker:(id)sender{
    btnYear = (UIButton*)sender;
    [btnYear.titleLabel setTextAlignment: NSTextAlignmentLeft];
    NSLog(@"date is %@",btnYear.titleLabel.text);

    [self showDropDownForButton:sender adContents:yearArray multipleSelection:NO];
}

#pragma mark - VSDropdown Delegate methods.


-(void)showDropDownForButton:(UIButton *)sender adContents:(NSArray *)contents multipleSelection:(BOOL)multipleSelection
{
    [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"signup"];
    [_dropdown setupDropdownForView:sender];
    
    [_dropdown setSeparatorColor:sender.titleLabel.textColor];
    
    if (_dropdown.allowMultipleSelection)
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:[[sender titleForState:UIControlStateNormal] componentsSeparatedByString:@";"]];
        
    }
    else
    {
        [_dropdown reloadDropdownWithContents:contents andSelectedItems:@[[sender titleForState:UIControlStateNormal]]];
        
    }
    
}
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected
{
    UIButton *btn = (UIButton *)dropDown.dropDownView;
    
    NSString *allSelectedItems = nil;
    if (dropDown.selectedItems.count > 1)
    {
        allSelectedItems = [dropDown.selectedItems componentsJoinedByString:@";"];
        
    }
    else
    {
        allSelectedItems = [dropDown.selectedItems firstObject];
        
    }
    [btn setTitle:allSelectedItems forState:UIControlStateNormal];
    
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
        [self.view endEditing:YES];
        [self forwardPage];


    //Do what you want here
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
      [self.view endEditing:YES];
    [self backPage];
}
-(void)backPage{
    
    if(_page==0){
        
    }else{
        // update the scroll view to the appropriate page
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * (_page - 1);
        frame.origin.y = 0;
        
        UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
        UIViewController *newViewController = [self.childViewControllers objectAtIndex:_page - 1];
        [oldViewController viewWillDisappear:YES];
        [newViewController viewWillAppear:YES];
        
        [self.scrollView scrollRectToVisible:frame animated:YES];
        
        self.pageControl.currentPage = _page - 1;
        // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
        _pageControlUsed = YES;
        
    }

}
-(void)forwardPage{
    if ([self check:pageControl.currentPage]==NO){
        
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * (_page+1);
        frame.origin.y = 0;
        
        UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
        UIViewController *newViewController = [self.childViewControllers objectAtIndex:_page +1];
        [oldViewController viewWillDisappear:YES];
        [newViewController viewWillAppear:YES];
        
        [self.scrollView scrollRectToVisible:frame animated:YES];
        
        self.pageControl.currentPage = _page+1 ;
        // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
        _pageControlUsed = YES;
    }
}
-(BOOL)check:(NSUInteger  )methodNum
{
    NSString *errorMessage=nil;
    if (methodNum==0) {
        errorMessage = [self validateFields];
    }else if(methodNum==1){
        errorMessage = [self validateFieldpassword];
    }else if(methodNum==2){
        errorMessage = [self validateFieldlastname];
    }
    if (errorMessage) {
        if (!alert) {
            alert= [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] ;
        [alert show];
            alert=nil;
        }
        return YES;
        
    }
    return NO;
    
}

- (NSString *)validateFieldlastname {
    NSString *errorMessage ;
    NSString *strlastname = [_lastNmaeTXT.text removeWhiteSpaces];
    NSString *firstrlastname = [_fisrtNameTXT.text removeWhiteSpaces];
    if ([strlastname length] == 0) {
        errorMessage =LastnameStr;
        return errorMessage;
    }else{
        if ([firstrlastname length] == 0){
            errorMessage =FirstnameStr;
            return errorMessage;
        }
           return 0;
    }
    
    return 0;
}
- (NSString *)validateFieldpassword {
    NSString *errorMessage ;
    NSString *strPassword = [self.passwordTXT1.text removeWhiteSpaces];
    if ([strPassword length] == 0) {
        errorMessage =EnterPassword;
        return errorMessage;
    }
    
    return 0;
}
- (NSString *)validateFields {
    
    NSString *errorMessage ;
    NSString *strEmail = [_emailTxt1.text removeWhiteSpaces];
  //  NSLog(@"hello is%@",_emailTxt1.text);
    if ([strEmail length] == 0) {
        errorMessage =EnterEmail;
        return errorMessage;
    }
    
    BOOL isEmailvalid = [Constant1 validateEmail:strEmail];
    if (!isEmailvalid) {
        errorMessage =InvalidEmail;
        return errorMessage;
    }
    
    
    return 0;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    for (NSUInteger i =0; i < [self.childViewControllers count]; i++) {
        [self loadScrollViewWithPage:i];
    }
    
    self.pageControl.currentPage = 0;
    _page = 0;
    [self.pageControl setNumberOfPages:[self.childViewControllers count]];
    
    
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    if (viewController.view.superview != nil) {
        [viewController viewWillAppear:animated];
    }
    
    self.scrollView.contentSize = CGSizeMake(screenRect.size.width * [self.childViewControllers count], scrollView.frame.size.height);
    
    self.scrollView.contentOffset=CGPointZero;
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self.childViewControllers count]) {
        UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewDidAppear:animated];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    if ([self.childViewControllers count]) {
        UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewWillDisappear:animated];
        }
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    if (viewController.view.superview != nil) {
        [viewController viewDidDisappear:animated];
    }
    [super viewDidDisappear:animated];
}
- (BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers {
    return NO;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    [viewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    _rotating = YES;
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    [viewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    self.scrollView.contentSize = CGSizeMake(screenRect.size.width * [self.childViewControllers count], scrollView.frame.size.height);
    NSUInteger page = 0;
    for (viewController in self.childViewControllers) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        viewController.view.frame = frame;
        page++;
    }
    
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * _page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:NO];
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    _rotating = NO;
    UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    [viewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (void)loadScrollViewWithPage:(int)page {
    if (page < 0)
        return;
    if (page >= [self.childViewControllers count])
        return;
    
    // replace the placeholder if necessary
    UIViewController *controller = [self.childViewControllers objectAtIndex:page];
    if (controller == nil) {
        return;
    }
    
    // add the controller's view to the scroll view
    if (controller.view.superview == nil) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        frame.size.width = screenRect.size.width;
        controller.view.frame = frame;
        [self.scrollView addSubview:controller.view];
    }
}


- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
    UIViewController *newViewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
    [oldViewController viewDidDisappear:YES];
    [newViewController viewDidAppear:YES];
    
    _page = self.pageControl.currentPage;
}

#pragma mark -
#pragma mark UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    
    if (_pageControlUsed || _rotating) {
        
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (self.pageControl.currentPage != page) {
        UIViewController *oldViewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
        UIViewController *newViewController = [self.childViewControllers objectAtIndex:page];
        [oldViewController viewWillDisappear:YES];
        [newViewController viewWillAppear:YES];
        self.pageControl.currentPage = page;
        [oldViewController viewDidDisappear:YES];
        [newViewController viewDidAppear:YES];
        _page = page;
        pagenumber=_page;
    }
}
// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
 _pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _pageControlUsed = NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"Text field did begin editing %@",textField.placeholder);
    activefield=textField;
    placeholdertext1=activefield.placeholder;
     activefield.autocapitalizationType = UITextAutocapitalizationTypeSentences;
      textField.placeholder=nil;
}

// This method is called once we complete editing
-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"Text field ended editing");
    activefield=textField;
//    NSString *placeholdertext;
//    if (textField==_emailTxt1) {
//        placeholdertext=@"Enter Your Email Address";
//    }else if(textField==_passwordTXT1) {
//           placeholdertext=@"Enter Your Password";
//    }else if(textField==_fisrtNameTXT) {
//        placeholdertext=@"Enter Your Firstname";
//    }else if(textField==_lastNmaeTXT) {
//        placeholdertext=@"Enter Your Lastname";
//    }
    
    
    if ([textField.text isEqualToString:@""] || [[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
    {
        [textField setText:@""];
        textField.placeholder = placeholdertext1;
    }
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [activefield resignFirstResponder];
    
    return YES;
}
@end
