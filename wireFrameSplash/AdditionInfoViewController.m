//
//  AdditionInfoViewController.m
//  wireFrameSplash
//
//  Created by home on 4/14/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "AdditionInfoViewController.h"
#import "ProfileTableViewCell.h"
#import "DeviceConstant.h"
#import "Constant1.h"
#import "NSString+StringValidation.h"
#import "AppDelegate.h"
#import "WebServiceHelper.h"


@interface AdditionInfoViewController ()


@end

@implementation AdditionInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[_selectView layer] setBorderWidth:1.0f];
    [[_selectView layer] setBorderColor:[UIColor grayColor].CGColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillLayoutSubviews{
    if (IS_IPHONE4||IS_IPHONE5) {
        _srcollView.contentSize=CGSizeMake(_srcollView.frame.size.width, 600);
        
    }
    
}



#pragma mark - Validation

- (NSString *)validateFields {
    
    NSString *errorMessage ;
    NSString *strJob = [self.jodtitleTXT.text removeWhiteSpaces];
    NSString *strParish = [self.parishTXT.text removeWhiteSpaces];
    NSString *strEmployee = [self.employeeTXT.text removeWhiteSpaces];
    NSString *strHigh = [self.highschoollTXT.text removeWhiteSpaces];
    NSString *strSibling = [self.siblingTXT.text removeWhiteSpaces];
    NSString *strchildren = [self.childrenTXT.text removeWhiteSpaces];
    NSString *strFavourite = [self.fovorituteTXT.text removeWhiteSpaces];
    NSString *strInspariartion = [self.inspirationTXT.text removeWhiteSpaces];
    
    if ([strJob length] == 0) {
        errorMessage = strjobEnter;
        return errorMessage;
    }
       if ([strParish length] == 0) {
        errorMessage = strParishEnter;
        return errorMessage;
    }
    
    if ([strEmployee length] == 0) {
        errorMessage =strEmployeeEnter;
        return errorMessage;
    }
    if ([strHigh length] == 0) {
        errorMessage = strHighEnter;
        return errorMessage;
    }
    if ([strSibling length] == 0) {
        errorMessage =strSiblingEnter;
        return errorMessage;
    }
    if ([strchildren length] == 0) {
        errorMessage = strchildrenEnter;
        return errorMessage;
    }
    if ([strFavourite length] == 0) {
        errorMessage =strFavouriteEnter;
        return errorMessage;
    }
    if ([strInspariartion length] == 0) {
        errorMessage = strInspariartionEnter;
        return errorMessage;
    }

    
    return 0;
}





- (IBAction)selectRelationMethod:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Single", @"Married", @"",nil];
    if(dropDown == nil) {
        CGFloat f = 200;
        dropDown = [[NIDropDown alloc]showDropDown:[sender superview] :&f :arr :nil :@"down" button:sender];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }

    
}

#pragma  NIDropdown delagte
- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [sender removeFromSuperview];
    sender = nil;
    [self rel];
}

-(void)rel{
    dropDown = nil;
}





- (IBAction)back_act:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)submitMethod:(id)sender {
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (appDelegate.isReachable) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please Note" message:NoNetwork  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message show];
        
    } else {
        
        NSString *errorMessage = [self validateFields];
        
        if (errorMessage) {
            [[[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
            return;
            
        }
        else{
            
            
            NSDictionary *dict = @{
                                   @"id" :[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                                   @"token" : [[NSUserDefaults standardUserDefaults]valueForKey:@"api_token"],
                                   @"job_title" :self.jodtitleTXT.text,
                                   @"parish" : self.parishTXT.text,
                                   @"employer" :self.employeeTXT.text,
                                   @"high_school" : self.highschoollTXT.text,
                                   @"relationship_status" :self.selectRelationBTN.titleLabel.text,
                                   @"children" : self.childrenTXT.text,
                                   @"favourite_prayer" :self.fovorituteTXT.text,
                                   @"inspirationa_saying" : self.inspirationTXT.text,
                                
                                   };
            [[WebServiceHelper sharedInstance] callPostDataWithMethod:@"additional_information" withParameters:dict withHud:YES success:^(id response){
                if ([response isKindOfClass:[NSDictionary class]]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([response[@"status"]intValue ] ==1){
                        UIAlertView *at=[[UIAlertView alloc]initWithTitle:@"Pls Notes!" message:response[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [at show];

                            
                        }
                        else{
                            NSString  *emailDict=[response valueForKey:@"message"];
                            NSLog(@"the dict is to be %@",emailDict);
                            UIAlertView *at=[[UIAlertView alloc]initWithTitle:@"Pls Notes!" message:emailDict delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [at show];
                            
                            
                        }
                    });
                }
            } errorBlock:^(id error)
             {
                 NSLog(@"error");
                 
             }];
        }
    }
    
}




@end
