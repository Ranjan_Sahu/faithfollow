//
//  PageContentViewController.m
//  PageViewDemo
//
//  Created by Simon on 24/11/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "PageContentViewController.h"
#import "CustomTableViewCell.h"
#import "AllprofileViewController.h"

@interface PageContentViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation PageContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

   // NSLog(@"page is%lu",(unsigned long)_pageIndex);
    _greenView.hidden=YES;

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.tableviewoutlet.tableFooterView=[UIView new];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* identifier=@"Cell";
    
    CustomTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];

    
    if (cell==nil) {
        
        cell =[[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    
    cell.namelabel.text=@"Adam oram";
    if (_pageIndex==1) {
        [cell.button_btn setTitle:@"View Profile" forState:UIControlStateNormal];
    }
    return cell;
}
- (IBAction)button_act:(id)sender {
      if (_pageIndex==1) {
          AllprofileViewController *allprofileView=[self.storyboard instantiateViewControllerWithIdentifier:@"AllprofileViewController"];
          [self.view addSubview:allprofileView.view];
      }
}
@end
