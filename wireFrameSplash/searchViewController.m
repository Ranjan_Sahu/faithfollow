//
//  secondViewController.m
//  wireFrameSplash
//
//  Created by webwerks on 11/04/16.
//  Copyright © 2016 home. All rights reserved.
//

#import "searchViewController.h"
#import "CustomTableViewCell.h"

@interface searchViewController ()

@end

@implementation searchViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
   UINavigationController *searchResultsController = [[self storyboard] instantiateViewControllerWithIdentifier:@"TableSearchResultsNavController"];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];
    self.searchController.searchResultsUpdater = self;
    
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x,
                                                       self.searchController.searchBar.frame.origin.y,
                                                       self.searchController.searchBar.frame.size.width, 44.0);
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    _airlines=[[NSMutableArray alloc]initWithObjects:@"A",@"B",@"C",@"D",nil];
    
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.tableView.tableFooterView=[UIView new];
    [self.navigationItem setHidesBackButton:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (cell==nil) {
       UITableViewCell* cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
 
   cell.namelabel.text=@"Harris Paul";
    
   
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 135.0f;
}

-(void)InvitesFriendMethod {
    NSLog(@"hello invited");
}

-(void)TickClickBTN:(UIButton*)sender {
    
    sender.selected=!sender.selected;
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [self.tableView convertPoint:buttonOrigin fromView:[sender superview]];
    NSIndexPath *rowIndexPath = [self.tableView indexPathForRowAtPoint:originInTableView];
    NSInteger section = [rowIndexPath section];
    NSLog(@"Facebook Button Clicked");
    NSLog(@"Tableview Section %ld",section);
    NSLog(@"Tableview Row %ld",rowIndexPath.row);
}


#pragma mark - UISearchControllerDelegate & UISearchResultsDelegate

// Called when the search bar becomes first responder
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchString = self.searchController.searchBar.text;
    [self updateFilteredContentForAirlineName:searchString];
    if (self.searchController.searchResultsController) {
        UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
       /* SearchResultsTableViewController *vc = (SearchResultsTableViewController *)navController.topViewController;
        vc.searchResults = self.searchResults;
        [vc.tableView reloadData];*/
    }
}

- (void)updateFilteredContentForAirlineName:(NSString *)searchString {
    if ([searchString length]>0)
    {
        if (searchString == nil) {
            //            self.searchResults = [flatArray mutableCopy];
        } else {
            NSMutableArray *searchResults = [[NSMutableArray alloc] init];
            NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name beginswith[c] %@", searchString];
            //            NSArray *flatArray = [[dict allValues] valueForKeyPath: @"@unionOfArrays.@self"];
            //            searchResults = [[[flatArray filteredArrayUsingPredicate:resultPredicate]valueForKey:@"name"] mutableCopy];
            self.searchResults = searchResults;
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
