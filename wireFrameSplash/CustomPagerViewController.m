//
//  CustomPageViewController.m
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//

#import "CustomPagerViewController.h"
#import "HomeViewController.h"
#import "Constant1.h"
#import "NSString+StringValidation.h"

@interface CustomPagerViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>{
    CGRect screenRect;
    int pagenumber;
 
    CGFloat top;
    NSMutableArray *dateArray,*monthArray,*yearArray;
    UIButton* male_btn,* female_btn;
}
@end

@implementation CustomPagerViewController

- (void)viewDidLoad
{
    // Do any additional setup after loading the view, typically from a nib.
    [super viewDidLoad];
    
    self.navigationController.navigationBar.topItem.title =@"";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.navigationItem.title = @"SIGN UP";
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    //    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View1"]];
    //    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View2"]];
    //    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View3"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View4"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View5"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"View6"]];
    //    UIViewController *viewcon1= [self.childViewControllers objectAtIndex:0];
    //    UIViewController *viewcon2= [self.childViewControllers objectAtIndex:1];
    //    UIViewController *viewcon3= [self.childViewControllers objectAtIndex:2];
    UIViewController *viewcon4= [self.childViewControllers objectAtIndex:3];
    UIViewController *viewcon5= [self.childViewControllers objectAtIndex:4];
    UIViewController *viewcon6= [self.childViewControllers objectAtIndex:5];
    
    //    _emialTXT=[viewcon1.view viewWithTag:110];
    //    _passwordTXT=[viewcon2.view viewWithTag:210];
    //    _fisrtNameTXT=[viewcon3.view viewWithTag:309];
    //    _lastNmaeTXT=[viewcon3.view viewWithTag:310];
    //    _emialTXT.delegate=self;
    //     _passwordTXT.delegate=self;
    //    _fisrtNameTXT.delegate=self;
    //    _lastNmaeTXT.delegate=self;
    
    UIButton *View1Button,*View2Button,*View3Button,*View4Button,*View5Button ;
    //    View1Button=[viewcon1.view viewWithTag:111];
    //    [View1Button addTarget:self action:@selector(continuePage1) forControlEvents:UIControlEventTouchUpInside];
    //    View2Button=[viewcon2.view viewWithTag:211];
    //    [View2Button addTarget:self action:@selector(continuePage2) forControlEvents:UIControlEventTouchUpInside];
    //    View3Button=[viewcon3.view viewWithTag:311];
    //    [View3Button addTarget:self action:@selector(continuePage3) forControlEvents:UIControlEventTouchUpInside];
    
    View5Button=[viewcon5.view viewWithTag:511];
    [View5Button addTarget:self action:@selector(continuePage5) forControlEvents:UIControlEventTouchUpInside];
    
    male_btn=[viewcon5.view viewWithTag:510];
    [male_btn addTarget:self action:@selector(male_act:) forControlEvents:UIControlEventTouchUpInside];
    female_btn=[viewcon5.view viewWithTag:509];
    [female_btn addTarget:self action:@selector(female_act:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* cameraPick=[viewcon6.view viewWithTag:611];
    [cameraPick addTarget:self action:@selector(camera) forControlEvents:UIControlEventTouchUpInside];
    UIButton*  galleryPick=[viewcon6.view viewWithTag:612];
    [galleryPick addTarget:self action:@selector(gallery) forControlEvents:UIControlEventTouchUpInside];
    UIButton* dateButton=[viewcon4.view viewWithTag:408];
    [dateButton addTarget:self action:@selector(datepicker:) forControlEvents:UIControlEventTouchUpInside];
    dateButton.layer.borderWidth=1.0f;
    dateButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    UIButton* monthButton=[viewcon4.view viewWithTag:409];
    [monthButton addTarget:self action:@selector(monthpicker:) forControlEvents:UIControlEventTouchUpInside];
    monthButton.layer.borderWidth=1.0f;
    monthButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    UIButton* yearButton=[viewcon4.view viewWithTag:410];
    [yearButton addTarget:self action:@selector(yearpicker:) forControlEvents:UIControlEventTouchUpInside];
    yearButton.layer.borderWidth=1.0f;
    yearButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
//    _dropdown = [[VSDropdown alloc]initWithDelegate:self];
//    [_dropdown setAdoptParentTheme:YES];
//    [_dropdown setShouldSortItems:YES];
    dateArray=[[NSMutableArray alloc]init];
    monthArray=[[NSMutableArray alloc]init];
    yearArray=[[NSMutableArray alloc]init];
    for (int i=1; i<13; i++) {
        [monthArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    for (int i=1; i<32; i++) {
        [dateArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    for (int i=1950; i<2010; i++) {
        [yearArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    top=_topScroloViewCOnstraint.constant;
    
    male_btn.selected=YES;
    

    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"signup"];
}
-(void)keyboardWasShown:(NSNotification*)aNotification
{
    _topScroloViewCOnstraint.constant=top-24.0f;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    _topScroloViewCOnstraint.constant=top+24.0f;
}

//-(void)gallery{
//    NSLog(@"gellery");
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
//    
//    [self presentViewController:picker animated:YES completion:NULL];
//}
//-(void)camera{
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//    
//    [self presentViewController:picker animated:YES completion:NULL];
//}
//#pragma mark - imagePickerController Delegate methods.
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    
//    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    //self.imageView.image = chosenImage;
//    NSLog(@"image is%@",chosenImage);
//    
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//    
//}
//- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
//    
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//    
//}
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
}
//  *filed1,*filed3,*filed4,*filed2;
#pragma mark - Validation













- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
