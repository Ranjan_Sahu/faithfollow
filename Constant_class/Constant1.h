//
//  Constant.h
//  Demp_projeect
//
//  Created by home on 2/15/16.
//  Copyright (c) 2016 home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import "WebServiceHelper.h"

static NSString *const NoNetwork = @"The Internet connection appears to be offline";
static NSString *const EnterEmail = @"Please enter Email Address";
static NSString *const EnterPassword = @"Please enter Password";
static NSString *const InvalidEmail = @"Please enter valid Email";
static NSString *const FirstnameStr = @"first name  should not be Empty";
static NSString *const LastnameStr =@"Last name should not be Empty";
static NSString *const ConfirmStr = @"Confirm should not be Empty";
static NSString *const phoneStr=@"phone should not be Empty";
static NSString *const REConfirmStr =@"Please reenter confirm password";
static NSString *const CountryStr=@"country should not be Empty";
static NSString *const ZipcodeStr =@"zipcode should not be Empty";
static NSString *const ZipcodeDigit =@"zipcode should  be  Equal six digit";
static NSString *const StateStr =@"State  should not be Empty";
static NSString *const CityStr =@"City should not be Empty";
static NSString *const CitylandStr =@"CityLand should not be Empty";
static NSString *const AddressStr =@"address field  should not be Empty";
static NSString *const STDStr=@"DOB field should not be Empty";
static NSString *const CurrentPass=@"Curentpassword should not be Empty";
static NSString *const NewpassStr=@"Newpassword  should not be Empty";
static NSString *const termselectedStr=@"please select Term and condition ! ";
static NSString *const GenderStr=@"please select Gender ! ";
static NSString *const zipStr1=@"Zipcode number should be six digits";



// edit Setting
static NSString *const appName=@"please enter your App Name";
static NSString *const myfriend=@"please enter your Friend";
static NSString *const enterpassword=@"Please enter Password";
static NSString *const retypepass=@"Please enter Retype Password";
static NSString *const username = @"User name  should not be Empty";
static NSString *const MobileNumber=@"phone number should be ten digits";
static NSString *const denomination=@"Select Denomination";

//Addition information

static NSString *const strjobEnter=@"Enter your Job Title";
static NSString *const strParishEnter=@"Enter your Parish";
static NSString *const strEmployeeEnter=@"Enter your Employee Title";
static NSString *const strHighEnter=@"Enter your high school";
static NSString *const strSiblingEnter=@"Enter your sibling";
static NSString *const strchildrenEnter=@"Enter your children";
static NSString *const strFavouriteEnter=@"Enter your favourite";
static NSString *const strInspariartionEnter=@"Enter your inspiration";





@interface Constant1 : NSObject
+ (BOOL)validateEmail:(NSString *)strEmailId;
+ (BOOL)myMobileNumberValidate:(NSString*)number;
+ (BOOL)myZipNumberValidate:(NSString*)number;


@end
