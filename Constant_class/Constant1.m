//
//  Constant.m
//  Demp_projeect
//
//  Created by home on 2/15/16.
//  Copyright (c) 2016 home. All rights reserved.
//

#import "Constant1.h"

@implementation Constant1

+ (BOOL)validateEmail:(NSString *)strEmailId {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:strEmailId];
}

+ (BOOL)myMobileNumberValidate:(NSString*)number{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    return [numberTest evaluateWithObject:number];
}

+ (BOOL)myZipNumberValidate:(NSString*)number{
    NSString *numberRegEx = @"[0-9]{6}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    return [numberTest evaluateWithObject:number];
    
}



@end
