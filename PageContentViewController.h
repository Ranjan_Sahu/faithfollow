//
//  PageContentViewController.h
//  PageViewDemo
//
//  Created by Simon on 24/11/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController

@property NSUInteger pageIndex;


@property (weak, nonatomic) IBOutlet UITableView *tableviewoutlet;
@property (strong, nonatomic) IBOutlet UIView *greenView;


- (IBAction)button_act:(id)sender;


@end
